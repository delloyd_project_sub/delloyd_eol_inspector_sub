﻿//*****************************************************************************
// Filename	: 	CatWrapper.cpp
// Created	:	2016/2/11 - 15:07
// Modified	:	2016/2/11 - 15:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "CatWrapper.h"
#include <strsafe.h>

#include "hwif\\hwif.h"
#include "boxlib\\cspace_lite.h"
#include "boxinc\box_type_win.h"
#include "boxinc\box_log_win.h"

// OLD : 0 // NEW : 1
#define LIVE_SQUAD_FOURCC				1


#define UYVY_TO_UYVY				0
#define UYVY_TO_NV12				1
#define UYVY_TO_YUY2				2

#define Y41P_TO_UYVY				3
#define Y41P_TO_NV12				4
#define Y41P_TO_YUY2				5

#define YUY2_TO_UYVY				6
#define YUY2_TO_NV12				7
#define YUY2_TO_YUY2				8

extern	cHWIF g_hw;

CCatWrapper::CCatWrapper()
{
	m_hTimerQueue		= NULL;
	m_hTimer_Dev_Mon	= NULL;
	m_dwCycle_Dev_Mon	= 300;

	for (int iCh = 0; iCh < VDCAPT_CODE_MAXCH; iCh++)
	{
		m_dtRPSOutTBL[iCh]		= 0; // Run FPS
		m_dtStartTBL[iCh]		= 0; // Start flag
		m_dtVLossTBL[iCh]		= 0; //-> 1이면 Open된 채널
		m_dtProcSkipTBL[iCh]	= 0;
	}

	m_hWndOwner			= NULL;
	m_nWM_ChgStatus		= NULL;
	m_nWM_RecvVideo		= NULL;
	m_dwSelectedCh		= 0;
	
	m_dwBDInx			= 0;
	m_dwBDID			= 0;
	m_dwNTSC			= 0;
	m_dwHD1orSD0		= 0;
	m_dwVS1orVC0		= 0;

	m_blBDOpenFlag		= FALSE;
	m_dwDCtxWnd_flg		= 1;
	
	m_dwBDFuncVIDEO		= 0;
	m_dwBDFuncVCAPT		= 0;
	m_dwBDFuncHCAPT		= 0;
	m_dwBDFuncSD_VS		= 0;
	m_dwBDFuncHD_VS		= 0;
	
	m_dwBDFuncACAPT		= 0;
	m_dwBDFuncAPLAY		= 0;
	m_dwBDFuncRUART		= 0;
	m_dwBDFuncSQUAD		= 0;

	m_dwstaVICH			= 0;
	m_dwendVICH			= 0;
	m_dwMaxVICH			= SDCAPT_CODE_MAXCH;
	m_dwMaxHDCH			= HDCAPT_CODE_MAXCH;
	m_dwMaxSDCH			= SDCAPT_CODE_MAXCH;

	CreateTimerQueue_Mon();
	
}

CCatWrapper::~CCatWrapper()
{
	TRACE(_T("<<< Start ~CCatWrapper >>> \n"));

	DeleteTimerQueue_Mon();

	TRACE(_T("<<< End ~CCatWrapper >>> \n"));
}

//=============================================================================
// Method		: CreateTimerQueue_Mon
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:13
// Desc.		:
//=============================================================================
void CCatWrapper::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue = CreateTimerQueue();
		if (NULL == m_hTimerQueue)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CreateTimerQueue_Mon ()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
void CCatWrapper::DeleteTimerQueue_Mon()
{
	__try
	{
		if (!DeleteTimerQueue(m_hTimerQueue))
			TRACE("DeleteTimerQueue failed (%d)\n", GetLastError());

	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CCatWrapper::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CCatWrapper::DeleteTimerQueue_Mon()\n"));
}

//=============================================================================
// Method		: CreateTimer_Dev_Mon
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
void CCatWrapper::CreateTimer_Dev_Mon()
{
	__try
	{
		TRACE(_T("CreateTimer_Dev_Mon\n"));

		TRACE(_T("Comart BD Monitoring : Start\n"));

		if (!CreateTimerQueueTimer(&m_hTimer_Dev_Mon, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine_Dev_Mon, (PVOID)this, 0, m_dwCycle_Dev_Mon, 0))
		{
			TRACE(_T("CreateTimer_Dev_Mon failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CreateTimer_Dev_Mon ()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimer_Dev_Mon
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
void CCatWrapper::DeleteTimer_Dev_Mon()
{
	__try
	{
		TRACE(_T("Comart BD Monitoring : Stop\n"));

		if (NULL != m_hTimer_Dev_Mon)
		{
			if (!DeleteTimerQueueTimer(m_hTimerQueue, m_hTimer_Dev_Mon, INVALID_HANDLE_VALUE))
			{
				TRACE(_T("DeleteTimerQueueTimer(m_hTimer_Dev_Mon) Error : \n"));
			}
			m_hTimer_Dev_Mon = NULL;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : DeleteTimer_Dev_Mon ()\n"));
	}

	TRACE(_T("타이머 종료 : CCatWrapper::DeleteTimer_Dev_Mon()\n"));
}

//=============================================================================
// Method		: TimerRoutine_Dev_Mon
// Access		: public static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
VOID CALLBACK CCatWrapper::TimerRoutine_Dev_Mon(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	((CCatWrapper*)lpParam)->Monitor_Dev();
}

//=============================================================================
// Method		: Monitor_Dev
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
void CCatWrapper::Monitor_Dev()
{
	// Timer (100ms)
	// 1. void	_VCAPT_WND::OnMsg_VidLoss(UINT IN_nID)
	//		g_hw._VL_STATUS(dtVL);	//DWORD	dtVL[VDCAPT_CODE_MAXCH];
	// 2. void _VCAPT_WND::OnMsg_SDIInfo(UINT IN_nIDEvent)
	//		nrv = g_hw._SDI_STATUS(	m_dtSDIXZTBL,  m_dtSDIYZTBL, m_dtSDIIPTBL, m_dtSDIFPSTBL, m_dtSDIVTGTBL);
	// 3. void	_VCAPT_WND::OnMsg_RunOut(UINT IN_nIDEvent)
	//		g_hw._VCAPT_GetFPS(dtFPS);	//DWORD	dtFPS[MAX_VCAPT_CHANNEL];

	// 1.	
	
	// 상태 체크
	g_hw._VL_STATUS(m_dtVL_Status);

	BOOL bChange = FALSE;
	for (DWORD dwi = 0; dwi < m_dwMaxVICH; dwi++)
	{
		if (m_dtVLossTBL[dwi] != m_dtVL_Status[dwi + m_dwstaVICH])
		{
			// 상태 변경 됨
			bChange = TRUE;
			m_dtVLossTBL[dwi] = m_dtVL_Status[dwi + m_dwstaVICH];
		}
	}

	// 오너 윈도우로 상태 변경 메세지 통지
	if (bChange)
	{
		if ((NULL != m_hWndOwner) && (NULL != m_nWM_ChgStatus))
		{
			::SendNotifyMessage(m_hWndOwner, m_nWM_ChgStatus, 0, 0);
		}
	}

	//i: OnMsg_RunOut : APND data
	for (DWORD dwi = 0; dwi < m_dwMaxVICH; dwi++)
	{
		if (m_dtVLossTBL[dwi])
			continue;

		//m_dtFPSOutTBL[dwi] = 0;
		m_dtRPSOutTBL[dwi] = 0;
		//m_dtCSZOutTBL[dwi] = 0;
	}
}

//=============================================================================
// Method		: Search_Cat3d
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/2/23 - 16:09
// Desc.		:
//=============================================================================
BOOL CCatWrapper::Search_Cat3d()
{
	if (g_hw._DETECT(HWI_FIND_ALL) < 0)							//if no found then
	{
		TRACE(_T("[board] => CAT3D board not found\r\n"));
		return FALSE;
	}
	TRACE(_T("[board] => CAT3D board %d EA found\r\n"), g_hw.m_tDETECT.dwNumB);

	for (UINT iIdx = 0; iIdx < g_hw.m_tDETECT.dwNumB; iIdx++)
	{
		g_hw.m_tDETECT.lpszNAME[iIdx];
	}

	UINT nSelBoard = 0;
	for (UINT iIdx = 0; iIdx < g_hw.m_tDETECT.dwNumB; iIdx++)
	{
		//CAT3D_ID_CAP02

		switch (GrabberBoardType)
		{
		case GBT_XECAP_50B:
			if (CAT3D_ID_CAP02 == g_hw.m_tDETECT.dwTYPE[iIdx])
			{
				nSelBoard = iIdx;//CAT3D_ID_CAP16LP; //_T("CAP16LP")
				//SetBrightnessContrast(110, 110);
			}
			break;
		case GBT_XECAP_400EF:
			if (CAT3D_ID_CAP32E == g_hw.m_tDETECT.dwTYPE[iIdx])
			{
				if (g_hw.m_tDETECT.tDetail[iIdx].dwCAT_BDID == 0x37)
					nSelBoard = iIdx;//CAT3D_ID_CAP16LP; //_T("CAP16LP")
			}
			break;
		case GBT_XECAP_400EH:
			if (CAT3D_ID_SD16EH == g_hw.m_tDETECT.dwTYPE[iIdx])
			{
				nSelBoard = iIdx;
			}
			break;
		default:
			break;
		}
	
		TRACE(_T("* board name : [ %s ] -----\r\n"),	g_hw.m_tDETECT.lpszNAME[iIdx]);
		TRACE(_T("  Model ID   : %08d\r\n"),	(DWORD)	g_hw.m_tDETECT.dwTYPE[iIdx]);
		TRACE(_T("  CARD Rev ID: %08d\r\n"),	(DWORD)	g_hw.m_tDETECT.tDetail[iIdx].dwHWRevisionID);
		TRACE(_T("  Customer ID: %08d\r\n"),	(DWORD)	g_hw.m_tDETECT.tDetail[iIdx].dwCustomerHwID);
		TRACE(_T("  FPGA Rev ID: %08d\r\n"),	(DWORD)	g_hw.m_tDETECT.tDetail[iIdx].dwFPGA_RevID);
		TRACE(_T("  CATBD RevID: %08d\r\n"),	(DWORD)	g_hw.m_tDETECT.tDetail[iIdx].dwCAT_BDID);
		TRACE(_T("  Max VideoCH: %8dCH\r\n"),	(DWORD)	g_hw.m_tDETECT.tDetail[iIdx].dwMaxVCapCHNumB);
	}

	if (0 < g_hw.m_tDETECT.dwNumB)
	{
		//Select_Board(0);
		Select_Board(nSelBoard);
	}

	return TRUE;
}

//=============================================================================
// Method		: Select_Board
// Access		: public  
// Returns		: void
// Parameter	: DWORD IN_dwInx
// Qualifier	:
// Last Update	: 2016/2/23 - 16:09
// Desc.		: 보드 인덱스로 보드 선택
//=============================================================================
void CCatWrapper::Select_Board(DWORD IN_dwInx)
{
	if (IsOpen_Board()) 
		return;
		
	m_dwBDInx	= IN_dwInx;
	m_dwBDID	= g_hw.m_tDETECT.dwTYPE[IN_dwInx];

	g_hw.m_tDETECT.dtEnSQUAD[IN_dwInx];
	g_hw.m_tDETECT.dtEnAPLAY[IN_dwInx];
	g_hw.m_tDETECT.dtEnACAPT[IN_dwInx];
	g_hw.m_tDETECT.dtMustVID[IN_dwInx];

	Init_Board();

	if (g_hw.m_tDETECT.dtMustVID[IN_dwInx])
	{
		m_dwBDFuncVIDEO = 1;
	}
}

//=============================================================================
// Method		: Init_Board
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/23 - 16:10
// Desc.		:
//=============================================================================
void CCatWrapper::Init_Board()
{
	m_blBDOpenFlag = 0;

	m_dwBDFuncVIDEO = 1;


	m_dwBDFuncVCAPT = 0;
	m_dwBDFuncHCAPT = 0;

	m_dwBDFuncSD_VS = 0;
	m_dwBDFuncHD_VS = 0;


	m_dwBDFuncACAPT = 0;
	m_dwBDFuncAPLAY = 0;
	m_dwBDFuncRUART = 0;
	m_dwBDFuncSQUAD = 0;

}

//=============================================================================
// Method		: IsOpen_Board
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/2/23 - 16:10
// Desc.		:
//=============================================================================
BOOL CCatWrapper::IsOpen_Board()
{
	return m_blBDOpenFlag;
}

//=============================================================================
// Method		: Open_Board
// Access		: public  
// Returns		: BOOL
// Parameter	: DWORD IN_dwNTSC
// Qualifier	:
// Last Update	: 2016/2/23 - 16:10
// Desc.		:
//=============================================================================
BOOL CCatWrapper::Open_Board(DWORD IN_dwNTSC)
{
	if (IsOpen_Board()) return TRUE;

	DWORD dwRv = 0;
	DWORD dwBDFuncFlag = 0;

	m_dwNTSC = IN_dwNTSC;

	dwBDFuncFlag = g_hw._GET_OPEN_FLAG(m_dwBDInx, m_dwBDFuncVIDEO, m_dwBDFuncACAPT, m_dwBDFuncAPLAY, m_dwBDFuncRUART, m_dwBDFuncSQUAD);

	if (g_hw._OPEN(m_dwBDInx, dwBDFuncFlag, IN_dwNTSC, &dwRv) == FALSE)
	{
		TRACE(_T("CAT3DI_dw_BDOpen(%X,%X,%X) FAIL ..\r\n"), m_dwBDInx, dwBDFuncFlag, IN_dwNTSC);
		return FALSE;
	}

	m_blBDOpenFlag = TRUE;

	TRACE(_T("[board]=>CAT3DI_BDOpen(%X, %X, %X) OK\r\n"), m_dwBDInx, dwBDFuncFlag, m_dwNTSC);

	return TRUE;
}

//=============================================================================
// Method		: Close_Board
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/23 - 16:11
// Desc.		:
//=============================================================================
void CCatWrapper::Close_Board()
{
	if (!IsOpen_Board())
		return;

	g_hw._CLOSE();

	TRACE(_T("[board]=>CAT3DI_BDClose(%X) OK\r\n"), m_dwBDInx);

	m_blBDOpenFlag = FALSE;
}

//=============================================================================
// Method		: Init_Begin
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/23 - 16:11
// Desc.		:
//=============================================================================
void CCatWrapper::Init_Begin()
{
	DWORD	 dwi;

	m_dwMaxHDCH = (DWORD)(g_hw.m_stVCAPT_MaxHDCH > HDCAPT_CODE_MAXCH) ? HDCAPT_CODE_MAXCH : (DWORD)g_hw.m_stVCAPT_MaxHDCH;
	m_dwMaxSDCH = (DWORD)(g_hw.m_stVCAPT_MaxSDCH > SDCAPT_CODE_MAXCH) ? SDCAPT_CODE_MAXCH : (DWORD)g_hw.m_stVCAPT_MaxSDCH;
	m_dwMaxVICH = (DWORD)(m_dwHD1orSD0) ? m_dwMaxHDCH : m_dwMaxSDCH;

	m_dwstaVICH = (DWORD)(m_dwHD1orSD0) ? (DWORD)(g_hw.m_stVCAPT_staHDCH) : (DWORD)(g_hw.m_stVCAPT_staSDCH);
	m_dwendVICH = (DWORD)(m_dwHD1orSD0) ? ((DWORD)(g_hw.m_stVCAPT_staHDCH) + m_dwMaxHDCH) : ((DWORD)(g_hw.m_stVCAPT_staSDCH) + m_dwMaxSDCH);

	ZeroMemory(m_tVIDI, sizeof(m_tVIDI));

	ZeroMemory(m_dtVLossTBL, sizeof(m_dtVLossTBL));

	ZeroMemory(m_dtResolCONST, sizeof(m_dtResolCONST));
	ZeroMemory(m_dtmtrxResolC, sizeof(m_dtmtrxResolC));

	ZeroMemory(m_dt960InpTBL, sizeof(m_dt960InpTBL));
	ZeroMemory(m_dtFPSInpTBL, sizeof(m_dtFPSInpTBL));
	ZeroMemory(m_dtRTOInpTBL, sizeof(m_dtRTOInpTBL));
	ZeroMemory(m_dt411InpTBL, sizeof(m_dt411InpTBL));
	ZeroMemory(m_dtCNMInpTBL, sizeof(m_dtCNMInpTBL));
	ZeroMemory(m_dtColInpTBL, sizeof(m_dtColInpTBL));

	ZeroMemory(m_dtColBriTBL, sizeof(m_dtColBriTBL));
	ZeroMemory(m_dtColConTBL, sizeof(m_dtColConTBL));
	ZeroMemory(m_dtColHueTBL, sizeof(m_dtColHueTBL));
	ZeroMemory(m_dtColSatTBL, sizeof(m_dtColSatTBL));
	ZeroMemory(m_dtColShaTBL, sizeof(m_dtColShaTBL));
	ZeroMemory(m_dtColRedTBL, sizeof(m_dtColRedTBL));
	ZeroMemory(m_dtColGrnTBL, sizeof(m_dtColGrnTBL));
	ZeroMemory(m_dtColBluTBL, sizeof(m_dtColBluTBL));

	//ZeroMemory(m_dtFPSOutTBL, sizeof(m_dtFPSOutTBL));
	ZeroMemory(m_dtRPSOutTBL, sizeof(m_dtRPSOutTBL));

	ZeroMemory(m_dtStartTBL, sizeof(m_dtStartTBL));

	ZeroMemory(m_dtProcSkipTBL, sizeof(m_dtProcSkipTBL));

	for (dwi = 0; dwi < m_dwMaxVICH; dwi++)
	{
		m_dtRTOInpTBL[dwi] = 0;
		m_dtFPSInpTBL[dwi] = (DWORD)g_hw.m_stVCAPT_FPSTBL[dwi + m_dwstaVICH];
		m_dt411InpTBL[dwi] = (DWORD)g_hw.m_stVCAPT_411TBL[dwi + m_dwstaVICH];
		//m_dtResolCONST[dwi] = (DWORD)g_hw.m_stVCAPT_ResTBL[dwi + m_dwstaVICH];
		m_dtResolCONST[dwi] = (DWORD)VCAPT_TRON_720X480; //VCAPT_TRON_704X480;

		if (m_dwHD1orSD0) // HD 영상
		{
			m_dtCNMInpTBL[dwi] = HWIHD_BRI;
			m_dtColInpTBL[dwi] = (DWORD)g_hw.m_stBRI[dwi + m_dwstaVICH];

			m_dtColBriTBL[dwi] = (DWORD)g_hw.m_stBRI[dwi + m_dwstaVICH];
			m_dtColConTBL[dwi] = (DWORD)0x80;
			m_dtColRedTBL[dwi] = (DWORD)g_hw.m_stRED[dwi + m_dwstaVICH];
			m_dtColGrnTBL[dwi] = (DWORD)g_hw.m_stGREEN[dwi + m_dwstaVICH];
			m_dtColBluTBL[dwi] = (DWORD)g_hw.m_stBLUE[dwi + m_dwstaVICH];

			switch (m_dtResolCONST[dwi])
			{
			case VCAPT_TRON_1920X1080P:
				m_dtmtrxResolC[dwi] = 1;
				break;

			case VCAPT_TRON_1920X1080I:
				m_dtmtrxResolC[dwi] = 2;
				break;

			case VCAPT_TRON_1920X540:
				m_dtmtrxResolC[dwi] = 3;
				break;

			case VCAPT_TRON_1280X720:
				m_dtmtrxResolC[dwi] = 4;
				break;

			case VCAPT_TRON_720X576P:
				m_dtmtrxResolC[dwi] = 5;
				break;

			case VCAPT_TRON_720X576:
				m_dtmtrxResolC[dwi] = 6;
				break;

			default:
				m_dtmtrxResolC[dwi] = VCAPT_TRON_DISABLE;
				break;
			}
		}
		else // SD 영상
		{
			m_dt960InpTBL[dwi] = 0;

			m_dtCNMInpTBL[dwi] = HWISD_BRI;
			m_dtColInpTBL[dwi] = (DWORD)g_hw.m_stBRI[dwi + m_dwstaVICH];

			m_dtColBriTBL[dwi] = (DWORD)g_hw.m_stBRI[dwi + m_dwstaVICH];
			m_dtColConTBL[dwi] = (DWORD)g_hw.m_stCON[dwi + m_dwstaVICH];
			m_dtColHueTBL[dwi] = (DWORD)g_hw.m_stHUE[dwi + m_dwstaVICH];
			m_dtColSatTBL[dwi] = (DWORD)g_hw.m_stSAT[dwi + m_dwstaVICH];
			m_dtColShaTBL[dwi] = (DWORD)0x80;

			m_dtmtrxResolC[dwi] = ((m_dtResolCONST[dwi] >= VCAPT_TRON_720X576) && (m_dtResolCONST[dwi] <= VCAPT_TRON_320X288)) ? (m_dtResolCONST[dwi] - VCAPT_TRON_720X576 + 1) : VCAPT_TRON_DISABLE;
		}
	}

	m_dwDCtxWnd_flg = 1;

	CreateTimer_Dev_Mon();
}

//=============================================================================
// Method		: Final_End
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/23 - 16:12
// Desc.		:
//=============================================================================
void CCatWrapper::Final_End()
{
	DeleteTimer_Dev_Mon();

	for (DWORD dwi = 0; dwi < m_dwMaxVICH; dwi++)
	{
		Stop_Capture(dwi);
	}
}

//=============================================================================
// Method		: Start_Capture
// Access		: public  
// Returns		: BOOL
// Parameter	: DWORD IN_dwCH
// Qualifier	:
// Last Update	: 2016/2/23 - 16:12
// Desc.		:
//=============================================================================
BOOL CCatWrapper::Start_Capture(DWORD IN_dwCH)
{
	DWORD	dwVL = m_dtVLossTBL[IN_dwCH];
	DWORD	dwFPS = m_dtFPSInpTBL[IN_dwCH];
	DWORD	dwSTA = m_dtStartTBL[IN_dwCH];

// 	if (dwVL == 0)
// 		return FALSE;
// 
// 	if (dwFPS == 0)
// 		return FALSE;
// 
 	if (dwSTA)
 		return FALSE;

	DWORD	dwRTO		= m_dtRTOInpTBL[IN_dwCH];
	DWORD	dwEXT411	= m_dt411InpTBL[IN_dwCH] % 3;
	DWORD	dwResolC	= m_dtResolCONST[IN_dwCH];
	DWORD	dw960H		= m_dt960InpTBL[IN_dwCH];

	DWORD	dwXZ = GetG_VCAPT_XZ(dw960H, dwResolC);
	DWORD	dwYZ = GetG_VCAPT_YZ(m_dwNTSC, dwResolC);
	DWORD	dwIP = GetG_VCAPT_IP(dwResolC);

	m_tVIDI[IN_dwCH].dwXZ = dwXZ;
	m_tVIDI[IN_dwCH].dwYZ = dwYZ;
	m_tVIDI[IN_dwCH].dw411 = dwEXT411;
	m_tVIDI[IN_dwCH].llcapture_cnt = 0;

// 	m_tVIDI[IN_dwCH].dwcvrt_flg = 0;
// 	m_tVIDI[IN_dwCH].dwcvrt_type = 0;//0 : UYVY, 2: YUY2
// 
// 	if (dwEXT411 == 1)
// 	{
// 		m_tVIDI[IN_dwCH].dwcvrt_flg = 1;
// 		m_tVIDI[IN_dwCH].dwcvrt_type = 0;//0 : UYVY, 2: YUY2
// 		m_tVIDI[IN_dwCH].amem.t_new((dwXZ * dwYZ * 2) + 128, 256);
// 	}

	get_flag_4squad(IN_dwCH, dwEXT411);

	//m_tVIDI[IN_dwCH].anv12mem.t_new((dwXZ * dwYZ * 2) + 128, 256);
	if (m_tVIDI[IN_dwCH].dwcvrt_flg)
	{
	//	m_tVIDI[IN_dwCH].amem.t_new((dwXZ * dwYZ * 2) + 128, 256);
	}


	DWORD dwOVI = 0;

	switch (GrabberBoardType)
	{
	case GBT_XECAP_50B:
	case GBT_XECAP_400EF:
		if (g_hw._VCAPT_STA(CBFunc_VCEvent, this, (IN_dwCH + m_dwstaVICH), dwResolC, dwEXT411, dw960H, dwOVI))
		{
			m_dtStartTBL[IN_dwCH] = 1;
		}
		break;
	case GBT_XECAP_400EH:
		if (g_hw._VCAPT_STA(OnMsg_VCEvent, this, (IN_dwCH + m_dwstaVICH), dwResolC, dwEXT411, dw960H, dwOVI))
		{
			m_dtStartTBL[IN_dwCH] = 1;
		}
		break;
	default:
		break;
	}


	return TRUE;
}
void		CCatWrapper::get_flag_4squad(DWORD IN_dwCH, DWORD IN_nin411)
{
	//i: input  UYVY:0, Y41P:1, YUY2:2
	//i: output UYVY:0, NV12:1, YUY2:2
	IN_nin411 %= 3;
	DWORD	dwctype = (IN_nin411 * 3) + LIVE_SQUAD_FOURCC;
	DWORD	dwflag = 0;


	switch (dwctype)
	{
	case UYVY_TO_UYVY: dwflag = 0;	break;		//UYVY to UYVY : use  ori  addr
	case UYVY_TO_NV12: dwflag = 0;	break;		//UYVY to NV12 : use  NV12 addr
	case UYVY_TO_YUY2: dwflag = 1;	break;		//UYVY to YUY2 : cvrt UYVY to YUY2

	case Y41P_TO_UYVY: dwflag = 1;	break;		//NV12 to UYVY : cvrt Y41P to UYVY
	case Y41P_TO_NV12: dwflag = 0;	break;		//NV12 to NV12 : use  NV12 addr
	case Y41P_TO_YUY2: dwflag = 1;	break;		//NV12 to YUY2 : cvrt Y41P to YUY2

	case YUY2_TO_UYVY: dwflag = 1;	break;		//YUY2 to UYVY : cvrt YUY2 to UYVY 
	case YUY2_TO_NV12: dwflag = 0;	break;		//YUY2 to NV12 : use  NV12 addr
	case YUY2_TO_YUY2: dwflag = 0;	break;		//YUY2 to YUY2 : use  ori  Addr

	default: dwflag = 0;
	}

	m_tVIDI[IN_dwCH].dwcvrt_flg = dwflag;
	m_tVIDI[IN_dwCH].dwcvrt_type = dwctype;




}
//=============================================================================
// Method		: Stop_Capture
// Access		: public  
// Returns		: void
// Parameter	: DWORD IN_dwCH
// Qualifier	:
// Last Update	: 2016/2/23 - 16:12
// Desc.		:
//=============================================================================
void CCatWrapper::Stop_Capture(DWORD IN_dwCH)
{
	if (!m_dtStartTBL[IN_dwCH])
		return;

	g_hw._VCAPT_STO(IN_dwCH + m_dwstaVICH);

	m_dtStartTBL[IN_dwCH] = 0;
}

//=============================================================================
// Method		: Start_CaptureAll
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/16 - 23:29
// Desc.		:
//=============================================================================
void CCatWrapper::Start_CaptureAll()
{
	//for (DWORD iCh = 0; iCh < m_dwMaxVICH; iCh++)
	for (DWORD iCh = 0; iCh < m_nUseChannelCount; iCh++)
		Start_Capture(iCh);
}

//=============================================================================
// Method		: Stop_CaptureAll
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/16 - 23:29
// Desc.		:
//=============================================================================
void CCatWrapper::Stop_CaptureAll()
{
	//for (DWORD iCh = 0; iCh < m_dwMaxVICH; iCh++)
	for (DWORD iCh = 0; iCh < m_nUseChannelCount; iCh++)
		Stop_Capture(iCh);
}

//=============================================================================
// Method		: CBFunc_VCEvent
// Access		: public static  
// Returns		: UINT
// Parameter	: PVOID IN_vpArg
// Parameter	: UINT IN_stCH
// Parameter	: UINT IN_stAddr
// Parameter	: UINT IN_stXZ
// Parameter	: UINT IN_stYZ
// Parameter	: UINT IN_st411
// Qualifier	:
// Last Update	: 2016/2/16 - 23:30
// Desc.		:
//=============================================================================
UINT CCatWrapper::CBFunc_VCEvent(PVOID IN_vpArg, UINT IN_stCH, UINT IN_stAddr, UINT IN_stXZ, UINT IN_stYZ, UINT IN_st411)
{
	CCatWrapper  *pThis = (CCatWrapper *)IN_vpArg;

	if (pThis == NULL)
		return 0;

	BYTE	*bpAddr = (BYTE *)IN_stAddr;
	int		 nChIdx = (int)IN_stCH - (int)(pThis->m_dwstaVICH);

	if (nChIdx < 0) 
		return 0;

	if (nChIdx >= (int)(pThis->m_dwMaxVICH)) 
		return 0;

	if (pThis->m_dtVLossTBL[nChIdx] == 0)
		return 0;

	if (pThis->m_dtStartTBL[nChIdx] == 0)
		return 0;

// 	if (pThis->m_dtProcSkipTBL[nstch] == 0)
// 		return 0;

	pThis->Event_Y41PtoRGB32(nChIdx, bpAddr);
	
	BYTE* pbyConv = pThis->Event_Conv422(nChIdx, bpAddr);
	if (pbyConv)
		bpAddr = pbyConv;

	// 선택된 카메라만 화면에 출력
	//if (pThis->m_dwSelectedCh == nChIdx)
// 	if (NULL != pThis->m_pwndRGB[nChIdx])
// 	{
// 		pThis->Event_ProcRPS(nChIdx);
// 
// 		pThis->Event_DCWndDraw(nChIdx, bpAddr);
// 		
// 		// 테스트용
// 		//pThis->Event_DCWndDraw(nChIdx, (LPBYTE)pThis->m_stRGB[nChIdx].m_pMatRGB[0]);
// 	}

	return 0;
}
UINT  	CCatWrapper::OnMsg_VCEvent(void   *IN_vpthis, stuint IN_stCH, stuint IN_stAddr, stuint IN_stXZ, stuint IN_stYZ, stuint IN_st411)
{
	CCatWrapper  *tpVCAPT = (CCatWrapper *)IN_vpthis;

	if (tpVCAPT == NULL) return 0;
	//if (tpVCAPT->GetSafeHwnd() == NULL) return 0;

// 	if (tpVCAPT->m_bLoop == TRUE)
// 	{
// 		return 0;
// 	}
// //	boxlog(1, EYECHK, _T("Start"));
// 	tpVCAPT->m_bLoop = TRUE;

	BYTE		*bpAddr = (BYTE *)IN_stAddr;
	int			 nstch = (int)IN_stCH - (int)(tpVCAPT->m_dwstaVICH);



	if (nstch < 0) return 0;
	if (nstch >= (int)(tpVCAPT->m_dwMaxVICH)) return 0;

	if (tpVCAPT->m_dtVLossTBL[nstch] == 0) return 0;
	//if (tpVCAPT->m_dtStaTBL[nstch] == 0) return 0;
	//if (tpVCAPT->m_dtProcSkipTBL[nstch] == 0) return 0;



	tpVCAPT->Event_ProcRPS(nstch);
//	boxlog(1, EYECHK, _T("Start2"));

// 	BYTE	*bpnv12, *bpddraw;
// 
// 	bpnv12 = tpVCAPT->Evnt_cvrt_nv12(nstch, bpAddr);
// 	bpddraw = (tpVCAPT->m_tVIDI[nstch].dw411 == 1) ? bpnv12 : bpAddr;



	tpVCAPT->Evnt_DCWndDraw(nstch, bpAddr);

//	boxlog(1, EYECHK, _T("Start"));


//	tpVCAPT->m_bLoop = FALSE;
// 	> TRACE(" TIME:%d:%d \n", GetTickCount()/1000, GetTickCount()%1000);
	//boxlog(1, EYECHK, _T("end"));

	return 0;
}

void	CCatWrapper::Evnt_DCWndDraw(DWORD IN_dwCH, LPBYTE IN_bpAddr)
{
// 	if (m_tVIDI[IN_dwCH].pDCWnd == NULL) return;
// 	if (m_tVIDI[IN_dwCH].pDCWnd->GetSafeHwnd() == NULL) return;

	DWORD	dw4CC;

	switch (m_tVIDI[IN_dwCH].dw411)
	{
	case 0: dw4CC = MKFOURCC('U', 'Y', 'V', 'Y');	break;

	case 1: if (m_tVIDI[IN_dwCH].dwcvrt_flg)
	{
				dw4CC = (m_tVIDI[IN_dwCH].dwcvrt_type == 0) ? MKFOURCC('U', 'Y', 'V', 'Y') : MKFOURCC('Y', 'U', 'Y', '2');
	}
			else
			{
				dw4CC = MKFOURCC('Y', '4', '1', 'P'); break;
			}
			break;

	case 2:
	default: dw4CC = MKFOURCC('Y', 'U', 'Y', '2'); break;
	}

	render(IN_dwCH, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, dw4CC, IN_bpAddr);
}


void CCatWrapper::render(DWORD IN_dwCH, DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422)
{
	RGBCONV(IN_dwCH, IN_dwXZ, IN_dwYZ, IN_dw4CC, IN_lpbYC422);

}
void CCatWrapper::RGBCONV(DWORD IN_dwCH, DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422)
{
	int		nrv = -1;

	DWORD biSizeImage = (ULONG)m_tVIDI[IN_dwCH].dwXZ * (ULONG)m_tVIDI[IN_dwCH].dwYZ * 4;

	// 버퍼에 메모리 할당
	m_stRGB[IN_dwCH].AssignMem(m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ);

	// Y41P 영상을 RGB32영상으로 변환
	switch (IN_dw4CC)
	{
	case MKFOURCC('U', 'Y', 'V', 'Y'):
		nrv = CSPACE_UYVY_TO_RGB32(IN_lpbYC422, IN_dwXZ, IN_dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);
		break;

	case MKFOURCC('Y', '4', '1', 'P'):
		nrv = CSPACE_Y41P_TO_RGB32(IN_lpbYC422, IN_dwXZ, IN_dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);

		break;

	case MKFOURCC('Y', 'U', 'Y', '2'):
		nrv = CSPACE_YUY2_TO_RGB32(IN_lpbYC422, IN_dwXZ, IN_dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);
		break;

	default:break;

		//dek> SAVEDIB ( IN_dwWidth, IN_dwHeight, MKFOURCC('U','Y','V','Y'), IN_lpbYC422 );

	}
// 
// 	switch (GrabberBoardType)
// 	{
// 	case GBT_XECAP_50B:
// 		nrv = CSPACE_UYVY_TO_RGB32(IN_lpbYC422, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);
// 		break;
// 	case GBT_XECAP_400EF:
// 		nrv = CSPACE_Y41P_TO_RGB32(IN_lpbYC422, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);
// 		break;
// 	case GBT_XECAP_400EH:
// 		break;
// 	default:
// 		break;
// 	}

	// Even 영상 삭제
	//DeleteEvenVideo(IN_dwCH, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0]);g

	// 50E 모델이면 누적 영상으로 판정해야 함
#ifdef USE_ADJUST_RGB
	if (Adjust_RGB32(IN_dwCH))
	{
		if ((NULL != m_hWndOwner) && (NULL != m_nWM_RecvVideo))
		{
			::SendMessage(m_hWndOwner, m_nWM_RecvVideo, (WPARAM)IN_dwCH, 0);
		}
	}
#else
	if ((NULL != m_hWndOwner) && (NULL != m_nWM_RecvVideo))
	{
		::SendMessage(m_hWndOwner, m_nWM_RecvVideo, (WPARAM)IN_dwCH, 0);
	}
#endif

// 	ZeroMemory(&m_biInf, sizeof(m_biInf));
// 
// 	m_biInf.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
// 	m_biInf.bmiHeader.biWidth = (ULONG)IN_dwXZ;
// 	m_biInf.bmiHeader.biHeight = (ULONG)IN_dwYZ;
// 	m_biInf.bmiHeader.biPlanes = 1;
// 	m_biInf.bmiHeader.biBitCount = 32;
// 	m_biInf.bmiHeader.biCompression = BI_RGB;
// 	m_biInf.bmiHeader.biSizeImage = (ULONG)IN_dwXZ * (ULONG)IN_dwYZ * 4;
// 
// 	if (m_dwRGBSZ != m_biInf.bmiHeader.biSizeImage)
// 	{
// 		if (m_lpbRGB)
// 		{
// 			m_lpbRGB = NULL;
// 			m_amem.t_del();
// 		}
// 	}
// 
// 	if (m_lpbRGB == NULL)
// 	{
// 		m_lpbRGB = (LPBYTE)m_amem.t_new(m_biInf.bmiHeader.biSizeImage + 128, 256);
// 		m_dwRGBSZ = m_biInf.bmiHeader.biSizeImage;
// 	}
// 
// 
// 	switch (IN_dw4CC)
// 	{
// 	case MKFOURCC('U', 'Y', 'V', 'Y'):
// 		nrv = CSPACE_UYVY_TO_RGB32(IN_lpbYC422, IN_dwXZ, IN_dwYZ, m_lpbRGB, 0);
// 		break;
// 
// 	case MKFOURCC('Y', '4', '1', 'P'):
// 		nrv = CSPACE_Y41P_TO_RGB32(IN_lpbYC422, IN_dwXZ, IN_dwYZ, m_lpbRGB, 0);
// 
// 		break;
// 
// 	case MKFOURCC('Y', 'U', 'Y', '2'):
// 		nrv = CSPACE_YUY2_TO_RGB32(IN_lpbYC422, IN_dwXZ, IN_dwYZ, m_lpbRGB, 0);
// 		break;
// 
// 	default:break;
// 
// 		//dek> SAVEDIB ( IN_dwWidth, IN_dwHeight, MKFOURCC('U','Y','V','Y'), IN_lpbYC422 );
// 
// 	}
// 


// 	if ((NULL != m_hWndOwner) && (NULL != m_nWM_RecvVideo))
// 	{
// 		::SendMessage(m_hWndOwner, m_nWM_RecvVideo, (WPARAM)IN_dwCH, 0);
// 	}
}
BYTE * 	CCatWrapper::Evnt_cvrt_nv12(DWORD IN_dwCH, LPBYTE IN_bpAddr)
{
	if (IN_bpAddr == NULL) return NULL;
	//if (m_tVIDI[IN_dwCH].anv12mem.m_align_ptr == NULL) return NULL;

	if (pData == NULL)
	{
		pData = new BYTE[m_tVIDI[IN_dwCH].dwXZ * m_tVIDI[IN_dwCH].dwYZ * 2];
	}


	BYTE	*ptrY = pData;
 	BYTE	*ptrU = ptrY + (m_tVIDI[IN_dwCH].dwXZ * m_tVIDI[IN_dwCH].dwYZ);
 	BYTE	*ptrV = ptrU + (m_tVIDI[IN_dwCH].dwXZ * m_tVIDI[IN_dwCH].dwYZ / 4);
 
 	if (m_tVIDI[IN_dwCH].dw411 == 1)
 		CSPACE_Y41P_TO_NV12(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, ptrY, ptrU, 0);
 	else if (m_tVIDI[IN_dwCH].dw411 == 0)
 		CSPACE_UYVY_TO_NV12(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, ptrY, ptrU, 0);
 	else	CSPACE_YUY2_TO_NV12(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, ptrY, ptrU, 0);


// 	m_stRGB[IN_dwCH].AssignMem(m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ);
// 
// 	// Y41P 영상을 RGB32영상으로 변환
// 	int nrv = 0;
// 
// 	switch (GrabberBoardType)
// 	{
// 	case GBT_XECAP_50B:
// 		nrv = CSPACE_UYVY_TO_RGB32(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);
// 		break;
// 	case GBT_XECAP_400EF:
// 		nrv = CSPACE_Y41P_TO_RGB32(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);
// 		break;
// 	case GBT_XECAP_400EH:
// 		nrv = CSPACE_UYVY_TO_RGB32(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);
// 		break;
// 	default:
// 		break;
// 	}

// 	if ((NULL != m_hWndOwner) && (NULL != m_nWM_RecvVideo))
// 	{
// 		::SendMessage(m_hWndOwner, m_nWM_RecvVideo, (WPARAM)IN_dwCH, 0);
// 	}

	return pData;
}


//=============================================================================
// Method		: Event_Conv422
// Access		: public  
// Returns		: LPBYTE
// Parameter	: DWORD IN_dwCH
// Parameter	: LPBYTE IN_bpAddr
// Qualifier	:
// Last Update	: 2016/2/23 - 16:12
// Desc.		:
//=============================================================================
LPBYTE CCatWrapper::Event_Conv422(DWORD IN_dwCH, LPBYTE IN_bpAddr)
{
	if (IN_bpAddr == NULL)
		return NULL;
	
	if (m_tVIDI[IN_dwCH].dwcvrt_flg == 0)
		return NULL;
	
	if (m_tVIDI[IN_dwCH].amem.m_align_ptr == NULL)
		return NULL;

	switch (m_tVIDI[IN_dwCH].dwcvrt_type)//0 : UYVY, 2: YUY2
	{
	case 0:
		CSPACE_Y41P_TO_UYVY(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, m_tVIDI[IN_dwCH].amem.m_align_ptr, 0);
		break;

	case 2:
	default:
		CSPACE_Y41P_TO_YUY2(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, m_tVIDI[IN_dwCH].amem.m_align_ptr, 0);
		break;
	}

	return m_tVIDI[IN_dwCH].amem.m_align_ptr;
}

//=============================================================================
// Method		: Event_ProcRPS
// Access		: public  
// Returns		: void
// Parameter	: DWORD IN_dwCH
// Qualifier	:
// Last Update	: 2016/2/23 - 16:13
// Desc.		:
//=============================================================================
void CCatWrapper::Event_ProcRPS(DWORD IN_dwCH)
{
	m_tVIDI[IN_dwCH].tFPS.Calcurate();
	m_dtRPSOutTBL[IN_dwCH] = m_tVIDI[IN_dwCH].tFPS.GetFPS();
}

//=============================================================================
// Method		: Event_DCWndDraw
// Access		: public  
// Returns		: void
// Parameter	: DWORD IN_dwCH
// Parameter	: LPBYTE IN_bpAddr
// Qualifier	:
// Last Update	: 2016/2/23 - 16:13
// Desc.		:
//=============================================================================
void CCatWrapper::Event_DCWndDraw(DWORD IN_dwCH, LPBYTE IN_bpAddr)
{
// 	if (m_tVIDI[IN_dwCH].pDCWnd == NULL)
// 		return;
// 
// 	if (m_tVIDI[IN_dwCH].pDCWnd->GetSafeHwnd() == NULL)
// 		return;

	DWORD	dw4CC;

	switch (m_tVIDI[IN_dwCH].dw411)
	{
	case 0:
		dw4CC = MKFOURCC('U', 'Y', 'V', 'Y');
		break;

	case 1:
		if (m_tVIDI[IN_dwCH].dwcvrt_flg)
		{
			dw4CC = (m_tVIDI[IN_dwCH].dwcvrt_type == 0) ? MKFOURCC('U', 'Y', 'V', 'Y') : MKFOURCC('Y', 'U', 'Y', '2');
		}
		else
		{
			dw4CC = MKFOURCC('Y', '4', '1', 'P');
			break;
		}
		break;

	case 2:
	default:
		dw4CC = MKFOURCC('Y', 'U', 'Y', '2');
		break;
	}

// 	m_tVIDI[IN_dwCH].pDCWnd->render(m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, dw4CC, IN_bpAddr);
// 
// 	if (IN_dwCH == m_dwSelectedCh)
// 	{
// 		if (NULL != m_pwndRGB_Sub)
// 			m_pwndRGB_Sub->render(m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, dw4CC, IN_bpAddr);
// 	}
}

//=============================================================================
// Method		: DeleteEvenVideo
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD IN_dwCH
// Parameter	: __inout LPBYTE lpVideo
// Qualifier	:
// Last Update	: 2016/2/23 - 16:12
// Desc.		:
//=============================================================================
void CCatWrapper::DeleteEvenVideo(__in DWORD IN_dwCH, __inout LPBYTE lpVideo)
{
	DWORD dwLineSize = m_tVIDI[IN_dwCH].dwXZ * 4;
	PBYTE pOffset = (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0];
	PBYTE pTarget = NULL;
	for (DWORD iCnt = 0; iCnt < m_tVIDI[IN_dwCH].dwYZ; iCnt += 2)
	{
		pTarget = pOffset + dwLineSize;
		memcpy(pTarget, pOffset, dwLineSize);

		pOffset += dwLineSize;
		pOffset += dwLineSize;
	}
}

//=============================================================================
// Method		: Adjust_RGB32
// Access		: public  
// Returns		: BOOL
// Parameter	: __in DWORD IN_dwCH
// Qualifier	:
// Last Update	: 2016/2/23 - 16:12
// Desc.		:
//=============================================================================
BOOL CCatWrapper::Adjust_RGB32(__in DWORD IN_dwCH)
{
	// 50E 모델이 아니면 리턴
#ifdef USE_ADJUST_RGB
 	if (CAT3D_ID_CAP02 != g_hw.m_tDETECT.dwTYPE[m_dwBDInx])
 		return TRUE;
 
 	m_stRGB[IN_dwCH].m_dwSize;
 
 	for (DWORD dwY = 0; dwY < m_tVIDI[IN_dwCH].dwYZ; dwY++)
 	{
 		for (DWORD dwX = 0; dwX < m_tVIDI[IN_dwCH].dwXZ; dwX++)
 		{
 			m_stRGB[IN_dwCH].m_pMatRGBTmp[dwY][dwX].rgbBlue	 += m_stRGB[IN_dwCH].m_pMatRGB[dwY][dwX].rgbBlue;
			m_stRGB[IN_dwCH].m_pMatRGBTmp[dwY][dwX].rgbGreen += m_stRGB[IN_dwCH].m_pMatRGB[dwY][dwX].rgbGreen;
			m_stRGB[IN_dwCH].m_pMatRGBTmp[dwY][dwX].rgbRed	 += m_stRGB[IN_dwCH].m_pMatRGB[dwY][dwX].rgbRed;
 		}
 	}
 
	// 카운트 증가
 	++m_stRGB[IN_dwCH].m_TempCnt;
 
 	if (MAX_VIDEO_TEST_CNT <= m_stRGB[IN_dwCH].m_TempCnt)
 	{
		// 평균 데이터		
		for (DWORD dwY = 0; dwY < m_tVIDI[IN_dwCH].dwYZ; dwY++)
		{
			for (DWORD dwX = 0; dwX < m_tVIDI[IN_dwCH].dwXZ; dwX++)
			{
				m_stRGB[IN_dwCH].m_pMatRGB[dwY][dwX].rgbBlue	= (BYTE)(m_stRGB[IN_dwCH].m_pMatRGBTmp[dwY][dwX].rgbBlue / m_stRGB[IN_dwCH].m_TempCnt);
				m_stRGB[IN_dwCH].m_pMatRGB[dwY][dwX].rgbGreen	= (BYTE)(m_stRGB[IN_dwCH].m_pMatRGBTmp[dwY][dwX].rgbGreen / m_stRGB[IN_dwCH].m_TempCnt);
				m_stRGB[IN_dwCH].m_pMatRGB[dwY][dwX].rgbRed		= (BYTE)(m_stRGB[IN_dwCH].m_pMatRGBTmp[dwY][dwX].rgbRed / m_stRGB[IN_dwCH].m_TempCnt);
			}
		}

		// Temp 초기화
		for (DWORD dwY = 0; dwY < m_tVIDI[IN_dwCH].dwYZ; dwY++)
		{
			for (DWORD dwX = 0; dwX < m_tVIDI[IN_dwCH].dwXZ; dwX++)
			{
				m_stRGB[IN_dwCH].m_pMatRGBTmp[dwY][dwX].rgbBlue	 = 0;
				m_stRGB[IN_dwCH].m_pMatRGBTmp[dwY][dwX].rgbGreen = 0;
				m_stRGB[IN_dwCH].m_pMatRGBTmp[dwY][dwX].rgbRed	 = 0;
			}
		}
		m_stRGB[IN_dwCH].m_TempCnt = 0;

		return TRUE;
	}
 	else
	{
		return FALSE;
	}

#else
	return TRUE;
#endif
}

//=============================================================================
// Method		: Event_Y41PtoRGB32
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD IN_dwCH
// Parameter	: __in LPBYTE IN_bpAddr
// Qualifier	:
// Last Update	: 2016/2/18 - 13:35
// Desc.		:
//=============================================================================
void CCatWrapper::Event_Y41PtoRGB32(__in DWORD IN_dwCH, __in LPBYTE IN_bpAddr)
{
	if (IN_bpAddr == NULL)
		return;

	DWORD biSizeImage = (ULONG)m_tVIDI[IN_dwCH].dwXZ * (ULONG)m_tVIDI[IN_dwCH].dwYZ * 4;

	// 버퍼에 메모리 할당
	m_stRGB[IN_dwCH].AssignMem(m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ);	

	// Y41P 영상을 RGB32영상으로 변환
	int nrv = 0;

	switch (GrabberBoardType)
	{
	case GBT_XECAP_50B:
		nrv = CSPACE_UYVY_TO_RGB32(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);
		break;
	case GBT_XECAP_400EF:
		nrv = CSPACE_Y41P_TO_RGB32(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);
		break;
	case GBT_XECAP_400EH:
		break;
	default:
		break;
	}
	
	// Even 영상 삭제
	//DeleteEvenVideo(IN_dwCH, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0]);g

	// 50E 모델이면 누적 영상으로 판정해야 함
#ifdef USE_ADJUST_RGB
	if (Adjust_RGB32(IN_dwCH)) 
	{
		if ((NULL != m_hWndOwner) && (NULL != m_nWM_RecvVideo))
		{
			::SendMessage(m_hWndOwner, m_nWM_RecvVideo, (WPARAM)IN_dwCH, 0);
		}
	}
#else
	if ((NULL != m_hWndOwner) && (NULL != m_nWM_RecvVideo))
	{
		::SendMessage(m_hWndOwner, m_nWM_RecvVideo, (WPARAM)IN_dwCH, 0);
	}
#endif
}

//=============================================================================
// Method		: Event_Y41PtoRGB32_T
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD IN_dwCH
// Parameter	: __in LPBYTE IN_bpAddr
// Qualifier	:
// Last Update	: 2016/12/28 - 16:24
// Desc.		:
//=============================================================================
void CCatWrapper::Event_Y41PtoRGB32_T(__in DWORD IN_dwCH, __in LPBYTE IN_bpAddr)
{
	if (IN_bpAddr == NULL)
		return;

	DWORD biSizeImage = (ULONG)m_tVIDI[IN_dwCH].dwXZ * (ULONG)m_tVIDI[IN_dwCH].dwYZ * 4;

	// 버퍼에 메모리 할당
	m_stRGB[IN_dwCH].AssignMem(m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ);

	// 	if (m_stRGB[IN_dwCH].m_dwRGBSZ != biSizeImage)
	// 	{
	// 		if (m_stRGB[IN_dwCH].m_lpbRGB)
	// 		{
	// 			m_stRGB[IN_dwCH].m_lpbRGB = NULL;
	// 			m_stRGB[IN_dwCH].m_amem.t_del();
	// 		}
	// 
	// 		m_stRGB[IN_dwCH].m_dwRGBSZ = biSizeImage;
	// 	}
	// 
	// 	if (m_stRGB[IN_dwCH].m_lpbRGB == NULL)
	// 	{
	// 		m_stRGB[IN_dwCH].m_lpbRGB = (LPBYTE)m_stRGB[IN_dwCH].m_amem.t_new(biSizeImage + 128, 256);
	// 		m_stRGB[IN_dwCH].m_dwRGBSZ = biSizeImage;
	// 	}


	// Y41P 영상을 RGB32영상으로 변환
	int nrv = 0;
	//nrv = CSPACE_Y41P_TO_RGB32(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, m_stRGB[IN_dwCH].m_lpbRGB, 0);
	nrv = CSPACE_Y41P_TO_RGB32(IN_bpAddr, m_tVIDI[IN_dwCH].dwXZ, m_tVIDI[IN_dwCH].dwYZ, (LPBYTE)m_stRGB[IN_dwCH].m_pMatRGB[0], 0);

	// RGB 원본데이터와 행렬 데이터와 비교
	//	RGBQUAD** pRGBItr = m_stRGB[IN_dwCH].m_pMatRGB;
	//	LPBYTE lpOffset = m_stRGB[IN_dwCH].m_lpbRGB;	

	//	for (DWORD dwY = 0; dwY < m_tVIDI[IN_dwCH].dwYZ; dwY++)
	//	{
	//		for (DWORD dwX = 0; dwX < m_tVIDI[IN_dwCH].dwXZ; dwX++)
	//		{
	// 			if (pRGBItr[dwY][dwX].rgbBlue != *lpOffset++)
	// 			{
	// 				TRACE(_T("Error: Blue, y: %d, x: %d\n"), dwY, dwX);
	// 			}
	// 
	// 			if (pRGBItr[dwY][dwX].rgbGreen != *lpOffset++)
	// 			{
	// 				TRACE(_T("Error: Green, y: %d, x: %d\n"), dwY, dwX);
	// 			}
	// 
	// 			if (pRGBItr[dwY][dwX].rgbRed != *lpOffset++)
	// 			{
	// 				TRACE(_T("Error: Red, y: %d, x: %d\n"), dwY, dwX);
	// 			}
	// 			
	// 			lpOffset++;
	//		}
	//	}
}

//=============================================================================
// Method		: SetBrightnessContrast
// Access		: public  
// Returns		: void
// Parameter	: BYTE Bright
// Parameter	: BYTE Constrast
// Qualifier	:
// Last Update	: 2016/12/28 - 16:24
// Desc.		:
//=============================================================================
void CCatWrapper::SetBrightnessContrast(BYTE Bright, BYTE Constrast)
{
	DWORD	dwReturn;
	for (UINT nCh = 0; nCh < m_nUseChannelCount; nCh++)
	{
		dwReturn = (DWORD)CAT3DI_BRIGHTNESSControl(m_dwBDInx, nCh, Bright); //빛의 세기 입력
		dwReturn = (DWORD)CAT3DI_CONTRASTControl(m_dwBDInx, nCh, Constrast);
// 		dwReturn = (DWORD)CAT3DI_HUEControl(m_dwBDInx, nCh, 128);
// 		dwReturn = (DWORD)CAT3DI_SATURATIONUControl(m_dwBDInx, nCh, 128);
	}
}

//=============================================================================
// Method		: SetFPS
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/12/28 - 16:24
// Desc.		:
//=============================================================================
void CCatWrapper::SetFPS()
{
	DWORD dtBUP[VDCAPT_CODE_MAXCH] = {0, };
	for (UINT dwi = 0; dwi < m_nUseChannelCount; dwi++)
	{
		dtBUP[dwi + m_dwstaVICH] = 30;//m_dtFPSInpTBL[dwi];
	}
	g_hw._VCAPT_frameWr   (m_dwstaVICH, m_dwendVICH,    dtBUP);
}
