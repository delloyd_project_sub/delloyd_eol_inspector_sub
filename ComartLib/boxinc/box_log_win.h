

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifndef _BOX_LOG_WIN_V369VW_INC
#define _BOX_LOG_WIN_V369VW_INC

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if defined(_WIN64)
#pragma message("--- [box_log_win.h][WIN64]")
#else 
#if defined(_WIN32)
#pragma message("--- [box_log_win.h][WIN32]")
#else
#error "COMPILER NOT SELECT"
#endif
#endif
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-








// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// *
// * 
// *
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
#include <stdlib.h>			//i:B.OF>exit call
#include <tchar.h>
#define BOXEXIT				99
#define BOXNODLAY 			0
#define BOXDLAY 			3

#define RET_FALSE			FALSE
#define RET_TRUE			TRUE

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	windprintf(stuint IN_stlev, TCHAR *IN_lpcszStr, ...);

//i>#define LOG_METHOD 			OutputDebugString
//i>#define ERR_METHOD 			OutputDebugString
#define LOG_METHOD 				windprintf
#define ERR_METHOD 				windprintf



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
enum	DMSG_LEVEL 
{		DMSG_HWERR		= 0x0001,	DMSG_SWERR	= 0x0002,
		DBGTR			= 0x0004,	EYECHK		= 0x0008,			
		NO0D0A			= 0x8000,
};














// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define boxlog(a, b, ...)      		                    			\
		{	if ((a))                                       			\
			{	LOG_METHOD(b, __VA_ARGS__);							\
			}														\
		}

#define boxreturn0(a, c, ...)					                    \
		{	if ((a))                                       		    \
			{   LOG_METHOD(c, __VA_ARGS__);							\
				return ;											\
			}														\
		}

#define boxreturn(a, b, c, ...)        		                    	\
		{	if ((a))                                       		    \
			{   LOG_METHOD(c, __VA_ARGS__);							\
				return b;											\
			}														\
		}

#define brxreturn(a, b, c, ...)        		                    	\
		{	if (!(a))                                   		    \
			{   LOG_METHOD(c, __VA_ARGS__);							\
				return b;											\
			}														\
		}

#define boxassert(a, b, ...)        		                    	\
		{	if ((a))                                       		    \
			{	LOG_METHOD(DMSG_SWERR, _T("%s:%s:%d:"), __FILE__,__FUNCTION__,__LINE__);	\
				LOG_METHOD(DMSG_SWERR, __VA_ARGS__);				\
																	\
				if (b == BOXEXIT  ) exit(0); 						\
				if (b != BOXNODLAY) Sleep (b);						\
			}														\
		}

#define brxassert(a, b, ...)	                    		      	\
		{	if ((a))                                       		    \
			{	LOG_METHOD(DMSG_SWERR, _T("%s:%s:%d:"), __FILE__,__FUNCTION__,__LINE__);	\
				LOG_METHOD(DMSG_SWERR, __VA_ARGS__);				\
																	\
				if (b == BOXEXIT  ) exit(0); 						\
				if (b != BOXNODLAY) Sleep (b);						\
			}														\
		}
















// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_BOX_LOG_WIN_V369VW_INC)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-




