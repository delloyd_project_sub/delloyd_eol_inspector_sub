
/* 	filename: 	data file
 * **************************************************************************************
 *
 * 	created  :	2012/11/28
 *	author   :	boxucom@gmail.com
 *
 *  version  :  1	[2012/11/28] : START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifndef _BOX_DFILE_S_
#define _BOX_DFILE_S_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#include "..\\boxinc\\box_type_win.h"
#include "..\\boxinc\\box_log_win.h"














// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *	@class		
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
class			dfile
{
public:
	virtual	   ~dfile() { close(); }
				dfile() { m_nopen = 0; m_hfile = INVALID_HANDLE_VALUE; }


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	enum		{ DF_WRITE = 0,	DF_READ = 1, };	

	int 		open(int IN_nRW, const TCHAR *IN_szfilename, ...);
	void		close();

	int			write(LPBYTE IN_bpbuf, int IN_nsize);
	int			read (LPBYTE IN_bpbuf, int IN_nsize);
	
// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
	int			fname_dlg(CWnd*  IN_pMom,  int IN_nRW, const TCHAR *IN_szfiletype, const TCHAR *IN_szfilter);
	
	int			fname_get(TCHAR *IO_szbuf, int IN_nbufsiz);
	int 		fname_set(const TCHAR *IN_szfilename, ...);



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	HANDLE			m_hfile;
	int				m_nopen;
	int				m_nRW;

	LARGE_INTEGER	m_tfsize;	

// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
	TCHAR			m_fname_slct[MAX_PATH + 128];
};












// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_BOX_DFILE_S_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-








