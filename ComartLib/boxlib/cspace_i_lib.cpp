
/* 	filename : 	cspace lib link
 * **************************************************************************************
 *
 * 	created  :	2013/01/17
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 * 
 *  version  :  1	[2013/01/17] : START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */

#include "StdAfx.h"
#include "..\\boxinc\\box_type_win.h"
#include "..\\boxinc\\box_log_win.h"

#include "cspace_lite.h"



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>#pragma optimize("", off)


#if defined(_WIN64)
	#pragma	comment(lib,"cspace_lite_64b.lib")
#else
	#pragma	comment(lib,"cspace_lite_32b.lib")
#endif







/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: 
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

