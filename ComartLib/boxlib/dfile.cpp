

/* 	filename : 	d file
 * **************************************************************************************
 *
 * 	created  :	2011/11/13
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *				0	[08/07/2003] : reference code [  CAT3D SDK  ]
 *  version  :  1   [2011/11/13] : START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */

#include "StdAfx.h"
#include "..\boxinc\box_type_win.h"
#include "..\boxinc\box_log_win.h"

#include "dfile.h"

#include <windows.h>
#include <stdio.H>
#include <strsafe.h>


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>#pragma optimize("", off)











/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: d file
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * @ open / close
//  
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		dfile::open(int	IN_nRW, const TCHAR *IN_szfilename, ...)
{
	if (IN_szfilename)
	{	va_list			args;	
		TCHAR			lpszbuf[MAX_PATH + 128];

		va_start		(args,    IN_szfilename);
		StringCbVPrintf	(lpszbuf, sizeof(lpszbuf), IN_szfilename, args);	
		va_end			(args);

		int	nstrcnt		= (int)_tcslen(lpszbuf);
		_tcsncpy_s (m_fname_slct, _countof(m_fname_slct), lpszbuf, nstrcnt );
	}


	if (m_nopen) close();

	m_nRW = IN_nRW;	
	if (IN_nRW == DF_READ)
	{	
		m_hfile = ::CreateFile( m_fname_slct, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (m_hfile == INVALID_HANDLE_VALUE) return -1;

		::GetFileSizeEx(m_hfile, &m_tfsize);		
	} else 
	{
		m_hfile = ::CreateFile( m_fname_slct, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (m_hfile == INVALID_HANDLE_VALUE) return -1;
		m_tfsize.QuadPart = 0;		
	}

	m_nopen = 1;
	return 1;
}


void	dfile::close()
{
	if (m_hfile == INVALID_HANDLE_VALUE) return;

	::CloseHandle(m_hfile);
	m_hfile = INVALID_HANDLE_VALUE;
	m_nopen = 0;

	
}







// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * @ read / write
//  
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int 	dfile::write(LPBYTE IN_bpbuf, int IN_nsize)
{
	boxreturn( (m_hfile == INVALID_HANDLE_VALUE), -1, DBGTR, _T("file not open\n"));
	boxreturn( (IN_bpbuf== NULL),				  -1, DBGTR, _T("input pointer (IN_bpbuf) is null\n"));
	boxreturn( (IN_nsize==    0),				  -1, DBGTR, _T("input size (IN_nsize) is 0\n"));

	BOOL	blrv;
	DWORD	dwwrn;

	blrv = ::WriteFile(m_hfile, IN_bpbuf, (DWORD)IN_nsize, &dwwrn, NULL);	
	return (int)dwwrn;
}


int 	dfile::read(LPBYTE IN_bpbuf, int IN_nsize)
{
	boxreturn( (m_hfile == INVALID_HANDLE_VALUE), -1, DBGTR, _T("file not open\n"));
	boxreturn( (IN_bpbuf== NULL),				  -1, DBGTR, _T("input pointer (IN_bpbuf) is null\n"));
	boxreturn( (IN_nsize==    0),				  -1, DBGTR, _T("input size (IN_nsize) is 0\n"));

	BOOL	blrv;
	DWORD	dwrdn;	

	blrv = ::ReadFile(m_hfile, IN_bpbuf, (DWORD)IN_nsize, &dwrdn, NULL);
	return (int)dwrdn;
}














// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * @ file name
//  
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		dfile::fname_dlg
(CWnd*  IN_pMom,  int IN_nRW, 
 const  TCHAR *IN_szfiletype, const TCHAR *IN_szfilter)

{	
	boxreturn( (IN_pMom       == NULL), -1, DBGTR, _T("input pointer (IN_pMom) is null\n"));
	boxreturn( (IN_szfiletype == NULL), -1, DBGTR, _T("input pointer (IN_szfiletype) is null\n"));
	boxreturn( (IN_szfilter   == NULL), -1, DBGTR, _T("input pointer (IN_szfilter) is null\n"));


	INT_PTR			 nrv;
	CFileDialog		*pcFDlg;
	CString			 cstrfname;

	pcFDlg = new CFileDialog((BOOL)IN_nRW, IN_szfiletype, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, IN_szfilter, IN_pMom);
	nrv			= pcFDlg->DoModal();
	cstrfname	= pcFDlg->GetPathName();
	delete pcFDlg;

	if (nrv == IDCANCEL) return 0;

	_tcsncpy_s(m_fname_slct, _countof(m_fname_slct), cstrfname, _tcslen(cstrfname));
	return 1;
}


int 	dfile::fname_get(TCHAR *IO_szbuf, int IN_nbufsiz)
{
	boxreturn( (IO_szbuf	== NULL), -1, DBGTR, _T("input pointer (IO_szbuf) is null\n"));
	boxreturn( (IN_nbufsiz	==    0), -1, DBGTR, _T("input size (IN_nbufsiz) is 0\n"));

	StringCbPrintf(IO_szbuf, IN_nbufsiz, _T("%s"), m_fname_slct);
	//del>_tcsncpy_s (IO_szbuf, IN_nbufsiz, m_fname_slct, _tcslen(m_fname_slct));
	return 1;
}



int 	dfile::fname_set(const TCHAR *IN_szfilename, ...)
{
	boxreturn( (IN_szfilename == NULL), -1, DBGTR, _T("input pointer (IN_szfilename) is null\n"));

	va_list			args;	
	TCHAR			lpszbuf[MAX_PATH + 128];

	va_start		(args,    IN_szfilename);
	StringCbVPrintf	(lpszbuf, sizeof(lpszbuf), IN_szfilename, args);	
	va_end			(args);
	
	_tcsncpy_s (m_fname_slct, _countof(m_fname_slct), lpszbuf, _tcslen(lpszbuf) );
	return 1;
}









