﻿
/* 	filename: 	ddi div wnd
 * **************************************************************************************
 * 	created  :	2014/09/24
 *	author   :	boxucom@gmail.com
 * 	file base:	MFC
 *
 *  version  :  1	[2014/09/24] : START CODE -> CLASS MADE / CODE WRITING..
 * **************************************************************************************
 */

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if !defined(_ddi_divwnd_dI_)
#define _ddi_divwnd_dI_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*
//*	
//*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//#include "..\\resource.h"
#include "..\\boxlib\\stools.h"
#include "..\\boxlib\\ddif.h"

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define DIVW_HDBASE1						1			// current (14.11.24) SD no support
#define DIVW_SDBASE0						0			// current (14.11.24) SD no support

#define DIVW_BASE_XZ						1920
#define DIVW_BASE_YZ						1088
#define DIVW_BASE_UYVY						0			// UYVY
#define DIVW_BASE_YUY2						2			// YUY2	
#define DIVW_BASE_SIZ						(1920 * 1080 * 2)

#define DDIV_MODE_MAXCH						16
#define DDIV_MODE_MAXMODE					11

#define	DDIV_MODE_01SCRN					0		// 1x1  mode
#define DDIV_MODE_02SCRN1					1		// 2X1  mode
#define DDIV_MODE_02SCRN2					2		// 1X2  mode
#define DDIV_MODE_04SCRN					3		// 2X2  mode
#define DDIV_MODE_06SCRN1					4		// 3X2  mode
#define DDIV_MODE_06SCRN2					5		// 1big mode
#define DDIV_MODE_08SCRN					6		// 1big mode
#define DDIV_MODE_09SCRN					7		// 3X3  mode	
#define DDIV_MODE_10SCRN					8		// 2big mode
#define DDIV_MODE_13SCRN					9		// 1big mode
#define DDIV_MODE_16SCRN					10		// 4x4  mode

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!		@class		
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
class	 ddif;

class	 ddi_div_wnd	: public CWnd
{
public:
		 ddi_div_wnd();
virtual ~ddi_div_wnd();

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	void	CreateNShow	(DWORD IN_dwClientXZ, DWORD IN_dwClientYZ, TCHAR *IN_lptcszWndName, DWORD IN_dwTOP);
		
	void	ddi_open	(int in_nmaxch, int in_nDDIF_type, int in_nHDbase1or0);
	void	ddi_close	();

	void	division	(DWORD IN_dwsta, DWORD  IN_dwmode);
	void	render		(DWORD IN_dwCH,  DWORD  IN_dwXZ, DWORD IN_dwYZ, 
						 DWORD IN_dw4CC, LPBYTE IN_bpYC);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:	
	int		ddi_ch_open	(DWORD IN_nch,  DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC);
	void	ddi_ch_close(DWORD IN_nch);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:
	ddif	   *m_pddif;	
	int			m_division_inited;		
	int			m_nddif_syncch;

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:
	DWORD		m_dwDIVsta, m_dwDIVmode, m_dwDIVMaxCH, m_dwInpMaxCH;
	DWORD		m_dwDrawEn_DIVCHtbl[DDIV_MODE_MAXCH];
	RECT		m_rtDrawRecttbl	   [DDIV_MODE_MAXCH];	

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:
	struct			vch_inform_TAG
	{	
		DWORD		dwXZ, dwYZ, dw4CC;		
		DWORD		dwInited;
	}	m_tdivVCH	[DDIV_MODE_MAXCH];

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:
	int			m_nPaintEn;

	TCHAR		m_lpszWndName[MAX_PATH];	
    RECT		m_rectOrSz;	

	BITMAPINFO	m_biInf;
	
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * MFC MESSAGE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	virtual BOOL	DestroyWindow();
	
public:
	DECLARE_MESSAGE_MAP()	
	afx_msg void	OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void	OnLButtonUp(  UINT nFlags, CPoint point);
	afx_msg void	OnClose();
	afx_msg void	OnPaint();
	
};

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_ddi_divwnd_dI_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

