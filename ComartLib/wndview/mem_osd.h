﻿
/* 	filename: 	mem osd
 * **************************************************************************************
 * 	created  :	2014/05/30
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 *
 *  version  :  1	[2014/05/30] : START CODE -> CLASS MADE / CODE WRITING.. 
 * **************************************************************************************
 */

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if !defined(_MEM_OSD_d9_I_)
#define _MEM_OSD_d9_I_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#include "..\\resource.h"
#include "..\\boxlib\\stools.h"

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *	@class		
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

class		mem_osd
{
public:	   ~mem_osd();
			mem_osd();

	int		m_nready;		
// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
public:	
	BOOL	make_str	(TCHAR *in_szstr, 
						 TCHAR *in_szfontname, int in_nfontyz, COLORREF in_nfontcolor,
						 int	in_nxz, int in_nyz);

	BOOL	merge		(LPBYTE in_bpimg,	int	in_nstax,  int in_nstay,
											int in_nimgxz, int in_nimgyz);
	
private:
	BYTE	*m_bposd;
	int		m_nosdxz, m_nosdyz;

};


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_MEM_OSD_d9_I_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

