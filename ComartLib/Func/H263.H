/*	H263.H
 * **************************************************************************************
 *							
 *	Version		: 0.1	(06/26/2004)
 *						START CODE ->	CLASS MADE / CODE WRITING..
 *
 * ======================================================================================
 * 
 *
 *
 *
 *
 * **************************************************************************************
 *				  Dynamic Linking Library Interface for H.263 LIBRARY
 *
 *	WRITER		: jae-Sange PARK 
 *	ENVIRONMENT	: Windows98 /SE /ME /2K /XP (REGULAR DLL)
 *				  Visual Studio C++ .NET 2K3
 *
 *
 *
 *
 *
 *
 *	TARGET		: ComArT PC Device  
 *
 *
 * **************************************************************************************
 */

  

// **************************************************************************************
// * FILE RE-INCLUDE CHECK
// **************************************************************************************
#if !defined(AFX_H263_Y123012_H__7F8ABB06_20FD_11D4_A6AD_00C026DD15CC__INCLUDED_)
#define AFX_H263_Y123012_H__7F8ABB06_20FD_11D4_A6AD_00C026DD15CC__INCLUDED_
// **************************************************************************************
#if _MSC_VER >= 1000
#pragma once
#endif 




// **************************************************************************************
// * BASIC CLASS and FUNCTIONS INCLUDE
// **************************************************************************************
#include <windows.H>









// **************************************************************************************
// * BASIC CLASS and FUNCTIONS USE DEFINE
// **************************************************************************************





// **************************************************************************************
// * BASIC CLASS and FUNCTIONS USE DEFINE
// **************************************************************************************
#ifndef	H263_EXPORTS
// **************************************************************************************
		#define H263API extern "C"	__declspec(dllexport)
		#define H263CLASS			__declspec(dllexport)
// **************************************************************************************
#endif//H263_EXPORTS
// **************************************************************************************


















// **************************************************************************************
// * Purpose	: DECODER DECLARE
// **************************************************************************************
class H263CLASS CH263Decoder
{
public:
	virtual VOID	BlueBirdDec_Initialize(
						BOOL isPreviousComparison = TRUE) = 0;

	virtual VOID	BlueBirdDec_Decode(
						LPBYTE pSrc,				//INF:IN : SOURCE STREAM DATA POINTER 
						LPBYTE pDest,				//INF:IO : DEST   IMAGE  DATA POINTER	(YC420)
						int	   nSrcSize)= 0;		//INF:IN : SOURCE IMAGE  DATA SIZE (BYTE COUNT)

	virtual VOID	BlueBirdDec_Release() = 0;
};

//---------------------------------------------------------------------------------------
class H263CLASS	CH263Encoder
{
public:	
	virtual BOOL	BlueBirdEnc_ResolutionQuery(
						int nWidth,					//INF:IN : SOURCE IMAGE DATA WIDTH  (PIXEL)
						int nHeight) = 0;			//INF:IN : SOURCE IMAGE DATA HEIGHT (PIXEL)	
	virtual BOOL	BlueBirdEnc_Initialize(
						int  width,					//INF:IN : SOURCE IMAGE DATA WIDTH  (PIXEL)
						int  height,				//INF:IN : SOURCE IMAGE DATA HEIGHT (PIXEL)
						int  qp,					//INF:IN : QUANTIZATION VALUE (MIN: 1 ~ XX:MAX)	
						BOOL isPreviosComparison = TRUE) = 0;
	virtual VOID	BlueBirdEnc_SetGOP(
						int nPeriode) = 0;			//INF:IN : GOP Setting (Default : 30)	
	virtual VOID	BlueBirdEnc_Encode(
						LPBYTE org,					//INF:IN : SOURCE IMAGE DATA POINTER 	(YC420)
						LPBYTE dest,				//INF:IO : DEST  STREAM DATA POINTER
						int	&  size,				//INF:IO : DEST  STREAM DATA SIZE
						int &  key,					//INF:IO : DEST  STREAM DATA TYPE (IFRAME:1, PFRAME:0)
						BOOL iFrame = FALSE) = 0;	//INF:IN : GOP NEW START (NOW IFRAME OCCUR)

	virtual VOID	BlueBirdEnc_Release()  = 0;
};


//---------------------------------------------------------------------------------------
H263API	void			CheckLicense();
H263API	CH263Decoder *	CreateBlueBirdDecoder();
H263API CH263Encoder *	CreateBlueBirdEncoder();










// **************************************************************************************
#endif	//#define AFX_H263_Y123012_H__7F8ABB06_20FD_11D4_A6AD_00C026DD15CC__INCLUDED_
// **************************************************************************************



