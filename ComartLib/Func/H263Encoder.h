﻿#ifndef __H263ENCODER_H__
#define __H263ENCODER_H__

#include <windows.h>

DECLARE_HANDLE(HBlueBirdEncoder);

class AFX_EXT_CLASS CBlueBirdEncoder
{
public:
	// 코덱 해상도 지원 여부를 알려주는 함수
	virtual BOOL BlueBirdEnc_ResolutionQuery(/*[in]*/ int nWidth,	// 소스 영상 데이타의 가로사이즈
										/*[in]*/ int nHeight) = 0;	// 소스 영상 데이타의 세로사이즈
	// 코덱 초기화
	virtual BOOL BlueBirdEnc_Initialize(/*[in]*/ int nWidth,			// 소스 영상 데이타의 가로사이즈
								 /*[in]*/ int nHeight,				// 소스 영상 데이타의 세로사이즈
								 /*[in]*/ int qp,					// 압축해서 나올 영상 데이타의 퀄리티 (qp : MIN 1)
								 /*[in]*/ BOOL isPreviosComparison = TRUE) = 0;

	// I-P frame의 간격(GOP)을 정하는 함수
	virtual VOID BlueBirdEnc_SetGOP(/*[in]*/int nPeriode) = 0;		// GOP Setting (Default : 30프레임)

	// 압축
	virtual VOID BlueBirdEnc_Encode(/*[in]*/ BYTE *org,				// YUV 영상 데이타
							 /*[out]*/BYTE *dest,					// 압축되서 나온 데이타
							 /*[out]*/int &size,					// 압축되서 나온 데이타 사이즈
							 /*[out]*/int &key,						// key == 1이면 IFrame, key == 0이면 PFrame
							 /*[in]*/ BOOL iFrame = FALSE) = 0;		// I_FRAME을 얻고 싶을때 TRUE로 Setting

	// 객체 소멸
	virtual VOID BlueBirdEnc_Release() = 0;
};

extern "C" __declspec(dllexport) BOOL CheckLicense();
extern "C" __declspec(dllexport) CBlueBirdEncoder *CreateBlueBirdEncoder();

extern "C" __declspec(dllexport) BOOL BlueBirdEnc_ResolutionQuery(HBlueBirdEncoder hEnc, int nWidth, int nHeight);
extern "C" __declspec(dllexport) BOOL BlueBirdEnc_Initialize(HBlueBirdEncoder hEnc, int nWidth, int nHeight, int qp, BOOL isPreviosComparison = TRUE);
extern "C" __declspec(dllexport) VOID BlueBirdEnc_SetGOP(HBlueBirdEncoder hEnc, int nPeriode);
extern "C" __declspec(dllexport) VOID BlueBirdEnc_Encode(HBlueBirdEncoder hEnc, BYTE *org, BYTE *dest, int &size, int &key, BOOL iFrame = FALSE);
extern "C" __declspec(dllexport) VOID BlueBirdEnc_Release(HBlueBirdEncoder hEnc);
extern "C" __declspec(dllexport) HBlueBirdEncoder BlueBirdEnc_Create();

#endif
