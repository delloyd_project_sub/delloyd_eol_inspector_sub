//*****************************************************************************
// Filename	: 	ColorConvert.h
// Created	:	2017/3/27 - 16:06
// Modified	:	2017/3/27 - 16:06
//
// Author	:	PiRing
//	
// Purpose	:	영상 변환용 클래스
//*****************************************************************************
#ifndef ColorConvert_h__
#define ColorConvert_h__

#pragma once

#include <afxwin.h>

typedef enum  enBayerDemosaicMethod
{
	BAYER_DEMOSAIC_METHOD_NEAREST,
	BAYER_DEMOSAIC_METHOD_SIMPLE,
	BAYER_DEMOSAIC_METHOD_BILINEAR,
	BAYER_DEMOSAIC_METHOD_HQLINEAR,
	BAYER_DEMOSAIC_METHOD_VNG,
	BAYER_DEMOSAIC_METHOD_AHD
};

// enum enStorageType
// {
// 	ST_invalid = -1,	///< invalid not set image storage type
// 	ST_unsignedchar,	///< (8bit) unsigned char image storage type
// 	ST_char,			///< (8bit) signed char image storage type
// 	ST_unsignedshortint,///< (16bit) unsigned integer image storage type
// 	ST_shortint,		///< (16bit) signed integer image storage type
// 	ST_unsignedint,		///< (32bit) unsigned integer image storage type
// 	ST_int,				///< (32bit) signed integer image storage type
// 	ST_float,			///< float image storage type
// 	ST_double
// 	///< double image storage type
// };


// brief These are the most often used color models.
// Determines the data storage size per channel per pixel.
enum enColorModel
{
	CM_invalid = -1,		///< invalid (not set) image format
	CM_Grey,				///< gray values, 1 channel
	CM_RGB,					///< color values, 3 channels, order: red,green,blue
	CM_BGR,					///< color values, 3 channels, order: blue,green,red
	CM_YUYV422,				///< YUYV422, 2 channels, full luminance Y, subsampled half U,V
	CM_UYVY422,				///< UYVY422, 2 channels, full luminance Y, subsampled half U,V inverse order
	CM_YUV420P,				///< YUV420P, 2 channels, full luminance Y, 1 U, 1 V. Y, U and V are grouped together for better compression
	CM_YUV444,				///< CM_YUV444, 3 channels, all channels have full data
	CM_YUV411,				///< YUV411, 2 channles, full luminance, 1 U, 1 V.
	CM_HSV,					///< HSV, 3 channels, order: hue, sat , value
	CM_HSI_OBS,				///< obsolete, HSI is unused and identical to HSL
	CM_DV,					///< DV, color model used for digital video cameras such as Mini-DV
	CM_RGBA,				///< RGBA, 4 channels, order: red,green,blue,alpha
	CM_GreyA,				///< GreyA, 2 channels, grey plus Alpha
	CM_Bayer_RGGB,			///< Bayer_RGGB, 1 channel RGB image Bayer tile
	CM_Bayer_GBRG,			///< Bayer_GBRG, 1 channel RGB image Bayer tile
	CM_Bayer_GRBG,			///< Bayer_GRBG, 1 channel RGB image Bayer tile
	CM_Bayer_BGGR,			///< Bayer_BGGR, 1 channel RGB image Bayer tile
	CM_HSL,					///< HSL, similar to HSV but space is a double tipped cone
	CM_hsL,					///< hsl, similar to HSL but euclidean (h,s) for CNCC
	//CM_SymTensor2x2,		///< SymTensor2x2 The image contains a 2x2 symmetric tensor
	CM_BGRA,				///< BGRA color values, 4 channels, order: blue,green,red,alpha
	CM_RGBE,				///< RGBE color values, 4 channels, RADIANCE hdr format, four low dynamic channels meaning: 3x mantissa (red,green,blue) +1x (exponent)
	//CM_PGR_XB3_F7M3_GBRG,	///< PGR XB3 in format 7 mode 3 delivers an image that consists of 3 channels with 8bbp (overal 24bpp), each channel codes an whole color image in GBRG bayer pattern, ch0 = left image, ch1 = right image, ch3 = center image
	//CM_DepthAndVariance,	///< Todo: Unclear, I think one channel float, why isn't grey used for that?
	CM_YUYV,				///< Todo: Conflict with YUVU model, what does it do?
	CM_LUV,					///< CIELUV color space, 3 channels, http://en.wikipedia.org/wiki/CIELUV_color_space
	CM_XYZ,					///< XYZ, 3 channels, http://en.wikipedia.org/wiki/Xyz_color_space
	CM_LAB,					///< LAB, 3 channels, http://en.wikipedia.org/wiki/Lab_color_space
	//CM_Disparity,			///< Disparity images Q: should disp and depth be treated separately, if not what would be a good name to cover both?
	//CM_Depth,				///< Depth images A: separated for now.
	//CM_I1I2I3,				///< Othe's principle component generalization for RGB based segmentation
	CM_DOES_NOT_EXIST
	//< end of list marker for loops. >= is invalid
};

enum enColorModelOut
{
	CMOUT_RGB	= CM_RGB,
	CMOUT_RGBA	= CM_RGBA,
};

enum enBitsPerPixel
{
	BPP_1_bit,
	BPP_4_bit,
	BPP_8_bit,
	BPP_9_bit,
	BPP_9_5_bit,
	BPP_10_bit,
	BPP_12_bit,
	BPP_16_bit,
	BPP_24_bit,
	BPP_32_bit,
};

enum enBitsMask
{
	BM_1_bit		= 0x00000001,
	BM_4_bit		= 0x0000000F,
	BM_8_bit		= 0x000000FF,
	BM_9_bit		= 0x000001FF,
	BM_9_5_bit		= 0x000003FF,
	BM_10_bit		= 0x000003FF,
	BM_12_bit		= 0x00000FFF,
	BM_16_bit		= 0x0000FFFF,
	BM_24_bit		= 0x00FFFFFF,
	BM_32_bit		= 0xFFFFFFFF,
};


/**
Sony DFW-X700 :
r = y + 1.4022 v got
b = y + 1.7710 u
g = y - 0.3475 u - 0.7144 v got
*/
#define YUV2RGB(y, u, v, r, g, b)\
  r = y + ( ((v << 11) + (v << 10) - (v << 8) + (v << 6) - (v << 3) ) >> 11);\
  g = y - ( ((v<<10) + (v<<9) - (v<<6) -  (v<<3) ) >> 11 ) - \
  ( ((u << 9) + (u << 7) + (u << 6) + (u << 3) + u) >> 11 );\
  b = y + ( ((u << 12) - (u << 9) + (u << 5)+(u<<4)) >> 11);\
  r = r < 0 ? 0 : r;\
  g = g < 0 ? 0 : g;\
  b = b < 0 ? 0 : b;\
  r = r > 255 ? 255 : r;\
  g = g > 255 ? 255 : g;\
  b = b > 255 ? 255 : b


//-----------------------------------------------------------------------------
// CColorConvert
//-----------------------------------------------------------------------------
class CColorConvert
{
public:
	CColorConvert();
	~CColorConvert();

	static void	ClearBorders				(__inout LPBYTE pbyRGB, __in int nXRes, __in int nYRes, __in int nWidth);

	static BOOL	FlipVertical				(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static BOOL	FlipVertical				(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight);
	static BOOL	FlipHorizontal				(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static BOOL	FlipHorizontal				(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight);

	static void	YUV420pToRGB32				(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static void	YUV420pToRGB24				(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static void	YUV422ToRGB32				(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in BOOL bUYVY422 = TRUE);
	static void	YUV422ToRGB24				(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in BOOL bUYVY422 = TRUE);
	static void	RGB24ToRGB32				(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static void	RGB32ToRGB24				(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);

	static void	BayerToRGBSimple			(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enColorModel nColorModel);
	static void	BayerToRGBBilinear			(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enColorModel nColorModel);
	static void	BayerToRGBNearestNeighbour	(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enColorModel nColorModel);
	static void	BayerToRGBHQLinear			(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enColorModel nColorModel);	

	static int	BayerToRGB24				(__in enColorModel nColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static int	BayerToRGB32				(__in enColorModel nColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static void	BayerToRGB					(__in enColorModel nColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enBayerDemosaicMethod nMethod, __in BOOL bFlip);

	static void	Grey16ToRGB24				(__in const LPWORD pbySrc, __in UINT nPixelCount, __out LPBYTE pbyDst);
	static void	Grey16ToRGB32				(__in const LPWORD pbySrc, __in UINT nPixelCount, __out LPBYTE pbyDst);
	static void Grey16ToRGB24_CV			(__in const LPWORD pbySrc, __in UINT nHeight, __in UINT nWidth, __out LPBYTE pbyDst);
	static void Grey16ToRGB32_CV			(__in const LPWORD pbySrc, __in UINT nHeight, __in UINT nWidth, __out LPBYTE pbyDst);

	void		ConvertToRGB32				(__in enColorModel nSrcColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	void		ConvertToRGB32				(__in enColorModel nSrcColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPRGBQUAD pbyDst);
	void		ConvertToRGB24				(__in enColorModel nSrcColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	void		ConvertToRGB24				(__in enColorModel nSrcColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPRGBTRIPLE pbyDst);
};

#endif // ColorConvert_h__

