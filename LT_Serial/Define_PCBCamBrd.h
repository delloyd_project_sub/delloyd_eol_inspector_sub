﻿//*****************************************************************************
// Filename	: 	Define_CamBrd.h
// Created	:	2016/3/8 - 13:31
// Modified	:	2016/3/8 - 13:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Define_CamBrd_h__
#define Define_CamBrd_h__

//=============================================================================
// LGIT - Camera Board 통신 프로토콜
//=============================================================================
/*
Transmit from host
<STX><CMD><DATA><ETX>

Response from host
<STX><CMD><DATA><ETX>

Where:
<STX>		= 1 ASCII '!'
<CMD>		= 1 ASCII character
<DATA>		= Command Argument, up to 9 ASCII characters.
<ETX>		= 1 ASCII '@'
*/
namespace LGIT_CamBrd
{

#define	CamBrd_STX				0x21	//'!'
#define	CamBrd_ETX				0x40	//'@'
#define CamBrd_DummyChar		0x30	//'0'
#define CamBrd_ProtoLength		11
#define CamBrd_DataLength		8
#define CamBrd_CH_DataLength	5 //4
#define CamBrd_Channel			2

	// 고정된 통신 프로토콜 선언
	static const char*	g_szCamBrd_BoardCheck		= "!000000000@";
	static const char*	g_szCamBrd_GetInCurrent		= "!400000000@";
	static const char*	g_szCamBrd_GetInSound		= "!900000000@";
	static const char*	g_szCamBrd_SetIRLED_On		= "!L10000000@";
	static const char*	g_szCamBrd_SetIRLED_Off		= "!L00000000@";
	static const char*	g_szCamBrd_StartBtn			= "!B10000000@";
	static const char*	g_szCamBrd_StopBtn			= "!B01000000@";
	static const char*	g_szCamBrd_OpenShort			= "!Y00000000@";

	// 프로토콜 커맨드 
	typedef enum enCamBrdCmd
	{
		CMD_BoardCheck		= '0',
		CMD_SetVoltage		= 'V',
		CMD_GetInCurrent	= '4',
		CMD_GetInSound		= '9',
		CMD_OverCurrent		= 'D',
		CMD_IRLED			= 'L',
		CMD_Button			= 'B',
		CMD_OpenShort	= 'Y',
	};

	typedef struct _tag_CamBrdCurrent
	{
		long lOutCurrent;

		_tag_CamBrdCurrent()
		{
			lOutCurrent = FALSE;
		}

	}ST_CamBrdCurrent, *PST_CamBrdCurrent;

	typedef struct _tag_CamBrdProtocol
	{
		char		STX;
		char		CMD;
		char		Data[CamBrd_DataLength];
		//char		ChData[CamBrd_Channel][CamBrd_CH_DataLength + 1];
		char		ETX;

		_tag_CamBrdProtocol()
		{
			STX = CamBrd_STX;
			CMD = CamBrd_DummyChar;
			ETX = CamBrd_ETX;
			memset(Data, CamBrd_DummyChar, CamBrd_DataLength);
			//memset(ChData, CamBrd_DummyChar, CamBrd_CH_DataLength);
		};

		void MakeProtocol_SetVolt(__in float* fVolt)
		{
			CMD = CMD_SetVoltage;
			memset(Data, CamBrd_DummyChar, CamBrd_DataLength);

			CStringA szBuf;
			UINT nOffset = 0;
			for (int iCh = 0; iCh < CamBrd_Channel; iCh++)
			{
				szBuf.Format("%03.0f", fVolt[iCh] * 10.0f);

				memcpy(&Data[nOffset], szBuf.GetBuffer(0), 3);

				nOffset += 3;
			}
		};

	}ST_CamBrdProtocol, *PST_CamBrdProtocol;

	typedef struct _tag_CamBrdRecvProtocol
	{
		char		STX;
		char		CMD;
		CStringA	Data;
		char		ETX;

		CStringA	Protocol;

		int			iCurrent[CamBrd_Channel];
		int			iSoundVoltage;
		//int			iOpenShort;
		BOOL		bOpenShort;
		BOOL		bOverCurrent;
		BOOL		bStartBtn;
		BOOL		bStopBtn;
		BOOL		bResult;

		_tag_CamBrdRecvProtocol()
		{
			for (int i = 0; i < CamBrd_Channel; i++)
				iCurrent[i] = 0;

			bOverCurrent = FALSE;
			bStartBtn = FALSE;
			bStopBtn = FALSE;
			bOpenShort = FALSE;
			iSoundVoltage = 0;
			//iOpenShort = 0;
		};

		_tag_CamBrdRecvProtocol& operator= (_tag_CamBrdRecvProtocol& ref)
		{
			STX = ref.STX;
			CMD = ref.CMD;
			Data = ref.Data;
			ETX = ref.ETX;

			Protocol = ref.Protocol;

			return *this;
		};

		BOOL SetRecvProtocol(__in const char* pszProtocol, __in UINT_PTR nLength)
		{
			if (NULL == pszProtocol)
				return FALSE;

			if (nLength != CamBrd_ProtoLength)
				return FALSE;

			CStringA strProtocol = pszProtocol;

			INT_PTR nDataLength = nLength - 3; // STX, ETX, CMD
			INT_PTR nOffset = 0;

			STX  = pszProtocol[nOffset++];
			CMD  = pszProtocol[nOffset++];
			Data = strProtocol.Mid((int)nOffset++, (int)nDataLength);
			ETX  = pszProtocol[nOffset];

			switch (CMD)
			{
			case CMD_BoardCheck:
				AnalyzeData_BoardCheck();
				break;

			case CMD_SetVoltage:
				AnalyzeData_SetVolt();
				break;

			case CMD_GetInCurrent:
				AnalyzeData_Current();
				break;

			case CMD_GetInSound:
				AnalyzeData_Sound();
				break;

			case CMD_OverCurrent:
				AnalyzeData_OverCurrent();
				break;

			case CMD_IRLED:
				AnalyzeData_SetIRLED();
				break;

			case CMD_Button:
				AnalyzeData_Button();
				break;
				
			case CMD_OpenShort:
				AnalyzeData_OpenShort();
				break;
	
			default:
				break;
			}	

			// Check STX
			if (CamBrd_STX != STX)
				return FALSE;

			// Check ETX
			if (CamBrd_ETX != ETX)
				return FALSE;

			return TRUE;
		};

		void AnalyzeData_BoardCheck()
		{
			Data.ReleaseBuffer();
		};

		void AnalyzeData_SetVolt()
		{
			Data.ReleaseBuffer();
		};

		void AnalyzeData_SetIRLED()
		{
			Data.ReleaseBuffer();
		};

		void AnalyzeData_Button()
		{
			Data.ReleaseBuffer();
		};


		void AnalyzeData_Current()
		{
			CStringA szBuff;

			for (int i = 0; i < CamBrd_Channel; i++)
			{
				szBuff = Data.Mid(i * CamBrd_CH_DataLength, 5);
				iCurrent[i] = atoi(szBuff);
			}
		};

		void AnalyzeData_Sound()
		{
			CStringA szBuff;

			szBuff = Data.Mid(0, 4);
			iSoundVoltage = atoi(szBuff);
		};

		void AnalyzeData_OverCurrent()
		{
			CStringA szBuff;

			szBuff = Data.Left(1);

			if (1 == atoi(szBuff))
				bOverCurrent = TRUE;
			else
				bOverCurrent = FALSE;

			Data.ReleaseBuffer();
		};
		void AnalyzeData_OpenShort()
		{
			CStringA szBuff;

			szBuff = Data.Left(1);

			if (1 == atoi(szBuff))
				bOpenShort = TRUE;
			else
				bOpenShort = FALSE;

			Data.ReleaseBuffer();
		};

	}ST_CamBrdRecvProtocol, *PST_CamBrdRecvProtocol;

};

#endif // Define_CamBrd_h__
