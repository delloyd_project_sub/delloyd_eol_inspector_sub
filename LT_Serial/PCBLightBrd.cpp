﻿//*****************************************************************************
// Filename	: 	PCBLightBrd.cpp
// Created	:	2016/05/09
// Modified	:	2016/05/09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "StdAfx.h"
#include "PCBLightBrd.h"
#include "ErrorCode.h"

using namespace PR_ErrorCode;

//=============================================================================
// 생성자
//=============================================================================
CPCBLightBrd::CPCBLightBrd()
{
	SetDelimeter(LightBrd_STX, LightBrd_ETX, LightBrd_ProtoLength, LightBrd_ProtoLength);

	m_fMinVoltage = 0.0f;
	m_fMaxVoltage = 12.0f;

	m_fUnitVolt = (m_fMaxVoltage - m_fMinVoltage) / 1024.0f;
}

//=============================================================================
// 소멸자
//=============================================================================
CPCBLightBrd::~CPCBLightBrd()
{
	//TRACE(_T("<<< Start ~CPCBLightBrd >>> \n"));



	//TRACE(_T("<<< End ~CPCBLightBrd >>> \n"));
}


//=============================================================================
// Method		: CPCBLightBrd::OnFilterRecvData
// Access		: protected 
// Returns		: LRESULT
//					 1	-> ACK 처리 성공
//					 0	-> 데이터가 중간에 짤렸음 다음 데이터 수신시 처리
//					-1	-> 쓰레기 데이터 처리
//					-2	-> 매개변수 오류
// Parameter	: const char * szACK	-> 수신된 ACK 데이터
// Parameter	: DWORD dwAckSize		-> 수신된 ACK 데이터 크기
// Qualifier	:
// Last Update	: 2015/11/28 - 16:35
// Desc.		: 
//=============================================================================
LRESULT CPCBLightBrd::OnFilterRecvData(const char* szACK, DWORD dwAckSize)
{
	//return CSerialCom_Base::OnFilterRecvData(szACK, dwAckSize);

	if (NULL == szACK)
	{
		ASSERT(_T("NULL == szACK"));
		m_dwErrCode = ERROR_INVALID_PARAMETER;
		TRACE(_T("FilterAckData() : %d\n"), __LINE__);
		return FALSE;
	}

	ASSERT(0 != dwAckSize);

	DWORD_PTR	dwQueueSize = 0;
	DWORD		dwDataLength = 0;

	INT_PTR		iFindSTXIndex = 0;
	INT_PTR		iFindETXIndex = 0;

	//ResetAckBuffer();
	m_dwACKBufSize = 0;

	//-----------------------------------------------------
	// Data가 들어오면 우선 큐에 집어 넣는다?
	//-----------------------------------------------------
	m_SerQueue.Push(szACK, dwAckSize);

	//-----------------------------------------------------
	// STX를 찾는다.
	//-----------------------------------------------------
	if (0 <= (iFindSTXIndex = m_SerQueue.EraseUntilFindDelimiter(m_chSTX)))
	{
		//-----------------------------------------------------
		// ETX를 찾는다.
		//-----------------------------------------------------
		dwQueueSize = m_SerQueue.GetSize();

		// 프로토콜 사이즈보다 작으면 데이터가 들어오길 기다린다.
		if (dwQueueSize < (DWORD)m_nMinProtocolLength)
		{
			TRACE(_T("FilterAckData() : %d\n"), __LINE__);
			return FALSE;
		}

		iFindETXIndex = 0;
		while (0 < (iFindETXIndex = m_SerQueue.FindDelimiter(m_chETX, iFindETXIndex)))
		{
			// ETX 위치가 프로토콜 마지막
			// 			if (iFindETXIndex < (INT_PTR)(m_nMaxProtocolLength - 1))
			// 			{
			// 				++iFindETXIndex; // ETX 찾은 위치 다음부터 찾는다.(무한 루프 방지)
			// 				continue;
			// 			}
			// 			else
			//			{
			dwDataLength = (DWORD)iFindETXIndex + 1;
			m_SerQueue.PopData(m_szACKBuf, (DWORD)iFindETXIndex + 1);
			break;
			//			}
		}

		if (iFindETXIndex <= 0) // STX를 찾고 ETX를 못찾으면 데이터가 중간에 나뉘어 들어온걸로 판단함
		{
			// STX ~> ETX 까지 검색한 데이터 사이즈가 프로토콜 사이즈보다 넘어가면 다음 데이터가 들어오면 처리
			if (dwQueueSize < (DWORD)m_nMaxProtocolLength)
			{
				//TRACE(_T("FilterAckData() : %d\n"), __LINE__);
				return FALSE;
			}
			else // 아니면 에러 처리
			{
				m_SerQueue.Empty();
				TRACE(_T("FilterAckData() : %d\n"), __LINE__);
				return FALSE;
			}
		}
	}
	else // STX가 없으면 쓰레기 데이터로 간주하고 처리	
	{
		// 오류 : 쓰레기 데이터
		m_SerQueue.Empty();
		TRACE(_T("FilterAckData() : %d\n"), __LINE__);
		return FALSE;
	}

	m_dwACKBufSize = dwDataLength;
	m_szACKBuf[m_dwACKBufSize] = 0x00;
	OnRecvProtocol(m_szACKBuf, m_dwACKBufSize);

	// 이벤트 핸들이 설정되어있다면 이벤트를 발생시킨다.
	if (NULL != m_hEvent_ACK)
		SetEvent(m_hEvent_ACK);

	//TRACE(_T("FilterAckData : Complete Ack Protocol\n"));
// 	if (NULL != m_hOwnerWnd)
// 	{
// 		::SendNotifyMessage(m_hOwnerWnd, m_WM_Ack, (WPARAM)m_szACKBuf, (LPARAM)m_dwACKBufSize);
// 	}

	//-----------------------------------------------------
	// 큐 비우기
	//-----------------------------------------------------
	m_SerQueue.Empty();
	m_dwErrCode = NO_ERROR;

	return TRUE;
}

//=============================================================================
// Method		: OnRecvProtocol
// Access		: virtual protected  
// Returns		: void
// Parameter	: const char * szACK
// Parameter	: DWORD dwAckSize
// Qualifier	:
// Last Update	: 2016/5/9 - 23:28
// Desc.		:
//=============================================================================
void CPCBLightBrd::OnRecvProtocol(const char* szACK, DWORD dwAckSize)
{
	m_stRecvProtocol.SetRecvProtocol(szACK, dwAckSize);
}

//=============================================================================
// Method		: Send_PortCheck
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byPortNo
// Qualifier	:
// Last Update	: 2016/5/9 - 23:28
// Desc.		:
//=============================================================================
BOOL CPCBLightBrd::Send_BoardCheck(__out BYTE& byPortNo)
{
	TRACE(_T("CPCBLightBrd::Send_PortCheck \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;
	if (TransferStart(g_szLightBrd_PortCheck, LightBrd_ProtoLength))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			// Power Controller Number
			byPortNo = m_stRecvProtocol.Data[0];

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("CMD : 광원 보드 Port Check ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("CMD : 광원 보드 Port Check ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Send_OutputVolt
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nSlotNo
// Parameter	: __in float fVolt
// Qualifier	:
// Last Update	: 2017/6/29 - 15:25
// Desc.		:
//=============================================================================
BOOL CPCBLightBrd::Send_OutputVolt(__in UINT nSlotNo, __in float fVolt)
{
	TRACE(_T("CPCBLightBrd::Send_OutputVolt \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	m_stProtocol.MakeProtocol_OutputVolt(nSlotNo, fVolt);

	if (TransferStart((char*)&m_stProtocol, LightBrd_ProtoLength))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			m_stRecvProtocol.Data[0]; // 슬롯 넘버

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("CMD : 광원 보드 Output Volt ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("CMD : 광원 보드 Output Volt ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Send_DetectVolt
// Access		: public  
// Returns		: BOOL
// Parameter	: __out ST_SlotVolt & stOutSlotVolt
// Qualifier	:
// Last Update	: 2017/6/29 - 16:00
// Desc.		:
//=============================================================================
BOOL CPCBLightBrd::Send_DetectVolt(__out ST_SlotVolt& stOutSlotVolt)
{
	TRACE(_T("CPCBLightBrd::Send_DetectVolt \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	if (TransferStart(g_szLightBrd_DetectVolt, LightBrd_ProtoLength))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			memcpy(&stOutSlotVolt, &m_stRecvProtocol.RecvedSlotVolt, sizeof(ST_SlotVolt));

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("CMD : 광원 보드 Detect Volt All ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("CMD : 광원 보드 Detect Volt All ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Send_OutputCurrent
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nSlotNo
// Parameter	: __in float fCurrent
// Qualifier	:
// Last Update	: 2017/6/29 - 17:03
// Desc.		:
//=============================================================================
BOOL CPCBLightBrd::Send_OutputCurrent(__in UINT nSlotNo, __in float fCurrent)
{
	TRACE(_T("CPCBLightBrd::Send_OutputCurrent \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	m_stProtocol.MakeProtocol_OutputCurrent(nSlotNo, fCurrent);

	if (TransferStart((char*)&m_stProtocol, LightBrd_ProtoLength))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			m_stRecvProtocol.Data[0]; // 슬롯 넘버

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("CMD : 광원 보드 Output Current ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("CMD : 광원 보드 Output Current ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Send_AmbientLightCurrent
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nSlotNo
// Parameter	: __in WORD wValue
// Qualifier	:
// Last Update	: 2017/6/29 - 17:04
// Desc.		:
//=============================================================================
BOOL CPCBLightBrd::Send_AmbientLightCurrent(__in UINT nSlotNo, __in WORD wValue)
{
	if (wValue > 1023)
		wValue = 1023;

	float fCurrent = (LightBrd_MinCurrent + (LightBrd_MaxCurrent / 1024.0f * wValue));

	return Send_OutputCurrent(nSlotNo, fCurrent);
}
