﻿//*****************************************************************************
// Filename	: 	IF_Illumination.cpp
// Created	:	2016/05/09
// Modified	:	2016/05/09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "StdAfx.h"
#include "IF_Illumination.h"
#include "ErrorCode.h"

using namespace PR_ErrorCode;

//=============================================================================
// 생성자
//=============================================================================
CIF_Illumination::CIF_Illumination()
{
	SetDelimeter(IF_IlluminationBrd_STX, IF_IlluminationBrd_ETX, IF_IlluminationBrd_RecvLength, IF_IlluminationBrd_ProtoLength);
}

//=============================================================================
// 소멸자
//=============================================================================
CIF_Illumination::~CIF_Illumination()
{
	TRACE(_T("<<< Start ~CIF_Illumination >>> \n"));



	TRACE(_T("<<< End ~CIF_Illumination >>> \n"));
}

//=============================================================================
// Method		: CIF_Illumination::OnFilterRecvData
// Access		: protected 
// Returns		: LRESULT
//					 1	-> ACK 처리 성공
//					 0	-> 데이터가 중간에 짤렸음 다음 데이터 수신시 처리
//					-1	-> 쓰레기 데이터 처리
//					-2	-> 매개변수 오류
// Parameter	: const char * szACK	-> 수신된 ACK 데이터
// Parameter	: DWORD dwAckSize		-> 수신된 ACK 데이터 크기
// Qualifier	:
// Last Update	: 2015/11/28 - 16:35
// Desc.		: 
//=============================================================================
LRESULT CIF_Illumination::OnFilterRecvData(const char* szACK, DWORD dwAckSize)
{
	return CSerialCom_Base::OnFilterRecvData(szACK, dwAckSize);
}

//=============================================================================
// Method		: OnRecvProtocol
// Access		: virtual protected  
// Returns		: void
// Parameter	: const char * szACK
// Parameter	: DWORD dwAckSize
// Qualifier	:
// Last Update	: 2016/5/9 - 23:27
// Desc.		:
//=============================================================================
void CIF_Illumination::OnRecvProtocol(const char* szACK, DWORD dwAckSize)
{
	m_stRecvProtocol.SetRecvProtocol(szACK, dwAckSize);
}

void CIF_Illumination::ResetProtocol()
{

}

BOOL CIF_Illumination::Send_SetBrightness(__in UINT nCH, __in UINT nBrightness)
{
	TRACE(_T("CIF_Illumination::Send_SetBrightness \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	if (nCH == 4)
	{
		if (nBrightness > 30)
		{
			nBrightness = 30;
		}
	}

	m_stProtocol.MakeProtocol_SetBrightness(nCH, nBrightness);

	if (TransferStart((char*)&m_stProtocol, IF_IlluminationBrd_ProtoLength))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			bReturn = m_stRecvProtocol.bPass;
			m_dwErrCode = NO_ERROR;
			TRACE(_T("CMD : 광원보드 Send_SetBrightness ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("CMD : 광원보드 Send_SetBrightness ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}


BOOL CIF_Illumination::Send_SetOnOff(__in UINT nCH, __in BOOL bMode)
{
	TRACE(_T("CIF_Illumination::Send_SetOnOff \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;


	m_stProtocol.MakeProtocol_SetOnOff(nCH, bMode);

	if (TransferStart((char*)&m_stProtocol, IF_IlluminationBrd_ProtoLength))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			bReturn = m_stRecvProtocol.bPass;
			m_dwErrCode = NO_ERROR;
			TRACE(_T("CMD : 광원보드 Send_SetOnOff ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("CMD : 광원보드 Send_SetOnOff ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

BOOL CIF_Illumination::Send_GetBrightness(__in UINT nCH, __out UINT& nOutBrightness)
{
	TRACE(_T("CPCBCamBrd::Send_GetBrightness \n"));

	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;
	m_stProtocol.MakeProtocol_GetBrightness(nCH);
	
	if (TransferStart((char*)&m_stProtocol, IF_IlluminationBrd_ProtoLength))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
	
			if (m_stRecvProtocol.nCH == nCH + 1)
			{
				nOutBrightness = m_stRecvProtocol.nBrightness;

				m_dwErrCode = NO_ERROR;
				bReturn = TRUE;
				TRACE(_T("CMD : 광원보드 Send_GetBrightness ACK OK\n"));
			}
			else{
				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("CMD : 광원보드 Send_GetBrightness ACK ERROR_  채널 번호 다름\n"));
			}
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("CMD : 광원보드 Send_GetBrightness ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

BOOL CIF_Illumination::Send_GetOnOff(__in UINT nCH, __out BOOL& bOutOnOff)
{
	TRACE(_T("CPCBCamBrd::Send_GetOnOff \n"));

	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;
	m_stProtocol.MakeProtocol_GetOnOff(nCH);

	if (TransferStart((char*)&m_stProtocol, IF_IlluminationBrd_ProtoLength))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			// Board Number
			if (m_stRecvProtocol.nCH == nCH)
			{
				bOutOnOff = m_stRecvProtocol.bOnOffMode;

				m_dwErrCode = NO_ERROR;
				bReturn = TRUE;
				TRACE(_T("CMD : 광원보드 Send_GetOnOff ACK OK\n"));
			}
			else{
				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("CMD : 광원보드 Send_GetOnOff ACK ERROR_  채널 번호 다름\n"));
			}
			

		
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("CMD : 광원보드 Send_GetOnOff ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}