﻿#ifndef List_Work_Angle_h__
#define List_Work_Angle_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Ang_Worklist
{
	Ang_W_Recode,
	Ang_W_Time,
	Ang_W_Equipment,
	Ang_W_Model,
	Ang_W_SWVersion,
	Ang_W_LOTNum,
	Ang_W_Barcode,
	//Ang_W_Operator,
	Ang_W_Result,
	Ang_W_LeftC,
	Ang_W_RightC,
	Ang_W_TopC,
	Ang_W_BottomC,
	Ang_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_Ang_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	_T("Left ↔ Center"),
	_T("Right ↔ Center"),
	_T("Top ↔ Center"),
	_T("Bottom ↔ Center"),
	NULL
};

const int	iListAglin_Ang_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Ang_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_Angle

class CList_Work_Angle : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_Angle)

public:
	CList_Work_Angle();
	virtual ~CList_Work_Angle();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_Angle_h__
