﻿//*****************************************************************************
// Filename	: Wnd_WorklistFFT.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistFFT_h__
#define Wnd_WorklistFFT_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_FFT.h"

//=============================================================================
// Wnd_WorklistFFT
//=============================================================================
class CWnd_WorklistFFT : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistFFT)

public:
	CWnd_WorklistFFT();
	virtual ~CWnd_WorklistFFT();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

//	CList_Work_FFT			m_list_FFT[TICnt_FFT];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistFFT_h__


