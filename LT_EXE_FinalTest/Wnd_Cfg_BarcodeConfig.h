//*****************************************************************************
// Filename	: 	Wnd_Cfg_BarcodeConfig.h
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_BarcodeConfig_h__
#define Wnd_Cfg_BarcodeConfig_h__

#pragma once

#include <afxwin.h>
#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_Enum_Cm.h"
#include "Def_TestItem.h"
#include "List_BarcodeConfig.h"
#include "Def_TestDevice.h"
#include "File_WatchList.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_BarcodeConfig
//-----------------------------------------------------------------------------
class CWnd_Cfg_BarcodeConfig : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_BarcodeConfig)

public:
	CWnd_Cfg_BarcodeConfig();
	virtual ~CWnd_Cfg_BarcodeConfig();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnBarcodeItem	(UINT nID);
	afx_msg void	OnBnClickedBnBarcodelistCtrl(UINT nID);
	afx_msg void	OnBnClickedSeqCheck			();
	afx_msg void	OnSelChangeCbTestItem		();

	CFont			m_font;

	enum enBarcodeItem
	{
		BI_RefBarcode,			// 표준 바코드 입력
		BI_Model,				// 모델 등록
		BI_MaxEnum,
	};

	enum enBarcodeListCtrl
	{
		BCC_Add,
		BCC_Insert,
		BCC_Remove,
		BCC_Order_Up,
		BCC_Order_Down,
		BCC_MaxEnum,
	};

 	// 스텝 목록
	CList_BarcodeConfig	m_lc_RefBarcodeList;

	// 스텝 항목 추가/삭제/이동
	CMFCButton		m_bn_ListCtrl[BCC_MaxEnum];
	
	// 스텝 설정 항목
	CMFCButton		m_chk_BarcodeItem[BI_MaxEnum];
	CMFCMaskedEdit	m_ed_BarcodeItem[BI_MaxEnum];
	
	// 개별 검사 항목
	CComboBox		m_cb_ModelItem;

	CVGStatic		m_st_Name;
	CVGStatic		m_st_TestName;

	// UI에 설정된 스텝 데이터 구하기
	BOOL	GetBarcodeInfoData			(__out ST_BarcodeRef& stRefBarcode);

	// 버튼 제어 함수
	void	Item_Add();
	void	Item_Insert();
	void	Item_Remove();
	void	Item_Up();
	void	Item_Down();
	void	Item_Enable(enBarcodeItem enBarcode);

	ST_Device*	m_pstDevice;
	CString m_szModelPath;
	CString m_szModel;
	CString m_szRefBarcode;

	CFile_WatchList	m_IniWatch;
	void RefreshFileList(__in const CStringList* pFileList);

public:

	void SetReferenceSeperator(__in CString szRefBarcode)
	{
		m_szRefBarcode = szRefBarcode;
	};

	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	// 파일이 있는 경로 설정
	void SetPath(__in LPCTSTR szModelPath)
	{
		m_szModelPath = szModelPath;
	};

	void SetModel(__in LPCTSTR szModel)
	{
		m_szModel = szModel;
	};

	// 검사기 종류 설정
	void		SetSystemType		(__in enInsptrSysType nSysType);

	// 저장된 Step Info 데이터 불러오기
	void		Set_BarcodeInfo		(__in const ST_BarcodeRefInfo* pstBarcodeInfo);
	void		Get_BarcodeInfo		(__out ST_BarcodeRefInfo& stBarcodeInfo);

	// 초기 상태로 설정 (New)
	void		Init_DefaultSet		();

};

#endif // Wnd_Cfg_BarcodeConfig_h__


