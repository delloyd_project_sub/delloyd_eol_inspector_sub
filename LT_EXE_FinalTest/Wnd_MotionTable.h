﻿//*****************************************************************************
// Filename	: 	Wnd_MotionTable.h
// Created	:	2017/03/28 - 13:47
// Modified	:	2017/03/28 - 13:47
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************

#pragma once
#include "VGStatic.h"
#include "MotionManager.h"
#include "Def_Motion.h"

#define  MAX_AIXS		 100

#define	 MT_WIDTH_OFFSET 1.0
#define	 HEIGHT_OFFSET	 1.0

enum enMotionStatic
{
	ST_MT_AXIS_NAME = 0,
	ST_MT_POWER_NAME,
	ST_MT_POS_NAME,
	ST_MT_ORI_NAME,
	ST_MT_MOTION_NAME,
	ST_MT_HLIT_NAME,
	ST_MT_HOME_NAME,
	ST_MT_LLIT_NAME,
	ST_MT_ALRAM_NAME,
	ST_MT_MAXNUM,
};

static LPCTSTR g_szMotionStatusName[] =
{
	_T("AXIS NAME"),
	_T("POWER"),
	_T("POSITION"),
	_T("ORIGIN"),
	_T("IN MOTION"),
	_T("+ LIMIT"),
	_T("HOME"),
	_T("- LIMIT"),
	_T("ALRAM"),
	NULL
};

// CWnd_MotionTable
class CWnd_MotionTable : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MotionTable)

public:
	CWnd_MotionTable();
	virtual ~CWnd_MotionTable();

	CMotionManager*	m_pstDevice;
	void SetPtr_Device(__in CMotionManager* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	enInsptrSysType m_nSysType;
	void SetSystemType (__in enInsptrSysType nSysType)
	{
		m_nSysType = nSysType;
		SetUpdateAxisCnt();
	};

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnDestroy				();
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnAxisSelect			(UINT nID);
	afx_msg void	OnAxisAmpCtr			(UINT nID);
	afx_msg void	OnAxisAlramCtr			(UINT nID);

	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	void	SetUpdataAxisName			();
	void	SetUpdateDataReset			(UINT nAxis);
	void	SetDeleteTimer				();

	DECLARE_MESSAGE_MAP()

protected:

	UINT			m_nAxisNum;
	UINT			m_nAxisMax;

	//	타이머
	HANDLE			m_hTimerQueue;
	HANDLE			m_hTimer_SensorCheck;
	
	CButton			m_gb_Item;
	CVGStatic		m_st_Name;

	CVGStatic		m_st_Axis[MAX_AIXS];
	CVGStatic		m_st_Sen[MAX_AIXS][ST_MT_MAXNUM];

	// 센서 모니터링용 쓰레드
	HANDLE			m_hExitEvent		= NULL;	// 외부의 쓰레드 종료 이벤트
	HANDLE			m_hThr_SensorMon	= NULL;
	BOOL			m_bLoop_SensorMon	= TRUE;
	static UINT WINAPI	Thread_SensorMon	(__in LPVOID lParam);

	// 센서 모니터링 타이머
	static VOID CALLBACK TimerRoutine_SensorCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);

	void		OnMonitorSensorCheck		();
	void		CreateTimerQueue_Mon		();
	void		DeleteTimerQueue_Mon		();
	void		CreateTimerSensorCheck		();
	void		DeleteTimerSensorCheck		();

	void		GetMotorAmpStatus			();
	void		GetMotorOriginStatus		();
	void		GetMotorCurrentPosStatus	();
	void		GetMotorMotionStatus		();
	void		GetMotorLimitStatus			();
	void		GetMotorHomeStatus			();
	void		GetMotorAlarmStatus			();

	void		SetUpdateAxisCnt			();

public:

	BOOL		StartThread_SensorMon		();
	BOOL		StopThread_SensorMon		();
};