﻿//*****************************************************************************
// Filename	: Wnd_WorklistRotate.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistRotate_h__
#define Wnd_WorklistRotate_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_Rotate.h"

//=============================================================================
// Wnd_WorklistRotate
//=============================================================================
class CWnd_WorklistRotate : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistRotate)

public:
	CWnd_WorklistRotate();
	virtual ~CWnd_WorklistRotate();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	CList_Work_Rotate			m_list_Rotate[TICnt_Rotation];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistRotate_h__


