﻿//*****************************************************************************
// Filename	: Wnd_WorklistPatternNoise.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistPatternNoise_h__
#define Wnd_WorklistPatternNoise_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_PatternNoise.h"

//=============================================================================
// Wnd_WorklistPatternNoise
//=============================================================================
class CWnd_WorklistPatternNoise : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistPatternNoise)

public:
	CWnd_WorklistPatternNoise();
	virtual ~CWnd_WorklistPatternNoise();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

//	CList_Work_PatternNoise				m_list_PatternNoise[TICnt_PatternNoise];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistPatternNoise_h__


