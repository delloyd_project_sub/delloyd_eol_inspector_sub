﻿#ifndef Wnd_BaseParticleManualOp_h__
#define Wnd_BaseParticleManualOp_h__

#pragma once

#include "Wnd_ParticleManualOp.h"

// CWnd_BaseParticleManualOp

class CWnd_BaseParticleManualOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseParticleManualOp)

public:
	CWnd_BaseParticleManualOp();
	virtual ~CWnd_BaseParticleManualOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_ParticleManualOp m_wndParticleManualOp[TICnt_ParticleManual];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseParticleManualOp_h__
