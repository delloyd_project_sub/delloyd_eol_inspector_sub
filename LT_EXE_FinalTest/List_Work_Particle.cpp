﻿// List_Work_Particle.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_Particle.h"


// CList_Work_Particle

IMPLEMENT_DYNAMIC(CList_Work_Particle, CListCtrl)

CList_Work_Particle::CList_Work_Particle()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_Particle::~CList_Work_Particle()
{
	m_Font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CList_Work_Particle, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CList_Work_Particle 메시지 처리기입니다.
int CList_Work_Particle::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	InitHeader();
	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_Particle::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	int iColWidth[Par_W_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol <Par_W_MaxCol; nCol++)
		iColDivide += iHeaderWidth_Par_Worklist[nCol];

	for (int nCol = 0; nCol < Par_W_MaxCol; nCol++)
		SetColumnWidth(nCol, iHeaderWidth_Par_Worklist[nCol]);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
BOOL CList_Work_Particle::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Header_MaxNum
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
UINT CList_Work_Particle::Header_MaxNum()
{
	return (UINT)Par_W_MaxCol;
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_Particle::InitHeader()
{
	for (int nCol = 0; nCol < Par_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_Par_Worklist[nCol], iListAglin_Par_Worklist[nCol], iHeaderWidth_Par_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_Particle::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));
	SetRectRow(iNewCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_Particle::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

 	CString strText;

	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, Par_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, Par_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, Par_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, Par_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, Par_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, Par_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, Par_W_Barcode, strText);

	strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	SetItemText(nRow, Par_W_Operator, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stParticle[m_nTestIndex].stParticleResult.nResult].szText);
	SetItemText(nRow, Par_W_Result, strText);

	if (pstCamInfo->stParticle[m_nTestIndex].stParticleResult.nResult == TER_Init)
	{
		strText.Format(_T("X"));
		SetItemText(nRow, Par_W_Count, strText);
	}
	else
	{
		strText.Format(_T("%d"), pstCamInfo->stParticle[m_nTestIndex].stParticleResult.nFailCount);
		SetItemText(nRow, Par_W_Count, strText);
	}
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:43
// Desc.		:
//=============================================================================
void CList_Work_Particle::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = Par_W_MaxCol;
	CString temp[Par_W_MaxCol];
	for (int t = 0; t < Par_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
