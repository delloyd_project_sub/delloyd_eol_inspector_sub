﻿#include "stdafx.h"
#include "TI_EIAJ.h"

#define hjm_re  0 // 0:만곡현상 수치 안나오도록 함 1:만곡현상 수치 낮아지도록 함 

CTI_EIAJ::CTI_EIAJ()
{
	m_Black_Ref_Value = 0;
	m_White_Ref_Value = 0;
	m_Ref_Threshold = 0;
	m_BW_Ref_Sub_Value = 0;
	m_avg_check_line = 0;
	m_nWidth = 720;
	m_nHeight = 480;
}

CTI_EIAJ::~CTI_EIAJ()
{
	
}

//=============================================================================
// Method		: SetImageSize
// Access		: public  
// Returns		: BOOL
// Parameter	: DWORD dwWidth
// Parameter	: DWORD dwHeight
// Qualifier	:
// Last Update	: 2017/7/7 - 10:47
// Desc.		:
//=============================================================================
BOOL CTI_EIAJ::SetImageSize(DWORD dwWidth, DWORD dwHeight)
{
	if (dwWidth <= 0 || dwHeight <= 0)
		return FALSE;

	m_nWidth = dwWidth;
	m_nHeight = dwHeight;

	return TRUE;
}

//=============================================================================
// Method		: CheckResolution
// Access		: public  
// Returns		: BOOL
// Parameter	: ST_LT_TI_EIAJ * pstEIAJ
// Parameter	: LPBYTE IN_RGB
// Parameter	: int iNum
// Parameter	: int iloopcnt
// Qualifier	:
// Last Update	: 2017/1/16 - 13:50
// Desc.		:
//=============================================================================
BOOL CTI_EIAJ::CheckEIAJ(ST_LT_TI_EIAJ *pstEIAJ, LPBYTE IN_RGB, int iNum, int iloopcnt)
{
	pstEIAJ->stEIAJOp.rectData[iNum].bResult = FALSE;

	BYTE R, G, B;

	int iWidth	 = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Width();
	int iHeight	 = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Height();
	int iOffSetX = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.left;
	int iOffSetY = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.top;

	if (iWidth < 1)
		return FALSE;

	if (iHeight < 1)
		return FALSE;

	if (iOffSetX <1)
		return FALSE;

	if (iOffSetY < 1)
		return FALSE;

	IplImage* pImage_Binary = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
	IplImage* pImage_Egde = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
	IplImage* pImage_src = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 3);



	BYTE	*BWImage = new BYTE[iWidth * iHeight];

	BYTE	Sum_Y = 0;
	int		iRCount = 1;

 	for (int y = 0; y < iHeight; y++)
	{
		//영역으로 지정한 범위의 RGB데이터를 추출하여 밝기 영상만 추출한다. 	
 		for (int x = 0; x < iWidth; x++)
		{
			//0~100	
			if (iOffSetX + x > -1 && iOffSetX + x < (int)m_nWidth && iOffSetY + y > -1 && iOffSetY + y < (int)m_nHeight)
 			{
				B = IN_RGB[(iOffSetY + y)*(m_nWidth * 3) + (iOffSetX + x) * 3];
				G = IN_RGB[(iOffSetY + y)*(m_nWidth * 3) + (iOffSetX + x) * 3 + 1];
				R = IN_RGB[(iOffSetY + y)*(m_nWidth * 3) + (iOffSetX + x) * 3 + 2];
 
				pImage_src->imageData[y*pImage_src->widthStep + (x * 3) + 0] = B;
				pImage_src->imageData[y*pImage_src->widthStep + (x * 3) + 1] = G;
				pImage_src->imageData[y*pImage_src->widthStep + (x * 3) + 2] = R;


 				Sum_Y = (BYTE)((0.29900*R) + (0.58700*G) + (0.11400*B));
 
 				int deltaY = (iOffSetY + y);
 				int deltaX = (iOffSetX + x);
 
 				//BWImage[(y)* (iWidth)+(x)] = Sum_Y;
 			}
 		}
 		iRCount++;
 	}
	// 델로이드 EOL 기준 EIAJ 알고리즘과 동일하게...
	cvCvtColor(pImage_src, pImage_Binary, CV_RGB2GRAY);

	//cvSmooth(pImage_Binary, pImage_Binary, CV_GAUSSIAN, 3, 3, 1.0, 1.0);

	cvSmooth(pImage_Binary, pImage_Binary, CV_GAUSSIAN, 1, 1, 1.0, 1.0);

	cvCopy(pImage_Binary, pImage_Egde);

	cvCanny(pImage_Egde, pImage_Egde, 0, 150);

	for (int y = 0; y < iHeight; y++)
	{
		for (int x = 0; x < iWidth; x++)
		{
			BWImage[y * iWidth + x] = pImage_Binary->imageData[y * pImage_Binary->widthStep + x];
		}
	}


	// EOL은 Switch 문으로 코딩 되어 있음.
	// 핵심 내용은 똑같음.
	BOOL Egde_Corr = FALSE;
	int* Line_Count = 0;

	int Fail_Line = 0;
	BOOL bFind_Egde = FALSE;

	CvScalar Tmp;

	if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Bottom_Top ||	pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Left_Right)
	{
		bFind_Egde = SetStartEndPos(pstEIAJ, BWImage, iNum, pstEIAJ->stEIAJOp.rectData[iNum].i_Mode, m_White_Ref_Value, m_Black_Ref_Value);

		if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Bottom_Top)
		{
			Line_Count = new int[iHeight];
			//memset(Line_Count, 0, iHeight);
			for (int i = 0; i < iHeight; i++){
				Line_Count[i] = 0;
			}

			for (int j = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y - 5; j > pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y + 5; j--)
			{
				for (int i = 0; i < pImage_Egde->width; i++)
				{
					Tmp = cvGet2D(pImage_Egde, j, i);
					if (Tmp.val[0] > 0)
						Line_Count[j]++;
				}
				if (Line_Count[j] <= 4)
				{
					Fail_Line = j;
					Egde_Corr = TRUE;
					break;
				}
			}

			delete[] Line_Count;

			if (abs(Fail_Line - pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y) < abs(Fail_Line - pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y) && Egde_Corr == TRUE)
			{
				pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt] = Fail_Line;
			}
			else
			{
				if (pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x == 0 && pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y == 0
					|| pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x == 0 && pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y == 0)
				{
					pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt] = 0;
				}
				else
				{
					pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt]
						= GetCheckLine_LR_DU_using_Sharpness(pstEIAJ, BWImage, pstEIAJ->stEIAJOp.rectData[iNum].i_Mode, iWidth, iHeight, iNum, iloopcnt);
				}
			}

		}
		else if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Left_Right)
		{
			Line_Count = new int[iWidth];
			//memset(Line_Count, 0, iWidth);
			for (int i = 0; i < iWidth; i++){
				Line_Count[i] = 0;
			}

			for (int i = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x + 10; i < pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x - 10; i++)
			{
				for (int j = 0; j < pImage_Egde->height; j++)
				{
					Tmp = cvGet2D(pImage_Egde, j, i);
					if (Tmp.val[0] > 0)
						Line_Count[i]++;
				}
				if (Line_Count[i] <= 4)
				{
					Fail_Line = i;
					Egde_Corr = TRUE;
					break;
				}
			}

			delete[] Line_Count;

			if (abs(Fail_Line - pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x) < abs(Fail_Line - pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x) && Egde_Corr == TRUE)
			{
				pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt] = Fail_Line;
			}
			else
			{
				if (pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x == 0 && pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y == 0
					|| pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x == 0 && pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y == 0)
				{
					pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt] = 0;
				}
				else
				{
					pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt]
						= GetCheckLine_LR_DU_using_Sharpness(pstEIAJ, BWImage, pstEIAJ->stEIAJOp.rectData[iNum].i_Mode, iWidth, iHeight, iNum, iloopcnt);
				}
			}
		}
	}
	if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Top_Bottom ||	pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Right_Left)
	{
		bFind_Egde = SetStartEndPos(pstEIAJ, BWImage, iNum, pstEIAJ->stEIAJOp.rectData[iNum].i_Mode, m_White_Ref_Value, m_Black_Ref_Value);
		
		if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Top_Bottom)
		{
			Line_Count = new int[iHeight];
			//memset(Line_Count, 0, iHeight);
			for (int i = 0; i < iHeight; i++){
				Line_Count[i] = 0;
			}

			for (int j = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y + 5; j < pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y - 5; j++)
			{
				for (int i = 0; i < pImage_Egde->width; i++)
				{
					Tmp = cvGet2D(pImage_Egde, j, i);
					if (Tmp.val[0] > 0)
						Line_Count[j]++;
				}
				if (Line_Count[j] <= 4)
				{
					Fail_Line = j;
					Egde_Corr = TRUE;
					break;
				}
			}
			delete[]Line_Count;

			if (abs(Fail_Line - pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y) < abs(Fail_Line - pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y) && Egde_Corr == TRUE)
			{
				pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt] = Fail_Line;
			}
			else
			{
				if (pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x == 0 && pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y == 0
					|| pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x == 0 && pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y == 0)
				{
					pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt] = 0;
				}
				else
				{
					pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt]
						= GetCheckLine_RL_UD_using_Sharpness(pstEIAJ, BWImage, pstEIAJ->stEIAJOp.rectData[iNum].i_Mode, iWidth, iHeight, iNum, iloopcnt);
				}
			}
		}
		else if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Right_Left)
		{
			Line_Count = new int[iWidth];
			//memset(Line_Count, 0, iWidth);
			for (int i = 0; i < iWidth; i++){
				Line_Count[i] = 0;
			}
			for (int i = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x - 10; i > pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x + 10; i--)
			{
				for (int j = 0; j < pImage_Egde->height; j++)
				{
					Tmp = cvGet2D(pImage_Egde, j, i);
					if (Tmp.val[0] > 0)
						Line_Count[i]++;
				}
				if (Line_Count[i] <= 4)
				{
					Fail_Line = i;
					Egde_Corr = TRUE;
					break;
				}
			}
			delete[]Line_Count;

			if (abs(Fail_Line - pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x) < abs(Fail_Line - pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x) && Egde_Corr == TRUE)
			{
				pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt] = Fail_Line;
			}
			else
			{
				if (pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x == 0 && pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y == 0
					|| pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x == 0 && pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y == 0)
				{
					pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt] = 0;
				}
				else
				{
					pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer[pstEIAJ->stEIAJOp.rectData[iNum].checkLine_Buffer_cnt]
						= GetCheckLine_RL_UD_using_Sharpness(pstEIAJ, BWImage, pstEIAJ->stEIAJOp.rectData[iNum].i_Mode, iWidth, iHeight, iNum, iloopcnt);
				}
			}
		}
	}

	delete[]BWImage;
	cvReleaseImage(&pImage_src);
	cvReleaseImage(&pImage_Binary);
	cvReleaseImage(&pImage_Egde);


	return FALSE;
}

//=============================================================================
// Method		: SetStartEndPos
// Access		: public  
// Returns		: void
// Parameter	: ST_LT_TI_EIAJ * pstEIAJ
// Parameter	: BYTE * BWImage
// Parameter	: int iNum
// Parameter	: int iMode
// Parameter	: int iref_white_value
// Parameter	: int iref_black_value
// Qualifier	:
// Last Update	: 2017/1/16 - 14:49
// Desc.		:
//=============================================================================
BOOL CTI_EIAJ::SetStartEndPos(ST_LT_TI_EIAJ *pstEIAJ, BYTE *BWImage, int iNum, int iMode, int iref_white_value, int iref_black_value)
{
	unsigned int curr_pixel_v;

	int iWidth	= pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Width();
	int iHeight = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Height();

	BYTE *temp_BWImage = new BYTE[iWidth * iHeight];

	pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x	= 0;
	pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y	= 0;
	pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x	= 0;
	pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y	= 0;

	IplImage *GrayImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);

	//kdy
	for (int y = 0; y < iHeight; y++)
	{
		for (int x = 0; x < iWidth; x++)
		{
			GrayImage->imageData[y * GrayImage->widthStep + x] = BWImage[y * (iWidth) + x];
		}
	}

	cvNormalize(GrayImage, GrayImage, 0, 255, CV_MINMAX);

	cvThreshold(GrayImage, GrayImage, 150, 255, CV_THRESH_BINARY);
	
	cvRectangle(GrayImage,cvPoint(0,0),cvPoint(iWidth-1,iHeight-1),CV_RGB(255,255,255),8);

#if 0
	for (int x = 0; x < iWidth; x++)
	{
		for (int y = 0; y< iHeight; y++)
		{
			curr_pixel_v = GrayImage->imageData[y * GrayImage->widthStep + x];

			if (curr_pixel_v >(double)(iref_white_value) / 1.5)
				temp_BWImage[(y)*(iWidth)+(x)] = 255;
			else
				temp_BWImage[(y)*(iWidth)+(x)] = 0;
		}
	}
#endif
	for (int x = 0; x < iWidth; x++)
	{
		for (int y = 0; y< iHeight; y++)
		{
			curr_pixel_v = GrayImage->imageData[y * GrayImage->widthStep + x];

			if (curr_pixel_v == 0 )
				temp_BWImage[(y)*(iWidth)+(x)] = 0;
			else
				temp_BWImage[(y)*(iWidth)+(x)] = 100;
		}
	}

	int count = 0;

	BOOL seek_OK = FALSE;
	if (iMode == Resol_Mode_Left_Right)
	{
		count = 0;

		int Limit_X_Min = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.left + pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Width() / 3;
		int Limit_X_Max = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.right- pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Width() / 3;
		for (int x = 0; x < iWidth/2; x++)
		{
			for (int y = 0; y < iHeight; y++)
			{
				curr_pixel_v = temp_BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v == 0)
				{
					if (iMode == Resol_Mode_Left_Right){
						pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x = x;
						pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y = y;
					}
					else if (iMode == Resol_Mode_Right_Left){
						pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x = x;
						pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y = y;
					}
					seek_OK = TRUE;
					break;
				}
			}
			if (seek_OK == TRUE)
				break;
		}

		seek_OK = FALSE;
		count = 0;
		for (int x = iWidth - 1; x > -1; x--)
		{
			for (int y = 0; y < iHeight; y++)
			{
				curr_pixel_v = temp_BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v == 0)
				{
					if (iMode == Resol_Mode_Left_Right){
						pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x = x;
						pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y = y;
					}
					else if (iMode == Resol_Mode_Right_Left){
						pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x = x;
						pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y = y;
					}

					seek_OK = TRUE;
					break;
				}
			}

			if (seek_OK == TRUE)
				break;
		}
	}
	else if ( iMode == Resol_Mode_Right_Left)
	{
		BOOL seek_OK = FALSE;
		count = 0;

		int Limit_X_Min = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.left + pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Width() / 4;
		int Limit_X_Max = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.right - pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Width() / 4;
		for (int x = 0; x < iWidth / 2; x++)
		{
			for (int y = 0; y < iHeight; y++)
			{
				curr_pixel_v = temp_BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v == 0)
				{
					if (Limit_X_Min  < x && Limit_X_Max > x)
					{
					}
					else
					{
						if (count == 0)
						{
							count++;
						}
						else{

							if (iMode == Resol_Mode_Left_Right){
								pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x = x;
								pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y = y;
							}
							else if (iMode == Resol_Mode_Right_Left){
								pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x = x;
								pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y = y;
							}
							seek_OK = TRUE;
							break;

						}
					}


				}
			}
			if (seek_OK == TRUE)
				break;
		}

		seek_OK = FALSE;
		count = 0;
		for (int x = iWidth - 1; x > -1; x--)
		{
			for (int y = 0; y < iHeight; y++)
			{
				curr_pixel_v = temp_BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v == 0)
				{
					if (Limit_X_Min < x && Limit_X_Max > x)
					{
					}
					else{
						if (count == 0)
						{
							count++;
						}
						else{
							if (iMode == Resol_Mode_Left_Right){
								pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x = x;
								pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y = y;
							}
							else if (iMode == Resol_Mode_Right_Left){
								pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x = x;
								pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y = y;
							}

							seek_OK = TRUE;
							break;
						}
					}
				}
			}

			if (seek_OK == TRUE)
				break;
		}
	}
	else if (iMode == Resol_Mode_Top_Bottom || iMode == Resol_Mode_Bottom_Top){
		bool seek_OK = FALSE;
		count = 0;

		for (int y = 0; y < iHeight; y++)
		{
			for (int x = 0; x < iWidth; x++)
			{
				curr_pixel_v = temp_BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v == 0)
				{
					if (count == 0)
					{
						count++;
					}
					else
					{
						if (iMode == Resol_Mode_Bottom_Top){
							pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x = x;
							pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y = y;
						}
						else if (iMode == Resol_Mode_Top_Bottom){
							pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x = x;
							pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y = y;
						}

						seek_OK = TRUE;
						break;
					}
				}
			}
			if (seek_OK == TRUE)
				break;
		}
		seek_OK = FALSE;
		count = 0;

		for (int y = iHeight - 1; y > -1; y--)
		{
			for (int x = 0; x < iWidth; x++)
			{
				curr_pixel_v = temp_BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v == 0)
				{
					if (count == 0)
					{
						count++;
					}
					else
					{
						if (iMode == Resol_Mode_Bottom_Top){
							pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x = x;
							pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y = y;
						}
						else if (iMode == Resol_Mode_Top_Bottom){
							pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x = x;
							pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y = y;
						}

						seek_OK = TRUE;
						break;
					}
				}
			}
			if (seek_OK == TRUE)
				break;			
		}
	}

	cvReleaseImage(&GrayImage);
	delete[]temp_BWImage;

	return seek_OK;
}

//=============================================================================
// Method		: Resolution_Test
// Access		: public  
// Returns		: UINT
// Parameter	: ST_LT_TI_EIAJ * pstEIAJ
// Parameter	: LPBYTE *pImageBuf
// Qualifier	:
// Last Update	: 2017/1/16 - 13:17
// Desc.		:
//=============================================================================
UINT CTI_EIAJ::EIAJ_Test(ST_LT_TI_EIAJ *pstEIAJ, LPBYTE pImageBuf)
{
	if (pstEIAJ == NULL)
		return TER_MachineCheck;

	int itempValue = 0;

	//pstEIAJ->stEIAJData.reset();
	

	CvPoint SearchPoint = SearchCenterPoint(pImageBuf);

	for (int lop = ReOp_CenterBlack; lop < ReOp_ItemNum; lop++)
	{
		if (pstEIAJ->stEIAJOp.rectData[lop].bUse == TRUE)
		{
			int posX = pstEIAJ->stEIAJOp.StdrectData[lop].RegionList.CenterPoint().x;
			int posY = pstEIAJ->stEIAJOp.StdrectData[lop].RegionList.CenterPoint().y;
			int OffsetX = pstEIAJ->stEIAJOp.StdrectData[0].RegionList.CenterPoint().x;
			int OffsetY = pstEIAJ->stEIAJOp.StdrectData[0].RegionList.CenterPoint().y;
			int Width = pstEIAJ->stEIAJOp.StdrectData[lop].RegionList.Width();
			int Height = pstEIAJ->stEIAJOp.StdrectData[lop].RegionList.Height();

			pstEIAJ->stEIAJOp.rectData[lop]._Rect_Position_Sum(posX - OffsetX + SearchPoint.x, posY - OffsetY + SearchPoint.y, Width, Height);
		}
		
		
	}

	for (int lop = 0; lop < 2; lop++)
	{
		Get_Ref_Threshold_Value(pstEIAJ, pImageBuf, lop);//중심점을 측정하여 적용한다. 
	}

	for (int lop = ReOp_CenterLeft; lop < ReOp_ItemNum; lop++)
	{
		m_avg_check_line = 0;
		pstEIAJ->stEIAJData.bUse[lop] = pstEIAJ->stEIAJOp.rectData[lop].bUse;

		if (pstEIAJ->stEIAJOp.rectData[lop].bUse == TRUE){

			//	m_old_centerOffset = new_centerOffset;

			if (pstEIAJ->stEIAJOp.rectData[lop].i_MtfRatio  > 30.0)
				m_Ref_Threshold = (UINT)((double)(m_White_Ref_Value - m_Black_Ref_Value) * (double)(31.0 / 100.0));
			else
				m_Ref_Threshold = (UINT)((double)(m_White_Ref_Value - m_Black_Ref_Value) * (double)(pstEIAJ->stEIAJOp.rectData[lop].i_MtfRatio / 100.0));

			CheckEIAJ(pstEIAJ, pImageBuf, lop, 0);
			pstEIAJ->stEIAJOp.rectData[lop].checkLine_Buffer_cnt++;

			if (pstEIAJ->stEIAJOp.rectData[lop].checkLine_Buffer_cnt == 2)
			{
				for (int k = 0; k < 1; k++)
					pstEIAJ->stEIAJOp.rectData[lop].checkLine_Buffer[k] = pstEIAJ->stEIAJOp.rectData[lop].checkLine_Buffer[k + 1];

				pstEIAJ->stEIAJOp.rectData[lop].checkLine_Buffer_cnt = 1;
			}

			for (int k = 0; k < pstEIAJ->stEIAJOp.rectData[lop].checkLine_Buffer_cnt; k++)
				m_avg_check_line += pstEIAJ->stEIAJOp.rectData[lop].checkLine_Buffer[k];

			m_avg_check_line = m_avg_check_line / 1;	//10 : Circular Buffer Number


			pstEIAJ->stEIAJOp.rectData[lop].CheckLine = m_avg_check_line;
			pstEIAJ->stEIAJData.nValueResult[lop] = 0;


			int data = 0;
			int iResult = 0;
			if ((lop > 1) && (lop < 6))
			{
				iResult = GetResultData(pstEIAJ, lop);//; + i_CenterOffset;
			}
			else
			{
				iResult = GetResultData(pstEIAJ, lop);//; + i_EdgeOffset;
			}
			pstEIAJ->stEIAJData.nValueResult[lop] = iResult + pstEIAJ->stEIAJOp.rectData[lop].i_offset;
		}

		if (pstEIAJ->stEIAJData.nValueResult[lop] == 0)
		{
			IplImage* TmpIMG = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);
			for (int y = 0; y < m_nHeight; y++)
			{
				//영역으로 지정한 범위의 RGB데이터를 추출하여 밝기 영상만 추출한다. 	
				for (int x = 0; x < m_nWidth; x++)
				{
					TmpIMG->imageData[y*TmpIMG->widthStep + (x * 3) + 0] = pImageBuf[y*(m_nWidth * 3) + x * 3];
					TmpIMG->imageData[y*TmpIMG->widthStep + (x * 3) + 1] = pImageBuf[y*(m_nWidth * 3) + x * 3 + 1];
					TmpIMG->imageData[y*TmpIMG->widthStep + (x * 3) + 2] = pImageBuf[y*(m_nWidth * 3) + x * 3 + 2];

				}
			}

			//cvSaveImage("D:\\test\\test1_Result_0.bmp", TmpIMG);
			cvReleaseImage(&TmpIMG);
		}



	}

	
	for (int lop = ReOp_CenterLeft; lop < ReOp_ItemNum; lop++)
	{
		if (pstEIAJ->stEIAJData.nValueResult[lop] >= (int)pstEIAJ->stEIAJOp.rectData[lop].i_Threshold_Min
			&& pstEIAJ->stEIAJData.nValueResult[lop] <= (int)pstEIAJ->stEIAJOp.rectData[lop].i_Threshold_Max)
		{
			pstEIAJ->stEIAJData.nEachResult[lop] = TRUE;
		}
		else
		{
			pstEIAJ->stEIAJData.nEachResult[lop] = FALSE;
		}
	}

	UINT PassCnt = 0, TotalCnt = 0;
	for (int t = 0; t < 12; t++)
	{
		if (pstEIAJ->stEIAJOp.rectData[t + 2].bUse)
		{
			if (pstEIAJ->stEIAJData.nEachResult[t+2] == TRUE)
			{
				PassCnt++;
			}
			TotalCnt++;
		}
	}

	pstEIAJ->stEIAJData.nTotal_cnt = TotalCnt;
	pstEIAJ->stEIAJData.nResult_cnt = PassCnt;

	if (pstEIAJ->stEIAJData.nTotal_cnt == pstEIAJ->stEIAJData.nResult_cnt)
		pstEIAJ->stEIAJData.nResult = TER_Pass;
	else
		pstEIAJ->stEIAJData.nResult = TER_Fail;

	UINT CenterSum_Data = 0;
	UINT sideCnt = 0;
	UINT sidePassCnt = 0;

	for (int t = 5; t < 12; t++)
	{
		if (pstEIAJ->stEIAJOp.rectData[t + 2].bUse)
		{
			if (pstEIAJ->stEIAJOp.rectData[t + 2].Result_data >= 250)
			{
				sidePassCnt++;
			}
			sideCnt++;
		}
	}

	//DeleteMemory(pstEIAJ);

	return pstEIAJ->stEIAJData.nResult;
}

//=============================================================================
// Method		: Get_Ref_Threshold_Value
// Access		: public  
// Returns		: UINT
// Parameter	: ST_LT_TI_EIAJ * pstEIAJ
// Parameter	: LPBYTE IN_RGB
// Parameter	: int iNum
// Qualifier	:
// Last Update	: 2017/1/16 - 13:28
// Desc.		:
//=============================================================================
UINT CTI_EIAJ::Get_Ref_Threshold_Value(ST_LT_TI_EIAJ *pstEIAJ, LPBYTE IN_RGB, int iNum)
{
	BYTE R, G, B;

	pstEIAJ->stEIAJOp.rectData[iNum].bResult = FALSE;

	int Width	= pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Width();
	int Height	= pstEIAJ->stEIAJOp.rectData[iNum].RegionList.Height();
	int OffSetX = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.left;
	int OffSetY = pstEIAJ->stEIAJOp.rectData[iNum].RegionList.top;

	if (Height < 1)
		return 0;

	if (Width < 1)
		return 0;

	if (OffSetX < 1)
		return 0;

	if (OffSetY < 1)
		return 0;

	BYTE *BWImage = new BYTE[Width * Height * 3];

	double	Sum_Y = 0;

	for (int y = 0; y < Height; y++){//영역으로 지정한 범위의 RGB데이터를 추출하여 밝기 영상만 추출한다. 	
		for (int x = 0; x < Width; x++){//0~100	

			B = IN_RGB[(OffSetY + y)*(m_nWidth * 3) + (OffSetX + x) * 3];
			G = IN_RGB[(OffSetY + y)*(m_nWidth * 3) + (OffSetX + x) * 3 + 1];
			R = IN_RGB[(OffSetY + y)*(m_nWidth * 3) + (OffSetX + x) * 3 + 2];

			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			BWImage[(y) * (Width) + (x)] = (BYTE)Sum_Y;
// 			BWImage[(y)*(Width * 4) + (x)* 4 + 1] = (BYTE)Sum_Y;
// 			BWImage[(y)*(Width * 4) + (x)* 4 + 2] = (BYTE)Sum_Y;
		}
	}

	unsigned int Total_Y = 0, Avg_Y = 0;

	if (iNum == 1)
	{
		for (int y = 0; y < Height; y++)
		{
			for (int x = 0; x < Width; x++)
			{
				Total_Y += BWImage[(y) * (Width) + (x)];
			}
		}

		if (pstEIAJ->stEIAJOp.rectData[1].bUse)
			m_White_Ref_Value = Total_Y / (Width * Height);
	}
	else if (iNum == 0)
	{
		for (int y = 0; y < Height; y++)
		{
			for (int x = 0; x < Width; x++)
			{
				Total_Y += BWImage[(y) * (Width) + (x)];
			}
		}

		if (pstEIAJ->stEIAJOp.rectData[0].bUse)
			m_Black_Ref_Value = Total_Y / (Width * Height);
	}

	m_BW_Ref_Sub_Value = m_White_Ref_Value - m_Black_Ref_Value;

	delete[] BWImage;

	return TER_Pass;
}

//=============================================================================
// Method		: GetResultData
// Access		: public  
// Returns		: int
// Parameter	: ST_LT_TI_EIAJ * pstEIAJ
// Parameter	: int iNum
// Qualifier	:
// Last Update	: 2017/1/16 - 14:14
// Desc.		:
//=============================================================================
int CTI_EIAJ::GetResultData(ST_LT_TI_EIAJ *pstEIAJ, int iNum)
{
	double x = 0, y = 0;
	double a = -0.0009375, b = 1.0725;	//해상력 출력 이차함수 계수(중앙부분 130도 광각 렌즈)

	if (pstEIAJ->stEIAJOp.rectData[iNum].CheckLine == -1)
		return 0;

	if (iNum < 6)
	{
		a = 0.0;
		b = 1;
	}
	else
	{
		a = -0.0009375;
		b = 1.0725;
	}

	if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Top_Bottom)
	{
		if (pstEIAJ->stEIAJOp.rectData[iNum].CheckLine >= pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y)
			pstEIAJ->stEIAJOp.rectData[iNum].CheckLine = pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y;

		if (pstEIAJ->stEIAJOp.rectData[iNum].CheckLine >= pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y &&
			pstEIAJ->stEIAJOp.rectData[iNum].CheckLine <= pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y)
			x = pstEIAJ->stEIAJOp.rectData[iNum].CheckLine - pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y;
		else
			return 0;
	}
	else if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Bottom_Top)
	{
		if (pstEIAJ->stEIAJOp.rectData[iNum].CheckLine <= pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y)
			pstEIAJ->stEIAJOp.rectData[iNum].CheckLine = pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y;

		if (pstEIAJ->stEIAJOp.rectData[iNum].CheckLine >= pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y &&
			pstEIAJ->stEIAJOp.rectData[iNum].CheckLine <= pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y)
			x = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y - pstEIAJ->stEIAJOp.rectData[iNum].CheckLine;
		else
			return 0;
	}

	else if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Left_Right)
	{
		if (pstEIAJ->stEIAJOp.rectData[iNum].CheckLine >= pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x)
			pstEIAJ->stEIAJOp.rectData[iNum].CheckLine = pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x;

		if (pstEIAJ->stEIAJOp.rectData[iNum].CheckLine >= pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x &&
			pstEIAJ->stEIAJOp.rectData[iNum].CheckLine <= pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x)
			x = pstEIAJ->stEIAJOp.rectData[iNum].CheckLine - pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x;
		else
			return 0;
	}
	else if (pstEIAJ->stEIAJOp.rectData[iNum].i_Mode == Resol_Mode_Right_Left)
	{
		if (pstEIAJ->stEIAJOp.rectData[iNum].CheckLine <= pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x)
			pstEIAJ->stEIAJOp.rectData[iNum].CheckLine = pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x;

		if (pstEIAJ->stEIAJOp.rectData[iNum].CheckLine >= pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x &&
			pstEIAJ->stEIAJOp.rectData[iNum].CheckLine <= pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x)
			x = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x - pstEIAJ->stEIAJOp.rectData[iNum].CheckLine;
		else
			return 0;
	}


	y = (a * (x*x)) + (b*x);

	double sx1, sy1, sx2, sy2;
	sx1 = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x;
	sy1 = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y;
	sx2 = pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x;
	sy2 = pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y;

	double dist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));

	if (dist == 0)
		return 0;

	double ratio = y / dist;
	if (ratio > 1.0)
		ratio = 1.0;
	int RD_OFFSET = (int)(ratio * (pstEIAJ->stEIAJOp.rectData[iNum].i_Range_Max - pstEIAJ->stEIAJOp.rectData[iNum].i_Range_Min));

	return pstEIAJ->stEIAJOp.rectData[iNum].i_Range_Min + RD_OFFSET;
}

//=============================================================================
// Method		: GetEdgeValue
// Access		: public  
// Returns		: int
// Parameter	: LPBYTE IN_RGB
// Parameter	: int iStartX
// Parameter	: int iStartY
// Parameter	: int iWidth
// Parameter	: int iHeight
// Qualifier	:
// Last Update	: 2017/1/16 - 15:29
// Desc.		:
//=============================================================================
int CTI_EIAJ::GetEdgeValue(LPBYTE IN_RGB, int iStartX, int iStartY, int iWidth, int iHeight)
{

	if (iStartX < 0)
		iStartX = 0;
	if (iStartY < 0)
		iStartY = 0;
	if (iStartX + iWidth >(int)m_nWidth)
	{
		int dev = (iStartX + iWidth) - m_nWidth;
		iStartX = iStartX - dev;
	}
	if (iStartY + iHeight >(int)m_nHeight)
	{
		int dev = (iStartY + iHeight) - m_nHeight;
		iStartY = iStartY - dev;
	}

	BYTE *BWImage = new BYTE[iWidth * iHeight * 3];

	IplImage *grayImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
	IplImage *grayImage2 = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);

	cvSetZero(grayImage);
	cvSetZero(grayImage2);
	BYTE R, G, B; BYTE Sum_Y;
	for (int y = iStartY; y < (iStartY + iHeight); y++)
	{
		for (int x = iStartX; x < (iStartX + iWidth); x++)
		{


			B = IN_RGB[(y)*(m_nWidth * 3) + (x)* 3];
			G = IN_RGB[(y)*(m_nWidth * 3) + (x)* 3 + 1];
			R = IN_RGB[(y)*(m_nWidth * 3) + (x)* 3 + 2];

			Sum_Y = (R + G + B) / 3;

			BWImage[(y - iStartY)*(iWidth * 3) + (x - iStartX) * 3] = Sum_Y;
			BWImage[(y - iStartY)*(iWidth * 3) + (x - iStartX) * 3 + 1] = Sum_Y;
			BWImage[(y - iStartY)*(iWidth * 3) + (x - iStartX) * 3 + 2] = Sum_Y;

			grayImage->imageData[(y - iStartY) * grayImage->widthStep + (x - iStartX)] = BWImage[(y - iStartY)*(iWidth * 3) + (x - iStartX) * 3];
		}
	}

	int Left_VertValue, Center_VertValue, Right_VertValue;
	int Upper_VertValue, Down_VertValue;

	for (int i = 1; i < iHeight - 1; i++)
	{
		for (int j = 1; j < iWidth - 1; j++)
		{
			Left_VertValue		= (unsigned char)BWImage[i * iWidth * 3 + (j - 1) * 3];
			Center_VertValue	= (unsigned char)BWImage[i * iWidth * 3 + (j)* 3];
			Right_VertValue		= (unsigned char)BWImage[i * iWidth * 3 + (j + 1) * 3];

			Upper_VertValue		= (unsigned char)BWImage[(i - 1) * iWidth * 3 + (j)* 3];
			Center_VertValue	= (unsigned char)BWImage[i * iWidth * 3 + (j)* 3];
			Down_VertValue		= (unsigned char)BWImage[(i + 1) * iWidth * 3 + (j)* 3];



			if (abs(Left_VertValue - Center_VertValue) > 10 && abs(Right_VertValue - Center_VertValue) > 10)
				grayImage2->imageData[i * grayImage2->widthStep + (j)] = (unsigned char)(abs(Left_VertValue - Center_VertValue) + abs(Right_VertValue - Center_VertValue) / 2);
			else
				grayImage2->imageData[i * grayImage2->widthStep + (j)] = 0;

			if (abs(Upper_VertValue - Center_VertValue) > 10 && abs(Down_VertValue - Center_VertValue) > 10)
				grayImage2->imageData[i * grayImage2->widthStep + (j)] = (unsigned char)(abs(Upper_VertValue - Center_VertValue) + abs(Down_VertValue - Center_VertValue) / 2);
			else
				grayImage2->imageData[i * grayImage2->widthStep + (j)] = 0;
		}
	}

	int Y = 0;
	int SUM_Y = 0;

	for (int y = 1; y < iHeight - 1; y++)
	{
		for (int x = 1; x < iWidth - 1; x++)
		{
			Y = (unsigned char)grayImage2->imageData[y * grayImage2->widthStep + x];

			SUM_Y += Y;


		}
	}
	//	cvSaveImage("D:\\EdgeImage1.bmp", grayImage);
	//	cvSaveImage("D:\\EdgeImage2.bmp", grayImage2);
	cvReleaseImage(&grayImage);
	cvReleaseImage(&grayImage2);

	delete[]BWImage;

	return SUM_Y / 100;
}

//=============================================================================
// Method		: SearchCenterPoint
// Access		: public  
// Returns		: CvPoint
// Parameter	: LPBYTE IN_RGB
// Qualifier	:
// Last Update	: 2017/1/14 - 13:00
// Desc.		:
//=============================================================================
CvPoint CTI_EIAJ::SearchCenterPoint(LPBYTE IN_RGB)
{
	CvPoint resultPt = cvPoint((-1) * m_nWidth, (-1) * m_nHeight);

	IplImage *OriginImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBOrgImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);

	BYTE R, G, B;
	//double Sum_Y;

	for (int y = 0; y < (int)m_nHeight; y++)
	{
		for (int x = 0; x < (int)m_nWidth; x++)
		{
			B = IN_RGB[y*(m_nWidth * 3) + x * 3];
			G = IN_RGB[y*(m_nWidth * 3) + x * 3 + 1];
			R = IN_RGB[y*(m_nWidth * 3) + x * 3 + 2];

			if (R < 100 && G < 100 && B < 100)
				OriginImage->imageData[y*OriginImage->widthStep + x] = (char)255;
			else
				OriginImage->imageData[y*OriginImage->widthStep + x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}

	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvCopyImage(OriginImage, SmoothImage);

	cvCanny(SmoothImage, CannyImage, 0, 255);

	cvCopyImage(CannyImage, temp_PatternImage);

	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	int counter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		counter++;

	if (counter == 0)
	{
		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&OriginImage);
		cvReleaseImage(&PreprecessedImage);
		cvReleaseImage(&CannyImage);
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);
		cvReleaseImage(&temp_PatternImage);
		cvReleaseImage(&RGBOrgImage);

		return resultPt;
	}


	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area = 0, arcCount = 0;
	counter = 0;
	double old_dist = 999999;

	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size = 0, old_size = 0;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area) / (arcCount*arcCount);

		if (circularity > 0.8)
		{

			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width / 2;
			center_y = rect.y + rect.height / 2;

			if (center_x > (int)m_nWidth / 4 && center_x < (int)m_nWidth * 3 / 4 && center_y >(int)m_nHeight / 4 && center_y < (int)m_nHeight * 3 / 4)
			{
				if (rect.width < (int)m_nWidth / 2 && rect.height < (int)m_nHeight / 2 && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
					double distance = GetDistance(rect.x + rect.width / 2, rect.y + rect.height / 2, m_nWidth / 2, m_nHeight / 2);

					obj_Cnt++;

					size = rect.width * rect.height;

					if (distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);

				}
			}
		}

		counter++;
	}

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);

	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);
	cvReleaseImage(&PreprecessedImage);
	cvReleaseImage(&CannyImage);

	delete[]rectArray;
	delete[]areaArray;


	return resultPt;
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: int ix1
// Parameter	: int iy1
// Parameter	: int ix2
// Parameter	: int iy2
// Qualifier	:
// Last Update	: 2017/1/14 - 13:02
// Desc.		:
//=============================================================================
double CTI_EIAJ::GetDistance(int ix1, int iy1, int ix2, int iy2)
{
	double result;

	result = sqrt((double)((ix2 - ix1)*(ix2 - ix1)) + ((iy2 - iy1)*(iy2 - iy1)));

	return result;
}


//=============================================================================
// Method		: GetCheckLine_LR_DU_using_Sharpness
// Access		: public  
// Returns		: int
// Parameter	: ST_LT_TI_EIAJ * pstEIAJ
// Parameter	: BYTE * BWImage
// Parameter	: int icheck_mode
// Parameter	: int iWidth
// Parameter	: int iHeight
// Parameter	: int iNum
// Parameter	: int iloopcnt
// Qualifier	:
// Last Update	: 2017/1/16 - 15:09
// Desc.		:
//=============================================================================
int CTI_EIAJ::GetCheckLine_LR_DU_using_Sharpness(ST_LT_TI_EIAJ *pstEIAJ, BYTE *BWImage, int icheck_mode, int iWidth, int iHeight, int iNum, int iloopcnt)
{
	int max_v = 0, min_v = 255, loc_index = 0;
	BYTE curr_pixel_v/*, next_pixel_v, direction*/;
	bool Detected_Flag = FALSE;
	int result_Line;
	int avg_Deviation = 0/*, sub_Y*/;
	double BW_Ratio = 0.0;
	int black_line_cnt = 0;

	double range_Factor = 0.05;
	int Limit_SFRValue = 10;

	if (icheck_mode == Resol_Mode_Left_Right)
	{
		IplImage *tempImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
		BYTE *Q_BWImage = new BYTE[iWidth * iHeight];


		for (int y = 0; y < iHeight; y++)
		{
			for (int x = 0; x < iWidth; x++)
			{
				tempImage->imageData[y * tempImage->widthStep + x] = BWImage[y * (iWidth)+x];
			}
		}
		cvCanny(tempImage, tempImage, 50, 100);

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value;

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value;

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio;

		pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value = new double[iWidth * iHeight];
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value = new double[iWidth * iHeight];
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio = new double[iWidth * iHeight];

		bool weight_line_flag = FALSE;

		for (int x = 0; x < iWidth; x++)
		{
			for (int y = 0; y < iHeight - 1; y++)
			{
				curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v > m_White_Ref_Value)
					curr_pixel_v = m_White_Ref_Value;
				if (curr_pixel_v < m_Black_Ref_Value)
					curr_pixel_v = m_Black_Ref_Value;

				BWImage[(y)*(iWidth)+(x)] = curr_pixel_v;

				// 버그 !!
				unsigned int EdgeValue = (BYTE)tempImage->imageData[y * tempImage->widthStep + x];

				if (curr_pixel_v > EdgeValue)
					Q_BWImage[(y)*(iWidth)+(x)] = 255;
				else
					Q_BWImage[(y)*(iWidth)+(x)] = 0;
			}
		}

		CvPoint oldSFRValPos = cvPoint(0, 0);

		for (int i = 0; i < iHeight * iWidth; i++){
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[i] = 0;
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[i] = 0;
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio[i] = 0;
		}

		pstEIAJ->stEIAJOp.rectData[iNum].SFR_Height = 100;
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_Width = iWidth;


		for (int x = 0; x < iWidth; x++)
		{
			avg_Deviation = 0;

			int subCnt = 0;
			int StartY_pos = -1, EndY_pos = -1;

			for (int y = 0; y < iHeight; y++)
			{
				if (Q_BWImage[(y)*(iWidth)+(x)] == 0)
				{
					StartY_pos = y;
					break;
				}
			}
			for (int y = iHeight - 1; y > 0; y--)
			{
				if (Q_BWImage[(y)*(iWidth)+(x)] == 0)
				{
					EndY_pos = y;
					break;
				}
			}

			if (StartY_pos != -1 && EndY_pos != -1 && (EndY_pos - StartY_pos) != 0)
			{
				UINT Dmax = 0, Dmin = 255;
				int meanVal = 0;

				for (int y = StartY_pos + 1; y < EndY_pos - 1; y++)
				{
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_pixel_v > Dmax)
						Dmax = curr_pixel_v;
					if (curr_pixel_v < Dmin)
						Dmin = curr_pixel_v;					
				}

				for (int y = StartY_pos + 1; y < EndY_pos - 1; y++)
				{
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_pixel_v >(Dmax + Dmin) / 2.0)
						Q_BWImage[(y)*(iWidth)+(x)] = 255;
					else
						Q_BWImage[(y)*(iWidth)+(x)] = 0;
				}

				for (int y = 0; y < StartY_pos; y++)
					Q_BWImage[(y)*(iWidth)+(x)] = 127;
				for (int y = iHeight - 1; y > EndY_pos; y--)
					Q_BWImage[(y)*(iWidth)+(x)] = 127;



				Dmax = 0;
				Dmin = 0;
				int DmaxCnt = 0;
				int DminCnt = 0;

				for (int y = StartY_pos; y < EndY_pos; y++)
				{
					curr_pixel_v = Q_BWImage[(y)*(iWidth)+(x)];

					unsigned int curr_Q_pixel_v = Q_BWImage[(y)*(iWidth)+(x)];
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_Q_pixel_v == 255)
					{
						Dmax += curr_pixel_v;
						DmaxCnt++;
					}
					else if (curr_Q_pixel_v == 0)
					{
						Dmin += curr_pixel_v;
						DminCnt++;
					}
				}

				if (DmaxCnt > DminCnt)
					BW_Ratio = (double)DminCnt / (double)DmaxCnt;
				else
					BW_Ratio = (double)DmaxCnt / (double)DminCnt;


				if (DmaxCnt != 0 && DminCnt != 0)
				{
					pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio[x] = BW_Ratio;

					Dmax = Dmax / (DmaxCnt);
					Dmin = Dmin / (DminCnt);

					double Ri = (double)(m_White_Ref_Value - m_Black_Ref_Value) / (double)(m_White_Ref_Value + m_Black_Ref_Value);
					double Ro = (double)(Dmax - Dmin) / (double)(Dmax + Dmin);
					double MTF = Ro / Ri;

					oldSFRValPos = cvPoint(x, (int)(MTF*100.0));

					pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x] = (MTF*100.0);
				}
			}
		}

		double sum_square_x = 0, sum_x = 0, sum_xy = 0, sum_y = 0, sum_1 = 0;
		double a, b;
		double inverse_x11, inverse_x12, inverse_x21, inverse_x22, inverse_param;

		int SFR_range = abs(pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x - pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x);

		for (int x = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x + ((double)SFR_range*range_Factor); x<pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x + ((double)SFR_range*(1 - range_Factor)); x++)
		{
			if ((pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x]) > Limit_SFRValue && pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x] < 150)
			{
				sum_square_x += x*x;
				sum_x += x;
				sum_1 += 1;
				sum_y += pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x];
				sum_xy += x * pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x];
			}
		}

		inverse_param = 1.0 / ((sum_square_x*sum_1) - (sum_x*sum_x));
		inverse_x11 = inverse_param*sum_1;
		inverse_x12 = (inverse_param*sum_x) * -1;
		inverse_x21 = (inverse_param*sum_x) * -1;
		inverse_x22 = (inverse_param*sum_square_x);

		a = (inverse_x11*sum_xy) + (inverse_x12*sum_y);
		b = (inverse_x21*sum_xy) + (inverse_x22*sum_y);

		for (int x = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x; x < pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x; x++)
		{
			double y = a*x + b;

			if (x > pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x)
			{
				double curr_y, before_y;

				curr_y = y;
				before_y = pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[x - 1];

				if (curr_y >= before_y)
				{
					y = before_y - 0.1;
				}
			}

			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[x] = y;
		}

		for (int x = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x; x < pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x; x++)
		{
			if (100.0 - (pstEIAJ->stEIAJOp.rectData[iNum].i_MtfRatio) < pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[x])
			{
				result_Line = x;
				Detected_Flag = TRUE;
			}
		}

		if (Detected_Flag == FALSE)
			result_Line = 1;

		delete[] Q_BWImage;

		cvReleaseImage(&tempImage);


	}
	else if (icheck_mode == Resol_Mode_Bottom_Top)//세로영역	// 좌->우로 체크
	{
		IplImage *tempImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
		BYTE *Q_BWImage = new BYTE[iHeight * iWidth];


		for (int y = 0; y < iHeight; y++)
		for (int x = 0; x < iWidth; x++)
			tempImage->imageData[y * tempImage->widthStep + x] = BWImage[y * (iWidth)+x];

		cvCanny(tempImage, tempImage, 50, 100);

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value;

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value;

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio;


		pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value = new double[iWidth * iHeight];
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value = new double[iWidth * iHeight];
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio = new double[iWidth * iHeight];

		bool weight_line_flag = FALSE;

		for (int y = iHeight - 1; y > -1; y--)
		{
			int subCnt = 0;
			int StartY_pos = -1, EndY_pos = -1;

			for (int x = 0; x < iWidth; x++)
			{
				curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v > m_White_Ref_Value)
					BWImage[(y)*(iWidth)+(x)] = m_White_Ref_Value;
				if (curr_pixel_v < m_Black_Ref_Value)
					BWImage[(y)*(iWidth)+(x)] = m_Black_Ref_Value;

				BWImage[(y)*(iWidth)+(x)] = curr_pixel_v;

				// 버그 !!
				unsigned int EdgeValue = (BYTE)tempImage->imageData[y * tempImage->widthStep + x];

				if (curr_pixel_v > EdgeValue)
					Q_BWImage[(y)*(iWidth)+(x)] = 255;
				else
					Q_BWImage[(y)*(iWidth)+(x)] = 0;
			}
		}

		CvPoint oldSFRValPos = cvPoint(0, 0);

		for (int i = 0; i < iHeight * iWidth; i++){
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[i] = 0;
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[i] = 0;
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio[i] = 0;
		}

		pstEIAJ->stEIAJOp.rectData[iNum].SFR_Width = 100;
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_Height = iHeight;


		for (int y = iHeight - 1; y > -1; y--)
		{
			int subCnt = 0;
			int StartY_pos = -1, EndY_pos = -1;

			for (int x = 0; x < iWidth; x++)
			{
				if (Q_BWImage[(y)*(iWidth)+(x)] == 0)
				{
					StartY_pos = x;
					break;
				}
			}
			for (int x = iWidth - 1; x > 0; x--)
			{
				if (Q_BWImage[(y)*(iWidth)+(x)] == 0)
				{
					EndY_pos = x;
					break;
				}
			}


			if (StartY_pos != -1 && EndY_pos != -1 && (EndY_pos - StartY_pos) != 0)
			{
				UINT Dmax = 0, Dmin = 255;
				int meanVal = 0;

				for (int x = StartY_pos + 1; x < EndY_pos - 1; x++)
				{
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_pixel_v > Dmax)
						Dmax = curr_pixel_v;
					if (curr_pixel_v < Dmin)
						Dmin = curr_pixel_v;				
				}

				for (int x = StartY_pos + 1; x < EndY_pos - 1; x++)
				{
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_pixel_v >(Dmax + Dmin) / 2.0)
						Q_BWImage[(y)*(iWidth)+(x)] = 255;
					else
						Q_BWImage[(y)*(iWidth)+(x)] = 0;
				}

				for (int x = 0; x < StartY_pos; x++)
					Q_BWImage[(y)*(iWidth)+(x)] = 127;
				for (int x = iWidth - 1; x > EndY_pos; x--)
					Q_BWImage[(y)*(iWidth)+(x)] = 127;

				Dmax = 0;
				Dmin = 0;
				int DmaxCnt = 0;
				int DminCnt = 0;

				for (int x = StartY_pos; x < EndY_pos; x++)
				{
					unsigned int curr_Q_pixel_v = Q_BWImage[(y)*(iWidth)+(x)];
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_Q_pixel_v == 255)
					{
						Dmax += curr_pixel_v;
						DmaxCnt++;
					}
					else if (curr_Q_pixel_v == 0)
					{
						Dmin += curr_pixel_v;
						DminCnt++;
					}
				}

				if (DmaxCnt > DminCnt)
					BW_Ratio = (double)DminCnt / (double)DmaxCnt;
				else
					BW_Ratio = (double)DmaxCnt / (double)DminCnt;

				if (DmaxCnt != 0 && DminCnt != 0)
				{
					pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio[y] = BW_Ratio;

					Dmax = Dmax / (DmaxCnt);
					Dmin = Dmin / (DminCnt);

					double Ri = (double)(m_White_Ref_Value - m_Black_Ref_Value) / (double)(m_White_Ref_Value + m_Black_Ref_Value);
					double Ro = (double)(Dmax - Dmin) / (double)(Dmax + Dmin);
					double MTF = Ro / Ri;

					oldSFRValPos = cvPoint((int)(MTF*100.0), y);

					pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y] = (MTF*100.0);
				}
			}
		}

		double sum_square_x = 0, sum_x = 0, sum_xy = 0, sum_y = 0, sum_1 = 0;
		double a, b;
		double inverse_x11, inverse_x12, inverse_x21, inverse_x22, inverse_param;

		int SFR_range = abs(pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y - pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y);

		for (int y = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y; y > pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y - ((double)SFR_range*0.8); y--)	//약 75% 지점까지 감안한다..
		{
			if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y] > Limit_SFRValue && pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y] < 150)
			{
				sum_square_x += y*y;
				sum_x += y;
				sum_1 += 1;
				sum_y += pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y];
				sum_xy += y * pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y];
			}
		}

		inverse_param = 1.0 / ((sum_square_x*sum_1) - (sum_x*sum_x));
		inverse_x11 = inverse_param*sum_1;
		inverse_x12 = (inverse_param*sum_x) * -1;
		inverse_x21 = (inverse_param*sum_x) * -1;
		inverse_x22 = (inverse_param*sum_square_x);

		a = (inverse_x11*sum_xy) + (inverse_x12*sum_y);
		b = (inverse_x21*sum_xy) + (inverse_x22*sum_y);

		for (int y = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y; y > pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y; y--)
		{
			double x = a*y + b;

			if (y < pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y)
			{
				double curr_x, before_x;

				curr_x = x;
				before_x = pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[y + 1];

				if (curr_x >= before_x)
				{
					x = before_x - 0.1;
				}
			}

			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[y] = x;
		}

		for (int y = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y; y > pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y; y--)
		{
			if (100.0 - (pstEIAJ->stEIAJOp.rectData[iNum].i_MtfRatio) < pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[y])
			{
				result_Line = y;
				Detected_Flag = TRUE;
			}
		}

		if (Detected_Flag == FALSE)
			result_Line = iHeight - 1;

		delete[] Q_BWImage;

		cvReleaseImage(&tempImage);
	}


	return result_Line;
}

//=============================================================================
// Method		: GetCheckLine_RL_UD_using_Sharpness
// Access		: public  
// Returns		: int
// Parameter	: ST_LT_TI_EIAJ * pstEIAJ
// Parameter	: BYTE * BWImage
// Parameter	: int icheck_mode
// Parameter	: int iWidth
// Parameter	: int iHeight
// Parameter	: int iNum
// Parameter	: int iloopcnt
// Qualifier	:
// Last Update	: 2017/1/16 - 15:16
// Desc.		:
//=============================================================================
int CTI_EIAJ::GetCheckLine_RL_UD_using_Sharpness(ST_LT_TI_EIAJ *pstEIAJ, BYTE *BWImage, int icheck_mode, int iWidth, int iHeight, int iNum, int iloopcnt)
{
	int max_v = 0, min_v = 255, loc_index = 0;
	int unsigned curr_pixel_v/*, next_pixel_v, direction*/;
	bool Detected_Flag = FALSE;
	int result_Line;
	int avg_Deviation = 0/*, sub_Y*/;
	double BW_Ratio = 0.0;
	int black_line_cnt = 0;

	double range_Factor = 0.05;
	int Limit_SFRValue = 10; // 퍼센트 수치


	if (icheck_mode == Resol_Mode_Right_Left)//가로 영역	// 상->하로 체크
	{
		IplImage *tempImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
		BYTE *Q_BWImage = new BYTE[iWidth * iHeight];

		//kdy
		for (int y = 0; y < iHeight; y++)
		{
			for (int x = 0; x < iWidth; x++)
			{
				tempImage->imageData[y * tempImage->widthStep + x] = BWImage[y * (iWidth)+x];
			}
		}

		cvCanny(tempImage, tempImage, 50, 100);

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value;

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value;

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio;

		pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value = new double[iWidth * iHeight];
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value = new double[iWidth * iHeight];
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio = new double[iWidth * iHeight];

		bool weight_line_flag = FALSE;

		for (int x = 0; x < iWidth; x++)
		{
			for (int y = 0; y < iHeight - 1; y++)
			{
				curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v > m_White_Ref_Value)
					curr_pixel_v = m_White_Ref_Value;
				if (curr_pixel_v < m_Black_Ref_Value)
					curr_pixel_v = m_Black_Ref_Value;

				BWImage[(y)*(iWidth)+(x)] = curr_pixel_v;

				// 버그 !!
				unsigned int EdgeValue = (BYTE)tempImage->imageData[y * tempImage->widthStep + x];

				if (curr_pixel_v > EdgeValue)
					Q_BWImage[(y)*(iWidth)+(x)] = 255;
				else
					Q_BWImage[(y)*(iWidth)+(x)] = 0;
			}
		}

		CvPoint oldSFRValPos = cvPoint(0, 0);

		for (int i = 0; i < iHeight * iWidth; i++){
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[i] = 0;
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[i] = 0;
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio[i] = 0;
		}
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_Height = 100;
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_Width = iWidth;

		for (int x = 0; x < iWidth; x++)
		{
			avg_Deviation = 0;

			int subCnt = 0;
			int StartY_pos = -1, EndY_pos = -1;

			for (int y = 0; y < iHeight; y++)
			{
				if (Q_BWImage[(y)*(iWidth)+(x)] == 0)
				{
					StartY_pos = y;
					break;
				}
			}
			for (int y = iHeight - 1; y > 0; y--)
			{
				if (Q_BWImage[(y)*(iWidth)+(x)] == 0)
				{
					EndY_pos = y;
					break;
				}
			}

			if (StartY_pos != -1 && EndY_pos != -1 && (EndY_pos - StartY_pos) != 0)
			{
				UINT Dmax = 0, Dmin = 255;
				int meanVal = 0;

				for (int y = StartY_pos + 1; y < EndY_pos - 1; y++)
				{
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_pixel_v > Dmax)
						Dmax = curr_pixel_v;
					if (curr_pixel_v < Dmin)
						Dmin = curr_pixel_v;			
				}

				for (int y = StartY_pos + 1; y < EndY_pos - 1; y++)
				{
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_pixel_v >(Dmax + Dmin) / 2.0)
						Q_BWImage[(y)*(iWidth)+(x)] = 255;
					else
						Q_BWImage[(y)*(iWidth)+(x)] = 0;
				}

				for (int y = 0; y < StartY_pos; y++)
					Q_BWImage[(y)*(iWidth)+(x)] = 127;
				for (int y = iHeight - 1; y > EndY_pos; y--)
					Q_BWImage[(y)*(iWidth)+(x)] = 127;

				Dmax = 0;
				Dmin = 0;
				int DmaxCnt = 0;
				int DminCnt = 0;

				int temp_Start_y = 0, temp_End_y = 0;

				for (int y = StartY_pos; y < EndY_pos; y++)
				{
					unsigned int curr_Q_pixel_v = Q_BWImage[(y)*(iWidth)+(x)];
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_Q_pixel_v == 255)
					{
						Dmax += curr_pixel_v;
						DmaxCnt++;
					}
					else if (curr_Q_pixel_v == 0)
					{
						Dmin += curr_pixel_v;
						DminCnt++;
					}
				}

				if (DmaxCnt > DminCnt)
					BW_Ratio = (double)DminCnt / (double)DmaxCnt;
				else
					BW_Ratio = (double)DmaxCnt / (double)DminCnt;

				if (DmaxCnt != 0 && DminCnt != 0)
				{
					pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio[x] = BW_Ratio;

					Dmax = Dmax / (DmaxCnt);
					Dmin = Dmin / (DminCnt);

					double Ri = (double)(m_White_Ref_Value - m_Black_Ref_Value) / (double)(m_White_Ref_Value + m_Black_Ref_Value);
					double Ro = (double)(Dmax - Dmin) / (double)(Dmax + Dmin);
					double MTF = Ro / Ri;

					oldSFRValPos = cvPoint(x, (int)(MTF*100.0));

					pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x] = (MTF*100.0);
				}
			}
		}

		double sum_square_x = 0, sum_x = 0, sum_xy = 0, sum_y = 0, sum_1 = 0;
		double a, b;
		double inverse_x11, inverse_x12, inverse_x21, inverse_x22, inverse_param;

		int SFR_range = abs(pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x - pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x);

		for (int x = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x - ((double)SFR_range*range_Factor); x > pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x - ((double)SFR_range*(1 - range_Factor)); x--)
		{
			if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x] > Limit_SFRValue && (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x]<150))
			{
				sum_square_x += x*x;
				sum_x += x;
				sum_1 += 1;
				sum_y += pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x];
				sum_xy += x * pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[x];
			}
		}

		inverse_param = 1.0 / ((sum_square_x*sum_1) - (sum_x*sum_x));
		inverse_x11 = inverse_param*sum_1;
		inverse_x12 = (inverse_param*sum_x) * -1;
		inverse_x21 = (inverse_param*sum_x) * -1;
		inverse_x22 = (inverse_param*sum_square_x);

		a = (inverse_x11*sum_xy) + (inverse_x12*sum_y);
		b = (inverse_x21*sum_xy) + (inverse_x22*sum_y);

		for (int x = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x; x > pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x; x--)
		{
			double y = a*x + b;

			if (x < pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x)
			{
				double curr_y, before_y;

				curr_y = y;
				before_y = pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[x + 1];

				if (curr_y >= before_y)
				{
					y = before_y - 0.1;
				}
			}

			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[x] = y;
		}

		for (int x = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.x; x > pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.x; x--)
		{
			if (100.0 - (pstEIAJ->stEIAJOp.rectData[iNum].i_MtfRatio) < pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[x])
			{
				result_Line = x;
				Detected_Flag = TRUE;
			}
		}

		if (Detected_Flag == FALSE)
			result_Line = iWidth - 1;

		delete[] Q_BWImage;
		cvReleaseImage(&tempImage);

	}
	else if (icheck_mode == Resol_Mode_Top_Bottom)//상하영역	// 좌->우로 체크
	{
		IplImage *tempImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
		BYTE *Q_BWImage = new BYTE[iHeight * iWidth];


		for (int y = 0; y < iHeight; y++)
		for (int x = 0; x < iWidth; x++)
			tempImage->imageData[y * tempImage->widthStep + x] = BWImage[y * (iWidth)+x];

		cvCanny(tempImage, tempImage, 50, 100);

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value;

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value;

		if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio;


		pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value = new double[iHeight * iWidth];
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value = new double[iHeight * iWidth];
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio = new double[iHeight * iWidth];

		bool weight_line_flag = FALSE;

		for (int y = 0; y < iHeight; y++)
		{
			int subCnt = 0;
			int StartY_pos = -1, EndY_pos = -1;

			for (int x = 0; x < iWidth; x++)
			{
				curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

				if (curr_pixel_v > m_White_Ref_Value)
					BWImage[(y)*(iWidth)+(x)] = m_White_Ref_Value;
				if (curr_pixel_v < m_Black_Ref_Value)
					BWImage[(y)*(iWidth)+(x)] = m_Black_Ref_Value;

				BWImage[(y)*(iWidth)+(x)] = curr_pixel_v;

				// 버그 !!
				unsigned int EdgeValue = (BYTE)tempImage->imageData[y * tempImage->widthStep + x];

				if (curr_pixel_v > EdgeValue)
					Q_BWImage[(y)*(iWidth)+(x)] = 255;
				else
					Q_BWImage[(y)*(iWidth)+(x)] = 0;
			}
		}

		CvPoint oldSFRValPos = cvPoint(0, 0);

		for (int i = 0; i < iHeight * iWidth; i++){
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[i] = 0;
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[i] = 0;
			pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio[i] = 0;
		}
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_Height = iHeight;
		pstEIAJ->stEIAJOp.rectData[iNum].SFR_Width = 100;

		for (int y = 0; y < iHeight; y++)
		{
			int subCnt = 0;
			int StartY_pos = -1, EndY_pos = -1;

			for (int x = 0; x < iWidth; x++)
			{
				if (Q_BWImage[(y)*(iWidth)+(x)] == 0)
				{
					StartY_pos = x;
					break;
				}
			}
			for (int x = iWidth - 1; x > 0; x--)
			{
				if (Q_BWImage[(y)*(iWidth)+(x)] == 0)
				{
					EndY_pos = x;
					break;
				}
			}


			if (StartY_pos != -1 && EndY_pos != -1 && (EndY_pos - StartY_pos) != 0)
			{
				UINT Dmax = 0, Dmin = 255;
				int meanVal = 0;

				for (int x = StartY_pos + 1; x < EndY_pos - 1; x++)
				{
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_pixel_v > Dmax)
						Dmax = curr_pixel_v;
					if (curr_pixel_v < Dmin)
						Dmin = curr_pixel_v;					
				}

				for (int x = StartY_pos + 1; x < EndY_pos - 1; x++)
				{
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_pixel_v >(Dmax + Dmin) / 2.0)
						Q_BWImage[(y)*(iWidth)+(x)] = 255;
					else
						Q_BWImage[(y)*(iWidth)+(x)] = 0;
				}

				for (int x = 0; x < StartY_pos; x++)
					Q_BWImage[(y)*(iWidth)+(x)] = 127;

				for (int x = iWidth - 1; x > EndY_pos; x--)
					Q_BWImage[(y)*(iWidth)+(x)] = 127;

				Dmax = 0;
				Dmin = 0;
				int DmaxCnt = 0;
				int DminCnt = 0;

				for (int x = StartY_pos; x < EndY_pos; x++)
				{
					unsigned int curr_Q_pixel_v = Q_BWImage[(y)*(iWidth)+(x)];
					curr_pixel_v = BWImage[(y)*(iWidth)+(x)];

					if (curr_Q_pixel_v == 255)
					{
						Dmax += curr_pixel_v;
						DmaxCnt++;
					}
					else if (curr_Q_pixel_v == 0)
					{
						Dmin += curr_pixel_v;
						DminCnt++;
					}
				}

				if (DmaxCnt > DminCnt)
					BW_Ratio = (double)DminCnt / (double)DmaxCnt;
				else
					BW_Ratio = (double)DmaxCnt / (double)DminCnt;

				if (DmaxCnt != 0 && DminCnt != 0)
				{
					pstEIAJ->stEIAJOp.rectData[iNum].SFR_BWRatio[y] = BW_Ratio;

					Dmax = Dmax / (DmaxCnt);
					Dmin = Dmin / (DminCnt);

					double Ri = (double)(m_White_Ref_Value - m_Black_Ref_Value) / (double)(m_White_Ref_Value + m_Black_Ref_Value);
					double Ro = (double)(Dmax - Dmin) / (double)(Dmax + Dmin);
					double MTF = Ro / Ri;

					oldSFRValPos = cvPoint((int)(MTF*100.0), y);

					pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y] = (MTF*100.0);
				}
			}
		}

		double sum_square_x = 0, sum_x = 0, sum_xy = 0, sum_y = 0, sum_1 = 0;
		double a, b;
		double inverse_x11, inverse_x12, inverse_x21, inverse_x22, inverse_param;

		int SFR_range = abs(pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y - pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y);

		for (int y = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y + ((double)SFR_range*range_Factor); y < pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y + ((double)SFR_range*(1 - range_Factor)); y++)	//약75% 지점까지감안한다..
		{
			
			if (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y] > Limit_SFRValue && (pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y] < 300))
			{
				sum_square_x += y*y;
				sum_x += y;
				sum_1 += 1;
				sum_y += pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y];
				sum_xy += y * pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_Value[y];
			}
		}

		inverse_param = 1.0 / ((sum_square_x*sum_1) - (sum_x*sum_x));
		inverse_x11 = inverse_param*sum_1;
		inverse_x12 = (inverse_param*sum_x) * -1;
		inverse_x21 = (inverse_param*sum_x) * -1;
		inverse_x22 = (inverse_param*sum_square_x);

		a = (inverse_x11*sum_xy) + (inverse_x12*sum_y);
		b = (inverse_x21*sum_xy) + (inverse_x22*sum_y);

		for (int y = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y; y < pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y; y++)
		{
			double x = a*y + b;

			if (y > pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y)
			{
				double curr_x, before_x;

				curr_x = x;
				before_x = pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[y - 1];

				if (curr_x >= before_x)
				{
					x = before_x - 0.1;
				}
			}

			pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[y] = x;
		}

		for (int y = pstEIAJ->stEIAJOp.rectData[iNum].PatternStartPos.y; y < pstEIAJ->stEIAJOp.rectData[iNum].PatternEndPos.y; y++)
		{
			if (100.0 - (pstEIAJ->stEIAJOp.rectData[iNum].i_MtfRatio) < pstEIAJ->stEIAJOp.rectData[iNum].SFR_MTF_LineFitting_Value[y])
			{
				result_Line = y;
				Detected_Flag = TRUE;
			}
		}

		if (Detected_Flag == FALSE)
			result_Line = 1;

		delete[] Q_BWImage;
		cvReleaseImage(&tempImage);
	}



	return result_Line;
}

//=============================================================================
// Method		: DeleteMemory
// Access		: public  
// Returns		: void
// Parameter	: ST_LT_TI_EIAJ * pstEIAJ
// Qualifier	:
// Last Update	: 2017/8/13 - 15:22
// Desc.		:
//=============================================================================
void CTI_EIAJ::DeleteMemory(ST_LT_TI_EIAJ *pstEIAJ)
{
	if (pstEIAJ == NULL)
	{
		return;
	}
	for (UINT nIdx = 0; nIdx < ReOp_ItemNum; nIdx++)
	{
		if (pstEIAJ->stEIAJOp.rectData[nIdx].SFR_MTF_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[nIdx].SFR_MTF_Value;

		if (pstEIAJ->stEIAJOp.rectData[nIdx].SFR_MTF_LineFitting_Value != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[nIdx].SFR_MTF_LineFitting_Value;

		if (pstEIAJ->stEIAJOp.rectData[nIdx].SFR_BWRatio != NULL)
			delete[] pstEIAJ->stEIAJOp.rectData[nIdx].SFR_BWRatio;
	}
}
