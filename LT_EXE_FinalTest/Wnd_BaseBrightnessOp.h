﻿#ifndef Wnd_BaseBrightnessOp_h__
#define Wnd_BaseBrightnessOp_h__

#pragma once

#include "Wnd_BrightnessOp.h"

// CWnd_BaseBrightnessOp

class CWnd_BaseBrightnessOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseBrightnessOp)

public:
	CWnd_BaseBrightnessOp();
	virtual ~CWnd_BaseBrightnessOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_BrightnessOp m_wndBrightnessOp[TICnt_Brightness];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseBrightnessOp_h__
