﻿#ifndef List_ColorResult_h__
#define List_ColorResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_ColorResult
{
	ColorResult_Object = 0,
	
	ColorResult_ValueR,
	ColorResult_MinSpecR,
	ColorResult_MaxSpecR,

	ColorResult_ValueG,
	ColorResult_MinSpecG,
	ColorResult_MaxSpecG,

	ColorResult_ValueB,
	ColorResult_MinSpecB,
	ColorResult_MaxSpecB,

	ColorResult_Result,
	ColorResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_ColorResult[] =
{
	_T(""),
	_T("Red"),
	_T("Min"),
	_T("Max"),
	_T("Green"),
	_T("Min"),
	_T("Max"),
	_T("Blue"),
	_T("Min"),
	_T("Max"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_ColorResult
{
	ColorResult_ItemNum = 4,
};

static LPCTSTR	g_lpszItem_ColorResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_ColorResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ColorResult[] =
{
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};
// List_ColorInfo

class CList_ColorResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ColorResult)

public:
	CList_ColorResult();
	virtual ~CList_ColorResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Color(ST_LT_TI_Color* pstColor)
	{
		if (pstColor == NULL)
			return;

		m_pstColor = pstColor;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Color*  m_pstColor;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_ColorInfo_h__
