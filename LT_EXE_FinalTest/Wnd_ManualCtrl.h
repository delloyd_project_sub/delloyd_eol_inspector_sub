﻿//*****************************************************************************
// Filename	: 	Wnd_ManualCtrl.h
// Created	:	2017/1/4 - 13:33
// Modified	:	2017/1/4 - 13:33
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_ManualCtrl_h__
#define Wnd_ManualCtrl_h__

#pragma once

#include "Def_TestDevice.h"
// CWnd_ManualCtrl

static LPCTSTR	g_szManual_Button[] =
{
	_T("Camera Power On"),
	_T("Camera Power Off"),
//	_T("Camera LED On"),
//	_T("Camera LED Off"),
	_T("Image Save"),
//	_T("Overlay Image Save"),
//	_T("Initialize"),
	_T("Image Load"),
	_T("LED Light On"),
	_T("LED Light Off"),
	_T("IR Light On"),
	_T("IR Light Off"),
	NULL
};

enum enManualCtrlBtn
{
	BT_MCTRL_POWER_ON,
	BT_MCTRL_POWER_OFF,
// 	BT_MCTRL_IRLED_ON,
// 	BT_MCTRL_IRLED_OFF,
	BT_MCTRL_IMAGE_SAVE,
//	BT_MCTRL_PIC_IMAGE_SAVE,
//	BT_MCTRL_INITIALIZE,
	BT_MCTRL_IMAGE_LOAD,
	BT_MCTRL_LED_ON,
	BT_MCTRL_LED_OFF,
	BT_MCTRL_IR_ON,
	BT_MCTRL_IR_OFF,
	BT_MCTRL_MAXNUM,
};

class CWnd_ManualCtrl : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ManualCtrl)

public:
	CWnd_ManualCtrl();
	virtual ~CWnd_ManualCtrl();


	void SetCtrlID(UINT nWM_ID)
	{
		m_wm_ManualID = nWM_ID;
	}

protected:

	UINT m_wm_ManualID = NULL;
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnRangeCmds		(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	void			ClickOut		(UINT nID);

	DECLARE_MESSAGE_MAP()

	
	ST_Device*		m_pDevice;

	CVGStatic		m_st_Name;

	CFont			m_font;
	CMFCButton		m_Btn_Item[BT_MCTRL_MAXNUM];

public:


	void SetPtr_Device(__in ST_Device* pDevice)
	{
		m_pDevice = pDevice;
	};
	void ImageLoadMode(bool bMode);
};
#endif // Wnd_ManualCtrl_h__


