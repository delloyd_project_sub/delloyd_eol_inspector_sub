﻿// List_FFTOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_FFTOp.h"

#define IFFTOp_ED_CELLEDIT		5001

// CList_FFTOp

IMPLEMENT_DYNAMIC(CList_FFTOp, CListCtrl)

CList_FFTOp::CList_FFTOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	m_pstFFT = NULL;
}

CList_FFTOp::~CList_FFTOp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_FFTOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_FFTOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_FFTOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IFFTOp_ED_CELLEDIT, &CList_FFTOp::OnEnKillFocusEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_FFTOp 메시지 처리기입니다.
int CList_FFTOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER, CRect(0, 0, 0, 0), this, IFFTOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:36
// Desc.		:
//=============================================================================
void CList_FFTOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[FFTOp_MaxCol] = { 0, };
	int iColDivide	= 0;
	int iUnitWidth	= 0;
	int iMisc		= 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = FFTOp_Min; nCol < FFTOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_FFTOp[FFTOp_Channel]) / (FFTOp_MaxCol - FFTOp_Min);
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:55
// Desc.		:
//=============================================================================
BOOL CList_FFTOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:56
// Desc.		:
//=============================================================================
void CList_FFTOp::InitHeader()
{
	for (UINT nCol = 0; nCol < FFTOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_FFTOp[nCol], iListAglin_FFTOp[nCol], iHeaderWidth_FFTOp[nCol]);
	}

	for (UINT nCol = 0; nCol < FFTOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_FFTOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:56
// Desc.		:
//=============================================================================
void CList_FFTOp::InsertFullData()
{
	if (m_pstFFT == NULL)
		return;

	DeleteAllItems();

	for (UINT nIndx = 0; nIndx < FFTOp_ItemNum; nIndx++)
	{
		InsertItem(nIndx, _T(""));
		SetRectRow(nIndx);
	}

}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_FFTOp::SetRectRow(UINT nRow)
{
	CString strText;

	if (m_pstFFT->stFFTOpt.dSpecMin[nRow] >= m_pstFFT->stFFTOpt.dSpecMax[nRow])
	{
		m_pstFFT->stFFTOpt.dSpecMax[nRow] = m_pstFFT->stFFTOpt.dSpecMin[nRow] + 1;
	}

	strText.Format(_T("%s"), g_lpszItem_FFTOp[nRow]);
	SetItemText(nRow, FFTOp_Channel, strText);

	strText.Format(_T("%.2f"), m_pstFFT->stFFTOpt.dSpecMin[nRow]);
	SetItemText(nRow, FFTOp_Min, strText);

	strText.Format(_T("%.2f"), m_pstFFT->stFFTOpt.dSpecMax[nRow]);
	SetItemText(nRow, FFTOp_Max, strText);

	strText.Format(_T("%.2f"), m_pstFFT->stFFTOpt.dbOffset[nRow]);
	SetItemText(nRow, FFTOp_Offset, strText);

	strText.Format(_T("%d"), m_pstFFT->stFFTOpt.nTestTime[nRow]);
	SetItemText(nRow, FFTOp_TestTime, strText);
 }

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_FFTOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_FFTOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem <5 && pNMItemActivate->iSubItem >0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_FFTOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == FFTOp_TestTime)
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}
	else
	{
		UpdateCellData_double(m_nEditRow, m_nEditCol, _ttof(strText));
	}


	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
BOOL CList_FFTOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	CString str;
	
	switch (nCol)
	{
	case FFTOp_TestTime:
		m_pstFFT->stFFTOpt.nTestTime[nRow] = iValue;
		break;

	default:
		break;
	}
 
	str.Format(_T("%d"), iValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);
 
	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_FFTOp::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	switch (nCol)
	{
	case FFTOp_Offset:
		m_pstFFT->stFFTOpt.dbOffset[nRow] = dValue;
		break;

	case FFTOp_Min:
		m_pstFFT->stFFTOpt.dSpecMin[nRow] = dValue;
		break;

	case FFTOp_Max:
		m_pstFFT->stFFTOpt.dSpecMax[nRow] = dValue;
		break;

	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Parameter	: ST_LT_TI_Current & stCurrent
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
void CList_FFTOp::GetCellData()
{
	if (m_pstFFT == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_FFTOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue	  = _ttoi(strText);
		double dbValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + zDelta / 120;
			dbValue = dbValue + zDelta / 120 * 0.1;
		}
		else
		{
			if (0 < iValue)
			{
				iValue = iValue + zDelta / 120;
			}

			if (0 < dbValue)
			{
				dbValue = dbValue + zDelta / 120 * 0.1;
			}
		}

		if (m_nEditCol == FFTOp_TestTime)
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
		else
		{
			UpdateCellData_double(m_nEditRow, m_nEditCol, dbValue);
		}
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
