﻿#ifndef List_OperModeOp_h__
#define List_OperModeOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_OperModeOp
{
	OperOp_FPS,
	OperOp_Min,
	OperOp_Max,
	OperOp_Offset,
	OperOp_MaxCol,
};

// 헤더
static const LPCTSTR g_lpszHeader_OperOp[] =
{
	_T("FPS"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Offset"),
	NULL
};

typedef enum enListItemNum_
{
	OperOp_Site1,
	OperOp_Site2,
	OperOp_ItemNum,
};

static const LPCTSTR g_lpszItem_OperOp[] =
{
	_T("15 Fps"),
	_T("30 Fps"),
	NULL
};

const int	iListAglin_OperOp[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_OperOp[] =
{
	100,
	130,
	130,
	130,
};
// List_OperModeInfo

class CList_OperModeOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_OperModeOp)

public:
	CList_OperModeOp();
	virtual ~CList_OperModeOp();

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);
	void GetCellData();

	void SetPtr_OperMode(ST_LT_TI_OperationMode *pstOperMode)
	{
		if (pstOperMode == NULL)
			return;

		m_pstOperMode = pstOperMode;
	};

protected:

	ST_LT_TI_OperationMode*	m_pstOperMode;

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL		UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL		UpdateCellData_double	(UINT nRow, UINT nCol, double dValue);

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick			(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk			(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnEnKillFocusEdit	();

	afx_msg BOOL	OnMouseWheel		(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()
};

#endif // List_OperModeInfo_h__
