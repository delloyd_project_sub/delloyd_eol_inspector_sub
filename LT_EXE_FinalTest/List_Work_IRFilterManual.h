﻿#ifndef List_Work_IRFilterManual_h__
#define List_Work_IRFilterManual_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_IRFilterManual_Worklist
{
	IRFilterManual_W_Recode,
	IRFilterManual_W_Time,
	IRFilterManual_W_Equipment,
	IRFilterManual_W_Model,
	IRFilterManual_W_SWVersion,
	IRFilterManual_W_LOTNum,
	IRFilterManual_W_Barcode,
	//IRFilterManual_W_Operator,
	IRFilterManual_W_Result,
	IRFilterManual_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_IRFilterManual_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	NULL
};

const int	iListAglin_IRFilterManual_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_IRFilterManual_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_IRFilterManual

class CList_Work_IRFilterManual : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_IRFilterManual)

public:
	CList_Work_IRFilterManual();
	virtual ~CList_Work_IRFilterManual();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_IRFilterManual_h__
