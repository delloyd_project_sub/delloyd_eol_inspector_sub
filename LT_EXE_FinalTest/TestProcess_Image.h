#pragma once

#include "Def_DataStruct.h"
#include "TI_CenterPoint.h"
#include "TI_SFR.h"
#include "TI_Color.h"
#include "TI_Particle.h"
#include "TI_Brightness.h"
#include "TI_EIAJ.h"
#include "TI_PatternNoise.h"
#include "TI_Reverse.h"
#include "TI_DefectPixel.h"


class CTestProcess_Image
{
public:
	CTestProcess_Image();
	~CTestProcess_Image();

	// 화각 검사
	void OnTestProcessAngle(__in IplImage* pFrameImage, __in ST_Angle_Opt* pstOpt, __out ST_Angle_Result &stResult);

	// 중심점 검사
	void OnTestProcessCenterPoint(__in IplImage* pFrameImage, __in ST_CenterPoint_Opt* pstOpt, __out ST_CenterPoint_Result &stResult);

	// 컬러 검사
	void OnTestProcessColor(__in IplImage* pFrameImage, __in ST_CenterPoint_Opt* pstCenterOpt, __in ST_Color_Opt* pstOpt, __out ST_Color_Result &stResult);

	// 이물 검사
	void OnTestProcessParticle(__in IplImage* pFrameImage, __in ST_Particle_Opt* pstOpt, __out ST_Particle_Result &stResult);

	void OnTestProcessDefectPixel(__in IplImage * pFrameImage, __in ST_DefectPixel_Opt * pstOpt, __out  ST_DefectPixel_Data & stResult);

	// 광량비 검사
	void OnTestProcessBrightness(__in IplImage* pFrameImage, __in ST_Brightness_Opt* pstOpt, __out ST_Brightness_Result &stResult);

	// 로테이트 검사
	void OnTestProcessRotate(__in IplImage* pFrameImage, __in ST_CenterPoint_Opt* pstCenterOpt, __in ST_Rotate_Opt* pstOpt, __out ST_Rotate_Result &stResult);

	// SFR 검사
	void OnTestProcessSFR(__in IplImage* pFrameImage, __in ST_SFR_Opt* pstOpt, __out ST_SFR_Result &stResult);

	// EIAJ 검사
	void OnTestProcessEIAJ(__in IplImage* pFrameImage, ST_LT_TI_EIAJ *pstEIAJ);

	// Reverse 검사
	void OnTestProcessReverse(__in IplImage* pFrameImage, __in ST_Reverse_Opt* pstOpt, __out ST_Reverse_Result &stResult);
	void OnTestProcessReverseColor(__in IplImage* pFrameImage, __in ST_Reverse_Opt* pstOpt, __out ST_Reverse_Result &stResult);

	// Pattern Noise 검사
	void OnTestProcessPatternNoise(__in IplImage* pFrameImage, __in ST_PatternNoise_Opt* pstOpt, __out ST_PatternNoise_Result &stResult);

	// IR Filter 검사
	void OnTestProcessIRFilter(__in IplImage* pFrameImage, __in ST_IRFilter_Opt* pstOpt, __out ST_IRFilter_Result &stResult);

	// SFR 오브젝트 위치 보정
	void OnTestProcessSFRROI_Auto(__in IplImage* pFrameImage, __inout ST_SFR_Opt* pstOpt);

	// COLOR 오브젝트 위치 보정
	void OnTestProcessColorROI_Auto(__in IplImage* pFrameImage, __inout ST_Color_Opt* pstOpt);
	
	void OnTestProcessRotateROI_Auto(__in IplImage* pFrameImage, __inout ST_Rotate_Opt* pstOpt);
	// Reverse 오브젝트 위치 보정
	void OnTestProcessReverseROI_Auto(__in IplImage* pFrameImage, __inout ST_Reverse_Opt* pstOpt);
	
	void OnTestProcessFovROI_Auto(__in IplImage* pFrameImage, __inout ST_Angle_Opt* pstOpt);
	// Auto Setting - Spec : Angle
	void OnAutoSettingProcessAngle(__in IplImage* pFrameImage, __inout ST_Angle_Opt* pstOpt, __out ST_Angle_Result &stResult);

	// Auto Setting - Spec : Color
	void OnAutoSettingProcessColor(__in IplImage* pFrameImage, __in ST_CenterPoint_Opt* pstCenterOpt, __inout ST_Color_Opt* pstOpt, __out ST_Color_Result &stResult);

	void OnSetCameraType (__in UINT nType)
	{
		m_nCameraType = nType;
	}

	// SFR 오브젝트 위치 보정
	void OnTestProcessSFRROI_Auto_Center(__in BOOL bMode, __in IplImage* pFrameImage, __inout ST_SFR_Opt* pstOpt);


	void DeleteMemory(ST_LT_TI_EIAJ *pstEIAJ);

protected:

	void CamType_CenterPoint	(__in int iImgW, __in int iImgH, __out int &nPosX, __out  int &nPosY);

	BOOL Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in int iMinSpec, __in int iMaxSpec, __in int iValue);
	UINT m_nCameraType = CamState_Original;

	// 중심점 검출 알고리즘 
	CTI_CenterPoint TI_CenterPoint;

	// SFR 알고리즘 
	CTI_SFR			TI_SFR;

	// Color 알고리즘
	CTI_Color		TI_Color;

	// Particle 알고리즘
	CTI_Particle	TI_Particle;

	// DefectPixel 알고리즘
	CTI_DefectPixel	TI_DefectPixel;

	// Brightness 알고리즘
	CTI_Brightness	TI_Brightness;

	// EIAJ 알고리즘
	CTI_EIAJ	TI_EIAJ;

	// PatternNoise 알고리즘
	CTI_PatternNoise	TI_PatternNoise;

	// Reverse 알고리즘
	CTI_Reverse	TI_Reverse;
};

