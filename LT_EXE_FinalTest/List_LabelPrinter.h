﻿#ifndef List_LabelPrinter_h__
#define List_LabelPrinter_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_LabelPrinter
{
	LabelPrt_Object = 0,
	LabelPrt_Text,
	LabelPrt_PosX,
	LabelPrt_PosY,
	LabelPrt_Width,
	LabelPrt_Height,
	//LabelPrt_Font,
	LabelPrt_MaxCol,
};

static LPCTSTR	g_lpszHeader_LabelPrinter[] =
{
	_T(""),
	_T("Text"),
	_T("X"),
	_T("Y"),
	_T("W"),
	_T("H"),
	//_T("Font"),
	NULL
};

typedef enum enListItemNum_LabelPrinter
{
	LabelPrt_ItemNum = enLabelDataType_Max,
};

static LPCTSTR	g_lpszItem_LabelPrinter[] =
{
	NULL
};

const int	iListAglin_LabelPrinter[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_LabelPrinter[] =
{
	100,
	580,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
};

// List_SFRInfo

class CList_LabelPrinter : public CListCtrl
{
	DECLARE_DYNAMIC(CList_LabelPrinter)

public:
	CList_LabelPrinter();
	virtual ~CList_LabelPrinter();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_LabelPrinter(ST_LabelPrinter* pstLabel)
	{
		if (pstLabel == NULL)
			return;

		m_pstLabel = pstLabel;
	};

protected:

	ST_LabelPrinter*  m_pstLabel;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Font;

	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL		UpdateCellData		(UINT nRow, UINT nCol, CString szValue);

public:

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusEdit();
};

#endif // List_SFRInfo_h__
