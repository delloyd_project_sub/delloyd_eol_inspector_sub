﻿#ifndef Wnd_Cfg_LabelPrinter_h__
#define Wnd_Cfg_LabelPrinter_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "List_LabelPrinter.h"
#include "Def_DataStruct.h"
#include "File_WatchList.h"

// CWnd_Cfg_LabelPrinter

enum enLabelStatic
{
	STI_LABEL_PRNFILE,
	STI_LABEL_COUNT,
	STI_LABEL_SERIAL_NUMBER,
	STI_LABEL_MAX,
};

static LPCTSTR	g_szLabelStatic[] =
{
	_T("Prn File (*.prn)"),
	_T("Print Count"),
	_T("Serial Number"),
	NULL
};

enum enLabelButton
{
	BTN_LABEL_TEST = 0,
	BTN_LABEL_SAVE,
	BTN_LABEL_MAX,
};

static LPCTSTR	g_szLabelButton[] =
{
	_T("Test Printing"),
	_T("Save"),
	NULL
};

enum enLabelComobox
{
	CMB_LABEL_PRNFiLE = 0,
	CMB_LABEL_MAX,
};

enum enLabelEdit
{
	EDT_LABEL_COUNT,
	EDT_LABEL_SERIAL_NUMBER,
	EDT_LABEL_MAX,
};

class CWnd_Cfg_LabelPrinter : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_LabelPrinter)

public:
	CWnd_Cfg_LabelPrinter();
	virtual ~CWnd_Cfg_LabelPrinter();

protected:
	DECLARE_MESSAGE_MAP()

	CFile_WatchList		m_IniWatch;
	ST_ModelInfo		*m_pstModelInfo;
	CList_LabelPrinter	m_List;
	CString				m_szPrnPath;

	CFont				m_font;
	CFont				m_font2;

	CVGStatic			m_st_Item[STI_LABEL_MAX];
	CMFCButton			m_bn_Item[BTN_LABEL_MAX];
	CComboBox			m_cb_Item[CMB_LABEL_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_LABEL_MAX];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	void RefreshFileList(__in UINT nComboID, __in const CStringList* pFileList);

public:

	void SetPath(__in LPCTSTR szPrnFilePath);
	void SetPtr_ModelInfo	(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData	();
	void GetUpdateData	();
};
#endif // Wnd_Cfg_LabelPrinter_h__
