﻿//*****************************************************************************
// Filename	: 	Wnd_SiteInfo.cpp
// Created	:	2016/7/5 - 16:18
// Modified	:	2016/7/5 - 16:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_SiteInfo.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_SiteInfo.h"
#include "CommonFunction.h"

IMPLEMENT_DYNAMIC(CWnd_SiteInfo, CVGGroupWnd)

CWnd_SiteInfo::CWnd_SiteInfo()
{
	VERIFY(m_Font.CreateFont(
		12,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pstModelInfo	= NULL;
	m_nUseTestCount = MAX_STEP_COUNT;

	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_bUseTestItem[nIdx] = FALSE;
	}
}

CWnd_SiteInfo::~CWnd_SiteInfo()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_SiteInfo, CVGGroupWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_SiteInfo message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
int CWnd_SiteInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CVGGroupWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_stItem_T[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Data);
		m_stItem_T[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_stItem_T[nIdx].SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
	}

	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_stItem_T[nIdx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
		m_stItem_V[nIdx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_wnd_VideoView.SetOwner(GetParent());
	m_wnd_VideoView.Create(NULL, _T(""), dwStyle, rectDummy, this, 103);

//	m_st_BackColor.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_BackColor.SetBackColor(RGB(160, 160, 160));
	m_st_BackColor.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_stJudgment.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_stJudgment.SetColorStyle(CVGStatic::ColorStyle_Green);
	m_stJudgment.SetFont_Gdip(L"Arial", 44.0F);
	m_stJudgment.Create(_T("STAND BY"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_TestTimeT.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_TestTimeT.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_TestTimeT.SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
	m_st_TestTimeT.Create(_T("Test Time"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_TestTimeV.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);


	m_st_BarcodeT.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_BarcodeT.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_BarcodeT.SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
	m_st_BarcodeT.Create(_T("Barcode"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_BarcodeV.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);


// 	m_Group.SetTitle(L"CAMERA VIEW_TEST INFO");
// 
// 	if (!m_Group.Create(_T("CAMERA VIEW_TEST INFO"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
// 	{
// 		TRACE0("출력 창을 만들지 못했습니다.\n");
// 		return -1;
// 	}
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::OnSize(UINT nType, int cx, int cy)
{
	CVGGroupWnd::OnSize(nType, cx, cy);

	if (NULL == m_pstModelInfo)
		return;

	if ((cx == 0) && (cy == 0))
		return;

#if 0
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iWidth  = cx;
	int iHeight = cy;

	int iLeft = 0;
	int iTop  = 0;
	int iResult_H = 120;

	//int iVideo_W = (int)(iWidth / 3 * 2);
	//int iVideo_H = iHeight - iResult_H - iSpacing;

	int iVideo_W = 720;
	int iVideo_H = 480;

	m_Group.MoveWindow(0, 0, cx, cy);

	m_stJudgment.MoveWindow(iLeft, iTop, iWidth, iResult_H);

	iTop += iResult_H + iSpacing;
	m_wnd_VideoView.MoveWindow(iLeft, iTop, iVideo_W, iVideo_H);

	 // 검사 정보
	iLeft += iVideo_W + iSpacing;
	
 	int iSubWidth = 120;
 	int iSubLeft = iLeft + iSubWidth - 1;
	int iCtrlHeight = (iVideo_H - (m_nUseTestCount + 1)) / ((m_nUseTestCount + 1) + 1);
	int iFullWidth = (cx - iVideo_W) - iSpacing - 2;
	int iHalfWidth = (cx - iVideo_W) - iSubWidth - iSpacing - 1;
  	

	m_st_BarcodeT.MoveWindow(iLeft, iTop, iSubWidth, iCtrlHeight);
	m_st_BarcodeV.MoveWindow(iSubLeft, iTop, iHalfWidth, iCtrlHeight);
	iTop += iCtrlHeight + 1;

	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		if (m_bUseTestItem[nIdx])
		{
			m_stItem_T[nIdx].MoveWindow(iLeft, iTop, iSubWidth, iCtrlHeight);
			m_stItem_V[nIdx].MoveWindow(iSubLeft, iTop, iHalfWidth, iCtrlHeight);

			iTop += iCtrlHeight + 1;
		}
		else
		{
			m_stItem_T[nIdx].MoveWindow(0, 0, 0, 0);
			m_stItem_V[nIdx].MoveWindow(0, 0, 0, 0);
		}
	}

	m_st_TestTimeT.MoveWindow(iLeft, iTop, iSubWidth, iCtrlHeight);
	m_st_TestTimeV.MoveWindow(iSubLeft, iTop, iHalfWidth, iCtrlHeight);
#else

	if ((cx == 0) && (cy == 0))
		return;

	int iLeftMargin = 7;
	int iRightMargin = 7;
	int iTopMargin = 27;
	int iBottomMargin = 7;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iLeftMargin;
	int iTop = iTopMargin;
	int iBottom = cy - iBottomMargin;
	int iWidth = (int)(cx * 0.70) - iLeftMargin - iRightMargin;
	int iHeight = cy - iTopMargin - iBottomMargin;
	int iHeightVideo = (int)(cy * 0.85) - iTopMargin - iBottomMargin;

	// 영상 (720 x 480)
	int iVideoWidth = 720; // VIDEO_WIDTH;//(iWidth - iSpacing) * 3 / 5;
	int iVideoHeight = 480; // VIDEO_HEIGHT;//VIDEO_HEIGHT * iVideoWidth / VIDEO_WIDTH;


	int iWidthTemp = iVideoWidth;
	int iHeightTemp = iVideoHeight;


	iWidthTemp  = 720;
	iHeightTemp = 480;

	//m_Group.MoveWindow(0, 0, cx, cy);

	m_st_BackColor.MoveWindow(iLeft, iTop, 0, 0);
	m_wnd_VideoView.MoveWindow(iLeft + (abs(iWidthTemp - iWidth) / 2), iTop + (abs(iHeightTemp - iHeightVideo) / 2), iWidthTemp, iHeightTemp);

	m_stJudgment.MoveWindow(iLeft, iTop + iHeightVideo + iBottomMargin, iWidth, iHeight - iHeightVideo - 8);

	iTop += iVideoHeight + iSpacing;
	int iTempHeight = iHeight - iVideoHeight - iSpacing;

	// 검사 정보
	//iLeft += iWidth + iSpacing;



	int iVideo_W = iWidth;
	int iVideo_H = iHeightVideo + (iHeight - iHeightVideo - 8) + iCateSpacing + iSpacing;
	// 검사 정보
	iLeft += iWidth + iSpacing;
	iTop = iTopMargin;

	int iSubWidth = 120;
	int iSubLeft = iLeft + iSubWidth - 1;
	int iCtrlHeight = (iVideo_H/* - (m_nUseTestCount + 1)*/) / ((m_nUseTestCount + 1)+1); 
	int iFullWidth = (cx - iVideo_W - iSpacing - iSpacing) - iSpacing - 2;
	int iHalfWidth = (cx - iVideo_W - iSpacing - iSpacing) - iSubWidth - iSpacing - 1;


	m_st_BarcodeT.MoveWindow(iLeft, iTop, iSubWidth, iCtrlHeight);
	m_st_BarcodeV.MoveWindow(iSubLeft, iTop, iHalfWidth, iCtrlHeight);
	iTop += iCtrlHeight-1;

	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		if (m_bUseTestItem[nIdx])
		{
			m_stItem_T[nIdx].MoveWindow(iLeft, iTop, iSubWidth, iCtrlHeight+1);
			m_stItem_V[nIdx].MoveWindow(iSubLeft, iTop, iHalfWidth, iCtrlHeight+1);

			iTop += iCtrlHeight - 1;
		}
		else
		{
			m_stItem_T[nIdx].MoveWindow(0, 0, 0, 0);
			m_stItem_V[nIdx].MoveWindow(0, 0, 0, 0);
		}
	}

	m_st_TestTimeT.MoveWindow(iLeft, iTop, iSubWidth, iCtrlHeight);
	m_st_TestTimeV.MoveWindow(iSubLeft, iTop, iHalfWidth, iCtrlHeight);


#endif
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
BOOL CWnd_SiteInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CVGGroupWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnRedrawControl
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/17 - 19:30
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::OnRedrawControl()
{
	m_st_BackColor.Invalidate();

	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_stItem_T[nIdx].Invalidate();
		m_stItem_V[nIdx].Invalidate();
	}

	m_stJudgment.Invalidate();
}

//=============================================================================
// Method		: RedrawWindow
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/16 - 19:26
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::RedrawWindow()
{
	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	}
}

//=============================================================================
// Method		: SetTestItem
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/4 - 11:21
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestItem()
{
	INT_PTR iCnt = m_pstModelInfo->stStepInfo.GetCount();

	if (iCnt <= MAX_STEP_COUNT)
	{
		UINT nTestItemID = 0;
		UINT nTestItemCnt = 0;
		m_nUseTestCount = 0;

		memset(m_bUseTestItem, FALSE, sizeof(BOOL) * MAX_STEP_COUNT);

		for (INT_PTR iIdx = 0; iIdx < iCnt; iIdx++)
		{
			if (TRUE == m_pstModelInfo->stStepInfo.StepList[iIdx].bTestUse)
			{
				nTestItemID = m_pstModelInfo->stStepInfo.StepList[iIdx].nTestItem;
				nTestItemCnt = m_pstModelInfo->stStepInfo.StepList[iIdx].nTestItemCnt;

				if (g_nLT_TestItemCount[nTestItemID] > 1)
				{
					CString szTestItemName;
					switch (nTestItemID)
					{
					case TIID_TestInitialize:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemInitial[nTestItemCnt]);
						break;
					case TIID_TestFinalize:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemFinal[nTestItemCnt]);
						break;
					case TIID_Current:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemCurrent[nTestItemCnt]);
						break;
					//case TIID_OperationMode:
					//	szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemOperationMode[nTestItemCnt]);
					//	break;
					case TIID_CenterPoint:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemCenterPt[nTestItemCnt]);
						break;
					case TIID_Rotation:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemRotate[nTestItemCnt]);
						break;
 					case TIID_EIAJ:
 						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemEIAJ[nTestItemCnt]);
 						break;
					//case TIID_SFR:
					//	szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemSFR[nTestItemCnt]);
					//	break;
					case TIID_FOV:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemAngle[nTestItemCnt]);
						break;
					case TIID_Color:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemColor[nTestItemCnt]);
						break;
					case TIID_Reverse:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemReverse[nTestItemCnt]);
						break;
					//case TIID_LEDTest:
					//	szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemLED[nTestItemCnt]);
					//	break;

					//case TIID_PatternNoise:
					//	szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemPatternNoise[nTestItemCnt]);
					//	break;

					case TIID_Brightness:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemBrightness[nTestItemCnt]);
						break;

					case TIID_IRFilter:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemIRFilter[nTestItemCnt]);
						break;

 					case TIID_ParticleManual:
 						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemParticleManual[nTestItemCnt]);
 						break;

					case TIID_BlackSpot:
						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemBlackSpot[nTestItemCnt]);
						break;

// 					case TIID_IRFilterManual:
// 						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemIRFilterManual[nTestItemCnt]);
// 						break;

// 					case TIID_FFT:
// 						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemFFT[nTestItemCnt]);
// 						break;

// 					case TIID_Particle:
// 						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemBlackSpot[nTestItemCnt]);
// 						break;

// 					case TIID_SFR:
// 						szTestItemName.Format(_T("%s %s"), g_szLT_TestItem_Name[nTestItemID], g_szLT_TestItemSFR[nTestItemCnt]);
// 						break;

					default:
						break;
					}

					m_stItem_T[iIdx].SetText(szTestItemName);
					m_bUseTestItem[iIdx] = TRUE;
				}
				else
				{
					m_stItem_T[iIdx].SetText(g_szLT_TestItem_Name[nTestItemID]);
					m_bUseTestItem[iIdx] = TRUE;
				}
				
				m_nUseTestCount++;
			}
		}
	}
	else
	{
		// 에러
	}

	RedrawWindow();
}

//=============================================================================
// Method		: SetElapsedTime
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD dwTime
// Qualifier	:
// Last Update	: 2016/11/16 - 23:42
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetElapsedTime(__in DWORD dwTime)
{
	CString szTime;

	szTime.Format(_T("%.1f Sec"), dwTime / 1000.0f);

	m_st_TestTimeV.SetText(szTime.GetBuffer());
	szTime.ReleaseBuffer();
}

//=============================================================================
// Method		: SetTestResult
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in UINT nStepIdx
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2018/1/14 - 14:27
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestResult(__in enTestEachResult EachResult, __in UINT nStepIdx, __in CString strText)
{
	m_stItem_V[nStepIdx].SetBackColor_COLORREF(g_TestEachResult[EachResult].BackColor);
	m_stItem_V[nStepIdx].SetTextColor(g_TestEachResult[EachResult].TextColor);
	m_stItem_V[nStepIdx].SetText(strText);
}

//=============================================================================
// Method		: SetTestJudgment
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Qualifier	:
// Last Update	: 2018/1/14 - 15:28
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestJudgment(__in enTestEachResult EachResult)
{
	m_stJudgment.SetColorStyle((CVGStatic::ColorStyle)g_TestJudgment[EachResult].nColorStyle);
	m_stJudgment.SetText(g_TestJudgment[EachResult].szText);

}

//=============================================================================
// Method		: SetTestJudgment
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Qualifier	:
// Last Update	: 2018/1/14 - 15:28
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestJudgment(__in enTestEachResult EachResult, __in CString szValue)
{
	m_stJudgment.SetColorStyle((CVGStatic::ColorStyle)g_TestJudgment[EachResult].nColorStyle);
	m_stJudgment.SetText(szValue);

}

//=============================================================================
// Method		: SetResetResult
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/14 - 14:29
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetResetResult()
{
	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_stItem_V[nIdx].SetText(_T(""));
		m_stItem_V[nIdx].SetBackColor_COLORREF(RGB(255, 255, 255));
	}
}


//=============================================================================
// Method		: SetBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/11/16 - 23:44
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetBarcode(__in LPCTSTR szBarcode)
{
	if (NULL != szBarcode)
		m_st_BarcodeV.SetText(szBarcode);
}
