﻿#ifndef Wnd_BaseLEDTestOp_h__
#define Wnd_BaseLEDTestOp_h__

#pragma once

#include "Wnd_LEDTestOp.h"

// CWnd_BaseLEDTestOp

class CWnd_BaseLEDTestOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseLEDTestOp)

public:
	CWnd_BaseLEDTestOp();
	virtual ~CWnd_BaseLEDTestOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
//	CWnd_LEDTestOp m_wndLEDTestOp[TICnt_LEDTest];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseLEDTestOp_h__
