﻿#ifndef List_Work_VideoSignal_h__
#define List_Work_VideoSignal_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_VideoSignal_Worklist
{
	VideoSignal_W_Recode,
	VideoSignal_W_Time,
	VideoSignal_W_Equipment,
	VideoSignal_W_Model,
	VideoSignal_W_SWVersion,
	VideoSignal_W_LOTNum,
	VideoSignal_W_Barcode,
	VideoSignal_W_Operator,
	VideoSignal_W_Result,
	VideoSignal_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_VideoSignal_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Operator"),
	_T("Result"),
	NULL
};

const int	iListAglin_VideoSignal_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_VideoSignal_Worklist[] =
{
	40,
	100,
	100,
	100,
	100,
	100,
	100,
	100,
	100,
};

// CList_Work_VideoSignal

class CList_Work_VideoSignal : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_VideoSignal)

public:
	CList_Work_VideoSignal();
	virtual ~CList_Work_VideoSignal();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_VideoSignal_h__
