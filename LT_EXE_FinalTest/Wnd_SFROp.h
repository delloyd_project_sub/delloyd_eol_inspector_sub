﻿#ifndef Wnd_SFROp_h__
#define Wnd_SFROp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "List_SFROp.h"
#include "Def_DataStruct.h"
#include "File_WatchList.h"

// CWnd_SFROp

enum enSFRStatic
{
	STI_SFR_PIXELSIZE,
	STI_SFR_RESULTTYPE,
	STI_SFR_IIC_1,
//	STI_SFR_IIC_2,
	STI_SFR_MAX,
};

static LPCTSTR	g_szSFRStatic[] =
{
	_T("Pixel Size"),
	_T("Result Type"),
	_T("IIC Setting"),
//	_T("IIC Cam Off"),
	NULL
};

enum enSFRButton
{
	BTN_SFR_TEST = 0,
	BTN_SFR_ROIRESET,
	BTN_SFR_FIELD,
	BTN_SFR_EDGE,
	BTN_SFR_DISTORTION,
	BTN_SFR_IIC_1,
//	BTN_SFR_IIC_2,
	BTN_SFR_MAX,
};

static LPCTSTR	g_szSFRButton[] =
{
	_T("TEST"),
	_T("RESET"),
	_T("FIELD"),
	_T("EDGE"),
	_T("DISTORTION"),
	_T(""),
//	_T(""),
	NULL
};

enum enSFRComobox
{
	CMB_SFR_ResultType,
	CMB_SFR_IIC_1,	
//	CMB_SFR_IIC_2,
	CMB_SFR_MAX
};

static LPCTSTR	g_szSFRCommoBox[] =
{
	NULL
};

enum enSFREdit
{
	EDT_SFR_PIXELSIZE,
	EDT_SFR_MAX,
};

class CWnd_SFROp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_SFROp)

public:
	CWnd_SFROp();
	virtual ~CWnd_SFROp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo		*m_pstModelInfo;
	CList_SFROp			m_List;

	CFont				m_font;
	CFont				m_font2;

	CVGStatic			m_st_Item[STI_SFR_MAX];
	CMFCButton			m_bn_Item[BTN_SFR_MAX];
	CComboBox			m_cb_Item[CMB_SFR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_SFR_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	CFile_WatchList	m_IniWatch;
	CString			m_szI2CPath;


	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void RefreshFileList(__in UINT nComboID, __in const CStringList* pFileList);

	void SetPath(__in LPCTSTR szI2CPath)
	{
		m_szI2CPath = szI2CPath;
	};

	void SetPtr_ModelInfo	(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;

	};

	void SetUpdateData	();
	void GetUpdateData	();
};
#endif // Wnd_SFROp_h__
