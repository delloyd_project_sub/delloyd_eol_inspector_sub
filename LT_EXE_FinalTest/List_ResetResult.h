﻿#ifndef List_ResetResult_h__
#define List_ResetResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_ResetResult
{
	ResetResult_Object = 0,
	ResetResult_Result,
	ResetResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_ResetResult[] =
{
	_T(""),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_ResetResult
{
	ResetResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_ResetResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_ResetResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ResetResult[] =
{
	80,
	80,
};

// List_ResetInfo

class CList_ResetResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ResetResult)

public:
	CList_ResetResult();
	virtual ~CList_ResetResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Reset(ST_LT_TI_Reset* pstReset)
	{
		if (pstReset == NULL)
			return;

		m_pstReset = pstReset;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Reset*  m_pstReset;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_ResetInfo_h__
