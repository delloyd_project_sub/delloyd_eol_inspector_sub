﻿#ifndef Wnd_PatternNoiseOp_h__
#define Wnd_PatternNoiseOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "List_PatternNoiseOp.h"
#include "Def_DataStruct_Cm.h"

// CWnd_PatternNoiseOp
enum enPNStatic
{
	STI_PN_dBThr,
	STI_PN_EnvThr,
	STI_PN_MAX,
};

static LPCTSTR	g_szPNStatic[] =
{
	_T("dB Thr"),
	_T("Environment Thr"),
	NULL
};

enum enPatternNoiseButton
{
	BTN_PN_TEST = 0,
	BTN_PN_RESET,
	BTN_PN_MAX,
};

static LPCTSTR	g_szPatternNoiseButton[] =
{
	_T("TEST"),
	_T("RESET"),
	NULL
};

enum enPNEdit
{
	EDT_PN_dBThr,
	EDT_PN_EnvThr,
	EDT_PN_MAX,
};

class CWnd_PatternNoiseOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_PatternNoiseOp)

public:
	CWnd_PatternNoiseOp();
	virtual ~CWnd_PatternNoiseOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo			*m_pstModelInfo;
	CList_PatternNoiseOp		m_List;

	CFont				m_font;
	CVGStatic			m_st_Item[STI_PN_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_PN_MAX];
	CButton				m_bn_Item[BTN_PN_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_PatternNoiseOp_h__
