﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem.h
// Created	:	2017/1/3 - 10:37
// Modified	:	2017/1/3 - 10:37
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_TestItem_h__
#define Wnd_Cfg_TestItem_h__

#pragma once

#include "Wnd_BaseView.h"
#include "Wnd_BaseOperModeOp.h"
#include "Wnd_BaseCurrentOp.h"
#include "Wnd_BaseCenterPointOp.h"
#include "Wnd_BaseRotateOp.h"
#include "Wnd_BaseEIAJOp.h"
#include "Wnd_BaseAngleOp.h"
#include "Wnd_BaseColorOp.h"
#include "Wnd_BaseReverseOp.h"
#include "Wnd_BaseLEDTestOp.h"
#include "Wnd_BaseParticleOp.h"
#include "Wnd_BaseParticleManualOp.h"
#include "Wnd_BaseVideoSignalOp.h"
#include "Wnd_BaseResetOp.h"
#include "Wnd_BaseSFROp.h"
#include "Wnd_BasePatternNoiseOp.h"
#include "Wnd_BaseIRFilterOp.h"
#include "Wnd_BaseBrightnessOp.h"
#include "Wnd_BaseDefectPixelOp.h"


// #include "Wnd_BaseIRFilterManualOp.h"
// #include "Wnd_BaseParticleOp.h"
// #include "Wnd_BaseFFTOp.h"

#include "Def_DataStruct.h"
#include "Def_TestDevice.h"

class CWnd_Cfg_TestItem : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_TestItem)

public:
	CWnd_Cfg_TestItem();
	virtual ~CWnd_Cfg_TestItem();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);

	CFont	m_font_Data;

	// TabCtrl Option
	CMFCTabCtrl					m_tc_Option;

	CWnd_BaseCurrentOp			m_wnd_CurrentOp;
//	CWnd_BaseOperModeOp			m_wnd_OperModeOp;
	CWnd_BaseCenterPointOp		m_wnd_CenterPointOp;
	CWnd_BaseRotateOp			m_wnd_RotateOp;
	CWnd_BaseEIAJOp				m_wnd_EIAJOp;
	CWnd_BaseAngleOp			m_wnd_AngleOp;
	CWnd_BaseColorOp			m_Wnd_ColorOp;
	CWnd_BaseReverseOp			m_wnd_ReverseOp;
//	CWnd_BaseLEDTestOp			m_wnd_LEDTestOp;
	CWnd_BaseParticleManualOp	m_wnd_ParticleMAOp;
	CWnd_BaseParticleOp			m_wnd_BlackSpotOp;
	CWnd_BaseDefectPixelOp		m_wnd_DefectPixelOp;
//	CWnd_BaseVideoSignalOp		m_wnd_VideoSignalOp;
//	CWnd_BaseResetOp			m_wnd_ResetOp;
// 	CWnd_BaseSFROp				m_wnd_SFROp;

// 	CWnd_BasePatternNoiseOp		m_wnd_PatternNoiseOp;
	CWnd_BaseIRFilterOp			m_wnd_IRFilterOp;
 	CWnd_BaseBrightnessOp		m_wnd_BrightnessOp;

// 	CWnd_BaseIRFilterManualOp	m_wnd_IRFilterManualOp;
// 	CWnd_BaseParticleOp		m_wnd_BlackSpotOp;
// 	CWnd_BaseFFTOp			m_wnd_FFTOp;
	
	ST_TestItemMode			m_stTestItem;

	ST_ModelInfo*			m_pstModelInfo;
	ST_Device*				m_pstDevice;

public:

	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	void SetPath(__in LPCTSTR szIICPath)
	{
		//m_wnd_SFROp.SetPath(szIICPath);
	};

	int	GetTabCursel()
	{
		m_tc_Option.GetActiveTab();
	};

	BOOL		IsTestAll				();

	// 모델 데이터를 UI에 표시
	void		SetModelInfo			(ST_ModelInfo* pstModelInfo);
	void		GetModelInfo			();

	void		SetStatusEngineerMode	(__in enPermissionMode InspMode);

};

#endif // Wnd_Cfg_TestItem_h__
