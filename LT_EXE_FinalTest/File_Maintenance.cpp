﻿#include "stdafx.h"
#include "File_Maintenance.h"
#include "Def_Enum.h"
#include "CommonFunction.h"

#define		TEACH_AppName					_T("TEACH_OP")

CFile_Maintenance::CFile_Maintenance()
{
}


CFile_Maintenance::~CFile_Maintenance()
{
}

//=============================================================================
// Method		: Load_Teach_Info
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_TeachInfo & stTeachInfo
// Qualifier	:
// Last Update	: 2018/3/13 - 10:48
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::LoadTeachFile(__in LPCTSTR szPath, __out ST_TeachInfo& stTeachInfo)
{
	TCHAR   inBuff[255] = { 0, };
	CString strValue, strApp;

	for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
	{
		strApp.Format(_T("Teach_%d"), nIdx);
		GetPrivateProfileString(TEACH_AppName, strApp, strValue, inBuff, 80, szPath);
		stTeachInfo.dbTeachData[nIdx] = _ttof(inBuff);
	}
	return TRUE;
}

//=============================================================================
// Method		: Save_Teach_Info
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_TeachInfo * pstTeachInfo
// Qualifier	:
// Last Update	: 2018/3/13 - 10:48
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::SaveTeachFile(__in LPCTSTR szPath, __in const ST_TeachInfo* pstTeachInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstTeachInfo)
		return FALSE;

	CString strValue;
	CString strAppName;

	for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
	{
		strValue.Format(_T("%6.2f"), pstTeachInfo->dbTeachData[nIdx]);
		strAppName.Format(_T("Teach_%d"), nIdx);
		WritePrivateProfileString(TEACH_AppName, strAppName, strValue, szPath);
	}
	return TRUE;
}
