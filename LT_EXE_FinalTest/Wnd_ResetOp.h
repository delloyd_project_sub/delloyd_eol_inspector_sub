﻿#ifndef Wnd_ResetOp_h__
#define Wnd_ResetOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "Def_DataStruct.h"

// CWnd_ResetOp

enum enResetStatic
{
	STI_RESET_DELAY = 0,
	STI_RESET_MAX,
};

static LPCTSTR	g_szResetStatic[] =
{
	_T("Delay"),
	NULL
};

enum enResetButton
{
	BTN_Reset_TEST = 0,
	BTN_Reset_MAX,
};

static LPCTSTR	g_szResetButton[] =
{
	_T("TEST"),
	NULL
};

enum enResetComobox
{
	CMB_Reset_MAX = 1,
};

enum enResetEdit
{
	EDT_RESET_DELAY = 0,
	EDT_RESET_MAX,
};



class CWnd_ResetOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ResetOp)

public:
	CWnd_ResetOp();
	virtual ~CWnd_ResetOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo		*m_pstModelInfo;

	CFont				m_font;
	
	CVGStatic			m_st_Item[STI_RESET_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_RESET_MAX];
	CButton				m_bn_Item[BTN_Reset_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};

#endif // Wnd_ResetOp_h__
