﻿//*****************************************************************************
// Filename	: 	Wnd_ManualCtrl.h
// Created	:	2017/1/4 - 13:33
// Modified	:	2017/1/4 - 13:33
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_ManualMotion_h__
#define Wnd_ManualMotion_h__

#pragma once

#include "Def_TestDevice.h"

#define CYL_OUT  FALSE
#define CYL_IN	 TRUE
// CWnd_Manual_Motion

static LPCTSTR	g_szManual_Motion_Button[] =
{
	//_T("Motor Origin"),
	_T("Move Motor Loading"),
	_T("Move Motor Inspection"),
	_T("Move Motor Particle Loading"),
	_T("Move Motor Particle Inspection"),
	_T("Particle Cylinder In"),
	_T("Particle Cylinder Out"),
	//_T("LED Light On"),
	//_T("LED Light Off"),
	//_T("IR Light On"),
	//_T("IR Light Off"),
	NULL
};

enum enManual_MotionCtrlBtn
{
	//BT_MMotion_Initialize,
	BT_MMotion_Load,
	BT_MMotion_Inspect,
	BT_MMotion_Part_Load,
	BT_MMotion_Part_Insp,
	BT_MMotion_Cylinder_In,
	BT_MMotion_Cylinder_Out,
	//BT_MMotion_LED_On,
	//BT_MMotion_LED_Off,
	//BT_MMotion_IR_On,
	//BT_MMotion_IR_Off,
	BT_MMotion_MAXNUM,
};

class CWnd_Manual_Motion : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Manual_Motion)

public:
	CWnd_Manual_Motion();
	virtual ~CWnd_Manual_Motion();

	void SetCtrlID(UINT nWM_ID)
	{
		m_wm_ManualID = nWM_ID;
	}


protected:

	UINT m_wm_ManualID = NULL;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnRangeCmds		(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	void			ClickOut		(UINT nID);
	void			SetBtnState		();

	DECLARE_MESSAGE_MAP()

	
	ST_Device*		m_pDevice;

	CVGStatic		m_st_Name;

	CFont			m_font;
	CMFCButton		m_Btn_Item[BT_MMotion_MAXNUM];
	BOOL			m_bParticle_In;
	BOOL			m_bParticle_Out;
	BOOL			m_bInitalize;



public:


	void SetParticle_Cylinder_IN(BOOL bParticleIn)
	{
		m_bParticle_In = bParticleIn;
		//!SH _190109 : 변화 적용 필요
		SetBtnState();
	}

	void SetParticle_Cylinder_OUT(BOOL bParticleOut)
	{
		m_bParticle_Out = bParticleOut;
		SetBtnState();
	}

	void SetPtr_Device(__in ST_Device* pDevice)
	{
		m_pDevice = pDevice;
	};
	void ImageLoadMode(bool bMode);
};
#endif // Wnd_ManualCtrl_h__


