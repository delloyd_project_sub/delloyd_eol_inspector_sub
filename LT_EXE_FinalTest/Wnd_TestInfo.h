﻿//*****************************************************************************
// Filename	: 	Wnd_TestInfo.h
// Created	:	2016/5/13 - 11:39
// Modified	:	2016/5/13 - 11:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_TestInfo_h__
#define Wnd_TestInfo_h__

#pragma once

#include "VGGroupWnd.h"
#include "VGStatic.h"
#include "Def_Enum.h"


enum enTestButton
{
	TB_Start,
	TB_Stop,
	TB_LotStart,
	TB_LotStop,
	TB_ModelChange,
	//TB_MasterTest,
	//TB_StartUpMode,
	//TB_MasterMode,
	TB_MasterSet,
	TB_MaxEnum,
};

static LPCTSTR g_szTestButton[] =
{
	_T("START"),
	_T("STOP"),
	_T("LOT START"),
	_T("LOT STOP"),
	_T("MODEL CHANGE"),
//	_T("MASTER TEST"),
//	_T("Start-Up Mode"),
//	_T("MASTER Mode"),
	_T("MASTER SET"),
	NULL
};

//-----------------------------------------------------------------------------
// CWnd_TestInfo
//-----------------------------------------------------------------------------
class CWnd_TestInfo : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TestInfo)

public:
	CWnd_TestInfo();
	virtual ~CWnd_TestInfo();

protected:
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnRangeCmds		(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()

	CFont			m_Font;	
	CMFCButton		m_bn_Test[TB_MaxEnum];
	enPermissionMode	m_enInspMode;

public:

	void	PermissionMode	(enPermissionMode InspMode);
	void	ButtonEnable	(BOOL bMode);

	void	LotStartBtnEnable(BOOL bEnable);
	void	LotStopBtnEnable(BOOL bEnable);
	void	StartBtnEnable(BOOL bEnable);
	void	StopBtnEnable(BOOL bEnable);
	//void	MasterTestBtnEnable(BOOL bEnable);
	void	MasterModeBtnEnable(BOOL bEnable);

	BOOL	GetStartBtnEnable();
	BOOL	GetStopBtnEnable();
};

#endif // Wnd_TestInfo_h__


