﻿//*****************************************************************************
// Filename	: Wnd_LogView.cpp
// Created	: 2010/12/2
// Modified	: 2015/12/22
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_LogView.h"
#include "Def_Enum.h"

//=============================================================================
// CWnd_LogView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_LogView, CWnd_BaseView)

//=============================================================================
//
//=============================================================================
CWnd_LogView::CWnd_LogView()
{
	VERIFY(m_font_Default.CreateFont(
		20,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		26,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_LogView::~CWnd_LogView()
{
	TRACE(_T("<<< Start ~CWnd_LogView >>> \n"));
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_LogView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


//=============================================================================
// CWnd_LogView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_LogView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/22
// Desc.		:
//=============================================================================
int CWnd_LogView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_log_All.SetTitle(L"System Log");
	m_log_Error.SetTitle(L"Error Log");

	if (!m_log_All.Create(_T("System Log"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	if (!m_log_Error.Create(_T("Error Log"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 22))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	return 0;
}

//=============================================================================
// Method		: CWnd_LogView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2015/12/22
// Desc.		:
//=============================================================================
void CWnd_LogView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	int iLeftMagrin		= 10;
	int iRightMagin		= 10;
	int iTopMargin		= 10;
	int iBottomMargin	= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 10;

	int iLeft		= iLeftMagrin;
	int iTop		= iTopMargin;
	int iBottom		= cy - iBottomMargin;
	int iWidth		= cx - iLeftMagrin - iRightMagin;
	int iHeight		= cy - iTopMargin - iBottomMargin;
	int iHalfWidth	= (cx - iLeftMagrin - iRightMagin - iSpacing) / 2;
	int iHalfHeight	= (iHeight - iSpacing) / 2;

	m_log_All.MoveWindow(iLeft, iTop, iHalfWidth, iHeight);

	iLeft += iHalfWidth + iSpacing;
	m_log_Error.MoveWindow(iLeft, iTop, iHalfWidth, iHeight);
}

//=============================================================================
// Method		: CWnd_LogView::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/22
// Desc.		:
//=============================================================================
BOOL CWnd_LogView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: AddLog
// Access		: public  
// Returns		: void
// Parameter	: LPCTSTR lpszLog
// Parameter	: BOOL bError
// Parameter	: UINT nLogType
// Parameter	: COLORREF clrTextColor
// Qualifier	:
// Last Update	: 2015/12/22 - 18:48
// Desc.		:
//=============================================================================
void CWnd_LogView::AddLog(LPCTSTR lpszLog, BOOL bError /*= FALSE*/, UINT nLogType /*= 0*/, COLORREF clrTextColor /*= RGB(0, 0, 0)*/)
{
	m_log_All.AddText(lpszLog, clrTextColor);

	if (bError)
	{
		m_log_Error.AddText(lpszLog, clrTextColor);
	}

}
