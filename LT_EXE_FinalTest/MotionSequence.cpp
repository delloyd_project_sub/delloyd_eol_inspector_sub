﻿//*****************************************************************************
// Filename	: 	MotionSequence.cpp
// Created	:	2017/01/09 - 13:54
// Modified	:	2017/01/09 - 13:54
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "MotionSequence.h"
#include "CommonFunction.h"

CMotionSequence::CMotionSequence(LPVOID p)
{
	m_hOwnerWnd			= NULL;
	m_pstOption			= NULL;
	m_pstModelInfo		= NULL;
	m_pDigitalIOCtrl	= NULL;
	m_pMotion			= NULL;
	m_bAreaCheck		= FALSE;
	m_bOriginStop		= FALSE;
	m_bXmlStop			= FALSE;
	m_bIoLoopStop		= FALSE;

	for (UINT nCh = 0; nCh < MAX_PCB_LIGHT_CNT; nCh++)
	{
		m_pLightBrd[nCh] = NULL;
	}
}

CMotionSequence::~CMotionSequence(void)
{
}

//=============================================================================
// Method		: AllMotorOrigin
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/28 - 15:15
// Desc.		:
//=============================================================================
BOOL CMotionSequence::AllMotorOrigin()
{
	if (!m_pMotion->SetAllAmpCtr(ON))
		return FALSE;

	if (!m_pMotion->SetAllAlarmClear())
		return FALSE;

	m_bOriginStop = FALSE;

	for (UINT nStep = 0; nStep < 2; nStep++)
	{
		if (m_bOriginStop == TRUE)
			return FALSE;

		if(!OriginProcedure(nStep))
			return FALSE;

		Sleep(1000);
	}

	return TRUE;
}

//=============================================================================
// Method		: RunProcedure
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nStep
// Qualifier	:
// Last Update	: 2017/3/9 - 18:35
// Desc.		:
//=============================================================================
BOOL CMotionSequence::OriginProcedure(UINT nStep)
{
// 	clock_t start_tm;
// 
// 	switch (nStep)
// 	{
// 	case 0:
// 		m_pMotion->SetMotorEStop(MotorAxisY);
// 		Sleep(200);
// 
// 		m_pMotion->SetMotorOrigin(MotorAxisY);
// 
// 		start_tm = clock();
// 		do
// 		{
// 			// 제한 시간 동안 마무리 되지 않을 경우 자동 STOP	
// 			if ((DWORD)clock() - start_tm > MotorOriginWaitTime )
// 			{
// 				m_pMotion->m_pMotionCtr[MotorAxisY].OriginStopAxis();
// 				return FALSE;
// 			}
// 
// 			// 원점 정지 시
// 			if (m_pMotion->GetOriginStopStatus(MotorAxisY))
// 			{
// 				m_bOriginStop = TRUE;
// 				return FALSE;
// 			}
// 
// 			DoEvents(1);
// 
// 		} while (!m_pMotion->m_pMotionCtr[MotorAxisY].GetOriginStatus());
// 		
// 		break;
// 
// 	case 1:
// 		m_pMotion->SetMotorEStop(MotorAxisX);
// 		m_pMotion->SetMotorEStop(MotorAxisZ);
// 		m_pMotion->SetMotorEStop(MotorAxisR);
// 
// 		Sleep(200);
// 
// 		m_pMotion->SetMotorOrigin(MotorAxisX);
// 		m_pMotion->SetMotorOrigin(MotorAxisZ);
// 		m_pMotion->SetMotorOrigin(MotorAxisR);
// 
// 		start_tm = clock();
// 		do
// 		{
// 			// 제한 시간 동안 마무리 되지 않을 경우 자동 STOP	
// 			if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
// 			{
// 				m_pMotion->m_pMotionCtr[MotorAxisX].OriginStopAxis();
// 				m_pMotion->m_pMotionCtr[MotorAxisZ].OriginStopAxis();
// 				m_pMotion->m_pMotionCtr[MotorAxisR].OriginStopAxis();
// 				return FALSE;
// 			}
// 
// 			// 원점 정지 시
// 			if (m_pMotion->GetOriginStopStatus(MotorAxisX) || m_pMotion->GetOriginStopStatus(MotorAxisZ) || m_pMotion->GetOriginStopStatus(MotorAxisR))
// 			{
// 				m_bOriginStop = TRUE;
// 				return FALSE;
// 			}
// 
// 			DoEvents(1);
// 
// 		} while (!m_pMotion->m_pMotionCtr[MotorAxisX].GetOriginStatus() || !m_pMotion->m_pMotionCtr[MotorAxisZ].GetOriginStatus() || !m_pMotion->m_pMotionCtr[MotorAxisR].GetOriginStatus());
// 
// 		break;
// 
// 	default:
// 		break;
// 	}

	return TRUE;
}

//=============================================================================
// Method		: XmlProcedure
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/5/11 - 11:57
// Desc.		:
//=============================================================================
BOOL CMotionSequence::XmlProcedure()
{
// 	m_bXmlStop = FALSE;
// 	ST_SequenceItem stSeqItem;
// 	for (int iCnt = 0; iCnt <= m_pSeqList->StepList.GetUpperBound(); iCnt++)
// 	{
// 		stSeqItem = m_pSeqList->StepList.GetAt(iCnt);
// 
// 		if (m_bXmlStop)
// 			break;
// 
// 		m_pstMotion->MotorAxisDitailMove(POS_ABS_MODE, stSeqItem.nAxis, stSeqItem.dbPos, stSeqItem.dbVel, stSeqItem.dbAcc);
// 		
// 		if (m_bXmlStop)
// 			break;
// 		
// 		Sleep(stSeqItem.nDelay);
// 	}

	return TRUE;
}

//=============================================================================
// Method		: Stage3AxisInit
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/9/18 - 11:21
// Desc.		:
//=============================================================================
BOOL CMotionSequence::Stage3AxisInit()
{
// 	ST_AxisMoveParam stAxisParam[MotorAxisNum];
// 
// 	stAxisParam[MotorAxisX].lAxisNum = MotorAxisX;
// 	stAxisParam[MotorAxisX].dbPos = 0;
// 	stAxisParam[MotorAxisX].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisX].lAxisNum].dbVel;
// 	stAxisParam[MotorAxisX].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisX].lAxisNum].dbAcc;
// 	stAxisParam[MotorAxisX].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisX].lAxisNum].dbAcc;
// 
// 	stAxisParam[MotorAxisY].lAxisNum = MotorAxisY;
// 	stAxisParam[MotorAxisY].dbPos = 0;
// 	stAxisParam[MotorAxisY].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbVel;
// 	stAxisParam[MotorAxisY].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbAcc;
// 	stAxisParam[MotorAxisY].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbAcc;
// 
// 	if (m_pMotion->MotorMultiMove(POS_ABS_MODE, 2, stAxisParam) == FALSE)
// 	{
// 		return FALSE;
// 	}
// 
// 	if (m_pMotion->MotorAxisMove(POS_ABS_MODE, MotorAxisR, 0) == FALSE)
// 	{
// 		return FALSE;
// 	}

	return TRUE;
}


//=============================================================================
// Method		: IsReadyOk
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nPort
// Parameter	: BOOL bStatus
// Qualifier	:
// Last Update	: 2017/8/13 - 9:44
// Desc.		:
//=============================================================================
BOOL CMotionSequence::IsReadyOk(UINT nPort, BOOL bStatus /*= TRUE*/)
{
	CString str;
	m_bIoLoopStop = TRUE;

	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
		return FALSE;

	BOOL bRet(FALSE);

	clock_t	start_tm = clock();
	do
	{
		if (m_pDigitalIOCtrl->Get_DI_Status(nPort) == bStatus)
		{
			bRet = TRUE;
			break;
		}
		
		DoEvents(1);

	} while (((DWORD)(clock() - start_tm) < DIoWaitTime) && (m_bIoLoopStop));

	return bRet;
}

//=============================================================================
// Method		: CenterPointAdjustment
// Access		: public  
// Returns		: BOOL
// Parameter	: int iOffsetX
// Parameter	: int iOffsetY
// Qualifier	:
// Last Update	: 2017/8/12 - 21:44
// Desc.		:
//=============================================================================
BOOL CMotionSequence::CenterPointAdjustment(int iOffsetX, int iOffsetY)
{
	return TRUE;
}

//=============================================================================
// Method		: RotateAdjustment
// Access		: public  
// Returns		: BOOL
// Parameter	: double dbDegree
// Qualifier	:
// Last Update	: 2017/8/13 - 9:16
// Desc.		:
//=============================================================================
BOOL CMotionSequence::RotateAdjustment(double dbDegree)
{
	return TRUE;
}


//=============================================================================
// Method		: ColletDegreeMove
// Access		: public  
// Returns		: BOOL
// Parameter	: __in AXT_MOTION_ABSREL_MODE nMode
// Parameter	: __in double dDegree
// Qualifier	:
// Last Update	: 2017/10/19 - 13:55
// Desc.		:
//=============================================================================
BOOL CMotionSequence::ColletDegreeMove(__in AXT_MOTION_ABSREL_MODE nMode, __in double dDegree)
{
// 	double dPos_Degree = dDegree * 100;
// 
// 	if (m_pMotion->MotorAxisMove(nMode, MotorAxisR, dPos_Degree) == FALSE)
// 	{
// 		return FALSE;
// 	}

	return TRUE;
}



//=============================================================================
// Method		: FocusingDegreeMove
// Access		: public  
// Returns		: BOOL
// Parameter	: __in double dDegree
// Parameter	: __in double dConchoid
// Qualifier	:
// Last Update	: 2017/10/22 - 15:21
// Desc.		:
//=============================================================================
BOOL CMotionSequence::FocusingDegreeMove(__in double dDegree, __in double dConchoid)
{
// 	double dPos_Degree		= dDegree * 100;
// 	double dPos_Conchoid	= dConchoid * 1000;
// 
// 	ST_AxisMoveParam stAxisParam[DEF_MULTI_AXIS_CTRL];
// 
// 	stAxisParam[0].lAxisNum		= MotorAxisY;
// 	stAxisParam[0].dbPos		= dPos_Conchoid;
// 	stAxisParam[0].dbVel		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbVel;
// 	stAxisParam[0].dbAcc		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbAcc;
// 	stAxisParam[0].dbDec		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbAcc;
// 
// 	stAxisParam[1].lAxisNum		= MotorAxisR;
// 	stAxisParam[1].dbPos		= dPos_Degree;
// 	stAxisParam[1].dbVel		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbVel;
// 	stAxisParam[1].dbAcc		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbAcc;
// 	stAxisParam[1].dbDec		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbAcc;
// 
// 	if (!m_pMotion->MotorAxisLinearMove(POS_REL_MODE, DEF_MULTI_AXIS_CTRL, stAxisParam))
// 	{
// 		return FALSE;
//	}

	return TRUE;
}


//************************************
// Method:    FocusingInitMove
// FullName:  CMotionSequence::FocusingInitMove
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: __in double dInitY
// Parameter: __in double dInitR
//************************************
BOOL CMotionSequence::FocusingInitMove(__in double dInitY, __in double dInitR)
{
// 	ST_AxisMoveParam stAxisParam[DEF_MULTI_AXIS_CTRL];
// 
// 	stAxisParam[0].lAxisNum = MotorAxisY;
// 	stAxisParam[0].dbPos = dInitY;
// 	stAxisParam[0].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbVel;
// 	stAxisParam[0].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbAcc;
// 	stAxisParam[0].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbAcc;
// 
// 	stAxisParam[1].lAxisNum = MotorAxisR;
// 	stAxisParam[1].dbPos = dInitR;
// 	stAxisParam[1].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbVel;
// 	stAxisParam[1].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbAcc;
// 	stAxisParam[1].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbAcc;
// 
// 	if (!m_pMotion->MotorAxisLinearMove(POS_ABS_MODE, DEF_MULTI_AXIS_CTRL, stAxisParam))
// 	{
// 		return FALSE;
// 	}

	return TRUE;
}

//=============================================================================
// Method		: InterlockMove
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/10/19 - 14:23
// Desc.		:
//=============================================================================
// BOOL CMotionSequence::InterlockMove(__in BOOL bOnOff)
// {
// 	// 보드 연결 상태 확인
// 	if (!m_pDigitalIOCtrl->AXTState())
// 	{
// 		m_Log.LogMsg_Err(_T("Disconnect IoBoard..."));
// 		return FALSE;
// 	}
// 
// 	m_pDigitalIOCtrl->SetOutPort(DO_InterLockIn, IO_SignalT_SetOff);
// 	m_pDigitalIOCtrl->SetOutPort(DO_InterLockOut, IO_SignalT_SetOff);
// 
// 	if (bOnOff == ON)
// 	{
// 		m_pDigitalIOCtrl->SetOutPort(DO_InterLockIn, IO_SignalT_SetOn);
// 	}
// 	else if (bOnOff == OFF)
// 	{
// 		m_pDigitalIOCtrl->SetOutPort(DO_InterLockOut, IO_SignalT_SetOn);
// 	}
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: CYLJigControl
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/9/27 - 19:43
// Desc.		:
//=============================================================================
// BOOL CMotionSequence::CYLJigControl(__in BOOL bOnOff)
// {
// 	// 보드 연결 상태 확인
// 	if (!m_pDigitalIOCtrl->AXTState())
// 	{
// 		m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
// 		return FALSE;
// 	}
// 
// 	// 안전 센서
// // 	if (m_pDigitalIOCtrl->Get_DI_Status(DI_Safety) == FALSE && m_pstOption->Inspector.bUseAreaSen_Err == TRUE)
// // 	{
// // 		MessageView(_T("Safety Sensor Detect, Check the Safety Sensor!!"));
// // 		m_Log.LogMsg_Err(_T("Area Sensor Detect !!"));
// // 		return FALSE;
// // 
// 
// // 	if (bOnOff == ON)
// // 	{
// // 		m_Log.LogMsg(_T("Jig Cylinder In Start....."));
// // 
// // 		m_pDigitalIOCtrl->SetOutPort(DO_OutCylinder,	IO_SignalT_SetOff);
// // 		m_pDigitalIOCtrl->SetOutPort(DO_InCylinder,		IO_SignalT_SetOn);
// // 
// // 		if (IsReadyOk(DI_InCylinder) == TRUE)
// // 		{
// // 			m_Log.LogMsg(_T("Jig Cylinder In End....."));
// // 			return TRUE;
// // 		}
// // 		else
// // 		{
// // 			m_Log.LogMsg(_T("Jig Cylinder In End Error....."));
// // 			return FALSE;
// // 		}
// // 	}
// // 	else if (bOnOff == OFF)
// // 	{
// // 		m_Log.LogMsg(_T("Jig Cylinder Out Start....."));
// // 
// // 		m_pDigitalIOCtrl->SetOutPort(DO_OutCylinder, IO_SignalT_SetOn);
// // 		m_pDigitalIOCtrl->SetOutPort(DO_InCylinder, IO_SignalT_SetOff);
// // 
// // 		if (IsReadyOk(DI_OutCylinder) == TRUE)
// // 		{
// // 			m_Log.LogMsg(_T("Jig Cylinder Out End....."));
// // 			return TRUE;
// // 		}
// // 		else
// // 		{
// // 			m_Log.LogMsg(_T("Jig Cylinder Out End Error....."));
// // 			return FALSE;
// // 		}
// 
// 	return TRUE;
// }


BOOL CMotionSequence::ModuleFixUnFixCYLMotion(__in BOOL bFixUnFix)
{
// 	보드 연결 상태 확인
// 	if (!m_pDigitalIOCtrl->AXTState())
// 	{
// 		m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
// 		return FALSE;
// 	}
// 
// 	m_pDigitalIOCtrl->SetOutPort(DO_ModuleFixCYL, IO_SignalT_SetOff);
// 	m_pDigitalIOCtrl->SetOutPort(DO_ModuleUnFixCYL, IO_SignalT_SetOff);
// 
// 	if (bFixUnFix == FIX)
// 	{
// 		m_Log.LogMsg(_T("Module Fix Cylinder Start....."));
// 
// 		m_pDigitalIOCtrl->SetOutPort(DO_ModuleFixCYL, IO_SignalT_SetOn);
// 
// 		if (IsReadyOk(DI_ModuleFixCYLSensor) == TRUE)
// 		{
// 			m_Log.LogMsg(_T("Module Fix Cylinder End....."));
// 			return TRUE;
// 		}
// 		else
// 		{
// 			m_Log.LogMsg(_T("Module Fix Cylinder End Error....."));
// 			return FALSE;
// 		}
// 	}
// 	else if (bFixUnFix == UNFIX)
// 	{
// 		m_Log.LogMsg(_T("Module UnFix Cylinder Start....."));
// 
// 		m_pDigitalIOCtrl->SetOutPort(DO_ModuleUnFixCYL, IO_SignalT_SetOn);
// 
// 		if (IsReadyOk(DI_ModuleUnFixCYLSensor) == TRUE)
// 		{
// 			m_Log.LogMsg(_T("Module UnFix Cylinder End....."));
// 			return TRUE;
// 		}
// 		else
// 		{
// 			m_Log.LogMsg(_T("Module UnFix Cylinder End Error....."));
// 			return FALSE;
// 		}
// 	}

	return TRUE;
}


BOOL CMotionSequence::PCBFixUnFixCYLMotion(__in BOOL bFixUnFix)
{
	// 보드 연결 상태 확인
// 	if (!m_pDigitalIOCtrl->AXTState())
// 	{
// 		m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
// 		return FALSE;
// 	}
// 
// 	m_pDigitalIOCtrl->SetOutPort(DO_PCBFixCYL, IO_SignalT_SetOff);
// 	m_pDigitalIOCtrl->SetOutPort(DO_PCBUnFixCYL, IO_SignalT_SetOff);
// 
// 	if (bFixUnFix == FIX)
// 	{
// 		m_Log.LogMsg(_T("PCB Fix Cylinder Start....."));
// 
// 		m_pDigitalIOCtrl->SetOutPort(DO_PCBFixCYL, IO_SignalT_SetOn);
// 
// 		if (IsReadyOk(DI_PCBFixCYLSensor) == TRUE)
// 		{
// 			m_Log.LogMsg(_T("PCB Fix Cylinder End....."));
// 			return TRUE;
// 		}
// 		else
// 		{
// 			m_Log.LogMsg(_T("PCB Fix Cylinder End Error....."));
// 			return FALSE;
// 		}
// 	}
// 	else if (bFixUnFix == UNFIX)
// 	{
// 		m_Log.LogMsg(_T("PCB UnFix Cylinder Start....."));
// 
// 		m_pDigitalIOCtrl->SetOutPort(DO_PCBUnFixCYL, IO_SignalT_SetOn);
// 
// 		if (IsReadyOk(DI_PCBUnFixCYLSensor) == TRUE)
// 		{
// 			m_Log.LogMsg(_T("PCB UnFix Cylinder End....."));
// 			return TRUE;
// 		}
// 		else
// 		{
// 			m_Log.LogMsg(_T("PCB UnFix Cylinder End Error....."));
// 			return FALSE;
// 		}
// 	}

	return TRUE;
}


BOOL CMotionSequence::DriverInOutCYLMotion(__in BOOL bFixUnFix)
{
	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
	{
		m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
		return FALSE;
	}
	
// 	m_pDigitalIOCtrl->SetOutPort(DO_DriverInCYL, IO_SignalT_SetOff);
// 	m_pDigitalIOCtrl->SetOutPort(DO_DriverOutCYL, IO_SignalT_SetOff);

// 	if (bFixUnFix == FIX)
// 	{
// 		m_Log.LogMsg(_T("Driver In Cylinder Start....."));
// 
// 		m_pDigitalIOCtrl->SetOutPort(DO_DriverInCYL, IO_SignalT_SetOn);
// 
// 		if (IsReadyOk(DI_DriverInCYLSensor) == TRUE)
// 		{
// 			m_Log.LogMsg(_T("Driver In Cylinder End....."));
// 			return TRUE;
// 		}
// 		else
// 		{
// 			m_Log.LogMsg(_T("Driver In Cylinder End Error....."));
// 			return FALSE;
// 		}
// 	}
// 	else if (bFixUnFix == UNFIX)
// 	{
// 		m_Log.LogMsg(_T("Driver Out Cylinder Start....."));
// 
// 		m_pDigitalIOCtrl->SetOutPort(DO_DriverOutCYL, IO_SignalT_SetOn);
// 
// 		if (IsReadyOk(DI_DriverOutCYLSensor) == TRUE)
// 		{
// 			m_Log.LogMsg(_T("Driver Out Cylinder End....."));
// 			return TRUE;
// 		}
// 		else
// 		{
// 			m_Log.LogMsg(_T("Driver Out Cylinder End Error....."));
// 			return FALSE;
// 		}
// 	}

	return TRUE;
}

//=============================================================================
// Method		: ReleaseCYLMotion
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/12/27 - 15:15
// Desc.		:
//=============================================================================
BOOL CMotionSequence::ReleaseCYLMotion()
{
// 	if (!m_pDigitalIOCtrl->AXTState())
// 	{
// 		m_Log.LogMsg_Err(_T("Disconnect IoBoard..."));
// 		return FALSE;
// 	}
// 
// 	// Driver Out
// 	if(!DriverInOutCYLMotion(OFF))
// 	{
// 		m_Log.LogMsg_Err(_T("Driver In/Out Cylinder Error"));
// 		return FALSE;
// 	}
// 
// 	// PCB Un Fix
// 	if (!PCBFixUnFixCYLMotion(UNFIX))
// 	{
// 		m_Log.LogMsg_Err(_T("PCB Fix/UnFix Cylinder Error"));
// 		return FALSE;
// 	}
// 
// 	// Module Un Fix
// 	if (!ModuleFixUnFixCYLMotion(UNFIX))
// 	{
// 		m_Log.LogMsg_Err(_T("Module Fix/UnFix Cylinder Error"));
// 		return FALSE;
// 	}

	return TRUE;
}

//=============================================================================
// Method		: TestMotionSequence
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/8/13 - 10:28
// Desc.		:
//=============================================================================
// BOOL CMotionSequence::TestMotionSequence(__in BOOL bOnOff)
// {
// 	// 보드 연결 상태 확인
// 	if (!m_pDigitalIOCtrl->AXTState())
// 	{
// 		m_Log.LogMsg_Err(_T("Disconnect IoBoard..."));
// 		return FALSE;
// 	}
// 
// 	// 안전 센서
// // 	if (m_pDigitalIOCtrl->Get_DI_Status(DI_Safety) == FALSE && m_pstOption->Inspector.bUseAreaSen_Err == TRUE)
// // 	{
// // 		MessageView(_T("Safety Sensor Detect, Check the Safety Sensor!!"));
// // 		m_Log.LogMsg_Err(_T("Area Sensor Detect !!"));
// // 		return FALSE;
// // 
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: ButtonLampControl
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nPort
// Parameter	: __in enum_IO_SignalType enType
// Qualifier	:
// Last Update	: 2017/9/18 - 11:46
// Desc.		:
//=============================================================================
BOOL CMotionSequence::ButtonLampControl(__in UINT nPort, __in enum_IO_SignalType enType)
{
	CString szMassage;

// 	switch (nPort)
// 	{
// 	case DO_ModuleFixBtnLamp:
// 		szMassage.Format(_T("Module Fix Button Lamp"));
// 		break;
// 
// 	case DO_ModuleUnFixBtnLamp:
// 		szMassage.Format(_T("Module UnFix Button Lamp"));
// 		break;
// 
// 	case DO_PCBFixBtnLamp:
// 		szMassage.Format(_T("PCB Fix Button Lamp"));
// 		break;
// 
// 	case DO_PCBUnFixBtnLamp:
// 		szMassage.Format(_T("PCB UnFix Button Lamp"));
// 		break;
// 
// 	case DO_DriverInBtnLamp:
// 		szMassage.Format(_T("Driver In Button Lamp"));
// 		break;
// 
// 	case DO_DriverOutBtnLamp:
// 		szMassage.Format(_T("Driver Out Button Lamp"));
// 		break;
// 
// 	case DO_StartLeftBtnLamp:
// 		szMassage.Format(_T("Start Left Button Lamp"));
// 		break;
// 
// 	case DO_StartRight_StopBtnLamp:
// 		szMassage.Format(_T("Start Right/Stop Button Lamp"));
// 		break;
// 
// 	default:
// 		break;
// 	}
// 
// 	if (m_pDigitalIOCtrl->SetOutPort(nPort, enType) == FALSE)
// 	{
// 		szMassage = szMassage + _T("Error");
// 		m_Log.LogMsg(szMassage);
// 		return FALSE;
// 	}
// 
// 	szMassage = szMassage + _T("Ok");
// 	m_Log.LogMsg(szMassage);
	return TRUE;
}

//=============================================================================
// Method		: TowerLamp
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nColor
// Parameter	: __in BOOL bMode
// Parameter	: long lTime
// Qualifier	:
// Last Update	: 2017/9/9 - 15:11
// Desc.		:
//=============================================================================
BOOL CMotionSequence::TowerLamp(UINT nColor, __in BOOL bMode, long lTime /*= 2000*/)
{
	enum_IO_SignalType enType;

	if (m_pDigitalIOCtrl == NULL)
		return FALSE;

	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
		return FALSE;

	// 점멸 기능으로 동작
	if (bMode == ON)
		enType = IO_SignalT_SetOn;

	if (bMode == OFF)
		enType = IO_SignalT_SetOff;

	for (UINT n = 0; n < 3; n++)
	{
		m_pDigitalIOCtrl->Set_DO_Status(DO_TowerLamp_Red + n, IO_SignalT_SetOff);
	}

	if (m_pDigitalIOCtrl->Set_DO_Status(DO_TowerLamp_Red + nColor, enType) != AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: TowerLampBuzzer
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nSound
// Parameter	: __in BOOL bMode
// Parameter	: long lTime
// Qualifier	:
// Last Update	: 2017/9/9 - 15:11
// Desc.		:
//=============================================================================
BOOL CMotionSequence::TowerLampBuzzer(UINT nSound, __in BOOL bMode, long lTime /*= 2000*/)
{
	enum_IO_SignalType enType;

	if (m_pDigitalIOCtrl == NULL)
		return FALSE;

	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
		return FALSE;

	// 점멸 기능으로 동작
	if (bMode == ON)
		enType = IO_SignalT_PulseOn;

	if (bMode == OFF)
		enType = IO_SignalT_PulseOff;

	for (UINT n = 0; n < 5; n++)
	{
		m_pDigitalIOCtrl->Set_DO_Status(DO_TowerLamp_Buzzer, IO_SignalT_PulseOff);
	}

	if (m_pDigitalIOCtrl->Set_DO_Status(nSound, enType) != AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: LightVoltControl
// Access		: public  
// Returns		: BOOL
// Parameter	: __in enLightCtrl enItem
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/9/5 - 11:36
// Desc.		:
//=============================================================================
BOOL CMotionSequence::LightVoltControl(__in enLightCtrl enItem, __in float fVoltage)
{
	if (m_pLightBrd[0] == NULL || m_pLightBrd[1] == NULL)
		return FALSE;

	if (m_pstModelInfo == NULL)
		return FALSE;

	switch (enItem)
	{
	case LightChat_Center:
		for (UINT nCh = 0; nCh < 4; nCh++)
		{
			if (m_pLightBrd[0]->Send_OutputVolt(Slot_A + nCh, fVoltage) == FALSE)
			{
				for (UINT nIdx = 0; nIdx < 4; nIdx++)
				{
					m_pLightBrd[0]->Send_OutputVolt(Slot_A + nIdx, 0);
				}

				return FALSE;
			}
		}
		break;

	case LightChat_Left:
		for (UINT nCh = 0; nCh < 2; nCh++)
		{
			if (m_pLightBrd[1]->Send_OutputVolt(Slot_A + nCh, fVoltage) == FALSE)
			{
				for (UINT nIdx = 0; nIdx < 2; nIdx++)
				{
					m_pLightBrd[1]->Send_OutputVolt(Slot_A + nIdx, 0);
				}

				return FALSE;
			}
		}
		break;

	case LightChat_Right:
		for (UINT nCh = 0; nCh < 2; nCh++)
		{
			if (m_pLightBrd[1]->Send_OutputVolt(Slot_C + nCh, fVoltage) == FALSE)
			{
				for (UINT nIdx = 0; nIdx < 2; nIdx++)
				{
					m_pLightBrd[1]->Send_OutputVolt(Slot_C + nIdx, 0);
				}

				return FALSE;
			}
		}
		break;

	case LightChat_Particle:
		if (m_pLightBrd[0]->Send_OutputVolt(Slot_E, fVoltage) == FALSE)
		{
			m_pLightBrd[0]->Send_OutputVolt(Slot_E, 0);
			return FALSE;
		}
		break;

	default:
		break;
	}

	return TRUE;
}

//=============================================================================
// Method		: LightCurrentControl
// Access		: public  
// Returns		: BOOL
// Parameter	: __in enLightCtrl enItem
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/9/5 - 11:36
// Desc.		:
//=============================================================================
BOOL CMotionSequence::LightCurrentControl(__in enLightCtrl enItem, __in UINT nCode)
{
	if (m_pLightBrd[0] == NULL || m_pLightBrd[1] == NULL)
		return FALSE;

	if (m_pstModelInfo == NULL)
		return FALSE;

	switch (enItem)
	{
	case LightChat_Center:
		for (UINT nCh = 0; nCh < 4; nCh++)
		{
			if (m_pLightBrd[0]->Send_AmbientLightCurrent(Slot_A + nCh, nCode) == FALSE)
			{
				for (UINT nIdx = 0; nIdx < 4; nIdx++)
				{
					m_pLightBrd[0]->Send_OutputVolt(Slot_A + nIdx, 0);
				}

				return FALSE;
			}
		}
		break;

	case LightChat_Left:
		for (UINT nCh = 0; nCh < 2; nCh++)
		{
			if (m_pLightBrd[1]->Send_AmbientLightCurrent(Slot_A + nCh, nCode) == FALSE)
			{
				for (UINT nIdx = 0; nIdx < 2; nIdx++)
				{
					m_pLightBrd[1]->Send_OutputVolt(Slot_A + nIdx, 0);
				}

				return FALSE;
			}
		}
		break;

	case LightChat_Right:
		for (UINT nCh = 0; nCh < 2; nCh++)
		{
			if (m_pLightBrd[1]->Send_AmbientLightCurrent(Slot_C + nCh, nCode) == FALSE)
			{
				for (UINT nIdx = 0; nIdx < 2; nIdx++)
				{
					m_pLightBrd[1]->Send_OutputVolt(Slot_C + nIdx, 0);
				}

				return FALSE;
			}
		}
		break;

	case LightChat_Particle:
		if (m_pLightBrd[0]->Send_AmbientLightCurrent(Slot_E, nCode) == FALSE)
		{
			m_pLightBrd[0]->Send_OutputVolt(Slot_E, 0);
			return FALSE;
		}
		break;

	default:
		break;
	}

	return TRUE;
}

LRESULT CMotionSequence::MotorGetStatus()
{
	// Handle 확인
	if (NULL == m_pMotion)
		return RC_Invalid_Handle;

	// 보드 OPEN 상태 확인
	if (FALSE == m_pMotion->GetBoardOpen())
		return RC_Motion_Err_BoardOpen;

	// 모터 알람 여부 확인
	for (UINT nIdx = 0; nIdx < (UINT)m_pMotion->m_AllMotorData.MotorInfo.lMaxAxisCnt; nIdx++)
	{
		if (TRUE == m_pMotion->m_AllMotorData.pMotionParam[nIdx].bAxisUse)
		{
			if (TRUE == m_pMotion->GetAlarmSenorStatus(nIdx))
			{
				m_Log.LogMsg_Err(_T("%s, AXIS[%d]"), g_szResultCode[RC_Motor_Err_Alarm], nIdx);
				return RC_Motion_Err_Alarm;
			}
		}
	}

	return RC_OK;
}

//=============================================================================
// Method		: MotorMoveAxis
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in int iAxis
// Parameter	: __in double dbPos
// Qualifier	:
// Last Update	: 2017/10/14 - 20:42
// Desc.		:
//=============================================================================
LRESULT CMotionSequence::MotorMoveAxis(__in int iAxis, __in double dbPos)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	if (FALSE == m_pMotion->MotorAxisMove(POS_ABS_MODE, iAxis, dbPos))
		return RC_Motion_Err_MoveAxis;

	return lReturn;
}

LRESULT CMotionSequence::OnAction_Load()
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	if (m_pDigitalIOCtrl->Get_DI_Status(DI_SensorParticleIn) == TRUE)
	{
		//!SH _190107: 이물 광원 나와 있다고 메세지 출력 필요
		lReturn = RC_Motion_Err_MoveAxis_Particle;
		return lReturn;
	}

	//!SH _190128: x,y,distance 와 이물 광원 붙어 있을 때 처리를 생각 해봐야 함.

	if (!MultiMoveAxis(m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_X], m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_Y], m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_Dist])){
		lReturn = RC_Motion_Err_MoveAxis;
	}

#if 0

	ST_AxisMoveParam stAxisParam[3];
	int nAxisNum = 3;

	stAxisParam[0].lAxisNum = AX_StageX;
	stAxisParam[1].lAxisNum = AX_StageY;
	stAxisParam[2].lAxisNum = AX_StageDistance;

	stAxisParam[0].dbPos = m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_X];
	stAxisParam[1].dbPos = m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_Y];
	stAxisParam[2].dbPos = m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_Dist];

	for (UINT nAxis = 0; nAxis < nAxisNum; nAxis++)
	{
		stAxisParam[nAxis].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbVel;
		stAxisParam[nAxis].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
		stAxisParam[nAxis].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
	}
	
	lReturn = m_pMotion->MotorMultiMove(POS_ABS_MODE, nAxisNum, stAxisParam);
	lReturn = MotorMoveAxis(AX_StageDistance, m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_Dist]);

#else
// 	ST_AxisMoveParam stAxisParam[2];
// 
// 	stAxisParam[0].lAxisNum = AX_StageX;
// 	stAxisParam[1].lAxisNum = AX_StageY;
// 	//stAxisParam[2].lAxisNum = AX_StageDistance;
// 
// 	stAxisParam[0].dbPos = m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_X];
// 	stAxisParam[1].dbPos = m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_Y];
// 	//stAxisParam[2].dbPos = m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_Dist];
// 
// 	for (UINT nAxis = 0; nAxis < 2; nAxis++)
// 	{
// 		stAxisParam[nAxis].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbVel;
// 		stAxisParam[nAxis].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
// 		stAxisParam[nAxis].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
// 	}
// 
// 	lReturn = m_pMotion->MotorMultiMove(POS_ABS_MODE, 2, stAxisParam);
// 	lReturn = MotorMoveAxis(AX_StageDistance, m_pstTeachInfo->dbTeachData[TC_ImageTest_Loading_Dist]);

#endif
	return lReturn;
}

// 멀티축 움직이기
BOOL CMotionSequence::MultiMoveAxis(double dbX, double dbY, double dbDistance, bool bParticleLoadMode /*= FALSE*/)
{
	BOOL bRet = TRUE;

	ST_AxisMoveParam stAxisParam[3];

	if (bParticleLoadMode == TRUE)
	{
		int nAxisNum = 2;

		LRESULT lReturn  = MotorMoveAxis(AX_StageDistance, dbDistance);

		if (lReturn != RC_OK)
		{
			return FALSE;
		}

		stAxisParam[0].lAxisNum = AX_StageX;
		stAxisParam[1].lAxisNum = AX_StageY;

		stAxisParam[0].dbPos = dbX;
		stAxisParam[1].dbPos = dbY;

		for (UINT nAxis = 0; nAxis < nAxisNum; nAxis++)
		{
			stAxisParam[nAxis].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbVel;
			stAxisParam[nAxis].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
			stAxisParam[nAxis].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
			
			AxmMotSetAbsRelMode(stAxisParam[nAxis].lAxisNum, POS_ABS_MODE);
		
		}



		for (int _a = 0; _a < nAxisNum; _a++)
		{
			double dwRetCode = AxmMoveStartPos(stAxisParam[_a].lAxisNum, stAxisParam[_a].dbPos, stAxisParam[_a].dbVel,
				stAxisParam[_a].dbAcc,
				stAxisParam[_a].dbDec);
			CString strData;

			if (dwRetCode != AXT_RT_SUCCESS)
			{
				bRet = FALSE;
			}
			else
			{
			}
		}


		// 축별로 상태 확인하자.
		clock_t start_tm = clock();
		DWORD dwStatus;

		BOOL bStopCheck = TRUE;
		do
		{
			bStopCheck = TRUE;
			// 동작 완료 여부를 확인하자
			for (int _a = 0; _a < nAxisNum; _a++)
			{
				AxmStatusReadInMotion(stAxisParam[_a].lAxisNum, &dwStatus);

				if (dwStatus)
					bStopCheck = FALSE;
			}

			if (bStopCheck)
				break;

			DoEvents(10);

		} while ((DWORD)(clock() - start_tm) < 100000);

		// 현재 위치 인지 확인
		if (bStopCheck)
		{
			for (int _a = 0; _a < nAxisNum; _a++)
			{
				double dbPos = GetCurrentPos(stAxisParam[_a].lAxisNum);

				if (!(dbPos == stAxisParam[_a].dbPos))
					bRet = FALSE;
			}
		}
		else
		{
			bRet = FALSE;
		}




	}
	else{
		int nAxisNum = 3;

		stAxisParam[0].lAxisNum = AX_StageX;
		stAxisParam[1].lAxisNum = AX_StageY;
		stAxisParam[2].lAxisNum = AX_StageDistance;

		stAxisParam[0].dbPos = dbX;
		stAxisParam[1].dbPos = dbY;
		stAxisParam[2].dbPos = dbDistance;

		for (UINT nAxis = 0; nAxis < nAxisNum; nAxis++)
		{
			stAxisParam[nAxis].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbVel;
			stAxisParam[nAxis].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
			stAxisParam[nAxis].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
			AxmMotSetAbsRelMode(stAxisParam[nAxis].lAxisNum, POS_ABS_MODE);
		}



		for (int _a = 0; _a < nAxisNum; _a++)
		{
			double dwRetCode = AxmMoveStartPos(stAxisParam[_a].lAxisNum, stAxisParam[_a].dbPos, stAxisParam[_a].dbVel,
				stAxisParam[_a].dbAcc,
				stAxisParam[_a].dbDec);
			CString strData;

			if (dwRetCode != AXT_RT_SUCCESS)
			{
				bRet = FALSE;
			}
			else
			{
			}
		}


		// 축별로 상태 확인하자.
		clock_t start_tm = clock();
		DWORD dwStatus;

		BOOL bStopCheck = TRUE;
		do
		{
			bStopCheck = TRUE;
			// 동작 완료 여부를 확인하자
			for (int _a = 0; _a < nAxisNum; _a++)
			{
				AxmStatusReadInMotion(stAxisParam[_a].lAxisNum, &dwStatus);

				if (dwStatus)
					bStopCheck = FALSE;
			}

			if (bStopCheck)
				break;

			DoEvents(10);

		} while ((DWORD)(clock() - start_tm) < 100000);

		// 현재 위치 인지 확인
		if (bStopCheck)
		{
			for (int _a = 0; _a < nAxisNum; _a++)
			{
				double dbPos = GetCurrentPos(stAxisParam[_a].lAxisNum);

				if (!(dbPos == stAxisParam[_a].dbPos))
					bRet = FALSE;
			}
		}
		else
		{
			bRet = FALSE;
		}
	}

	

	return bRet;
}

double CMotionSequence::GetCurrentPos(int nAxisNum)
{
	double dCnt = 0.;

	// 현재 지령펄스와, Encoder펄스를 확인하는 함수
	AxmStatusGetCmdPos(nAxisNum, &dCnt);

	//dCnt = dCnt * m_pMotion->m_AllMotorData.pMotionParam[nAxisNum].dbGearRatio;
	//dCnt = dCnt * 1.0;
	dCnt = dCnt * 1.0;

	// 	if(nAxisNum == AXIS_R)
	// 		dCnt += param[AXIS_R].OriginOffset;

	return dCnt;
}
LRESULT CMotionSequence::OnAction_Inspect(__in double dbTestAxisX, __in  double dbTestAxisY, __in  double dbTestDistance)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	if (m_pDigitalIOCtrl->Get_DI_Status(DI_SensorParticleIn) == TRUE)
	{
		//!SH _190107: 이물 광원 나와 있다고 메세지 출력 필요
		lReturn = RC_Motion_Err_MoveAxis_Particle;
		return lReturn;
	}
	//!SH _190130 : X,Y 축 1mm 당 690 pulse, distance 1mm 당 1000 pulse, 원점에서 차트 거리 106mm  
	double dbPosition = (dbTestDistance - 106) * 1000;

	if (!MultiMoveAxis(dbTestAxisX * 690, dbTestAxisY * 690, dbPosition)){
		lReturn = RC_Motion_Err_MoveAxis;
	}

	return lReturn;
}

LRESULT CMotionSequence::OnAction_Par_Load()
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	if (m_pDigitalIOCtrl->Get_DI_Status(DI_SensorParticleIn) == TRUE && m_pMotion->GetCurrentPos(AX_StageDistance) < m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
	{
		//!SH _190107: 이물 광원 나와 있다고 메세지 출력 필요
		lReturn = RC_Motion_Err_MoveAxis_Particle;
		return lReturn;
	}

	if (!MultiMoveAxis(m_pstTeachInfo->dbTeachData[TC_ImageTest_Par_Inspection_X], m_pstTeachInfo->dbTeachData[TC_ImageTest_Par_Inspection_Y], m_pstTeachInfo->dbTeachData[TC_ImageTest_Par_Loading_Dist],TRUE)){
		lReturn = RC_Motion_Err_MoveAxis;
	}

	return lReturn;
}

LRESULT CMotionSequence::OnAction_Par_Inspect()
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	//if (m_pDigitalIOCtrl->Get_DI_Status(DI_SensorParticleIn) == TRUE)
	if (m_pMotion->GetCurrentPos(AX_StageDistance) < m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_Start] || m_pDigitalIOCtrl->Get_DI_Status(DI_SensorParticleOut) == TRUE)
	{
		//!SH _190107: 이물 광원 나와 있다고 메세지 출력 필요
		lReturn = RC_Motion_Err_MoveAxis_Particle;
		return lReturn;
	}

	if (!MultiMoveAxis(m_pstTeachInfo->dbTeachData[TC_ImageTest_Par_Inspection_X], m_pstTeachInfo->dbTeachData[TC_ImageTest_Par_Inspection_Y], m_pstTeachInfo->dbTeachData[TC_ImageTest_Par_Inspectoin_Dist])){
		lReturn = RC_Motion_Err_MoveAxis;
	}

	return lReturn;
}

LRESULT CMotionSequence::OnAction_Par_Cylinder_In()
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;


	if (m_pMotion->GetCurrentPos(AX_StageDistance) < m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End] || m_pMotion->GetCurrentPos(AX_StageDistance) > m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_Start])
	{
		BOOL bStatus = FALSE;

		//!SH _190104: m_pDigitalIOCtrl 이 NULL 일때 처리 필요!
		m_pDigitalIOCtrl->Set_DO_Status(DO_SOLParticleOut, (enum_IO_SignalType)IO_SignalT_SetOff);
		m_pDigitalIOCtrl->Set_DO_Status(DO_SOLParticleIn, (enum_IO_SignalType)IO_SignalT_SetOff);
		lReturn = m_pDigitalIOCtrl->Set_DO_Status(DO_SOLParticleIn, (enum_IO_SignalType)IO_SignalT_SetOn);
		
		for (int i = 0; i < 100; i++)
		{
			if (m_pDigitalIOCtrl->Get_DI_Status(DI_SensorParticleIn) == TRUE)
			{
				bStatus = TRUE;
				break;
			}

			DoEvents(33);
		}

		if (bStatus == TRUE)
		{
			lReturn = RCA_OK;
		}
		else
		{
			lReturn = RCA_MachineCheck;
		}
	}
	
	return lReturn;
}

LRESULT CMotionSequence::OnAction_Par_Cylinder_Out(BOOL bInitMode /*= FALSE*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	if ((m_pMotion->GetCurrentPos(AX_StageDistance) < m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End] || m_pMotion->GetCurrentPos(AX_StageDistance) > m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_Start]) || bInitMode == TRUE)
	{
		BOOL bStatus = FALSE;

		//!SH _190104: m_pDigitalIOCtrl 이 NULL 일때 처리 필요!
		m_pDigitalIOCtrl->Set_DO_Status(DO_SOLParticleOut, (enum_IO_SignalType)IO_SignalT_SetOff);
		m_pDigitalIOCtrl->Set_DO_Status(DO_SOLParticleIn, (enum_IO_SignalType)IO_SignalT_SetOff);
		lReturn = m_pDigitalIOCtrl->Set_DO_Status(DO_SOLParticleOut, (enum_IO_SignalType)IO_SignalT_SetOn);

		for (int i = 0; i < 100; i++)
		{
			if (m_pDigitalIOCtrl->Get_DI_Status(DI_SensorParticleOut) == TRUE)
			{
				bStatus = TRUE;
				break;
			}

			DoEvents(33);
		}

		if (bStatus == TRUE)
		{
			lReturn = RCA_OK;
		}
		else
		{
			lReturn = RCA_MachineCheck;
		}
	}

	return lReturn;
}

LRESULT CMotionSequence::OnAction_LED_ON()
{
	LRESULT lReturn = RC_OK;
	if ((m_pDigitalIOCtrl->Get_DO_Status(DO_RLYParticleIRLight) == IO_SignalT_SetOff)
		&& (m_pDigitalIOCtrl->Get_DO_Status(DO_RLYParticleLEDLight) == IO_SignalT_SetOn))
	{
		return lReturn;
	}
	m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleIRLight, (enum_IO_SignalType)IO_SignalT_SetOff);
	m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleLEDLight, (enum_IO_SignalType)IO_SignalT_SetOff);
	lReturn = m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleLEDLight, (enum_IO_SignalType)IO_SignalT_SetOn);
		
	Sleep(1000);
	return lReturn;
}

LRESULT CMotionSequence::OnAction_LED_OFF()
{
	LRESULT lReturn = RC_OK;
	if ((m_pDigitalIOCtrl->Get_DO_Status(DO_RLYParticleIRLight) == IO_SignalT_SetOff)
		&& (m_pDigitalIOCtrl->Get_DO_Status(DO_RLYParticleLEDLight) == IO_SignalT_SetOff))
	{
		return lReturn;
	}
	m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleIRLight, (enum_IO_SignalType)IO_SignalT_SetOff);
	m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleLEDLight, (enum_IO_SignalType)IO_SignalT_SetOff);
	lReturn = m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleLEDLight, (enum_IO_SignalType)IO_SignalT_SetOff);

	Sleep(1000);
	return lReturn;
}

LRESULT CMotionSequence::OnAction_IR_LED_ON()
{
	LRESULT lReturn = RC_OK;
	if ((m_pDigitalIOCtrl->Get_DO_Status(DO_RLYParticleIRLight) == IO_SignalT_SetOn)
		&& (m_pDigitalIOCtrl->Get_DO_Status(DO_RLYParticleLEDLight) == IO_SignalT_SetOff))
	{
		return lReturn;
	}
	m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleIRLight, (enum_IO_SignalType)IO_SignalT_SetOff);
	m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleLEDLight, (enum_IO_SignalType)IO_SignalT_SetOff);
	lReturn = m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleIRLight, (enum_IO_SignalType)IO_SignalT_SetOn);

	Sleep(1000);
	return lReturn;
}

LRESULT CMotionSequence::OnAction_IR_LED_OFF()
{
	LRESULT lReturn = RC_OK;
	if ((m_pDigitalIOCtrl->Get_DO_Status(DO_RLYParticleIRLight) == IO_SignalT_SetOff)
		&& (m_pDigitalIOCtrl->Get_DO_Status(DO_RLYParticleLEDLight) == IO_SignalT_SetOff))
	{
		return lReturn;
	}
	m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleIRLight, (enum_IO_SignalType)IO_SignalT_SetOff);
	m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleLEDLight, (enum_IO_SignalType)IO_SignalT_SetOff);
	lReturn = m_pDigitalIOCtrl->Set_DO_Status(DO_RLYParticleIRLight, (enum_IO_SignalType)IO_SignalT_SetOff);

	Sleep(1000);
	return lReturn;
}

//=============================================================================
// Method		: WarringView
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2017/7/12 - 10:56
// Desc.		:
//=============================================================================
BOOL CMotionSequence::MessageView(__in CString szText)
{
	BOOL bResult = FALSE;

	m_Dlg_Popup.PopupMessage(szText);
	if (m_Dlg_Popup.DoModal() == IDOK)
	{
		bResult = TRUE;
	}

	return bResult;
}

void CMotionSequence::OnSetting(BOOL bFinMode){

	
}

BOOL CMotionSequence::RunStatusLamp(UINT nColor, __in BOOL bMode)
{
	enum_IO_SignalType enType;

	if (m_pDigitalIOCtrl == NULL)
		return FALSE;

	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
		return FALSE;

	// 점멸 기능으로 동작
	if (bMode == ON)
		enType = IO_SignalT_SetOn;

	if (bMode == OFF)
		enType = IO_SignalT_SetOff;

	for (UINT n = 0; n < 3; n++)
	{
		m_pDigitalIOCtrl->Set_DO_Status(DO_LampRun + n, IO_SignalT_SetOff);
	}

	if (m_pDigitalIOCtrl->Set_DO_Status(nColor, enType) != AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}
