﻿//*****************************************************************************
// Filename	: Wnd_DeviceView.cpp
// Created	: 2016/05/29
// Modified	: 2016/09/22
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_DeviceView.h"

//=============================================================================
// CWnd_DeviceView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_DeviceView, CWnd_BaseView)

//=============================================================================
//
//=============================================================================
CWnd_DeviceView::CWnd_DeviceView()
{
	m_pDevice = NULL;
}

CWnd_DeviceView::~CWnd_DeviceView()
{
}

BEGIN_MESSAGE_MAP(CWnd_DeviceView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

//=============================================================================
// CWnd_DeviceView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_DeviceView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
int CWnd_DeviceView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_Wnd_IOTable.Create(NULL, _T(""), dwStyle, rectDummy, this, 10);

	return 0;
}

//=============================================================================
// Method		: CWnd_DeviceView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
void CWnd_DeviceView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin		= 5;
	int iSpacing	= 5;
	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;

	m_Wnd_IOTable.MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: CWnd_DeviceView::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
BOOL CWnd_DeviceView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}


//=============================================================================
// Method		: Set_IO_DI_OffsetData
// Access		: public  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/10/31 - 17:00
// Desc.		:
//=============================================================================
void CWnd_DeviceView::Set_IO_DI_OffsetData(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	m_Wnd_IOTable.Set_IO_DI_OffsetData(byBitOffset, bOnOff);
}

//=============================================================================
// Method		: Set_IO_DI_Data
// Access		: public  
// Returns		: void
// Parameter	: __in LPBYTE lpbyDIData
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2018/2/14 - 16:58
// Desc.		:
//=============================================================================
void CWnd_DeviceView::Set_IO_DI_Data(__in LPBYTE lpbyDIData, __in UINT nCount)
{
	m_Wnd_IOTable.Set_IO_DI_Data(lpbyDIData, nCount);
}

//=============================================================================
// Method		: Set_IO_DO_OffsetData
// Access		: public  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/14 - 16:58
// Desc.		:
//=============================================================================
void CWnd_DeviceView::Set_IO_DO_OffsetData(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	m_Wnd_IOTable.Set_IO_DO_OffsetData(byBitOffset, bOnOff);
}

//=============================================================================
// Method		: Set_IO_DO_Data
// Access		: public  
// Returns		: void
// Parameter	: __in LPBYTE lpbyDOData
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2018/2/14 - 16:58
// Desc.		:
//=============================================================================
void CWnd_DeviceView::Set_IO_DO_Data(__in LPBYTE lpbyDOData, __in UINT nCount)
{
	m_Wnd_IOTable.Set_IO_DO_Data(lpbyDOData, nCount);
}