﻿#ifndef Wnd_OperModeOp_h__
#define Wnd_OperModeOp_h__

#pragma once

#include "VGStatic.h"
#include "List_OperModeOp.h"
#include "Def_DataStruct.h"

// CWnd_OperModeOp

enum enOperModeStatic
{
	STI_OPER_DELAY,
	STI_OPER_MAX
};

static LPCTSTR	g_szOperModeStatic[] =
{
	_T("Delay"),
	NULL
};

enum enOperModeButton
{
	BTN_OPER_TEST,
	BTN_OPER_MAX,
};

static LPCTSTR	g_szOperModeButton[] =
{
	_T("TEST"),
	NULL
};

enum enOperModeComobox
{
	CMB_OPER_MAX = 1,
};

enum enOperModeEdit
{
	EDT_OPER_DELAY,
	EDT_OPER_MAX,
};
class CWnd_OperModeOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_OperModeOp)

public:
	CWnd_OperModeOp();
	virtual ~CWnd_OperModeOp();

	void SetUpdateData	();
	void GetUpdateData	();

	void SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	}

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

protected:

	ST_ModelInfo	*m_pstModelInfo;
	CList_OperModeOp m_List;

	CFont			m_font;

	CVGStatic			m_st_Item[STI_OPER_MAX];
	CButton				m_bn_Item[BTN_OPER_MAX];
	CComboBox			m_cb_Item[CMB_OPER_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_OPER_MAX];
	
	// 검사 항목이 다수 인경우
	UINT			m_nTestItemCnt;

	DECLARE_MESSAGE_MAP()

	afx_msg void OnSize				(UINT nType, int cx, int cy);
	afx_msg int	 OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void OnRangeBtnCtrl		(UINT nID);
	virtual BOOL PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // Wnd_OperModeOp_h__
