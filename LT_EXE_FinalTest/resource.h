//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// LT MainExecution.rc에서 사용되고 있습니다.
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_DLG_BARCODE                 101
#define IDD_PROPPAGE_OPTION             102
#define IDB_BOTTOM_LEFT                 103
#define IDP_SOCKETS_INIT_FAILED         104
#define IDB_BOTTOM_MIDDLE               104
#define IDD_DIALOG_PARTICLE             104
#define IDB_BOTTOM_RIGHT                105
#define IDD_DIALOG1                     105
#define IDD_DLG_REFBARCODE_OP           105
#define IDB_MIDDLE_LEFT                 106
#define IDR_IMAGE_LOAD                  106
#define IDB_MIDDLE_RIGHT                107
#define IDD_DLG_LOT                     107
#define IDB_TOP_LEFT                    108
#define IDD_DLG_LOT_OVERLAP             108
#define IDI_ICON2                       108
#define IDB_TOP_MIDDLE                  109
#define IDB_TOP_RIGHT                   110
#define IDR_POPUP_EDIT                  119
#define ID_STATUSBAR_PANE1              120
#define ID_STATUSBAR_PANE2              121
#define IDS_STATUS_PANE1                122
#define IDS_STATUS_PANE2                123
#define IDS_TOOLBAR_STANDARD            124
#define IDS_TOOLBAR_CUSTOMIZE           125
#define IDR_MAINFRAME                   128
#define IDR_MAINFRAME_256               129
#define ID_SET_STYLE                    201
#define IDS_STR_OPTION                  202
#define IDS_STR_OPT_PAGE_01             203
#define IDS_STR_OPT_PAGE_02             204
#define IDS_STR_OPT_PAGE_03             205
#define IDS_STR_OPT_PAGE_04             206
#define IDS_STR_OPT_PAGE_05             207
#define IDS_STR_OPT_PAGE_06             208
#define IDS_STR_OPT_PAGE_07             209
#define IDS_STR_OPT_PAGE_08             210
#define IDS_STR_OPT_PAGE_09             211
#define IDS_EDIT_MENU                   306
#define IDB_BITMAP_CHECK                311
#define IDB_BITMAP_CHECKNO              312
#define IDB_OUTPUT_TAB_ICONS            316
#define IDI_ICON_LOGO                   317
#define IDI_ICON_Luritech               318
#define IDI_ICON_CONNECT                320
#define IDD_DLG_CHK_PASSWORD            323
#define IDB_SELECT                      339
#define IDB_SELECT_NO                   340
#define IDB_CHECKED                     341
#define IDB_UNCHECKED                   342
#define IDB_SELECT_16                   343
#define IDB_SELECTNO_16                 344
#define IDB_CHECKED_16                  368
#define IDB_UNCHECKED_16                369
#define IDI_ICON_DEVICE                 374
#define IDI_ICON_TSP                    375
#define IDB_TEST_START                  378
#define IDB_TEST_START_R                380
#define IDB_LOAD_FILE                   382
#define IDB_LOAD_FILE_G                 383
#define IDB_START                       384
#define IDB_STOP                        385
#define IDB_BITMAP_Luritech             387
#define IDC_ED_PASSWORD                 1012
#define IDC_ST_PASSWORD                 1014
#define IDD_DLG_SEL_MODEL               1100
#define IDD_DLG_OPERATE_MODE            1101
#define IDD_DLG_ERR                     1103
#define IDD_DLG_POPUP                   1105
#define IDD_DLG_USER_CONFIG             1106
#define IDD_DLG_USER_CONFIG1            1107
#define IDD_DLG_PARTICLE_MANUAL         1107
#define IDD_DLG_CENTERPOINT_TUNNING     1108
#define IDD_DLG_CENTERPOINT_TUNNING1    1109
#define IDD_DLG_LEDTEST                 1109
#define IDD_DLG_FAILBOX_CONFIRM         1110
#define IDD_DLG_LABEL_PRINTER           1111
#define IDD_DLG_FAILBOX_CONFIRM1        1112
#define IDD_DLG_STARTUP_CHECK           1112
#define IDD_DLG_MASTER_MODE             1113
#define IDD_DLG_MASTER_TEST             1114
#define ID_TABVIEW_1                    3001
#define ID_TABVIEW_2                    3002
#define ID_TABVIEW_3                    3003
#define ID_TABVIEW_4                    3004
#define ID_TABVIEW_5                    3005
#define ID_TABVIEW_6                    3006
#define ID_TABVIEW_7                    3007
#define ID_TABVIEW_8                    3008
#define ID_TABVIEW_9                    3009
#define IDI_ICON_Delloyd                4107
#define IDI_ICON_C_C                    4108

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40002
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
