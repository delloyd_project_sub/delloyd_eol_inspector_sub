﻿//*****************************************************************************
// Filename	: 	TestManager_Device.h
// Created	:	2016/9/28 - 19:54
// Modified	:	2016/9/28 - 19:54
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_Device_h__
#define TestManager_Device_h__

#pragma once

#include "TestManager_Base.h"

#include "Def_TestDevice.h"
#include "Def_DataStruct.h"
#include "LT_Option.h"
#include "Def_ErrorCode.h"

//-----------------------------------------------------------------------------
// CTestManager_Device
//-----------------------------------------------------------------------------
class CTestManager_Device : public CTestManager_Base
{
public:
	CTestManager_Device();
	virtual ~CTestManager_Device();

protected:

	//-------------------------------------------------------------------------
	// 옵션
	//-------------------------------------------------------------------------
	stLT_Option		m_stOption;
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);

	//-------------------------------------------------------------------------
	// 주변장치 제어
	//-------------------------------------------------------------------------

	// 전체 주변장치와 통신 연결 전의 초기 작업
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);

	// 전체 주변장치와 통신 연결 시도
	virtual void	ConnectDevicez					();

	// 전체 주변장치의 연결 해제
	virtual void	DisconnectDevicez				();

	// 프레임 그래버 보드 연결 및 해제
	virtual BOOL	ConnectGrabberBrd_ComArt		(__in BOOL bConnect);
	//virtual BOOL	ConnectGrabberBrd_DAQ			(__in BOOL bConnect);

	// 바코드 리더기 연결 및 해제
	virtual BOOL	ConnectBCR						(__in BOOL bConnect);

	// 바코드 리더기 연결 및 해제
	virtual BOOL	ConnectLabelPrinter				(__in BOOL bConnect);

	// PCB 보드 연결 및 해제
	virtual BOOL	ConnectPCB_CameraBrd			(__in BOOL bConnect);
	virtual BOOL	ConnectPCB_LightBrd				(__in BOOL bConnect);

	// 모션보드 & IO 연결 및 해제
	virtual BOOL	ConnectIO						(__in BOOL bConnect);
	virtual BOOL	ConnectMotor					(__in BOOL bConnect);

	// Digital Indicator 제어 연결 및 해제
	virtual BOOL	ConnectIndicator				(__in BOOL bConnect);

	//-------------------------------------------------------------------------
	// 통신 연결 상태 UI에 표시 
	//-------------------------------------------------------------------------
	
	// 그래버 보드
	virtual void	OnSetStatus_GrabberBrd_ComArt	(__in BOOL bConnect){};
	//virtual void	OnSetStatus_GrabberBrd_DAQ		(__in BOOL bConnect){};

	// 프레임 그래버, 채널 영상 상태
	virtual void	OnSetStatus_VideoStatus			(__in UINT nChIdx, __in UINT nConnect){};

	// 바코드 리더기 통신 연결 상태
	virtual void	OnSetStatus_BCR					(__in UINT nConnect){};

	// 라벨 프린터 통신 연결 상태
	virtual void	OnSetStatus_LabelPrinter		(__in UINT nConnect){};

	// PCB 보드 통신 연결상태
	virtual void	OnSetStatus_PCBCamBrd			(__in UINT nConnect, __in UINT nIdxBrd){};
	virtual void	OnSetStatus_PCBLightBrd			(__in UINT nConnect, __in UINT nIdxBrd){};

	// I/O 보드 연결 상태
	virtual void	OnSetStatus_IOBoard				(__in BOOL bConnect){};
	virtual void	OnSetStatus_MotorBoard			(__in BOOL bConnect){};

	// MES 통신 상태
	virtual void	OnSetStatus_MES					(__in UINT nConnect){};

	// Digital Indicator 연결 상태
	virtual void	OnSetStatus_Indicator			(__in BOOL bConnect, __in UINT nChIdx = 0){};

	// 변위 센서 연결 상태
	virtual void	OnSetStatus_LaserSensor			(__in BOOL bConnect, __in UINT nSensorIdx = 0){};

	// 비전 카메라 연결 상태
	virtual void	OnSetStatus_VisionCam			(__in BOOL bConnect, __in UINT nCamIdx = 0){};

	// 비전 조명 연결 상태
	virtual void	OnSetStatus_VisionLight			(__in BOOL bConnect, __in UINT nChIdx = 0){};

	//-------------------------------------------------------------------------
	// 바코드
	//-------------------------------------------------------------------------
	// 바코드 입력
	virtual void	OnSet_Barcode					(__in LPCTSTR szBarcode){};

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	virtual void	OnFunc_IO_EMO					(){};
	virtual void	OnFunc_IO_ErrorReset			(){};
	virtual void	OnFunc_IO_Init					(){};
	virtual void	OnFunc_IO_Door					(){};
	virtual void	OnFunc_IO_Stop					(){};

	virtual void	OnAddErrorInfo					(__in enEquipErrorCode lErrorCode){};
	
	//-----------------------------------------------------
	// 모터 제어
	//-----------------------------------------------------
	virtual LRESULT SetMotorOrigin_All				();
	virtual LRESULT SetAjin_Motor					(__in long lAxis, __in AXT_MOTION_ABSREL_MODE enABSREL, __in double dbPos);
	virtual LRESULT SetAjin_Motor_EStop_All			();
	virtual LRESULT SetAjin_IO						(__in UINT nIO, __in enum_IO_SignalType nSignal);
	
	//-----------------------------------------------------
	// TOWER LAMP, 실린더
	//-----------------------------------------------------
	virtual LRESULT SetAjin_TowerLamp				(__in enIO_TowerLamp enItem, __in BOOL bOnOff);
	virtual LRESULT SetAjin_Cylinder				(__in enIO_Cylinder  enItem, __in BOOL bOnOff);

	//-----------------------------------------------------
	// 카메라 보드 (Luri)
	//-----------------------------------------------------
	virtual LRESULT SetLuriCamBrd_Port				(__in UINT nPort = 0);
	virtual LRESULT SetLuriCamBrd_Volt				(__in  float* fVolt,	__in UINT nPort = 0);
	virtual LRESULT SetLuriCamBrd_OpenShort			(__out bool & bCurrent, __in UINT nPort = 0);
	virtual LRESULT SetLuriCamBrd_Current			(__out double* iCurrent, __in UINT nPort = 0);
	virtual LRESULT SetLuriCamBrd_IRLED				(__in BOOL bOnOff, __in UINT nPort = 0);


	//-----------------------------------------------------
	// 광원 보드 (Luri)
	//-----------------------------------------------------
	virtual LRESULT SetLuriLightBrd_Port			(__in UINT nPort = 0);
	virtual LRESULT SetLuriLightBrd_Control			(__in UINT nCH, __in float fVolt, __in int iCurrent, __in UINT nPort = 0);
	virtual LRESULT SetLuriLightBrd_Volt			(__in UINT nCH, __in float fVolt,	__in UINT nPort = 0);
	virtual LRESULT SetLuriLightBrd_Current			(__in UINT nCH, __in int iCurrent,	__in UINT nPort = 0);

	//-----------------------------------------------------
	// 주변 장치 제어용 클래스 모음
	//-----------------------------------------------------
	ST_Device			m_Device;
	
public:

	// 생성자 처리용 코드
	virtual void	OnInitialize					();
	// 소멸자 처리용 코드
	virtual	void	OnFinalize						();
	// 장치 초기화
	virtual void	OnInitialDevice					();
	
	BOOL			b_IndicatorConnect[Indicator_Max];

};

#endif // TestManager_Device_h__

