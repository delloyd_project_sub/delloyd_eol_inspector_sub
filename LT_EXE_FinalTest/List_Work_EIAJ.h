﻿#ifndef List_Work_EIAJ_h__
#define List_Work_EIAJ_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_EIAJ_Worklist
{
	EIAJ_W_Recode,
	EIAJ_W_Time,
	EIAJ_W_Equipment,
	EIAJ_W_Model,
	EIAJ_W_SWVersion,
	EIAJ_W_LOTNum,
	EIAJ_W_Barcode,
	//EIAJ_W_Operator,
	EIAJ_W_Result,
	EIAJ_W_C1,
	EIAJ_W_C2,
	EIAJ_W_C3,
	EIAJ_W_C4,
	EIAJ_W_S1,
	EIAJ_W_S2,
	EIAJ_W_S3,
	EIAJ_W_S4,
	EIAJ_W_S5,
	EIAJ_W_S6,
	EIAJ_W_S7,
	EIAJ_W_S8,
	EIAJ_W_MaxCol,

};

// 헤더
static const TCHAR*	g_lpszHeader_EIAJ_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	_T("C1"),
	_T("C2"),
	_T("C3"),
	_T("C4"),
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),
	_T("S5"),
	_T("S6"),
	_T("S7"),
	_T("S8"),
	NULL
};

const int	iListAglin_EIAJ_Worklist[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_EIAJ_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_EIAJ

class CList_Work_EIAJ : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_EIAJ)

public:
	CList_Work_EIAJ();
	virtual ~CList_Work_EIAJ();

	CFont		m_Font;
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	UINT Header_MaxNum();

	void InitHeader();
	void InsertFullData(__in const ST_CamInfo* pstCamInfo);
	void SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo);

	void GetData(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

};

#endif // List_Work_EIAJ_h__
