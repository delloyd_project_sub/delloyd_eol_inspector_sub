﻿#ifndef Wnd_CurrentOp_h__
#define Wnd_CurrentOp_h__

#pragma once

#include "VGStatic.h"
#include "List_CurrentOp.h"
#include "Def_DataStruct.h"

// CWnd_CurrentOp

enum enCurrentStatic
{
	STI_CUR_OPT,
	STI_CUR_MAX = 1,
};

static LPCTSTR	g_szCurrentStatic[] =
{
	_T("OPTION LIST"),
	NULL
};

enum enCurrentButton
{
	BTN_CUR_TEST,
	BTN_CUR_MAX,
};

static LPCTSTR	g_szCurrentButton[] =
{
	_T("TEST"),
	NULL
};

enum enCurrentComobox
{
	CMB_CUR_MAX = 1,
};

enum enCurrentEdit
{
	EDT_CUR_MAX = 1,
};
class CWnd_CurrentOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_CurrentOp)

public:
	CWnd_CurrentOp();
	virtual ~CWnd_CurrentOp();

	void SetUpdateData	();
	void GetUpdateData	();

	void	SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	}

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

protected:

	ST_ModelInfo	*m_pstModelInfo;
	CList_CurrentOp m_List;

	CFont			m_font;

	CVGStatic			m_st_Item[STI_CUR_MAX];
	CButton				m_bn_Item[BTN_CUR_MAX];
	CComboBox			m_cb_Item[CMB_CUR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_CUR_MAX];
	
	// 검사 항목이 다수 인경우
	UINT			m_nTestItemCnt;

	DECLARE_MESSAGE_MAP()

	afx_msg void OnSize				(UINT nType, int cx, int cy);
	afx_msg int	 OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void OnRangeBtnCtrl		(UINT nID);
	virtual BOOL PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // Wnd_CurrentOp_h__
