﻿// List_RotateOp.Rtp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_EIAJOp.h"

#define IReOp_ED_CELLEDIT		5001
#define IReOp_CB_CELLCOMBO		5002

// CList_EIAJOp
IMPLEMENT_DYNAMIC(CList_EIAJOp, CListCtrl)

CList_EIAJOp::CList_EIAJOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol	= 0;
	m_nEditRow	= 0;
	m_pstEIAJ = NULL;
}

CList_EIAJOp::~CList_EIAJOp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_EIAJOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_EIAJOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_EIAJOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IReOp_ED_CELLEDIT, &CList_EIAJOp::OnEnKillFocusEReOpellEdit)
	ON_CBN_KILLFOCUS(IReOp_CB_CELLCOMBO, &CList_EIAJOp::OnEnKillFocusEReOpellCombo)
	ON_CBN_SELCHANGE(IReOp_CB_CELLCOMBO, &CList_EIAJOp::OnEnSelectEReOpellCombo)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_EIAJOp 메시지 처리기입니다.
int CList_EIAJOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	CString str;

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER /*| ES_NUMBER*/, CRect(0, 0, 0, 0), this, IReOp_ED_CELLEDIT);
	
	m_cb_Mode.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IReOp_CB_CELLCOMBO);
	m_cb_Mode.ResetContent();

	for (UINT nIdx = 0; nIdx < Resol_Mode_NO; nIdx++)
	{
		str.Format(_T("%s"), g_szResolutionMode[nIdx]);
		m_cb_Mode.AddString(str);
	}

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/6/26 - 12:17
// Desc.		:
//=============================================================================
void CList_EIAJOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[ReOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < ReOp_MaxCol; nCol++)
	{
		iColDivide += iHeaderWidth_ReOp[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

// 	for (int nCol = 0; nCol < ReOp_MaxCol; nCol++)
// 	{
// 		iUnitWidth = (rectClient.Width() * iHeaderWidth_ReOp[nCol]) / iColDivide;
// 		iMisc += iUnitWidth;
// 		SetColumnWidth(nCol, iUnitWidth);
// 	}
// 
// 	iUnitWidth = ((rectClient.Width() * iHeaderWidth_ReOp[ReOp_Object]) / iColDivide) + (rectClient.Width() - iMisc);
//  	SetColumnWidth(ReOp_Object, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/6/26 - 12:17
// Desc.		:
//=============================================================================
BOOL CList_EIAJOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/14 - 14:41
// Desc.		:
//=============================================================================
void CList_EIAJOp::InitHeader()
{
	for (int nCol = 0; nCol < ReOp_MaxCol; nCol++)
		InsertColumn(nCol, g_lpszHeader_ReOp[nCol], iListAglin_ReOp[nCol], iHeaderWidth_ReOp[nCol]);

	for (int nCol = 0; nCol < ReOp_MaxCol; nCol++)
		SetColumnWidth(nCol, iHeaderWidth_ReOp[nCol]);
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LT_TI_Rotate * pstRotate
// Qualifier	:
// Last Update	: 2017/1/14 - 14:40
// Desc.		:
//=============================================================================
void CList_EIAJOp::InsertFullData()
{
	if (m_pstEIAJ == NULL)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < ReOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T("."));
		SetRectRow(nIdx);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/1/14 - 14:40
// Desc.		:
//=============================================================================
void CList_EIAJOp::SetRectRow(UINT nRow)
{
	CString strText;

	strText.Format(_T("%s"), g_szResolutionRegion[nRow]);
	SetItemText(nRow, ReOp_Object, strText);

	strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.CenterPoint().x);
	SetItemText(nRow, ReOp_PosX, strText);

	strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.CenterPoint().y);
	SetItemText(nRow, ReOp_PosY, strText);

	strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Width());
	SetItemText(nRow, ReOp_Width, strText);

	strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Height());
	SetItemText(nRow, ReOp_Height, strText);

	if (nRow >= ReOp_CenterLeft)
	{
		strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_MtfRatio);
		SetItemText(nRow, ReOp_MTF, strText);

		SetItemText(nRow, ReOp_Mode, g_szResolutionMode[m_pstEIAJ->stEIAJOp.rectData[nRow].i_Mode]);

		strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Min);
		SetItemText(nRow, ReOp_MinBon, strText);

		strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Max);
		SetItemText(nRow, ReOp_MaxBon, strText);

		strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Min);
		SetItemText(nRow, ReOp_MinSpc, strText);

		strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Max);
		SetItemText(nRow, ReOp_MaxSpc, strText);

 		strText.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_offset);
 		SetItemText(nRow, ReOp_Offset, strText);
	}

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].bUse == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

 }

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/14 - 14:40
// Desc.		:
//=============================================================================
void CList_EIAJOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	LVHITTESTINFO HITInfo;
	HITInfo.pt = pNMItemActivate->ptAction;
	HitTest(&HITInfo);
	
	if (HITInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer;
		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
		{
			m_pstEIAJ->stEIAJOp.rectData[pNMItemActivate->iItem].bUse = FALSE;
			//SetItemState(pNMItemActivate->iItem, 0x1000, LVIS_STATEIMAGEMASK);
		}
		if (nBuffer == 0x1000)
		{
			m_pstEIAJ->stEIAJOp.rectData[pNMItemActivate->iItem].bUse = TRUE;
			//SetItemState(pNMItemActivate->iItem, 0x2000, LVIS_STATEIMAGEMASK);
		}
	}
	
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/14 - 14:41
// Desc.		:
//=============================================================================
void CList_EIAJOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < ReOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			if (!(pNMItemActivate->iItem <= ReOp_CenterWhite && pNMItemActivate->iSubItem >= ReOp_MTF))
			{
				CRect rectCell;

				m_nEditCol = pNMItemActivate->iSubItem;
				m_nEditRow = pNMItemActivate->iItem;

				GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
				ClientToScreen(rectCell);
				ScreenToClient(rectCell);

				ModifyStyle(WS_VSCROLL, 0);

				if (pNMItemActivate->iSubItem == ReOp_Mode)
				{
					m_cb_Mode.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
					m_cb_Mode.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
					m_cb_Mode.SetFocus();
					m_cb_Mode.SetCurSel(m_pstEIAJ->stEIAJOp.rectData[pNMItemActivate->iItem].i_Mode);
				}
				else
				{
					m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
					m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
					m_ed_CellEdit.SetFocus();
				}
			}
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEReOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 13:15
// Desc.		:
//=============================================================================
void CList_EIAJOp::OnEnKillFocusEReOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText)))
	{
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnKillFocusEReOpellCombo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 11:10
// Desc.		:
//=============================================================================
void CList_EIAJOp::OnEnKillFocusEReOpellCombo()
{
	SetItemText(m_nEditRow, ReOp_Mode, g_szResolutionMode[m_pstEIAJ->stEIAJOp.rectData[m_nEditRow].i_Mode]);
	m_cb_Mode.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectEReOpellCombo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 18:55
// Desc.		:
//=============================================================================
void CList_EIAJOp::OnEnSelectEReOpellCombo()
{
	m_pstEIAJ->stEIAJOp.rectData[m_nEditRow].i_Mode = m_cb_Mode.GetCurSel();

	SetItemText(m_nEditRow, ReOp_Mode, g_szResolutionMode[m_pstEIAJ->stEIAJOp.rectData[m_nEditRow].i_Mode]);
	m_cb_Mode.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 17:54
// Desc.		:
//=============================================================================
BOOL CList_EIAJOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	CString str;

	if (nCol != ReOp_Offset)
	{
		if (iValue < 0)
			iValue = 0;
	}

	CRect Data = m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList;

	switch (nCol)
	{
	case  ReOp_PosX:
		m_pstEIAJ->stEIAJOp.rectData[nRow]._Rect_Position_Sum((int)iValue, Data.CenterPoint().y, Data.Width(), Data.Height());
		break;
	case  ReOp_PosY:
		m_pstEIAJ->stEIAJOp.rectData[nRow]._Rect_Position_Sum(Data.CenterPoint().x, (int)iValue, Data.Width(), Data.Height());
		break;
	case  ReOp_Width:
		m_pstEIAJ->stEIAJOp.rectData[nRow]._Rect_Position_Sum2((int)iValue, Data.Height());
		break;
	case  ReOp_Height:
		m_pstEIAJ->stEIAJOp.rectData[nRow]._Rect_Position_Sum2(Data.Width(), (int)iValue);
		break;
	case  ReOp_MTF:
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_MtfRatio = (int)iValue;
		break;
	case  ReOp_Mode:
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Mode = m_cb_Mode.GetCurSel();
		break;
	case  ReOp_MinBon:
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Min = (int)iValue;
		break;
	case  ReOp_MaxBon:
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Max = (int)iValue;
		break;
	case  ReOp_MinSpc:
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Min = (int)iValue;
		break;
	case  ReOp_MaxSpc:
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Max = (int)iValue;
		break;
	case  ReOp_Offset:
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_offset = (UINT)iValue;
		break;
	default:
		break;
	}

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.left < 0)
	{
		CRect DataTemp = m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList;
		m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.left = 0;
		m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.right = m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.left + DataTemp.Width();
	}

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.right > CAM_IMAGE_WIDTH)
	{
		CRect DataTemp = m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList;
		m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.right = CAM_IMAGE_WIDTH;

		if (CAM_IMAGE_WIDTH - DataTemp.Width() >= 0)
			m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.left = CAM_IMAGE_WIDTH - DataTemp.Width();
	}

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.top < 0)
	{
		CRect DataTemp = m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList;
		m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.top = 0;
		m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.bottom = DataTemp.Height() + m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.top;
	}

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.bottom > CAM_IMAGE_HEIGHT)
	{
		CRect DataTemp = m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList;
		m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.bottom = CAM_IMAGE_HEIGHT;

		if (CAM_IMAGE_HEIGHT - DataTemp.Height() >= 0)
			m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.top = CAM_IMAGE_HEIGHT - DataTemp.Height();
	}

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Height() <= 0)
		m_pstEIAJ->stEIAJOp.rectData[nRow]._Rect_Position_Sum2(m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Width(), 1);

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Height() >= CAM_IMAGE_HEIGHT)
		m_pstEIAJ->stEIAJOp.rectData[nRow]._Rect_Position_Sum2(m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Width(), CAM_IMAGE_HEIGHT);

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Width() <= 0)
		m_pstEIAJ->stEIAJOp.rectData[nRow]._Rect_Position_Sum2(1, m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Height());

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Height() >= CAM_IMAGE_HEIGHT)
		m_pstEIAJ->stEIAJOp.rectData[nRow]._Rect_Position_Sum2(CAM_IMAGE_HEIGHT, m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Height());

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Min < 0)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Min = 0;

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Max < 0)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Max = 0;

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Min >= m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Max)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Max = m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Min + 1;

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Min < 0)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Min = 0;

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Max < 0)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Max = 0;

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Min >= m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Max)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Max = m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Min + 1;

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_MtfRatio > 99)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_MtfRatio = 99;

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_MtfRatio < 0)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_MtfRatio = 0;

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Min >= m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Min)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Min = m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Min + 1;

	if (m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Max <= m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Max)
		m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Max = m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Max;

	switch (nCol)
	{
	case  ReOp_PosX:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.CenterPoint().x);
		break;
	case  ReOp_PosY:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.CenterPoint().y);
		break;
	case  ReOp_Width:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Width());
		break;
	case  ReOp_Height:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].RegionList.Height());
		break;
	case  ReOp_MTF:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_MtfRatio);
		break;
	case  ReOp_MinBon:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Min);
		break;
	case  ReOp_MaxBon:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_Range_Max);
		break;
	case  ReOp_MinSpc:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Min);
		break;
	case  ReOp_MaxSpc:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_Threshold_Max);
		break;
	case  ReOp_Offset:
		str.Format(_T("%d"), m_pstEIAJ->stEIAJOp.rectData[nRow].i_offset);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:14
// Desc.		:
//=============================================================================
BOOL CList_EIAJOp::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	CString str;
	str.Format(_T("%.1f"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: CheckRectValue
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const CRect * pRegionz
// Qualifier	:
// Last Update	: 2017/6/26 - 17:56
// Desc.		:
//=============================================================================
BOOL CList_EIAJOp::CheckRectValue(__in const CRect* pRegionz)
{
	if (NULL == pRegionz)
		return FALSE;

	if (pRegionz->left < 0)
		return FALSE;

	if (pRegionz->right < 0)
		return FALSE;

	if (pRegionz->top < 0)
		return FALSE;

	if (pRegionz->bottom < 0)
		return FALSE;

	if (pRegionz->Width() < 0)
		return FALSE;

	if (pRegionz->Height() < 0)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/6/27 - 18:04
// Desc.		:
//=============================================================================
BOOL CList_EIAJOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		int znum = zDelta / 120;
		bool FLAG = 0;

		if (znum > 0)
		{
			FLAG = 1;
		}
		else if (znum < 0)
		{
			FLAG = 0;
		}
		else if (znum == 0)
		{
			return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
		}

		int casenum = pWndFocus->GetDlgCtrlID();

		if ((Change_DATA_CHECK(m_nEditRow) == TRUE))
		{
			CString str_buf;
			int buf = 0;
			int retval = 0;

			m_ed_CellEdit.GetWindowText(str_buf);

			str_buf.Remove(' ');

			if (str_buf == "")
			{
				buf = 0;
			}
			else
			{
				buf = _ttoi(str_buf);
				buf += znum;
			}

			str_buf.Format(_T("%d"), buf);
			m_ed_CellEdit.SetWindowText(str_buf);
			m_ed_CellEdit.SetSel(-2, -1);

			UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(str_buf));
		}
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: Change_DATA_CHECK
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nIdx
// Qualifier	:
// Last Update	: 2017/6/26 - 18:12
// Desc.		:
//=============================================================================
BOOL CList_EIAJOp::Change_DATA_CHECK(UINT nIdx)
{
	// 상한
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.Width() > CAM_IMAGE_WIDTH)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.Height() > CAM_IMAGE_HEIGHT)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.CenterPoint().x > CAM_IMAGE_WIDTH)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.CenterPoint().y > CAM_IMAGE_WIDTH)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.top > CAM_IMAGE_HEIGHT)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.bottom > CAM_IMAGE_HEIGHT)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.left > CAM_IMAGE_WIDTH)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.right > CAM_IMAGE_WIDTH)
		return FALSE;

	// 하한
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.Width() < 1)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.Height() < 1)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.CenterPoint().x < 0)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.CenterPoint().y < 0)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.top < 0)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.bottom < 0)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.left < 0)
		return FALSE;
	if (m_pstEIAJ->stEIAJOp.rectData[nIdx].RegionList.right < 0)
		return FALSE;

	return TRUE;
}

void CList_EIAJOp::SetAccessMode_Change(UINT nMode){

	m_bOffsetMode = nMode;
	DeleteAllItems();
	//for (int nCol = 0; nCol < ReOp_MaxCol; nCol++)
	// 	{
	// 		DeleteColumn(nCol);
	// 	}
	if (nMode == TRUE)
	{
		// 		for (int nCol = 0; nCol < ReOp_MaxCol; nCol++)
		// 			InsertColumn(nCol, g_lpszHeader_ReOp[nCol], iListAglin_ReOp[nCol], iHeaderWidth_ReOp[nCol]);

		for (int nCol = 0; nCol < ReOp_MaxCol; nCol++)
			SetColumnWidth(nCol, iHeaderWidth_ReOp_offset[nCol]);
	}
	else
	{
		// 		for (int nCol = 0; nCol < ReOp_MaxCol; nCol++)
		// 			InsertColumn(nCol, g_lpszHeader_ReOp[nCol], iListAglin_ReOp[nCol], iHeaderWidth_ReOp[nCol]);

		for (int nCol = 0; nCol < ReOp_MaxCol; nCol++)
			SetColumnWidth(nCol, iHeaderWidth_ReOp[nCol]);
	}

	InsertFullData();

}