// Dlg_LabelPrinter.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_LabelPrinter.h"
#include "afxdialogex.h"

#include "Dlg_ChkPassword.h"

#define	IDC_BN_OK 1000

// CDlg_LabelPrinter 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_LabelPrinter, CDialogEx)

CDlg_LabelPrinter::CDlg_LabelPrinter(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_LabelPrinter::IDD, pParent)
{
	m_pDevice = NULL;
}

CDlg_LabelPrinter::~CDlg_LabelPrinter()
{

}


void CDlg_LabelPrinter::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}



BEGIN_MESSAGE_MAP(CDlg_LabelPrinter, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_TIMER()

	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CDlg_LabelPrinter 메시지 처리기입니다.


int CDlg_LabelPrinter::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	UINT nID = 100;
	m_wndLabelPrinter.SetOwner(GetOwner());
	m_wndLabelPrinter.SetPtr_ModelInfo(m_pstModelInfo);
	m_wndLabelPrinter.Create(NULL, _T(""), dwStyle, rectDummy, this, nID++);

	m_wndLabelPrinter.SetUpdateData();

	return 0;
}


void CDlg_LabelPrinter::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 5;
	int iCateSpacing = 10;
	int iMagin = 10;
	int iLeft = iMagin;
	int iTop = iMagin;
	int iWidth = cx - (iMagin * 2);
	int iHeight = cy - (iMagin * 2);

	m_wndLabelPrinter.MoveWindow(iLeft, iTop, iWidth, iHeight);
}


void CDlg_LabelPrinter::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	lpMMI->ptMaxTrackSize.x = 1000;
	lpMMI->ptMaxTrackSize.y = 400;

	lpMMI->ptMinTrackSize.x = 1000;
	lpMMI->ptMinTrackSize.y = 400;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


BOOL CDlg_LabelPrinter::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreCreateWindow(cs);
}


BOOL CDlg_LabelPrinter::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDlg_LabelPrinter::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


void CDlg_LabelPrinter::SetPath(__in LPCTSTR szPrnFilePath)
{
	m_szPrnPath = szPrnFilePath;
	m_wndLabelPrinter.SetPath(m_szPrnPath);
}

void CDlg_LabelPrinter::SetModelInfo(ST_ModelInfo* pstModelInfo)
{
	if (pstModelInfo == NULL)
		return;

	m_pstModelInfo = pstModelInfo;
}


void CDlg_LabelPrinter::GetModelInfo()
{

}

void CDlg_LabelPrinter::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnClose();
}
