﻿#ifndef File_Model_h__
#define File_Model_h__

#pragma once

#include "Def_DataStruct.h"

class CFile_Model
{
public:
	CFile_Model();
	~CFile_Model();

	BOOL	LoadModelFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveModelFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	BOOL	Load_Common				(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	Save_Common				(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// 검사 스텝 정보
	BOOL	Load_StepInfo			(__in LPCTSTR szSaveItem, __in LPCTSTR szPath, __out ST_StepInfo& stStepInfo);
	BOOL	Save_StepInfo			(__in LPCTSTR szSaveItem, __in LPCTSTR szPath, __in const ST_StepInfo* pstStepInfo);

	// 표준 바코드 정보
	BOOL	Load_RefBarcodeInfo		(__in LPCTSTR szPath, __out ST_BarcodeRefInfo& stBarcodeInfo);
	BOOL	Save_RefBarcodeInfo		(__in LPCTSTR szPath, __in const ST_BarcodeRefInfo* pstBarcodeInfo);

	// 작업자 등록 정보
	BOOL	Load_UserConfigInfo		(__in LPCTSTR szPath, __out ST_UserConfigInfo& stUserConfigInfo);
	BOOL	Save_UserConfigInfo		(__in LPCTSTR szPath, __in const ST_UserConfigInfo* pstUserConfigInfo);

	// Current
	BOOL	LoadCurrentOpFile		(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveCurrentOpFile		(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// Operation Mode
	BOOL	LoadOperModeOpFile		(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveOperModeOpFile		(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// LED
	BOOL	LoadLEDOpFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveLEDOpFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// CenterPoint
	BOOL	LoadCenterPointOpFile	(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveCenterPointOpFile	(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// Rotate
	BOOL	LoadRotateFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveRotateFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// EIAJ
	BOOL	LoadEIAJOpFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveEIAJOpFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

 	// Angle
 	BOOL	LoadAngleOpFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
 	BOOL	SaveAngleOpFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
 
 	// Color
 	BOOL	LoadColorOpFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
 	BOOL	SaveColorOpFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// Reverse
	BOOL	LoadReverseOpFile		(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveReverseOpFile		(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// Reset
	BOOL	LoadResetOpFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveResetOpFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

// 	// Particle Manual
// 	BOOL	LoadParticleManualOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
// 	BOOL	SaveParticleManualOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
// 
 	// Pattern Noise
 	BOOL	LoadPatternNoiseOpFile	(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
 	BOOL	SavePatternNoiseOpFile	(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
 
 	// IR Filter
 	BOOL	LoadIRFilterOpFile		(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
 	BOOL	SaveIRFilterOpFile		(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

// 	// Angle
// 	BOOL	LoadAngleOpFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
// 	BOOL	SaveAngleOpFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
// 
// 	// CenterPoint
// 	BOOL	LoadCenterPointOpFile	(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
// 	BOOL	SaveCenterPointOpFile	(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
// 
// 	// Color
// 	BOOL	LoadColorOpFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
// 	BOOL	SaveColorOpFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
// 
// 	// FFT
// 	BOOL	LoadFFTOpFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
// 	BOOL	SaveFFTOpFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
// 
// 	// Partilce
 	BOOL	LoadParticleOpFile		(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL LoadDefectPixelOpFile		(LPCTSTR szPath, ST_ModelInfo & stConfigInfo);
	BOOL SaveDefectPixelOpFile		(LPCTSTR szPath, const ST_ModelInfo * pstConfigInfo);
 	BOOL	SaveParticleOpFile		(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
// 
 	// Brightness
 	BOOL	LoadBrightnessOpFile	(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
 	BOOL	SaveBrightnessOpFile	(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
 	
 	// SFR
 	BOOL	LoadSFRFile				(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
 	BOOL	SaveSFRFile				(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
// 
// 	// Rotate
// 	BOOL	LoadRotateFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
// 	BOOL	SaveRotateFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// Pogo File
	BOOL	LoadPogoIniFile			(__in LPCTSTR szPath, __out ST_PogoInfo& stPogoInfo);
	BOOL	SavePogoIniFile			(__in LPCTSTR szPath, __in const ST_PogoInfo* pstPogoInfo);

	BOOL	LoadPogoCount			(__in LPCTSTR szPath, __in UINT nPogoIdx, __out DWORD& dwCount);
	BOOL	SavePogoCount			(__in LPCTSTR szPath, __in UINT nPogoIdx, __in DWORD dwCount);

	// 광원
	BOOL	LoadLightBrdFile		(__in LPCTSTR szPath, __out ST_SlotVolt& stLightInfo);
	BOOL	SaveLightBrdFile		(__in LPCTSTR szPath, __in const ST_SlotVolt* pstLightInfo);

	// 프린터
	BOOL	LoadLabelPrinterFile	(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveLabelPrinterFile	(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);
};

#endif // File_Model_h__
