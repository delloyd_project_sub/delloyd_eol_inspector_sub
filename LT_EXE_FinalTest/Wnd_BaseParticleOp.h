﻿#ifndef Wnd_BaseParticleOp_h__
#define Wnd_BaseParticleOp_h__

#pragma once

#include "Wnd_ParticleOp.h"

// CWnd_BaseParticleOp

class CWnd_BaseParticleOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseParticleOp)

public:
	CWnd_BaseParticleOp();
	virtual ~CWnd_BaseParticleOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_ParticleOp m_wndParticleOp[TICnt_BlackSpot];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseParticleOp_h__
