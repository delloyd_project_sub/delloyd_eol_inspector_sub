﻿// List_AngleSubOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_AngleSubOp.h"

#define IRtOp_ED_CELLEDIT			5001

// CList_AngleSubOp

IMPLEMENT_DYNAMIC(CList_AngleSubOp, CListCtrl)

CList_AngleSubOp::CList_AngleSubOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;

	m_pstAngle = NULL;
}

CList_AngleSubOp::~CList_AngleSubOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_AngleSubOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_AngleSubOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_AngleSubOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_AngleSubOp::OnEnKillFocusEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_AngleSubOp 메시지 처리기입니다.
int CList_AngleSubOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_AngleSubOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[AngSOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = AngSOp_MinSpac; nCol < AngSOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_AngleSubOp[AngSOp_Object]) / (AngSOp_MaxCol - AngSOp_MinSpac);
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_AngleSubOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_AngleSubOp::InitHeader()
{
	for (int nCol = 0; nCol < AngSOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_AngleSubOp[nCol], iListAglin_AngleSubOp[nCol], iHeaderWidth_AngleSubOp[nCol]);
	}

	for (int nCol = 0; nCol < AngSOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_AngleSubOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_AngleSubOp::InsertFullData()
{
	if (m_pstAngle == NULL)
		return;

 	DeleteAllItems();
 

	for (UINT nIdx = 0; nIdx < AngSOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_AngleSubOp::SetRectRow(UINT nRow)
{
	if (m_pstAngle == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szIDXAngle[nRow]);
	SetItemText(nRow, AngSOp_Object, strValue);

	strValue.Format(_T("%d"), m_pstAngle->stAngleOpt.nMinSpc[nRow]);
	SetItemText(nRow, AngSOp_MinSpac, strValue);
		
	strValue.Format(_T("%d"), m_pstAngle->stAngleOpt.nMaxSpc[nRow]);
	SetItemText(nRow, AngSOp_MaxSpac, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_AngleSubOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_AngleSubOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < AngSOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_AngleSubOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);
	
	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_AngleSubOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (m_pstAngle == NULL)
		return FALSE;

	switch (nCol)
	{
	case AngSOp_MinSpac:
		m_pstAngle->stAngleOpt.nMinSpc[nRow] = iValue;
		break;
	case AngSOp_MaxSpac:
		m_pstAngle->stAngleOpt.nMaxSpc[nRow] = iValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%d"), iValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_AngleSubOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	if (m_pstAngle == NULL)
		return FALSE;

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_AngleSubOp::GetCellData()
{
	if (m_pstAngle == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_AngleSubOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue	= _ttof(strText);

		if (0 < zDelta)
		{
			iValue	= iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120) * 0.01);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 1000)
			iValue = 1000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}