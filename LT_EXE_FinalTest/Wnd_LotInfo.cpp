﻿//*****************************************************************************
// Filename	: 	Wnd_LotInfo.cpp
// Created	:	2016/7/7 - 16:22
// Modified	:	2016/7/7 - 16:22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_LotInfo.h"

#define		IDC_TB_INFO		1001

IMPLEMENT_DYNAMIC(CWnd_LotInfo, CWnd)

CWnd_LotInfo::CWnd_LotInfo()
{
}


CWnd_LotInfo::~CWnd_LotInfo()
{
}

BEGIN_MESSAGE_MAP(CWnd_LotInfo, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/7/14 - 15:15
// Desc.		:
//=============================================================================
int CWnd_LotInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	int TabCnt = 0;

	// 탭 컨트롤
	m_tc_Info.Create(CMFCTabCtrl::STYLE_3D_SCROLLED, rectDummy, this, IDC_TB_INFO, CMFCTabCtrl::LOCATION_TOP);

	if (!m_grid_LotInfo.CreateGrid(WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, &m_tc_Info, 11))
	{
		TRACE("m_grid_LotInfo 출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	//if (!m_grid_MESInfo.CreateGrid(WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, &m_tc_Info, 12))
	//{
	//	TRACE("m_grid_MESInfo 출력 창을 만들지 못했습니다.\n");
	//	return -1;
	//}

	if (!m_grid_Yield.CreateGrid(WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, &m_tc_Info, 13))
	{
		TRACE("m_grid_Yield 출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	m_tc_Info.AddTab(&m_grid_LotInfo,	_T("LOT"),		TabCnt++, FALSE);
	//m_tc_Info.AddTab(&m_grid_MESInfo,	_T("MES"),		TabCnt++, FALSE);
	m_tc_Info.AddTab(&m_grid_Yield,		_T("YIELD"),	TabCnt++, FALSE);
	m_tc_Info.EnableTabSwap(FALSE);
	m_tc_Info.SetActiveTab(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/7/14 - 15:15
// Desc.		:
//=============================================================================
void CWnd_LotInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 3;

	int iLeft = 0;
	int iTop  = 0;
	int iHeight = cy - iSpacing;

	m_tc_Info.MoveWindow(iLeft, iTop, cx, iHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/7/14 - 15:15
// Desc.		:
//=============================================================================
BOOL CWnd_LotInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetInspectionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2016/7/14 - 16:08
// Desc.		:
//=============================================================================
void CWnd_LotInfo::SetInspectionMode(__in enPermissionMode InspMode)
{
}

//=============================================================================
// Method		: SetLotInifo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LOTInfo * pstLotInfo
// Qualifier	:
// Last Update	: 2016/7/22 - 11:31
// Desc.		:
//=============================================================================
void CWnd_LotInfo::SetLotInifo(__in const ST_LOTInfo* pstLotInfo)
{
	m_grid_LotInfo.SetLotInifo(pstLotInfo);
}


void CWnd_LotInfo::SetMESInifo(__in const ST_MES_TotalResult* pstMESInfo)
{
	//m_grid_MESInfo.SetMESInifo(pstMESInfo);
}

//=============================================================================
// Method		: SetYield
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_Yield * pstYield
// Qualifier	:
// Last Update	: 2016/7/22 - 11:47
// Desc.		:
//=============================================================================
void CWnd_LotInfo::SetYield(__in const ST_Yield* pstYield)
{
	m_grid_Yield.SetYield(pstYield);
}
