//*****************************************************************************
// Filename	: 	Wnd_Cfg_BarcodeConfig.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_BarcodeConfig.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_BarcodeConfig.h"
#include "resource.h"

// CWnd_Cfg_BarcodeConfig
#define IDC_LC_BARCODELIST		1000
#define IDC_CB_MODEL			1001

#define IDC_BN_BARCODECTRL_S		1100
#define IDC_BN_BARCODECTRL_E		IDC_BN_BARCODECTRL_S + BCC_MaxEnum - 1
#define IDC_CHK_BARCODEITEM_S		1200
#define IDC_CHK_BARCODEITEM_E		IDC_CHK_BARCODEITEM_S + BI_MaxEnum - 1
#define IDC_ED_BARCODEITEM_S		1300
#define IDC_ED_BARCODEITEM_E		IDC_ED_BARCODEITEM_S + BI_MaxEnum - 1


static LPCTSTR g_szBarcodeList[] =
{
	_T("Reference Barcode"),		// BI_RefBarcode			
	_T("Model Name"),				// BI_Model			

	NULL
};

static LPCTSTR g_szBarcodeCtrl[] =
{
	_T("Add"),		// BCC_Add
	_T("Insert"),	// BCC_Insert
	_T("Remove"),	// BCC_Remove
	_T("Up"),		// BCC_Order_Up
	_T("Down"),		// BCC_Order_Down
	NULL
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_BarcodeConfig, CWnd_BaseView)

CWnd_Cfg_BarcodeConfig::CWnd_Cfg_BarcodeConfig()
{
	VERIFY(m_font.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_BarcodeConfig::~CWnd_Cfg_BarcodeConfig()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_BarcodeConfig, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_CHK_BARCODEITEM_S, IDC_CHK_BARCODEITEM_E, OnBnClickedBnBarcodeItem)
	ON_COMMAND_RANGE(IDC_BN_BARCODECTRL_S, IDC_BN_BARCODECTRL_E, OnBnClickedBnBarcodelistCtrl)
END_MESSAGE_MAP()


// CWnd_Cfg_BarcodeConfig message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/24 - 16:23
// Desc.		:
//=============================================================================
int CWnd_Cfg_BarcodeConfig::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 리스트
	m_lc_RefBarcodeList.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LC_BARCODELIST);
	
	for (UINT nIdx = 0; nIdx < BCC_MaxEnum; nIdx++)
	{
		m_bn_ListCtrl[nIdx].m_bTransparent = TRUE;
		m_bn_ListCtrl[nIdx].Create(g_szBarcodeCtrl[nIdx], dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_BARCODECTRL_S + nIdx);
		m_bn_ListCtrl[nIdx].SetMouseCursorHand();
	}

	m_cb_ModelItem.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_MODEL);
	m_cb_ModelItem.EnableWindow(FALSE);
	
	// 항목 조건
	for (UINT nIdx = 0; nIdx < BI_MaxEnum; nIdx++)
	{
		m_chk_BarcodeItem[nIdx].Create(g_szBarcodeList[nIdx], dwStyle | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_BARCODEITEM_S + nIdx);
		m_ed_BarcodeItem[nIdx].Create(dwStyle | ES_CENTER | WS_BORDER, rectDummy, this, IDC_ED_BARCODEITEM_S + nIdx);

		m_chk_BarcodeItem[nIdx].SetMouseCursorHand();
		m_chk_BarcodeItem[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_BarcodeItem[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_BarcodeItem[nIdx].SizeToContent();

		m_ed_BarcodeItem[nIdx].EnableWindow(FALSE);
		m_ed_BarcodeItem[nIdx].SetFont(&m_font);

		m_ed_BarcodeItem[nIdx].SetWindowText(_T(""));
	}
	
	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Name.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Name.Create(_T("BARCODE ITEM ADD"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_TestName.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_TestName.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_TestName.SetFont_Gdip(L"Arial", 9.0F);
	m_st_TestName.Create(_T("BARCODE ITEM INFO"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	if (!m_szModelPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szModelPath, MODEL_FILE_EXT);
		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());
	}

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/24 - 16:23
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iCateSpacing= 10;
	int iMagin		= 10;
	int iLeft		= iMagin;
	int iTop		= iMagin;
	int iWidth		= cx - (iMagin * 2);
	int iHeight		= cy - (iMagin * 2);
	int iHalfWidth	= (iWidth - iCateSpacing) / 3;
	int iHalfHeight	= (iHeight - iCateSpacing)  / 2;
	int iCapWidth	= (iWidth - iSpacing * 4 ) / 5;
	int iValWidth	= 0;
	int iCtrlHeight	= 25;
	int iLeftSub	= 0;

	// 리스트
	m_lc_RefBarcodeList.MoveWindow(iLeft, iTop, iWidth, iHalfHeight);

	// 스텝 추가/삭제/이동
	iLeft = iMagin;
	iLeftSub = iLeft + iCapWidth + iSpacing;
	iTop += iHalfHeight + iCateSpacing;

	m_st_Name.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
	
	iTop += iCtrlHeight + iSpacing;

	for (UINT nIdx = 0; nIdx < BCC_MaxEnum; nIdx++)
	{
		m_bn_ListCtrl[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);

		iLeft += iCapWidth + iSpacing;
	}
	
	iTop += iCtrlHeight + iCateSpacing;
	iLeft = iMagin;
	m_st_TestName.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);

	iHalfWidth = (iWidth - iCateSpacing) / 2;
	iLeft = iMagin;

	iCapWidth = iHalfWidth - iLeft;
	iLeftSub = iLeft + iCapWidth + iSpacing;

	iTop += iCtrlHeight + iSpacing;
	iValWidth = iWidth - iLeftSub + iMagin;

	m_chk_BarcodeItem[BI_RefBarcode].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_ed_BarcodeItem[BI_RefBarcode].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_chk_BarcodeItem[BI_Model].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_cb_ModelItem.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);


}

//=============================================================================
// Method		: OnBnClickedBnStepItem
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/9/25 - 9:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::OnBnClickedBnBarcodeItem(UINT nID)
{
	enBarcodeItem nIDIdx = (enBarcodeItem)(nID - IDC_CHK_BARCODEITEM_S);
	
	BOOL bEnable = TRUE;

	if (BST_CHECKED == m_chk_BarcodeItem[nIDIdx].GetCheck())
	{
		bEnable = TRUE;
	}
	else
	{
		bEnable = FALSE;
	}
	
	m_ed_BarcodeItem[nIDIdx].EnableWindow(bEnable);
	
	switch (nIDIdx)
	{
	case CWnd_Cfg_BarcodeConfig::BI_RefBarcode:
		
		if (TRUE == bEnable)
		{
			for (int i = 0; i < BI_MaxEnum; i++)
			{
				m_chk_BarcodeItem[i].SetCheck(0);
			}

			m_chk_BarcodeItem[BI_RefBarcode].SetCheck(1);
			m_chk_BarcodeItem[BI_Model].SetCheck(1);

			Item_Enable(nIDIdx);
		}
		else
		{
			m_chk_BarcodeItem[BI_Model].SetCheck(0);

			m_cb_ModelItem.EnableWindow(FALSE);
			m_ed_BarcodeItem[BI_RefBarcode].EnableWindow(FALSE);
		}

		break;

	case CWnd_Cfg_BarcodeConfig::BI_Model:

		if (TRUE == bEnable)
		{
			for (int i = 0; i < BI_MaxEnum; i++)
			{
				m_chk_BarcodeItem[i].SetCheck(0);
			}

			m_chk_BarcodeItem[BI_RefBarcode].SetCheck(1);
			m_chk_BarcodeItem[BI_Model].SetCheck(1);

			Item_Enable(nIDIdx);
		}
		else
		{
			m_chk_BarcodeItem[BI_RefBarcode].SetCheck(0);

			m_cb_ModelItem.EnableWindow(FALSE);
			m_ed_BarcodeItem[BI_RefBarcode].EnableWindow(FALSE);
		}

		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnBnClickedBnStepCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/9/25 - 9:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::OnBnClickedBnBarcodelistCtrl(UINT nID)
{
	enBarcodeListCtrl nIDIdx = (enBarcodeListCtrl)(nID - IDC_BN_BARCODECTRL_S);

	switch (nIDIdx)
	{
	case CWnd_Cfg_BarcodeConfig::BCC_Add:
		Item_Add();
		break;

	case CWnd_Cfg_BarcodeConfig::BCC_Insert:
		Item_Insert();
		break;

	case CWnd_Cfg_BarcodeConfig::BCC_Remove:
		Item_Remove();
		break;

	case CWnd_Cfg_BarcodeConfig::BCC_Order_Up:
		Item_Up();
		break;

	case CWnd_Cfg_BarcodeConfig::BCC_Order_Down:
		Item_Down();
		break;

	default:
		break;
	}
}


//=============================================================================
// Method		: GetTestStepData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __out ST_StepUnit & stStepUnit
// Qualifier	:
// Last Update	: 2017/9/25 - 17:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_BarcodeConfig::GetBarcodeInfoData(__out ST_BarcodeRef& stRefBarcode)
{
	CString		szText;

	// Reference Barcode
	if (BST_CHECKED == m_chk_BarcodeItem[BI_RefBarcode].GetCheck() 
		&& BST_CHECKED == m_chk_BarcodeItem[BI_Model].GetCheck())
	{
		m_ed_BarcodeItem[BI_RefBarcode].GetWindowText(szText);

		if (szText.IsEmpty())
		{
			return FALSE;
		}

		stRefBarcode.szrefBarcode = m_szRefBarcode + szText;

		m_cb_ModelItem.GetWindowText(szText);
		stRefBarcode.szModelName = szText;
	}
	else
	{
		stRefBarcode.szModelName.Empty();
		stRefBarcode.szrefBarcode.Empty();

		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: Item_Add
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 10:25
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::Item_Add()
{
	ST_BarcodeRef stBarcodeRef;
	if (GetBarcodeInfoData(stBarcodeRef))
	{
		m_lc_RefBarcodeList.Item_Add(stBarcodeRef);
	}
}

//=============================================================================
// Method		: Item_Insert
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::Item_Insert()
{
	ST_BarcodeRef stBarcodeRef;
	if (GetBarcodeInfoData(stBarcodeRef))
	{
		m_lc_RefBarcodeList.Item_Insert(stBarcodeRef);
	}
}

//=============================================================================
// Method		: Item_Remove
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::Item_Remove()
{
	m_lc_RefBarcodeList.Item_Remove();
}

//=============================================================================
// Method		: Item_Up
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::Item_Up()
{
	m_lc_RefBarcodeList.Item_Up();
}

//=============================================================================
// Method		: Item_Down
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::Item_Down()
{
	m_lc_RefBarcodeList.Item_Down();
}


void CWnd_Cfg_BarcodeConfig::Item_Enable(enBarcodeItem enBarcode)
{
	switch (enBarcode)
	{
	case CWnd_Cfg_BarcodeConfig::BI_RefBarcode:
	case CWnd_Cfg_BarcodeConfig::BI_Model:
		m_cb_ModelItem.EnableWindow(TRUE);
		m_ed_BarcodeItem[BI_RefBarcode].EnableWindow(TRUE);

		if (!m_szModelPath.IsEmpty())
		{
			m_IniWatch.SetWatchOption(m_szModelPath, MODEL_FILE_EXT);
			m_IniWatch.RefreshList();

			RefreshFileList(m_IniWatch.GetFileList());
		}

		m_ed_BarcodeItem[BI_RefBarcode].SetWindowText(_T(""));

		break;

	default:
		break;
	}
}



//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::SetSystemType(__in enInsptrSysType nSysType)
{
}

//=============================================================================
// Method		: Set_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepInfo * pstInStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::Set_BarcodeInfo(__in const ST_BarcodeRefInfo* pstBarcodeInfo)
{
	m_lc_RefBarcodeList.Set_BarcodeRefInfo(pstBarcodeInfo);

	
}

//=============================================================================
// Method		: Get_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_StepInfo & stOutStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:38
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::Get_BarcodeInfo(__out ST_BarcodeRefInfo& stBarcodeInfo)
{
	m_lc_RefBarcodeList.Get_BarcodeRefInfo(stBarcodeInfo);
}

//=============================================================================
// Method		: Init_DefaultSet
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 20:56
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::Init_DefaultSet()
{
	// 리스트 초기화
	m_lc_RefBarcodeList.DeleteAllItems();

	// 체크 버튼 초기화
	for (UINT nIdx = 0; nIdx < BI_MaxEnum; nIdx++)
	{
		m_chk_BarcodeItem[nIdx].SetCheck(BST_UNCHECKED);
		m_ed_BarcodeItem[nIdx].EnableWindow(FALSE);
	}

	m_cb_ModelItem.SetCurSel(0);
	m_cb_ModelItem.EnableWindow(FALSE);

}



//=============================================================================
// Method		: RefreshFileList
// Access		: protected  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/7/6 - 9:47
// Desc.		:
//=============================================================================
void CWnd_Cfg_BarcodeConfig::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_ModelItem.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_ModelItem.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
// 	if (0 < pFileList->GetCount())
// 	{
// 		int iSel =  m_cb_ModelItem.FindStringExact(0, m_szModel);
// 		m_cb_ModelItem.SetCurSel(iSel);
// 	}
// 	else
	{
		m_cb_ModelItem.SetCurSel(0);
	}
}