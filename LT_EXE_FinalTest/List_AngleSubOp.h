﻿#ifndef List_AngleSubOp_h__
#define List_AngleSubOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_AngleSubOp
{
	AngSOp_Object = 0,
	AngSOp_MinSpac,
	AngSOp_MaxSpac,
	AngSOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_AngleSubOp[] =
{
	_T(""),
	_T("MinSpec"),
	_T("MaxSpec"),
	NULL
};

typedef enum enListItemNum_AngleSubOp
{
	AngSOp_ItemNum = IDX_AL_Max,
};

static LPCTSTR	g_lpszItem_AngleSubOp[] =
{
	NULL
};

const int	iListAglin_AngleSubOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_AngleSubOp[] =
{
	100,
	170,
	170,
};
// List_AngleInfo

class CList_AngleSubOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_AngleSubOp)

public:
	CList_AngleSubOp();
	virtual ~CList_AngleSubOp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();


	void SetPtr_Angle(ST_LT_TI_Angle* pstAngle)
	{
		if (pstAngle == NULL)
			return;

		m_pstAngle = pstAngle;
	};


protected:

	ST_LT_TI_Angle*  m_pstAngle;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dbValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusEdit();
};

#endif // List_AngleSubOp_h__
