﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_LightCtrl.cpp
// Created	:	2016/10/31 - 21:02
// Modified	:	2016/10/31 - 21:02
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_Cfg_LightCtrl.h"
#include "Reg_InspInfo.h"

typedef enum Light_ID
{
	IDC_BTN_LIT_VOLT_CH1 = 1001,
	IDC_BTN_LIT_VOLT_CH2,
	IDC_BTN_LIT_VOLT_CH3,
	IDC_BTN_LIT_VOLT_CH4,
	IDC_BTN_LIT_VOLT_CH5,
	IDC_BTN_LIT_STEP_CH1,
	IDC_BTN_LIT_STEP_CH2,
	IDC_BTN_LIT_STEP_CH3,
	IDC_BTN_LIT_STEP_CH4,
	IDC_BTN_LIT_STEP_CH5,
	IDC_EIT_LIT_VOLT_CH1,
	IDC_EIT_LIT_VOLT_CH2,
	IDC_EIT_LIT_VOLT_CH3,
	IDC_EIT_LIT_VOLT_CH4,
	IDC_EIT_LIT_VOLT_CH5,
	IDC_EIT_LIT_STEP_CH1,
	IDC_EIT_LIT_STEP_CH2,
	IDC_EIT_LIT_STEP_CH3,
	IDC_EIT_LIT_STEP_CH4,
	IDC_EIT_LIT_STEP_CH5,
	IDC_SLI_LIT_CH1,
	IDC_SLI_LIT_CH2,
	IDC_SLI_LIT_CH3,
	IDC_SLI_LIT_CH4,
	IDC_SLI_LIT_CH5,
};

//-----------------------------------------------------------------------------
// CWnd_Cfg_LightCtrl
//-----------------------------------------------------------------------------
IMPLEMENT_DYNAMIC(CWnd_Cfg_LightCtrl, CWnd_BaseView)

CWnd_Cfg_LightCtrl::CWnd_Cfg_LightCtrl()
{
	VERIFY(m_font_Data.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_LightCtrl::~CWnd_Cfg_LightCtrl()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_LightCtrl, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_HSCROLL()
	ON_COMMAND_RANGE(IDC_BTN_LIT_VOLT_CH1, IDC_BTN_LIT_VOLT_CH5, OnRangeBtnVolt)
	ON_COMMAND_RANGE(IDC_BTN_LIT_STEP_CH1, IDC_BTN_LIT_STEP_CH5, OnRangeBtnStep)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CWnd_Cfg_LightCtrl message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_Cfg_LightCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_LIT_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);

		if (nIdx < STI_LIT_VOLT_CH1)
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		else
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szLight_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < BTN_LIT_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szLight_Button[nIdx], dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BTN_LIT_VOLT_CH1 + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font_Data);
	}

	for (UINT nIdx = 0; nIdx < EIT_LIT_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EIT_LIT_VOLT_CH1 + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font_Data);
		m_ed_Item[nIdx].LimitText(4);
		
		if (nIdx >= EIT_LIT_STEP_CH1)
			m_ed_Item[nIdx].SetValidChars(_T("0123456789."));
	}


	for (UINT nIdx = 0; nIdx < SID_LIT_MAXNUM; nIdx++)
	{
		m_sd_Item[nIdx].Create(WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS | WS_BORDER | TBS_BOTH | TBS_NOTICKS, rectDummy, this, IDC_SLI_LIT_CH1 + nIdx);
		m_sd_Item[nIdx].SetRange(0, 1023);
		m_sd_Item[nIdx].SetLineSize(1);
		m_sd_Item[nIdx].SetTicFreq(1);
		m_sd_Item[nIdx].SetPageSize(1);
	}
	return 0;
}

//=============================================================================
// Method		: OnHScroll
// Access		: protected  
// Returns		: void
// Parameter	: UINT nSBCode
// Parameter	: UINT nPos
// Parameter	: CScrollBar * pScrollBar
// Qualifier	:
// Last Update	: 2017/6/29 - 15:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	BOOL bScrollbar = FALSE;
	UINT nSBIndex = 0;
	CString strValue;

	int iPos = 0;

	if (m_pstDevice == NULL)
		return;

	if (pScrollBar)
	{
		for (UINT nIdx = 0; nIdx < Slot_Max; nIdx++)
		{
			if (pScrollBar == (CScrollBar*)&m_sd_Item[nIdx])
			{
				bScrollbar = TRUE;
				nSBIndex = nIdx;
				break;
			}
		}

		if (bScrollbar == FALSE)
			return;


		switch (nSBCode)
		{

		case TB_ENDTRACK:
			iPos = m_sd_Item[nSBIndex].GetPos();

			strValue.Format(_T("%d"), iPos);
			m_ed_Item[EIT_LIT_STEP_CH1 + nSBIndex].SetWindowText(strValue);
			break;

		case TB_LINEUP:
		case TB_LINEDOWN:
		case TB_THUMBPOSITION:
		case TB_THUMBTRACK:
		default:
			break;
		}
	}

	CWnd_BaseView::OnHScroll(nSBCode, nPos, pScrollBar);
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin = 10;
	int iSpacing = 5;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iStTitleWidth	= iWidth / 13;
	int iStWidth		= iWidth / 7;
	int iBtnWidth		= iWidth / 8 - 1;
	int iSdWidth		= iStWidth + iStWidth + iBtnWidth + 5;

	int iBtnHeight = 26;
	int iLeftSub = 0;
	int List_W = iWidth - iBtnWidth - iBtnWidth;

	for (UINT nIdx = 0; nIdx < 3; nIdx++)
	{
		iLeft = iMagrin;
		m_st_Item[STI_LIT_CH1 + nIdx].MoveWindow(iLeft, iTop, iStTitleWidth, iBtnHeight + iBtnHeight + iBtnHeight + 10);

		iLeft += iStTitleWidth + 3;
		m_st_Item[STI_LIT_VOLT_CH1 + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);
		
		iLeft += iStWidth + 3;
		m_ed_Item[EIT_LIT_VOLT_CH1 + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 3;
		m_bn_Item[BTN_LIT_VOLT_CH1 + nIdx].MoveWindow(iLeft, iTop - 1, iBtnWidth, iBtnHeight + 2);

		iTop += iBtnHeight + 3;
		iLeft = iStTitleWidth + iMagrin + 3;
		m_st_Item[STI_LIT_CURRENT_CH1 + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 3;
		m_ed_Item[EIT_LIT_STEP_CH1 + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 3;
		m_bn_Item[BTN_LIT_STEP_CH1 + nIdx].MoveWindow(iLeft, iTop - 1, iBtnWidth, iBtnHeight + 2);

		iTop += iBtnHeight + 3;
		iLeft = iStTitleWidth + iMagrin + 3;
		m_sd_Item[nIdx].MoveWindow(iLeft, iTop, iSdWidth, iBtnHeight + 4);

		iTop += iBtnHeight + iMagrin;
	}

	iTop = iMagrin;
	iLeftSub = iLeft + iSdWidth + iMagrin;

	for (UINT nIdx = 3; nIdx < 4; nIdx++)
	{
		iLeft = iLeftSub;
		m_st_Item[STI_LIT_CH1 + nIdx].MoveWindow(iLeft, iTop, iStTitleWidth, iBtnHeight + iBtnHeight + iBtnHeight + 10);

		iLeft += iStTitleWidth + 3;
		m_st_Item[STI_LIT_VOLT_CH1 + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 3;
		m_ed_Item[EIT_LIT_VOLT_CH1 + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 3;
		m_bn_Item[BTN_LIT_VOLT_CH1 + nIdx].MoveWindow(iLeft, iTop - 1, iBtnWidth, iBtnHeight + 2);

		iTop += iBtnHeight + 3;
		iLeft = iStTitleWidth + iLeftSub + 3;
		m_st_Item[STI_LIT_CURRENT_CH1 + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 3;
		m_ed_Item[EIT_LIT_STEP_CH1 + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 3;
		m_bn_Item[BTN_LIT_STEP_CH1 + nIdx].MoveWindow(iLeft, iTop - 1, iBtnWidth, iBtnHeight + 2);

		iTop += iBtnHeight + 3;
		iLeft = iStTitleWidth + iLeftSub + 3;
		m_sd_Item[nIdx].MoveWindow(iLeft, iTop, iSdWidth, iBtnHeight + 4);

		iTop += iBtnHeight + iMagrin;
	}
}

//=============================================================================
// Method		: OnRangeBtnVolt
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/6/29 - 14:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::OnRangeBtnVolt(UINT nID)
{
	UINT nIndex = nID - IDC_BTN_LIT_VOLT_CH1;
	CString strValue;
	float fVolt = 0;

	m_ed_Item[EIT_LIT_VOLT_CH1 + nIndex].GetWindowText(strValue);

	if (fVolt > 12.0)
	{
		strValue = _T("12.0");
		fVolt	 = 12.0;
	}
	else
	{
		fVolt = (float)_ttof(strValue);
	}

	m_ed_Item[EIT_LIT_VOLT_CH1 + nIndex].SetWindowText(strValue);

	m_pstDevice->MotionSequence.LightVoltControl(enLightCtrl(LightChat_Center + nIndex), fVolt);
}

//=============================================================================
// Method		: OnRangeBtnStep
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/6/29 - 14:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::OnRangeBtnStep(UINT nID)
{
	UINT nIndex = nID - IDC_BTN_LIT_STEP_CH1;
	CString strValue;
	UINT nCode = 0;

	m_ed_Item[EIT_LIT_STEP_CH1 + nIndex].GetWindowText(strValue);

	if (nCode > 1023)
	{
		strValue = _T("1023");
		nCode = 1023;
	}
	else
	{
		nCode = _ttoi(strValue);
	}

	m_ed_Item[EIT_LIT_STEP_CH1 + nIndex].SetWindowText(strValue);
	m_sd_Item[nIndex].SetPos((int)nCode);

	m_pstDevice->MotionSequence.LightCurrentControl(enLightCtrl(LightChat_Center + nIndex), nCode);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_LightCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_LightCtrl::PreTranslateMessage(MSG* pMsg)
{
	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: SetLightInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_SlotVolt * pstSlotVolt
// Qualifier	:
// Last Update	: 2017/6/29 - 14:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::SetLightInfo(__in const ST_SlotVolt* pstSlotVolt)
{
	CString strValue;

	for (UINT nIdx = 0; nIdx < Slot_Max; nIdx++)
	{
		strValue.Format(_T("%.1f"), pstSlotVolt->fVolt[nIdx]);
		m_ed_Item[EIT_LIT_VOLT_CH1 + nIdx].SetWindowText(strValue);

		strValue.Format(_T("%d"), pstSlotVolt->wCurrent[nIdx]);
		m_ed_Item[EIT_LIT_STEP_CH1 + nIdx].SetWindowText(strValue);

		m_sd_Item[nIdx].SetPos((int)pstSlotVolt->wCurrent[nIdx]);
	}
}

//=============================================================================
// Method		: GetLightInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_SlotVolt & stSlotVolt
// Qualifier	:
// Last Update	: 2017/6/29 - 14:24
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::GetLightInfo(__out ST_SlotVolt& stSlotVolt)
{
	CString strValue;

	for (UINT nIdx = 0; nIdx < Slot_Max; nIdx++)
	{
		m_ed_Item[EIT_LIT_VOLT_CH1 + nIdx].GetWindowText(strValue);
		stSlotVolt.fVolt[nIdx] = (float)_ttof(strValue);

		m_ed_Item[EIT_LIT_STEP_CH1 + nIdx].GetWindowText(strValue);
		stSlotVolt.wCurrent[nIdx] = _ttoi(strValue);
	}
}