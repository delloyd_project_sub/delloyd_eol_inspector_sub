// Dlg_MasterMode.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_MasterMode.h"
#include "afxdialogex.h"
#include "CommonFunction.h"


//CWinThread* pThread_Master = NULL;

static volatile bool isThreadRunning_Master;

// UINT MyThread_Master(LPVOID ipParam)
// {
// 	CDlg_MasterMode* pClass = (CDlg_MasterMode*)ipParam;
// //	int iReturn = pClass->ThreadFunction();
// 	return 0L;
// }

#define		IDC_WND_IMAGEVIEW		1000
#define		IDC_CB_EACH_TEST_SEL	2000
#define		IDC_BTN_START			3000
#define		IDC_BTN_STOP			4000
typedef enum Master_ID
{
	IDC_BTN_TEST = 1001,
	IDC_BTN_SAVE,
	IDC_BTN_EXIT,
	IDC_ED_MasterInfoX,
	IDC_ED_MasterInfoY,
 	IDC_ED_MasterInfoR,
	IDC_ED_MasterSpcX,
	IDC_ED_MasterSpcY,
 	IDC_ED_MasterSpcR,
	IDC_ED_MasterOffsetX,
	IDC_ED_MasterOffsetY,
 	IDC_ED_MasterOffsetR,
};
typedef enum enEachTestSel
{
	enEachTestSel_NG,
	enEachTestSel_OK,
	enEachTestSel_Skip,
	enEachTestSel_Max
};

static LPCTSTR g_szEachTestSel[] =
{
	_T("NG"),
	_T("OK"),
	_T("NO TEST"),
	NULL
};

typedef enum enTotalTestStatus
{
	enTotalTestStatus_Standby,
	enTotalTestStatus_MasterCheck,
	enTotalTestStatus_MasterNG,
	enTotalTestStatus_MasterOK,
	enTotalTestStatus_UserStop,
	enTotalTestStatus_Max
};

static LPCTSTR g_szTotalTestStatus[] =
{
	_T("STAND BY"),
	_T("MASTER CHECK.."),
	_T("MASTER NG"),
	_T("MASTER OK"),
	_T("USER STOP"),
	NULL
};

// CDlg_MasterMode 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_MasterMode, CDialogEx)

CDlg_MasterMode::CDlg_MasterMode(CWnd* pParent /*=NULL*/)
: CDialogEx(CDlg_MasterMode::IDD, pParent)
{
	VERIFY(m_font_Default.CreateFont(
		25,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename}


	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename}
}

CDlg_MasterMode::~CDlg_MasterMode()
{
	//KillTimer(100);
	//KillTimer(99);

	isThreadRunning_Master = false;
/*	DestroyThread();*/
}


// void CDlg_MasterMode::CreateThread(UINT _method)
// {
// 	if (pThread_Master != NULL)
// 	{
// 		return;
// 	}
// 
// 	pThread_Master = AfxBeginThread(MyThread_Master, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
// 
// 	if (pThread_Master == NULL)
// 	{
// 		//cout<<"Fail to create camera thread!!";
// 	}
// 
// 	pThread_Master->m_bAutoDelete = FALSE;
// 	pThread_Master->ResumeThread();
// }


// bool CDlg_MasterMode::DestroyThread()
// {
// 	if (NULL != pThread_Master)
// 	{
// 		DWORD dwResult = ::WaitForSingleObject(pThread_Master->m_hThread, INFINITE);
// 
// 		if (dwResult == WAIT_TIMEOUT)
// 		{
// 			//cout<<"time out!"<<endl;
// 		}
// 		else if (dwResult == WAIT_OBJECT_0)
// 		{
// 			//cout<<"Thread END"<<endl;
// 		}
// 
// 		delete pThread_Master;
// 
// 		pThread_Master = NULL;
// 	}
// 	return true;
// }


void CDlg_MasterMode::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}




BEGIN_MESSAGE_MAP(CDlg_MasterMode, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BTN_TEST, OnBnClickedTest)
	ON_BN_CLICKED(IDC_BTN_SAVE, OnBnClickedSave)
	ON_BN_CLICKED(IDC_BTN_EXIT, OnBnClickedExit)

	//ON_WM_CLOSE()
END_MESSAGE_MAP()


// CDlg_MasterMode 메시지 처리기입니다.


int CDlg_MasterMode::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_bn_MasterTest.Create(_T("MASTER TEST"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_TEST);
	m_bn_MasterTest.SetFont(&m_font);

	m_bn_MasterSave.Create(_T("MASTER SAVE"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_SAVE);
	m_bn_MasterSave.SetFont(&m_font);

	m_bn_MasterExit.Create(_T("MASTER EXIT"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_EXIT);
	m_bn_MasterExit.SetFont(&m_font);

	m_st_UIItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_UIItem.SetColorStyle(CVGStatic::ColorStyle_Default);

	m_st_UIItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_UIItem.Create(_T("MASTER\n DATA"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < STI_MST_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);

		if (nIdx < STI_MST_MasterInfo)
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		else
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szMaster_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < EIT_MST_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_MasterInfoX + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font_Default);
		m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
		m_ed_Item[nIdx].SetWindowText(_T("0"));
	}

// 	m_Group.SetTitle(L"MASTER INFO");
// 
// 	if (!m_Group.Create(_T("MASTER INFO"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
// 	{
// 		TRACE0("출력 창을 만들지 못했습니다.\n");
// 		return -1;
// 	}

	m_bn_MasterTest.EnableWindow(TRUE);
	m_bn_MasterSave.EnableWindow(TRUE);

	for (UINT nIdx = 0; nIdx < EIT_MST_MAXNUM; nIdx++)
		m_ed_Item[nIdx].EnableWindow(TRUE);

	return 0;
}


void CDlg_MasterMode::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin = 10;
	int iSpacing = 5;

	int iLeft = iMagrin;
	int iTop = 26;

	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iStWidth = iWidth / 4;
	int iBtnWidth = iWidth / 7;
	int iBtnHeight = 20 * 1.8;

	int List_W = iMagrin;

	//m_Group.MoveWindow(0, 0, cx, cy);

	m_st_UIItem.MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	iLeft += iStWidth + 1;
	m_st_Item[STI_MST_X].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	iLeft += iStWidth + 1;
	m_st_Item[STI_MST_Y].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	iLeft += iStWidth + 1;
	m_st_Item[STI_MST_R].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	for (UINT nIdx = 0; nIdx < 3; nIdx++)
	{
		iTop += iBtnHeight + 1;
		iLeft = iMagrin;
		m_st_Item[STI_MST_MasterInfo + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 1;
		m_ed_Item[EIT_MST_MasterInfoX + nIdx * 3].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 1;
		m_ed_Item[EIT_MST_MasterInfoY + nIdx * 3].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 1;
		m_ed_Item[EIT_MST_MasterInfoR + nIdx * 3].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);
	}

	iStWidth = iWidth / 3;

	iTop += iBtnHeight + 1;
	iLeft = iMagrin;
	m_bn_MasterTest.MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	iLeft += iStWidth + 2;
	m_bn_MasterSave.MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	iLeft += iStWidth + 2;
	m_bn_MasterExit.MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);
}


void CDlg_MasterMode::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	lpMMI->ptMaxTrackSize.x = 1280;
	lpMMI->ptMaxTrackSize.y = 540;

	lpMMI->ptMinTrackSize.x = 1280;
	lpMMI->ptMinTrackSize.y = 540;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


void CDlg_MasterMode::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	LPBYTE pFrameImage = NULL;
	IplImage *pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	switch (nIDEvent)
	{
	case 99:

		if (m_pDevice->DigitalIOCtrl.AXTState() == TRUE)
		{
			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StartBtn))
			{
				if (m_bFlag_Butten[DI_StartBtn] == FALSE)
				{
					m_bFlag_Butten[DI_StartBtn] = TRUE;



					m_bFlag_Butten[DI_StartBtn] = FALSE;
				}
			}

			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StopBtn))
			{
				if (m_bFlag_Butten[DI_StopBtn] == FALSE)
				{
					m_bFlag_Butten[DI_StopBtn] = TRUE;



					m_bFlag_Butten[DI_StopBtn] = FALSE;
				}
			}
		}
		break;

	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_MasterMode::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}


BOOL CDlg_MasterMode::PreTranslateMessage(MSG* pMsg)
{
	return CWnd::PreTranslateMessage(pMsg);

}


BOOL CDlg_MasterMode::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

//=============================================================================
// Method		: OnBnClickedTest
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 16:33
// Desc.		:
//=============================================================================
void CDlg_MasterMode::OnBnClickedTest()
{
	CString strValue;

	m_ed_Item[EIT_MST_MasterInfoX].GetWindowText(strValue);
//	m_pModelinfo->iMasterInfoX = _ttoi(strValue);

	m_ed_Item[EIT_MST_MasterInfoY].GetWindowText(strValue);
//	m_pModelinfo->iMasterInfoY = _ttoi(strValue);

	m_ed_Item[EIT_MST_MasterInfoR].GetWindowText(strValue);
//	m_pModelinfo->dbMasterInfoR = _ttof(strValue);

	GetOwner()->SendNotifyMessage(WM_MASTER_SET, 0, 0);
}


//=============================================================================
// Method		: OnBnClickedSave
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 16:33
// Desc.		:
//=============================================================================
void CDlg_MasterMode::OnBnClickedSave()
{
	GetUIData();
	GetOwner()->SendNotifyMessage(WM_MODEL_SAVE, 0, 0);
}

//=============================================================================
// Method		: OnBnClickedExit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 16:33
// Desc.		:
//=============================================================================
void CDlg_MasterMode::OnBnClickedExit()
{
	//GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, MASTER_End, 0);
	OnOK();
}
//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/28 - 19:39
// Desc.		:
//=============================================================================
void CDlg_MasterMode::GetUIData()
{
	CString strValue;
	int iOffsetValue;
	double dbOffsetValue;

	m_ed_Item[EIT_MST_MasterSpcX].GetWindowText(strValue);
	m_pModelinfo->nMasterSpcX = _ttoi(strValue);

	m_ed_Item[EIT_MST_MasterSpcY].GetWindowText(strValue);
	m_pModelinfo->nMasterSpcY = _ttoi(strValue);

	m_ed_Item[EIT_MST_MasterSpcR].GetWindowText(strValue);
	m_pModelinfo->dbMasterSpcR = _ttof(strValue);

	m_ed_Item[EIT_MST_MasterOffsetX].GetWindowText(strValue);
	iOffsetValue = _ttoi(strValue);
	m_pModelinfo->dwWidth -= iOffsetValue;

	m_ed_Item[EIT_MST_MasterOffsetY].GetWindowText(strValue);
	iOffsetValue = _ttoi(strValue);
	m_pModelinfo->dwHeight -= iOffsetValue;

	m_ed_Item[EIT_MST_MasterOffsetR].GetWindowText(strValue);
	dbOffsetValue = _ttof(strValue);
	m_pModelinfo->stRotate[0].stRotateOpt.dbMasterDegree -= dbOffsetValue;
}
//=============================================================================
// Method		: PermissionMode
// Access		: public  
// Returns		: void
// Parameter	: enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2018/3/5 - 16:58
// Desc.		:
//=============================================================================
void CDlg_MasterMode::PermissionMode(enPermissionMode InspMode)
{
	switch (InspMode)
	{
	case Permission_Operator:
		m_bn_MasterTest.EnableWindow(FALSE);
		m_bn_MasterSave.EnableWindow(FALSE);

		for (UINT nIdx = 0; nIdx < EIT_MST_MAXNUM; nIdx++)
			m_ed_Item[nIdx].EnableWindow(FALSE);
		break;

	case Permission_Manager:
	case Permission_Administrator:
	case Permission_Engineer:
	case Permission_CNC:
		m_bn_MasterTest.EnableWindow(TRUE);
		m_bn_MasterSave.EnableWindow(TRUE);

		for (UINT nIdx = 0; nIdx < EIT_MST_MAXNUM; nIdx++)
			m_ed_Item[nIdx].EnableWindow(TRUE);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 16:51
// Desc.		:
//=============================================================================
void CDlg_MasterMode::SetUIData()
{
	CString strValue;

	strValue.Format(_T("%d"), m_pModelinfo->iMasterInfoX);
	m_ed_Item[EIT_MST_MasterInfoX].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pModelinfo->iMasterInfoY);
	m_ed_Item[EIT_MST_MasterInfoY].SetWindowText(strValue);

	strValue.Format(_T("%.1f"), m_pModelinfo->dbMasterInfoR);
	m_ed_Item[EIT_MST_MasterInfoR].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pModelinfo->nMasterSpcX);
	m_ed_Item[EIT_MST_MasterSpcX].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pModelinfo->nMasterSpcY);
	m_ed_Item[EIT_MST_MasterSpcY].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), m_pModelinfo->dbMasterSpcR);
	m_ed_Item[EIT_MST_MasterSpcR].SetWindowText(strValue);
}

//=============================================================================
// Method		: SetMasterData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 17:20
// Desc.		:
//=============================================================================
void CDlg_MasterMode::SetMasterData(__in int iOffsetX, __in int iOffsetY, __in double dbDegree)
{
	CString strValue;

	strValue.Format(_T("%d"), iOffsetX);
	m_ed_Item[EIT_MST_MasterOffsetX].SetWindowText(strValue);

	strValue.Format(_T("%d"), iOffsetY);
	m_ed_Item[EIT_MST_MasterOffsetY].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), dbDegree);
	m_ed_Item[EIT_MST_MasterOffsetR].SetWindowText(strValue);
}
