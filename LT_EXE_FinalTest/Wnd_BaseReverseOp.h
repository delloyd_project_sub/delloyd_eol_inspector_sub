﻿#ifndef Wnd_BaseReverseOp_h__
#define Wnd_BaseReverseOp_h__

#pragma once

#include "Wnd_ReverseOp.h"

// CWnd_BaseReverseOp

class CWnd_BaseReverseOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseReverseOp)

public:
	CWnd_BaseReverseOp();
	virtual ~CWnd_BaseReverseOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_ReverseOp m_wndReverseOp[TICnt_Reverse];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseReverseOp_h__
