﻿#ifndef List_Work_Reverse_h__
#define List_Work_Reverse_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Reverse_Worklist
{
	Reverse_W_Recode,
	Reverse_W_Time,
	Reverse_W_Equipment,
	Reverse_W_Model,
	Reverse_W_SWVersion,
	Reverse_W_LOTNum,
	Reverse_W_Barcode,
	Reverse_W_Operator,
	Reverse_W_Result,
	Reverse_W_State,
	Reverse_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_Reverse_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Operator"),
	_T("Result"),
	_T("State"),

	NULL
};

const int	iListAglin_Reverse_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Reverse_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_Reverse

class CList_Work_Reverse : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_Reverse)

public:
	CList_Work_Reverse();
	virtual ~CList_Work_Reverse();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_Reverse_h__
