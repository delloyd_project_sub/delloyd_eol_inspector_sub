﻿//*****************************************************************************
// Filename	: 	Grid_PogoCnt.cpp
// Created	:	2016/6/21 - 22:22
// Modified	:	2016/6/21 - 22:22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Grid_PogoCnt.h"


static LPCTSTR lpszRowHeader[] =
{
	_T("POGO Count"),
	_T("SOCKET 1"),
	_T("SOCKET 2"),
	_T("SOCKET 3"),
	_T("SOCKET 4"),
	NULL
};

typedef enum
{
	IDX_Y_Header = 0,
	IDX_Y_SOCKET1,
	IDX_Y_SOCKET2,
	IDX_Y_SOCKET3,
	IDX_Y_SOCKET4,
	IDX_ROW_MAX,
}enumRowHeaderYield;

static const COLORREF clrRowHeader[] =
{
	RGB(0, 150, 0),
	RGB(100, 150, 250),
	RGB(100, 150, 250),
	RGB(100, 150, 250),
	RGB(100, 150, 250),
	RGB(100, 150, 250),
	RGB(100, 150, 250),
	RGB(135, 169, 213)
};


static LPCTSTR lpszColHeader[] =
{
	_T(""),
	_T(""),
	NULL
};

typedef enum
{
	IDX_X_Header = 0,
	IDX_X_Item = IDX_X_Header,
	IDX_X_Data,
	IDX_COL_MAX,
}enumColHeaderYield;


#define		RGB_BLACK		RGB(0x00, 0x00, 0x00)
#define		RGB_WHITE		RGB(0xFF, 0xFF, 0xFF)
#define		RGB_YELLOW		RGB(0xFF, 0xFF, 0x00)
#define		RGB_ROW_HEADER	RGB(63, 101,  169)
#define		RGB_BK_ID		RGB(135, 169, 213)
#define		RGB_COL_HEADER	RGB(0xFF, 200, 100)
#define		RGB_TITLE		RGB(150, 200, 0xFF)

#define		RGB_WARNING		RGB(250, 90, 90)

//=============================================================================
//
//=============================================================================
CGrid_PogoCnt::CGrid_PogoCnt()
{
	SetRowColCount(IDX_ROW_MAX, IDX_COL_MAX);

	//setup the fonts
	VERIFY(m_font_Header.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pstPogoCnt = NULL;
}

//=============================================================================
//
//=============================================================================
CGrid_PogoCnt::~CGrid_PogoCnt()
{
	m_font_Header.DeleteObject();
	m_font_Data.DeleteObject();
}

//=============================================================================
// Method		: OnSetup
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/14 - 14:30
// Desc.		:
//=============================================================================
void CGrid_PogoCnt::OnSetup()
{
	__super::OnSetup();
}

//=============================================================================
// Method		: DrawGridOutline
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_PogoCnt::DrawGridOutline()
{
	CGrid_Base::DrawGridOutline();

	SetDefFont(&m_font_Data);

	// 헤더를 설정한다.
	InitHeader();
}

//=============================================================================
// Method		: CGrid_PogoCnt::CalGridOutline
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_PogoCnt::CalGridOutline()
{
	//CGrid_Base::CalGridOutline();

	// 윈도우 면적에 따라서 열의 너비를 결정
	CRect rect;
	GetWindowRect(&rect);
	int nWidth = rect.Width();
	int nHeight = rect.Height();

	if ((nWidth <= 0) || (nHeight <= 0))
		return;

	// 기본 열 추가 ---------------------------------------
	int iUnitWidth = nWidth / m_nMaxCols;
	int iMisc = nWidth - (iUnitWidth * (m_nMaxCols - 1));
	SetColWidth(IDX_X_Header, iMisc);
	for (UINT iCol = IDX_X_Header + 1; iCol < m_nMaxCols; iCol++)
	{
		SetColWidth(iCol, iUnitWidth);
	}

	// 패턴 행 헤더 추가 ----------------------------------		
	UINT nUnitHeight = nHeight / m_nMaxRows;
	UINT nRemindHeight = nHeight - (nUnitHeight * m_nMaxRows);

	// Height 계산하고 남거나 모자르는 공간 계산하여 헤더 Height 추가 처리
	for (UINT iRow = 0; iRow < nRemindHeight; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight + 1);
	}

	for (UINT iRow = nRemindHeight; iRow < m_nMaxRows; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight);
	}
}

//=============================================================================
// Method		: CGrid_PogoCnt::InitRowHeader
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_PogoCnt::InitHeader()
{
	JoinCells(IDX_X_Item, IDX_Y_Header, IDX_X_Data, IDX_Y_Header);
	QuickSetBackColor_COLORREF(IDX_X_Item, IDX_Y_Header, RGB_ROW_HEADER);
	QuickSetTextColor(IDX_X_Item, IDX_Y_Header, RGB_WHITE);
	QuickSetText(IDX_X_Item, IDX_Y_Header, lpszRowHeader[IDX_Y_Header]);

	for (UINT iRow = 1; iRow < m_nMaxRows; iRow++)
	{
		QuickSetFont(IDX_X_Item, iRow, &m_font_Data);
		QuickSetBackColor_COLORREF(IDX_X_Item, iRow, clrRowHeader[iRow]);
		QuickSetTextColor(IDX_X_Item, iRow, RGB_BLACK);
		QuickSetText(IDX_X_Item, iRow, lpszRowHeader[iRow]);

		QuickSetFont(IDX_X_Data, iRow, &m_font_Data);
		QuickSetBackColor_COLORREF(IDX_X_Data, iRow, RGB_WHITE);
		QuickSetTextColor(IDX_X_Data, iRow, RGB_BLACK);
		QuickSetText(IDX_X_Data, iRow, _T("0 / 0"));
	}

	//QuickSetText(IDX_X_Data, IDX_Y_BeatCnt, _T("0.00 %"));
}

//=============================================================================
// Method		: OnHint
// Access		: virtual protected  
// Returns		: int
// Parameter	: int col
// Parameter	: long row
// Parameter	: int section
// Parameter	: CString * string
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
int CGrid_PogoCnt::OnHint(int col, long row, int section, CString *string)
{
	return FALSE;
}

//=============================================================================
// Method		: CGrid_PogoCnt::OnGetCell
// Access		: virtual protected 
// Returns		: void
// Parameter	: int col
// Parameter	: long row
// Parameter	: CUGCell * cell
// Qualifier	:
// Last Update	: 2015/12/10 - 23:28
// Desc.		:
//=============================================================================
void CGrid_PogoCnt::OnGetCell(int col, long row, CUGCell *cell)
{
	CGrid_Base::OnGetCell(col, row, cell);

	switch (row)
	{
	case IDX_Y_Header:
		if (IDX_X_Item == col)
			cell->SetBorder(cell->GetBorder() | UG_BDR_RMEDIUM | UG_BDR_BMEDIUM);
		break;
	}
}

//=============================================================================
// Method		: OnDrawFocusRect
// Access		: virtual protected  
// Returns		: void
// Parameter	: CDC * dc
// Parameter	: RECT * rect
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_PogoCnt::OnDrawFocusRect(CDC *dc, RECT *rect)
{

}

//=============================================================================
// Method		: UpdatePogoCnt_Focus
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/22 - 13:42
// Desc.		:
//=============================================================================
void CGrid_PogoCnt::UpdatePogoCnt_Socket()
{
 	if (m_pstPogoCnt)
 	{
 		CString strText;

		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			strText.Format(_T("%d / %d"), m_pstPogoCnt->dwCount[nIdx], m_pstPogoCnt->dwCount_Max[nIdx]);

			// 상한치에 근접했을 경우 붉은 색으로 표시
			if ((m_pstPogoCnt->dwCount_Max[nIdx] <= m_pstPogoCnt->dwCount[nIdx]) || ((m_pstPogoCnt->dwCount_Max[nIdx] - m_pstPogoCnt->dwCount[nIdx]) < 50))
			{
				QuickSetBackColor_COLORREF(IDX_X_Data, IDX_Y_SOCKET1 + nIdx, RGB_WARNING);
			}
			else
			{
				QuickSetBackColor_COLORREF(IDX_X_Data, IDX_Y_SOCKET1 + nIdx, RGB_WHITE);
			}

			QuickSetText(IDX_X_Data, IDX_Y_SOCKET1 + nIdx, strText);
		}

 		RedrawCol(IDX_X_Data);
 	}
}

//=============================================================================
// Method		: ShowResetButton
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bShow
// Qualifier	:
// Last Update	: 2016/12/6 - 17:43
// Desc.		:
//=============================================================================
void CGrid_PogoCnt::ShowResetButton(__in BOOL bShow)
{

}
