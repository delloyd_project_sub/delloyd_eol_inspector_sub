#pragma once
#include "resource.h"
#include "Def_TestDevice.h"

// CDlg_StartUpCheck 대화 상자입니다.

class CDlg_StartUpCheck : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_StartUpCheck)

public:
	CDlg_StartUpCheck(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_StartUpCheck();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_STARTUP_CHECK };

	void SetPtrInspectionInfo(__inout ST_InspectionInfo* pstInspInfo)
	{
		if (pstInspInfo == NULL)
			return;

		m_pstInspInfo = pstInspInfo;
	};
	
protected:
	CFont		m_font;
	CFont		m_font2;
	CVGStatic	m_stNGItem;
	CVGStatic	m_stText;

	CMFCButton	m_bn_TestItem[TIID_MaxEnum];
	CMFCButton	m_bn_OK;
	CMFCButton	m_bn_Cancel;

	ST_InspectionInfo*	m_pstInspInfo;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnBnClickedBnOK();
	afx_msg void OnBnClickedBnCancel();
	//afx_msg void OnCbnSelChangeTestItem();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
