﻿//*****************************************************************************
// Filename	: 	Def_Motion.h
// Created	:	2016/10/03 - 16:40
// Modified	:	2016/10/03 - 16:40
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Motion_h__
#define Def_Motion_h__

#include "Def_Motor.h"

#define DEF_MOTOR_ORIGIN_TIME	60000

typedef enum enAxis_Motor
{
	AX_StageX = 0,
	AX_StageY = 1,
	AX_StageDistance = 2,
	AX_Stage_Max
};

static LPCTSTR g_lpszMotor_Axis[] =
{
	_T(" Stage X "),		//00
	_T(" Stage Y "),		//00
	_T(" Stage Distance "),		//00
	NULL
};

static double g_dbMotor_AxisLeadMax[AX_Stage_Max] =
{
	340.0,		// mm
};

static double g_dbMotor_AxisResolution[AX_Stage_Max] =
{
	1000.0,		// mm
};

static double g_dbMotorAxis_Y_Blemish[enInsptrSysTeach_Max] =
{
	129000.0,		// mm
	127000.0,	// mm
};



typedef enum enIO_TowerLamp
{
	TowerLampRed = 0,
	TowerLampYellow,
	TowerLampGreen,
	TowerLampBuzzer,
	TowerLampMaxNum,
};

typedef enum enIO_Cylinder
{
	CylinderModule = 0,
	CylinderMaxNum,
};

//=============================================================================
// IO Table, Define Name
//=============================================================================
// typedef enum enIO_In_BitOffset
// {
// 	DI_MainPower,
// 	DI_EMO,
// 	DI_SensorSafetyArea,
// 	DI_SensorDoor,
// 	DI_SensorAirCheck,
// 	DI_NotUseBit_05,
// 	DI_StartBtn,
// 	DI_StopBtn,
// 	DI_NotUseBit_08,
// 	DI_CylSensorBlemish_In,
// 	DI_CylSensorBlemish_Out,
// 	DI_NotUseBit_11,
// 	DI_NotUseBit_12,
// 	DI_NotUseBit_13,
// 	DI_NotUseBit_14,
// 	DI_FailBox,
// 	DI_NotUseBit_Max,
// };
typedef enum enIO_In_BitOffset
{
	DI_EMO,
	DI_StartBtn,
	DI_StopBtn,
	DI_Initial,
	DI_Door,
	DI_Area,
	DI_NotUseBit_6,
	DI_NotUseBit_7,
	DI_SensorParticleIn,
	DI_SensorParticleOut,
	DI_NotUseBit_10,
	DI_NotUseBit_11,
	DI_NotUseBit_12,
	DI_NotUseBit_13,
	DI_NotUseBit_14,
	DI_FailBox,
	DI_NotUseBit_Max,
};

//=============================================================================
// IO Table, UI View
//=============================================================================
//static LPCTSTR g_lpszDI_bit_Desc[] =
//{
//	_T("Main Power"),
//	_T("E.M.O"),
//	_T("[Sensor] Safety Zone"),
//	_T("[Sensor] Door Open"),
//	_T("[Sensor] Air Check"),
//	_T(""),
//	_T("Start Button"),
//	_T("Stop Button"),
//	_T(""),
//	_T("[Sensor] Blemish In"),
//	_T("[Sensor] Blemish Out"),
//	_T(""),
//	_T(""),
//	_T(""),
//	_T(""),
//	_T("[Sensor] Fail Box"),
//
//	NULL
//};

static LPCTSTR g_lpszDI_bit_Desc[] =
{
	_T("E.M.O"),
	_T("Start Button"),
	_T("Stop Button"),
	_T("INIT"),
	_T("DOOR"),
	_T("Area"),
	_T(""),
	_T(""),
	_T("[Sensor] Particle In"),
	_T("[Sensor] Particle Out"),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T("[Sensor] Fail Box"),
	NULL
};
//=============================================================================
// IO Table, Define Name
//=============================================================================
// typedef enum enIO_Out_BitOffset
// {
// 	DO_BoardPower,														//	00
// 	DO_NotUseBit_01,														//	01
// 	DO_ChartLight,															//	02
// 	DO_BlemishIR,															//	03
// 	DO_BlemishLight,														//	04
// 	DO_NotUseBit_05,														//	05
// 	DO_LampStartBtn,														//	06
// 	DO_LampStopBtn,														//	07
// 	DO_NotUseBit_08,														//	08
// 	DO_CylBlemish_In,													//	09
// 	DO_CylBlemish_Out,													//	10
// 	DO_NotUseBit_11,														//	11
// 	DO_TowerLamp_Red,												//	12
// 	DO_TowerLamp_Yellow,												//	13
// 	DO_TowerLamp_Green,												//	14
// 	DO_TowerLamp_Buzzer,											//	15
// 	DO_NotUseBit_Max,
// };
typedef enum enIO_Out_BitOffset
{
	DO_NotUseBit_01,
	DO_LampStartBtn,
	DO_LampStopBtn,
	DO_LampInitial,
	DO_LampRun,
	DO_LampPass,
	DO_LampFail,
	DO_SOLParticleOut,
	DO_SOLParticleIn,
	DO_RLYParticleIRLight,
	DO_RLYParticleLEDLight,
	DO_TowerLamp_Red,
	DO_TowerLamp_Yellow,
	DO_TowerLamp_Green,
	DO_TowerLamp_Buzzer,
	DO_Fluorescent_Lamp,
	DO_NotUseBit_Max,
};

//=============================================================================
// IO Table, UI View
//=============================================================================
//static LPCTSTR g_lpszDO_bit_Desc[] =
//{
//	_T("Board Power"),
//	_T(""),
//	_T("[Light] Chart"),
//	_T("[IR] Blemish"),
//	_T("[Light] Blemish"),
//	_T(""),
//	_T("[Lamp] Start Button"),
//	_T("[Lamp] Stop Button"),
//	_T(""),
//	_T("[Cylinder] Blemish In"),
//	_T("[Cylinder] Blemish Out"),
//	_T(""),
//	_T("[Lamp] Tower Red"),
//	_T("[Lamp] Tower Yellow"),
//	_T("[Lamp] Tower Green"),
//	_T("[Lamp] Tower Buzzer"),
//	
//	NULL
//};

static LPCTSTR g_lpszDO_bit_Desc[] =
{
	_T(""),
	_T("[LAMP] START"),
	_T("[LAMP] STOP"),
	_T("[LAMP] INIT"),
	_T("[LAMP] RUN"),
	_T("[LAMP] PASS"),
	_T("[LAMP] FAIL"),
	_T("[SOL] Particle Out"),
	_T("[SOL] Particle In"),
	_T("[RLY] Particle IR Light"),
	_T("[RLY] Particle LED Light"),
	_T("[TOWER] RED"),
	_T("[TOWER] YELLOW"),
	_T("[TOWER] GREEN"),
	_T("[TOWER] BUZZER"),
	_T("[LAMP] Fluorescent"),
	NULL
};
static LPCTSTR g_lpszDO_Signal[] =
{
	_T("OFF"),
	_T("ON"),
	NULL
};

// TEACH
typedef enum enMotor_TeachItem
{
	MT_1 = 0,
	MT_MaxTeachItem, 
};

static LPCTSTR g_lpszMotor_Teach[] =
{
	_T(" 01. "),	
	NULL
};

typedef enum enTeach_ImageTest
{
	TC_ImageTest_Loading_X = 0,
	TC_ImageTest_Loading_Y,
	TC_ImageTest_Loading_Dist,
	//TC_ImageTest_Inspection_X,
	//TC_ImageTest_Inspection_Y,
	//TC_ImageTest_Inspection_Dist,
	TC_ImageTest_Crash_Start,
	TC_ImageTest_Crash_End,
	TC_ImageTest_Par_Inspection_X,
	TC_ImageTest_Par_Inspection_Y,
	TC_ImageTest_Par_Inspectoin_Dist,
	TC_ImageTest_Par_Loading_Dist,
	TC_ImageTest_MAX,
};		   

static LPCTSTR g_szTeachItem_Image[] =
{
	_T("Load X (Puls)			"),			// Loading X (Puls)				
	_T("Load Y (Puls)			"),			// Loading Y (Puls)				
	_T("Load Dist (Puls)	"),			// Loading Z (Puls)		
	//_T("Inspect X (Puls)		"),			// Inspection X (Puls)				
	//_T("Inspect Y (Puls)		"),			// Inspection Y (Puls)				
	//_T("Inspect Dist (Puls)	"),			// Inspection Z (Puls)		
	_T("Crash Start (Puls)"),			// Crash Start Point Z (Puls)	
	_T("Crash End (Puls)	"),			// Crash End Point Z (Puls)	
	_T("Par_Inspect X (Puls)		"),			// Particle Inspection X (Puls)				
	_T("Par_Inspect Y (Puls)		"),			// Particle Inspection Y (Puls)				
	_T("Par_Inspect Dist (Puls)	"),			// Particle Inspection Z (Puls)				
	_T("Par_Load Dist (Puls)	"),			// Particle Loading Z (Puls)				
	NULL
};
#define TC_ALL_MAX TC_ImageTest_MAX
//------------------------------------------------------
// TEACH 위치 정보
//------------------------------------------------------
typedef struct _tag_TeachInfo
{
	double dbTeachData[TC_ALL_MAX];

	_tag_TeachInfo()
	{
		for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
			dbTeachData[nIdx] = 0.0;
	}

	_tag_TeachInfo& operator= (_tag_TeachInfo& ref)
	{
		for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
			dbTeachData[nIdx] = ref.dbTeachData[nIdx];

		return *this;
	};
}ST_TeachInfo, *LPST_TeachInfo;


#endif // Def_Motion_h__
