﻿//*****************************************************************************
// Filename	: 	List_Worklist.h
// Created	:	2016/08/12
// Modified	:	2016/08/12
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef List_Worklist_h__
#define List_Worklist_h__

#pragma once

#include "Def_DataStruct.h"

//-----------------------------------------------------------------------------
// CList_Worklist
//-----------------------------------------------------------------------------
class CList_Worklist : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Worklist)

public:
	CList_Worklist();
	virtual ~CList_Worklist();

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick			(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	CFont		m_Font;

	virtual void	InitHeader		();
	virtual void	InsertRowItemz	(__in const ST_Worklist* pstWorklist);
	void			AdjustColWidth	();

	void		SetRowItemz			(__in UINT nRow, __in const ST_Worklist* pstWorklist);

	CArray<ST_Worklist, ST_Worklist&> m_Worklist;

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);

	void		ClearWorklist		();

	BOOL		GetWorklist			(__in UINT nRowIndex, __out ST_Worklist& stWorklist);

};

#endif // List_Worklist_h__


