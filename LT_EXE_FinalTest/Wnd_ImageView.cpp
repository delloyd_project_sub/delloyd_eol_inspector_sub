﻿// Wnd_ImageView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_ImageView.h"
#include "resource.h"

// CWnd_ImageView

enum enMouse
{
	Mouse_None,
	Mouse_Edit,
};

typedef enum enPicMode
{
	PICMode_LINE,		// 직선
	PICMode_RECTANGLE,	// 직사각형
	PICMode_CIRCLE,		// 원
	PICMode_TXT,		// 글씨
	PICMode_MaxNum,
};

enum enMouseEdit
{
	MouseEdit_Init,
	MouseEdit_Left_WE,
	MouseEdit_Right_WE,
	MouseEdit_Top_NS,
	MouseEdit_Bottom_NS,
	MouseEdit_Left_NWSE,
	MouseEdit_Right_NWSE,
	MouseEdit_Left_NESW,
	MouseEdit_Right_NESW,
	MouseEdit_ALL,
};

IMPLEMENT_DYNAMIC(CWnd_ImageView, CWnd)

CWnd_ImageView::CWnd_ImageView()
{
	m_iROInum			= -1;
	m_nROICntMax		= 0;
	m_hTimerViewCheck	= NULL;
	m_hTimerQueue		= NULL;

	m_pLoadImage		= NULL;
	m_pPicImage			= NULL;
	
	m_BitmapInfo.bmiHeader.biSize			= sizeof(BITMAPINFOHEADER);
	m_BitmapInfo.bmiHeader.biPlanes			= 1;
	m_BitmapInfo.bmiHeader.biCompression	= BI_RGB;
	m_BitmapInfo.bmiHeader.biSizeImage		= 0;
	m_BitmapInfo.bmiHeader.biXPelsPerMeter	= 0;
	m_BitmapInfo.bmiHeader.biYPelsPerMeter	= 0;
	m_BitmapInfo.bmiHeader.biClrUsed		= 0;
	m_BitmapInfo.bmiHeader.biClrImportant	= 0;
	m_BitmapInfo.bmiColors[0].rgbBlue		= 0;
	m_BitmapInfo.bmiColors[0].rgbGreen		= 0;
	m_BitmapInfo.bmiColors[0].rgbRed		= 0;
	m_BitmapInfo.bmiColors[0].rgbReserved	= 0;
	m_BitmapInfo.bmiHeader.biWidth			= 0;
	m_BitmapInfo.bmiHeader.biHeight			= 0;

	m_pstModelInfo		= NULL;

	m_nMouseEdit		= MouseEdit_Init;

	m_bflg_MauseMode	= Mouse_None;
	m_bflg_Timer		= FALSE;
	m_bMasueModeUse		= FALSE;

 	CreateTimerQueue_Mon();
 	CreateTimerViewCheck();
}

CWnd_ImageView::~CWnd_ImageView()
{
	DeleteTimerQueue_Mon();

	if (m_pPicImage != NULL)
	{
		cvReleaseImage(&m_pPicImage);
		m_pPicImage = NULL;
	}

	if (m_pLoadImage != NULL)
	{
		cvReleaseImage(&m_pLoadImage);
		m_pLoadImage = NULL;
	}

}

BEGIN_MESSAGE_MAP(CWnd_ImageView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
	ON_WM_ERASEBKGND()
	ON_WM_SHOWWINDOW()
	// ON_COMMAND(ID_POPUP_IMAGELOAD, &CWnd_ImageView::OnPopImageLoad)
END_MESSAGE_MAP()
// CWnd_ImageView 메시지 처리기입니다.

//=============================================================================
// Method		: CreateTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_ImageView::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue = CreateTimerQueue();
		if (NULL == m_hTimerQueue)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		//	AddLog(_T("*** Exception Error : CreateTimerQueue_Mon ()"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_ImageView::DeleteTimerQueue_Mon()
{
	if (m_hTimerQueue == NULL)
		return;

	// 타이머가 종료될때까지 대기
	__try
	{
		if (!DeleteTimerQueue(m_hTimerQueue))
			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
		else
			m_hTimerQueue = NULL;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_ImageView::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CWnd_ImageView::DeleteTimerQueue_Mon()\n"));
}

//=============================================================================
// Method		: CreateTimer_InputCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_ImageView::CreateTimerViewCheck()
{
	__try
	{
		// Time Check Timer
		if (NULL == m_hTimerViewCheck)
		if (!CreateTimerQueueTimer(&m_hTimerViewCheck, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutineViewCheck, (PVOID)this, 3000, 30, WT_EXECUTEDEFAULT))
		{
			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_ImageView::CreateTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimerViewCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_ImageView::DeleteTimerViewCheck()
{
	__try
	{
		if (DeleteTimerQueueTimer(m_hTimerQueue, m_hTimerViewCheck, NULL))
		{
			m_hTimerViewCheck = NULL;
		}
		else
		{
			TRACE(_T("DeleteTimerQueueTimer : m_hTimer_InputCheck failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_ImageView::DeleteTimerViewCheck()\n"));
	}
}

//=============================================================================
// Method		: TimerRoutine_InputCheck
// Access		: protected static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
VOID CALLBACK CWnd_ImageView::TimerRoutineViewCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CWnd_ImageView* pThis = (CWnd_ImageView*)lpParam;

	pThis->OnImageView();
}

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/10/16 - 15:30
// Desc.		:
//=============================================================================
int CWnd_ImageView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_stImage.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_stImage.SetColorStyle(CVGStatic::ColorStyle_Black);
	m_stImage.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	return 0;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/10/16 - 15:30
// Desc.		:
//=============================================================================
BOOL CWnd_ImageView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_BTNTEXT + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/10/16 - 15:30
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if (cx == 0 || cy == 0)
		return;

	int Left = 0;
	int Top = 0;
	int W = cx;
	int H = cy;

	if (m_pLoadImage != NULL)
	{
		W = m_pLoadImage->width;
		H = m_pLoadImage->height;

		if (W >= H)
		{
			if (cx >= m_pLoadImage->width)
			{
				W = m_pLoadImage->width;
			}
			else
			{
				W = cx;
			}

			H = m_pLoadImage->height * W / m_pLoadImage->width;

			Left = abs(cx - W) / 2;
			Top = abs(cy - H) / 2;
		}
		else
		{
			if (cy >= m_pLoadImage->height)
			{
				H = m_pLoadImage->height;
			}
			else
			{
				H = cy;
			}

			W = m_pLoadImage->width * H / m_pLoadImage->height;

			Left = abs(cx - W) / 2;
			Top = abs(cy - H) / 2;
		}
	}

	m_stImage.MoveWindow(Left, Top, W, H);
}

//=============================================================================
// Method		: OnLButtonDown
// Access		: protected  
// Returns		: void
// Parameter	: UINT nFlags
// Parameter	: CPoint point
// Qualifier	:
// Last Update	: 2017/10/17 - 12:56
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (NULL == m_pLoadImage || NULL == m_pPicImage) 
		return;

	m_bflg_MauseMode = Mouse_Edit;


	CRect rt;
	m_stImage.GetWindowRect(&rt);

	CPoint WndPoint = point;
	ClientToScreen(&WndPoint);

	// CStatic 안에 들어오면,
	if (WndPoint.x > rt.left
		&& WndPoint.x < rt.right
		&& WndPoint.y > rt.top
		&& WndPoint.y < rt.bottom)
	{
		m_ptInit.x = (int)((WndPoint.x - rt.left) * (double)((double)m_pLoadImage->width / (double)rt.Width()));
		m_ptInit.y = (int)((WndPoint.y - rt.top) * (double)((double)m_pLoadImage->height / (double)rt.Height()));
	}

	CWnd::OnLButtonDown(nFlags, point);
}

//=============================================================================
// Method		: OnLButtonUp
// Access		: protected  
// Returns		: void
// Parameter	: UINT nFlags
// Parameter	: CPoint point
// Qualifier	:
// Last Update	: 2017/10/17 - 12:56
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_bflg_MauseMode = Mouse_None;
	SetCursor(LoadCursor(NULL, IDC_ARROW));

	CWnd::OnLButtonUp(nFlags, point);
}

void CWnd_ImageView::OnRButtonUp(UINT nFlags, CPoint point)
{
	CRect rc;
	GetClientRect(&rc);

	if (point.x > rc.left
		&& point.x < rc.right
		&& point.y > rc.top
		&& point.y < rc.bottom)
	{
		CMenu mnTemp, *pContextMenu;
		mnTemp.LoadMenuW(IDR_IMAGE_LOAD);
		pContextMenu = mnTemp.GetSubMenu(0);

		ClientToScreen(&point);
		//pContextMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	}

	CWnd::OnRButtonUp(nFlags, point);
}

//=============================================================================
// Method		: OnMouseMove
// Access		: protected  
// Returns		: void
// Parameter	: UINT nFlags
// Parameter	: CPoint point
// Qualifier	:
// Last Update	: 2017/10/17 - 12:56
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnMouseMove(UINT nFlags, CPoint point)
{
	if (FALSE == m_bMasueModeUse)
		return;

	if (NULL == m_pLoadImage || NULL == m_pPicImage)
		return;

	CRect rt;
	m_stImage.GetWindowRect(&rt);
	
	CPoint WndPoint = point;
	ClientToScreen(&WndPoint);

	// CStatic 안에 들어오면,
	if (WndPoint.x > rt.left 
		&& WndPoint.x < rt.right
		&& WndPoint.y > rt.top
		&& WndPoint.y < rt.bottom)
	{
		
		m_ptEdit.x = WndPoint.x - rt.left;
		m_ptEdit.y = WndPoint.y - rt.top;

		if (m_bflg_MauseMode == Mouse_Edit)
		{
			int mx = (int)(m_ptEdit.x * (double)((double)m_pLoadImage->width / (double)rt.Width()));
			int my = (int)(m_ptEdit.y * (double)((double)m_pLoadImage->height / (double)rt.Height()));

			int iAddX = mx - m_ptInit.x;
			int iAddY = my - m_ptInit.y;

			if (m_iROInum < 0)
				return;

			CRect rtROI = GetROIData(m_iROInum);

			switch (m_nMouseEdit)
			{
			case MouseEdit_Left_WE:
				if (rtROI.left + iAddX < 0)
					return;

				if (rtROI.right + iAddX > m_pLoadImage->width)
					return;

				rtROI.left += iAddX;
				break;
			case MouseEdit_Right_WE:
				if (rtROI.left + iAddX < 0)
					return;

				if (rtROI.right + iAddX > m_pLoadImage->width)
					return;

				rtROI.right += iAddX;
				break;
			case MouseEdit_Top_NS:
				if (rtROI.top + iAddY < 0)
					return;

				if (rtROI.bottom + iAddY > m_pLoadImage->height)
					return;

				rtROI.top += iAddY;
				break;
			case MouseEdit_Bottom_NS:
				if (rtROI.top + iAddY < 0)
					return;

				if (rtROI.bottom + iAddY > m_pLoadImage->height)
					return;

				rtROI.bottom += iAddY;
				break;
			case MouseEdit_Left_NWSE:
				if (rtROI.left + iAddX < 0 || rtROI.top + iAddY < 0)
					return;

				if (rtROI.right + iAddX > m_pLoadImage->width || rtROI.bottom + iAddY > m_pLoadImage->height)
					return;

				rtROI.left += iAddX;
				rtROI.top += iAddY;
				break;
			case MouseEdit_Right_NWSE:
				if (rtROI.left + iAddX < 0 || rtROI.top + iAddY < 0)
					return;

				if (rtROI.right + iAddX > m_pLoadImage->width || rtROI.bottom + iAddY > m_pLoadImage->height)
					return;

				rtROI.right += iAddX;
				rtROI.bottom += iAddY;
				break;
			case MouseEdit_Left_NESW:
				if (rtROI.left + iAddX < 0 || rtROI.top + iAddY < 0)
					return;

				if (rtROI.right + iAddX > m_pLoadImage->width || rtROI.bottom + iAddY > m_pLoadImage->height)
					return;

				rtROI.left += iAddX;
				rtROI.bottom += iAddY;
				break;
			case MouseEdit_Right_NESW:
				if (rtROI.left + iAddX < 0 || rtROI.top + iAddY < 0)
					return;

				if (rtROI.right + iAddX > m_pLoadImage->width || rtROI.bottom + iAddY > m_pLoadImage->height)
					return;

				rtROI.right += iAddX;
				rtROI.top += iAddY;
				break;
			case MouseEdit_ALL:

				if (rtROI.left + iAddX < 0 || rtROI.top + iAddY < 0)
					return;

				if (rtROI.right + iAddX > m_pLoadImage->width || rtROI.bottom + iAddY > m_pLoadImage->height)
					return;

				rtROI.left += iAddX;
				rtROI.top += iAddY;
				rtROI.right += iAddX;
				rtROI.bottom += iAddY;

				break;
			default:
				break;
			}


			SetROIData(m_iROInum, rtROI);

			GetOwner()->SendMessage(WM_CHANGE_PIC, 0, 0);

			m_ptInit.x = (int)(m_ptEdit.x * (double)((double)m_pLoadImage->width / (double)rt.Width()));
			m_ptInit.y = (int)(m_ptEdit.y * (double)((double)m_pLoadImage->height / (double)rt.Height()));
		}
	}
	else
	{
		m_ptEdit.x = -1;
		m_ptEdit.y = -1;
	}

	CWnd::OnMouseMove(nFlags, point);
}

//=============================================================================
// Method		: OnSetCursor
// Access		: protected  
// Returns		: BOOL
// Parameter	: CWnd * pWnd
// Parameter	: UINT nHitTest
// Parameter	: UINT message
// Qualifier	:
// Last Update	: 2017/10/17 - 12:57
// Desc.		:
//=============================================================================
BOOL CWnd_ImageView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if (FALSE == m_bMasueModeUse)
		return FALSE;

	if (NULL == m_pLoadImage || NULL == m_pPicImage)
		return FALSE;

	if (NULL == m_pstModelInfo)
		return FALSE;

	CRect rt;
	m_stImage.GetWindowRect(&rt);

	if (m_ptEdit.x + rt.left  > rt.left
		&& m_ptEdit.x + rt.left  < rt.right
		&& m_ptEdit.y + rt.top > rt.top
		&& m_ptEdit.y + rt.top < rt.bottom)
	{
		switch (m_pstModelInfo->nPicItem)
		{
		case PIC_Angle:
			m_nROICntMax = ROI_AL_Max;
			break;
		case PIC_CenterPoint:
			m_nROICntMax = 1;
			break;
		case PIC_Color:
			m_nROICntMax = ROI_CL_Max;
			break;
		case PIC_Current:
			m_nROICntMax = 0;
			break;
		case PIC_BlackSpot:
			m_nROICntMax = Part_ROI_Max;
			break;
		case PIC_DefectPixel:
			//m_nROICntMax = Part_ROI_Max;
			break;
		case PIC_Brightness:
			m_nROICntMax = ROI_BR_Max;
			break;
		case PIC_Rotate:
			m_nROICntMax = ROI_RT_Max;
			break;
		//case PIC_SFR:
		//	m_nROICntMax = ROI_SFR_Max;
		//	break;

		case PIC_EIAJ:
			m_nROICntMax = ReOp_ItemNum;
			break;
		//case PIC_PatternNoise:
		//	m_nROICntMax = ROI_PN_MaxEnum;
		//	break;
		case PIC_Reverse:
			m_nROICntMax = ROI_Rv_Max;
			break;
		default:
			m_nROICntMax = 0;
			break;
		}

		for (UINT nROI = 0; nROI < m_nROICntMax; nROI++)
		{
			CRect rtROI = GetROIData(nROI);

			int mx = (int)(m_ptEdit.x * (double)((double)m_pLoadImage->width / (double)rt.Width()));
			int my = (int)(m_ptEdit.y * (double)((double)m_pLoadImage->height / (double)rt.Height()));

			if (mx >= rtROI.left && mx <= rtROI.right && my >= rtROI.top && my <= rtROI.bottom)
			{
				m_iROInum = nROI;

				if (mx > rtROI.left + 5 && mx < rtROI.right - 5 && my > rtROI.top + 5 && my < rtROI.bottom - 5)
				{
					m_nMouseEdit = MouseEdit_ALL;
					break;
				}
				else if (mx > rtROI.left - 5 && mx < rtROI.left + 5 && my > rtROI.top + 5 && my < rtROI.bottom - 5)
				{
					m_nMouseEdit = MouseEdit_Left_WE;
					break;
				}
				else if (mx > rtROI.right - 5 && mx < rtROI.right + 5 && my > rtROI.top + 5 && my < rtROI.bottom - 5)
				{
					m_nMouseEdit = MouseEdit_Right_WE;
					break;
				}
				else if (mx > rtROI.left + 5 && mx < rtROI.right - 5 && my > rtROI.top - 5 && my < rtROI.top + 5)
				{
					m_nMouseEdit = MouseEdit_Top_NS;
					break;
				}
				else if (mx > rtROI.left + 5 && mx < rtROI.right - 5 && my > rtROI.bottom - 5 && my < rtROI.bottom + 5)
				{
					m_nMouseEdit = MouseEdit_Bottom_NS;
					break;
				}
				else if (mx > rtROI.left - 5 && mx < rtROI.left + 5 && my > rtROI.top - 5 && my < rtROI.top + 5)
				{
					m_nMouseEdit = MouseEdit_Left_NWSE;
					break;
				}
				else if (mx > rtROI.right - 5 && mx < rtROI.right + 5 && my > rtROI.bottom - 5 && my < rtROI.bottom + 5)
				{
					m_nMouseEdit = MouseEdit_Right_NWSE;
					break;
				}
				else if (mx > rtROI.left - 5 && mx < rtROI.left + 5 && my > rtROI.bottom - 5 && my < rtROI.bottom + 5)
				{
					m_nMouseEdit = MouseEdit_Left_NESW;
					break;
				}
				else if (mx > rtROI.right - 5 && mx < rtROI.right + 5 && my > rtROI.top - 5 && my < rtROI.top + 5)
				{
					m_nMouseEdit = MouseEdit_Right_NESW;
					break;
				}
				else
				{
					m_nMouseEdit = MouseEdit_Init;
				}
			}
			else
			{
				m_nMouseEdit = MouseEdit_Init;
			}
		}

		// 마우스 화살표 모양 변경
		switch (m_nMouseEdit)
		{
		case MouseEdit_Init:
			SetCursor(LoadCursor(NULL, IDC_ARROW));
			break;
		case MouseEdit_Left_WE:
		case MouseEdit_Right_WE:
			SetCursor(LoadCursor(NULL, IDC_SIZEWE));
			break;
		case MouseEdit_Top_NS:
		case MouseEdit_Bottom_NS:
			SetCursor(LoadCursor(NULL, IDC_SIZENS));
			break;
		case MouseEdit_Left_NWSE:
		case MouseEdit_Right_NWSE:
			SetCursor(LoadCursor(NULL, IDC_SIZENWSE));
			break;
		case MouseEdit_Left_NESW:
		case MouseEdit_Right_NESW:
			SetCursor(LoadCursor(NULL, IDC_SIZENESW));
			break;
		case MouseEdit_ALL:
			SetCursor(LoadCursor(NULL, IDC_SIZEALL));
			break;
		default:
			break;
		}

	}

	return TRUE;
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: public  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2017/2/27 - 15:02
// Desc.		:
//=============================================================================
BOOL CWnd_ImageView::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2018/1/12 - 11:25
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (FALSE == bShow)
	{
		OnUpdataResetImage();
	}
}

void CWnd_ImageView::OnPopImageLoad()
{
	OnUpdataLoadImage();
}

//=============================================================================
// Method		: OnUpdataResetImage
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/17 - 20:23
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnUpdataResetImage()
{
	if (m_pLoadImage != NULL)
	{
		cvReleaseImage(&m_pLoadImage);
		m_pLoadImage = NULL;
	}

	if (m_pPicImage != NULL)
	{
		cvReleaseImage(&m_pPicImage);
		m_pPicImage = NULL;
	}

	m_bflg_Timer = FALSE;
}

//=============================================================================
// Method		: OnUpdataLoadImage
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/16 - 15:36
// Desc.		:
//=============================================================================
BOOL CWnd_ImageView::OnUpdataLoadImage()
{
	BOOL bRet = TRUE;

	CString strFileExt = _T("Image File (*.BMP, *.PNG) | *.BMP;*.PNG; | All Files(*.*) |*.*||");
	CFileDialog fileDlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strImagePath;

	m_bflg_Timer = FALSE;

	Sleep(100);

	if (IDOK == fileDlg.DoModal())
	{
		CString strFilePath = fileDlg.GetPathName();

		if (m_pLoadImage != NULL)
		{
			cvReleaseImage(&m_pLoadImage);
			m_pLoadImage = NULL;
		}

		if (m_pPicImage != NULL)
		{
			cvReleaseImage(&m_pPicImage);
			m_pPicImage = NULL;
		}

		CString strFileFormat;
		if (AfxExtractSubString(strFileFormat, strFilePath, 1, '.') == TRUE)
		{
			if (strFileFormat.MakeLower() == _T("raw"))
			{
				WORD*	pBuf = new WORD[CAM_IMAGE_HEIGHT * CAM_IMAGE_WIDTH];

				USES_CONVERSION;
				FILE *infile = fopen(CT2A(strFilePath.GetBuffer(0)), "rb");
			
				if (infile == NULL)
				{
					return FALSE;
				}

				fread((char*)pBuf, sizeof(char), CAM_IMAGE_HEIGHT * CAM_IMAGE_WIDTH * 2, infile);
				fclose(infile);


				BYTE*	pBufData = new BYTE[CAM_IMAGE_HEIGHT * CAM_IMAGE_WIDTH];
				m_pLoadImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_16U, 1);

				memcpy(m_pLoadImage->imageData, (PWORD)pBuf, CAM_IMAGE_HEIGHT * CAM_IMAGE_WIDTH * sizeof(WORD));

				delete[] pBufData;
				delete[] pBuf;
			}
		}

		if (m_pLoadImage == NULL)
		{
			USES_CONVERSION;
			m_pLoadImage = cvLoadImage(CT2A(strFilePath.GetBuffer(0)), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
		}

		if (m_pLoadImage == NULL)
		{
			bRet = FALSE; // Dlg 선택 취소시 return
			return bRet;
		}

		m_pPicImage = cvCreateImage(cvSize(m_pLoadImage->width, m_pLoadImage->height), IPL_DEPTH_8U, 3);

	}
	else
	{
		bRet = FALSE; // Dlg 선택 취소시 return
	}

	CRect rc;
	GetClientRect(&rc);
	OnSize(0, rc.Width(), rc.Height());

	m_bflg_Timer = TRUE;

	return bRet;
}

//=============================================================================
// Method		: OnMonitorView
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/16 - 15:48
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnImageView()
{
	if (NULL == m_pstModelInfo)
		return;

	if (m_bflg_Timer == FALSE)
		return;

	if (m_pLoadImage != NULL)
	{
		if (m_stImage.m_hWnd != NULL)
		{
			m_BitmapInfo.bmiHeader.biBitCount	= m_pLoadImage->depth * m_pLoadImage->nChannels;
			m_BitmapInfo.bmiHeader.biWidth		= m_pLoadImage->width;
			m_BitmapInfo.bmiHeader.biHeight		= -1 * m_pLoadImage->height;
		
			cvCopy(m_pLoadImage, m_pPicImage);

			switch (m_pstModelInfo->nPicItem)
			{
			case PIC_Angle:
				PicAngle(m_pPicImage);
				break;
			case PIC_CenterPoint:
				PicCenterPoint(m_pPicImage);
				break;
			case PIC_Color:
				PicColor(m_pPicImage);
				break;
			case PIC_Current:
				PicCurrent(m_pPicImage);
				break;
			case PIC_Brightness:
				PicBrightness(m_pPicImage);
				break;
			case PIC_BlackSpot:
				PicParticle(m_pPicImage);
				break;
			case PIC_DefectPixel:
				PicDefectPixel(m_pPicImage);
				break;
			case PIC_Rotate:
				PicRotate(m_pPicImage);
				break;
			//case PIC_SFR:
			//	PicSFR(m_pPicImage);
			//	break;
			//case PIC_FFT:
			//	PicFFT(m_pPicImage);
			//	break;
			case PIC_EIAJ:
				PicEIAJ(m_pPicImage);
				break;
			case PIC_Reverse:
				PicReverse(m_pPicImage);
				break;
			//case PIC_PatternNoise:
			//	PicPatternNoise(m_pPicImage);
			//	break;
			default:
				break;
			}

			CDC *pcDC = m_stImage.GetDC();
			pcDC->SetStretchBltMode(COLORONCOLOR);

			CRect rt;
			m_stImage.GetClientRect(rt);

			StretchDIBits(pcDC->m_hDC, 0, 0, rt.Width(), rt.Height(), 0, 0, m_pPicImage->width, m_pPicImage->height, m_pPicImage->imageData, &m_BitmapInfo, DIB_RGB_COLORS, SRCCOPY);

			m_stImage.ReleaseDC(pcDC);
		}
	}
	else
	{
		m_stImage.SetText(_T(""));
	}

	Sleep(1);
}

//=============================================================================
// Method		: SetROIData
// Access		: protected  
// Returns		: void
// Parameter	: UINT nROI
// Parameter	: CRect rtROI
// Qualifier	:
// Last Update	: 2017/10/17 - 19:20
// Desc.		:
//=============================================================================
void CWnd_ImageView::SetROIData(UINT nROI, CRect rtROI)
{
	if (NULL == m_pstModelInfo)
		return;

	switch (m_pstModelInfo->nPicItem)
	{
	case PIC_Angle:
		m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case PIC_CenterPoint:
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointOpt.rtRoi = rtROI;
		break;
	case PIC_Color:
		m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case PIC_BlackSpot:
		m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case PIC_DefectPixel:
		//m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case PIC_Brightness:
		m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case PIC_Rotate:
		m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	//case PIC_SFR:
	//	m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nROI].rtRoi = rtROI;
	//	break;
	case PIC_EIAJ:
		m_pstModelInfo->stEIAJ[m_pstModelInfo->nTestCnt].stEIAJOp.StandardrectData[nROI].RegionList = rtROI;
		break;
	case PIC_Reverse:
		m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	//case PIC_PatternNoise:
	//	m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi = rtROI;
	//	break;
	default:
		break;
	}

	GetOwner()->SendNotifyMessage(WM_LIST_ROI_UPDATE, 0, 0);
}

//=============================================================================
// Method		: GetROIData
// Access		: protected  
// Returns		: CRect
// Parameter	: UINT nROI
// Qualifier	:
// Last Update	: 2017/10/17 - 19:03
// Desc.		:
//=============================================================================
CRect CWnd_ImageView::GetROIData(UINT nROI)
{
	CRect rtROI;

	if (NULL == m_pstModelInfo)
		return rtROI;

	switch (m_pstModelInfo->nPicItem)
	{
	case PIC_Angle:
		rtROI = m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nROI].rtRoi;
		break;
	case PIC_CenterPoint:
		rtROI = m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointOpt.rtRoi;
		break;
	case PIC_Color:
		rtROI = m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nROI].rtRoi;
		break;
	case PIC_BlackSpot:
		rtROI = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nROI].rtRoi;
		break;
	case PIC_DefectPixel:
		//rtROI = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nROI].rtRoi;
		break;
	case PIC_Brightness:
		rtROI = m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi;
		break;
	case PIC_Rotate:
		rtROI = m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nROI].rtRoi;
		break;
	//case PIC_SFR:
	//	rtROI = m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nROI].rtRoi;
	//	break;
	case PIC_EIAJ:
		rtROI = m_pstModelInfo->stEIAJ[m_pstModelInfo->nTestCnt].stEIAJOp.StandardrectData[nROI].RegionList;
		break;
	case PIC_Reverse:
		rtROI = m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nROI].rtRoi;
		break;
	//case PIC_PatternNoise:
	//	rtROI = m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi;
	//	break;
	default:
		break;
	}

	return rtROI;
}



int CWnd_ImageView::Overlay_PeakData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI)
{
	if (stOption.rectData[nROI].i_Range_Max == stOption.rectData[nROI].i_Range_Min)
		return 0;

	double dbValue = (double)(stData.nValueResult[nROI] - stOption.rectData[nROI].i_Range_Min) / (double)(stOption.rectData[nROI].i_Range_Max - stOption.rectData[nROI].i_Range_Min);

	double sx1 = stOption.rectData[nROI].PatternStartPos.x;
	double sy1 = stOption.rectData[nROI].PatternStartPos.y;
	double sx2 = stOption.rectData[nROI].PatternEndPos.x;
	double sy2 = stOption.rectData[nROI].PatternEndPos.y;

	double dbDist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int iOffset = (int)(dbDist * dbValue);

	return iOffset;
}

int CWnd_ImageView::Overlay_StandarData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI)
{
	if (stData.nValueResult[nROI] == stOption.rectData[nROI].i_Range_Min)
		return 0;

	double dbValue = (double)(stOption.rectData[nROI].i_Threshold_Min - stOption.rectData[nROI].i_Range_Min)
		/ (double)(stOption.rectData[nROI].i_Threshold_Max - stOption.rectData[nROI].i_Range_Min);

	double sx1 = stOption.rectData[nROI].PatternStartPos.x;
	double sy1 = stOption.rectData[nROI].PatternStartPos.y;
	double sx2 = stOption.rectData[nROI].PatternEndPos.x;
	double sy2 = stOption.rectData[nROI].PatternEndPos.y;

	double dbDist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int iOffset = (int)(dbDist * dbValue);

	return iOffset;
}

int CWnd_ImageView::Overlay_StandardPeakData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI)
{
	if (stOption.rectData[nROI].i_Range_Max == stData.nValueResult[nROI])
		return 0;

	double dbValue = (double)(stOption.rectData[nROI].i_Threshold_Max - stOption.rectData[nROI].i_Range_Min)
		/ (double)(stOption.rectData[nROI].i_Range_Max - stOption.rectData[nROI].i_Range_Min);

	double sx1 = stOption.rectData[nROI].PatternStartPos.x;
	double sy1 = stOption.rectData[nROI].PatternStartPos.y;
	double sx2 = stOption.rectData[nROI].PatternEndPos.x;
	double sy2 = stOption.rectData[nROI].PatternEndPos.y;

	double dbDist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int iOffset = (int)(dbDist * dbValue);

	return iOffset;
}


//=============================================================================
// Method		: PicCenterPoint
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/12/8 - 15:05
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicCenterPoint(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;

	int SelR = 255, SelG = 255, SelB = 0;
	int iThickness = m_pstModelInfo->dwWidth / 300;
	float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	if (m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointOpt.iSelectROI > -1)
	{
		SelR = 0, SelG = 255, SelB = 0;
	}

	OnDisplayPIC(lpImage, PICMode_RECTANGLE,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointOpt.rtRoi.left,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointOpt.rtRoi.top,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointOpt.rtRoi.right,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointOpt.rtRoi.bottom,
		SelR, SelG, SelB, iThickness);

	if (m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueX < 0 || m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueY < 0)
		return;

	OnDisplayPIC(lpImage, PICMode_LINE,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueX - 10,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueY,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueX + 10,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueY,
		0, 255, 255, iThickness);

	OnDisplayPIC(lpImage, PICMode_LINE,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueX,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueY - 10,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueX,
		m_pstModelInfo->stCenterPoint[m_pstModelInfo->nTestCnt].stCenterPointResult.iPicValueY + 10,
		0, 255, 255, iThickness);
}

//=============================================================================
// Method		: PicRotate
// Access		: public  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/10/15 - 17:45
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicRotate(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;

	int SelR = 255, SelG = 255, SelB = 0;
	int iThickness = m_pstModelInfo->dwWidth / 300;
	float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	CString strText;

	for (UINT nIdx = 0; nIdx < ROI_RT_Max; nIdx++)
	{
		SelR = 255, SelG = 255, SelB = 0;

		strText.Format(_T("%s"), g_szRoiRotate[nIdx]);

		if (nIdx == m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.iSelectROI)
		{
			SelR = 0, SelG = 255, SelB = 0;
		}
		if (m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].bTestMode == TRUE)
		{
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stTestRegionOp[nIdx].rtRoi.left,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stTestRegionOp[nIdx].rtRoi.top,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stTestRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stTestRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness);

			int iTop = m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stTestRegionOp[nIdx].rtRoi.top - 7;

			if (iTop < 10)
				iTop = m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stTestRegionOp[nIdx].rtRoi.bottom + 15;

			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stTestRegionOp[nIdx].rtRoi.left,
				iTop,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stTestRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stTestRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness, fFontSize, strText);
		}
		else{
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nIdx].rtRoi.left,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nIdx].rtRoi.top,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness);

			int iTop = m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nIdx].rtRoi.top - 7;

			if (iTop < 10)
				iTop = m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nIdx].rtRoi.bottom + 15;

			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nIdx].rtRoi.left,
				iTop,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateOpt.stRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness, fFontSize, strText);
		}
	
	}

	if (m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].bTestMode == TRUE){
		for (UINT nIdx = 0; nIdx < ROI_RT_Max; nIdx++)
		{
			if (m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[nIdx].x < 0 || m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[nIdx].y < 0)
				return;
		}

		OnDisplayPIC(lpImage, PICMode_LINE,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_LT].x,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_LT].y,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_RT].x,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_RT].y,
			0, 255, 255, iThickness);

		OnDisplayPIC(lpImage, PICMode_LINE,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_LB].x,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_LB].y,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_RB].x,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_RB].y,
			0, 255, 255, iThickness);

		OnDisplayPIC(lpImage, PICMode_LINE,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_LT].x,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_LT].y,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_LB].x,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_LB].y,
			0, 255, 255, iThickness);

		OnDisplayPIC(lpImage, PICMode_LINE,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_RT].x,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_RT].y,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_RB].x,
			m_pstModelInfo->stRotate[m_pstModelInfo->nTestCnt].stRotateResult.ptCenter[ROI_RT_RB].y,
			0, 255, 255, iThickness);
	}
	
}

//=============================================================================
// Method		: PicSFR
// Access		: public  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/10/15 - 17:34
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicSFR(IplImage* lpImage)
{
	//if (NULL == m_pstModelInfo)
	//	return;

	//int SelR = 255, SelG = 255, SelB = 0;
	//int iThickness = m_pstModelInfo->dwWidth / 300;
	//float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	//CString strText;

	//double db_1F_Width = (double)m_pstModelInfo->dwWidth / 2;
	//double db_1F_Height = (double)m_pstModelInfo->dwHeight / 2;
	//double db_1Field = sqrt(pow(db_1F_Width, 2) + pow(db_1F_Height, 2));

	//if (m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.bField == TRUE)
	//{
	//	for (int i = 0; i < 10; i++)
	//	{
	//		int iLeft = db_1F_Width - (db_1Field / 10 * (i + 1));
	//		int iRight = db_1F_Width + (db_1Field / 10 * (i + 1));
	//		int iTop = db_1F_Height - (db_1Field / 10 * (i + 1));
	//		int iBottom = db_1F_Height + (db_1Field / 10 * (i + 1));

	//		OnDisplayPIC(lpImage, PICMode_CIRCLE, iLeft, iTop + 35, iRight, iBottom,
	//			255, 255, 255, iThickness, fFontSize, _T(""));
	//	}
	//}

	//for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	//{
	//	if (TRUE == m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].bEnable)
	//	{
	//		SelR = 255, SelG = 255, SelB = 0;

	//		if (m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.iSelectROI == nIdx)
	//		{
	//			SelR = 0, SelG = 255, SelB = 0;
	//		}
	//		else
	//		{
	//			if (m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFRResult.nEachResult[nIdx] == TER_Pass)
	//			{
	//				SelR = 0, SelG = 0, SelB = 255;
	//			}
	//			else
	//			{
	//				SelR = 255, SelG = 0, SelB = 0;
	//			}
	//		}

	//		OnDisplayPIC(lpImage, PICMode_RECTANGLE,
	//			m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.left,
	//			m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.top,
	//			m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.right,
	//			m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.bottom,
	//			SelR, SelG, SelB, iThickness);

	//		int iLeft = m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.left;
	//		int iRight = m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.right;
	//		int iTop = m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.top;
	//		int iBottom = m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.bottom;
	//		int iWidth = m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.Width();
	//		int iHeight = m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.Height();

	//		switch (m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].nFont)
	//		{
	//		case SFR_Mode_F_Left:
	//			iLeft -= 160;
	//			iTop = iTop + iWidth / 2;
	//			break;

	//		case SFR_Mode_F_Right:
	//			iLeft = iRight + 20;
	//			iTop = iTop + iWidth / 2;
	//			break;

	//		case SFR_Mode_F_Up:
	//			iLeft -= 30;
	//			iTop -= 60;
	//			break;

	//		case SFR_Mode_F_Down:
	//			iLeft -= 30;
	//			iTop = iBottom + 40;
	//			break;

	//		default:
	//			break;
	//		}

	//		strText.Format(_T("%02d : %.2f"), nIdx + 1, 
	//			m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFRResult.dbValue[nIdx]);

	//		OnDisplayPIC(lpImage, PICMode_TXT, iLeft, iTop, iRight, iBottom,
	//			SelR, SelG, SelB, iThickness, fFontSize, strText);


	//		int roiposX = m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.CenterPoint().x;
	//		int roiposY = m_pstModelInfo->stSFR[m_pstModelInfo->nTestCnt].stSFROpt.stRegionOp[nIdx].rtRoi.CenterPoint().y;

	//		double db_Field_Width = (double)roiposX - db_1F_Width;
	//		double db_Field_Height = (double)roiposY - db_1F_Height;
	//		double db_Field = sqrt(pow(db_Field_Width, 2) + pow(db_Field_Height, 2)) / db_1Field;

	//		strText.Format(_T("( %.2fF )"), db_Field);
	//		OnDisplayPIC(lpImage, PICMode_TXT, iLeft, iTop + 35, iRight, iBottom,
	//			SelR, SelG, SelB, iThickness, fFontSize, strText);
	//	}
	//}
}

//=============================================================================
// Method		: PicAngle
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2018/1/11 - 15:26
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicAngle(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;

	int SelR = 255, SelG = 255, SelB = 0;
	int iThickness = m_pstModelInfo->dwWidth / 300;
	float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	for (UINT nIdx = 0; nIdx < ROI_AL_Max; nIdx++)
	{
		SelR = 255, SelG = 255, SelB = 0;

		CString strText;
		strText.Format(_T("%s"), g_szRoiAngle[nIdx]);

		if (m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.iSelectROI == nIdx)
		{
			SelR = 0, SelG = 255, SelB = 0;
		}

		if (m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].bTestMode == TRUE)
		{
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stTestRegionOp[nIdx].rtRoi.left,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stTestRegionOp[nIdx].rtRoi.top,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stTestRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stTestRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness);

			int iTop = m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stTestRegionOp[nIdx].rtRoi.top - 7;

			if (iTop < 10)
				iTop = m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stTestRegionOp[nIdx].rtRoi.bottom + 15;

			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stTestRegionOp[nIdx].rtRoi.left,
				iTop,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stTestRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stTestRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness, fFontSize, strText);
		}
		else{
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nIdx].rtRoi.left,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nIdx].rtRoi.top,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness);

			int iTop = m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nIdx].rtRoi.top - 7;

			if (iTop < 10)
				iTop = m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nIdx].rtRoi.bottom + 15;

			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nIdx].rtRoi.left,
				iTop,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleOpt.stRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness, fFontSize, strText);
		}
		

	}

	CString strText;
	if (m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].bTestMode == TRUE)
	{
		for (UINT nIdx = ROI_AL_Left; nIdx < ROI_AL_Max; nIdx++)
		{
			if (m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleResult.ptCenter[nIdx].x < 0 || m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleResult.ptCenter[nIdx].y < 0)
				return;

			OnDisplayPIC(lpImage, PICMode_LINE,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleResult.ptCenter[ROI_AL_Center].x,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleResult.ptCenter[ROI_AL_Center].y,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleResult.ptCenter[nIdx].x,
				m_pstModelInfo->stAngle[m_pstModelInfo->nTestCnt].stAngleResult.ptCenter[nIdx].y,
				0, 255, 255, 3);
		}
	}
	
}

//=============================================================================
// Method		: PicBrightness
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/11/30 - 11:51
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicBrightness(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;

	int SelR = 255, SelG = 255, SelB = 0;
	int iThickness = m_pstModelInfo->dwWidth / 300;
	float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	CString strText;

	for (UINT nIdx = 0; nIdx < ROI_BR_Max; nIdx++)
	{
		SelR = 255, SelG = 255, SelB = 0;

		double dRetBrightness = m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessResult.dbValue[nIdx];

		strText.Format(_T("%s"), g_szRoiBrightness[nIdx]);

		if (m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.iSelectROI == nIdx)
		{
			SelR = 0, SelG = 255, SelB = 0;
		}

		if (m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessResult.iSelectROI == nIdx)
		{
			if (m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessResult.nEachResult[nIdx] == TER_Pass)
			{
				SelR = 0, SelG = 0, SelB = 255;
			}
			else
			{
				SelR = 255, SelG = 0, SelB = 0;
			}
		}

		OnDisplayPIC(lpImage, PICMode_RECTANGLE,
			m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nIdx].rtRoi.left,
			m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nIdx].rtRoi.top,
			m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nIdx].rtRoi.bottom,
			SelR, SelG, SelB, iThickness);

		int iTop = m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nIdx].rtRoi.top - 7;

		if (iTop < 10)
			iTop = m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nIdx].rtRoi.bottom + 15;

		OnDisplayPIC(lpImage, PICMode_TXT,
			m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nIdx].rtRoi.left,
			iTop,
			m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstModelInfo->stBrightness[m_pstModelInfo->nTestCnt].stBrightnessOpt.stRegionOp[nIdx].rtRoi.bottom,
			SelR, SelG, SelB, iThickness, fFontSize, strText);
	}
}

//=============================================================================
// Method		: PicParticle
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/11/30 - 11:51
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicParticle(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;

	int SelR = 255, SelG = 255, SelB = 0;
	int iThickness = m_pstModelInfo->dwWidth / 300;
	float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	for (UINT nIdx = 0; nIdx < Part_ROI_Max; nIdx++)
	{
		SelR = 255, SelG = 255, SelB = 0;

		CString strText;
		strText.Format(_T("%s"), g_szPaticleRegion[nIdx]);

		enPicMode	enMode = PICMode_RECTANGLE;

		CRect rtRect;

		// Line 1 : 위
		rtRect.left = 0;
		rtRect.top = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.iEdgeH;
		rtRect.right = lpImage->width;
		rtRect.bottom = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.iEdgeH;

		OnDisplayPIC(lpImage, enMode,
			rtRect.left,
			rtRect.top,
			rtRect.right,
			rtRect.bottom,
			255, 255, 0, iThickness);

		// Line 2 : 아래
		rtRect.left = 0;
		rtRect.top = lpImage->height - m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.iEdgeH;
		rtRect.right = lpImage->width;
		rtRect.bottom = lpImage->height - m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.iEdgeH;

		OnDisplayPIC(lpImage, enMode,
			rtRect.left,
			rtRect.top,
			rtRect.right,
			rtRect.bottom,
			255, 255, 0, iThickness);

		// Line 3 : 좌
		rtRect.left = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.iEdgeW;
		rtRect.top = 0;
		rtRect.right = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.iEdgeW;
		rtRect.bottom = lpImage->height;

		OnDisplayPIC(lpImage, enMode,
			rtRect.left,
			rtRect.top,
			rtRect.right,
			rtRect.bottom,
			255, 255, 0, iThickness);

		// Line 4 : 우
		rtRect.left = lpImage->width - m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.iEdgeW;
		rtRect.top = 0;
		rtRect.right = lpImage->width - m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.iEdgeW;
		rtRect.bottom = lpImage->height;

		OnDisplayPIC(lpImage, enMode,
			rtRect.left,
			rtRect.top,
			rtRect.right,
			rtRect.bottom,
			255, 255, 0, iThickness);

		if (m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.nFailCount > 0)
		{
			for (UINT nROI = 0; nROI < m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.nFailCount; nROI++)
			{
				if (m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.iSelectROI == nIdx)
				{
					SelR = 255, SelG = 0, SelB = 0;
				}

				OnDisplayPIC(lpImage, PICMode_RECTANGLE,
					m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].left,
					m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].top,
					m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].right,
					m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].bottom,
					255, 0, 0, iThickness);

				int iTop = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].top - 7;

				if (iTop < 10)
					iTop = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].bottom + 15;

				strText.Format(_T("%d"), nROI + 1);
				OnDisplayPIC(lpImage, PICMode_TXT,
					m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].left, iTop,
					m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].right,
					m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].bottom,
					255, 0, 0, iThickness, fFontSize, strText);
			}
		}
	}
	//if (NULL == m_pstModelInfo)
	//	return;

	//int SelR = 255, SelG = 255, SelB = 0;
	//int iThickness = m_pstModelInfo->dwWidth / 300;
	//float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	//for (UINT nIdx = 0; nIdx < Part_ROI_Max; nIdx++)
	//{
	//	SelR = 255, SelG = 255, SelB = 0;

	//	CString strText;
	//	strText.Format(_T("%s"), g_szPaticleRegion[nIdx]);

	//	enPicMode	enMode;

	//	if (TRUE == m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].bEllipse)
	//	{
	//		enMode = PICMode_CIRCLE;
	//	}
	//	else
	//	{
	//		enMode = PICMode_RECTANGLE;
	//	}

	//	OnDisplayPIC(lpImage, enMode,
	//		m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].rtRoi.left,
	//		m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].rtRoi.top,
	//		m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].rtRoi.right,
	//		m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].rtRoi.bottom,
	//		255, 255, 0, iThickness);

	//	int iTop = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].rtRoi.top - 7;

	//	if (iTop < 10)
	//		iTop = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].rtRoi.bottom + 15;

	//	OnDisplayPIC(lpImage, PICMode_TXT,
	//		m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].rtRoi.left, iTop,
	//		m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].rtRoi.right,
	//		m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleOpt.stRegionOp[nIdx].rtRoi.bottom,
	//		255, 255, 0, iThickness, fFontSize, strText);

	//	if (m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.nFailCount > 0)
	//	{
	//		for (UINT nROI = 0; nROI < m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.nFailCount; nROI++)
	//		{
	//			if (m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.iSelectROI == nIdx)
	//			{
	//				SelR = 255, SelG = 0, SelB = 0;
	//			}

	//			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
	//				m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].left,
	//				m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].top,
	//				m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].right,
	//				m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].bottom,
	//				255, 0, 0, iThickness);

	//			int iTop = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].top - 7;

	//			if (iTop < 10)
	//				iTop = m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].bottom + 15;

	//			strText.Format(_T("%d"), nROI + 1);
	//			OnDisplayPIC(lpImage, PICMode_TXT,
	//				m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].left, iTop,
	//				m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].right,
	//				m_pstModelInfo->stBlackSpot[m_pstModelInfo->nTestCnt].stParticleResult.rtFailRoi[nROI].bottom,
	//				255, 0, 0, iThickness, fFontSize, strText);
	//		}
	//	}
	//}
}


void CWnd_ImageView::PicDefectPixel(IplImage* lpImage)
{
	if (NULL == lpImage)
		return;

	int SelR = 255, SelG = 255, SelB = 0;
	int iThickness = m_pstModelInfo->dwWidth / 300;
	float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	CRect	rtRect;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	{
		if (m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.nFailCount > 0)
		{
			for (UINT nCnt = 0; nCnt < m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.nFailCount[nIdx]; nCnt++)
			{
				if (DetectCount > nCnt)
				{
					rtRect = m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt];
					OnDisplayPIC(lpImage, PICMode_RECTANGLE,
						m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].left,
						m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].top,
						m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].right,
						m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].bottom,
						255, 100, 0, iThickness);

					rtRect.left = m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].left + 5;

					if (m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].top < 100)
						rtRect.top = m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].top + 30;
					else
						rtRect.top = m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].top - 15;

					CString failCount;
					failCount.Format(_T("[%d]"), nCnt);

					OnDisplayPIC(lpImage, PICMode_TXT,
						m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].left,
						m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].top,
						m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].right,
						m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.rtFailROI[nIdx][nCnt].bottom,
						255, 100, 0, iThickness, fFontSize,
						//g_szDefectPixel_Type[m_pstModelInfo->stDefectPixel[m_pstModelInfo->nTestCnt].stDefectPixelData.nFailType[nIdx][nCnt]]
						failCount
						);
				}
			}
		}
	}
}



//=============================================================================
// Method		: PicCurrent
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2018/1/14 - 13:53
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicCurrent(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;

}

//=============================================================================
// Method		: PicColor
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2018/1/14 - 13:53
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicColor(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;

	int SelR = 255, SelG = 255, SelB = 0;
	int iThickness = m_pstModelInfo->dwWidth / 300;
	float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	CString strText;

	for (UINT nIdx = 0; nIdx < ROI_CL_Max; nIdx++)
	{
		SelR = 255, SelG = 255, SelB = 0;

		if (m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.iSelectROI == nIdx)
		{
			SelR = 0, SelG = 255, SelB = 0;
		}

		if (m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorResult.iSelectROI == nIdx)
		{
			if (m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorResult.nEachResult[nIdx] == TER_Pass)
			{
				SelR = 0, SelG = 0, SelB = 255;
			}
			else
			{
				SelR = 255, SelG = 0, SelB = 0;
			}
		}

		if (m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].bTestMode)
		{
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stTestRegionOp[nIdx].rtRoi.left,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stTestRegionOp[nIdx].rtRoi.top,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stTestRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stTestRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness);

			int iTop = m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stTestRegionOp[nIdx].rtRoi.top - 7;

			if (iTop < 10)
				iTop = m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stTestRegionOp[nIdx].rtRoi.bottom + 15;

			strText.Format(_T("%s"), g_szRoiColor[nIdx]);

			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stTestRegionOp[nIdx].rtRoi.left, iTop,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stTestRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stTestRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness, fFontSize, strText);
		}
		else{
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nIdx].rtRoi.left,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nIdx].rtRoi.top,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness);

			int iTop = m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nIdx].rtRoi.top - 7;

			if (iTop < 10)
				iTop = m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nIdx].rtRoi.bottom + 15;

			strText.Format(_T("%s"), g_szRoiColor[nIdx]);

			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nIdx].rtRoi.left, iTop,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stColor[m_pstModelInfo->nTestCnt].stColorOpt.stRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness, fFontSize, strText);
		}
		
	}
}

void CWnd_ImageView::PicFFT(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;
}


void CWnd_ImageView::PicEIAJ(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;
	if (NULL == lpImage)
		return;


	int SelR = 255, SelG = 255, SelB = 0;
	int		iLineSize = 2;
	double	dbFontSize = 0.5;

	CRect	rtROIRect;
	CString strText;
	CRect	rtRect;
	CString szText;
	CRect	rtRectText;


	ST_EIAJ_Op stOption = m_pstModelInfo->stEIAJ[0].stEIAJOp;
	ST_EIAJ_Data stData = m_pstModelInfo->stEIAJ[0].stEIAJData;




	for (int nIdx = 0; nIdx < ReOp_ItemNum; nIdx++)
	{
		rtROIRect = stOption.rectData[nIdx].RegionList;

		if (nIdx == ReOp_CenterBlack || nIdx == ReOp_CenterWhite)
		{
			if (stOption.rectData[nIdx].bUse == TRUE)
			{
				Overlay_Process(lpImage, OvrMode_RECTANGLE, rtROIRect, RGB(255, 255, 255), iLineSize);
			}
		}
		else // index = 2
		{
			if (stOption.rectData[nIdx].bUse == TRUE)
			{
				if (m_bTestmode == FALSE)
				{
					Overlay_Process(lpImage, OvrMode_RECTANGLE, rtROIRect, RGB(255, 255, 0), iLineSize);

					strText.Format(_T("%s"), g_szResolutionRegion[nIdx]);

					if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Top_Bottom)
					{
						CRect rtRect;
						rtRect.left = rtROIRect.left + (rtROIRect.Width() * 2 / 3);
						rtRect.top = rtROIRect.top - 10;
						rtRect.right = 0;
						rtRect.bottom = 0;
						Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 255, 0), iLineSize, dbFontSize, strText);
					}
					else if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Bottom_Top)
					{
						CRect rtRect;
						rtRect.left = rtROIRect.left + (rtROIRect.Width() * 2 / 3);
						rtRect.top = rtROIRect.bottom + 20;
						rtRect.right = 0;
						rtRect.bottom = 0;
						Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 255, 0), iLineSize, dbFontSize, strText);
					}
					else if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
					{
						CRect rtRect;
						rtRect.left = rtROIRect.left + (rtROIRect.Width() * 1 / 5);
						rtRect.top = rtROIRect.top - 10;
						rtRect.right = 0;
						rtRect.bottom = 0;
						Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 255, 0), iLineSize, dbFontSize, strText);
					}
					else if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Right_Left)
					{
						CRect rtRect;
						rtRect.left = rtROIRect.right - (rtROIRect.Width() * 1 / 5);
						rtRect.top = rtROIRect.top - 10;
						rtRect.right = 0;
						rtRect.bottom = 0;
						Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 255, 0), iLineSize, dbFontSize, strText);
					}
				}
				else
				{
					if (stData.nEachResult[nIdx] == TER_Pass)
					{
						Overlay_Process(lpImage, OvrMode_RECTANGLE, rtROIRect, RGB(0, 0, 255), iLineSize);
					}
					else
					{
						Overlay_Process(lpImage, OvrMode_RECTANGLE, rtROIRect, RGB(255, 0, 0), iLineSize);
					}

					if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Bottom_Top
						|| stOption.rectData[nIdx].i_Mode == Resol_Mode_Top_Bottom)
					{
						int m_resultpeakvalue = Overlay_PeakData(stOption, stData, nIdx);
						int m_ConstValue = 0;
						int m_ResultValue = rtROIRect.top;


						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Top_Bottom)
						{
							m_ConstValue = rtROIRect.top + m_resultpeakvalue + stOption.rectData[nIdx].PatternStartPos.y;
						}
						else
						{
							m_ConstValue = rtROIRect.bottom - m_resultpeakvalue - (rtROIRect.Height() - stOption.rectData[nIdx].PatternStartPos.y);
						}

						int iOffset1 = rtROIRect.Height() - stOption.rectData[nIdx].PatternStartPos.y;
						int iOffset2 = stOption.rectData[nIdx].PatternEndPos.y;

						CRect rtRect1;
						rtRect1.left = rtROIRect.left;
						rtRect1.top = rtROIRect.bottom - iOffset1;
						rtRect1.right = rtROIRect.right;
						rtRect1.bottom = rtROIRect.bottom - iOffset1;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect1, RGB(0, 255, 0), iLineSize);

						CRect rtRect2;
						rtRect2.left = rtROIRect.left;
						rtRect2.top = rtROIRect.top + iOffset2;
						rtRect2.right = rtROIRect.right;
						rtRect2.bottom = rtROIRect.top + iOffset2;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect2, RGB(0, 255, 0), iLineSize);

						int iThresholdValue = 0;
						int iPeakThresholdValue = 0;
						int iThresholdPosition = Overlay_StandarData(stOption, stData, nIdx);

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Bottom_Top)
						{
							iThresholdValue = rtROIRect.bottom - iOffset1 - iThresholdPosition;
							iPeakThresholdValue = rtROIRect.bottom - iOffset1 - Overlay_StandardPeakData(stOption, stData, nIdx);
						}
						else
						{
							iThresholdValue = rtROIRect.top + stOption.rectData[nIdx].PatternStartPos.y + iThresholdPosition;
							iPeakThresholdValue = rtROIRect.top + stOption.rectData[nIdx].PatternStartPos.y + Overlay_StandardPeakData(stOption, stData, nIdx);
						}

						if (abs(rtROIRect.left - rtROIRect.right) != 0)
						{
							CRect rtRect1;
							rtRect1.left = rtROIRect.left;
							rtRect1.top = iThresholdValue;
							rtRect1.right = rtROIRect.right;
							rtRect1.bottom = iThresholdValue;
							Overlay_Process(lpImage, OvrMode_LINE, rtRect1, RGB(255, 0, 255), iLineSize);

							CRect rtRect2;
							rtRect2.left = rtROIRect.left;
							rtRect2.top = iPeakThresholdValue;
							rtRect2.right = rtROIRect.right;
							rtRect2.bottom = iPeakThresholdValue;
							Overlay_Process(lpImage, OvrMode_LINE, rtRect2, RGB(255, 0, 255), iLineSize);
						}


						CRect rtRect3;
						rtRect3.left = rtROIRect.left;
						rtRect3.top = m_ConstValue;
						rtRect3.right = rtROIRect.right;
						rtRect3.bottom = m_ConstValue;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect3, RGB(255, 255, 0), iLineSize);

						strText.Format(_T("%s [%d]"), g_szResolutionRegion[nIdx], stData.nValueResult[nIdx]);

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Top_Bottom)
						{
							CRect rtRect;
							rtRect.left = rtROIRect.left + 50;
							rtRect.top = rtROIRect.top + 15;
							rtRect.right = 0;
							rtRect.bottom = 0;
							if (stData.nEachResult[nIdx] == TER_Pass)
							{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 0, 255), iLineSize, dbFontSize, strText);
							}
							else{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, strText);
							}
						}
						else
						{
							int nGapX;
							if (strText.GetLength() > 7)
								nGapX = 85;
							else
								nGapX = 65;

							CRect rtRect;
							rtRect.left = rtROIRect.left - nGapX;
							rtRect.top = rtROIRect.bottom - 5;
							rtRect.right = 0;
							rtRect.bottom = 0;
							if (stData.nEachResult[nIdx] == TER_Pass)
							{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 0, 255), iLineSize, dbFontSize, strText);
							}
							else{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, strText);
							}
						}
					}
					else if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Right_Left
						|| stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
					{
						int m_resultpeakvalue = Overlay_PeakData(stOption, stData, nIdx);
						int m_ConstValue = 0;
						int m_ResultValue = rtROIRect.left;

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
						{
							m_ConstValue = rtROIRect.left + stOption.rectData[nIdx].PatternStartPos.x + m_resultpeakvalue;
						}
						else
						{
							m_ConstValue = rtROIRect.right - (rtROIRect.Width() - stOption.rectData[nIdx].PatternStartPos.x) - m_resultpeakvalue;
						}

						int iOffset1 = rtROIRect.Width() - stOption.rectData[nIdx].PatternEndPos.x;
						int iOffset2 = stOption.rectData[nIdx].PatternStartPos.x;

						CRect rtRect1;
						rtRect1.left = rtROIRect.left + iOffset2;
						rtRect1.top = rtROIRect.top;
						rtRect1.right = rtROIRect.left + iOffset2;
						rtRect1.bottom = rtROIRect.bottom;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect1, RGB(0, 255, 0), iLineSize);

						CRect rtRect2;
						rtRect2.left = rtROIRect.right - iOffset1;
						rtRect2.top = rtROIRect.top;
						rtRect2.right = rtROIRect.right - iOffset1;
						rtRect2.bottom = rtROIRect.bottom;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect2, RGB(0, 255, 0), iLineSize);

						int iThresholdPosition = Overlay_StandarData(stOption, stData, nIdx);
						int iPeakThresholdValue = Overlay_StandardPeakData(stOption, stData, nIdx);
						int iThresholdValue = 0;
						int iThresholdValue_Peak = 0;

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
						{
							iThresholdValue = rtROIRect.left + stOption.rectData[nIdx].PatternStartPos.x + iThresholdPosition;
							iThresholdValue_Peak = rtROIRect.left + stOption.rectData[nIdx].PatternStartPos.x + iPeakThresholdValue;

						}
						else
						{
							iThresholdValue = rtROIRect.right - (rtROIRect.Width() - stOption.rectData[nIdx].PatternStartPos.x) - iThresholdPosition;
							iThresholdValue_Peak = rtROIRect.right - (rtROIRect.Width() - stOption.rectData[nIdx].PatternStartPos.x) - iPeakThresholdValue;
						}

						if (abs(rtROIRect.top - rtROIRect.bottom) != 0)
						{
							CRect rtRect1;
							rtRect1.left = iThresholdValue;
							rtRect1.top = rtROIRect.top;
							rtRect1.right = iThresholdValue;
							rtRect1.bottom = rtROIRect.bottom;
							Overlay_Process(lpImage, OvrMode_LINE, rtRect1, RGB(255, 0, 255), iLineSize);

							CRect rtRect2;
							rtRect2.left = iThresholdValue_Peak;
							rtRect2.top = rtROIRect.top;
							rtRect2.right = iThresholdValue_Peak;
							rtRect2.bottom = rtROIRect.bottom;
							Overlay_Process(lpImage, OvrMode_LINE, rtRect2, RGB(255, 0, 255), iLineSize);

						}

						CRect rtRect3;
						rtRect3.left = m_ConstValue;
						rtRect3.top = rtROIRect.top;
						rtRect3.right = m_ConstValue;
						rtRect3.bottom = rtROIRect.bottom;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect3, RGB(255, 255, 0), iLineSize);

						strText.Format(_T("%s [%d]"), g_szResolutionRegion[nIdx], stData.nValueResult[nIdx]);

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
						{
							CRect rtRect;
							rtRect.left = rtROIRect.left;
							rtRect.top = rtROIRect.top - 10;
							rtRect.right = 0;
							rtRect.bottom = 0;
							if (stData.nEachResult[nIdx] == TER_Pass)
							{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 0, 255), iLineSize, dbFontSize, strText);
							}
							else{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, strText);

							}
						}
						else
						{
							CRect rtRect;
							rtRect.left = rtROIRect.left;
							rtRect.top = rtROIRect.top - 10;
							rtRect.right = 0;
							rtRect.bottom = 0;
							if (stData.nEachResult[nIdx] == TER_Pass)
							{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 0, 255), iLineSize, dbFontSize, strText);
							}
							else{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, strText);
							}
						}
					}
				}
			}
		}
	}
}

void CWnd_ImageView::PicReverse(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;

	int SelR = 255, SelG = 255, SelB = 0;
	int iThickness = m_pstModelInfo->dwWidth / 300;
	float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;

	CString strText;

	for (UINT nIdx = 0; nIdx < ROI_Rv_Max; nIdx++)
	{
		SelR = 255, SelG = 255, SelB = 0;

		if (m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.iSelectROI == nIdx)
		{
			SelR = 0, SelG = 255, SelB = 0;
		}

		//if (m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseResult.iSelectROI == nIdx)
		{
			if (m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseResult.nEachResult[nIdx] == TER_Pass)
			{
				SelR = 0, SelG = 0, SelB = 255;
			}
			else if (m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseResult.nEachResult[nIdx] == TER_Fail)
			{
				SelR = 255, SelG = 0, SelB = 0;
			}
		}

		if (m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].bTestMode == TRUE)
		{
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stTestRegionOp[nIdx].rtRoi.left,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stTestRegionOp[nIdx].rtRoi.top,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stTestRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stTestRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness);

			int iTop = m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stTestRegionOp[nIdx].rtRoi.top - 7;

			if (iTop < 10)
				iTop = m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stTestRegionOp[nIdx].rtRoi.bottom + 15;

			strText.Format(_T("%s"), g_szRoiReverse[nIdx]);

			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stTestRegionOp[nIdx].rtRoi.left, iTop,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stTestRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stTestRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness, fFontSize, strText);
		}
		else{
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nIdx].rtRoi.left,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nIdx].rtRoi.top,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness);

			int iTop = m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nIdx].rtRoi.top - 7;

			if (iTop < 10)
				iTop = m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nIdx].rtRoi.bottom + 15;

			strText.Format(_T("%s"), g_szRoiReverse[nIdx]);

			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nIdx].rtRoi.left, iTop,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstModelInfo->stReverse[m_pstModelInfo->nTestCnt].stReverseOpt.stRegionOp[nIdx].rtRoi.bottom,
				SelR, SelG, SelB, iThickness, fFontSize, strText);
		}

	}
}



void CWnd_ImageView::PicIRFilter(IplImage* lpImage)
{
	if (NULL == m_pstModelInfo)
		return;

	CString strText;

	int iThickness = m_pstModelInfo->dwWidth / 300;
	float fFontSize = m_pstModelInfo->dwWidth / 1000.0f;
	int iPosX = m_pstModelInfo->dwWidth / 2 - 130;
	int iPosY = m_pstModelInfo->dwHeight - (m_pstModelInfo->dwHeight / 10);

	strText.Format(_T("Red Gain : %.f , Env Val : %.f"), m_pstModelInfo->stIRFilter[m_pstModelInfo->nTestCnt].stIRFilterResult.dValue, m_pstModelInfo->stIRFilter[m_pstModelInfo->nTestCnt].stIRFilterResult.dValue_Env);

	//iThickness = 10;
	//fFontSize = 2.5;

	if (m_pstModelInfo->stIRFilter[m_pstModelInfo->nTestCnt].stIRFilterResult.nResult == TRUE)
	{
		OnDisplayPIC(lpImage, PICMode_TXT,
			iPosX, iPosY,
			iPosX, iPosY,
			0, 0, 255, iThickness, fFontSize, strText);
	}
	else
	{
		OnDisplayPIC(lpImage, PICMode_TXT,
			iPosX, iPosY,
			iPosX, iPosY,
			255, 0, 0, iThickness, fFontSize, strText);
	}
}

void CWnd_ImageView::PicPatternNoise(IplImage* lpImage)
{
//	if (NULL == m_pstModelInfo)
//		return;
//
//	int SelR = 255, SelG = 255, SelB = 0;
//	int iThickness = m_pstModelInfo->dwWidth / 300;
//	float fFontSize = m_pstModelInfo->dwWidth / 2000.0f;
//
//	CString strText;
//
//	for (UINT nIdx = 0; nIdx < ROI_PN_MaxEnum; nIdx++)
//	{
//		SelR = 255, SelG = 255, SelB = 0;
//
//		if (m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.iSelectROI == nIdx)
//		{
//			SelR = 0, SelG = 255, SelB = 0;
//		}
//
//		//if (m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseResult.iSelectROI == nIdx)
//		{
//			if (m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseResult.nEachResult[nIdx] == TER_Pass)
//			{
//				SelR = 0, SelG = 0, SelB = 255;
//			}
//			else if (m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseResult.nEachResult[nIdx] == TER_Fail)
//			{
//				SelR = 255, SelG = 0, SelB = 0;
//			}
//		}
//
//		OnDisplayPIC(lpImage, PICMode_RECTANGLE,
//			m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.left,
//			m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.top,
//			m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.right,
//			m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.bottom,
//			SelR, SelG, SelB, iThickness);
//
//		int iTop = m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.top - 7;
//
//		if (iTop < 10)
//			iTop = m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.bottom + 15;
//
//// 		if (nIdx == ROI_PN_S5)
//// 		{
//// 			strText.Format(_T("%0.2f"), m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseResult.dValue_dB[nIdx]);
//// 			OnDisplayPIC(lpImage, PICMode_TXT,
//// 				m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.left + 85,
//// 				iTop,
//// 				m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.right,
//// 				m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.bottom,
//// 				SelR, SelG, SelB, iThickness, fFontSize, strText);
//// 
//// 		}
//// 		else
//		{
//			strText.Format(_T("%0.2f"), m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseResult.dValue_dB[nIdx]);
//			OnDisplayPIC(lpImage, PICMode_TXT,
//				m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.left + 30,
//				iTop,
//				m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.right,
//				m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.bottom,
//				SelR, SelG, SelB, iThickness, fFontSize, strText);
//		}
//
//		strText.Format(_T("%s"), g_szRegionPatternNoise[nIdx]);
//
//		OnDisplayPIC(lpImage, PICMode_TXT,
//			m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.left, iTop,
//			m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.right,
//			m_pstModelInfo->stPatternNoise[m_pstModelInfo->nTestCnt].stPatternNoiseOpt.stRegionOp[nIdx].rtRoi.bottom,
//			SelR, SelG, SelB, iThickness, fFontSize, strText);
//	}
}

//=============================================================================
// Method		: OnUpdataROI
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * testImage
// Parameter	: enPicMode enMode
// Parameter	: int iLeft
// Parameter	: int iTop
// Parameter	: int iRight
// Parameter	: int iBottom
// Parameter	: int iRed
// Parameter	: int iGreen
// Parameter	: int iBlue
// Parameter	: int iTickness
// Parameter	: float fFontSize
// Parameter	: CString strText
// Qualifier	:
// Last Update	: 2017/10/15 - 17:29
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnDisplayPIC(IplImage* testImage, UINT nMode, int iLeft, int iTop, int iRight, int iBottom, int iRed, int iGreen, int iBlue, int iTickness, float fFontSize/* = 1*/, CString strText /*= _T("")*/)
{

// 	int nW = iRight - iLeft;
// 	int nH = iBottom - iTop;
// 
// 	if (nW < 1 || nH < 1)
// 	{
// 		return;
// 	}
	//if (iLeft < 1 || iTop < 1)
	//{
	//	return;
	//}

	CvPoint nStartPt;
	nStartPt.x = iLeft;
	nStartPt.y = iTop;

	CvPoint nEndPt;
	nEndPt.x = iRight;
	nEndPt.y = iBottom;

	int iPosX = iLeft + ((iRight - iLeft) / 2);
	int iPosY = iTop + ((iBottom - iTop) / 2);

	CvPoint nCenterPt;
	nCenterPt.x = iPosX;
	nCenterPt.y = iPosY;

	switch (nMode)
	{
	case PICMode_LINE:
		cvLine(testImage, nStartPt, nEndPt, CV_RGB(iRed, iGreen, iBlue), iTickness);
		break;
	case PICMode_RECTANGLE:
		cvRectangle(testImage, nStartPt, nEndPt, CV_RGB(iRed, iGreen, iBlue), iTickness);
		break;
	case PICMode_CIRCLE:
		cvCircle(testImage, nCenterPt, abs(iRight - iLeft) / 2, CV_RGB(iRed, iGreen, iBlue), iTickness);
		break;
	case PICMode_TXT:
	{
		CvFont* font = new CvFont;
		cvInitFont(font, CV_FONT_VECTOR0, fFontSize, fFontSize, 0, 1);
		cvPutText(testImage, CT2A(strText), nStartPt, font, CV_RGB(iRed, iGreen, iBlue)); // cvPoint 위치에 설정 색상으로 글씨 넣기

		delete font; // 해제
	}
	break;
	default:
		break;
	}
}
void CWnd_ImageView::Overlay_Process(__inout IplImage* lpImage, __in enOverlayMode enMode, __in CRect rtROI, __in COLORREF clrLineColor /*= RGB(255, 200, 0)*/, __in int iLineSize /*= 1*/, __in double dbFontSize /*= 1.0*/, __in CString szText /*= _T("")*/)
{
	CvPoint nStartPt;
	nStartPt.x = rtROI.left;
	nStartPt.y = rtROI.top;

	CvPoint nEndPt;
	nEndPt.x = rtROI.right;
	nEndPt.y = rtROI.bottom;

	CvPoint nCenterPt;
	nCenterPt.x = nStartPt.x + ((nEndPt.x - nStartPt.x) / 2);
	nCenterPt.y = nStartPt.y + ((nEndPt.y - nStartPt.y) / 2);

	CvSize Ellipse_Axis;
	Ellipse_Axis.width = (nEndPt.x - nStartPt.x) / 2;
	Ellipse_Axis.height = (nEndPt.y - nStartPt.y) / 2;


	switch (enMode)
	{
	case OvrMode_LINE:
		cvLine(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_RECTANGLE:
		cvRectangle(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_CIRCLE:
		//cvCircle(lpImage, nStartPt, abs(nEndPt.x - nStartPt.x) / 2, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		cvEllipse(lpImage, nCenterPt, Ellipse_Axis, 0, 0, 360, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_TXT:
	{
						CvFont* cvFont = new CvFont;
						cvInitFont(cvFont, CV_FONT_VECTOR0, dbFontSize, dbFontSize, 0, 1);

						//  [1/10/2019 ysJang] Center Point Pic 2줄로
						int nFind = szText.Find(_T("OFFSET"), 5);
						if (nFind > 0)
						{
							int nLength = szText.GetLength();
							CString szTmpText[2] = { _T(""), };
							szTmpText[0] = szText.Left(nFind);
							cvPutText(lpImage, CT2A(szTmpText[0]), nStartPt, cvFont, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)));

							nStartPt.y += 20;
							szTmpText[1] = szText.Right(nLength - nFind);
							cvPutText(lpImage, CT2A(szTmpText[1]), nStartPt, cvFont, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)));
						}
						else
							cvPutText(lpImage, CT2A(szText), nStartPt, cvFont, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor))); // cvPoint 위치에 설정 색상으로 글씨 넣기

						delete cvFont; // 해제
	}
		break;
	default:
		break;
	}
}

