﻿#ifndef List_SFROp_h__
#define List_SFROp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_SFROp
{
	SfOp_Object = 0,
	SfOp_PosX,
	SfOp_PosY,
	SfOp_Width,
	SfOp_Height,
	SfOp_MinSpc,
	SfOp_MaxSpc,
	SfOp_Offset,
	SfOp_Linepare,
	SfOp_Font,
	SfOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_SFROp[] =
{
	_T(""),
	_T("CX"),
	_T("CY"),
	_T("W"),
	_T("H"),
	_T("Min"),
	_T("Max"),
	_T("Offset"),
	_T("LP"),
	_T("Font"),
	NULL
};

typedef enum enListItemNum_SFROp
{
	SfOp_ItemNum = ROI_SFR_Max,
};

static LPCTSTR	g_lpszItem_SFROp[] =
{
	NULL
};

const int	iListAglin_SFROp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_SFROp[] =
{
	42,
	57,
	57,
	57,
	57,
	60,
	60,
	60,
	63,
	63,
};

// List_SFRInfo

class CList_SFROp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_SFROp)

public:
	CList_SFROp();
	virtual ~CList_SFROp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_SFR(ST_LT_TI_SFR* pstSFR)
	{
		if (pstSFR == NULL)
			return;

		m_pstSFR = pstSFR;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth  = nWidth;
		m_nHeight = nHeight;
	};


protected:

	ST_LT_TI_SFR*  m_pstSFR;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Font;

	UINT		m_nEditCol;
	UINT		m_nEditRow;

	UINT		m_nWidth;
	UINT		m_nHeight;

	BOOL		UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL		UpdateCelldbData	(UINT nRow, UINT nCol, double dbValue);

public:

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusEdit();
	afx_msg void	OnEnKillFocusESfrFontCombo();
	afx_msg void	OnEnSelectESfrFontCombo();
};

#endif // List_SFRInfo_h__
