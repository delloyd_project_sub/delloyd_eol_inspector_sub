﻿//*****************************************************************************
// Filename	: Wnd_DeviceView.h
// Created	: 2016/05/29
// Modified	: 2016/09/22
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_DeviceView_h__
#define Wnd_DeviceView_h__

#pragma once

#include "Def_TestDevice.h"
#include "VGStatic.h"
#include "Wnd_BaseView.h"
#include "Wnd_IOTable.h"

//=============================================================================
// CWnd_DeviceView
//=============================================================================
class CWnd_DeviceView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_DeviceView)

public:
	CWnd_DeviceView();
	virtual ~CWnd_DeviceView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	
	CWnd_IOTable	m_Wnd_IOTable;
	ST_Device*		m_pDevice;

public:
	
	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pDevice = pstDevice;
		m_Wnd_IOTable.SetPtr_Device(&m_pDevice->DigitalIOCtrl);
		m_Wnd_IOTable.Set_DIO_Name(g_lpszDI_bit_Desc, g_lpszDO_bit_Desc);
	};

	// DI Bit 상태 UI에 표시
	void	Set_IO_DI_OffsetData(__in BYTE byBitOffset, __in BOOL bOnOff);
	void	Set_IO_DI_Data(__in LPBYTE lpbyDIData, __in UINT nCount);

	// DO Bit 상태 UI에 표시
	void	Set_IO_DO_OffsetData(__in BYTE byBitOffset, __in BOOL bOnOff);
	void	Set_IO_DO_Data(__in LPBYTE lpbyDOData, __in UINT nCount);
};

#endif // Wnd_DeviceView_h__


