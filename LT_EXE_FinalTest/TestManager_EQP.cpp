﻿//*****************************************************************************
// Filename	: 	TestManager_EQP.cpp
// Created	:	2016/5/9 - 13:32
// Modified	:	2016/08/10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP.h"
#include "CommonFunction.h"
#include "File_Model.h"
#include "File_Report.h"
#include "Dlg_ChkPassword.h"
#include "Dlg_StartUpCheck.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

CTestManager_EQP::CTestManager_EQP()
{
	// 쓰레드 관련
	m_hThrTest_All		= NULL;
	//m_hThrAutoFocusing	= NULL;

	for (UINT nCnt = 0; nCnt < MAX_OPERATION_THREAD; nCnt++)
		m_hThrTest_Unit[nCnt] = NULL;

	for (UINT nCnt = 0; nCnt < Indicator_Max; nCnt++)
		m_bFlag_Indicator[nCnt] = TRUE;

	for (UINT nIdx = 0; nIdx < Indicator_Max; nIdx++)
		m_fValue[nIdx] = 0.00;

	for (UINT nIdx = 0; nIdx < DI_NotUseBit_Max; nIdx++)
		m_bFlag_Butten[nIdx] = TRUE;

	for (UINT nIdx = 0; nIdx < TIID_MaxEnum; nIdx++)
		m_bFlag_TestItem[nIdx] = FALSE;

	m_bFlag_ReadyTest		= FALSE;
	m_bFlag_TestProcess		= FALSE;
	m_dwTimeCheck			= 0;
	m_bFlag_UseStop			= FALSE;
	m_bFlag_loopTest		= FALSE;
	m_bTeststop				= FALSE;
	m_bFlag_MasterSet		= FALSE;
	m_bMovingMotor = FALSE;
	m_TestImage = NULL;
	OnInitialize();
}

CTestManager_EQP::~CTestManager_EQP()
{
	TRACE(_T("<<< Start ~CTestManager_EQP >>> \n"));
	m_ImageTest.DeleteMemory(&m_stInspInfo.ModelInfo.stEIAJ[0]);
	if (m_TestImage != NULL)
	{
		cvReleaseImage(&m_TestImage);
		m_TestImage = NULL;
	}
	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP >>> \n"));
}


//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnLoadOption()
{
	BOOL bReturn = __super::OnLoadOption();

	m_stInspInfo.bUseMasterCheck = m_stOption.Inspector.bUseMasterCheck;
	m_stInspInfo.bUseBarcode = m_stOption.Inspector.bUseBarcode;
	m_stInspInfo.bUsePrinter	= m_stOption.Inspector.bUsePrinter;
	m_stInspInfo.bUseArea		= m_stOption.Inspector.bUseAreaSen_Err;
	m_stInspInfo.bUseDoor		= m_stOption.Inspector.bUseDoorOpen_Err;
	m_stInspInfo.szBarcodeRef	= m_stOption.Inspector.szBarcodeRef;
	m_stInspInfo.nImageSaveType = m_stOption.Inspector.nImageSaveType;
	m_stInspInfo.szEqpCode = m_stOption.Inspector.EqpCode;
	m_stInspInfo.bUseFailBox = m_stOption.Inspector.bUseFailbox;

	if (!m_stOption.Inspector.szPath_Model.IsEmpty())
		m_stInspInfo.Path.szModel = m_stOption.Inspector.szPath_Model + _T("\\");

	if (!m_stOption.Inspector.szPath_Log.IsEmpty())
		m_stInspInfo.Path.szLog = m_stOption.Inspector.szPath_Log + _T("\\");

	if (!m_stOption.Inspector.szPath_Report.IsEmpty())
		m_stInspInfo.Path.szReport = m_stOption.Inspector.szPath_Report + _T("\\");
	
	if (!m_stOption.Inspector.szPath_Image.IsEmpty())
		m_stInspInfo.Path.szImage = m_stOption.Inspector.szPath_Image + _T("\\");

	if (!m_stOption.Inspector.szPath_I2CFile.IsEmpty())
		m_stInspInfo.Path.szI2C = m_stOption.Inspector.szPath_I2CFile + _T("\\");

	if (!m_stOption.Inspector.szPath_WavFile.IsEmpty())
		m_stInspInfo.Path.szWav = m_stOption.Inspector.szPath_WavFile + _T("\\");

// 	if (!m_stOption.Inspector.szPath_BarcodeFile.IsEmpty())
// 		m_stInspInfo.Path.szBarcodeInfo = m_stOption.Inspector.szPath_BarcodeFile + _T("\\");

	if (!m_stOption.Inspector.szPath_OperatorFile.IsEmpty())
		m_stInspInfo.Path.szOperatorInfo = m_stOption.Inspector.szPath_OperatorFile + _T("\\");

	if (!m_stOption.Inspector.szPath_Pogo.IsEmpty())
		m_stInspInfo.Path.szPogo = m_stOption.Inspector.szPath_Pogo + _T("\\");

	if (!m_stOption.Inspector.szPath_Motor.IsEmpty())
	{
		m_stInspInfo.Path.szMotor = m_stOption.Inspector.szPath_Motor + _T("\\");
		m_stInspInfo.Path.szMaintenance = m_stOption.Inspector.szPath_Motor + _T("\\");
	}

	if (!m_stOption.Inspector.szPath_PrnFile.IsEmpty())
		m_stInspInfo.Path.szPrnFile = m_stOption.Inspector.szPath_PrnFile + _T("\\");

	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/2/15 - 16:11
// Desc.		:
//=============================================================================
void CTestManager_EQP::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	__super::InitDevicez(hWndOwner);
	m_Device.MotionSequence.SetLogMsgID(hWndOwner, WM_LOGMSG);
//	m_TIProcessing.SetLogMsgID(hWndOwner, WM_LOGMSG);
}

//=============================================================================
// Method		: OnInitDigitalIOSignal
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/13 - 11:59
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitDigitalIOSignal()
{

}

//=============================================================================
// Method		: OnFunc_IO_EMO
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/28 - 10:54
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_EMO()
{
	m_bFlag_ReadyTest = FALSE;
	m_stInspInfo.CamInfo.nJudgment = TR_EquipAlarm;

	StopProcess_All();
	
	// Error Dialog 팝업
	OnAddErrorInfo(Err_EMO);

	// 프로그램 강제 종료..
}

//=============================================================================
// Method		: OnFunc_IO_ErrorReset
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/31 - 0:26
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_ErrorReset()
{
	OnInitDigitalIOSignal();
}

//=============================================================================
// Method		: OnFunc_IO_Init
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/31 - 0:27
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_Init()
{
	if (FALSE == IsTesting())
	{
		OnShowSplashScreen(TRUE, _T("모터 원점 잡기 수행 중입니다."));

		// LED, 경광등 초기화
		OnInitDigitalIOSignal();

		OnShowSplashScreen(FALSE);
	}
	else
	{
		MessageView(_T("Inspection is Running. Retry after the work is finished."));
	}
}

//=============================================================================
// Method		: OnFunc_IO_Door
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/15 - 13:36
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_Door()
{
	if (FALSE == m_stInspInfo.bUseDoor)
		return;

	m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
	m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLamp_Buzzer, ON);

	m_stInspInfo.CamInfo.nJudgment = TR_EquipAlarm;

	StopProcess_All();
	SetAjin_Motor_EStop_All();

	OnAddErrorInfo(Err_DoorSensor);

	// 초기 위치 복귀
	//OnFunc_Motion_Initial();
}


void CTestManager_EQP::OnFunc_IO_Safety()
{
	if (FALSE == m_stInspInfo.bUseArea || m_bMovingMotor == FALSE)
		return;

	m_bFlag_TestProcess = FALSE;

	m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
	m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLamp_Buzzer, ON);

	m_stInspInfo.CamInfo.nJudgment = TR_EquipAlarm;

	SetAjin_Motor_EStop_All();
	StopProcess_All();

	OnAddErrorInfo(Err_AreaSensor);

	// 초기 위치 복귀
	//OnFunc_Motion_Initial();
}

void CTestManager_EQP::OnFunc_IO_AirCheck()
{
// 	if (FALSE == m_stInspInfo.bUseArea)
// 		return;

	m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
	m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLamp_Buzzer, ON);

	m_stInspInfo.CamInfo.nJudgment = TR_EquipAlarm;

	StopProcess_All();
	SetAjin_Motor_EStop_All();

	OnAddErrorInfo(Err_MainPress);

	// 초기 위치 복귀
	//OnFunc_Motion_Initial();
}

//=============================================================================
// Method		: OnFunc_IO_MainPower
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/6 - 16:54
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_MainPower()
{
	m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
	m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLamp_Buzzer, ON);

	m_stInspInfo.CamInfo.nJudgment = TR_EquipAlarm;

	StopProcess_All();

	OnAddErrorInfo(Err_MainPower);

	// 프로그램 강제 종료..
}

//=============================================================================
// Method		: OnFunc_IO_Stop
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/12/14 - 11:46
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_Stop()
{
	m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
	m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLamp_Buzzer, ON);

	m_stInspInfo.CamInfo.nJudgment = TR_UserStop;

	StopProcess_All();

	OnAddErrorInfo(Err_UserStop);

	// 초기 위치 복귀
	//OnFunc_Motion_Initial();
}


void CTestManager_EQP::OnFunc_Motion_Initial()
{
	/*if (Motion_StageY_BlemishZone_Out() == RCA_OK)
	{
		Motion_StageY_Initial();
	}*/

	LRESULT lReturn = RC_OK;

	if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleOut) == TRUE)
	{
		lReturn = m_Device.MotionSequence.OnAction_Load();
		if (lReturn != RC_OK)
		{
			m_bMovingMotor = FALSE;
			return;
		}
	}
	else if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) > m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
	{
		//이물이 In 상태이고 이물 검사가 끝난 상태 일때
		lReturn = m_Device.MotionSequence.OnAction_Par_Load();
		if (lReturn != RC_OK)
		{
			m_bMovingMotor = FALSE;
			return;
		}
		lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
		if (lReturn != RC_OK)
		{
			m_bMovingMotor = FALSE;
			return ;
		}
		lReturn = m_Device.MotionSequence.OnAction_Load();
		if (lReturn != RC_OK)
		{
			m_bMovingMotor = FALSE;
			return ;
		}
	}
	else if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) < m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
	{
		//이물이 In 상태이지만, 차트 검사 위치에 있을 떄
		lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
		if (lReturn != RC_OK)
		{
			m_bMovingMotor = FALSE;
			return ;
		}
		lReturn = m_Device.MotionSequence.OnAction_Load();
		if (lReturn != RC_OK)
		{
			m_bMovingMotor = FALSE;
			return ;
		}
	}
}

//=============================================================================
// Method		: OnAddErrorInfo
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enErrorCode lErrorCode
// Qualifier	:
// Last Update	: 2016/10/31 - 1:05
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnAddErrorInfo(__in enEquipErrorCode lErrorCode)
{
	CDlg_ErrView Dlg_ErrView;
	Dlg_ErrView.ErrMessage(lErrorCode);
	Dlg_ErrView.DoModal();
}

//=============================================================================
// Method		: StartTest_MotionItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in double dbPos
// Qualifier	:
// Last Update	: 2018/1/14 - 10:38
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartTest_MotionItem(__in UINT nAxisItemID, __in double dbPos)
{
	LRESULT lReturn = RCA_OK;

// 	if (FALSE == CheckBarCodeInfo())
// 	{
// 		lReturn = RCA_NoBarcode;
// 	}

	lReturn = SetAjin_Motor((long)nAxisItemID, POS_ABS_MODE, dbPos);

	return lReturn;
}


LRESULT CTestManager_EQP::StartTest_IOItem(__in UINT nIO, __in UINT nSignal)
{
	LRESULT lReturn = RCA_OK;

// 	if (FALSE == CheckBarCodeInfo())
// 	{
// 		lReturn = RCA_NoBarcode;
// 	}

//	lReturn = SetAjin_IO(nIO, (enum_IO_SignalType)nSignal);

	switch (nIO)
	{
	default:
		break;
	}

	if (lReturn != RCA_OK)
	{
		lReturn = RCA_TransferErr;
	}

	return lReturn;
}


LRESULT CTestManager_EQP::StartTest_Utility(__in CString szMsg)
{
	LRESULT lReturn = RCA_OK;

	MessageView(szMsg);

	return lReturn;
}

//=============================================================================
// Method		: StartTest_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/1/13 - 22:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartTest_TestItem(__in UINT nTestItemID, __in UINT nTestIdx, __in UINT nStepIdx, __in int iRetryCount)
{
	LRESULT lReturn = TER_Init;

	m_stInspInfo.ModelInfo.nTestCnt = 0;

	switch (nTestItemID)
	{
	case TIID_TestInitialize:
	
		lReturn = OnTest_TestInitialize(nStepIdx);
		break;

	case TIID_TestFinalize:
	
		lReturn = OnTest_TestFinalize(nStepIdx);
		break;

	case TIID_Current:
		m_stInspInfo.ModelInfo.nPicItem = PIC_Current;
		lReturn = OnEachTest_Current(iRetryCount, nStepIdx, nTestIdx);
		break;

	//case TIID_OperationMode:
	//	m_stInspInfo.ModelInfo.nPicItem = PIC_OperMode;
	//	lReturn = OnEachTest_OperationMode(nStepIdx, nTestIdx);

	//	for (int i = 0; i < 50; i++)
	//		DoEvents(33);
	//	break;

	case TIID_CenterPoint:
		m_stInspInfo.ModelInfo.nPicItem = PIC_CenterPoint;

		switch (m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointOpt.nTestMode)
		{
		case CP_Mode_Measurement:
			lReturn = OnEachTest_CenterPoint(iRetryCount, nStepIdx, nTestIdx);
			break;
		case CP_Mode_Adjustment_Manual:
			lReturn = OnEachTest_CenterPoint_Manual(iRetryCount, nStepIdx, nTestIdx);
			break;
		case CP_Mode_Adjustment_Auto:
			lReturn = OnEachTest_CenterPoint_Adj(iRetryCount, nStepIdx, nTestIdx);
			break;
		default:
			break;
		}

		break;

	case TIID_Rotation:
		m_stInspInfo.ModelInfo.nPicItem = PIC_Rotate;
		lReturn = OnEachTest_Rotation(iRetryCount, nStepIdx, nTestIdx);
		break;

	//case TIID_SFR:
	//	
	//	m_stInspInfo.ModelInfo.nPicItem = PIC_SFR;
	//	lReturn = OnEachTest_SFR(iRetryCount, nStepIdx, nTestIdx);
	//	break;

	case TIID_EIAJ:
		m_stInspInfo.ModelInfo.nPicItem = PIC_EIAJ;
		lReturn = OnEachTest_EIAJ(iRetryCount, nStepIdx, nTestIdx);
		break;

	case TIID_FOV:
		m_stInspInfo.ModelInfo.nPicItem = PIC_Angle;
		lReturn = OnEachTest_Angle(iRetryCount, nStepIdx, nTestIdx);
		break;

	case TIID_Color:
		m_stInspInfo.ModelInfo.nPicItem = PIC_Color;
		lReturn = OnEachTest_Color(iRetryCount, nStepIdx, nTestIdx);
		break;

	case TIID_Reverse:
		m_stInspInfo.ModelInfo.nPicItem = PIC_Reverse;
		lReturn = OnEachTest_Reverse(iRetryCount, nStepIdx, nTestIdx);
		break;

	//case TIID_LEDTest:
	//	m_stInspInfo.ModelInfo.nPicItem = PIC_LEDTest;
	//	lReturn = OnEachTest_LEDTest(nStepIdx, nTestIdx);
	//	break;

	case TIID_ParticleManual:
		m_stInspInfo.ModelInfo.nPicItem = PIC_ParticleManual;
		lReturn = OnEachTest_ParticleManual(iRetryCount, nStepIdx, nTestIdx);
		break;

	case TIID_BlackSpot:
		m_stInspInfo.ModelInfo.nPicItem = PIC_BlackSpot;
		lReturn = OnEachTest_BlackSpot(iRetryCount, nStepIdx, nTestIdx);
		break;

	case TIID_DefectPixel:
		m_stInspInfo.ModelInfo.nPicItem = PIC_DefectPixel;
		lReturn = OnEachTest_DefectPixel(iRetryCount, nStepIdx, nTestIdx);
		break;

	case TIID_Brightness:
	
		m_stInspInfo.ModelInfo.nPicItem = PIC_Brightness;
		lReturn = OnEachTest_Brightness(iRetryCount, nStepIdx, nTestIdx);
		break;

	case TIID_IRFilter:
	
		m_stInspInfo.ModelInfo.nPicItem = PIC_IRFilter;
		lReturn = OnEachTest_IRFilter(iRetryCount, nStepIdx, nTestIdx);
		break;

	//case TIID_PatternNoise:
	//	for (int i = 0; i < 50; i++)
	//		DoEvents(33);
	//	m_stInspInfo.ModelInfo.nPicItem = PIC_PatternNoise;
	//	lReturn = OnEachTest_PatternNoise(iRetryCount, nStepIdx, nTestIdx);
	//	break;

	default:
		break;
	}
	// TempFolder에 저장한다.
	switch (nTestItemID)
	{
	// 저장.
	case TIID_Current:
	case TIID_CenterPoint:
	case TIID_Rotation:
	case TIID_EIAJ:
//	case TIID_SFR:
	case TIID_FOV:
	case TIID_Color:
	case TIID_Reverse:
	case TIID_BlackSpot:
	case TIID_ParticleManual:
	case TIID_Brightness:
	case TIID_IRFilter:
//	case TIID_PatternNoise:

		//if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
		{
			if (m_stInspInfo.nImageSaveType == ImageSaveType_ALL)
			{
				ReportImageSave((enLT_TestItem_ID)nTestItemID, nTestIdx, &m_stInspInfo.CamInfo.tmInputTime, lReturn);

				
			}
			else if (m_stInspInfo.nImageSaveType == ImageSaveType_NG)
			{
				if (lReturn == TER_Fail)
					ReportImageSave((enLT_TestItem_ID)nTestItemID, nTestIdx, &m_stInspInfo.CamInfo.tmInputTime, lReturn);

				
			}
		}


		break;

	// 저장 안함.
//	case TIID_LEDTest:
	case TIID_TestFinalize:
	case TIID_TestInitialize:
//	case TIID_OperationMode:
		break;

	default:
		break;
	}
	DoEvents(500);


	return lReturn;
}

LRESULT CTestManager_EQP::StartTest_TestItem_Motion(__in UINT nTestItemID)
{
	LRESULT lReturn = RCA_OK;
	
	m_bMovingMotor = TRUE;

	switch (nTestItemID)
	{
	case TIID_TestFinalize:

		m_bFlag_TestProcess = FALSE;

		// 이물광원이 OUT 상태일떄
		if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleOut) == TRUE)
		{
			lReturn = m_Device.MotionSequence.OnAction_Load();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
		}
		else if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) > m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
		{
			//이물이 In 상태이고 이물 검사가 끝난 상태 일때
			lReturn = m_Device.MotionSequence.OnAction_Par_Load();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
			lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
			lReturn = m_Device.MotionSequence.OnAction_Load();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
		}
		else if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) < m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
		{
			//이물이 In 상태이지만, 차트 검사 위치에 있을 떄
			lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
			lReturn = m_Device.MotionSequence.OnAction_Load();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
		}
		break;
	
	case TIID_Current:
		//!SH _190130: 아무것도 안함
		break;

	case TIID_TestInitialize:
	case TIID_CenterPoint:
	case TIID_Rotation:
	case TIID_FOV:
	case TIID_Color:
	case TIID_Reverse:

		if (m_Device.MotionManager.GetCurrentPos(AX_StageDistance) == m_stInspInfo.ModelInfo.dbTestDistance)
			break;

		if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) > m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
		{
			//이물이 In 상태이고 이물 검사가 끝난 상태 일때
			lReturn = m_Device.MotionSequence.OnAction_Load();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
			Sleep(500);
			lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
		}
		else if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) < m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
		{
			//이물이 In 상태이지만, 차트 검사 위치에 있을 떄
			lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
		}

		lReturn = m_Device.MotionSequence.OnAction_Inspect(m_stInspInfo.ModelInfo.dbTestAxisX, m_stInspInfo.ModelInfo.dbTestAxisY,m_stInspInfo.ModelInfo.dbTestDistance);
		if (lReturn != RC_OK)
		{
			m_bMovingMotor = FALSE;
			return lReturn;
		}

		m_bFlag_TestProcess = TRUE;

		break;

	case TIID_BlackSpot:
	case TIID_ParticleManual:
	case TIID_DefectPixel:
	case TIID_IRFilter:
	case TIID_Brightness:

		m_bFlag_TestProcess = TRUE;

		//!SH _190114: 이미 검사 위치면 Skip, x, y 축 고려 필요
		if (m_Device.MotionManager.GetCurrentPos(AX_StageDistance) == m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Par_Inspectoin_Dist])
			break;

		if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleOut) == TRUE)
		{
			lReturn = m_Device.MotionSequence.OnAction_Par_Load();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
		}
		else if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) > m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
		{
			//이물이 In 상태이고 이물 검사가 끝난 상태 일때
			lReturn = m_Device.MotionSequence.OnAction_Par_Load();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
			Sleep(500);
			lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
		}
		else if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) < m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
		{
			//이물이 In 상태이지만, 차트 검사 위치에 있을 떄
			lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
			Sleep(500);
			lReturn = m_Device.MotionSequence.OnAction_Par_Load();
			if (lReturn != RC_OK)
			{
				m_bMovingMotor = FALSE;
				return lReturn;
			}
		}

		lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_In();
		Sleep(500);
		if (lReturn != RC_OK)
		{
			return lReturn;
		}

		lReturn = m_Device.MotionSequence.OnAction_Par_Inspect();
		if (lReturn != RC_OK)
		{
			m_bMovingMotor = FALSE;
			return lReturn;
		}

		break;

	default:
		break;
	}
	m_bMovingMotor = FALSE;

	switch (nTestItemID)
	{
	case TIID_IRFilter:
		lReturn = m_Device.MotionSequence.OnAction_IR_LED_ON();
		//!SH _190130: 메세지만 전달
		break;
	case TIID_BlackSpot:
	case TIID_ParticleManual:
	case TIID_Brightness:
		lReturn = m_Device.MotionSequence.OnAction_LED_ON();
		//!SH _190130: 메세지만 전달
		break;
	}
	return lReturn;
}

//=============================================================================@
// Method		: OnTest_TestInitialize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/1/13 - 22:11
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_TestInitialize(__in UINT nStepIdx)
{
	OnLog(_T("OnTest_TestInitialize() :: Start"));
	LRESULT lReturn = RCA_OK;

	float fVoltageOn[2] = { m_stInspInfo.ModelInfo.fVoltage[0], 0 };
	float fVoltageOff[2] = { 0, };

	if (RCA_OK == lReturn)
	{
		OnLog(_T("OnTest_TestInitialize() :: CamBrd Volt On Start"));
		lReturn = SetLuriCamBrd_Volt(fVoltageOn);
		Sleep(m_stInspInfo.ModelInfo.nCameraDelay);
		OnLog(_T("OnTest_TestInitialize() :: CamBrd Volt On : %.2f mA // Delay : %d ms"), m_stInspInfo.ModelInfo.fVoltage[0], m_stInspInfo.ModelInfo.nCameraDelay);

		if (RCA_OK != lReturn)
		{
			OnLog(_T("OnTest_TestInitialize() :: Volt On TransferErr"));
			lReturn = RCA_TransferErr;
		}
	}

	// OpenShort 추가 [1/28/2019 Seongho.Lee]
	if (RCA_OK == lReturn && m_stInspInfo.ModelInfo.bUseOpenShort == TRUE)
	{
		OnLog(_T("OnTest_TestInitialize() :: OpenShort Test Start"));
		bool bOpenShort = FALSE;
		lReturn = SetLuriCamBrd_OpenShort(bOpenShort);

		if (RCA_OK != lReturn)
		{
			OnLog(_T("OnTest_TestInitialize() :: OpenShort Test TransferErr"));
			lReturn = RCA_TransferErr;
		}
		else
		{
			OnLog(_T("OnTest_TestInitialize() :: OpenShort Test Transfer OK"));
			if (bOpenShort == FALSE)
			{
				OnLog(_T("OnTest_TestInitialize() :: OpenShort Test Fail"));
				lReturn = RCA_OpenShort;
			}
			else // OpenShort 테스트를 통과한 경우 전원을 껏다 킨다.
			{
				OnLog(_T("OnTest_TestInitialize() :: OpenShort Test Pass"));
				Sleep(m_stInspInfo.ModelInfo.nCameraDelay + 500);
				lReturn = SetLuriCamBrd_Volt(fVoltageOff);
				OnLog(_T("OnTest_TestInitialize() :: Volt Off"));
				Sleep(m_stInspInfo.ModelInfo.nCameraDelay + 500);
				if (RCA_OK == lReturn)
				{
					lReturn = SetLuriCamBrd_Volt(fVoltageOn);
					OnLog(_T("OnTest_TestInitialize() :: Volt On"));
					if (RCA_OK != lReturn)
					{
						OnLog(_T("OnTest_TestInitialize() :: Volt On TransferErr"));
						lReturn = RCA_TransferErr;
					}
				}
				else
				{
					OnLog(_T("OnTest_TestInitialize() :: Volt Off TransferErr"));
					lReturn = RCA_TransferErr;
				}
			}
		}

		Sleep(m_stInspInfo.ModelInfo.nCameraDelay + 500);
		OnLog(_T("OnTest_TestInitialize() :: OpenShort Delay : %d ms"), m_stInspInfo.ModelInfo.nCameraDelay);
	}

	if (RCA_OK == lReturn)
	{
		OnSetStatus_PogoCount();
	}

	if (RCA_OK == lReturn)
	{
		m_stInspInfo.CamInfo.nJudgmentInitial = TER_Pass;

// 		if (m_stInspInfo.bUseBarcode == TRUE)
// 		{
// 			OnUpdateTestResult(TER_Pass, nStepIdx, m_stInspInfo.CamInfo.szBarcode);
// 		} 
// 		else
// 		{
			OnUpdateTestResult(TER_Pass, nStepIdx, g_szResultCode_All[lReturn]);
		//}
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnTest_TestFinalize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/1/13 - 22:11
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_TestFinalize(__in UINT nStepIdx)
{
	LRESULT lReturn = RCA_OK;

	if (RCA_OK == lReturn)
	{
		CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);
	}

	if (RCA_OK == lReturn)
	{
		float fVoltageOff[2] = { 0, 0 };

		lReturn = SetLuriCamBrd_Volt(fVoltageOff);

		if (RCA_OK != lReturn)
		{
			lReturn = RCA_TransferErr;
		}
	}

	if (RCA_OK == lReturn)
	{
		m_stInspInfo.CamInfo.nJudgmentFinalize = TER_Pass;
		OnUpdateTestResult(TER_Pass, nStepIdx, g_szResultCode_All[lReturn]);
	}
	else
	{
		lReturn = RCA_TransferErr;
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnTest_BarCode
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/1/13 - 22:11
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_BarCode(__in UINT nStepIdx)
{
	LRESULT lReturn = RCA_OK;

	if (RCA_OK != lReturn)
	{
		OnUpdateTestResult(TER_Pass, nStepIdx, g_szResultCode_All[lReturn]);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnTest_Angle
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/1/14 - 12:28
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_Angle(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;


	if (!GetImageBuffer())
	{
		lReturn = RCA_NoImage;
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
		return lReturn;
	}

	m_stInspInfo.ModelInfo.stAngle[nTestIdx].bTestMode = TRUE;

	m_ImageTest.OnTestProcessAngle(m_TestImage,
		&m_stInspInfo.ModelInfo.stAngle[nTestIdx].stAngleOpt,
		m_stInspInfo.ModelInfo.stAngle[nTestIdx].stAngleResult);

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stAngle[nTestIdx].stAngleResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s LC:%.2f RC:%.2f TC:%.2f BC:%.2f"),
				g_szResultCode_All[lReturn],
				m_stInspInfo.ModelInfo.stAngle[nTestIdx].stAngleResult.dValue[ROI_AL_Left - 1],
				m_stInspInfo.ModelInfo.stAngle[nTestIdx].stAngleResult.dValue[ROI_AL_Right - 1],
				m_stInspInfo.ModelInfo.stAngle[nTestIdx].stAngleResult.dValue[ROI_AL_Top - 1],
				m_stInspInfo.ModelInfo.stAngle[nTestIdx].stAngleResult.dValue[ROI_AL_Bottom - 1]);
		}
		
		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stAngle[nTestIdx].stAngleResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}



	return lReturn;
}

//=============================================================================
// Method		: OnTest_CenterPoint
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/1/14 - 13:10
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_CenterPoint(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;


	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;


	if (!GetImageBuffer())
	{
		lReturn = RCA_NoImage;
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
		return lReturn;
	}


	m_ImageTest.OnSetCameraType(m_stInspInfo.ModelInfo.nCameraType);
	m_ImageTest.OnTestProcessCenterPoint(m_TestImage, &m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointOpt, m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult);


	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s X:%d Y:%d"),
				g_szResultCode_All[lReturn],
				m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetX,
				m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetY);
		}

		

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}




	return lReturn;
}

//=============================================================================
// Method		: OnTest_CenterPoint_Manual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in int iRetryCount
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/4/19 - 15:13
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_CenterPoint_Manual(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;
// 
// 	IplImage *pImage = NULL;
// 	LPBYTE pGetImage = NULL;
// 
// 	DWORD dwWidth = 0;
// 	DWORD dwHeight = 0;
// 	UINT nChannel = 0;
// 
// 	if (RCA_OK == lReturn)
// 	{
// 		LPBYTE pFrameImage = NULL;
// 
// 		for (int i = 0; i < 30; i++)
// 		{
// 			pFrameImage = GetImageBuffer(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, dwWidth, dwHeight, nChannel);
// 
// 			Sleep(33);
// 			if (NULL != pFrameImage)
// 				break;
// 		}
// 
// 		if (NULL != pFrameImage)
// 		{
// 			if (m_stInspInfo.ModelInfo.nGrabType == GrabType_NTSC)
// 			{
// 				if (g_iImageSzHeight_Analog[m_stInspInfo.ModelInfo.nVideoType] != dwHeight)
// 				{
// 					lReturn = RCA_NoImage;
// 				}
// 			}
// 
// 			pGetImage = new BYTE[dwWidth * dwHeight * nChannel];
// 			memcpy(pGetImage, pFrameImage, dwWidth * dwHeight * nChannel);
// 
// 			int iCheckImage = 0;
// 
// 			for (int y = 0; y < dwHeight; y++)
// 			{
// 				for (int x = 0; x < dwWidth; x++)
// 				{
// 					BYTE byCheckR = pGetImage[y * dwWidth * nChannel + x * nChannel + 0];
// 					BYTE byCheckG = pGetImage[y * dwWidth * nChannel + x * nChannel + 1];
// 					BYTE byCheckB = pGetImage[y * dwWidth * nChannel + x * nChannel + 2];
// 
// 					if (byCheckR == 0x00 && byCheckG == 0x00 && byCheckB == 0x00)
// 					{
// 						iCheckImage++;
// 					}
// 				}
// 			}
// 
// 			if (80 <= ((double)iCheckImage / (double)(dwWidth * dwHeight)) * 100.0)
// 			{
// 				lReturn = RCA_NoImage;
// 			}
// 		}
// 		else
// 		{
// 			lReturn = RCA_NoImage;
// 		}
// 	}
// 
// 
// 	if (RCA_OK == lReturn)
// 	{
// 		pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);
// 
// 		for (int y = 0; y < dwHeight; y++)
// 		{
// 			for (int x = 0; x < dwWidth; x++)
// 			{
// 				pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pGetImage[y * dwWidth * nChannel + x * nChannel + 0];
// 				pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pGetImage[y * dwWidth * nChannel + x * nChannel + 1];
// 				pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pGetImage[y * dwWidth * nChannel + x * nChannel + 2];
// 			}
// 
// 		}
// 
// 		OnPopupCenterPtTunningResult();
// 
// 		CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);
// 		CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, ON);
// 	}
// 
// 	if (RCA_OK == lReturn)
// 	{
// 		CString szResultText;
// 
// 		if (m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.nResult == TER_Pass)
// 		{
// 			lReturn = RCA_OK;
// 
// 			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
// 		}
// 		else
// 		{
// 			lReturn = RCA_NG;
// 
// 			szResultText.Format(_T("%s X:%d Y:%d"),
// 				g_szResultCode_All[lReturn],
// 				m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetX,
// 				m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetY);
// 		}
// 
// 
// 
// 		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.nResult, nStepIdx, szResultText);
// 	}
// 	else
// 	{
// 		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
// 	}
// 
// 
// 	if (pGetImage != NULL)
// 		delete[] pGetImage;
// 
// 	if (pImage != NULL)
// 		cvReleaseImage(&pImage);

	return lReturn;

}

//=============================================================================
// Method		: OnTest_CenterPoint_Adj
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in int iRetryCount
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/4/19 - 15:11
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_CenterPoint_Adj(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	DWORD dwWidth  = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	LPBYTE pFrameImage = NULL;

	for (UINT nIdx = 0; nIdx < m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointOpt.nTryCnt; nIdx++)
	{
		// 초기 위치
// 		OpticalCenter_Adj_Horizon(0, 0);
// 		OpticalCenter_Adj_Vertical(0, 0);
		if (!GetImageBuffer())
		{
			lReturn = RCA_NoImage;
			OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
			return lReturn;
		}



			// 광축 측정
			m_ImageTest.OnSetCameraType(m_stInspInfo.ModelInfo.nCameraType);
			m_ImageTest.OnTestProcessCenterPoint(m_TestImage, &m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointOpt, m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult);

		// 광축 조정
// 		OpticalCenter_Adj_Horizon(0, m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetX);
// 		OpticalCenter_Adj_Vertical(0, m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetY);

	}

	if (RCA_OK == lReturn)
	{

		if (!GetImageBuffer())
		{
			lReturn = RCA_NoImage;
			OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
			return lReturn;
		}

		// 광축 측정
		m_ImageTest.OnSetCameraType(m_stInspInfo.ModelInfo.nCameraType);
		m_ImageTest.OnTestProcessCenterPoint(m_TestImage, &m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointOpt, m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult);


		OpticalCenter_Adj_AllWrite(0);
	}

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;
			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;
			szResultText.Format(_T("%s X:%d Y:%d"), g_szResultCode_All[lReturn], m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetX, m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetY);
		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnTest_Color
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/1/14 - 13:14
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_Color(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;


	if (!GetImageBuffer())
	{
		lReturn = RCA_NoImage;
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
		return lReturn;
	}

	m_stInspInfo.ModelInfo.stColor[nTestIdx].bTestMode = TRUE;
		m_ImageTest.OnTestProcessColor(m_TestImage,
			&m_stInspInfo.ModelInfo.stCenterPoint[0].stCenterPointOpt,
			&m_stInspInfo.ModelInfo.stColor[nTestIdx].stColorOpt,
			m_stInspInfo.ModelInfo.stColor[nTestIdx].stColorResult);

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stColor[nTestIdx].stColorResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			CString szBuf;
			szBuf.Format(_T("%s"), g_szResultCode_All[lReturn]);

			szResultText += szBuf;

			for (int i = 0; i < ROI_CL_Max; i++)
			{
				if (m_stInspInfo.ModelInfo.stColor[nTestIdx].stColorResult.nEachResult[i] == TER_Fail)
				{
					szBuf.Format(_T(" %s"), g_szRoiColor[i]);
					szResultText += szBuf;
				}
			}

		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stColor[nTestIdx].stColorResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}


	return lReturn;
}

//=============================================================================
// Method		: OnTest_Current
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/1/14 - 13:17
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_Current(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;


	OnLog(_T("OnEachTest_Current() :: Start // RetryCnt : %d"), iRetryCount);

	if (RCA_OK == lReturn)
	{
		double iValue = 0;

		for (int iRetry = 0; iRetry < iRetryCount + 1; iRetry++)
		{
			lReturn = TestProcessCurrent(&m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentOpt, m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult);

			if (RCA_OK == lReturn)
			{
				iValue += (m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.iValue[0]);
			}
		}

		m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.iValue[0] = iValue / (iRetryCount + 1);

		if (m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentOpt.nSpecMin[0] <= m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.iValue[0]
			&& m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentOpt.nSpecMax[0] >= m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.iValue[0])
		{
			m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.nEachResult[0] = TER_Pass;
			m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.nResult = TER_Pass;
		}
		else
		{
			m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.nEachResult[0] = TER_Fail;
			m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.nResult = TER_Fail;
		}
	}

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s Current:%.1fmA"),
				g_szResultCode_All[lReturn],
				m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.iValue[0]);
		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnTest_Particle
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/1/14 - 13:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_Particle(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;


	if (!GetImageBuffer())
	{
		lReturn = RCA_NoImage;
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
		return lReturn;
	}


		m_ImageTest.OnTestProcessParticle(m_TestImage,
			&m_stInspInfo.ModelInfo.stBlackSpot[nTestIdx].stParticleOpt,
			m_stInspInfo.ModelInfo.stBlackSpot[nTestIdx].stParticleResult);

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stBlackSpot[nTestIdx].stParticleResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s Particle:%d"),
				g_szResultCode_All[lReturn],
				m_stInspInfo.ModelInfo.stBlackSpot[nTestIdx].stParticleResult.nFailCount);
		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stBlackSpot[nTestIdx].stParticleResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}
	


	return lReturn;
}

//=============================================================================
// Method		: OnTest_Particle
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/1/14 - 13:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_Defectpixel(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	if (!GetImageBuffer())
	{
		lReturn = RCA_NoImage;
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
		return lReturn;
	}


		m_ImageTest.OnTestProcessDefectPixel(m_TestImage,
			&m_stInspInfo.ModelInfo.stDefectPixel[nTestIdx].stDefectPixelOpt,
			m_stInspInfo.ModelInfo.stDefectPixel[nTestIdx].stDefectPixelData);

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stDefectPixel[nTestIdx].stDefectPixelData.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s Particle:%d, %d"),
				g_szResultCode_All[lReturn],
				m_stInspInfo.ModelInfo.stDefectPixel[nTestIdx].stDefectPixelData.nFailCount[0],
				m_stInspInfo.ModelInfo.stDefectPixel[nTestIdx].stDefectPixelData.nFailCount[1]);
		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stDefectPixel[nTestIdx].stDefectPixelData.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}



	return lReturn;
}
//=============================================================================
// Method		: OnTest_Brightness
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/1/14 - 13:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_Brightness(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;


	if (!GetImageBuffer())
	{
		lReturn = RCA_NoImage;
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
		return lReturn;
	}


		m_ImageTest.OnTestProcessBrightness(m_TestImage,
			&m_stInspInfo.ModelInfo.stBrightness[nTestIdx].stBrightnessOpt,
			m_stInspInfo.ModelInfo.stBrightness[nTestIdx].stBrightnessResult);

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stBrightness[nTestIdx].stBrightnessResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s S1:%.0f S2:%.0f S3:%.0f S4:%.0f"),
				g_szResultCode_All[lReturn],
				m_stInspInfo.ModelInfo.stBrightness[nTestIdx].stBrightnessResult.dbValue[ROI_BR_Side_1],
				m_stInspInfo.ModelInfo.stBrightness[nTestIdx].stBrightnessResult.dbValue[ROI_BR_Side_2],
				m_stInspInfo.ModelInfo.stBrightness[nTestIdx].stBrightnessResult.dbValue[ROI_BR_Side_3],
				m_stInspInfo.ModelInfo.stBrightness[nTestIdx].stBrightnessResult.dbValue[ROI_BR_Side_4]);
		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stBrightness[nTestIdx].stBrightnessResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}


	return lReturn;
}

//=============================================================================
// Method		: OnTest_Rotation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/1/14 - 13:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_Rotation(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;


	if (!GetImageBuffer())
	{
		lReturn = RCA_NoImage;
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
		return lReturn;
	}

	m_stInspInfo.ModelInfo.stRotate[nTestIdx].bTestMode = TRUE;

		m_ImageTest.OnTestProcessRotate(m_TestImage,
			&m_stInspInfo.ModelInfo.stCenterPoint[0].stCenterPointOpt,
			&m_stInspInfo.ModelInfo.stRotate[nTestIdx].stRotateOpt,
			m_stInspInfo.ModelInfo.stRotate[nTestIdx].stRotateResult);

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stRotate[nTestIdx].stRotateResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s Degree:%.2f"),
				g_szResultCode_All[lReturn],
				m_stInspInfo.ModelInfo.stRotate[nTestIdx].stRotateResult.dbValue);

		}
		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stRotate[nTestIdx].stRotateResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}



	return lReturn;
}

//=============================================================================
// Method		: OnTest_SFR
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/1/14 - 13:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnTest_SFR(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	//IplImage* pImage = NULL;
	//LPBYTE pGetImage = NULL;

	//DWORD dwWidth = 0;
	//DWORD dwHeight = 0;
	//UINT nChannel = 0;

	//if (RCA_OK == lReturn)
	//{
	//	m_stInspInfo.ModelInfo.bI2CFile_1 = m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFROpt.bIICFile_1;
	//	if (m_stInspInfo.ModelInfo.bI2CFile_1 == TRUE)
	//	{
	//		m_stInspInfo.ModelInfo.szI2CFile_1 = m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFROpt.szIICFile_1;

	//		CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);
	//		CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFROpt.bDistortion, m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFROpt.bEdge, ON);
	//	} 
	//	else
	//	{

	//		EdgeOnOff(m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFROpt.bEdge);
	//		for (int i = 0; i < 30; i++)
	//		{
	//			DoEvents(33);
	//		}

	//		DistortionCorrection(m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFROpt.bDistortion);
	//		for (int i = 0; i < 30; i++)
	//		{
	//			DoEvents(33);
	//		}
	//	}


	//	LPBYTE pFrameImage = NULL;

	//	for (int i = 0; i < 30; i++)
	//	{
	//		pFrameImage = GetImageBuffer(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, dwWidth, dwHeight, nChannel);
	//		Sleep(33);

	//		if (NULL != pFrameImage)
	//			break;
	//	}

	//	if (NULL != pFrameImage)
	//	{
	//		if (m_stInspInfo.ModelInfo.nGrabType == GrabType_NTSC)
	//		{
	//			if (g_iImageSzHeight_Analog[m_stInspInfo.ModelInfo.nVideoType] != dwHeight)
	//			{
	//				lReturn = RCA_NoImage;
	//			}
	//		}

	//		pGetImage = new BYTE[dwWidth * dwHeight * nChannel];
	//		memcpy(pGetImage, pFrameImage, dwWidth * dwHeight * nChannel);

	//		int iCheckImage = 0;

	//		for (int y = 0; y < dwHeight; y++)
	//		{
	//			for (int x = 0; x < dwWidth; x++)
	//			{
	//				BYTE byCheckR = pGetImage[y * dwWidth * nChannel + x * nChannel + 0];
	//				BYTE byCheckG = pGetImage[y * dwWidth * nChannel + x * nChannel + 1];
	//				BYTE byCheckB = pGetImage[y * dwWidth * nChannel + x * nChannel + 2];

	//				if (byCheckR == 0x00 && byCheckG == 0x00 && byCheckB == 0x00)
	//				{
	//					iCheckImage++;
	//				}
	//			}
	//		}

	//		if (80 <= ((double)iCheckImage / (double)(dwWidth * dwHeight)) * 100.0)
	//		{
	//			lReturn = RCA_NoImage;
	//		}

	//	}
	//	else
	//	{
	//		lReturn = RCA_NoImage;
	//	}
	//}

	//if (RCA_OK == lReturn)
	//{

	//	pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	//	for (int y = 0; y < dwHeight; y++)
	//	{
	//		for (int x = 0; x < dwWidth; x++)
	//		{
	//			pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pGetImage[y * dwWidth * nChannel + x * nChannel + 0];
	//			pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pGetImage[y * dwWidth * nChannel + x * nChannel + 1];
	//			pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pGetImage[y * dwWidth * nChannel + x * nChannel + 2];
	//		}
	//	}



	//	m_ImageTest.OnTestProcessSFR(
	//		pImage,
	//		&m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFROpt,
	//		m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFRResult);

	//	
	//}

	//if (RCA_OK == lReturn)
	//{
	//	CString szResultText;

	//	if (m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFRResult.nResult == TER_Pass)
	//	{
	//		lReturn = RCA_OK;

	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}
	//	else
	//	{
	//		lReturn = RCA_NG;

	//		szResultText.Format(_T("%s SFR:%d/%d"),
	//			g_szResultCode_All[lReturn],
	//			m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFRResult.iPassCnt,
	//			m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFRResult.iTotalCnt);
	//	}

	//	OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stSFR[nTestIdx].stSFRResult.nResult, nStepIdx, szResultText);
	//}
	//else
	//{
	//	OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	//}

	//if (pGetImage != NULL)
	//	delete[] pGetImage;

	//if (pImage != NULL)
	//	cvReleaseImage(&pImage);

	//

	return lReturn;
}


LRESULT CTestManager_EQP::OnTest_EIAJ(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	if (RCA_OK == lReturn)
	{


		UINT nValue[ReOp_ItemNum - ReOp_CenterLeft] = { 0, };
	
		for (int iRetry = 0; iRetry < iRetryCount + 1; iRetry++)
		{
			for (int iCount = 0; iCount < 5; iCount++)
			{
				if (!GetImageBuffer())
				{
					lReturn = RCA_NoImage;
					OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
					return lReturn;
				}



					m_ImageTest.OnTestProcessEIAJ(
						m_TestImage, &m_stInspInfo.ModelInfo.stEIAJ[nTestIdx]);
				Sleep(33);

			}
// 					int ivaluecount = 0;
// 					for (int iRoi = ReOp_CenterLeft; iRoi < ReOp_ItemNum; iRoi++)
// 					{
// 						if (m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJOp.StandardrectData[iRoi].bUse == TRUE)
// 						{
// 							nValue[ivaluecount] = m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nValueResult[iRoi - ReOp_CenterLeft];
// 							ivaluecount++;
// 						}
// 					}
// 			
// 
// 
// 
// 				m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nResult = TER_Pass;
// 
// 				ivaluecount = 0;
// 				m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.iTotalCnt = 0;
// 				m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.iPassCnt = 0;
// 
// 				//for (int iRoi = 0; iRoi < ReOp_ItemNum - ReOp_CenterLeft; iRoi++)
// 				for (int iRoi = ReOp_CenterLeft; iRoi < ReOp_ItemNum; iRoi++)
// 				{
// 					if (m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJOp.StandardrectData[iRoi].bUse == TRUE)
// 					{
// 						m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.iTotalCnt++;
// 						m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nValueResult[iRoi - ReOp_CenterLeft] = nValue[ivaluecount] / (iRetryCount + 1);
// 
// 						if (m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJOp.StandardrectData[iRoi].i_Threshold_Min <= m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nValueResult[iRoi - ReOp_CenterLeft]
// 							&& m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJOp.StandardrectData[iRoi].i_Threshold_Max >= m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nValueResult[iRoi - ReOp_CenterLeft])
// 						{
// 							m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nEachResult[iRoi - ReOp_CenterLeft] = TER_Pass;
// 							m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.iPassCnt++;
// 
// 						}
// 						else
// 						{
// 							m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nEachResult[iRoi - ReOp_CenterLeft] = TER_Fail;
// 							m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nResult = TER_Fail;
// 						}
// 
// 						ivaluecount++;
// 					}
// 
// 				}

			
		}

		
	}
	

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s EIAJ:%d/%d"),
				g_szResultCode_All[lReturn],
				m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nResult_cnt,
				m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nTotal_cnt);
		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stEIAJ[nTestIdx].stEIAJData.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}


	return lReturn;
}


LRESULT CTestManager_EQP::OnTest_Reverse(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	
	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	if (RCA_OK == lReturn)
	{

		for (int iRetry = 0; iRetry < iRetryCount + 1; iRetry++)
		{
			if (!GetImageBuffer())
			{
				lReturn = RCA_NoImage;
				OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
				return lReturn;
			}
			m_stInspInfo.ModelInfo.stReverse[nTestIdx].bTestMode = TRUE;


				m_ImageTest.OnTestProcessReverseColor(
					m_TestImage,
					&m_stInspInfo.ModelInfo.stReverse[nTestIdx].stReverseOpt,
					m_stInspInfo.ModelInfo.stReverse[nTestIdx].stReverseResult);

				if (m_stInspInfo.ModelInfo.stReverse[nTestIdx].stReverseResult.nResult == TER_Pass)
				{
					break;
				}

			
		}
	}
	

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stReverse[nTestIdx].stReverseResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stReverse[nTestIdx].stReverseResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}

	return lReturn;
}


LRESULT CTestManager_EQP::OnTest_ParticleManual(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	if (RCA_OK == lReturn)
	{
		if (!GetImageBuffer())
		{
			lReturn = RCA_NoImage;
			OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
			return lReturn;
		}


		OnPopupParticleManualResult(m_stInspInfo.ModelInfo.stParticleMA[nTestIdx].stParticleResult.nResult);
	}

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stParticleMA[nTestIdx].stParticleResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;

			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stParticleMA[nTestIdx].stParticleResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}

	return lReturn;
}


LRESULT CTestManager_EQP::OnTest_PatternNoise(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;
//
//	
//
//	DWORD dwWidth = 0;
//	DWORD dwHeight = 0;
//	UINT nChannel = 0;
//
//	if (RCA_OK == lReturn)
//	{
//		LPBYTE pFrameImage = NULL;
//
//		IplImage* pImage = NULL;
//		LPBYTE pGetImage = NULL;
//
//		double dValue[ROI_PN_MaxEnum] = { 0, };
//
//		for (int iRetry = 0; iRetry < iRetryCount + 1; iRetry++)
//		{
//			for (int i = 0; i < 30; i++)
//			{
//				pFrameImage = GetImageBuffer(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, dwWidth, dwHeight, nChannel);
//				Sleep(33);
//
//				if (NULL != pFrameImage)
//					break;
//			}
//
//			if (NULL != pFrameImage)
//			{
//				if (m_stInspInfo.ModelInfo.nGrabType == GrabType_NTSC)
//				{
//					if (g_iImageSzHeight_Analog[m_stInspInfo.ModelInfo.nVideoType] != dwHeight)
//					{
//						lReturn = RCA_NoImage;
//					}
//				}
//
//				pGetImage = new BYTE[dwWidth * dwHeight * nChannel];
//				memcpy(pGetImage, pFrameImage, dwWidth * dwHeight * nChannel);
//
//				int iCheckImage = 0;
//
//				for (int y = 0; y < dwHeight; y++)
//				{
//					for (int x = 0; x < dwWidth; x++)
//					{
//						BYTE byCheckR = pGetImage[y * dwWidth * nChannel + x * nChannel + 0];
//						BYTE byCheckG = pGetImage[y * dwWidth * nChannel + x * nChannel + 1];
//						BYTE byCheckB = pGetImage[y * dwWidth * nChannel + x * nChannel + 2];
//
//						if (byCheckR == 0x00 && byCheckG == 0x00 && byCheckB == 0x00)
//						{
//							iCheckImage++;
//						}
//					}
//				}
//
//// 				if (80 <= ((double)iCheckImage / (double)(dwWidth * dwHeight)) * 100.0)
//// 				{
//// 					lReturn = RCA_NoImage;
//// 					break;
//// 				}
//
//			}
//			else
//			{
//				lReturn = RCA_NoImage;
//				break;
//			}
//
//			if (RCA_OK == lReturn)
//			{
//				pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);
//
//				for (int y = 0; y < dwHeight; y++)
//				{
//					for (int x = 0; x < dwWidth; x++)
//					{
//						pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pGetImage[y * dwWidth * nChannel + x * nChannel + 0];
//						pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pGetImage[y * dwWidth * nChannel + x * nChannel + 1];
//						pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pGetImage[y * dwWidth * nChannel + x * nChannel + 2];
//					}
//				}
//
//				m_ImageTest.OnTestProcessPatternNoise(
//					pImage,
//					&m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseOpt,
//					m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult);
//
//				for (int iRoi = 0; iRoi < ROI_PN_MaxEnum; iRoi++)
//				{
//					dValue[iRoi] += m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.dValue_dB[iRoi];
//				}
//
//			}
//
//			if (pGetImage != NULL)
//				delete[] pGetImage;
//
//			if (pImage != NULL)
//				cvReleaseImage(&pImage);
//		}
//
//		//판정
//		m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.nResult = TER_Pass;
//		
//		for (int iRoi = 0; iRoi < ROI_PN_MaxEnum; iRoi++)
//		{
//			m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.dValue_dB[iRoi] = dValue[iRoi] / (iRetryCount + 1);
//
//			if (m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.dValue_dB[iRoi] >= m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stInitPatternNoiseOpt.dThr_dB)
//			{
//				m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.nEachResult[iRoi] = TER_Pass;
//			}
//			else
//			{
//				m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.nEachResult[iRoi] = TER_Fail;
//				m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.nResult = TER_Fail;
//			}
//		}
//	}
//
//	
//
//	if (RCA_OK == lReturn)
//	{
//		CString szResultText;
//
//		if (m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.nResult == TER_Pass)
//		{
//			lReturn = RCA_OK;
//
//			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
//		}
//		else
//		{
//			lReturn = RCA_NG;
//
//			szResultText.Format(_T("%s PatternNoise:%d/%d"),
//				g_szResultCode_All[lReturn],
//				m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.iPassCnt,
//				m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.iTotalCnt);
//		}
//
//		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult.nResult, nStepIdx, szResultText);
//	}
//	else
//	{
//		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
//	}
//
//
	return lReturn;
}


LRESULT CTestManager_EQP::OnTest_IRFilter(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	OnLog(_T("OnTest_IRFilter() :: Start"));

	LRESULT	lReturn = RCA_OK;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	if (RCA_OK == lReturn)
	{
		OnLog(_T("OnTest_IRFilter() :: GetImageBuffer Start"));
		if (!GetImageBuffer())
		{
			lReturn = RCA_NoImage;
			OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
			return lReturn;
		}

		m_ImageTest.OnTestProcessIRFilter(m_TestImage, &m_stInspInfo.ModelInfo.stIRFilter[nTestIdx].stIRFilterOpt, m_stInspInfo.ModelInfo.stIRFilter[nTestIdx].stIRFilterResult);

		OnLog(_T("OnTest_IRFilter() :: Value : %.2f // Threshold : %.2f"), m_stInspInfo.ModelInfo.stIRFilter[nTestIdx].stIRFilterResult.dValue, m_stInspInfo.ModelInfo.stIRFilter[nTestIdx].stIRFilterOpt.dThreshold);
	}

	if (RCA_OK == lReturn)
	{
		CString szResultText;

		if (m_stInspInfo.ModelInfo.stIRFilter[nTestIdx].stIRFilterResult.nResult == TER_Pass)
		{
			lReturn = RCA_OK;
			szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
		}
		else
		{
			lReturn = RCA_NG;
			szResultText.Format(_T("%s IRFilter:%.2f"), g_szResultCode_All[lReturn], m_stInspInfo.ModelInfo.stIRFilter[nTestIdx].stIRFilterResult.dValue);
		}

		OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stIRFilter[nTestIdx].stIRFilterResult.nResult, nStepIdx, szResultText);
	}
	else
	{
		OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	}

	OnLog(_T("OnTest_IRFilter() :: End"));
	return lReturn;
}



LRESULT CTestManager_EQP::OnTest_IRFilterManual(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	//if (RCA_OK == lReturn)
	//{
	//	OnPopupParticleManualResult(m_stInspInfo.ModelInfo.stIRFilterManual[nTestIdx].stIRFilterResult.nResult);
	//}

	//if (RCA_OK == lReturn)
	//{
	//	CString szResultText;

	//	if (m_stInspInfo.ModelInfo.stIRFilterManual[nTestIdx].stIRFilterResult.nResult == TER_Pass)
	//	{
	//		lReturn = RCA_OK;

	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}
	//	else
	//	{
	//		lReturn = RCA_NG;

	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}

	//	OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stIRFilterManual[nTestIdx].stIRFilterResult.nResult, nStepIdx, szResultText);
	//}
	//else
	//{
	//	OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	//}

	return lReturn;
}

LRESULT CTestManager_EQP::OnTest_LEDTest(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	//if (RCA_OK == lReturn)
	//{
	//	m_stInspInfo.ModelInfo.stLED[nTestIdx].stLEDCurrResult.nResetCount = 0;
	//	OnPopupLEDTestResult(nTestIdx, m_stInspInfo.ModelInfo.stLED[nTestIdx].stLEDCurrResult.nResult);
	//}

	//if (RCA_OK == lReturn)
	//{
	//	CString szResultText;

	//	if (m_stInspInfo.ModelInfo.stLED[nTestIdx].stLEDCurrResult.nResult == TER_Pass)
	//	{
	//		lReturn = RCA_OK;

	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}
	//	else
	//	{
	//		lReturn = RCA_NG;

	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}

	//	OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stLED[nTestIdx].stLEDCurrResult.nResult, nStepIdx, szResultText);
	//}
	//else
	//{
	//	OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	//}

	return lReturn;
}

LRESULT CTestManager_EQP::OnTest_VideoSignal(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	//if (RCA_OK == lReturn)
	//{
	//	int iValue = 0;

	//	for (int iRetry = 0; iRetry < iRetryCount + 1; iRetry++)
	//	{
	//		// Lock Signal Read Function
	//		//lReturn = TestProcessCurrent(&m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentOpt, m_stInspInfo.ModelInfo.stCurrent[nTestIdx].stCurrentResult);
	//		lReturn = TestProcessVideoSignal(VideoView_Ch_1, m_stInspInfo.ModelInfo.stVideoSignal[nTestIdx].nResult);

	//		if (RCA_OK == lReturn)
	//		{
	//			break;
	//		}
	//	}
	//}

	//if (RCA_OK == lReturn)
	//{
	//	CString szResultText;

	//	if (m_stInspInfo.ModelInfo.stVideoSignal[nTestIdx].nResult == TER_Pass)
	//	{
	//		lReturn = RCA_OK;

	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}
	//	else
	//	{
	//		lReturn = RCA_NG;

	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}

	//	OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stVideoSignal[nTestIdx].nResult, nStepIdx, szResultText);
	//}
	//else
	//{
	//	OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	//}

	return lReturn;
}


LRESULT CTestManager_EQP::OnTest_Reset(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	//if (RCA_OK == lReturn)
	//{
	//	int iValue = 0;

	//	for (int iRetry = 0; iRetry < iRetryCount + 1; iRetry++)
	//	{
	//		// Lock Signal Read Function
	//		lReturn = TestProcessReset(VideoView_Ch_1, m_stInspInfo.ModelInfo.stReset[nTestIdx].nDelay, m_stInspInfo.ModelInfo.stReset[nTestIdx].nResult);

	//		if (RCA_OK == lReturn)
	//		{
	//			break;
	//		}
	//	}
	//}

	//if (RCA_OK == lReturn)
	//{
	//	CString szResultText;

	//	if (m_stInspInfo.ModelInfo.stReset[nTestIdx].nResult == TER_Pass)
	//	{
	//		lReturn = RCA_OK;

	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}
	//	else
	//	{
	//		lReturn = RCA_NG;

	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}

	//	OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stReset[nTestIdx].nResult, nStepIdx, szResultText);
	//}
	//else
	//{
	//	OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	//}

	return lReturn;
}

LRESULT CTestManager_EQP::OnTest_OperationMode(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	//float fVoltageOn[2] = { m_stInspInfo.ModelInfo.fVoltage[0], 0 };
	//float fVoltageOff[2] = { 0, 0 };

	//// 15fps
	//UINT nfpsMode = 0;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Fail;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Fail;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] = TER_Fail;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] = TER_Fail;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode].Reset();

	//if (lReturn == RCA_OK)
	//{
	//	if (CameraRegToFunctionChange(0, nfpsMode, 1, 1))
	//	{
	//		for (int i = 0; i < 50; i++)
	//			DoEvents(33);

	//		TestProcessVideoSignal(VideoView_Ch_1, m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode]);
	//		if (m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
	//		{
	//			TestProcessFrameCheck(VideoView_Ch_1, nfpsMode, m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode]);
	//			if (m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass)
	//			{
	//				TestProcessCurrent(&m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrOpt[nfpsMode], m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode]);
	//			}
	//		}

	//		m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Pass;
	//		//TestProcessReset(VideoView_Ch_1,
	//		//	m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetDelay, m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode]);

	//	}
	//	else
	//	{
	//		lReturn = RCA_NG;
	//	}
	//}
	//
	//if (m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode].nEachResult[0] == TER_Pass
	//	&& m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] == TER_Pass
	//	&& m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass
	//	&& m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
	//{
	//	m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Pass;
	//}


	//// 30fps
	//nfpsMode = 1;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Fail;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Fail;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] = TER_Fail;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] = TER_Fail;
	//m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode].Reset();

	//if (lReturn == RCA_OK)
	//{
	//	if (CameraRegToFunctionChange(0, nfpsMode, 1, 1))
	//	{
	//		for (int i = 0; i < 50; i++)
	//			DoEvents(33);

	//		TestProcessVideoSignal(VideoView_Ch_1, m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode]);
	//		if (m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
	//		{
	//			TestProcessFrameCheck(VideoView_Ch_1, nfpsMode, m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode]);
	//			if (m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass)
	//			{
	//				TestProcessCurrent(&m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrOpt[nfpsMode], m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode]);
	//			}
	//		}

	//		m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Pass;
	//		//TestProcessReset(VideoView_Ch_1,
	//		//	m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetDelay, m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode]);

	//	}
	//	else
	//	{
	//		lReturn = RCA_NG;
	//	}

	//	//CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);
	//	//CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, ON);
	//}
	//
	//if (m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode].nEachResult[0] == TER_Pass
	//	&& m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] == TER_Pass
	//	&& m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass
	//	&& m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
	//{
	//	m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Pass;
	//}

	//if (m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nEachResult[0] == TER_Pass
	//	&& m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nEachResult[1] == TER_Pass)
	//{
	//	m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResult = RCA_OK;
	//}
	//else
	//{
	//	m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResult = RCA_NG;
	//}

	//// 30fps
	//if (RCA_OK == lReturn)
	//{
	//	CString szResultText;

	//	if (m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResult == RCA_OK)
	//	{
	//		lReturn = RCA_OK;
	//		szResultText.Format(_T("%s"), g_szResultCode_All[lReturn]);
	//	}
	//	else
	//	{
	//		lReturn = RCA_NG;
	//		m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResult = lReturn;

	//		szResultText.Format(_T("%s 15fps :%.1fmA, 30fps :%.1fmA "),
	//			g_szResultCode_All[lReturn],
	//			m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrResult[0].iValue[0],
	//			m_stInspInfo.ModelInfo.stOperMode[nTestIdx].stCurrResult[1].iValue[0]);
	//	}

	//	OnUpdateTestResult((enTestEachResult)m_stInspInfo.ModelInfo.stOperMode[nTestIdx].nResult, nStepIdx, szResultText);
	//}
	//else
	//{
	//	OnUpdateTestResult(TER_Fail, nStepIdx, g_szResultCode_All[lReturn]);
	//}


	return lReturn;
}

LRESULT CTestManager_EQP::OnEachTest_Current(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_Current(iRetryCount, nTestStepIdx, nTestIdx);
}

LRESULT CTestManager_EQP::OnEachTest_OperationMode(__in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_OperationMode(nTestStepIdx, nTestIdx);
}

//=============================================================================
// Method		: OnEachTest_CenterPoint
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in int iRetryCount
// Parameter	: __in UINT nTestStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/4/19 - 15:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnEachTest_CenterPoint(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_CenterPoint(iRetryCount, nTestStepIdx, nTestIdx);
}

//=============================================================================
// Method		: OnEachTest_CenterPoint_Manual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in int iRetryCount
// Parameter	: __in UINT nTestStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/4/19 - 15:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnEachTest_CenterPoint_Manual(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_CenterPoint_Manual(iRetryCount, nTestStepIdx, nTestIdx);
}

//=============================================================================
// Method		: OnEachTest_CenterPoint_Adj
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in int iRetryCount
// Parameter	: __in UINT nTestStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/4/19 - 15:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnEachTest_CenterPoint_Adj(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	// 초기 위치
	OpticalCenter_Adj_Horizon(0, 0);
	OpticalCenter_Adj_Vertical(0, 0);

	for (int i = 0; i < 50; i++)
	{
		DoEvents(33);
	}

	// 광축 측정
	OnEachTest_CenterPoint(iRetryCount, nTestStepIdx, nTestIdx);

	// 광축 조정
	OpticalCenter_Adj_Horizon(0, m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetX);
	OpticalCenter_Adj_Vertical(0, m_stInspInfo.ModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetY);

	for (int i = 0; i < 50; i++)
	{
		DoEvents(33);
	}

	OnEachTest_CenterPoint(iRetryCount, nTestStepIdx, nTestIdx);

	// 광축 저장
	OpticalCenter_Adj_AllWrite(0);

	// Cam On
	CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);
	CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, ON);

	for (int i = 0; i < 100; i++)
	{
		DoEvents(33);
	}

	return lReturn = TotalResult = EachResult = OnEachTest_CenterPoint(iRetryCount, nTestStepIdx, nTestIdx);
}

LRESULT CTestManager_EQP::OnEachTest_Rotation(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_Rotation(iRetryCount, nTestStepIdx, nTestIdx);
}

LRESULT CTestManager_EQP::OnEachTest_EIAJ(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_EIAJ(iRetryCount, nTestStepIdx, nTestIdx);
}


LRESULT CTestManager_EQP::OnEachTest_SFR(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_SFR(iRetryCount, nTestStepIdx, nTestIdx);
}

LRESULT CTestManager_EQP::OnEachTest_Angle(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_Angle(iRetryCount, nTestStepIdx, nTestIdx);
}

LRESULT CTestManager_EQP::OnEachTest_Color(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_Color(iRetryCount, nTestStepIdx, nTestIdx);
}

LRESULT CTestManager_EQP::OnEachTest_Reverse(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_Reverse(iRetryCount, nTestStepIdx, nTestIdx);
}


LRESULT CTestManager_EQP::OnEachTest_LEDTest(__in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT	lReturn = RCA_OK;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	return lReturn = TotalResult = EachResult = OnTest_LEDTest(nTestStepIdx, nTestIdx);
}
 
  LRESULT CTestManager_EQP::OnEachTest_ParticleManual(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
  {
  	LRESULT	lReturn = RCA_OK;
  
  	UINT TotalResult = TR_Pass;
  	UINT EachResult = TER_Init;
  
  	return lReturn = TotalResult = EachResult = OnTest_ParticleManual(nTestStepIdx, nTestIdx);
  }

  LRESULT CTestManager_EQP::OnEachTest_BlackSpot(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
  {
	  LRESULT	lReturn = RCA_OK;

	  UINT TotalResult = TR_Pass;
	  UINT EachResult = TER_Init;

	  return lReturn = TotalResult = EachResult = OnTest_Particle(iRetryCount,nTestStepIdx, nTestIdx);
  }

  LRESULT CTestManager_EQP::OnEachTest_DefectPixel(__in int iRetryCount, UINT nTestStepIdx, UINT nTestIdx /*= 0*/)
  {
	  LRESULT	lReturn = RCA_OK;

	  UINT TotalResult = TR_Pass;
	  UINT EachResult = TER_Init;

	  return lReturn = TotalResult = EachResult = OnTest_Defectpixel(iRetryCount,nTestStepIdx, nTestIdx);
  }

  LRESULT CTestManager_EQP::OnEachTest_VideoSignal(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
  {
	  LRESULT lReturn = RCA_OK;

	  UINT TotalResult = TR_Pass;
	  UINT EachResult = TER_Init;

	  return lReturn = TotalResult = EachResult = OnTest_VideoSignal(iRetryCount, nTestStepIdx, nTestIdx);
  }

  LRESULT CTestManager_EQP::OnEachTest_Reset(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
  {
	  LRESULT lReturn = RCA_OK;

	  UINT TotalResult = TR_Pass;
	  UINT EachResult = TER_Init;

	  return lReturn = TotalResult = EachResult = OnTest_Reset(iRetryCount, nTestStepIdx, nTestIdx);
  }

 
 LRESULT CTestManager_EQP::OnEachTest_PatternNoise(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
 {
 	LRESULT	lReturn = RCA_OK;
 
 	UINT TotalResult = TER_Init;
 	UINT EachResult = TER_Init;
 
 	return lReturn = TotalResult = EachResult = OnTest_PatternNoise(iRetryCount, nTestStepIdx, nTestIdx);
 }


 LRESULT CTestManager_EQP::OnEachTest_IRFilter(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
 {
	 LRESULT	lReturn = RCA_OK;

	 UINT TotalResult = TR_Pass;
	 UINT EachResult = TER_Init;

	 return lReturn = TotalResult = EachResult = OnTest_IRFilter(iRetryCount, nTestStepIdx, nTestIdx);
 }


 LRESULT CTestManager_EQP::OnEachTest_Brightness(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
 {
	 LRESULT	lReturn = RCA_OK;

	 UINT TotalResult = TR_Pass;
	 UINT EachResult = TER_Init;

	 return lReturn = TotalResult = EachResult = OnTest_Brightness(iRetryCount, nTestStepIdx, nTestIdx);
 }
// 
// 
// LRESULT CTestManager_EQP::OnEachTest_IRFilterManual(__in UINT nTestStepIdx, __in UINT nTestIdx /*= 0*/)
// {
// 	LRESULT	lReturn = RCA_OK;
// 
// 	UINT TotalResult = TER_Init;
// 	UINT EachResult = TER_Init;
// 
// 	return lReturn = TotalResult = EachResult = OnTest_IRFilterManual(nTestStepIdx, nTestIdx);
// }




//=============================================================================
// Method		: OnDetectDigtalInSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnDetectDigitalInSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	enIO_In_BitOffset DInOffset = (enIO_In_BitOffset)byBitOffset;

	switch (DInOffset)
	{
// 	case DI_MainPower:
// 		if (FALSE == bOnOff)
// 		{
// 			if (m_bFlag_Butten[DInOffset] == FALSE)
// 			{
// 				m_bFlag_Butten[DInOffset] = TRUE;
// 
// 				OnFunc_IO_MainPower();
// 
// 				m_bFlag_Butten[DInOffset] = FALSE;
// 			}
// 		}
// 		break;

	case DI_EMO:
		if (FALSE == bOnOff)
		{
			if (m_bFlag_Butten[DInOffset] == FALSE)
			{
				m_bFlag_Butten[DInOffset] = TRUE;

				OnFunc_IO_EMO();

				m_bFlag_Butten[DInOffset] = FALSE;
			}
		}
		break;

 	case DI_Area: 
 		// test 진행 중일 때만 
 
 		if (FALSE == bOnOff)
 		{
 			if (m_bFlag_Butten[DInOffset] == FALSE)
 			{
 				m_bFlag_Butten[DInOffset] = TRUE;
 				
 				OnFunc_IO_Safety();
 
 				m_bFlag_Butten[DInOffset] = FALSE;
 			}
 		}
 		break;
 
 	case DI_Door:
 		if (FALSE == bOnOff)
 		{
 			if (m_bFlag_Butten[DInOffset] == FALSE)
 			{
 				m_bFlag_Butten[DInOffset] = TRUE;
 				
 				OnFunc_IO_Door();
 
 				m_bFlag_Butten[DInOffset] = FALSE;
 			}
 		}
 		break;
// 
// 	case DI_SensorAirCheck:
// 		if (FALSE == bOnOff)
// 		{
// 			if (m_bFlag_Butten[DInOffset] == FALSE)
// 			{
// 				m_bFlag_Butten[DInOffset] = TRUE;
// 				
// 				OnFunc_IO_AirCheck();
// 
// 				m_bFlag_Butten[DInOffset] = FALSE;
// 			}
// 		}
// 		break;

	case DI_StartBtn:
		if (TRUE == bOnOff)
		{
			if (m_bFlag_Butten[DInOffset] == FALSE)
			{
				m_bFlag_Butten[DInOffset] = TRUE;

				if (m_stInspInfo.PermissionMode == Permission_Administrator
					|| m_stInspInfo.PermissionMode == Permission_Engineer
					|| m_stInspInfo.PermissionMode == Permission_CNC)
				{
					if (OnGetStartBtnStatus() == TRUE)
						StartOperation_AutoAll();
				}
				else if (m_stInspInfo.PermissionMode == Permission_Operator)
				{
					if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
						if (OnGetStartBtnStatus() == TRUE)
							StartOperation_AutoAll();
				}
				else if (m_stInspInfo.PermissionMode == Permission_MES)
				{
					if (OnGetStartBtnStatus() == TRUE)
						StartOperation_AutoAll();
				}

				m_bFlag_Butten[DInOffset] = FALSE;
			}
		}
		break;
	case DI_StopBtn:
		if (TRUE == bOnOff)
		{
			if (m_bFlag_Butten[DInOffset] == FALSE)
			{
				m_bFlag_Butten[DInOffset] = TRUE;

				if (IsTesting_All())
				{
					if (OnPopupParticleManualVisible() == FALSE 
						&& OnPopupLEDTestVisible() == FALSE
						&& OnPopupCenterPtTunningVisible() == FALSE
						&& OnPopupFailBoxConfirmVisible() == FALSE)
					{
						OnFunc_IO_Stop();
					} 
				}

				m_bFlag_Butten[DInOffset] = FALSE;
			}
		}
		break;
//	case DI_StopBtn: // Air Check
//		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: StartOperation_Auto
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/4 - 14:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::StartOperation_Auto(__in UINT nUnitIdx, __in UINT nTestItemID)
{
	if (FALSE == m_bFlag_ReadyTest)
		return FALSE;

	if (IsTesting_Unit(nUnitIdx))
	{
		TRACE(_T("검사 작업이 진행 중입니다.\n"));
		return FALSE;
	}

	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		CloseHandle(m_hThrTest_Unit[nUnitIdx]);
		m_hThrTest_Unit[nUnitIdx] = NULL;
	}

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner = this;
	pParam->nIndex = nUnitIdx;
	pParam->nArg_1 = nTestItemID;
	pParam->nArg_2 = 0;

	m_hThrTest_Unit[nUnitIdx] = HANDLE(_beginthreadex(NULL, 0, ThreadTest_Unit, pParam, 0, NULL));

	return TRUE;
}

//=============================================================================
// Method		: StartOperation_AutoAll
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 15:55
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::StartOperation_AutoAll()
{
	if (FALSE == m_bFlag_ReadyTest)
	{
		//MessageView(_T("테스트 가능 모드가 아닙니다. \n 초기 셋팅 확인 후 프로그램을 재시작 하세요."));
		return FALSE;
	}

	if (IsTesting_All())
	{
		//MessageView(_T("TESTING..."));
		return FALSE;
	}

	if (NULL != m_hThrTest_All)
	{
		CloseHandle(m_hThrTest_All);
		m_hThrTest_All = NULL;
	}

	m_bFlag_UseStop = FALSE;

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner = this;
	pParam->nIndex = 0;
	pParam->nArg_1 = 0;
	pParam->nArg_2 = 0;

	m_hThrTest_All = HANDLE(_beginthreadex(NULL, 0, ThreadZone_All, pParam, 0, NULL));

	return TRUE;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/5/26 - 10:17
// Desc.		:
//=============================================================================
void CTestManager_EQP::StartOperation_Manual(__in UINT nUnitIdx)
{
	if (IsTesting_Unit(nUnitIdx))
	{
		MessageView(_T("Inspection is Running."));
		return;
	}

	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		CloseHandle(m_hThrTest_Unit[nUnitIdx]);
		m_hThrTest_Unit[nUnitIdx] = NULL;
	}

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner = this;
	pParam->nIndex = nUnitIdx;
	pParam->nArg_1 = FALSE;
	pParam->nArg_2 = 0;

	m_hThrTest_Unit[nUnitIdx] = HANDLE(_beginthreadex(NULL, 0, ThreadTest_Unit, pParam, 0, NULL));
}

//=============================================================================
// Method		: ThreadTest_Unit
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2016/5/18 - 17:37
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::ThreadTest_Unit(__in LPVOID lParam)
{
	CTestManager_EQP* pThis		= (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
	UINT	nUnitIdx			= ((stThreadParam*)lParam)->nIndex;
	UINT	nTestItemID			= ((stThreadParam*)lParam)->nArg_1;
	UINT	nThrType			= ((stThreadParam*)lParam)->nArg_2;

	if (NULL != lParam)
		delete lParam;

	pThis->AutomaticProcess(nUnitIdx, nTestItemID);
		
	//_endthreadex(0);
	return 0;
}

//=============================================================================
// Method		: ThreadZone_All
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2016/5/26 - 10:17
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::ThreadZone_All(__in LPVOID lParam)
{
	CTestManager_EQP* pThis = (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
	UINT bUpdateFlash = ((stThreadParam*)lParam)->nArg_1;

	if (NULL != lParam)
		delete lParam;

	pThis->AutomaticProcess_All();

	return 0;
}

//=============================================================================
// Method		: AutomaticProcess
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/4 - 14:24
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::AutomaticProcess(__in UINT nUnitIdx, __in UINT nTestItemID)
{
	TRACE(_T("=-=-= 채널 테스트 작업 시작 =-=-=\n"));
	OnLog(_T("=-=-= %d 채널 테스트 작업 시작 =-=-="), nUnitIdx + 1);
	
	LRESULT lReturn = RCC_OK;
	__try
	{
		OnInitialTest_Unit(nUnitIdx);

		lReturn = OnStartTest_Unit(nUnitIdx, nTestItemID);
	}
	__finally
	{
		if (RCC_OK == lReturn)
			OnFinallyTest_Unit(nUnitIdx, TRUE);
		else
			OnFinallyTest_Unit(nUnitIdx, FALSE);
	}

	// 불량 코드
	if ((RCC_OK != lReturn) && (RCC_TestSkip != lReturn))
	{
		m_stInspInfo.CamInfo.ResultCode = lReturn;
	}

	// 오류 처리

	if (RCC_OK == lReturn)
	{
		TRACE(_T("=-=-= 채널 테스트 작업 완료 =-=-=\n"));
		OnLog(_T("=-=-= %d 채널 테스트 작업 완료 =-=-="), nUnitIdx + 1);
		return TRUE;
	}
	else
	{
		TRACE(_T("=-=-= 채널 테스트 작업 중지 =-=-=\n"));
		OnLog(_T("=-=-= %d 채널 테스트 작업 중지 : Code -> %d =-=-="), nUnitIdx + 1, lReturn);
		return FALSE;
	}
}

//=============================================================================
// Method		: AutomaticProcess_All
// Access		: protected  
// Returns		: void
// Parameter	: void
// Qualifier	:
// Last Update	: 2016/7/15 - 14:10OnInitialTest_All
// Desc.		:
//=============================================================================
void CTestManager_EQP::AutomaticProcess_All()
{
	TRACE(_T("=-=-= 전체 Test 작업 시작 =-=-=\n"));
	OnLog(_T("=-=-= 전체 Test 작업 시작 =-=-="));

	LRESULT lReturn = 3;

	__try
	{
		// 초기화
		OnInitialTest_All();	

		lReturn = OnStartTest_All();
	}
	__finally
	{
		// 검사 판정
		OnJugdement_And_Report();

		// 에러 코드 처리
		OnPopupMessageTest_All((enResultCode_All)lReturn);		

		// 작업 종료 처리
		if (RCAF_OK == lReturn)
		{
			OnFinallyTest_All(TP_Completed);
			TRACE(_T("=-=-= 전체 Test 작업 완료 =-=-=\n"));
			OnLog(_T("=-=-= 전체 Test 작업 완료 =-=-="));
		}
		else
		{
			OnFinallyTest_All(TP_Stop);
			TRACE(_T("=-=-= 전체 Test 작업 중지 =-=-=\n"));
			OnLog(_T("=-=-= 전체 Test 작업 중지 =-=-="));
		}
	}

}

//=============================================================================
// Method		: StopProcess
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/5/26 - 10:17
// Desc.		:
//=============================================================================
void CTestManager_EQP::StopProcess(__in UINT nUnitIdx)
{
	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Unit[nUnitIdx], &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThrTest_Unit[nUnitIdx], dwExitCode);
			WaitForSingleObject(m_hThrTest_Unit[nUnitIdx], WAIT_ABANDONED);
			CloseHandle(m_hThrTest_Unit[nUnitIdx]);
			m_hThrTest_Unit[nUnitIdx] = NULL;
		}
	}
}

//=============================================================================
// Method		: StopProcess_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/10 - 21:14
// Desc.		:
//=============================================================================
void CTestManager_EQP::StopProcess_All()
{
	m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
	m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLamp_Buzzer, ON);

	OnTestProgress(TP_Error);
	m_stInspInfo.ModelInfo.nPicItem	 = PIC_Standby;
	

	m_bFlag_UseStop	= TRUE;

	for (UINT nIdx = 0; nIdx < TIID_MaxEnum; nIdx++)
		m_bFlag_TestItem[nIdx] = FALSE;
	
	//DoEvents(500);

	WaitForSingleObject(m_hThrTest_All, 100);

	for (UINT nIdx = 0; nIdx < MAX_OPERATION_THREAD; nIdx++)
		StopProcess(nIdx);

	if (NULL != m_hThrTest_All)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_All, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThrTest_All, dwExitCode);
			WaitForSingleObject(m_hThrTest_All, WAIT_ABANDONED);
			CloseHandle(m_hThrTest_All);
			m_hThrTest_All = NULL;
		}
	}

	ST_StepInfo* const pStepInfo = &m_stInspInfo.ModelInfo.stStepInfo;
	INT_PTR iStepCnt = m_stInspInfo.ModelInfo.stStepInfo.GetCount();

	switch (m_stInspInfo.CamInfo.nJudgment)
	{
	case TR_UserStop:
	case TR_EquipAlarm:

		for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
		{
			// TEST
			if (TRUE == pStepInfo->StepList[nStepIdx].bTestUse)
			{
				OnUpdateTestResult((enTestEachResult)m_stInspInfo.CamInfo.nJudgment, nStepIdx, g_szResultCode_All[m_stInspInfo.CamInfo.nJudgment]);
			}
		}
		break;
	}

	OnUpdateTestJudgment((enTestEachResult)m_stInspInfo.CamInfo.nJudgment);

	// Start Btn 활성화
	OnSetStartBtnStatus(TRUE);

	
	CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);

	float fVoltageOff[2] = { 0, 0 };
	SetLuriCamBrd_Volt(fVoltageOff);

}

//=============================================================================
// Method		: OnInitTest_All
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 10:25
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitialTest_All()
{
	

	// 테스트 상태 : Run	
	OnTestProgress(TP_Run);	
	RunLedChange(DO_LampRun);
	OnUpdateTResetResult();

	// Test 정보 초기화	
	m_stInspInfo.CamInfo.Reset();
	//m_stInspInfo.ModelInfo.Reset();

	// 투입 시간
	OnSetInputTime();

	// 채널 카메라 데이터 초기화
	m_stInspInfo.WorklistInfo.Reset();

	// 타워램프 RUN 중 표시
	m_Device.MotionSequence.TowerLamp(TowerLampYellow, ON);

	OnUpdateTestJudgment((enTestEachResult)TR_Testing);

	if (m_stInspInfo.PermissionMode == Permission_MES)
	{
		m_stInspInfo.MESInfo.szEqpCode = m_stInspInfo.szEqpCode;
		m_stInspInfo.MESInfo.szMESPath = m_stInspInfo.Path.szMes;

		if (m_stInspInfo.MESInfo.GetMESParameter(m_stInspInfo.szEqpCode))
		{
			OnChangeLotInfo(MESMode);
		}
	}

	// Start Btn 비활성화
	OnSetStartBtnStatus(FALSE);
}

//=============================================================================
// Method		: OnFinalTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: enTestProcess TestProc
// Qualifier	:
// Last Update	: 2016/5/30 - 10:25
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFinallyTest_All(enTestProcess TestProc)
{
	// 테스트 상태 : 종료
	if ((TP_Stop == TestProc) || (TP_Completed))
	{
		GetLocalTime(&m_stInspInfo.Time.tmEnd_All);
		OnTestProgress(TestProc);

		//OnSetStatus_Button(FALSE, FALSE);
	}
}

//=============================================================================
// Method		: OnInitialTest_Unit
// Access		: virtual protected  
// Returns		: void nUnitIdx
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/6/12 - 15:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitialTest_Unit(__in UINT nUnitIdx)
{	
	OnTestProgress(TP_Run);
	GetLocalTime(&m_stInspInfo.Time.tmStart_Unit[nUnitIdx]);
	m_stInspInfo.Time.dwStart[nUnitIdx] = timeGetTime();

	//m_Site[nUnitIdx].OnSetTestStatus(m_stInspInfo.CamObject[nUnitIdx]->nProgressStatus);
	
}

//=============================================================================
// Method		: OnFinallyTest_Unit
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in BOOL bResult
// Qualifier	:
// Last Update	: 2016/6/12 - 15:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFinallyTest_Unit(__in UINT nUnitIdx, __in BOOL bResult)
{
	GetLocalTime(&m_stInspInfo.Time.tmEnd_Unit[nUnitIdx]);

	if (bResult)
	{
		//OnTestProgress_Unit(nUnitIdx, TP_Completed);
		//OnTestResult_Unit(nUnitIdx, TR_Pass);
	}
	else
	{
		//OnTestProgress_Unit(nUnitIdx, TP_Stop);
		//OnTestResult_Unit(nUnitIdx, TR_Fail);
	}
	//m_Site[nUnitIdx].OnSetTestStatus(m_stInspInfo.CamObject[nUnitIdx]->nProgressStatus);
	
	// 실제 테스트 시간 누적
	//m_stInspInfo.CamInfo[nUnitIdx].dwTestTime += m_stInspInfo.Time.dwDuration[nUnitIdx];

	// Site별 검사 시간
	if (1000 < m_stInspInfo.Time.dwDuration[nUnitIdx])
	{
		m_stInspInfo.CycleTime.AddTestTimeSite(nUnitIdx, m_stInspInfo.Time.dwDuration[nUnitIdx]);

		// 테스트 항목별 검사 시간??
	}
}
//=============================================================================
// Method		: SetAutoMasterMode
// Access		: public  
// Returns		: BOOL
// Parameter	: BOOL bMode
// Qualifier	:
// Last Update	: 2017/9/9 - 16:06
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::SetAutoMasterMode()
{
	// 검사 항목
	ST_StepInfo* const pStepInfo = &m_stInspInfo.ModelInfo.stStepInfo;
	INT_PTR iStepCnt = m_stInspInfo.ModelInfo.stStepInfo.GetCount();
	DWORD dwElapTime = 0;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// TEST가 선택되어있고, 광축이거나 로테이트 검사항목일때
		if (TRUE == pStepInfo->StepList[nStepIdx].bTestUse &&
		   (pStepInfo->StepList[nStepIdx].nTestItem == TIID_CenterPoint || pStepInfo->StepList[nStepIdx].nTestItem == TIID_Rotation
		   || pStepInfo->StepList[nStepIdx].nTestItem == TIID_TestInitialize || pStepInfo->StepList[nStepIdx].nTestItem == TIID_TestFinalize))
		{

			EachResult = StartTest_TestItem_Motion(pStepInfo->StepList[nStepIdx].nTestItem);

			if (EachResult == RCA_MachineCheck)
			{
				TotalResult = TR_MachineCheck;
				break;
			}

			DWORD dwDelay = pStepInfo->StepList[nStepIdx].dwDelay / 50;
			for (int i = 0; i < 50; i++)
			{
				DoEvents(dwDelay);
			}

			EachResult = StartTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestItemCnt, nStepIdx, pStepInfo->StepList[nStepIdx].nRetryCnt);

			if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
			{
				//Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
				DWORD dwDelay = pStepInfo->StepList[nStepIdx].dwDelay / 50;
				for (int i = 0; i < 50; i++)
				{
					DoEvents(dwDelay);
				}
			}

			if (EachResult == RCA_OpenShort)
			{
				TotalResult = TR_OpenShort;
				break;
			}

			if (EachResult == RCA_TransferErr)
			{
				TotalResult = TR_TransferErr;
				break;
			}

			if (EachResult == RCA_SensorDetect)
			{
				TotalResult = TR_SensorDetect;
				break;
			}

			if (EachResult == RCA_NoImage)
			{
				TotalResult = TR_NoImage;
				break;
			}
		}
	}

	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// TEST가 선택되어있고, 광축이거나 로테이트 검사항목일때
		if (TRUE == pStepInfo->StepList[nStepIdx].bTestUse &&
			(pStepInfo->StepList[nStepIdx].nTestItem == TIID_TestFinalize))
		{

			if (TotalResult != TR_MachineCheck)
			{
				EachResult = StartTest_TestItem_Motion(pStepInfo->StepList[nStepIdx].nTestItem);

				if (EachResult == RCA_MachineCheck)
				{
					TotalResult = TR_MachineCheck;
					break;
				}
			}

			DWORD dwDelay = pStepInfo->StepList[nStepIdx].dwDelay / 50;
			for (int i = 0; i < 50; i++)
			{
				DoEvents(dwDelay);
			}

			EachResult = StartTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestItemCnt, nStepIdx, pStepInfo->StepList[nStepIdx].nRetryCnt);

		}
	}

	//광축측정을 하지 못하였을경우
	if (abs(m_stInspInfo.ModelInfo.stCenterPoint->stCenterPointResult.iResultOffsetX) > 999 || abs(m_stInspInfo.ModelInfo.stCenterPoint->stCenterPointResult.iResultOffsetY) > 999)
	{
		OnSetResult(TER_Fail, _T("Master Set CenterPoint Fail"));
		return FALSE;
	}

	// 1. 광축 측정 값 - 마스터 카메라 정보 값, 로테이트
	int iResultX = abs(m_stInspInfo.ModelInfo.stCenterPoint->stCenterPointResult.iResultOffsetX) - abs(m_stInspInfo.ModelInfo.iMasterInfoX);
	int iResultY = abs(m_stInspInfo.ModelInfo.stCenterPoint->stCenterPointResult.iResultOffsetY) - abs(m_stInspInfo.ModelInfo.iMasterInfoY);
	double dbResultR = abs(m_stInspInfo.ModelInfo.stRotate->stRotateResult.dbValue) - abs(m_stInspInfo.ModelInfo.dbMasterInfoR);
	

	// 결과 출력
	if (abs(iResultX) > m_stInspInfo.ModelInfo.nMasterSpcX || abs(iResultY) > m_stInspInfo.ModelInfo.nMasterSpcY || abs(dbResultR) > m_stInspInfo.ModelInfo.dbMasterSpcR)
	{
		m_stInspInfo.CamInfo.nJudgment = TR_Fail;
		OnSetResult(TER_Fail, _T("Master Set Spc Over"));
		return FALSE;
	}
	else
	{
		m_stInspInfo.CamInfo.nJudgment = TR_Pass;
		OnSetResult(TER_Pass, _T("Master Set Pass"));
	}

	// 마스터 정보 값을 기준으로 옵셋값 환산
	int iOffsetX = (m_stInspInfo.ModelInfo.dwWidth / 2) - m_stInspInfo.ModelInfo.stCenterPoint->stCenterPointResult.iValueX;
	int iOffsetY = (m_stInspInfo.ModelInfo.dwHeight / 2) - m_stInspInfo.ModelInfo.stCenterPoint->stCenterPointResult.iValueY;

	double dbOffsetR = m_stInspInfo.ModelInfo.stRotate->stRotateResult.dbValue * (-1);

	// 옵셋값을 기준 점에 적용
	OnSetMasterOffSetData(iOffsetX, iOffsetY, dbOffsetR);


	return TRUE;
}
//=============================================================================
// Method		: OnStartTest_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: void
// Qualifier	:
// Last Update	: 2016/7/15 - 14:17
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStartTest_All()
{
	LRESULT lReturn = TR_Init;

	// 검사 항목
	ST_StepInfo* const pStepInfo = &m_stInspInfo.ModelInfo.stStepInfo;
	INT_PTR iStepCnt = m_stInspInfo.ModelInfo.stStepInfo.GetCount();
	DWORD dwElapTime = 0;

	UINT TotalResult = TR_Pass;
	UINT EachResult = TER_Init;

	if (m_stInspInfo.bUseMasterCheck == TRUE)
	{
		if (FALSE == m_stInspInfo.m_bMasterCheck)
		{
			TotalResult = TR_MasterCheck;
			lReturn = m_stInspInfo.CamInfo.nJudgment = (enTestResult)TotalResult;
			return lReturn;
		}
	}
	

	if (m_stInspInfo.PermissionMode == Permission_MES)
	{
		if (FALSE == m_stInspInfo.MESInfo.bStatus)
		{
			TotalResult = TR_NoBarcode;
			lReturn = m_stInspInfo.CamInfo.nJudgment = (enTestResult)TotalResult;
			return lReturn;
		}
	}

	if (FALSE == CheckBarCodeInfo())
	{
		TotalResult = TR_NoBarcode;
		lReturn = m_stInspInfo.CamInfo.nJudgment = (enTestResult)TotalResult;
		return lReturn;
	}

	if (FALSE == CheckPogoCount())
	{
		TotalResult = TR_PogoCountOver;
		lReturn = m_stInspInfo.CamInfo.nJudgment = (enTestResult)TotalResult;
		return lReturn;
	}

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// TEST
		if (TRUE == pStepInfo->StepList[nStepIdx].bTestUse)
		{
			// 중간에 항목이 NG 나면 스킵 처리한다.
			if (m_stInspInfo.CamInfo.nJudgmentProc == TR_Fail && pStepInfo->StepList[nStepIdx].nTestItem != TIID_TestFinalize)
			{
				OnUpdateTestResult(TER_SKIP, nStepIdx, g_szResultCode_All[TER_SKIP]);
			}
			else
			{
				EachResult = StartTest_TestItem_Motion(pStepInfo->StepList[nStepIdx].nTestItem);

				if (EachResult == RCA_MachineCheck)
				{
					TotalResult = TR_MachineCheck;
					break;
				}

				DWORD dwDelay = pStepInfo->StepList[nStepIdx].dwDelay;
				if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
				{
					DoEvents(dwDelay);
				}

				EachResult = StartTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestItemCnt, nStepIdx, pStepInfo->StepList[nStepIdx].nRetryCnt);
			}

			if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
			{
				//Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
				DWORD dwDelay = pStepInfo->StepList[nStepIdx].dwDelay;
				DoEvents(dwDelay);
			}

			if (EachResult == RCA_OpenShort)
			{
				TotalResult = TR_OpenShort;
				break;
			}

			if (EachResult == RCA_TransferErr)
			{
				TotalResult = TR_TransferErr;
				break;
			}

			if (EachResult == RCA_SensorDetect)
			{
				TotalResult = TR_SensorDetect;
				break;
			}

			if (EachResult == RCA_NoImage)
			{
				TotalResult = TR_NoImage;
				break;
			}
		}

		// 스텝 딜레이
		if (EachResult != RCA_OK)
		{
			lReturn = TotalResult = TR_Fail;
			m_stInspInfo.CamInfo.nJudgmentProc = TR_Fail;
		}

	}

	m_stInspInfo.CamInfo.nJudgment = (enTestResult)TotalResult;

	return lReturn;
}

//=============================================================================
// Method		: OnStartTest_All_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/4 - 14:28
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStartTest_All_TestItem(__in UINT nUnitIdx, __in UINT nTestItemID)
{
	LRESULT lReturn = RCAF_OK;

	//  개별 검사 쓰레드 실행
	HANDLE	hEventz[USE_CHANNEL_CNT] = { NULL, };
	UINT	nIndex = 0;

	// 채널별 검사 사용여부
	// 제품 초기화 상태이거나 검사 진행이 Pass 상태이면 다음 검사 계속 진행
	if ((TR_Pass == m_stInspInfo.CamInfo.nJudgment) || (TR_Init == m_stInspInfo.CamInfo.nJudgment))
	{
		if (StartOperation_Auto(nUnitIdx, nTestItemID))
		{
			hEventz[nIndex++] = m_hThrTest_Unit[nUnitIdx];
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnStartTest_Unit
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/4 - 14:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStartTest_Unit(__in UINT nUnitIdx, __in UINT nTestItemID)
{
	LRESULT lReturn = RCAF_OK;

// 	switch (nTestItemID)
// 	{
// 	case TIID_TestInitialize:	
// 		CameraTestInitialize(Initial);
// 		
// 		// 에러 코드
// 		if (m_stInspInfo.CamInfo.nJudgmentInitial != TER_Pass)
// 			lReturn = RCAF_PowerOn_Err;
// 		break;
// 
// 	case TIID_TestFinalize:		
// 		CameraTestInitialize(Finalize);
// 
// 		// 에러 코드
// 		if (m_stInspInfo.CamInfo.nJudgmentFinalize != TER_Pass)
// 			lReturn = RCAF_PowerOff_Err;
// 		break;
// 	default:
// 		break;
// 	}

	return lReturn;
}

//=============================================================================
// Method		: OnStartTest_Unit_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/8 - 16:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStartTest_Unit_TestItem(__in UINT nTestItemID)
{
	LRESULT lReturn = RCC_OK;

	for (UINT nIndex = 0; nIndex < USE_CHANNEL_CNT; nIndex++)
	{
		if (m_stInspInfo.bTestEnable[nIndex])
		{
			switch (nTestItemID)
			{
			case TIID_TestInitialize:
				// 
				break;

			case TIID_TestFinalize:
				//
				break;

			default:
				break;
			}
		}
	}
	return lReturn;
}

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode_All nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnPopupMessageTest_All(__in enResultCode_All nResultCode)
{
	if (RCAF_OK == nResultCode)
		return;

	OnLog_Err(g_szResultCode_AF[nResultCode]);
	OnTestErrCode(g_szResultCode_AF[nResultCode]);

}

//=============================================================================
// Method		: OnCheckSaftyJIG
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/13 - 21:33
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnCheckSaftyJIG()
{
	return TRUE;
}

//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnJugdement_And_Report()
{
	ST_StepInfo* const pStepInfo = &m_stInspInfo.ModelInfo.stStepInfo;
	INT_PTR iStepCnt = m_stInspInfo.ModelInfo.stStepInfo.GetCount();

	CString path = m_Worklist.Get_Report_Yield_FullPath(m_stInspInfo.Path.szReport, m_stInspInfo.szModelName, m_stInspInfo.LotInfo.szLotName, &m_stInspInfo.CamInfo.tmInputTime);

	// 최종 검사 판정 업데이트?
	switch (m_stInspInfo.CamInfo.nJudgment)
	{
	case TR_Fail:
		OnTestResult(TR_Fail);
		RunLedChange(DO_LampFail);

		m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
 		//m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLamp_Buzzer, ON);
		
		if (m_stInspInfo.LotInfo.bLotStatus == TRUE) //LOT 모드 일 경우에만 WORKLIST 저장
		{
			m_stInspInfo.YieldInfo.IncreaseFail();
			
			++m_stInspInfo.LotInfo.nLotCount;
			OnSetLotInfo();

			m_FileReport.SaveYield_LOT(path, &m_stInspInfo.YieldInfo);

			OnUpdateYield();

			OnChangWorklistData();
		}



// 		// Fail Box 루틴 추가.
		if (m_stInspInfo.LotInfo.bLotStatus == TRUE && m_stInspInfo.bUseFailBox == TRUE) //LOT 모드 일 경우에만 WORKLIST 저장
			OnPopupFailBoxConfirm();

		break;

	case TR_Pass:
		OnTestResult(TR_Pass);
		
		RunLedChange(DO_LampPass);
		m_Device.MotionSequence.TowerLamp(TowerLampGreen, ON);
		//m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLampBuzzer, ON);

		if (m_stInspInfo.LotInfo.bLotStatus == TRUE) //LOT 모드 일 경우에만 WORKLIST 저장
		{
			m_stInspInfo.YieldInfo.IncreasePass();
		
			++m_stInspInfo.LotInfo.nLotCount;
			OnSetLotInfo();

			m_FileReport.SaveYield_LOT(path, &m_stInspInfo.YieldInfo);

			OnUpdateYield();

			OnChangWorklistData();
		}
		
		//if (m_stInspInfo.LotInfo.bLotStatus == TRUE /*|| m_stInspInfo.PermissionMode == Permission_MES*/) //LOT 모드 일 경우에만 WORKLIST 저장
		//	if (m_stInspInfo.bUsePrinter == TRUE) // 라벨 프린터 출력
		//		OnPrinterOut(m_stInspInfo.YieldInfo.dwPass);

		break;

	case TR_NoBarcode:
	case TR_TransferErr:
	case TR_OpenShort:
	case TR_MachineCheck:
	case TR_MachineException:
	case TR_PogoCountOver:
	case TR_Empty:
	case TR_Timeout:
	case TR_NoImage:
	case TR_SensorDetect:
	case TR_MasterCheck:
		OnTestResult(m_stInspInfo.CamInfo.nJudgment);
		RunLedChange(DO_LampFail);
		for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
		{
			// TEST
			if (TRUE == pStepInfo->StepList[nStepIdx].bTestUse)
			{
				OnUpdateTestResult((enTestEachResult)m_stInspInfo.CamInfo.nJudgment, nStepIdx, g_TestResult[m_stInspInfo.CamInfo.nJudgment].szText);
			}
		}
		OnChangWorklistData();

		OnFunc_Motion_Initial();
		break;

	case TR_UserStop:
	case TR_EquipAlarm:
	case TR_Testing:
	case TR_Skip:
	case TR_Init:
		break;

	default:
		return;
		break;
	}

	
	CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);

	float fVoltageOff[2] = { 0, 0 };
	SetLuriCamBrd_Volt(fVoltageOff);

	OnUpdateTestJudgment((enTestEachResult)m_stInspInfo.CamInfo.nJudgment);

	m_stInspInfo.ModelInfo.nPicItem = PIC_Standby;

	// Start Btn 활성화
	OnSetStartBtnStatus(TRUE);
}

//=============================================================================
// Method		: OnSetCycleTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/14 - 14:28
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSetCycleTime()
{
	m_stInspInfo.CycleTime.IncreaseOutputCount();

	if (0 < m_stInspInfo.dwTactTime)
	{
		m_stInspInfo.CycleTime.AddTactTime(m_stInspInfo.dwTactTime);
	}

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		if (m_stInspInfo.bTestEnable[nIdx])
		{
			m_stInspInfo.CycleTime.AddCycleTime(m_stInspInfo.dwTactTime);
		}
	}

	// UI 갱신
}

//=============================================================================
// Method		: OnResetCamInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/7/15 - 14:30
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnResetCamInfo()
{
	m_stInspInfo.ResetCamInfo();
}

//=============================================================================
// Method		: CheckModelInfo
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/7/21 - 11:48
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckModelInfo()
{
	enPermissionMode enAcessMode = GetPermissionMode();

	if (enAcessMode != Permission_Operator)
		return TRUE;

	if (m_stInspInfo.ModelInfo.szModelFile.IsEmpty())
	{
		MessageView(g_szResultCode_AF[RCAF_Model_Empty_Err]);
		OnLog_Err(_T("Not Model Check."));
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckLOTInfo
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/21 - 16:40
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckLOTInfo()
{
	enPermissionMode enAcessMode = GetPermissionMode();

	if (enAcessMode != Permission_Operator)
		return TRUE;

	if (m_stInspInfo.LotInfo.szLotName.IsEmpty())
	{
		MessageView(g_szResultCode_AF[RCAF_LotID_Empty_Err]);
		OnLog_Err(_T("Not Lot ID Check."));
		return FALSE;
	}

	if (m_stInspInfo.LotInfo.szOperator.IsEmpty())
	{
		MessageView(g_szResultCode_AF[RCAF_Operator_Empty_Err]);
		OnLog_Err(_T("Not Operator Check."));
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckBarCodeInfo
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/21 - 16:48
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckBarCodeInfo()
{
	enPermissionMode enAcessMode = GetPermissionMode();

	if (enAcessMode != Permission_Operator)
	{
		m_stInspInfo.CamInfo.szBarcode = _T("Administrator Mode");
		OnSetInsertBarcode(m_stInspInfo.CamInfo.szBarcode);
		return TRUE;
	}

	// 바코드 설정

 	if (m_stInspInfo.bUseBarcode == TRUE)
	{
		if (m_stInspInfo.szBarcodeBuf.IsEmpty())
		{
			//MessageView(g_szResultCode_AF[RCAF_Barcode_Empty_Err]);
			OnLog_Err(_T("Not Barcode Check."));
			return FALSE;
		}
		else
		{
			m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.szBarcodeBuf;
			OnSetInsertBarcode(m_stInspInfo.CamInfo.szBarcode);
			m_stInspInfo.szBarcodeBuf.Empty();
		}
	}
	else
	{
		m_stInspInfo.CamInfo.szBarcode = _T("Empty");
		OnSetInsertBarcode(m_stInspInfo.CamInfo.szBarcode);
	}

	

	return TRUE;
}

//=============================================================================
// Method		: CheckEASCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/11/8 - 11:20
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckEASCount()
{
	return TRUE;
}


BOOL CTestManager_EQP::CheckJigFix()
{
	BOOL bResult = FALSE;

	if (m_Device.DigitalIOCtrl.AXTState() == TRUE)
	{
		//bResult = m_Device.DigitalIOCtrl.SetOutPort(DO_FixCylinder)
	}

	if (bResult == FALSE)
	{
		MessageView(_T("The Product was not fixed."));
	}

	return bResult;
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 14:20
// Desc.		:
//=============================================================================
void CTestManager_EQP::CreateTimer_UpdateUI()
{
	__try
	{
		if (NULL == m_hTimer_Update_UI)
		if (!CreateTimerQueueTimer(&m_hTimer_Update_UI, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine_UpdateUI, (PVOID)this, 5000, 100, WT_EXECUTEDEFAULT))
		{
			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTestManager_Base::CreateTimer_UpdateUI()\n"));
	}
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnMonitor_TimeCheck()
{
	m_dwTimeCheck = timeGetTime();

	// 전체 검사 시간 체크
	if (TP_Run == m_stInspInfo.GetTestStatus())
	{
		if (m_stInspInfo.Time.dwStart_All < m_dwTimeCheck)
			m_stInspInfo.Time.dwDuration_All = m_dwTimeCheck - m_stInspInfo.Time.dwStart_All;
		else
			m_stInspInfo.Time.dwDuration_All = 0xFFFFFFFF - m_stInspInfo.Time.dwStart_All + m_dwTimeCheck;

		OnUpdateElapsedTime_All(m_stInspInfo.Time.dwDuration_All);
	}
}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Digital In 신호 체크
//=============================================================================
void CTestManager_EQP::OnMonitor_UpdateUI()
{
// 	if (m_bExitFlag == TRUE)
// 		return;
// 
// 	if (USE_TEST_MODE)
// 	{
// 		for (UINT nIdx = 0; nIdx < Indicator_Max; nIdx++)
// 		{
// 			if (m_bFlag_Indicator[nIdx] == TRUE && b_IndicatorConnect[nIdx] == TRUE)
// 				m_Device.Indicator[nIdx].Send_ReadIndicatorData(IndicatorSensor::Cmd_Real_Data, m_fValue[nIdx]);
// 
// 			if (b_IndicatorConnect[nIdx] == TRUE)
// 				OnSetIndicatorData(m_fValue[IndicatorX], m_fValue[IndicatorY]);
// 		}
// 
// 		if (m_Device.DigitalIOCtrl.AXTState() == TRUE)
// 		{
// 			for (BYTE nBitOffset = 0; nBitOffset < DI_NotUseBit_Max; nBitOffset++)
// 			{
// 				BOOL bStatus = m_Device.DigitalIOCtrl.Get_DI_Status(nBitOffset);
// 
// 				if (bStatus != m_stInspInfo.bDigitalIn[nBitOffset])
// 				{
// 					m_stInspInfo.bDigitalIn[nBitOffset] = bStatus;
// 					OnDetectDigitalInSignal(nBitOffset, bStatus);
// 				}
// 			}
// 		}
// 	}
}

//=============================================================================
// Method		: OnTestProgress
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestProcess nProcess
// Qualifier	:
// Last Update	: 2016/5/30 - 10:25
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnTestProgress(__in enTestProcess nProcess)
{
	m_stInspInfo.SetTestStatus(nProcess);
	m_stInspInfo.CamInfo.nProgressStatus = nProcess;
}

void CTestManager_EQP::OnTestFailResultCode_Unit(__in UINT nUnitIdx, __in enResultCode_TestItem nFailCode)
{
	//m_stInspInfo.CamObject[nUnitIdx].RomWriteCode = nFailCode;
}

//=============================================================================
// Method		: OnTestResult
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestResult nResult
// Qualifier	:
// Last Update	: 2017/2/21 - 17:10
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnTestResult(__in enTestResult nResult)
{
	m_stInspInfo.CamInfo.nJudgment = nResult;
	m_stInspInfo.SiteInfo.SetResult(nResult);
}

//=============================================================================
// Method		: OnSetInputTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/2 - 13:16
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSetInputTime()
{
	m_stInspInfo.SetInputTime();
}

//=============================================================================
// Method		: IncreasePogoCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/7/21 - 11:27
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IncreasePogoCount()
{
	OnSetStatus_PogoCount();
	
	return SavePogoCount();
}

//=============================================================================
// Method		: SavePogoCount
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in enPogoCntIndex nPogoIndex
// Qualifier	:
// Last Update	: 2016/6/22 - 14:52
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::SavePogoCount()
{
	CFile_Model	m_fileModel;
	CString		strFullPath;
	CString		strLog;
	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szPogo, m_stInspInfo.ModelInfo.szPogoName, POGO_FILE_EXT);

	if (FALSE == m_fileModel.SavePogoCount(strFullPath, 0, m_stInspInfo.PogoInfo.dwCount[0]))
	{
		strLog.Format(_T("Pogo INI File Save Fail."));
		OnLog_Err(strLog);
		MessageView(strLog);

		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: LoadPogoCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/20 - 17:13
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::LoadPogoCount()
{
	CFile_Model	m_fileModel;
	CString		strFullPath;
	CString		strLog;
	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szPogo, m_stInspInfo.ModelInfo.szPogoName, POGO_FILE_EXT);

	if (FALSE == m_fileModel.LoadPogoIniFile(strFullPath, m_stInspInfo.PogoInfo))
	{
		strLog.Format(_T("Cable File Can Not Load."));
		OnLog_Err(strLog);
		MessageView(strLog);
		
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckPogoCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/21 - 16:42
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckPogoCount()
{
	// POGO 카운트 값이 최대치에 도달했으면 경고 창 팝업

// 	if (m_stInspInfo.PogoInfo.IsDateMaxCount(0))
// 	{
// 		return FALSE;
// 	}

	if (m_stInspInfo.PogoInfo.IsMaxCount(0))
	{	
		//MessageView(_T("Pogo Count Max.\r\n Engineer Call."));
		return FALSE;
	}

	return TRUE;
}


BOOL CTestManager_EQP::LoadRefBarcode()
{
	CFile_Model	m_fileModel;

	CString szBarcodeOpPath;
	szBarcodeOpPath.Format(_T("%s\\refBarcode.ini"), m_stInspInfo.Path.szBarcodeInfo);

	if (!m_fileModel.Load_RefBarcodeInfo(szBarcodeOpPath, m_stInspInfo.stBarcodeInfo))
	{
		return FALSE;
	}

	return TRUE;
}


BOOL CTestManager_EQP::LoadOperatorInfo()
{
	CFile_Model	m_fileModel;

	CString szUserConfigPath;
	szUserConfigPath.Format(_T("%s\\OperatorConfig.ini"), m_stInspInfo.Path.szOperatorInfo);

	if (!m_fileModel.Load_UserConfigInfo(szUserConfigPath, m_stInspInfo.stUserConfigInfo))
	{
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitialize()
{
	//__super::OnInitialize();	

	CreateTimer_TimeCheck();
	CreateTimer_UpdateUI();
}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFinalize()
{
	//__super::OnFinalize();
	DeleteTimer_TimeCheck();
	DeleteTimer_UpdateUI();
}

//=============================================================================
// Method		: IsTesting
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/5/16 - 10:57
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting()
{
	if (NULL != m_hThrTest_All)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_All, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	for (UINT nUnitIdx = 0; nUnitIdx < MAX_OPERATION_THREAD; nUnitIdx++)
	{
		if (IsTesting_Unit(nUnitIdx))
			return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_All
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/9 - 11:52
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_All()
{
	if (NULL != m_hThrTest_All)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_All, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_Unit
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/5/16 - 10:57
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_Unit(__in UINT nUnitIdx)
{
	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Unit[nUnitIdx], &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_Exc
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nExcUnitIdx
// Qualifier	:
// Last Update	: 2016/6/30 - 16:59
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_Exc(__in UINT nExcUnitIdx)
{
	DWORD dwExitCode = NULL;
	GetExitCodeThread(m_hThrTest_All, &dwExitCode);
	if (STILL_ACTIVE == dwExitCode)
	{
		return TRUE;
	}

	for (UINT nUnitIdx = 0; nUnitIdx < MAX_OPERATION_THREAD; nUnitIdx++)
	{
		if (nUnitIdx != nExcUnitIdx)
		{
			if (IsTesting_Unit(nUnitIdx))
				return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsManagerMode
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/1/13 - 10:32
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsManagerMode()
{
	if (Permission_Manager == m_stInspInfo.PermissionMode)
		return TRUE;
	else
		return FALSE;
}

//=============================================================================
// Method		: GetPermissionMode
// Access		: public  
// Returns		: enPermissionMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
enPermissionMode CTestManager_EQP::GetPermissionMode()
{
	return m_stInspInfo.PermissionMode;
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	m_stInspInfo.PermissionMode = nAcessMode;
}
//=============================================================================
// Method		: SetOperateMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enOperateMode nOperMode
// Qualifier	:
// Last Update	: 2018/3/12 - 19:50
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetOperateMode(__in enOperateMode nOperMode)
{
	if (FALSE == IsTesting())
	{
		m_stInspInfo.OperateMode = nOperMode;
	}
}
//=============================================================================
// Method		: PermissionStatsView
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/13 - 22:29
// Desc.		:
//=============================================================================
void CTestManager_EQP::PermissionStatsView()
{
	;
}

//=============================================================================
// Method		: OnChangWorklistData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 15:21
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnChangWorklistData()
{
	// 	DATA MOVE
	for (int i = 0; i < TICnt_Current; i++)
		m_stInspInfo.CamInfo.stCurrent[i] = m_stInspInfo.ModelInfo.stCurrent[i];

	for (int i = 0; i < TICnt_CenterPoint; i++)
		m_stInspInfo.CamInfo.stCenterPoint[i] = m_stInspInfo.ModelInfo.stCenterPoint[i];

	for (int i = 0; i < TICnt_Rotation; i++)
		m_stInspInfo.CamInfo.stRotate[i] = m_stInspInfo.ModelInfo.stRotate[i];

	for (int i = 0; i < TICnt_EIAJ; i++)
		m_stInspInfo.CamInfo.stEIAJ[i] = m_stInspInfo.ModelInfo.stEIAJ[i];

	for (int i = 0; i < TICnt_FOV; i++)
		m_stInspInfo.CamInfo.stAngle[i] = m_stInspInfo.ModelInfo.stAngle[i];

	for (int i = 0; i < TICnt_Color; i++)
		m_stInspInfo.CamInfo.stColor[i] = m_stInspInfo.ModelInfo.stColor[i];

	for (int i = 0; i < TICnt_Reverse; i++)
		m_stInspInfo.CamInfo.stReverse[i] = m_stInspInfo.ModelInfo.stReverse[i];

	for (int i = 0; i < TICnt_ParticleManual; i++)
		m_stInspInfo.CamInfo.stParticleMA[i] = m_stInspInfo.ModelInfo.stParticleMA[i];

	//for (int i = 0; i < TICnt_LEDTest; i++)
	//	m_stInspInfo.CamInfo.stLED[i] = m_stInspInfo.ModelInfo.stLED[i];

	//for (int i = 0; i < TICnt_VideoSignal; i++)
	//	m_stInspInfo.CamInfo.stVideoSignal[i] = m_stInspInfo.ModelInfo.stVideoSignal[i];

	//for (int i = 0; i < TICnt_Reset; i++)
	//	m_stInspInfo.CamInfo.stReset[i] = m_stInspInfo.ModelInfo.stReset[i];

	//for (int i = 0; i < TICnt_OperationMode; i++)
	//	m_stInspInfo.CamInfo.stOperMode[i] = m_stInspInfo.ModelInfo.stOperMode[i];

	//for (int i = 0; i < TICnt_PatternNoise; i++)
	//	m_stInspInfo.CamInfo.stPatternNoise[i] = m_stInspInfo.ModelInfo.stPatternNoise[i];

	for (int i = 0; i < TICnt_BlackSpot; i++)
		m_stInspInfo.CamInfo.stParticle[i] = m_stInspInfo.ModelInfo.stBlackSpot[i];

	for (int i = 0; i < TICnt_IRFilter; i++)
		m_stInspInfo.CamInfo.stIRFilter[i] = m_stInspInfo.ModelInfo.stIRFilter[i];

	//for (int i = 0; i < TICnt_SFR; i++)
	//	m_stInspInfo.CamInfo.stSFR[i] = m_stInspInfo.ModelInfo.stSFR[i];

	for (int i = 0; i < TICnt_Brightness; i++)
		m_stInspInfo.CamInfo.stBrightness[i] = m_stInspInfo.ModelInfo.stBrightness[i];

	SaveFinalMesSystem();

	ST_StepInfo* const pStepInfo = &m_stInspInfo.ModelInfo.stStepInfo;
	INT_PTR iStepCnt = m_stInspInfo.ModelInfo.stStepInfo.GetCount();

	//// 이미지 파일 이동
	//CString szTempFolderPath;
	//BOOL bSave = FALSE;

	// 선택한 검사 항목 개수
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// TEST
		if (TRUE == pStepInfo->StepList[nStepIdx].bTestUse)
		{
			switch (pStepInfo->StepList[nStepIdx].nTestItem)
			{
			case TIID_TestInitialize:
				break;

			case TIID_TestFinalize:
				//m_WorklistPtr.pList_Total->InsertFullData(&m_stInspInfo.CamInfo);
				break;

			case TIID_Current:
				SaveCurrentMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;

			case TIID_CenterPoint:
				SaveCenterPointMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);

				//szTempFolderPath.FormSaveEIAJMesSystemat(_T("D:\\TempSaveImage\\%s"),
				//	g_szLT_TestItem_Name[pStepInfo->StepList[nStepIdx].nTestItem]);
				//bSave = TRUE;
				break;

			case TIID_Rotation:
				SaveRotateMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;

			case TIID_EIAJ:
				SaveEIAJMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;
			case TIID_FOV:
				SaveAngleMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;

			case TIID_Color:
				SaveColorMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;

			case TIID_Reverse:
				SaveReverseMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;

			case TIID_BlackSpot:
				SaveParticleMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;
			case TIID_DefectPixel:
				SaveDefectPixelMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
					break;
			case TIID_ParticleManual:
				SaveParticleManualMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;

			case TIID_Brightness:
				SaveBrightnessMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;

			case TIID_IRFilter:
				SaveIRFilterMesSystem(pStepInfo->StepList[nStepIdx].nTestItemCnt);
				break;

			default:
				break;
			}

			//if (bSave)
			//{
			//	CString szImageSrcFilePath;
			//	szImageSrcFilePath.Format(_T("%s\\%04d_%02d_%02d_%02d_%02d_%02d"),
			//		szTempFolderPath,
			//		m_stInspInfo.CamInfo.tmInputTime.wYear,
			//		m_stInspInfo.CamInfo.tmInputTime.wMonth,
			//		m_stInspInfo.CamInfo.tmInputTime.wDay,
			//		m_stInspInfo.CamInfo.tmInputTime.wHour,
			//		m_stInspInfo.CamInfo.tmInputTime.wMinute,
			//		m_stInspInfo.CamInfo.tmInputTime.wSecond
			//	);

			//	CString szImageSrcFile = szImageSrcFilePath + _T(".bmp");

			//	//MakeDirectory(m_stInspInfo.CamInfo.szReportFilePath);

			//	CString szImageDstFilePath;
			//	szImageDstFilePath.Format(_T("%s\\%04d_%02d_%02d_%02d_%02d_%02d"),
			//		m_stInspInfo.CamInfo.szReportFilePath,
			//		m_stInspInfo.CamInfo.tmInputTime.wYear,
			//		m_stInspInfo.CamInfo.tmInputTime.wMonth,
			//		m_stInspInfo.CamInfo.tmInputTime.wDay,
			//		m_stInspInfo.CamInfo.tmInputTime.wHour,
			//		m_stInspInfo.CamInfo.tmInputTime.wMinute,
			//		m_stInspInfo.CamInfo.tmInputTime.wSecond
			//	);
			//	CString szImageDstFile = szImageDstFilePath + _T(".bmp");

			//	::MoveFile(szImageSrcFile, szImageDstFile);
			//	::DeleteFile(szImageSrcFile);
			//}
		}
	}
}

CString CTestManager_EQP::MakeTestItem(__in ST_CamInfo* pstCamInfo)
{
	CString szData;
	CString szItem;

	// Current
	for (int i = 0; i < TICnt_Current; i++)
	{
		for (int iCh = 0; iCh < 2; iCh++)
		{
			szItem.Format(_T("%d"), pstCamInfo->stCurrent[i].stCurrentResult.iValue[iCh]);
			szData += szItem;
			szData += _T(",");
		}
	}

// 	// Center Point
// 	for (int i = 0; i < TICnt_CenterPoint; i++)
// 	{
// 		szItem.Format(_T("%d"), pstCamInfo->stCenterPoint[i].stCenterPointResult.iValueX);
// 		szData += szItem;
// 		szData += _T(",");
// 
// 		szItem.Format(_T("%d"), pstCamInfo->stCenterPoint[i].stCenterPointResult.iValueY);
// 		szData += szItem;
// 		szData += _T(",");
// 	}
// 
// 	// Rotation
// 	for (int i = 0; i < TICnt_Rotation; i++)
// 	{
// 		szItem.Format(_T("%.2f"), pstCamInfo->stRotate[i].stRotateResult.dbValue);
// 		szData += szItem;
// 		szData += _T(",");
// 	}
// 
// 	// Color
// 	for (int i = 0; i < TICnt_Color; i++)
// 	{
// 		for (int iRoi = 0; iRoi < ROI_CL_Max; iRoi++)
// 		{
// 			if (iRoi == ROI_CL_R)
// 			{
// 				szItem.Format(_T("%d"), pstCamInfo->stColor[i].stColorResult.iRed[iRoi]);
// 				szData += szItem;
// 				szData += _T(",");
// 			}
// 			else if (iRoi == ROI_CL_G)
// 			{
// 				szItem.Format(_T("%d"), pstCamInfo->stColor[i].stColorResult.iGreen[iRoi]);
// 				szData += szItem;
// 				szData += _T(",");
// 			}
// 			else if (iRoi == ROI_CL_B)
// 			{
// 				szItem.Format(_T("%d"), pstCamInfo->stColor[i].stColorResult.iBlue[iRoi]);
// 				szData += szItem;
// 				szData += _T(",");
// 			}
// 			else
// 			{
// 				szItem.Format(_T("%d"), pstCamInfo->stColor[i].stColorResult.iRed[iRoi]);
// 				szData += szItem;
// 				szData += _T(",");
// 
// 				szItem.Format(_T("%d"), pstCamInfo->stColor[i].stColorResult.iGreen[iRoi]);
// 				szData += szItem;
// 				szData += _T(",");
// 
// 				szItem.Format(_T("%d"), pstCamInfo->stColor[i].stColorResult.iBlue[iRoi]);
// 				szData += szItem;
// 				szData += _T(",");
// 			}
// 		}
// 	}
// 
// 	// Angle
// 	for (int i = 0; i < TICnt_FOV; i++)
// 	{
// 		for (int iRoi = 0; iRoi < IDX_AL_Max; iRoi++)
// 		{
// 			szItem.Format(_T("%.2f"), pstCamInfo->stAngle[i].stAngleResult.dValue[iRoi]);
// 			szData += szItem;
// 			szData += _T(",");
// 		}
// 	}
// 
// 	// SFR 
// 	for (int i = 0; i < TICnt_SFR; i++)
// 	{
// 		for (int iRoi = 0; iRoi < ROI_SFR_Max; iRoi++)
// 		{
// 			szItem.Format(_T("%.2f"), pstCamInfo->stSFR[i].stSFRResult.dbValue[iRoi]);
// 			szData += szItem;
// 			szData += _T(",");
// 		}
// 	}
// 
// 	// Particle
// 	for (int i = 0; i < TICnt_BlackSpot; i++)
// 	{
// 		szItem.Format(_T("%d"), pstCamInfo->stBlackSpot[i].stParticleResult.nFailCount);
// 		szData += szItem;
// 		szData += _T(",");
// 	}
// 
// 	// Brightness
// 	for (int i = 0; i < TICnt_Brightness; i++)
// 	{
// 		for (int iRoi = ROI_BR_Side_1; iRoi < ROI_BR_Max; iRoi++)
// 		{
// 			szItem.Format(_T("%.2f"), pstCamInfo->stBrightness[i].stBrightnessResult.dbValue[iRoi]);
// 			szData += szItem;
// 			szData += _T(",");
// 		}
// 	}
// 
// 	// Sound 
// 	for (int i = 0; i < TICnt_FFT; i++)
// 	{
// 		szItem.Format(_T("%.2f"), pstCamInfo->stFFT[i].stFFTResult.dValue[FFTCh1]);
// 		szData += szItem;
// 		szData += _T(",");
// 	}

	return szData;
}

//=============================================================================
// Method		: SaveCurrentMesSystem
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/22 - 15:30
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::SaveCurrentMesSystem(__in UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode = m_Worklist.GetStationName();
	//- Model
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;

	//- Cam Type
	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_Current[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_Current[nTestItemCnt]->GetData(m_WorklistPtr.pList_Current[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	//- Result
	m_MesData.Result = Data[Curr_W_Result];

	// 결과
	for (UINT nIdx = 0; nIdx < Curr_W_MaxCol - Curr_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[Curr_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < Curr_W_MaxCol - Curr_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_Curr_Worklist[Curr_W_Result + nIdx + 1]);

	CString szTestItem;

	if (nTestItemCnt > 0)
	{
		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_Current], g_szLT_TestItemCurrent[nTestItemCnt]);
	}
	else
	{
		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_Current]);
	}

	CString szResult;



	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}


	// ImageNext MES File Create
// 	{
// 		CFile_Report fileMode;
// 		fileMode.SaveMES_Report(m_stInspInfo.Path.szMes, &m_stInspInfo.CamInfo);
// 	}

	return TRUE;
}


BOOL CTestManager_EQP::SaveOperationModeMesSystem(__in UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	////- 시간
	//m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	////- Equipment
	//m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
	////- Model
	//m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	////- SW Ver
	//m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	////- LOT
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	//else
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	////- Operator
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	//else
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	////- Barcode
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	//else
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	////- Cam Type
	//m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	////- Result + Data
	//UINT DataNum = 0;
	//CString Data[100];

	//CString strIndex;
	//strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	//m_stInspInfo.CamInfo.szIndex = strIndex;

	//m_WorklistPtr.pList_OperMode[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	//m_WorklistPtr.pList_OperMode[nTestItemCnt]->GetData(m_WorklistPtr.pList_OperMode[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	////-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	////- Result
	//m_MesData.Result = Data[OperMode_W_Result];

	//// 결과
	//for (UINT nIdx = 0; nIdx < OperMode_W_MaxCol - OperMode_W_Result; nIdx++)
	//	m_MesData.Itemz.Add(Data[OperMode_W_Result + nIdx + 1]);

	//// Data에 대한 헤더 입력
	//for (UINT nIdx = 0; nIdx < OperMode_W_MaxCol - OperMode_W_Result; nIdx++)
	//	m_MesData.ItemHeaderz.Add(g_lpszHeader_OperMode_Worklist[OperMode_W_Result + nIdx + 1]);

	//CString szTestItem;

	//if (nTestItemCnt > 0)
	//{
	//	szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_OperationMode], g_szLT_TestItemOperationMode[nTestItemCnt]);
	//}
	//else
	//{
	//	szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_OperationMode]);
	//}

	//CString szResult;

	//if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	//{
	//	szResult = _T("PASS");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}
	//else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	//{
	//	szResult = _T("FAIL");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}

	return TRUE;
}

BOOL CTestManager_EQP::SaveEIAJMesSystem(__in UINT nTestItemCnt)
{
 	ST_MES_TestItemLog m_MesData;
 
 	//- 시간
 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
 	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode = m_Worklist.GetStationName();
 	//- Model
 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
 	//- SW Ver
 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
 	//- LOT
 	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;
 	//- Operator
 	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;
 	//- Barcode
 	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
 	//- Cam Type
 	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
 
 	//- Result + Data
 	UINT DataNum = 0;
 	CString Data[100];
 
 	CString strIndex;
 	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
 	m_stInspInfo.CamInfo.szIndex = strIndex;
 
 	m_WorklistPtr.pList_EIAJ[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
 	m_WorklistPtr.pList_EIAJ[nTestItemCnt]->GetData(m_WorklistPtr.pList_EIAJ[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
 
 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
 	//- Result
 	m_MesData.Result = Data[EIAJ_W_Result];
 
 	// 결과
 	for (UINT nIdx = 0; nIdx < EIAJ_W_MaxCol - EIAJ_W_Result; nIdx++)
 		m_MesData.Itemz.Add(Data[EIAJ_W_Result + nIdx + 1]);
 
 	// Data에 대한 헤더 입력
 	for (UINT nIdx = 0; nIdx < EIAJ_W_MaxCol - EIAJ_W_Result; nIdx++)
 		m_MesData.ItemHeaderz.Add(g_lpszHeader_EIAJ_Worklist[EIAJ_W_Result + nIdx + 1]);
 
 	CString szTestItem;
 	if (nTestItemCnt > 0)
 	{
 		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_EIAJ], g_szLT_TestItemEIAJ[nTestItemCnt]);
 	}
 	else
 	{
 		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_EIAJ]);
 	}
 
 	CString szResult;
 
 	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
 	{
 		szResult = _T("PASS");
 		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
 	}
 	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
 	{
 		szResult = _T("FAIL");
 		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
 	}
	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);

	return TRUE;
}



BOOL CTestManager_EQP::SaveSFRMesSystem(__in UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	////- 시간
	//m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	////- Equipment
	//m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
	////- Model
	//m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	////- SW Ver
	//m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	////- LOT
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	//else
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	////- Operator
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	//else
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	////- Barcode
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	//else
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	////- Cam Type
	//m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	////- Result + Data
	//UINT DataNum = 0;
	//CString Data[100];

	//CString strIndex;
	//strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	//m_stInspInfo.CamInfo.szIndex = strIndex;

	//m_WorklistPtr.pList_SFR[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	//m_WorklistPtr.pList_SFR[nTestItemCnt]->GetData(m_WorklistPtr.pList_SFR[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	////-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	////- Result
	//m_MesData.Result = Data[SFR_W_Result];

	//// 결과
	//for (UINT nIdx = 0; nIdx < SFR_W_MaxCol - SFR_W_Result; nIdx++)
	//	m_MesData.Itemz.Add(Data[SFR_W_Result + nIdx + 1]);

	//// Data에 대한 헤더 입력
	//for (UINT nIdx = 0; nIdx < SFR_W_MaxCol - SFR_W_Result; nIdx++)
	//	m_MesData.ItemHeaderz.Add(g_lpszHeader_SFR_Worklist[SFR_W_Result + nIdx + 1]);


	//CString szTestItem;
	//if (nTestItemCnt > 0)
	//{
	//	szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_SFR], g_szLT_TestItemSFR[nTestItemCnt]);
	//}
	//else
	//{
	//	szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_SFR]);
	//}

	//CString szResult;

	//if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	//{
	//	szResult = _T("PASS");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}
	//else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	//{
	//	szResult = _T("FAIL");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}

	return TRUE;
}


BOOL CTestManager_EQP::SaveReverseMesSystem(__in UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode = m_Worklist.GetStationName();
	//- Model
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	//- Cam Type
	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_Reverse[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_Reverse[nTestItemCnt]->GetData(m_WorklistPtr.pList_Reverse[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	//- Result
	m_MesData.Result = Data[Reverse_W_Result];

	// 결과
	for (UINT nIdx = 0; nIdx < Reverse_W_MaxCol - Reverse_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[Reverse_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < Reverse_W_MaxCol - Reverse_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_Reverse_Worklist[Reverse_W_Result + nIdx + 1]);

	CString szTestItem;
	if (nTestItemCnt > 0)
	{
		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_Reverse], g_szLT_TestItemReverse[nTestItemCnt]);
	}
	else
	{
		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_Reverse]);
	}

	CString szResult;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}

	return TRUE;
}


BOOL CTestManager_EQP::SaveParticleManualMesSystem(__in UINT nTestItemCnt)
{
 	ST_MES_TestItemLog m_MesData;
 
 	//- 시간
 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
 	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode = m_Worklist.GetStationName();
 	//- Model
 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
 	//- SW Ver
 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
 	//- Cam Type
 	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
 
 	//- Result + Data
 	UINT DataNum = 0;
 	CString Data[100];
 
 	CString strIndex;
 	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
 	m_stInspInfo.CamInfo.szIndex = strIndex;
 
 	m_WorklistPtr.pList_ParticleManual[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
 	m_WorklistPtr.pList_ParticleManual[nTestItemCnt]->GetData(m_WorklistPtr.pList_ParticleManual[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
 
 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
 	//- Result
 	m_MesData.Result = Data[ParticleManual_W_Result];
 
 	// 결과
 	for (UINT nIdx = 0; nIdx < ParticleManual_W_MaxCol - ParticleManual_W_Result; nIdx++)
 		m_MesData.Itemz.Add(Data[ParticleManual_W_Result + nIdx + 1]);
 
 	// Data에 대한 헤더 입력
 	for (UINT nIdx = 0; nIdx < ParticleManual_W_MaxCol - ParticleManual_W_Result; nIdx++)
 		m_MesData.ItemHeaderz.Add(g_lpszHeader_ParticleManual_Worklist[ParticleManual_W_Result + nIdx + 1]);
 
 	CString szTestItem;

	if (nTestItemCnt > 0)
	{
		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_ParticleManual], g_szLT_TestItemParticleManual[nTestItemCnt]);
	}
	else
	{
		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_ParticleManual]);
	}

	CString szResult;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}

	return TRUE;
}


BOOL CTestManager_EQP::SaveParticleMesSystem(__in UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode = m_Worklist.GetStationName();
	//- Model
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	//- Cam Type
	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_Particle[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_Particle[nTestItemCnt]->GetData(m_WorklistPtr.pList_Particle[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	//- Result
	m_MesData.Result = Data[Par_W_Result];

	// 결과
	for (UINT nIdx = 0; nIdx < Par_W_MaxCol - Par_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[Par_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < Par_W_MaxCol - Par_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_Par_Worklist[Par_W_Result + nIdx + 1]);

	CString szTestItem;

	if (nTestItemCnt > 0)
	{
		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_BlackSpot], g_szLT_TestItemBlackSpot[nTestItemCnt]);
	}
	else
	{
		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_BlackSpot]);
	}

	CString szResult;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}

	return TRUE;
}


BOOL CTestManager_EQP::SaveDefectPixelMesSystem(UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode = m_Worklist.GetStationName();
	//- Model
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	//- Cam Type
	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_DefectPixel[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_DefectPixel[nTestItemCnt]->GetData(m_WorklistPtr.pList_DefectPixel[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	//- Result
	m_MesData.Result = Data[Defect_W_Result];

	// 결과
	for (UINT nIdx = 0; nIdx < Defect_W_MaxCol - Defect_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[Defect_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < Defect_W_MaxCol - Defect_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_Par_Worklist[Defect_W_Result + nIdx + 1]);

	CString szTestItem;

	if (nTestItemCnt > 0)
	{
		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_DefectPixel], g_szLT_TestItemBlackSpot[nTestItemCnt]);
	}
	else
	{
		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_DefectPixel]);
	}

	CString szResult;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}

	return TRUE;
}

BOOL CTestManager_EQP::SaveLEDMesSystem(__in UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	////- 시간
	//m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	////- Equipment
	//m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
	////- Model
	//m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	////- SW Ver
	//m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	////- LOT
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	//else
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	////- Operator
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	//else
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	////- Barcode
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	//else
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	////- Cam Type
	//m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	////- Result + Data
	//UINT DataNum = 0;
	//CString Data[100];

	//CString strIndex;
	//strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	//m_stInspInfo.CamInfo.szIndex = strIndex;

	//m_WorklistPtr.pList_LED[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	//m_WorklistPtr.pList_LED[nTestItemCnt]->GetData(m_WorklistPtr.pList_LED[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	////-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	////- Result
	//m_MesData.Result = Data[LED_W_Result];

	//// 결과
	//for (UINT nIdx = 0; nIdx < LED_W_MaxCol - LED_W_Result; nIdx++)
	//	m_MesData.Itemz.Add(Data[LED_W_Result + nIdx + 1]);

	//// Data에 대한 헤더 입력
	//for (UINT nIdx = 0; nIdx < LED_W_MaxCol - LED_W_Result; nIdx++)
	//	m_MesData.ItemHeaderz.Add(g_lpszHeader_LED_Worklist[LED_W_Result + nIdx + 1]);

	//CString szTestItem;
	//if (nTestItemCnt > 0)
	//{
	//	szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_LEDTest], g_szLT_TestItemLED[nTestItemCnt]);
	//}
	//else
	//{
	//	szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_LEDTest]);
	//}
	//CString szResult;

	//if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	//{
	//	szResult = _T("PASS");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}
	//else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	//{
	//	szResult = _T("FAIL");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}
	return TRUE;
}


BOOL CTestManager_EQP::SaveVideoSignalMesSystem(__in UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	////- 시간
	//m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	////- Equipment
	//m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
	////- Model
	//m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	////- SW Ver
	//m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	////- LOT
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	//else
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	////- Operator
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	//else
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	////- Barcode
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	//else
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	////- Cam Type
	//m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	////- Result + Data
	//UINT DataNum = 0;
	//CString Data[100];

	//CString strIndex;
	//strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	//m_stInspInfo.CamInfo.szIndex = strIndex;

	//m_WorklistPtr.pList_VideoSignal[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	//m_WorklistPtr.pList_VideoSignal[nTestItemCnt]->GetData(m_WorklistPtr.pList_VideoSignal[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	////-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	////- Result
	//m_MesData.Result = Data[VideoSignal_W_Result];

	//// 결과
	//for (UINT nIdx = 0; nIdx < VideoSignal_W_MaxCol - VideoSignal_W_Result; nIdx++)
	//	m_MesData.Itemz.Add(Data[VideoSignal_W_Result + nIdx + 1]);

	//// Data에 대한 헤더 입력
	//for (UINT nIdx = 0; nIdx < VideoSignal_W_MaxCol - VideoSignal_W_Result; nIdx++)
	//	m_MesData.ItemHeaderz.Add(g_lpszHeader_VideoSignal_Worklist[VideoSignal_W_Result + nIdx + 1]);

	//CString szTestItem;
	//if (nTestItemCnt > 0)
	//{
	//	//szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_VideoSignal], g_szLT_TestItemVideoSignal[nTestItemCnt]);
	//}
	//else
	//{
	//	//szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_VideoSignal]);
	//}
	//CString szResult;

	//if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	//{
	//	szResult = _T("PASS");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}
	//else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	//{
	//	szResult = _T("FAIL");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}

	return TRUE;
}

BOOL CTestManager_EQP::SaveResetMesSystem(__in UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	////- 시간
	//m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	////- Equipment
	//m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
	////- Model
	//m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	////- SW Ver
	//m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	////- LOT
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	//else
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	////- Operator
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	//else
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	////- Barcode
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	//else
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	////- Cam Type
	//m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	////- Result + Data
	//UINT DataNum = 0;
	//CString Data[100];

	//CString strIndex;
	//strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	//m_stInspInfo.CamInfo.szIndex = strIndex;

	//m_WorklistPtr.pList_Reset[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	//m_WorklistPtr.pList_Reset[nTestItemCnt]->GetData(m_WorklistPtr.pList_Reset[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	////-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	////- Result
	//m_MesData.Result = Data[Reset_W_Result];

	//// 결과
	//for (UINT nIdx = 0; nIdx < Reset_W_MaxCol - Reset_W_Result; nIdx++)
	//	m_MesData.Itemz.Add(Data[Reset_W_Result + nIdx + 1]);

	//// Data에 대한 헤더 입력
	//for (UINT nIdx = 0; nIdx < Reset_W_MaxCol - Reset_W_Result; nIdx++)
	//	m_MesData.ItemHeaderz.Add(g_lpszHeader_Reset_Worklist[Reset_W_Result + nIdx + 1]);

	//CString szTestItem;
	//if (nTestItemCnt > 0)
	//{
	//	//szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_Reset], g_szLT_TestItemReset[nTestItemCnt]);
	//}
	//else
	//{
	//	//szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_Reset]);
	//}
	//CString szResult;

	//if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	//{
	//	szResult = _T("PASS");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}
	//else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	//{
	//	szResult = _T("FAIL");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}

	return TRUE;
}

BOOL CTestManager_EQP::SavePatternNoiseMesSystem(__in UINT nTestItemCnt)
{
 	ST_MES_TestItemLog m_MesData;
 //
 //	//- 시간
 //	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
 //	//- Equipment
 //	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
 //	//- Model
 //	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
 //	//- SW Ver
 //	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	////- LOT
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	//else
	//	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	////- Operator
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	//else
	//	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	////- Barcode
	//if (m_stInspInfo.PermissionMode == Permission_MES)
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	//else
	//	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
 //	//- Cam Type
 //	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
 //
 //	//- Result + Data
 //	UINT DataNum = 0;
 //	CString Data[100];
 //
 //	CString strIndex;
 //	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
 //	m_stInspInfo.CamInfo.szIndex = strIndex;
 //
 //	m_WorklistPtr.pList_PatternNoise[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
 //	m_WorklistPtr.pList_PatternNoise[nTestItemCnt]->GetData(m_WorklistPtr.pList_PatternNoise[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
 //
 //	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
 //	//- Result
 //	m_MesData.Result = Data[PatternNoise_W_Result];
 //
 //	// 결과
 //	for (UINT nIdx = 0; nIdx < PatternNoise_W_MaxCol - PatternNoise_W_Result; nIdx++)
 //		m_MesData.Itemz.Add(Data[PatternNoise_W_Result + nIdx + 1]);
 //
 //	// Data에 대한 헤더 입력
 //	for (UINT nIdx = 0; nIdx < PatternNoise_W_MaxCol - PatternNoise_W_Result; nIdx++)
 //		m_MesData.ItemHeaderz.Add(g_lpszHeader_PatternNoise_Worklist[PatternNoise_W_Result + nIdx + 1]);
 //
	//CString szTestItem;
	//if (nTestItemCnt > 0)
	//{
	//	szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_PatternNoise], g_szLT_TestItemPatternNoise[nTestItemCnt]);
	//}
	//else
	//{
	//	szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_PatternNoise]);
	//}
	//CString szResult;

	//if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	//{
	//	szResult = _T("PASS");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}
	//else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	//{
	//	szResult = _T("FAIL");
	//	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	//}

	return TRUE;
}


//BOOL CTestManager_EQP::SaveIRFilterManualMesSystem(__in UINT nTestItemCnt)
//{
// 	ST_MES_TestItemLog m_MesData;
// 
// 	//- 시간
// 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
// 	//- Equipment
// 	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
// 	//- Model
// 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
// 	//- SW Ver
// 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
// 	//- LOT
// 	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;
// 	//- Operator
// 	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;
// 	//- Barcode
// 	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
// 	//- Cam Type
// 	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
// 
// 	//- Result + Data
// 	UINT DataNum = 0;
// 	CString Data[100];
// 
// 	CString strIndex;
// 	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
// 	m_stInspInfo.CamInfo.szIndex = strIndex;
// 
// 	m_WorklistPtr.pList_IRFilterManual[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
// 	m_WorklistPtr.pList_IRFilterManual[nTestItemCnt]->GetData(m_WorklistPtr.pList_IRFilterManual[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
// 
// 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
// 	//- Result
// 	m_MesData.Result = Data[IRFilterManual_W_Result];
// 
// 	// 결과
// 	for (UINT nIdx = 0; nIdx < IRFilterManual_W_MaxCol - IRFilterManual_W_Result; nIdx++)
// 		m_MesData.Itemz.Add(Data[IRFilterManual_W_Result + nIdx + 1]);
// 
// 	// Data에 대한 헤더 입력
// 	for (UINT nIdx = 0; nIdx < IRFilterManual_W_MaxCol - IRFilterManual_W_Result; nIdx++)
// 		m_MesData.ItemHeaderz.Add(g_lpszHeader_IRFilterManual_Worklist[IRFilterManual_W_Result + nIdx + 1]);
// 
// 	CString szTestItem;
// 	szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_IRFilterManual], g_szLT_TestItemIRFilterManual[nTestItemCnt]);
// 	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
// 
// 	// ImageNext MES File Create
// 	{
// 		CFile_Report fileMode;
// 		fileMode.SaveMES_Report(m_stInspInfo.Path.szMes, &m_stInspInfo.CamInfo);
// 	}

//	return TRUE;
//}


BOOL CTestManager_EQP::SaveIRFilterMesSystem(__in UINT nTestItemCnt)
{
	ST_MES_TestItemLog m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode = m_Worklist.GetStationName();
	//- Model
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	//- Cam Type
	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_IRFilter[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_IRFilter[nTestItemCnt]->GetData(m_WorklistPtr.pList_IRFilter[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	//- Result
	m_MesData.Result = Data[IRFilter_W_Result];

	// 결과
	for (UINT nIdx = 0; nIdx < IRFilter_W_MaxCol - IRFilter_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[IRFilter_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < IRFilter_W_MaxCol - IRFilter_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_Curr_Worklist[IRFilter_W_Result + nIdx + 1]);

	CString szTestItem;

	if (nTestItemCnt > 0)
	{
		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_IRFilter], g_szLT_TestItemIRFilter[nTestItemCnt]);
	}
	else
	{
		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_IRFilter]);
	}

	CString szResult;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}

	return TRUE;
}

 
 BOOL CTestManager_EQP::SaveCenterPointMesSystem(__in UINT nTestItemCnt)
 {
 	ST_MES_TestItemLog m_MesData;
 
 	//- 시간
 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
 	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode = m_Worklist.GetStationName();
 	//- Model
 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
 	//- SW Ver
 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
 	//- Cam Type
 	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
 
 	//- Result + Data
 	UINT DataNum = 0;
 	CString Data[100];
 
 	CString strIndex;
 	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
 	m_stInspInfo.CamInfo.szIndex = strIndex;
 
 	m_WorklistPtr.pList_CenterPt[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
 	m_WorklistPtr.pList_CenterPt[nTestItemCnt]->GetData(m_WorklistPtr.pList_CenterPt[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
 
 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
 	//- Result
 	m_MesData.Result = Data[CP_W_Result];
 
 	// 결과
 	for (UINT nIdx = 0; nIdx < CP_W_MaxCol - CP_W_Result; nIdx++)
 		m_MesData.Itemz.Add(Data[CP_W_Result + nIdx + 1]);
 
 	// Data에 대한 헤더 입력
 	for (UINT nIdx = 0; nIdx < CP_W_MaxCol - CP_W_Result; nIdx++)
 		m_MesData.ItemHeaderz.Add(g_lpszHeader_CP_Worklist[CP_W_Result + nIdx + 1]);
 
 	CString szTestItem;

// 	if (nTestItemCnt > 0)
// 	{
// 		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_CenterPoint], g_szLT_TestItemCenterPt[nTestItemCnt]);
// 	}
// 	else
// 	{
// 		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_CenterPoint]);
// 	}

	szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_CenterPoint]);

	CString szResult;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
 
 	return TRUE;
 }
 
 //=============================================================================
 // Method		: SaveFinalMesSystem
 // Access		: public  
 // Returns		: BOOL
 // Qualifier	:
 // Last Update	: 2017/2/22 - 15:24
 // Desc.		:
 //=============================================================================
 BOOL CTestManager_EQP::SaveFinalMesSystem()
 {
	//ST_MES_TestItemLog m_MesData;
	 ST_MES_FinalResult m_MesData;

	 //- 시간
	 m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	 //- Equipment
	 m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode = m_Worklist.GetStationName();
	 //- Model
	 m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	 //- SW Ver
	 m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	 //- LOT
	 if (m_stInspInfo.PermissionMode == Permission_MES)
		 m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	 else
		 m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	 //- Operator
	 if (m_stInspInfo.PermissionMode == Permission_MES)
		 m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	 else
		 m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	 //- Barcode
	 if (m_stInspInfo.PermissionMode == Permission_MES)
		 m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	 else
		 m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	 //- Cam Type
	 m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	 //- Result + Data
	 UINT DataNum = 0;
	 CString Data[100];

	 CString strIndex;
	 strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	 m_stInspInfo.CamInfo.szIndex = strIndex;

	 m_WorklistPtr.pList_Total->InsertFullData(&m_stInspInfo.CamInfo);
	 m_WorklistPtr.pList_Total->GetData(m_WorklistPtr.pList_Total->GetItemCount() - 1, DataNum, Data);

	 //-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	 //- Result
	 m_MesData.Result = Data[Total_W_Result];

	 // 결과
	 for (UINT nIdx = 0; nIdx < Total_W_MaxCol - Total_W_Result; nIdx++)
		 m_MesData.Itemz.Add(Data[Total_W_Result + nIdx + 1]);

	 // Data에 대한 헤더 입력
	 for (UINT nIdx = 0; nIdx < Total_W_MaxCol - Total_W_Result; nIdx++)
		 m_MesData.ItemHeaderz.Add(g_lpszHeader_Total_Worklist[Total_W_Result + nIdx + 1]);

	 CString szTestItem;

	// if (nTestItemCnt > 0)
	// {
	//	 szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_Rotation], g_szLT_TestItemRotate[nTestItemCnt]);
	// }
	// else
	// {
	//	 szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_Rotation]);
	// }

	 CString szResult;

// 	 if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
// 	 {
// 		 szResult = _T("PASS");
// 		 m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
// 	 }
// 	 else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
// 	 {
// 		 szResult = _T("FAIL");
// 		 m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
// 	 }
	 m_Worklist.Save_TotalResult_List(m_stInspInfo.CamInfo.szReportFilePath, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData);
	 return TRUE;
 }

 
 BOOL CTestManager_EQP::SaveRotateMesSystem(__in UINT nTestItemCnt)
 {
 	ST_MES_TestItemLog m_MesData;
 
 	//- 시간
 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
 	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode;;
 	//- Model
 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
 	//- SW Ver
 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
 	//- Cam Type
 	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
 
 	//- Result + Data
 	UINT DataNum = 0;
 	CString Data[100];
 
 	CString strIndex;
 	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
 	m_stInspInfo.CamInfo.szIndex = strIndex;
 
 	m_WorklistPtr.pList_Rotate[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
 	m_WorklistPtr.pList_Rotate[nTestItemCnt]->GetData(m_WorklistPtr.pList_Rotate[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
 
 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
 	//- Result
 	m_MesData.Result = Data[Rota_W_Result];
 
 	// 결과
 	for (UINT nIdx = 0; nIdx < Rota_W_MaxCol - Rota_W_Result; nIdx++)
 		m_MesData.Itemz.Add(Data[Rota_W_Result + nIdx + 1]);
 
 	// Data에 대한 헤더 입력
 	for (UINT nIdx = 0; nIdx < Rota_W_MaxCol - Rota_W_Result; nIdx++)
 		m_MesData.ItemHeaderz.Add(g_lpszHeader_Rota_Worklist[Rota_W_Result + nIdx + 1]);
 
 	CString szTestItem;

	if (nTestItemCnt > 0)
	{
		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_Rotation], g_szLT_TestItemRotate[nTestItemCnt]);
	}
	else
	{
		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_Rotation]);
	}

	CString szResult;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
 
 	return TRUE;
 }

 
 
 BOOL CTestManager_EQP::SaveColorMesSystem(__in UINT nTestItemCnt)
 {
 	ST_MES_TestItemLog m_MesData;
 
 	//- 시간
 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
 	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode;;
 	//- Model
 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
 	//- SW Ver
 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
 	//- Cam Type
 	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
 
 	//- Result + Data
 	UINT DataNum = 0;
 	CString Data[100];
 
 	CString strIndex;
 	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
 	m_stInspInfo.CamInfo.szIndex = strIndex;
 
 	m_WorklistPtr.pList_Color[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
 	m_WorklistPtr.pList_Color[nTestItemCnt]->GetData(m_WorklistPtr.pList_Color[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
 
 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
 	//- Result
 	m_MesData.Result = Data[Col_W_Result];
 
 	// 결과
 	for (UINT nIdx = 0; nIdx < Col_W_MaxCol - Col_W_Result; nIdx++)
 		m_MesData.Itemz.Add(Data[Col_W_Result + nIdx + 1]);
 
 	// Data에 대한 헤더 입력
 	for (UINT nIdx = 0; nIdx < Col_W_MaxCol - Col_W_Result; nIdx++)
 		m_MesData.ItemHeaderz.Add(g_lpszHeader_Col_Worklist[Col_W_Result + nIdx + 1]);
 
 	CString szTestItem;
	if (nTestItemCnt > 0)
	{
		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_Color], g_szLT_TestItemColor[nTestItemCnt]);
	}
	else
	{
		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_Color]);
	}
	CString szResult;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
 
 	return TRUE;
 }
 
 
 BOOL CTestManager_EQP::SaveAngleMesSystem(__in UINT nTestItemCnt)
 {
 	ST_MES_TestItemLog m_MesData;
 
 	//- 시간
 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
 	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode;
 	//- Model
 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
 	//- SW Ver
 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
 	//- Cam Type
 	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
 
 	//- Result + Data
 	UINT DataNum = 0;
 	CString Data[100];
 
 	CString strIndex;
 	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
 	m_stInspInfo.CamInfo.szIndex = strIndex;
 
 	m_WorklistPtr.pList_Angle[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
 	m_WorklistPtr.pList_Angle[nTestItemCnt]->GetData(m_WorklistPtr.pList_Angle[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
 
 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가 
 	//- Result
 	m_MesData.Result = Data[Ang_W_Result];
 
 	// 결과
 	for (UINT nIdx = 0; nIdx < Ang_W_MaxCol - Ang_W_Result; nIdx++)
 		m_MesData.Itemz.Add(Data[Ang_W_Result + nIdx + 1]);
 
 	// Data에 대한 헤더 입력
 	for (UINT nIdx = 0; nIdx < Ang_W_MaxCol - Ang_W_Result; nIdx++)
 		m_MesData.ItemHeaderz.Add(g_lpszHeader_Ang_Worklist[Ang_W_Result + nIdx + 1]);
 
 	CString szTestItem;

	if (nTestItemCnt > 0)
	{
		szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_FOV], g_szLT_TestItemAngle[nTestItemCnt]);
	}
	else
	{
		szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_FOV]);
	}
	CString szResult;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	}
 
 	return TRUE;
 }
 
 BOOL CTestManager_EQP::SaveBrightnessMesSystem(__in UINT nTestItemCnt)
 {
	 ST_MES_TestItemLog m_MesData;

	 //- 시간
	 m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	 //- Equipment
	 m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = m_stInspInfo.szEqpCode;;
	 //- Model
	 m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
	 //- SW Ver
	 m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	 //- LOT
	 if (m_stInspInfo.PermissionMode == Permission_MES)
		 m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	 else
		 m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	 //- Operator
	 if (m_stInspInfo.PermissionMode == Permission_MES)
		 m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	 else
		 m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	 //- Barcode
	 if (m_stInspInfo.PermissionMode == Permission_MES)
		 m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	 else
		 m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
	 //- Cam Type
	 m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];

	 //- Result + Data
	 UINT DataNum = 0;
	 CString Data[100];

	 CString strIndex;
	 strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	 m_stInspInfo.CamInfo.szIndex = strIndex;

	 m_WorklistPtr.pList_Brightness[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
	 m_WorklistPtr.pList_Brightness[nTestItemCnt]->GetData(m_WorklistPtr.pList_Brightness[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);

	 //-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	 //- Result
	 m_MesData.Result = Data[Br_W_Result];

	 // 결과
	 for (UINT nIdx = 0; nIdx < Br_W_MaxCol - Br_W_Result; nIdx++)
		 m_MesData.Itemz.Add(Data[Br_W_Result + nIdx + 1]);

	 // Data에 대한 헤더 입력
	 for (UINT nIdx = 0; nIdx < Br_W_MaxCol - Br_W_Result; nIdx++)
		 m_MesData.ItemHeaderz.Add(g_lpszHeader_Br_Worklist[Br_W_Result + nIdx + 1]);

	 CString szTestItem;

	 if (nTestItemCnt > 0)
	 {
		 szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_Brightness], g_szLT_TestItemBrightness[nTestItemCnt]);
	 }
	 else
	 {
		 szTestItem.Format(_T("%s"), g_szLT_TestItem_Name[TIID_Brightness]);
	 }
	 CString szResult;

	 if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	 {
		 szResult = _T("PASS");
		 m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	 }
	 else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	 {
		 szResult = _T("FAIL");
		 m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
	 }

	 return TRUE;
 }

// BOOL CTestManager_EQP::SaveParticleMesSystem(__in UINT nTestItemCnt)
// {
// 	ST_MES_TestItemLog m_MesData;
// 
// 	//- 시간
// 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
// 	//- Equipment
// 	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
// 	//- Model
// 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
// 	//- SW Ver
// 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
// 	//- LOT
// 	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;
// 	//- Operator
// 	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;
// 	//- Barcode
// 	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
// 	//- Cam Type
// 	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
// 
// 	//- Result + Data
// 	UINT DataNum = 0;
// 	CString Data[100];
// 
// 	CString strIndex;
// 	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
// 	m_stInspInfo.CamInfo.szIndex = strIndex;
// 
// 	m_WorklistPtr.pList_Particle[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
// 	m_WorklistPtr.pList_Particle[nTestItemCnt]->GetData(m_WorklistPtr.pList_Particle[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
// 
// 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
// 	//- Result
// 	m_MesData.Result = Data[Par_W_Result];
// 
// 	// 결과
// 	for (UINT nIdx = 0; nIdx < Par_W_MaxCol - Par_W_Result; nIdx++)
// 		m_MesData.Itemz.Add(Data[Par_W_Result + nIdx + 1]);
// 
// 	// Data에 대한 헤더 입력
// 	for (UINT nIdx = 0; nIdx < Par_W_MaxCol - Par_W_Result; nIdx++)
// 		m_MesData.ItemHeaderz.Add(g_lpszHeader_Par_Worklist[Par_W_Result + nIdx + 1]);
// 
// 	CString szTestItem;
// 	szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_Particle], g_szLT_TestItemBlackSpot[nTestItemCnt]);
// 	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
// 
// 	return TRUE;
// }
// 
// 

// 
// 
// BOOL CTestManager_EQP::SaveFFTMesSystem(__in UINT nTestItemCnt)
// {
// 	ST_MES_TestItemLog m_MesData;
// 
// 	//- 시간
// 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
// 	//- Equipment
// 	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
// 	//- Model
// 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
// 	//- SW Ver
// 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
// 	//- LOT
// 	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;
// 	//- Operator
// 	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;
// 	//- Barcode
// 	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
// 	//- Cam Type
// 	m_MesData.CamType = m_stInspInfo.CamInfo.szCamType = g_szCamState[m_stInspInfo.ModelInfo.nCameraType];
// 
// 	//- Result + Data
// 	UINT DataNum = 0;
// 	CString Data[100];
// 
// 	CString strIndex;
// 	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
// 	m_stInspInfo.CamInfo.szIndex = strIndex;
// 
// 	m_WorklistPtr.pList_FFT[nTestItemCnt]->InsertFullData(&m_stInspInfo.CamInfo);
// 	m_WorklistPtr.pList_FFT[nTestItemCnt]->GetData(m_WorklistPtr.pList_FFT[nTestItemCnt]->GetItemCount() - 1, DataNum, Data);
// 
// 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
// 	//- Result
// 	m_MesData.Result = Data[FFT_W_Result];
// 
// 	// 결과
// 	for (UINT nIdx = 0; nIdx < FFT_W_MaxCol - FFT_W_Result; nIdx++)
// 		m_MesData.Itemz.Add(Data[FFT_W_Result + nIdx + 1]);
// 
// 	// Data에 대한 헤더 입력
// 	for (UINT nIdx = 0; nIdx < FFT_W_MaxCol - FFT_W_Result; nIdx++)
// 		m_MesData.ItemHeaderz.Add(g_lpszHeader_FFT_Worklist[FFT_W_Result + nIdx + 1]);
// 
// 	CString szTestItem;
// 	szTestItem.Format(_T("%s_%s"), g_szLT_TestItem_Name[TIID_FFT], g_szLT_TestItemFFT[nTestItemCnt]);
// 	m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, szTestItem);
// 
// 	return TRUE;
// }


//=============================================================================
// Method		: StartAutoFocusingThr
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/10/22 - 14:55
// Desc.		:
//=============================================================================
// BOOL CTestManager_EQP::StartAutoFocusingThr()
// {
// 	if (NULL != m_hThrAutoFocusing)
// 	{
// 		CloseHandle(m_hThrAutoFocusing);
// 		m_hThrAutoFocusing = NULL;
// 	}
// 
// 	stThreadParam* pParam = new stThreadParam;
// 	pParam->pOwner = this;
// 	pParam->nIndex = 0;
// 	pParam->nArg_1 = 0;
// 	pParam->nArg_2 = 0;
// 
// 	m_hThrAutoFocusing = HANDLE(_beginthreadex(NULL, 0, StartAutoFocusing, pParam, 0, NULL));
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: StartAutoFocusing
// Access		: public static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2017/10/22 - 14:47
// Desc.		:
//=============================================================================
// UINT WINAPI CTestManager_EQP::StartAutoFocusing(__in LPVOID lParam)
// {
// 	CTestManager_EQP* pThis = (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
// 
// 	if (NULL != lParam)
// 		delete lParam;
// 
// 	pThis->StartAutoFocusingProcess();
// 
// 	return 0;
// }


//=============================================================================
// Method		: EndAutoFocusingThr
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/22 - 14:56
// Desc.		:
//=============================================================================
// void CTestManager_EQP::EndAutoFocusingThr()
// {
// 	DWORD dwEvent = WaitForSingleObject(m_hThrAutoFocusing, 1000);
// 
// 	if (WAIT_TIMEOUT == dwEvent)
// 	{
// 		TRACE(_T("Auto Focusing Thread Timeout\n"));
// 	}
// 	else if (WAIT_FAILED == dwEvent)
// 	{
// 		TRACE(_T("Auto Focusing Thread WAIT_FAILED:%d\n"), GetLastError());
// 	}
// }

// 
// void CTestManager_EQP::StartAutoFocusingProcess()
// {
// 	//m_TIProcessing.InitFocusingRun(m_stInspInfo.CamInfo.dInitPosY, m_stInspInfo.CamInfo.dInitPosR);
// 
// 	Sleep(500);
// 	m_TIProcessing.AutoFocusingRun(m_stInspInfo.CamInfo.dDisplaceAvg);
// 	
// 	//m_bFlag_AutoFocus = TRUE;
// }

//=============================================================================
// Method		: SaveSFRMesSystem
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/8/11 - 10:49
// Desc.		:
//=============================================================================
// BOOL CTestManager_EQP::SaveSFRMesSystem()
// {
// 
// 	ST_MES_TestItemLog m_MesData;
// 
// 	//- 시간
// 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
// 	//- Equipment
// 	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
// 	//- Model
// 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
// 	//- SW Ver
// 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
// 	//- LOT
// 	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;
// 	//- Operator
// 	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;
// 	//- Barcode
// 	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
// 
// 	//- Result + Data
// 	UINT DataNum = 0;
// 	CString Data[100];
// 
// 	m_WorklistPtr.pList_SFR->InsertFullData(&m_stInspInfo.CamInfo);
// 	m_WorklistPtr.pList_SFR->GetData(m_WorklistPtr.pList_SFR->GetItemCount() - 1, DataNum, Data);
// 
// 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
// 	m_MesData.Result = Data[SFR_W_Result];
// 
// 	// 결과
// 	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum - SFR_W_Result; nIdx++)
// 		m_MesData.Itemz.Add(Data[SFR_W_Result + nIdx + 1]);
// 
// 	// Data에 대한 헤더 입력
// 	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
// 	{
// 		m_MesData.ItemHeaderz.Add(g_lpszHeader_SFR_Worklist[SFR_W_Result + nIdx + 1]);
// 	}
// 
// 	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("SFR"));
// 
// 	return TRUE;
// }


//=============================================================================
// Method		: SaveParticleMesSystem
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/22 - 15:35
// Desc.		:
//=============================================================================
// BOOL CTestManager_EQP::SaveParticleMesSystem()
// {
// 	ST_MES_TestItemLog m_MesData;
// 
// 	//- 시간
// 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
// 	//- Equipment
// 	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
// 	//- Model
// 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
// 	//- SW Ver
// 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
// 	//- LOT
// 	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;
// 	//- Operator
// 	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;
// 	//- Barcode
// 	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
// 
// 	//- Result + Data
// 	UINT DataNum = 0;
// 	CString Data[100];
// 
// 	m_WorklistPtr.pList_Particle->InsertFullData(&m_stInspInfo.CamInfo);
// 	m_WorklistPtr.pList_Particle->GetData(m_WorklistPtr.pList_Particle->GetItemCount() - 1, DataNum, Data);
// 
// 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
// 	m_MesData.Result = Data[Par_W_Result];
// 
// 	// 결과
// 	for (UINT nIdx = 0; nIdx < Par_W_MaxCol - Par_W_Result; nIdx++)
// 		m_MesData.Itemz.Add(Data[Par_W_Result + nIdx + 1]);
// 
// 	// Data에 대한 헤더 입력
// 	for (UINT nIdx = 0; nIdx < Par_W_MaxCol - Par_W_Result; nIdx++)
// 		m_MesData.ItemHeaderz.Add(g_lpszHeader_Par_Worklist[Par_W_Result + nIdx + 1]);
// 
// 	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("Particle"));
// 
// 	return TRUE;
// }



LPBYTE CTestManager_EQP::GetImageBuffer(__in UINT nViewCh, __in UINT nGrabType, __out DWORD& dwWidth, __out DWORD& dwHeight, __out UINT& nChannel)
{
	// 카메라 연결 상태
	if (IsCameraConnect(nViewCh, nGrabType) == FALSE)
		return NULL;

	// Result Buffer
	LPBYTE pRGBDATA = NULL;
	
	// NTSC Buffer
	ST_VideoRGB_NTSC* pNTSCRGB = NULL;
	
	if (nGrabType == GrabType_NTSC)
	{
		pNTSCRGB = m_Device.Cat3DCtrl.GetRecvRGBData(nViewCh);
		pRGBDATA = (LPBYTE)((m_Device.Cat3DCtrl.GetRecvRGBData(nViewCh))->m_pMatRGB[0]);

		dwWidth = pNTSCRGB->m_dwWidth;
		dwHeight = pNTSCRGB->m_dwHeight;
		nChannel = 4;
	}
// 	else if (nGrabType == GrabType_LVDS)
// 	{
// 		pLVDSRGB = m_Device.DAQCtrl.GetRecvVideoRGB(nViewCh);
// 		pRGBDATA = (LPBYTE)(m_Device.DAQCtrl.GetRecvRGBData(nViewCh));
// 
// 		dwWidth = pLVDSRGB->m_dwWidth;
// 		dwHeight = pLVDSRGB->m_dwHeight;
// 		nChannel = 3;
// 	}

	return pRGBDATA;
}

BOOL CTestManager_EQP::IsCameraConnect(__in UINT nCh, __in UINT nGrabType)
{
	switch (nGrabType)
	{
	case GrabType_NTSC:

		if(m_Device.Cat3DCtrl.IsConnect() == FALSE)
			return FALSE;

		if (m_Device.Cat3DCtrl.GetStatus(nCh) == FALSE)
			return FALSE;

		break;

// 	case GrabType_LVDS:
// 
// 		if (m_Device.DAQCtrl.GetSignalStatus(nCh) == FALSE)
// 			return FALSE;
// 
// 		break;

	default:
		break;
	}

	return TRUE;
}

BOOL CTestManager_EQP::CameraOnOff(__in UINT nCh, __in UINT nGrabType, __in UINT nSWVer, __in UINT nFpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff, __in BOOL bOnOff)
{
	BOOL bResult = FALSE;

	switch (m_stInspInfo.ModelInfo.nGrabType)
	{
	case GrabType_NTSC:

		bResult = TRUE;

		break;

// 	case GrabType_LVDS:
// 
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.dwClock = 1000000;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nDataMode = DAQ_Data_16bit_Mode;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nHsyncPolarity = DAQ_HsyncPol_Inverse;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nPClockPolarity = DAQ_PclkPol_RisingEdge;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nDvalUse = DAQ_DeUse_HSync_Use;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nVideoMode = DAQ_Video_Signal_Mode;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nMIPILane = DAQMIPILane_1;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nClockSelect = DAQClockSelect_FixedClock;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nClockUse = DAQClockUse_Off;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.dWidthMultiple = 1.0;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.dHeightMultiple = 1.0;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nConvFormat = Conv_CbYCrY_GRBG;
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.nSensorType = enImgSensorType_YCBCR;
// 
// 		if (m_stInspInfo.ModelInfo.bI2CFile_1 == TRUE)
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.szIICPath_1 = m_stInspInfo.Path.szI2C + m_stInspInfo.ModelInfo.szI2CFile_1 + _T(".") + I2C_FILE_EXT;
// 		else
// 			m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.szIICPath_1.Empty();
// 
// 		if (m_stInspInfo.ModelInfo.bI2CFile_2 == TRUE)
// 		m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.szIICPath_2 = m_stInspInfo.Path.szI2C + m_stInspInfo.ModelInfo.szI2CFile_2 + _T(".") + I2C_FILE_EXT;
// 		else
// 			m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.szIICPath_2.Empty();
// 
// 		
// 		m_Device.DAQCtrl.SetDAQOption(nCh, m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption);
// 
// 		if (FALSE == m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.szIICPath_1.IsEmpty())
// 		{
// 			Sleep(100);
// 			if (!m_Device.DAQCtrl.SetI2CFilePath(nCh, m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.szIICPath_1))
// 			{
// 				int a = 0;
// 			}
// 		}
// 
// 		if (bOnOff == ON)
// 		{
// 			CameraRegToFunctionChange(nSWVer, nFpsMode, nDistortion, nEdgeOnOff);
// 			m_Device.DAQCtrl.Capture_Start(nCh);
// 			Sleep(m_stInspInfo.ModelInfo.nCameraDelay);
// 
// 		}
// 		else if (bOnOff == OFF)
// 		{
// 			m_Device.DAQCtrl.Capture_Stop(nCh);
// 
// 			//if (FALSE == m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.szIICPath_2.IsEmpty())
// 			//{
// 			//	Sleep(100);
// 			//	m_Device.DAQCtrl.SetI2CFilePath(nCh, m_stInspInfo.ModelInfo.stLVDSInfo.stLVDSOption.szIICPath_2);
// 			//}
// 		}
// 		bResult = TRUE;
// 
// 		break;

	default:
		break;
	}

	return bResult;

	
}

UINT CTestManager_EQP::TestProcessCurrent(__in ST_Current_Opt* pstOpt, __out ST_Current_Result &stResult)
{
	OnLog(_T("CTestManager_EQP::TestProcessCurrent() :: Start"));
	if (pstOpt == NULL)
		return RCA_TransferErr;

	double iCurrent[CamBrd_Channel] = { 0, };
	
	BOOL bResult = TRUE;
	UINT nTestResult = RCA_OK;

	// 1. 측정
	nTestResult = m_Device.PCBCamBrd[0].Send_GetCurrent(iCurrent);
	
	if (RCA_OK == nTestResult)
	{
		// 2. 옵셋
		for (int iCh = 0; iCh < Def_CamBrd_Channel; iCh++)
		{
			stResult.iValue[iCh] = iCurrent[iCh];
			stResult.iValue[iCh] *= pstOpt->dbOffset[iCh];
			//stResult.iValue[iCh] *= 0.1;

			OnLog(_T("CTestManager_EQP::TestProcessCurrent() :: Current : %.2f"), stResult.iValue[iCh]);

 			// 3. 결과	 
 			if (pstOpt->nSpecMin[iCh] <= stResult.iValue[iCh] && pstOpt->nSpecMax[iCh] >= stResult.iValue[iCh])
 			{
 				stResult.nEachResult[iCh] = TER_Pass;
 			}
 			else
 			{
 				stResult.nEachResult[iCh] = TER_Fail;
 				bResult = FALSE;
 			}
		}

 		if (bResult == TRUE)
 		{
 			stResult.nResult = TER_Pass;
 		}
 		else
 		{
 			stResult.nResult = TER_Fail;
 		}
	}
	else
	{
		nTestResult = RCA_TransferErr;
	}

	return nTestResult;
}


UINT CTestManager_EQP::TestProcessVideoSignal(__in UINT nCh, __out UINT& nResult)
{
	UINT nTestResult = RCA_OK;

	BYTE ReadData[1] = { 0, };

	// Lock 신호 Read
// 	if (m_Device.DAQCtrl.I2C_Read(nCh, 0x44, 1, 0x00, 1, ReadData) == TRUE)
// 	{
// 		if (ReadData[0] == 0xFF)
// 		{
// 			nTestResult = RCA_OK;
// 			nResult = TER_Pass;
// 		}
// 		else
// 		{
// 			nTestResult = RCA_NG;
// 			nResult = TER_Fail;
// 		}
// 	}
// 	else
// 	{
// 		nTestResult = RCA_NG;
// 		nResult = TER_Fail;
// 	}

	return nTestResult;
}


UINT CTestManager_EQP::TestProcessReset(__in UINT nCh, __in UINT nResetDelay, __out UINT& nResult)
{
	BOOL bResult = TRUE;
	UINT nTestResult = RCA_OK;

	BYTE ReadData[1] = { 0, };

	BYTE WriteData1[1] = { 0x00 };
	BYTE WriteData2[1] = { 0x00 };
	BYTE WriteData3[1] = { 0x01 };
	
	// Reset Sequence
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x44, 1, 0x07, 1, WriteData1) == FALSE)
// 		return nTestResult = RCA_NG;
// 
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x44, 1, 0x03, 1, WriteData2) == FALSE)
// 		return nTestResult = RCA_NG;
// 
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x44, 1, 0x03, 1, WriteData3) == FALSE)
// 		return nTestResult = RCA_NG;
// 	
// 	Sleep(nResetDelay);
// 	
// 	// Lock 신호 Read
// 	if (m_Device.DAQCtrl.I2C_Read(nCh, 0x44, 1, 0x00, 1, ReadData) == TRUE)
// 	{
// 		if (ReadData[0] == 0xFF)
// 		{
// 			nTestResult = RCA_OK;
// 			nResult = TER_Pass;
// 		}
// 		else
// 		{
// 			nTestResult = RCA_NG;
// 			nResult = TER_Fail;
// 		}
// 	}
// 	else
// 	{
// 		nTestResult = RCA_NG;
// 		nResult = TER_Fail;
// 	}

	return nTestResult;
}

UINT CTestManager_EQP::TestProcessFrameCheck(__in UINT nCh, __in UINT nfpsMode, __out UINT& nResult)
{
	UINT nTestResult = RCA_OK;

	DWORD dwFrameRate = 0;
//	m_Device.DAQCtrl.LVDS_GetFrameRate(nCh, &dwFrameRate);

	switch (nfpsMode)
	{
	case 0: // 15fps
		if (dwFrameRate >= 15)
		{
			nResult = RCA_OK;
		}
		break;

	case 1: // 30fps
		if (dwFrameRate >= 30)
		{
			nResult = RCA_OK;
		}
		break;

	default:
		break;
	}

	return nTestResult = nResult;
}

//=============================================================================
// Method		: OpticalCenter_Adj
// Access		: public  
// Returns		: UINT
// Parameter	: __in UINT nCh
// Parameter	: __in int iCenterX
// Parameter	: __in int iCenterY
// Parameter	: __in int iTargerX
// Parameter	: __in int iTargerY
// Qualifier	:
// Last Update	: 2018/4/19 - 15:40
// Desc.		:
//=============================================================================
UINT CTestManager_EQP::OpticalCenter_Adj(__in UINT nCh, __in int iCenterX, __in int iCenterY, __in int iTargerX, __in int iTargerY)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffsetX[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x14, };
	BYTE WriteDataOffsetY[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x16, };

	int iZeroOffsetX = 41;
	int iZeroOffsetY = 43;
	
	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC_HV[2] = { 0x00, };
	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff};

	int iMoveX = 0;
	int iMoveY = 0;

	int iReadOffsetX = 0;
	int iReadOffsetY = 0;

	iMoveX = iCenterX - iTargerX;
	iMoveX /= 2;
	iMoveX *= 2;

	iMoveY = iCenterY - iTargerY;
	iMoveY /= 2;
	iMoveY *= 2;
	
	// Horizon
	if (iMoveX >= 0) // 양수
	{
		WriteDataCRC_HV[0] = iZeroOffsetX + iMoveX;
		
		WriteDataOffsetX[6] = (BYTE)iMoveX;
		WriteDataOffsetX[7] = WriteDataMark[0];
	}
	else // 음수
	{
		WriteDataCRC_HV[0] = (iZeroOffsetX + iMoveX) - 1;
		
		WriteDataOffsetX[6] = (BYTE)(256 + iMoveX);
		WriteDataOffsetX[7] = WriteDataMark[1];
	}

	WriteDataOffsetX[8] = WriteDataCRC_HV[0];

// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffsetX) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				//return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	// Vertical
	if (iMoveY >= 0) // 양수
	{
		WriteDataCRC_HV[1] = iZeroOffsetY + iMoveY;

		WriteDataOffsetY[6] = (BYTE)iMoveY;
		WriteDataOffsetY[7] = WriteDataMark[0];
	}
	else // 음수
	{
		WriteDataCRC_HV[1] = (iZeroOffsetY + iMoveY) - 1;

		WriteDataOffsetY[6] = (BYTE)(256 + iMoveY);
		WriteDataOffsetY[7] = WriteDataMark[1];
	}
	
	WriteDataOffsetY[8] = WriteDataCRC_HV[1];

// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffsetY) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				//return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	// Serial NOR Flash All Write
// 	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
// 	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };
// 
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 4, FlachAllWriteData1) == TRUE)
// 	{
// 		if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 8, FlachAllWriteData2) == TRUE)
// 		{
// 			BYTE ReadCheck[256] = { 0x0, };
// 			BYTE ConfirmCheck[5] = { 0xff, 0xff, 0xff, 0xff, 0xff };
// 			if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 			{
// 				int iCheckFailCnt = 0;
// 				for (int i = 0; i < 5; i++) // Check Data 5 Count
// 				{
// 					if (ReadCheck[i] != ConfirmCheck[i])
// 					{
// 						iCheckFailCnt++;
// 					}
// 				}
// 
// 				if (iCheckFailCnt > 0)
// 				{
// 					//return nTestResult = RCA_NG;
// 				}
// 			}
// 			else
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	return nTestResult;
}


UINT CTestManager_EQP::OpticalCenter_Adj(__in UINT nAxis, __in UINT nCh, __in int iOffset)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffset[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, };

	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC = 0x00;

	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	int iZeroOffset = 0;

	switch (nAxis)
	{
	case 0:
		if (iOffset < 0)
		{
			WriteDataOffset[5] = 0x16;
			iZeroOffset = 43;

			WriteDataCRC = (iZeroOffset + iOffset) - 1;

			WriteDataOffset[6] = (BYTE)(256 + iOffset);
			WriteDataOffset[7] = WriteDataMark[1];
			WriteDataOffset[8] = WriteDataCRC;
		}
		else
		{
			WriteDataOffset[5] = 0x16;
			iZeroOffset = 43;

			WriteDataCRC = iZeroOffset + iOffset;

			WriteDataOffset[6] = (BYTE)iOffset;
			WriteDataOffset[7] = WriteDataMark[0];
			WriteDataOffset[8] = WriteDataCRC;
		}
		
		break;

	case 1:
		if (iOffset < 0)
		{
			WriteDataOffset[5] = 0x14;
			iZeroOffset = 41;

			WriteDataCRC = (iZeroOffset + iOffset) - 1;

			WriteDataOffset[6] = (BYTE)(256 + iOffset);
			WriteDataOffset[7] = WriteDataMark[1];
			WriteDataOffset[8] = WriteDataCRC;
		} 
		else
		{
			WriteDataOffset[5] = 0x14;
			iZeroOffset = 41;

			WriteDataCRC = iZeroOffset + iOffset;

			WriteDataOffset[6] = (BYTE)iOffset;
			WriteDataOffset[7] = WriteDataMark[0];
			WriteDataOffset[8] = WriteDataCRC;
		}
		
		break;

	default:
		break;
	}

// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffset) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				//return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	// Serial NOR Flash All Write
	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };

// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, FlachAllWriteData1) == TRUE)
// 	{
// 		if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x05, 4, FlachAllWriteData2) == TRUE)
// 		{
// 			BYTE ReadCheck[256] = { 0x0, };
// 			BYTE ConfirmCheck[5] = { 0xff, 0xff, 0xff, 0xff, 0xff };
// 			if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 			{
// 				int iCheckFailCnt = 0;
// 				for (int i = 0; i < 5; i++) // Check Data 5 Count
// 				{
// 					if (ReadCheck[i] != ConfirmCheck[i])
// 					{
// 						iCheckFailCnt++;
// 					}
// 				}
// 
// 				if (iCheckFailCnt > 0)
// 				{
// 					//return nTestResult = RCA_NG;
// 				}
// 			}
// 			else
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}


	return nTestResult;
}

UINT CTestManager_EQP::OpticalCenter_Adj_Horizon(__in UINT nCh, __in int iOffset)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffset[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x14 };

	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC = 0x00;

	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	if (iOffset < 0)
	{
		WriteDataCRC = (41 + iOffset) - 1;
		WriteDataOffset[6] = (BYTE)(256 + iOffset);
		WriteDataOffset[7] = WriteDataMark[1];
		WriteDataOffset[8] = WriteDataCRC;
	}
	else
	{
		WriteDataCRC = (41 + iOffset);
		WriteDataOffset[6] = (BYTE)iOffset;
		WriteDataOffset[7] = WriteDataMark[0];
		WriteDataOffset[8] = WriteDataCRC;
	}

// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffset) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 5, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}

// 			if (iCheckFailCnt > 0)
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}	


	return nTestResult;
}

UINT CTestManager_EQP::OpticalCenter_Adj_Vertical(__in UINT nCh, __in int iOffset)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffset[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x16 };

	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC = 0x01;

	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	if (iOffset < 0)
	{
		WriteDataCRC = (43 + iOffset) - 1;
		WriteDataOffset[6] = (BYTE)(256 + iOffset);
		WriteDataOffset[7] = WriteDataMark[1];
		WriteDataOffset[8] = WriteDataCRC;
	}
	else
	{
		WriteDataCRC = (43 + iOffset);
		WriteDataOffset[6] = (BYTE)iOffset;
		WriteDataOffset[7] = WriteDataMark[0];
		WriteDataOffset[8] = WriteDataCRC;
	}

// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffset) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 5, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// // 			if (iCheckFailCnt > 0)
// // 			{
// // 				return nTestResult = RCA_NG;
// // 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}


	return nTestResult;
}

UINT CTestManager_EQP::OpticalCenter_Adj_AllWrite(__in UINT nCh)
{
	UINT nTestResult = RCA_OK;
	
	// Serialize Disable
	BYTE Serialize_Disable[1] = { 0x43 };
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x40 << 1, 1, 0x04, 1, Serialize_Disable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
//	}
	Sleep(1000);

	// Default Set 0fps
	BYTE Default_Set_0fps_1[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x00, 0x13 };
	BYTE Default_Set_0fps_2[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x00, 0x14 };
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, Default_Set_0fps_1) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, Default_Set_0fps_2) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
	// Serial NOR Flash All Write
	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };

// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, FlachAllWriteData1) == TRUE)
// 	{
// 		if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x05, 4, FlachAllWriteData2) == TRUE)
// 		{
// 			BYTE ReadCheck[256] = { 0x0, };
// 			BYTE ConfirmCheck[5] = { 0x0, };
// 			if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x09, 256, ReadCheck) == TRUE)
// 			{
// 				int iCheckFailCnt = 0;
// 				for (int i = 0; i < 5; i++) // Check Data 5 Count
// 				{
// 					if (ReadCheck[i] != ConfirmCheck[i])
// 					{
// 						iCheckFailCnt++;
// 					}
// 				}
// 
// 				// 				if (iCheckFailCnt > 0)
// 				//  	{
// 				//  		return nTestResult = RCA_NG;
// 				//  	}
// 			}
// 			else
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}
	
// 
// 	// Serialize Disable
// 	BYTE Serialize_Disable[1] = { 0x43 };
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x40 << 1, 1, 0x04, 1, Serialize_Disable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(1000);
// 
// 	// Default Set 0fps
// 	BYTE Default_Set_0fps_1[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x00, 0x13 };
// 	BYTE Default_Set_0fps_2[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x00, 0x14 };
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, Default_Set_0fps_1) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, Default_Set_0fps_2) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	// Serial NOR Flash All Write
//   	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
//   	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };
//   
//   	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, FlachAllWriteData1) == TRUE)
//   	{
//   		if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x05, 4, FlachAllWriteData2) == TRUE)
//   		{
//   			BYTE ReadCheck[256] = { 0x0, };
//   			BYTE ConfirmCheck[5] = { 0x0, };
//   			if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x09, 256, ReadCheck) == TRUE)
//   			{
//   				int iCheckFailCnt = 0;
//   				for (int i = 0; i < 5; i++) // Check Data 5 Count
//   				{
//   					if (ReadCheck[i] != ConfirmCheck[i])
//   					{
//   						iCheckFailCnt++;
//   					}
//   				}
//   
//   // 				if (iCheckFailCnt > 0)
// //  	{
// //  		return nTestResult = RCA_NG;
// //  	}
//   			}
//   			else
//   			{
//   				return nTestResult = RCA_NG;
//   			}
//   		}
//   	}
//   	else
//   	{
//   		return nTestResult = RCA_NG;
//   	}


	return nTestResult;
}

UINT CTestManager_EQP::DistortionCorrection(__in BOOL bOnOff)
{
	UINT nTestResult = RCA_OK;

// 	if (bOnOff == TRUE)
// 	{
// 		BYTE DistortionDataOff[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x01, 0x2f };
// 		BYTE DistortionDataOn[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x00, 0x2e };
// 
// 		if (m_Device.DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, DistortionDataOff) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		BYTE DistortionDataOff[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x01, 0x2f };
// 		BYTE DistortionDataOn[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x00, 0x2e };
// 
// 		if (m_Device.DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, DistortionDataOn) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}


	return nTestResult;
}

UINT CTestManager_EQP::EdgeOnOff(__in BOOL bOnOff)
{
	UINT nTestResult = RCA_OK;

	if (bOnOff == TRUE)
{
		BYTE EdgeDataOn[8] = { 0x01, 0x06, 0x02, 0x59, 0x00, 0x14, 0x01, 0x80 };

// 		if (m_Device.DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, EdgeDataOn) == FALSE)
// 	{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		BYTE EdgeDataOff[8] = { 0x01, 0x06, 0x02, 0x59, 0x00, 0x14, 0x00, 0x7f };
// 
// 		if (m_Device.DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, EdgeDataOff) == FALSE)
// 	{
// 			//	nTestResult = RCA_NG;
// 	}
}

	return nTestResult;
}

UINT CTestManager_EQP::OperationModeChange(__in UINT nfpsMode)
{
	UINT nTestResult = RCA_OK;

	// Serialize Disable Setting
// 	BYTE SerializeDisable[1] = { 0x43 };
// 	if (m_Device.DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeDisable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(200);

	// Fps Mode Output
// 	if (nfpsMode == 0) // 15fps
// 	{
// 		BYTE IspSetting_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x01, 0x14 };
// 		if (m_Device.DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(100);
// 
// 		BYTE IspSetting_F_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x80, 0x94 };
// 		if (m_Device.DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_F_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(500);
// 	}
// 	else if (nfpsMode == 1) // 30fps
// 	{
// 		BYTE IspSetting_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x30, 0x43};
// 		if (m_Device.DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(100);
// 
// 		BYTE IspSetting_F_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x80, 0x94 };
// 		if (m_Device.DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_F_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(500);
// 	}

	// Serialize Enable Setting
// 	BYTE SerializeEnable[1] = { 0x83 };
// 	if (m_Device.DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeEnable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(100);

	return nTestResult;
}

UINT CTestManager_EQP::CameraRegToFunctionChange(__in UINT nSWVer, __in UINT nfpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff)
{
	UINT nTestResult = RCA_OK;

	// Serialize Disable Setting
	BYTE SerializeDisable[1] = { 0x43 };
// 	if (m_Device.DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeDisable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(100);
// 
// 	// ISP Reset
// 	BYTE ISP_LOW[1] = { 0x0f };
// 	BYTE ISP_HIGH[1] = { 0x8f };
// 	m_Device.DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x0d, 1, ISP_LOW);
// 	Sleep(100);
// 	m_Device.DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x0d, 1, ISP_HIGH);
// 	Sleep(1000);

	// Distortion
	switch (nDistortion)
	{
	case 0:
		DistortionCorrection(FALSE);
		break;
	case 1:
		DistortionCorrection(TRUE);
		break;
	default:
		break;
	}

	// Edge
	switch (nEdgeOnOff)
	{
	case 0:
		EdgeOnOff(FALSE);
		break;
	case 1:
		EdgeOnOff(TRUE);
		break;
	default:
		break;
	}

	// Fps Setting
	switch (nfpsMode)
	{
	case 0: // 15
		OperationModeChange(0);
		break;
	case 1: // 30
		OperationModeChange(1);
		break;
	default:
		break;
	}

	Sleep(1000);

	// Serialize Enable Setting
	BYTE SerializeEnable[1] = { 0x83 };
// 	if (m_Device.DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeEnable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	Sleep(100);
	
	return nTestResult;
}

UINT CTestManager_EQP::Motion_StageY_CL_Distance(__in double dDistance)
{
	UINT nTestResult = RCA_OK;

//	SetAjin_IO(DO_ChartLight, (enum_IO_SignalType)IO_SignalT_SetOn);

	double dbPuls = (g_dbMotor_AxisLeadMax[AX_StageDistance] - dDistance) * g_dbMotor_AxisResolution[AX_StageDistance];

	nTestResult = SetAjin_Motor(AX_StageDistance, POS_ABS_MODE, dbPuls);

	return nTestResult;
}

UINT CTestManager_EQP::Motion_StageY_BlemishZone_In(__in UINT nTestItemID)
{
	UINT nTestResult = RCA_OK;

	switch (nTestItemID)
	{
	case TIID_Brightness:
// 		SetAjin_IO(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		SetAjin_IO(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOn);
		break;

	case TIID_IRFilter:
// 		SetAjin_IO(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		SetAjin_IO(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOff);
		break;

//	case TIID_PatternNoise:
// 		SetAjin_IO(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		SetAjin_IO(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOff);
//		break;

//	case TIID_LEDTest:
// 		SetAjin_IO(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		SetAjin_IO(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOn);
//		break;

	case TIID_ParticleManual:
// 		SetAjin_IO(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		SetAjin_IO(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOn);
		break;

	default:
		break;
	}

	double dbPuls_Step1 = 80000;
	double dbPuls_Step2 = g_dbMotorAxis_Y_Blemish[SET_EQUP_TEACH];
	//double dbPuls_Step1 = 120000;
	//double dbPuls_Step2 = 150000;

	// In 상태라면 그대로 리턴.
	if (m_Device.MotionManager.GetCurrentPos(AX_StageDistance) == dbPuls_Step2)
	{
// 		if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_In) == TRUE)
// 		{
// 			return nTestResult = RCA_OK;
// 		}
	}

	// 현재 위치 확인 - pulse 값 : 스테이지가 다크유닛을 지나쳐 있거나 근처에 있다면
	if (m_Device.MotionManager.GetCurrentPos(AX_StageDistance) >= dbPuls_Step1)
	{
		// 다크유닛 빠지고,
		BOOL bStatus = FALSE;

// 		SetAjin_IO(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		SetAjin_IO(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOff);

// 		nTestResult = SetAjin_IO(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		for (int i = 0; i < 100; i++)
// 		{
// 			if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_Out) == TRUE)
// 			{
// 				bStatus = TRUE;
// 				break;
// 			}
// 			DoEvents(33);
// 		}

		if (bStatus == TRUE)
		{
			nTestResult = RCA_OK;
		} 
		else
		{
			nTestResult = RCA_MachineCheck;
		}

	}

	// 1 Step
	if (nTestResult == RCA_OK)
	{
		nTestResult = SetAjin_Motor(AX_StageDistance, POS_ABS_MODE, dbPuls_Step1);
	}

	// 다크유닛 들어옴.
	if (nTestResult == RCA_OK)
	{
		BOOL bStatus = FALSE;

// 		SetAjin_IO(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		SetAjin_IO(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOff);

// 		SetAjin_IO(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		for (int i = 0; i < 100; i++)
// 		{
// 			if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_In) == TRUE)
// 			{
// 				bStatus = TRUE;
// 				break;
// 			}
//
// 			DoEvents(33);
// 		}

		if (bStatus == TRUE)
		{
			nTestResult = RCA_OK;
		}
		else
		{
			nTestResult = RCA_MachineCheck;
		}
	}

	// 2 Step
	if (nTestResult == RCA_OK)
	{
		nTestResult = SetAjin_Motor(AX_StageDistance, POS_ABS_MODE, dbPuls_Step2);
	}

	return nTestResult;
}

UINT CTestManager_EQP::Motion_StageY_BlemishZone_Out()
{
	UINT nTestResult = RCA_OK;

// 	SetAjin_IO(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOff);
// 	SetAjin_IO(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);

	double dbPuls_Step1 = 80000;
	double dbPuls_Step2 = 0;

	// Out 상태라면 그대로 리턴.
// 	if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_Out) == TRUE)
// 	{
// 		return nTestResult = RCA_OK;
// 	}
	
	// 현재 위치 확인 - pulse 값 : 스테이지가 다크유닛을 지나쳐 있거나 근처에 있다면
	if (m_Device.MotionManager.GetCurrentPos(AX_StageDistance) >= dbPuls_Step1)
	{
		// 1 Step
		if (nTestResult == RCA_OK)
		{
			nTestResult = SetAjin_Motor(AX_StageDistance, POS_ABS_MODE, dbPuls_Step1);
		}

		// 다크유닛 빠지고,
		BOOL bStatus = FALSE;

// 		SetAjin_IO(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		SetAjin_IO(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		
// 		SetAjin_IO(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		for (int i = 0; i < 100; i++)
// 		{
// 			if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_Out) == TRUE)
// 			{
// 				bStatus = TRUE;
// 				break;
// 			}
// 			DoEvents(33);
// 		}

		if (bStatus == TRUE)
		{
			nTestResult = RCA_OK;
		}
		else
		{
			nTestResult = RCA_MachineCheck;
		}

	}

	return nTestResult;
}

UINT CTestManager_EQP::Motion_StageY_Initial()
{
	UINT nTestResult = RCA_OK;
	nTestResult = SetAjin_Motor(AX_StageDistance, POS_ABS_MODE, 0);

	return nTestResult;
}

UINT CTestManager_EQP::OnPrinterOut(__in int iLotCount)
{
	int iPrintingCount = 1;
	int iSerialCount = iLotCount - 1;

	if (iSerialCount < 0)
	{
		return 1;
	}

	if (iPrintingCount < 0)
	{
		return 1;
	}

	//for (int j = 0; j <= iSerialCount; j++)
	{
		ST_ZebraPrinterFont stFontData[enLabelDataType_Max];
		for (int i = 0; i < enLabelDataType_Max; i++)
		{
			int iposx = m_stInspInfo.ModelInfo.stPrinter.iPosX[i];
			int iposy = m_stInspInfo.ModelInfo.stPrinter.iPosY[i];
			int iwidth = m_stInspInfo.ModelInfo.stPrinter.iWidthSz[i];
			int iheight = m_stInspInfo.ModelInfo.stPrinter.iHeightSz[i];
			CString szText = m_stInspInfo.ModelInfo.stPrinter.szText[i];

			if (i == enLabelDataType_Barcode)
			{
				stFontData[i].MakePrintingToFont(1, 1, iposx, iposy, iwidth, iheight, (CStringA)szText, iSerialCount);
			}
			else
			{
				if (i == enLabelDataType_Date)
				{
					stFontData[i].MakePrintingToFont(0, 1, iposx, iposy, iwidth, iheight, (CStringA)szText, iSerialCount);
				}
				else
				{
					stFontData[i].MakePrintingToFont(0, 0, iposx, iposy, iwidth, iheight, (CStringA)szText);
				}
			}

			m_Device.Printer.SettingToFontData(i, stFontData[i]);
		}

		CString szPrnPath = m_stInspInfo.Path.szPrnFile + _T("\\") + m_stInspInfo.ModelInfo.stPrinter.szPrnFileName + _T(".prn");
		m_Device.Printer.SendToLabelOutput(iPrintingCount, szPrnPath);

		Sleep(500);
	}



	return 1;
}

UINT CTestManager_EQP::OpticalCenter_Adj_initial(__in UINT nCh)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffsetX[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x14, 0x00, 0x00, 0x29 };
	BYTE WriteDataOffsetY[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x16, 0x00, 0x00, 0x2b };

// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18, 1, 0x0a, 9, WriteDataOffsetX) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18, 1, 0x0a, 9, WriteDataOffsetY) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	// Serial NOR Flash All Write
// 	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
// 	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };
// 
// 	if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18, 1, 0x0a, 4, FlachAllWriteData1) == TRUE)
// 	{
// 		if (m_Device.DAQCtrl.I2C_Write(nCh, 0x18, 1, 0x0a, 8, FlachAllWriteData2) == TRUE)
// 		{
// 			BYTE ReadCheck[256] = { 0x0, };
// 			BYTE ConfirmCheck[5] = { 0xff, 0xff, 0xff, 0xff, 0xff };
// 			if (m_Device.DAQCtrl.I2C_Read(nCh, 0x18, 1, 0x0a, 256, ReadCheck) == TRUE)
// 			{
// 				int iCheckFailCnt = 0;
// 				for (int i = 0; i < 5; i++) // Check Data 5 Count
// 				{
// 					if (ReadCheck[i] != ConfirmCheck[i])
// 					{
// 						iCheckFailCnt++;
// 					}
// 				}
// 
// 				if (iCheckFailCnt > 0)
// 				{
// 					return nTestResult = RCA_NG;
// 				}
// 			}
// 			else
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	return nTestResult;
}

BOOL CTestManager_EQP::GetImageBuffer(){

	LPBYTE pFrameImage = NULL;
	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	for (int i = 0; i < 30; i++)
	{
		pFrameImage = GetImageBuffer(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, dwWidth, dwHeight, nChannel);
		Sleep(33);

		if (NULL != pFrameImage)
			break;
	}

	if (pFrameImage == NULL)
	{
		return FALSE;
	}

	if (m_TestImage != NULL)
	{
		cvReleaseImage(&m_TestImage);
		m_TestImage = NULL;
	}
	m_TestImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	for (int y = 0; y < dwHeight; y++)
	{
		for (int x = 0; x < dwWidth; x++)
		{
			m_TestImage->imageData[y * m_TestImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
			m_TestImage->imageData[y * m_TestImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
			m_TestImage->imageData[y * m_TestImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
		}
	}
	return TRUE;
}

void CTestManager_EQP::RunLedChange(UINT nColor, BOOL bMode /*= TRUE*/){
	m_Device.MotionSequence.RunStatusLamp(nColor, bMode);
}