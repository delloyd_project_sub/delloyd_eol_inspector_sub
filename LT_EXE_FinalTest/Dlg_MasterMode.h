#pragma once
#include "resource.h"

#include "Def_TestDevice.h"
#include "Wnd_VideoView.h"
#include "TestProcess_Image.h"
#include "VGStatic.h"

// CDlg_MasterMode 대화 상자입니다.

// CWnd_MasterSet

enum enMaster_Static{
	STI_MST_X = 0,
	STI_MST_Y,
	STI_MST_R,
	STI_MST_MasterInfo,
	STI_MST_MasterSpc,
	STI_MST_MasterOffset,
	STI_MST_MAXNUM,
};
static LPCTSTR	g_szMaster_Static[] =
{
	_T("OFFSET X \n [Pix]"),
	_T("OFFSET Y \n [Pix]"),
	_T("OFFSET R \n [Degree]"),
	_T("MASTER \n INFO"),
	_T("MASTER \n SPC"),
	_T("MASTER \n OFFSET"),
	NULL
};

enum enMaster_Edit{
	EIT_MST_MasterInfoX = 0,
	EIT_MST_MasterInfoY,
	EIT_MST_MasterInfoR,
	EIT_MST_MasterSpcX,
	EIT_MST_MasterSpcY,
	EIT_MST_MasterSpcR,
	EIT_MST_MasterOffsetX,
	EIT_MST_MasterOffsetY,
	EIT_MST_MasterOffsetR,
	EIT_MST_MAXNUM,
};
class CDlg_MasterMode : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_MasterMode)

public:
	CDlg_MasterMode(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_MasterMode();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MASTER_TEST };

// 	void CreateThread(UINT _method);
// 	bool DestroyThread();


	BOOL m_bStopFlag;
	UINT m_nResult;
	UINT m_nViewChannel;
	BOOL m_bFlag_Butten[DI_NotUseBit_Max];

	CString m_strI2CPath;

	UINT m_nMode;
	UINT m_nPermisionMode;

	ST_Device* m_pDevice;
	ST_ModelInfo* m_pModelinfo;

	void		PermissionMode(enPermissionMode InspMode);

	UINT GetResult()
	{
		return m_nResult;
	};

	void SetPtr_Device(__in ST_Device* pDevice)
	{
		if (pDevice == NULL)
			return;

		m_pDevice = pDevice;
	};

	void SetPtr_Modelinfo(__in ST_ModelInfo* pModelinfo)
	{
		if (pModelinfo == NULL)
			return;

		m_pModelinfo = pModelinfo;
	};

	void SetVideoChannel(__in UINT nCH)
	{
		m_nViewChannel = nCH;
	};

	void SetPath(__in CString szI2CPath)
	{
		m_strI2CPath = szI2CPath;
	};

	void SetMode(__in UINT nMode)
	{
		m_nMode = nMode;
	};

	void SetPermisionMode(__in UINT nMode)
	{
		m_nPermisionMode = nMode;
	};

	CWnd_VideoView m_wndVideoView;
	CTestProcess_Image	m_ImageTest;

protected:
	CFont			m_font;
	CFont			m_font_Default;

	CVGStatic m_stTotalTestStatus;
	CVGStatic m_stTestItem[TIID_MaxEnum];
	CVGStatic m_stEachTestStatus[TIID_MaxEnum];

	CComboBox m_cbEachTestSel[TIID_MaxEnum];

	CMFCButton		m_bn_MasterTest;
	CMFCButton		m_bn_MasterSave;
	CMFCButton		m_bn_MasterExit;

	CVGStatic		m_st_UIItem;
	CVGStatic		m_st_Item[STI_MST_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[EIT_MST_MAXNUM];

	// Master Check
//	BOOL MasterCheck();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void	OnBnClickedTest();
	afx_msg void	OnBnClickedSave();
	afx_msg void	OnBnClickedExit();
	// 	afx_msg void OnBnClickedBnLeft();
	// 	afx_msg void OnBnClickedBnRight();
	// 	afx_msg void OnBnClickedBnOK();
	// 	afx_msg void OnBnClickedBnInit();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void		GetUIData();
	void		SetUIData();
	void		SetMasterData(__in int iOffsetX, __in int iOffsetY, __in double dbDegree);

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
};
