﻿#ifndef Wnd_BaseEIAJOp_h__
#define Wnd_BaseEIAJOp_h__

#pragma once

#include "Wnd_EIAJOp.h"

// CWnd_BaseEIAJOp

class CWnd_BaseEIAJOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseEIAJOp)

public:
	CWnd_BaseEIAJOp();
	virtual ~CWnd_BaseEIAJOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_EIAJOp m_wndEIAJOp[TICnt_EIAJ];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();

	void SetStatusEngineerMode(__in enPermissionMode InspMode);
};
#endif // Wnd_BaseEIAJOp_h__
