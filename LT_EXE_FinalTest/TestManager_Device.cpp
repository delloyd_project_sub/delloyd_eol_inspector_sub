﻿//*****************************************************************************
// Filename	: 	TestManager_Device.cpp
// Created	:	2016/10/6 - 13:46
// Modified	:	2016/10/6 - 13:46
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_Device.h"
#include "CommonFunction.h"

CTestManager_Device::CTestManager_Device()
{
	OnInitialize();
}


CTestManager_Device::~CTestManager_Device()
{
	TRACE(_T("<<< Start ~CTestManager_Device >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_Device >>> \n"));
}

//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/5/18 - 18:58
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::OnLoadOption()
{
	CLT_Option	stOption;
	stOption.SetInspectorType((enInsptrSysType)SET_INSPECTOR);
	BOOL		bReturn = TRUE;

	stOption.SetRegistryPath(REG_PATH_OPTION_BASE);
	bReturn &= stOption.LoadOption_Inspector(m_stOption.Inspector);
	bReturn &= stOption.LoadOption_BCR(m_stOption.BCR);
	bReturn &= stOption.LoadOption_PCB(m_stOption.PCB);
	bReturn &= stOption.LoadOption_SerialDevice(m_stOption.SerialDevice);
	bReturn &= stOption.LoadOption_VisionCam(m_stOption.VisionCam);
	
#ifdef USE_LIGHTBRD_MODE
	bReturn &= stOption.LoadOption_Light(m_stOption.PCB);
#endif

	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: HWND hWndOwner
// Qualifier	:
// Last Update	: 2016/5/18 - 18:41
// Desc.		: 주변 장치 초기화
//=============================================================================
void CTestManager_Device::InitDevicez(HWND hWndOwner /*= NULL*/)
{
	CString strText;

	// 바코드 리더기
	m_Device.BCR.SetOwnerHwnd(hWndOwner);
	m_Device.BCR.SetAckMsgID(WM_RECV_BARCODE);

	// 라벨 프린터
	m_Device.Printer.SetOwnerHwnd(hWndOwner);
	m_Device.Printer.SetExitEvent(m_hEvent_ProgramExit);
	m_Device.Printer.SetSemaphore(_T("Smph_Label_Printer"));
	m_Device.Printer.SetLogMsgID(WM_LOGMSG_LABELPRINTER);

	for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nIdxBrd++)
	{
		strText.Format(_T("Smph_CameraBoard_%02d"), nIdxBrd + 1);
		m_Device.PCBCamBrd[nIdxBrd].SetOwnerHwnd(hWndOwner);
		m_Device.PCBCamBrd[nIdxBrd].SetExitEvent(m_hEvent_ProgramExit);
		m_Device.PCBCamBrd[nIdxBrd].SetSemaphore(strText);
		m_Device.PCBCamBrd[nIdxBrd].SetLogMsgID(WM_LOGMSG_PCB_CAM);
		m_Device.PCBCamBrd[nIdxBrd].SetAckMsgID(WM_RECV_MAIN_BRD_ACK);
		//m_Device.PCBCamBrd[nIdxBrd].SetAckMsgID(WM_RECV_PCB_LIGHT);
	}

	// Light 보드
#ifdef USE_LIGHTBRD_MODE
	for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nLightBrdCount; nIdxBrd++)
	{
		strText.Format(_T("Smph_LightBoard_%02d"), nIdxBrd + 1);
		m_Device.PCBLightBrd[nIdxBrd].SetOwnerHwnd(hWndOwner);
		m_Device.PCBLightBrd[nIdxBrd].SetExitEvent(m_hEvent_ProgramExit);
		m_Device.PCBLightBrd[nIdxBrd].SetSemaphore(strText);
		m_Device.PCBLightBrd[nIdxBrd].SetLogMsgID(WM_LOGMSG_PCB_LIGHT);
		//m_Device.PCBLightBrd[nIdxBrd].SetAckMsgID(WM_RECV_MAIN_BRD_ACK);
		//m_Device.PCBLightBrd[nIdxBrd].SetAckMsgID(WM_RECV_PCB_LIGHT);
	}
#endif

	// Indicator 센서
	for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nIndigatorCnt; nIdxBrd++)
	{
		strText.Format(_T("Smph_IndicatorBoard_%02d"), nIdxBrd + 1);
		m_Device.Indicator[nIdxBrd].SetOwnerHwnd(hWndOwner);
		m_Device.Indicator[nIdxBrd].SetExitEvent(m_hEvent_ProgramExit);
		m_Device.Indicator[nIdxBrd].SetSemaphore(strText);
		m_Device.Indicator[nIdxBrd].SetLogMsgID(WM_LOGMSG_INDICATOR);
		//m_Device.PCBCamBrd[nIdxBrd].SetAckMsgID(WM_RECV_PCB_LIGHT);
	}

	// 프레임 그래버 보드 (NTSC)
	m_Device.Cat3DCtrl.SetOwner(hWndOwner);
	m_Device.Cat3DCtrl.SetGrabberBoardType(GBT_XECAP_400EH);
	m_Device.Cat3DCtrl.SetWM_ChgStatus(WM_CAMERA_CHG_STATUS);
	m_Device.Cat3DCtrl.SetWM_RecvVideo(WM_CAMERA_RECV_VIDEO);

	// IO
	m_Device.DigitalIOCtrl.AXTInit();
	m_Device.DigitalIOCtrl.SetOwnerHwnd(hWndOwner);
	m_Device.DigitalIOCtrl.Set_WM_BitChanged(WM_RECV_DIO_BIT);
	m_Device.DigitalIOCtrl.Set_WM_FirstRead(WM_RECV_DIO_FST_READ);

	// 비전 카메라
// 	m_Device.VisionCam.SetOwner(hWndOwner);
// 	m_Device.VisionCam.SetWinMessage(WM_COMM_STATUS_VC, WM_VC_RECV_VIDEO);
// 	m_Device.VisionCam.SetDeviceType(0)

	// 비전 조명
// 	m_Device.IF_Illumination.SetOwnerHwnd(hWndOwner);
// 	m_Device.IF_Illumination.SetExitEvent(m_hEvent_ProgramExit);
// 	m_Device.IF_Illumination.SetSemaphore(strText);
// 	m_Device.IF_Illumination.SetLogMsgID(WM_LOGMSG_VISION_LIGHT);

	// MES
#ifdef USE_EVMS_MODE
	m_Device.Mes.SetOwnerHWND(hWndOwner);
	m_Device.Mes.SetWmLog(WM_LOGMSG);
	m_Device.Mes.SetWmRecv(WM_RECV_MES);
	m_Device.Mes.SetWmCommStatus(WM_COMM_STATUS_MES);
	m_Device.Mes.SetExitEvent(m_hEvent_ProgramExit);
	m_Device.Mes.SetAddress(ntohl(m_stOption.MES.Address.dwAddress), m_stOption.MES.Address.dwPort);
#endif

	OnInitialDevice();
}

//=============================================================================
// Method		: ConnectDevicez
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/18 - 18:58
// Desc.		:
//=============================================================================
void CTestManager_Device::ConnectDevicez()
{
	OnShowSplashScreen(TRUE, _T("Connect Devices"));
	
	OnShowSplashScreen(TRUE, _T("Connect Barcode"));
	ConnectBCR(TRUE);

	//OnShowSplashScreen(TRUE, _T("Connect Label Printer"));
	//ConnectLabelPrinter(TRUE);

	OnShowSplashScreen(TRUE, _T("Connect Digital I/O & Motion"));
	ConnectIO(TRUE);
	ConnectMotor(TRUE);


	OnShowSplashScreen(TRUE, _T("Connect Indicator"));
	ConnectIndicator(TRUE);

	OnShowSplashScreen(TRUE, _T("Connect Frame Grabber Board"));
	ConnectGrabberBrd_ComArt(TRUE);
//	ConnectGrabberBrd_DAQ(TRUE);

	OnShowSplashScreen(TRUE, _T("Connect Interface Board"));
	ConnectPCB_CameraBrd(TRUE);
}

//=============================================================================
// Method		: DisconnectDevicez
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/31 - 22:11
// Desc.		:
//=============================================================================
void CTestManager_Device::DisconnectDevicez()
{
	OnShowSplashScreen(TRUE, _T("Disconnect Devices"));

	OnShowSplashScreen(TRUE, _T("Disconnect Frame Grabber Board"));
	ConnectGrabberBrd_ComArt(FALSE);
//	ConnectGrabberBrd_DAQ(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Camera Board"));
	ConnectPCB_CameraBrd(FALSE);

#ifdef USE_LIGHTBRD_MODE
	OnShowSplashScreen(TRUE, _T("Disconnect Light Board"));
	ConnectPCB_LightBrd(FALSE);
#endif

	OnShowSplashScreen(TRUE, _T("Disconnect Digital I/O & Motion"));
	ConnectIO(FALSE);
	//ConnectMotor(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Indicator"));
	ConnectIndicator(FALSE);

// 	OnShowSplashScreen(TRUE, _T("Disconnect Vision Camera"));
// 	ConnectVisionCam(FALSE);

// 	OnShowSplashScreen(TRUE, _T("Disconnect Vision Light Ctrl Board"));
// 	ConnectVisionLight(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Barcode"));
	ConnectBCR(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Label Printer"));
	ConnectLabelPrinter(FALSE);

	OnShowSplashScreen(FALSE);
}


//=============================================================================
// Method		: ConnectGrabberBrd
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/10/6 - 20:28
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectGrabberBrd_ComArt(__in BOOL bConnect)
{
	// 카메라 영상 중지
	m_Device.Cat3DCtrl.Stop_CaptureAll();
	m_Device.Cat3DCtrl.Final_End();
	m_Device.Cat3DCtrl.Close_Board(); // NTSC

	if (bConnect)
	{
		if (m_Device.Cat3DCtrl.Search_Cat3d())
		{
			if (m_Device.Cat3DCtrl.Open_Board(1)) // NTSC
			{
				m_Device.Cat3DCtrl.Init_Begin();

				OnSetStatus_GrabberBrd_ComArt(TRUE);

				// 밝기, 채도 설정
				m_Device.Cat3DCtrl.SetBrightnessContrast(110, 110);

				// 기본 프레임 30으로 설정 (XCap400E 모델)
				m_Device.Cat3DCtrl.SetFPS();

				// 영상 시작
				m_Device.Cat3DCtrl.Start_CaptureAll();

				return TRUE;
			}
			else
			{
				OnLog_Err(_T("Analog Grabber Board : Open Fail"));
			}
		}
		else
		{
			OnLog_Err(_T("Analog Grabber Board : PCI Board Recognition Fail in Device"));
		}
	}
	else
	{
		OnSetStatus_GrabberBrd_ComArt(FALSE);

		return TRUE;
	}

	return FALSE;
}


//=============================================================================
// Method		: ConnectBCR
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/10/6 - 19:59
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectBCR(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString strComPort;
		HANDLE hBCR = NULL;

		strComPort.Format(_T("//./COM%d"), m_stOption.BCR.ComPort.Port);

		hBCR = m_Device.BCR.Connect(strComPort,
									m_stOption.BCR.ComPort.BaudRate,
									m_stOption.BCR.ComPort.Parity,
									m_stOption.BCR.ComPort.StopBits,
									m_stOption.BCR.ComPort.ByteSize);

		if (NULL != hBCR)// 접속에 성공
		{
			OnLog(_T("BCR: COMM%d Comm Connect Success"), m_stOption.BCR.ComPort.Port);
			OnSetStatus_BCR(TRUE);
		}
		else
		{
			OnLog_Err(_T("BCR: COMM%d Comm Connect Fail"), m_stOption.BCR.ComPort.Port);
			OnSetStatus_BCR(FALSE);
		}
	}
	else // 연결 해제
	{
		if (m_Device.BCR.Disconnect())
		{
			OnSetStatus_BCR(TRUE);
		}
		else
		{
			;
		}
	}

	return TRUE;
}


BOOL CTestManager_Device::ConnectLabelPrinter(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString strComPort;
		HANDLE hBCR = NULL;

		strComPort.Format(_T("//./COM%d"), m_stOption.BCR.ComPort_Label.Port);

		for (int i = 0; i < 30; i++)
		{
			m_Device.Printer.Disconnect();

			hBCR = m_Device.Printer.Connect(strComPort,
				m_stOption.BCR.ComPort_Label.BaudRate,
				m_stOption.BCR.ComPort_Label.Parity,
				m_stOption.BCR.ComPort_Label.StopBits,
				m_stOption.BCR.ComPort_Label.ByteSize);

			if (hBCR != NULL)
				break;
		}

		if (NULL != hBCR)// 접속에 성공
		{
			OnLog(_T("Printer: COMM%d Comm Connect Success"), m_stOption.BCR.ComPort_Label.Port);
			OnSetStatus_LabelPrinter(TRUE);
		}
		else
		{
			OnLog_Err(_T("Printer: COMM%d Comm Connect Fail"), m_stOption.BCR.ComPort_Label.Port);
			OnSetStatus_LabelPrinter(FALSE);
		}
	}
	else // 연결 해제
	{
		if (m_Device.Printer.Disconnect())
		{
			OnSetStatus_LabelPrinter(FALSE);
		}
		else
		{
			;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectPCB_CameraBrd
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/9/28 - 18:15
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectPCB_CameraBrd(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString	strLog;
		CString strComPort;
		HANDLE hBCR = NULL;

		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nIdxBrd++)
		{
			strComPort.Format(_T("//./COM%d"), m_stOption.PCB.ComPort_CamBrd[nIdxBrd].Port);

			//for (int i = 0; i < 5; i++)
			{
				m_Device.PCBCamBrd[nIdxBrd].Disconnect();

				hBCR = m_Device.PCBCamBrd[nIdxBrd].Connect(strComPort,
					m_stOption.PCB.ComPort_CamBrd[nIdxBrd].BaudRate,
					m_stOption.PCB.ComPort_CamBrd[nIdxBrd].Parity,
					m_stOption.PCB.ComPort_CamBrd[nIdxBrd].StopBits,
					m_stOption.PCB.ComPort_CamBrd[nIdxBrd].ByteSize);
			}
			

			if (NULL != hBCR)// 접속에 성공
			{
				OnLog(_T("PCB Camera Board: COM %d Comm Connect Success"), m_stOption.PCB.ComPort_CamBrd[nIdxBrd].Port);
				OnSetStatus_PCBCamBrd(COMM_STATUS_CONNECT, nIdxBrd);

				// 보드 체크로 통신 싱크 확인
				BYTE byPort = 0;
				if (m_Device.PCBCamBrd[nIdxBrd].Send_BoardCheck(byPort) == SRC_OK)
				{
					OnSetStatus_PCBCamBrd(COMM_STATUS_SYNC_OK, nIdxBrd);
				}
				else
				{
					OnSetStatus_PCBCamBrd(COMM_STATUS_NOTCONNECTED, nIdxBrd);
				}
			}
			else
			{
				OnLog_Err(_T("PCB Camera Board: COM %d Comm Connect Fail"), m_stOption.PCB.ComPort_CamBrd[nIdxBrd].Port);
				OnSetStatus_PCBCamBrd(COMM_STATUS_NOTCONNECTED, nIdxBrd);
			}
		}
	}
	else // 연결 해제
	{
		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nIdxBrd++)
		{
			if (m_Device.PCBCamBrd[nIdxBrd].Disconnect())
			{
				OnSetStatus_PCBCamBrd(COMM_STATUS_DISCONNECT, nIdxBrd);
			}
			else
			{
				;
			}
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectPCB_LightBrd
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/6/28 - 16:27
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectPCB_LightBrd(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString	strLog;
		CString strComPort;
		HANDLE hBCR = NULL;

		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nLightBrdCount; nIdxBrd++)
		{
			strComPort.Format(_T("//./COM%d"), m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Port);

			hBCR = m_Device.PCBLightBrd[nIdxBrd].Connect(strComPort,
				m_stOption.PCB.ComPort_LightBrd[nIdxBrd].BaudRate,
				m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Parity,
				m_stOption.PCB.ComPort_LightBrd[nIdxBrd].StopBits,
				m_stOption.PCB.ComPort_LightBrd[nIdxBrd].ByteSize);

			if (NULL != hBCR)// 접속에 성공
			{
				OnLog(_T("PCB Light Board: COM %d Comm Connect Success"), m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Port);
				OnSetStatus_PCBLightBrd(COMM_STATUS_CONNECT, nIdxBrd);

				// 보드 체크로 통신 싱크 확인
				BYTE byPort = 0;
				if (m_Device.PCBLightBrd[nIdxBrd].Send_BoardCheck(byPort))
				{
					if (byPort == '1')
					OnSetStatus_PCBLightBrd(COMM_STATUS_SYNC_OK, nIdxBrd);
					else
						OnLog_Err(_T("PCB Light Board: COM %d Board Check Fail"), m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Port);
				}
			}
			else
			{
				OnLog_Err(_T("PCB Light Board: COM %d Comm Connect Fail"), m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Port);
				OnSetStatus_PCBLightBrd(COMM_STATUS_NOTCONNECTED, nIdxBrd);
			}
		}
	}
	else // 연결 해제
	{
		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nLightBrdCount; nIdxBrd++)
		{
			if (m_Device.PCBLightBrd[nIdxBrd].Disconnect())
			{
				OnSetStatus_PCBCamBrd(COMM_STATUS_DISCONNECT, nIdxBrd);
			}
			else
			{
				;
			}
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectMotion
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/10/16 - 16:13
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectIO(__in BOOL bConnect)
{
	if (bConnect)
	{
		if (m_Device.DigitalIOCtrl.AXTState())
		{
			OnLog(_T("IO Board Connect Success"));
			OnSetStatus_IOBoard(TRUE);

// 			if (m_Device.DigitalIOCtrl.GetOutStatus(DO_BoardPower) == FALSE)
// 			{
// 				m_Device.DigitalIOCtrl.Set_DO_Status(DO_BoardPower, IO_SignalT_SetOn);
// 				Sleep(3000);
// 			}
		}
		else
		{
			OnLog(_T("IO Board Connect Fail"));
			OnSetStatus_IOBoard(FALSE);
		}
	}
	else
	{
		for (UINT nPort = 0; nPort < DO_NotUseBit_Max; nPort++)
		{
			m_Device.DigitalIOCtrl.Set_DO_Status(nPort, IO_SignalT_SetOff);
			m_Device.DigitalIOCtrl.Set_DO_Status(nPort, IO_SignalT_ToggleStop);
		}

//		m_Device.DigitalIOCtrl.Set_DO_Status(DO_BoardPower, IO_SignalT_SetOn);
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectMotor
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/6/28 - 10:24
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectMotor(__in BOOL bConnect)
{
	if (bConnect)
	{
		if (m_Device.DigitalIOCtrl.AXTState())
		{
			OnLog(_T("Motor Board Connect Success"));
			OnSetStatus_MotorBoard(TRUE);



			//SetMotorOrigin_All(); //HTH
		}
		else
		{
			OnLog(_T("Motor Board Connect Fail"));
			OnSetStatus_MotorBoard(FALSE);
		}
	}
	else
	{

	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectIndicator
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/1/2 - 16:51
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectIndicator(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString strComPort;
		CStringA strCheck;
		HANDLE hBCR = NULL;

		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nIndigatorCnt; nIdxBrd++)
		{
			strComPort.Format(_T("//./COM%d"), m_stOption.BCR.ComPort_Motor[nIdxBrd].Port);

			hBCR = m_Device.Indicator[nIdxBrd].Connect(strComPort,
				m_stOption.BCR.ComPort_Motor[nIdxBrd].BaudRate,
				m_stOption.BCR.ComPort_Motor[nIdxBrd].Parity,
				m_stOption.BCR.ComPort_Motor[nIdxBrd].StopBits,
				m_stOption.BCR.ComPort_Motor[nIdxBrd].ByteSize);

			if (NULL != hBCR)// 접속에 성공
			{
				OnLog(_T("Indicator: COMM%d Comm Contact OK"), m_stOption.BCR.ComPort_Motor[nIdxBrd].Port);
				OnSetStatus_Indicator(COMM_STATUS_CONNECT, nIdxBrd);

				if (m_Device.Indicator[nIdxBrd].Send_PortCheck(strCheck))
				{
					if (strCheck == _T("ID"))
					{
						OnSetStatus_Indicator(COMM_STATUS_SYNC_OK, nIdxBrd);
						b_IndicatorConnect[nIdxBrd] = TRUE;
					}
					else
					{
						b_IndicatorConnect[nIdxBrd] = FALSE;
					}
				}
			}
			else
			{
				OnLog_Err(_T("Indicator: COMM%d Comm Contact Fail"), m_stOption.BCR.ComPort_Motor[nIdxBrd].Port);
				OnSetStatus_Indicator(COMM_STATUS_NOTCONNECTED, nIdxBrd);
				b_IndicatorConnect[nIdxBrd] = FALSE;
			}
		}
	}
	else // 연결 해제
	{
		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nIndigatorCnt; nIdxBrd++)
		{
			if (m_Device.Indicator[nIdxBrd].Disconnect())
			{
				b_IndicatorConnect[nIdxBrd] = FALSE;
				OnSetStatus_Indicator(COMM_STATUS_DISCONNECT, nIdxBrd);
			}
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: SetMotorOrigin_All
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/1/14 - 13:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetMotorOrigin_All()
{
	LRESULT lReturn = RCA_OK;
	
	// 보드 OPEN 상태 확인
	if (FALSE == m_Device.MotionManager.GetBoardOpen())
		return RC_Motion_Err_BoardOpen;

	if (FALSE == m_Device.MotionManager.SetAllAmpCtr(ON))
		return RC_Invalid_Handle;

	if (FALSE == m_Device.MotionManager.SetAllAlarmClear())
		return RC_Invalid_Handle;

	for (UINT nAxis = 0; nAxis < (UINT)m_Device.MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (TRUE == m_Device.MotionManager.GetAxisUseStatus(nAxis))
		{
			if (FALSE == m_Device.MotionManager.SetOriginReset(nAxis))
			{
				//m_Log.LogMsg_Err(_T("%s, Origin AXIS[%d]"), g_szResultCode[RC_Motor_Err_Reset], nAxis);
				return RC_Motor_Err_Reset;
			}

			if (FALSE == m_Device.MotionManager.SetMotorEStop(nAxis))
			{
				return RC_Motor_Err_EStop;
			}
		}


	}
	BOOL bOrigin = TRUE;

	bOrigin = m_Device.MotionManager.SetMotorOrigin(AX_StageX);
	bOrigin &= m_Device.MotionManager.SetMotorOrigin(AX_StageY);

	if (FALSE == bOrigin)
		return RCA_OK; // RC_Motion_Err_Origin_Start;

	clock_t Start_tm = clock();

	while (1)
	{
		if (((DWORD)clock() - Start_tm) > DEF_MOTOR_ORIGIN_TIME)
		{
			m_Device.MotionManager.SetOriginStop(AX_StageX);
			m_Device.MotionManager.SetOriginStop(AX_StageY);
			break;
		}

		if (m_Device.MotionManager.GetOriginStatus(AX_StageX) || m_Device.MotionManager.GetOriginStatus(AX_StageY))
		{
			break;
		}

		DoEvents(100);
	}
	
	// origin
	bOrigin = m_Device.MotionManager.SetMotorOrigin(AX_StageDistance);

	if (FALSE == bOrigin)
		return RCA_OK; // RC_Motion_Err_Origin_Start;

	Start_tm = clock();

	while (1)
	{
		if (((DWORD)clock() - Start_tm) > DEF_MOTOR_ORIGIN_TIME)
		{
			m_Device.MotionManager.SetOriginStop(AX_StageDistance);
			break;
		}

		if (m_Device.MotionManager.GetOriginStatus(AX_StageDistance))
		{
			break;
		}

		DoEvents(100);
	}

	for (UINT nPort = 0; nPort < DO_NotUseBit_Max; nPort++)
	{
		m_Device.DigitalIOCtrl.Set_DO_Status(nPort, IO_SignalT_SetOff);
		m_Device.DigitalIOCtrl.Set_DO_Status(nPort, IO_SignalT_ToggleStop);
	}

	return lReturn;
}

//=============================================================================
// Method		: SetAjin_Motor
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in long lAxis
// Parameter	: __in AXT_MOTION_ABSREL_MODE enABSREL
// Parameter	: __in double dbPos
// Qualifier	:
// Last Update	: 2018/1/14 - 13:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetAjin_Motor(__in long lAxis, __in AXT_MOTION_ABSREL_MODE enABSREL, __in double dbPos)
{
	LRESULT lReturn = RCA_OK;

	// Exception Process...
	switch (lAxis)
	{
	case AX_StageDistance:

		// Y축의 지령 펄스가 0 ~ 100000.0 사이이면,
// 		if (120000.0 < dbPos
// 			&& 250000.0 >= dbPos)
// 		{
// 			// X축의 위치가 0 보다 크면 예외처리.
// 			if (40000.0 < m_Device.MotionManager.GetCurrentPos(AX_StageX))
// 			{
// 				if (!m_Device.MotionManager.MotorAxisMove(enABSREL, AX_StageX, 0))
// 				{
// 					lReturn = RCA_MachineCheck;
// 					break;
// 				}
// 
// 				lReturn = RCA_MachineExpcetion;
// 			}
// 		}
// 
// 		if (RCA_OK == lReturn)
// 		{
// 			if (!m_Device.MotionManager.MotorAxisMove(enABSREL, lAxis, dbPos))
// 			{
// 				lReturn = RCA_MachineCheck;
// 			}
// 		}

		if (RCA_OK == lReturn)
		{
			if (!m_Device.MotionManager.MotorAxisMove(enABSREL, lAxis, dbPos))
			{
				lReturn = RCA_MachineCheck;
			}
		}


		break;

	default:

		if (RCA_OK == lReturn)
		{
			if (!m_Device.MotionManager.MotorAxisMove(enABSREL, lAxis, dbPos))
			{
				lReturn = RCA_MachineCheck;
			}
		}

		break;
	}

	return lReturn;
}



LRESULT CTestManager_Device::SetAjin_Motor_EStop_All()
{
	LRESULT lReturn = RCA_OK;

	if (!m_Device.MotionManager.SetAllMotorEStop())
	{
		lReturn = RCA_MachineCheck;
	}

	return lReturn;
}

LRESULT CTestManager_Device::SetAjin_IO(__in UINT nIO, __in enum_IO_SignalType nSignal)
{
	LRESULT lReturn = RCA_OK;

	if (m_Device.DigitalIOCtrl.Set_DO_Status(nIO, nSignal) != AXT_RT_SUCCESS)
	{
		lReturn = RCA_MachineCheck;
	}

	return lReturn;
}

//=============================================================================
// Method		: SetAjin_TowerLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enIO_TowerLamp enItem
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/1/14 - 13:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetAjin_TowerLamp(__in enIO_TowerLamp enItem, __in BOOL bOnOff)
{
	LRESULT lReturn = RCA_OK;
	
	enum_IO_SignalType SignalType;

	if (ON == bOnOff)
	{
		SignalType = IO_SignalT_SetOn;
	}
	else
	{
		SignalType = IO_SignalT_SetOff;
	}

	switch (enItem)
	{
	case TowerLampRed:
		m_Device.DigitalIOCtrl.Set_DO_Status(DO_TowerLamp_Red, SignalType);
		break;
	case TowerLampYellow:
		m_Device.DigitalIOCtrl.Set_DO_Status(DO_TowerLamp_Yellow, SignalType);
		break;
	case TowerLampGreen:
		m_Device.DigitalIOCtrl.Set_DO_Status(DO_TowerLamp_Green, SignalType);
		break;
	case TowerLampBuzzer:
		m_Device.DigitalIOCtrl.Set_DO_Status(DO_TowerLamp_Buzzer, SignalType);
		break;
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: SetAjin_Cylinder
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enIO_Cylinder enItem
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/1/14 - 13:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetAjin_Cylinder(__in enIO_Cylinder enItem, __in BOOL bOnOff)
{
	LRESULT lReturn = RCA_OK;

	enum_IO_SignalType SignalType;

	if (ON == bOnOff)
	{
		SignalType = IO_SignalT_SetOn;
	}
	else
	{
		SignalType = IO_SignalT_SetOff;
	}

	switch (enItem)
	{
	case CylinderModule:
// 		m_Device.DigitalIOCtrl.Set_DO_Status(DO_FixCylinder, IO_SignalT_SetOff);
// 		m_Device.DigitalIOCtrl.Set_DO_Status(DO_UnFixCylinder, IO_SignalT_SetOff);
// 
// 		if (ON == bOnOff)
// 			m_Device.DigitalIOCtrl.Set_DO_Status(DO_FixCylinder, IO_SignalT_SetOn);
// 		else
// 			m_Device.DigitalIOCtrl.Set_DO_Status(DO_UnFixCylinder, IO_SignalT_SetOn);

		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: SetLuriCamBrd_Port
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nPort
// Qualifier	:
// Last Update	: 2018/1/14 - 13:47
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetLuriCamBrd_Port(__in UINT nPort /*= 0*/)
{

	LRESULT lReturn = RCA_OK;

	BYTE byBrdNo;

	lReturn = m_Device.PCBCamBrd[nPort].Send_BoardCheck(byBrdNo);

	return lReturn;
}

//=============================================================================
// Method		: SetLuriCamBrd_Volt
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in float fVolt
// Parameter	: __in UINT nPort
// Qualifier	:
// Last Update	: 2018/1/14 - 13:47
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetLuriCamBrd_Volt(__in float* fVolt, __in UINT nPort /*= 0*/)
{
	LRESULT lReturn = RCA_OK;

	lReturn = m_Device.PCBCamBrd[nPort].Send_Volt(fVolt);

	return lReturn;
}
//=============================================================================
// Method		: SetLuriCamBrd_OpenShort
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in float fVolt
// Parameter	: __in UINT nPort
// Qualifier	:
// Last Update	: 2018/1/14 - 13:47
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetLuriCamBrd_OpenShort(__out bool& bCurrent, __in UINT nPort /*= 0*/)
{
	LRESULT lReturn = RCA_OK;

	lReturn = m_Device.PCBCamBrd[nPort].Send_OpenShort(bCurrent);

	return lReturn;
}
//=============================================================================
// Method		: SetLuriCamBrd_Current
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __out int & iCurrent
// Parameter	: __in UINT nPort
// Qualifier	:
// Last Update	: 2018/1/14 - 13:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetLuriCamBrd_Current(__out double* iCurrent, __in UINT nPort /*= 0*/)
{
	LRESULT lReturn = RCA_OK;

	lReturn = m_Device.PCBCamBrd[nPort].Send_GetCurrent(iCurrent);

	return lReturn;
}



LRESULT CTestManager_Device::SetLuriCamBrd_IRLED(__in BOOL bOnOff, __in UINT nPort /*= 0*/)
{
	LRESULT lReturn = RCA_OK;

	lReturn = m_Device.PCBCamBrd[nPort].Send_IRLED(bOnOff);

	return lReturn;
}

//=============================================================================
// Method		: SetLuriLightBrd_Port
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nPort
// Qualifier	:
// Last Update	: 2018/1/14 - 13:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetLuriLightBrd_Port(__in UINT nPort /*= 0*/)
{
	LRESULT lReturn = RCA_OK;
	BYTE byBrdNo;

	lReturn = m_Device.PCBLightBrd[nPort].Send_BoardCheck(byBrdNo);

	return lReturn;
}

//=============================================================================
// Method		: SetLuriLightBrd_Control
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nCH
// Parameter	: __in float fVolt
// Parameter	: __in int iCurrent
// Parameter	: __in UINT nPort
// Qualifier	:
// Last Update	: 2018/1/14 - 13:47
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetLuriLightBrd_Control(__in UINT nCH, __in float fVolt, __in int iCurrent, __in UINT nPort /*= 0*/)
{
	LRESULT lReturn;

	lReturn = m_Device.PCBLightBrd[nPort].Send_OutputVolt(nCH, fVolt);

	if (RCA_OK == lReturn)
		return lReturn;

	Sleep(1000);

	lReturn = m_Device.PCBLightBrd[nPort].Send_AmbientLightCurrent(nCH, iCurrent);

	return lReturn;
}

//=============================================================================
// Method		: SetLuriLightBrd_Volt
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nCH
// Parameter	: __in float fVolt
// Parameter	: __in UINT nPort
// Qualifier	:
// Last Update	: 2018/1/14 - 13:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetLuriLightBrd_Volt(__in UINT nCH, __in float fVolt, __in UINT nPort /*= 0*/)
{
	LRESULT lReturn = RCA_OK;

	lReturn = m_Device.PCBLightBrd[nPort].Send_OutputVolt(nCH, fVolt);

	return lReturn;
}

//=============================================================================
// Method		: SetLuriLightBrd_Current
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nCH
// Parameter	: __in int iCurrent
// Parameter	: __in UINT nPort
// Qualifier	:
// Last Update	: 2018/1/14 - 13:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_Device::SetLuriLightBrd_Current(__in UINT nCH, __in int iCurrent, __in UINT nPort /*= 0*/)
{
	LRESULT lReturn = RCA_OK;

	lReturn = m_Device.PCBLightBrd[nPort].Send_AmbientLightCurrent(nCH, iCurrent);

	return lReturn;
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/28 - 20:10
// Desc.		:
//=============================================================================
void CTestManager_Device::OnInitialize()
{
	for (UINT nIdx = 0; nIdx < Indicator_Max; nIdx++)
	{
		b_IndicatorConnect[nIdx] = FALSE;
	}

	
}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/28 - 20:10
// Desc.		:
//=============================================================================
void CTestManager_Device::OnFinalize()
{
	for (int i = 0; i < DO_NotUseBit_Max; i++)
	{
		m_Device.DigitalIOCtrl.Set_DO_Status(i, IO_SignalT_SetOff);
	}

	// UnFix
	//m_Device.DigitalIOCtrl.Set_DO_Status(DO_UnFixCylinder, IO_SignalT_SetOn);

	// Board Power Off
	float fVoltageOff[2] = { 0, };
	SetLuriCamBrd_Volt(fVoltageOff);

	Sleep(500);
}

void CTestManager_Device::OnInitialDevice()
{
// 	for (int i = 0; i < DO_NotUseBit_Max; i++)
// 	{
// 		m_Device.DigitalIOCtrl.Set_DO_Status(i, IO_SignalT_SetOff);
// 	}

	// Camera 보드
//	m_Device.DigitalIOCtrl.Set_DO_Status(DO_BoardPower, IO_SignalT_SetOn);

	// 버튼
	m_Device.DigitalIOCtrl.Set_DO_Status(DO_LampStartBtn, IO_SignalT_SetOn);
	m_Device.DigitalIOCtrl.Set_DO_Status(DO_LampStopBtn, IO_SignalT_SetOn);

	// Board Power Off
	float fVoltageOff[2] = { 0, };
	SetLuriCamBrd_Volt(fVoltageOff);

	Sleep(500);
}
