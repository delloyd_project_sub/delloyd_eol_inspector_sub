// Dlg_UserConfigOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_UserConfigOp.h"
#include "afxdialogex.h"

#include "File_Model.h"

#define		IDC_WND_USERCONFIGOP	1000
#define		IDC_BN_SAVE				2000

// CDlg_UserConfigOp 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_UserConfigOp, CDialogEx)

CDlg_UserConfigOp::CDlg_UserConfigOp(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_UserConfigOp::IDD, pParent)
{
	m_pstUserConfigInfo = NULL;
}

CDlg_UserConfigOp::~CDlg_UserConfigOp()
{
}

void CDlg_UserConfigOp::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlg_UserConfigOp, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_SAVE, OnBnClickedBnSave)
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CDlg_UserConfigOp 메시지 처리기입니다.


int CDlg_UserConfigOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_wndUserConfig.SetOwner(this);
	m_wndUserConfig.Create(NULL, _T("Operator Config"), dwStyle /*| WS_BORDER*/, rectDummy, this, IDC_WND_USERCONFIGOP);

	m_bn_Save.Create(_T("Save (Apply)"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);
	m_bn_Save.SetMouseCursorHand();

	Set_UserConfigInfo(m_pstUserConfigInfo);

	return 0;
}


void CDlg_UserConfigOp::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 5;
	int iCateSpacing = 10;
	int iMagin = 10;
	int iLeft = iMagin;
	int iTop = iMagin;
	int iWidth = cx - (iMagin * 2);
	int iHeight = cy - (iMagin * 2);

	int iCtrlHeight = 35;
	int iCtrlWidth = 147;

	iLeft = cx - iMagin - iCtrlWidth;
	m_bn_Save.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft = iMagin;
	iTop += iCtrlHeight + iSpacing;
	m_wndUserConfig.MoveWindow(iLeft, iTop, iWidth, iHeight - (iCtrlHeight + iSpacing));
}


void CDlg_UserConfigOp::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	lpMMI->ptMaxTrackSize.x = 650;
	lpMMI->ptMaxTrackSize.y = 650;

	lpMMI->ptMinTrackSize.x = 650;
	lpMMI->ptMinTrackSize.y = 650;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


BOOL CDlg_UserConfigOp::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreCreateWindow(cs);
}


BOOL CDlg_UserConfigOp::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDlg_UserConfigOp::OnBnClickedBnSave()
{
	Get_UserConfigInfo(*m_pstUserConfigInfo);

	CFile_Model	m_fileModel;

	CString szOperatorPath;
	szOperatorPath.Format(_T("%s\\OperatorConfig.ini"), m_szUserConfigPath);

	if (m_fileModel.Save_UserConfigInfo(szOperatorPath, m_pstUserConfigInfo))
	{
		Set_UserConfigInfo(m_pstUserConfigInfo);
	}

	AfxMessageBox(_T("Operator information has been saved."));
}


void CDlg_UserConfigOp::Set_UserConfigInfo(__in const ST_UserConfigInfo* pstUserConfigInfo)
{
	m_wndUserConfig.Set_UserConfigInfo(pstUserConfigInfo);
}


void CDlg_UserConfigOp::Get_UserConfigInfo(__out ST_UserConfigInfo& stUserConfigInfo)
{
	m_wndUserConfig.Get_UserConfigInfo(stUserConfigInfo);
}


void CDlg_UserConfigOp::SetPath(__in LPCTSTR szUserConfigPath)
{
	m_szUserConfigPath = szUserConfigPath;
	m_wndUserConfig.SetPath(m_szUserConfigPath);
}