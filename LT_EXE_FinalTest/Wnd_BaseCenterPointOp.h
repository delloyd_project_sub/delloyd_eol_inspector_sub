﻿#ifndef Wnd_BaseCenterPointOp_h__
#define Wnd_BaseCenterPointOp_h__

#pragma once

#include "Wnd_CenterPointOp.h"

// CWnd_BaseCenterPointOp

class CWnd_BaseCenterPointOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseCenterPointOp)

public:
	CWnd_BaseCenterPointOp();
	virtual ~CWnd_BaseCenterPointOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_CenterPointOp m_wndCenterPointOp[TICnt_CenterPoint];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseCenterPointOp_h__
