﻿// Wnd_ParticleOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_ParticleOp.h"
#include "resource.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_ParticleOp
typedef enum ParticleOptID
{
	IDC_BTN_ITEM   = 1001,
	IDC_CMB_ITEM   = 2001,
	IDC_EDT_ITEM   = 3001,
	IDC_LIST_ITEM  = 4001,
	IDC_BTN_INDEX  = 5001,
};

IMPLEMENT_DYNAMIC(CWnd_ParticleOp, CWnd)

CWnd_ParticleOp::CWnd_ParticleOp()
{
	m_pstModelInfo	= NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_ParticleOp::~CWnd_ParticleOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_ParticleOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_ParticleOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_ParticleOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	for (UINT nItem = 0; nItem < STI_PAR_MAX; nItem++)
	{
		m_st_Item[nItem].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nItem].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nItem].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nItem].Create(g_szParticleStatic[nItem], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < BTN_PAR_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szParticleButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_PAR_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_PAR_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt = PtOp_ItemNum;
	int iMargin = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;

	int iList_H = iHeaderH + iIdxCnt * 15;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	iLeft = iMargin;
	iTop = iMargin;
	m_st_Item[STI_PAR_SENSITIVITY].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 1;
	m_ed_Item[EDT_PAR_SENSITIVITY].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_PAR_EDGE_W].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 1;
	m_ed_Item[EDT_PAR_EDGE_W].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop+= iSTHeight + iSpacing;
	m_st_Item[STI_PAR_EDGE_H].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 1;
	m_ed_Item[EDT_PAR_EDGE_H].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = cx - iMargin - iSTWidth;
	m_bn_Item[BTN_PAR_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft = iMargin;
	iTop += iSpacing + iSTHeight;
	m_st_Item[STI_PAR_OPT].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	iLeft = iMargin;
	iTop += iCtrl_H + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_ParticleOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (NULL == m_pstModelInfo)
		return;

	if (TRUE == bShow)
	{
		m_pstModelInfo->nTestMode = TIID_BlackSpot;
		m_pstModelInfo->nTestCnt = m_nTestItemCnt;
		m_pstModelInfo->nPicItem = PIC_BlackSpot;
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_PAR_TEST:
		GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_BlackSpot);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::SetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	CString strValue;


	m_List.SetPtr_Particle(&m_pstModelInfo->stBlackSpot[m_nTestItemCnt]);
	m_List.SetImageSize(m_pstModelInfo->dwWidth, m_pstModelInfo->dwHeight);
	m_List.InsertFullData();

	strValue.Format(_T("%.f"), m_pstModelInfo->stBlackSpot[m_nTestItemCnt].stParticleOpt.fDustDis);
	m_ed_Item[EDT_PAR_SENSITIVITY].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->stBlackSpot[m_nTestItemCnt].stParticleOpt.iEdgeH);
	m_ed_Item[EDT_PAR_EDGE_H].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->stBlackSpot[m_nTestItemCnt].stParticleOpt.iEdgeW);
	m_ed_Item[EDT_PAR_EDGE_W].SetWindowText(strValue);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemOpt & stTestItemOpt
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::GetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	CString strValue;

	m_ed_Item[EDT_PAR_SENSITIVITY].GetWindowText(strValue);
	m_pstModelInfo->stBlackSpot[m_nTestItemCnt].stParticleOpt.fDustDis = (float)_ttof(strValue);

	m_ed_Item[EDT_PAR_EDGE_H].GetWindowText(strValue);
	m_pstModelInfo->stBlackSpot[m_nTestItemCnt].stParticleOpt.iEdgeH = _ttoi(strValue);

	m_ed_Item[EDT_PAR_EDGE_W].GetWindowText(strValue);
	m_pstModelInfo->stBlackSpot[m_nTestItemCnt].stParticleOpt.iEdgeW = _ttoi(strValue);
}
