﻿#ifndef List_Work_Color_h__
#define List_Work_Color_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Col_Worklist
{
	Col_W_Recode,
	Col_W_Time,
	Col_W_Equipment,
	Col_W_Model,
	Col_W_SWVersion,
	//Col_W_CameraType,
	Col_W_LOTNum,
	Col_W_Barcode,
	//Col_W_Operator,
	Col_W_Result,
	Col_W_RedR,
	Col_W_RedG,
	Col_W_RedB,
	Col_W_GreenR,
	Col_W_GreenG,
	Col_W_GreenB,
	Col_W_BlueR,
	Col_W_BlueG,
	Col_W_BlueB,
	Col_W_BlackR,
	Col_W_BlackG,
	Col_W_BlackB,
	Col_W_MaxCol, 
};

// 헤더
static const TCHAR*	g_lpszHeader_Col_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	_T("Red [R]"),
	_T("Red [G]"),
	_T("Red [B]"),
	_T("Green [R]"),
	_T("Green [G]"),
	_T("Green [B]"),
	_T("Blue [R]"),
	_T("Blue [G]"),
	_T("Blue [B]"),
	_T("Black [R]"),
	_T("Black [G]"),
	_T("Black [B]"),
	NULL
};

const int	iListAglin_Col_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Col_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};
// CList_Work_Color

class CList_Work_Color : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_Color)

public:
	CList_Work_Color();
	virtual ~CList_Work_Color();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_Color_h__


