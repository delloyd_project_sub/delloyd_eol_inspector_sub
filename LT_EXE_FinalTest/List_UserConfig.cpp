//*****************************************************************************
// Filename	: 	List_UserConfig.cpp
// Created	:	2017/9/24 - 16:35
// Modified	:	2017/9/24 - 16:35
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// List_UserConfig.cpp : implementation file
//

#include "stdafx.h"
#include "List_UserConfig.h"

// CList_UserConfig
typedef enum enBarcodeRefHeader
{
	RBCH_No,
	RBCH_ConfigTime,
	RBCH_UserID,
	RBCH_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader[] =
{
	_T("No"),					
	_T("Registration Time"),	
	_T("Operator ID"),

	NULL					   
};

const int	iListAglin[] =
{
	LVCFMT_LEFT,	 
	LVCFMT_CENTER,	 
	LVCFMT_CENTER,
};

// 540 기준
const int	iHeaderWidth[] =
{
	40, 	
	200,	
	200,
};


IMPLEMENT_DYNAMIC(CList_UserConfig, CListCtrl)

CList_UserConfig::CList_UserConfig()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_pHeadWidth = iHeaderWidth;
}

CList_UserConfig::~CList_UserConfig()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_UserConfig, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK,	&CList_UserConfig::OnNMClick)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_UserConfig message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
int CList_UserConfig::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_UserConfig::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
BOOL CList_UserConfig::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SINGLESEL | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_UserConfig::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	int index = pNMView->iItem;

	*pResult = 0;
}

//=============================================================================
// Method		: InitHeader
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/21 - 16:11
// Desc.		:
//=============================================================================
void CList_UserConfig::InitHeader()
{
	if (FALSE == m_bIntiHeader)
	{
		m_bIntiHeader = TRUE;

		int iMaxCol = RBCH_MaxCol;

		for (int nCol = 0; nCol < iMaxCol; nCol++)
		{
			InsertColumn(nCol, g_lpszHeader[nCol], iListAglin[nCol], iHeaderWidth[nCol]);
		}
	}

	for (int nCol = 0; nCol < RBCH_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, m_pHeadWidth[nCol]);
	}
}

//=============================================================================
// Method		: ResetOrderingNumbers
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 16:05
// Desc.		:
//=============================================================================
void CList_UserConfig::ResetOrderingNumbers()
{
	CString szText;

	for (int nIdx = 0; nIdx < GetItemCount(); nIdx++)
	{
		// TSH_No
		szText.Format(_T("%d"), nIdx + 1);
		SetItemText(nIdx, RBCH_No, szText);
	}
}

//=============================================================================
// Method		: SetSelectItem
// Access		: public  
// Returns		: void
// Parameter	: __in int nItem
// Qualifier	:
// Last Update	: 2017/9/25 - 18:28
// Desc.		:
//=============================================================================
void CList_UserConfig::SetSelectItem(__in int nItem)
{
	if (nItem < GetItemCount())
	{
		SetHotItem(nItem);
		SetItemState(nItem, LVIS_FOCUSED, LVIS_FOCUSED);
		SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);
		SetFocus();
	}
}

//=============================================================================
// Method		: InsertTestStep
// Access		: public  
// Returns		: void
// Parameter	: __in int nItem
// Parameter	: __in const ST_StepUnit * pTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 19:57
// Desc.		:
//=============================================================================
void CList_UserConfig::InsertUserConfigItem(__in int nItem, __in const ST_UserConfig* pUserConfig)
{
	ASSERT(GetSafeHwnd());
	if (NULL == pUserConfig)
		return;

	if (GetItemCount() <= nItem)
		return;

	int iNewCount = nItem;

	InsertItem(iNewCount, _T(""));

	CString szText;

	// RBCH_No
	szText.Format(_T("%d"), iNewCount);
	SetItemText(iNewCount, RBCH_No, szText);

 	// RBCH_ConfigTime
	SetItemText(iNewCount, RBCH_ConfigTime, pUserConfig->szRegistrationTime);

	// RBCH_UserID
	SetItemText(iNewCount, RBCH_UserID, pUserConfig->szOperatorID);

	// 번호 재부여
	ResetOrderingNumbers();

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: InsertTestStep
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepUnit * pTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 14:22
// Desc.		:
//=============================================================================
void CList_UserConfig::AddUserConfigItem(__in const ST_UserConfig* pUserConfig)
{
	ASSERT(GetSafeHwnd());
	if (NULL == pUserConfig)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));

	CString szText;

	// RBCH_No
	szText.Format(_T("%d"), iNewCount);
	SetItemText(iNewCount, RBCH_No, szText);

	// RBCH_ConfigTime
	SetItemText(iNewCount, RBCH_ConfigTime, pUserConfig->szRegistrationTime);

	// RBCH_UserID
	SetItemText(iNewCount, RBCH_UserID, pUserConfig->szOperatorID);

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: Set_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepInfo * pstInStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:35
// Desc.		:
//=============================================================================
void CList_UserConfig::Set_UserConfigInfo(__in const ST_UserConfigInfo* pstUserConfigInfo)
{
	m_stUserConfigInfo.Operatorlist.RemoveAll();
	DeleteAllItems();

	if (NULL != pstUserConfigInfo)
	{
		m_stUserConfigInfo.Operatorlist.Copy(pstUserConfigInfo->Operatorlist);

		for (UINT nIdx = 0; nIdx < pstUserConfigInfo->Operatorlist.GetCount(); nIdx++)
		{
			AddUserConfigItem(&(pstUserConfigInfo->Operatorlist[nIdx]));
		}
	}
}

//=============================================================================
// Method		: Get_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_StepInfo & stOutStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 23:27
// Desc.		:
//=============================================================================
void CList_UserConfig::Get_UserConfigInfo(__out ST_UserConfigInfo& stUserConfigInfo)
{
	stUserConfigInfo.Operatorlist.RemoveAll();
	stUserConfigInfo.Operatorlist.Copy(m_stUserConfigInfo.Operatorlist);
}

//=============================================================================
// Method		: Item_Add
// Access		: public  
// Returns		: void
// Parameter	: __in ST_StepUnit & stTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 23:22
// Desc.		:
//=============================================================================
void CList_UserConfig::Item_Add(__in ST_UserConfig& stUserConfig)
{
	POSITION posSel = GetFirstSelectedItemPosition();

	// 데이터 정상인가 확인
	if (m_stUserConfigInfo.Operatorlist.GetCount() != GetItemCount())
	{
		// 에러
		AfxMessageBox(_T("CList_UserConfig::Item_Add() Data Count Error"));
		return;
	}

	if (MAX_STEP_COUNT < m_stUserConfigInfo.Operatorlist.GetCount())
	{
		// 에러
		AfxMessageBox(_T("Limit Max Step"));
		return;
	}

	m_stUserConfigInfo.Operatorlist.Add(stUserConfig);
	AddUserConfigItem(&stUserConfig);
}

//=============================================================================
// Method		: Item_Insert
// Access		: public  
// Returns		: void
// Parameter	: __in ST_StepUnit & stTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 23:22
// Desc.		:
//=============================================================================
void CList_UserConfig::Item_Insert(__in ST_UserConfig& stUserConfig)
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		m_stUserConfigInfo.Operatorlist.InsertAt(iIndex, stUserConfig);

		InsertUserConfigItem(iIndex, &stUserConfig);
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Remove
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_UserConfig::Item_Remove()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		DeleteItem(iIndex);
		ResetOrderingNumbers();

		m_stUserConfigInfo.Operatorlist.RemoveAt(iIndex);

		// 아이템 선택 활성화
		if (iIndex < GetItemCount())
		{
			SetSelectItem(iIndex);
		}
		else
		{
			SetSelectItem(iIndex - 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Up
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_UserConfig::Item_Up()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		// 0번 인덱스는 위로 이동 불가
		if ((0 < iIndex) && (1 < GetItemCount()))
		{
			ST_UserConfig stUserConfig = m_stUserConfigInfo.Operatorlist.GetAt(iIndex);

			DeleteItem(iIndex);
			InsertUserConfigItem(iIndex - 1, &stUserConfig);

			m_stUserConfigInfo.Operatorlist.RemoveAt(iIndex);
			m_stUserConfigInfo.Operatorlist.InsertAt(iIndex - 1, stUserConfig);

			// 아이템 선택 활성화
			SetSelectItem(iIndex - 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Down
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_UserConfig::Item_Down()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		// 마지막 인덱스는 아래로 이동 불가
		if ((iIndex < (GetItemCount() - 1)) && (1 < GetItemCount()))
		{
			ST_UserConfig stUserConfig = m_stUserConfigInfo.Operatorlist.GetAt(iIndex);

			DeleteItem(iIndex);
			m_stUserConfigInfo.Operatorlist.RemoveAt(iIndex);

			// 변경되는 위치가 최하단이면, Insert 대신 Add 사용
			if ((iIndex + 1) < (GetItemCount()))
			{
				InsertUserConfigItem(iIndex + 1, &stUserConfig);
				m_stUserConfigInfo.Operatorlist.InsertAt(iIndex + 1, stUserConfig);
			}
			else
			{
				AddUserConfigItem(&stUserConfig);
				m_stUserConfigInfo.Operatorlist.Add(stUserConfig);
			}

			// 아이템 선택 활성화
			SetSelectItem(iIndex + 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}