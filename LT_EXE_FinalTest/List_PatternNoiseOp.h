﻿#ifndef List_PatternNoiseOp_h__
#define List_PatternNoiseOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_PatternNoiseOp
{
	PNOp_Object = 0,
	PNOp_PosX,
	PNOp_PosY,
	PNOp_Width,
	PNOp_Height,
	PNOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_PatternNoiseOp[] =
{
	_T(""),
	_T("Pos X"),
	_T("Pos Y"),
	_T("Width"),
	_T("Height"),
	NULL 
};

typedef enum enListItemNum_PatternNoiseOp
{
	PNOp_ItemNum = ROI_PN_MaxEnum,
};

static LPCTSTR	g_lpszItem_PatternNoiseOp[] =
{
	NULL
};

const int	iListAglin_PatternNoiseOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_PatternNoiseOp[] =
{
	70,
	70,
	70,
	70,
	70,
	70,
};
// List_RotateInfo

class CList_PatternNoiseOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_PatternNoiseOp)

public:
	CList_PatternNoiseOp();
	virtual ~CList_PatternNoiseOp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Brightenss(ST_LT_TI_PatternNoise* pstPatternNoise)
	{
		if (pstPatternNoise == NULL)
			return;

		m_pstPatternNoise = pstPatternNoise;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth  = nWidth;
		m_nHeight = nHeight;
	};

protected:

	ST_LT_TI_PatternNoise*  m_pstPatternNoise;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData			(UINT nRow, UINT nCol, int  iValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);
	
	afx_msg void	OnEnKillFocusEdit		();
	//afx_msg void	OnEnKillFocusCombo		();
	//afx_msg void	OnEnSelectFocusCombo	();
};

#endif // List_RotateInfo_h__
