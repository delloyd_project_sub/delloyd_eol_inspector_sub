﻿#ifndef Wnd_MasterSet_h__
#define Wnd_MasterSet_h__

#pragma once

#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "TI_PicControl.h"
#include "VGGroupWnd.h"

// CWnd_MasterSet

enum enMasterSet_Static{
	STI_MT_X = 0,
	STI_MT_Y,
 	STI_MT_R,
	STI_MT_MasterInfo,
	STI_MT_MasterSpc,
	STI_MT_MasterOffset,
	STI_MT_MAXNUM,
};
static LPCTSTR	g_szMasterSet_Static[] =
{
	_T("OFFSET X \n [Pix]"),
	_T("OFFSET Y \n [Pix]"),
 	_T("OFFSET R \n [Degree]"),
	_T("MASTER \n INFO"),
	_T("MASTER \n SPC"),
	_T("MASTER \n OFFSET"),
	NULL
};

enum enMasterSet_Edit{
	EIT_MT_MasterInfoX = 0,
	EIT_MT_MasterInfoY,
	EIT_MT_MasterInfoR,
	EIT_MT_MasterSpcX,
	EIT_MT_MasterSpcY,
	EIT_MT_MasterSpcR,
	EIT_MT_MasterOffsetX,
	EIT_MT_MasterOffsetY,
	EIT_MT_MasterOffsetR,
	EIT_MT_MAXNUM,
};

class CWnd_MasterSet : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MasterSet)

public:
	CWnd_MasterSet();
	virtual ~CWnd_MasterSet();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedTest();
	afx_msg void	OnBnClickedSave();
	afx_msg void	OnBnClickedExit();
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage(MSG* pMsg);

	CFont			m_font;
	CFont			m_font_Default;

	CMFCButton		m_bn_MasterTest;
	CMFCButton		m_bn_MasterSave;
	CMFCButton		m_bn_MasterExit;

	CVGStatic		m_st_UIItem;
	CVGStatic		m_st_Item[STI_MT_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[EIT_MT_MAXNUM];
	CVGGroupWnd		m_Group;

	ST_ModelInfo	*m_pstModelInfo;

public:

	void		GetUIData		();
	void		SetUIData		();
	void		SetMasterData	(__in int iOffsetX, __in int iOffsetY, __in double dbDegree);
	void		PermissionMode		(enPermissionMode InspMode);


	void		SetModelInfo(__in ST_ModelInfo* pModelInfo)
	{
		if (pModelInfo == NULL)
			return;

		m_pstModelInfo = pModelInfo;
		SetUIData();
	}

};
#endif // Wnd_CameraView_h__
