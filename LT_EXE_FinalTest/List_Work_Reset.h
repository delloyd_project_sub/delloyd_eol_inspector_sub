﻿#ifndef List_Work_Reset_h__
#define List_Work_Reset_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Reset_Worklist
{
	Reset_W_Recode,
	Reset_W_Time,
	Reset_W_Equipment,
	Reset_W_Model,
	Reset_W_SWVersion,
	Reset_W_LOTNum,
	Reset_W_Barcode,
	Reset_W_Operator,
	Reset_W_Result,
	Reset_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_Reset_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Operator"),
	_T("Result"),
	NULL
};

const int	iListAglin_Reset_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Reset_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_Reset

class CList_Work_Reset : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_Reset)

public:
	CList_Work_Reset();
	virtual ~CList_Work_Reset();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_Reset_h__
