// Dlg_FailConfirm.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_FailConfirm.h"
#include "afxdialogex.h"
#include "cv.h"
#include "highgui.h"

#include "Dlg_ChkPassword.h"

#define		IDC_BN_OK				1000

// CDlg_FailConfirm 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_FailConfirm, CDialogEx)

CDlg_FailConfirm::CDlg_FailConfirm(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_FailConfirm::IDD, pParent)
{
	m_pDevice = NULL;

	VERIFY(m_font.CreateFont(
		30,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	for (UINT nIdx = 0; nIdx < DI_NotUseBit_Max; nIdx++)
		m_bFlag_Butten[nIdx] = FALSE;
}

CDlg_FailConfirm::~CDlg_FailConfirm()
{
	KillTimer(99);
}


void CDlg_FailConfirm::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}



BEGIN_MESSAGE_MAP(CDlg_FailConfirm, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BN_OK, OnBnClickedBnOK)
	ON_WM_TIMER()

	ON_MESSAGE(WM_RECV_MAIN_BRD_ACK, OnRecvMainBrd)

	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CDlg_FailConfirm 메시지 처리기입니다.


int CDlg_FailConfirm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_stText.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_stText.SetColorStyle(CVGStatic::ColorStyle_Red);
	m_stText.SetFont_Gdip(L"Arial", 20.0F);
	m_stText.Create(_T("A defect has occurred.\nPlease Put the Camera in the FailBox"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_bn_OK.Create(_T("OK"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_OK);
	m_bn_OK.SetMouseCursorHand();
	m_bn_OK.SetFont(&m_font);

	SetTimer(99, 50, NULL);

	SetConfirmToBtnEnable(TRUE);

	return 0;
}


void CDlg_FailConfirm::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 5;
	int iCateSpacing = 10;
	int iMagin = 10;
	int iLeft = iMagin;
	int iTop = iMagin;
	int iWidth = cx - (iMagin * 2);
	int iHeight = cy - (iMagin * 2);

	m_stText.MoveWindow(iLeft, iTop, iWidth, iHeight * 5 / 6);

	iTop += iHeight * 5 / 6 + iSpacing;
	m_bn_OK.MoveWindow(iLeft, iTop, iWidth, iHeight / 6);

	//iLeft += iWidth / 2;
	//m_bn_NG.MoveWindow(iLeft, iTop, iWidth / 2, iHeight / 6);
}


void CDlg_FailConfirm::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	lpMMI->ptMaxTrackSize.x = 1000;
	lpMMI->ptMaxTrackSize.y = 400;

	lpMMI->ptMinTrackSize.x = 1000;
	lpMMI->ptMinTrackSize.y = 400;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


BOOL CDlg_FailConfirm::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreCreateWindow(cs);
}


BOOL CDlg_FailConfirm::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDlg_FailConfirm::OnBnClickedBnOK()
{
	CDlg_ChkPassword dlgPassword(this);

	if (IDOK == dlgPassword.DoModal())
	{
		KillTimer(99);
		Sleep(500);

		OnOK();
	}
}

void CDlg_FailConfirm::OnConfirmOK()
{
	KillTimer(99);
	Sleep(500);

	OnOK();
}

void CDlg_FailConfirm::SetConfirmToBtnEnable(__in BOOL bEnable)
{
	m_bn_OK.EnableWindow(bEnable);
}


LRESULT CDlg_FailConfirm::OnRecvMainBrd(WPARAM wParam, LPARAM lParam)
{
	// Start, Stop Button Recieve..
	LPCTSTR szData = (LPCTSTR)wParam;

	return 0;
}

void CDlg_FailConfirm::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	LPBYTE pFrameImage = NULL;
	IplImage *pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	switch (nIDEvent)
	{
	case 99:

		if (m_pDevice->DigitalIOCtrl.AXTState() == TRUE)
		{
			// Fail Box sensor
			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_FailBox))
			{
				if (m_bFlag_Butten[DI_FailBox] == FALSE)
				{
					m_bFlag_Butten[DI_FailBox] = TRUE;

					OnConfirmOK();

					m_bFlag_Butten[DI_FailBox] = FALSE;
				}
			}

// 			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StartBtn))
// 			{
// 				if (m_bFlag_Butten[DI_StartBtn] == FALSE)
// 				{
// 					m_bFlag_Butten[DI_StartBtn] = TRUE;
// 
// 					if (m_bn_OK.IsWindowEnabled() == TRUE)
// 						OnBnClickedBnOK();
// 
// 					m_bFlag_Butten[DI_StartBtn] = FALSE;
// 				}
// 			}
		}

		break;

	case 100:
		break;
	
	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}



void CDlg_FailConfirm::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	KillTimer(99);

	CDialogEx::OnClose();
}
