﻿// List_IRFilterManualResult.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_IRFilterManualResult.h"

// CList_IRFilterManualResult



IMPLEMENT_DYNAMIC(CList_IRFilterManualResult, CListCtrl)

CList_IRFilterManualResult::CList_IRFilterManualResult()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	
	m_nWidth	= 720;
	m_nHeight	= 480;

	m_pstIRFilterManual	= NULL;
}

CList_IRFilterManualResult::~CList_IRFilterManualResult()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_IRFilterManualResult, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_IRFilterManualResult::OnNMClick)
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CList_IRFilterManualResult::OnNMCustomdraw)
END_MESSAGE_MAP()

// CList_IRFilterManualResult 메시지 처리기입니다.
int CList_IRFilterManualResult::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_IRFilterManualResult::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < IRFilterManualResult_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, rectClient.Width() / IRFilterManualResult_MaxCol);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_IRFilterManualResult::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_IRFilterManualResult::InitHeader()
{
	for (int nCol = 0; nCol < IRFilterManualResult_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_IRFilterManualResult[nCol], iListAglin_IRFilterManualResult[nCol], iHeaderWidth_IRFilterManualResult[nCol]);
	}

	for (int nCol = 0; nCol < IRFilterManualResult_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_IRFilterManualResult[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_IRFilterManualResult::InsertFullData()
{
	if (m_pstIRFilterManual == NULL)
		return;

 	DeleteAllItems();
 

	for (UINT nIdx = 0; nIdx < IRFilterManualResult_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_IRFilterManualResult::SetRectRow(UINT nRow)
{
	if (m_pstIRFilterManual == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%d"), nRow + 1);
	SetItemText(nRow, IRFilterManualResult_Object, strValue);

	strValue.Format(_T("%s"), g_lpszItem_IRFilterManualResult[m_pstIRFilterManual->stIRFilterResult.nResult]);
	SetItemText(nRow, IRFilterManualResult_Result, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_IRFilterManualResult::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	*pResult = 0;
}


void CList_IRFilterManualResult::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	int nRow = 0, nSub = 0;
	switch (lplvcd->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;          // 아이템외에 일반적으로 처리하는 부분
		lplvcd->clrTextBk = RGB(0, 0, 255);
		break;
	case CDDS_ITEMPREPAINT:                          // 행 아이템에 대한 처리를 할 경우
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
		break;
	case CDDS_ITEMPREPAINT | CDDS_SUBITEM:  // 행과 열 아이템에 대한 처리를 할 경우
		nRow = (int)lplvcd->nmcd.dwItemSpec;         // 행 인덱스를 가져옴
		nSub = (int)lplvcd->iSubItem;                       // 열 인덱스를 가져옴

		if (nSub == IRFilterManualResult_Result)
		{
			if (m_pstIRFilterManual->stIRFilterResult.nResult == TRUE)
			{
				lplvcd->clrTextBk = RGB(0, 0, 255);           // 해당 행, 열 아이템의 배경색을 지정한다.
				lplvcd->clrText = RGB(255, 255, 255);                      // 해당 행, 열 아이템의 글자색을 지정한다.
			}
			else
			{
				lplvcd->clrTextBk = RGB(255, 0, 0);           // 해당 행, 열 아이템의 배경색을 지정한다.
				lplvcd->clrText = RGB(255, 255, 255);                      // 해당 행, 열 아이템의 글자색을 지정한다.
			}
		}

		break;
	default:
		*pResult = CDRF_DODEFAULT;

		break;
	}
}
