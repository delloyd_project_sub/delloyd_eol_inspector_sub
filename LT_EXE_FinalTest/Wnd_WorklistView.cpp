﻿//*****************************************************************************
// Filename	: Wnd_WorklistView.cpp
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_WorklistView.h"

#define	IDC_LIST_ARRAY			1201
#define	IDC_LIST_CHANNEL		1202

enum enWorklist
{
	IDC_LIST_TOTAL	= 1000,
//	IDC_LIST_OPERMODE,
	IDC_LIST_CURRENT,
 	IDC_LIST_CENTERPOINT,
	IDC_LIST_ROTATE,
	IDC_LIST_COLOR,
	IDC_LIST_PARTICLE,
//	IDC_LIST_SFR,
//	IDC_LIST_FFT,
	IDC_LIST_ANGLE,
	IDC_LIST_BRIGHTNESS,
	IDC_LIST_EIAJ,
	IDC_LIST_REVERSE,
	IDC_LIST_PARTICLE_MANUAL,
//	IDC_LIST_PATTERN_NOISE,
	IDC_LIST_IRFILTER,
//	IDC_LIST_IRFILTER_MANUAL,
	IDC_LIST_LED,
	IDC_LIST_DEFECTPIXEL,
//	IDC_LIST_RESET,
//	IDC_LIST_VIDEOSIGNAL,
};

//=============================================================================
// CWnd_WorklistView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_WorklistView, CWnd)

//=============================================================================
//
//=============================================================================
CWnd_WorklistView::CWnd_WorklistView()
{
}

CWnd_WorklistView::~CWnd_WorklistView()
{
}

BEGIN_MESSAGE_MAP(CWnd_WorklistView, CWnd)
	ON_WM_CREATE	()
	ON_WM_SIZE		()
	ON_NOTIFY		(NM_CLICK, IDC_LIST_ARRAY, OnNMClickListArray)
END_MESSAGE_MAP()


//=============================================================================
// CWnd_WorklistView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_WorklistView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
int CWnd_WorklistView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_list_Array.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ARRAY);
	m_tc_Worklist.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_TOP);

 	m_list_Total.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_TOTAL);
	
	m_list_Current.Create		(NULL, g_szLT_TestItem_Name[TIID_Current],			dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_CURRENT);
//	m_list_OperMode.Create(NULL, g_szLT_TestItem_Name[TIID_OperationMode], dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_OPERMODE);
	//	m_list_Reset.Create			(NULL, g_szLT_TestItem_Name[TIID_Reset],			dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_RESET);
	m_list_CenterPoint.Create	(NULL, g_szLT_TestItem_Name[TIID_CenterPoint],		dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_CENTERPOINT);
	m_list_Rotate.Create		(NULL, g_szLT_TestItem_Name[TIID_Rotation],			dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_ROTATE);
 //	m_list_SFR.Create			(NULL, g_szLT_TestItem_Name[TIID_SFR],				dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_SFR);
	m_list_EIAJ.Create			(NULL, g_szLT_TestItem_Name[TIID_EIAJ],				dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_EIAJ);
	m_list_Angle.Create			(NULL, g_szLT_TestItem_Name[TIID_FOV],			dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_ANGLE);
	m_list_Color.Create			(NULL, g_szLT_TestItem_Name[TIID_Color],			dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_COLOR);
	m_list_Reverse.Create		(NULL, g_szLT_TestItem_Name[TIID_Reverse],			dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_REVERSE);

	m_list_ParticleManual.Create(NULL, g_szLT_TestItem_Name[TIID_ParticleManual],	dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_PARTICLE_MANUAL);
	m_list_BlackSpot.Create		(NULL, g_szLT_TestItem_Name[TIID_BlackSpot], dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_PARTICLE);
	m_list_DefectPixel.Create(NULL, g_szLT_TestItem_Name[TIID_DefectPixel], dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_DEFECTPIXEL);
//	m_list_LED.Create			(NULL, g_szLT_TestItem_Name[TIID_LEDTest],			dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_LED);
//	m_list_VideoSignal.Create	(NULL, g_szLT_TestItem_Name[TIID_VideoSignal],		dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_VIDEOSIGNAL);
// 	m_list_PatternNoise.Create	(NULL, g_szLT_TestItem_Name[TIID_PatternNoise],		dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_PATTERN_NOISE);
 	m_list_Brightness.Create	(NULL, g_szLT_TestItem_Name[TIID_Brightness],		dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_BRIGHTNESS);
	m_list_IRFilter.Create		(NULL, g_szLT_TestItem_Name[TIID_IRFilter],			dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_IRFILTER);

// 	m_list_IRFilterManual.Create(NULL, g_szLT_TestItem_Name[TIID_IRFilterManual], dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_IRFILTER_MANUAL);
// 	m_list_Particle.Create		(NULL, g_szLT_TestItem_Name[TIID_Particle],		dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_PARTICLE);
// 	m_list_FFT.Create			(NULL, g_szLT_TestItem_Name[TIID_FFT],			dwStyle, rectDummy, &m_tc_Worklist, IDC_LIST_FFT);
	

	UINT nIdx = 0;
	m_tc_Worklist.AddTab(&m_list_Total,	_T("Total"), nIdx, FALSE);
	m_tc_Worklist.AddTab(&m_list_Current,			g_szLT_TestItem_Name[TIID_Current],			nIdx++, FALSE);
	//m_tc_Worklist.AddTab(&m_list_OperMode, g_szLT_TestItem_Name[TIID_OperationMode], nIdx++, FALSE);
	//	m_tc_Worklist.AddTab(&m_list_Reset,				g_szLT_TestItem_Name[TIID_Reset],			nIdx++, FALSE);
//	m_tc_Worklist.AddTab(&m_list_VideoSignal,		g_szLT_TestItem_Name[TIID_VideoSignal],		nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_CenterPoint,		g_szLT_TestItem_Name[TIID_CenterPoint],		nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_Rotate,			g_szLT_TestItem_Name[TIID_Rotation],		nIdx++, FALSE);
	//m_tc_Worklist.AddTab(&m_list_SFR,				g_szLT_TestItem_Name[TIID_SFR],				nIdx++,	FALSE);
	m_tc_Worklist.AddTab(&m_list_EIAJ,				g_szLT_TestItem_Name[TIID_EIAJ],			nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_Angle,				g_szLT_TestItem_Name[TIID_FOV],				nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_Color,				g_szLT_TestItem_Name[TIID_Color],			nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_Reverse,			g_szLT_TestItem_Name[TIID_Reverse],			nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_ParticleManual,	g_szLT_TestItem_Name[TIID_ParticleManual],	nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_BlackSpot,			g_szLT_TestItem_Name[TIID_BlackSpot],		nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_DefectPixel, g_szLT_TestItem_Name[TIID_DefectPixel], nIdx++, FALSE);
	//m_tc_Worklist.AddTab(&m_list_Particle, g_szLT_TestItem_Name[TIID_DefectPixel], nIdx++, FALSE);
	//m_tc_Worklist.AddTab(&m_list_LED,				g_szLT_TestItem_Name[TIID_LEDTest],			nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_Brightness,		g_szLT_TestItem_Name[TIID_Brightness],		nIdx++, FALSE);
	//m_tc_Worklist.AddTab(&m_list_PatternNoise,		g_szLT_TestItem_Name[TIID_PatternNoise],	nIdx++, FALSE);
	m_tc_Worklist.AddTab(&m_list_IRFilter,			g_szLT_TestItem_Name[TIID_IRFilter],		nIdx++, FALSE);

// 	m_tc_Worklist.AddTab(&m_list_IRFilterManual, g_szLT_TestItem_Name[TIID_IRFilterManual], nIdx++, FALSE);
// 	m_tc_Worklist.AddTab(&m_list_Particle,		 g_szLT_TestItem_Name[TIID_Particle],		nIdx++,		FALSE);
// 	m_tc_Worklist.AddTab(&m_list_FFT,			 g_szLT_TestItem_Name[TIID_FFT],			nIdx++,		FALSE);
	
	m_tc_Worklist.EnableTabSwap(FALSE);
	m_tc_Worklist.SetActiveTab(0);

	return 0;
}

//=============================================================================
// Method		: CWnd_WorklistView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
void CWnd_WorklistView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin  = 5;
	int iSpacing = 5;

	int iLeft	= iMagrin;
	int iTop	= iMagrin;
	int iWidth	= cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	m_tc_Worklist.MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: CWnd_WorklistView::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
BOOL CWnd_WorklistView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnNMClickListArray
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * result
// Qualifier	:
// Last Update	: 2016/8/12 - 16:05
// Desc.		:
//=============================================================================
void CWnd_WorklistView::OnNMClickListArray(NMHDR * pNMHDR, LRESULT * result)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;

	int index = pNMView->iItem;

	if (0 <= index)
	{
		ST_Worklist stWorklist;
	}
}

//=============================================================================
// Method		: InsertWorklist
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_Worklist * pstWorklist
// Qualifier	:
// Last Update	: 2016/8/12 - 16:56
// Desc.		:
//=============================================================================
void CWnd_WorklistView::InsertWorklist(__in const ST_Worklist* pstWorklist)
{
	m_list_Array.InsertWorklist(pstWorklist);

	int iCount = m_list_Array.GetItemCount();
	
	if (0 < iCount)
	{
		m_list_Array.SetSelectionMark(iCount - 1);
		m_list_Array.SetItemState(iCount - 1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		m_list_Array.SetFocus();

		//m_list_Channel.SetWorklistChInfo(pstWorklist);
	}
}

//=============================================================================
// Method		: GetPtr_Worklist
// Access		: public  
// Returns		: void
// Parameter	: ST_Worklist & pWorklist
// Qualifier	:
// Last Update	: 2017/2/22 - 14:05
// Desc.		:
//=============================================================================
void CWnd_WorklistView::GetPtr_Worklist(ST_Worklist& pWorklist)
{
 	pWorklist.pList_Total = &m_list_Total;

	m_list_Current.GetPtr_Worklist(pWorklist);
	//m_list_OperMode.GetPtr_Worklist(pWorklist);
	m_list_CenterPoint.GetPtr_Worklist(pWorklist);
	m_list_Rotate.GetPtr_Worklist(pWorklist);
	m_list_EIAJ.GetPtr_Worklist(pWorklist);
 	//m_list_SFR.GetPtr_Worklist(pWorklist);
	m_list_Angle.GetPtr_Worklist(pWorklist);
	m_list_Color.GetPtr_Worklist(pWorklist);
	m_list_Reverse.GetPtr_Worklist(pWorklist);
	m_list_ParticleManual.GetPtr_Worklist(pWorklist);
	//m_list_LED.GetPtr_Worklist(pWorklist);
	//m_list_Reset.GetPtr_Worklist(pWorklist);
	//m_list_VideoSignal.GetPtr_Worklist(pWorklist);
	//m_list_PatternNoise.GetPtr_Worklist(pWorklist);
	m_list_BlackSpot.GetPtr_Worklist(pWorklist);
	m_list_DefectPixel.GetPtr_Worklist(pWorklist);
	m_list_IRFilter.GetPtr_Worklist(pWorklist);
	m_list_Brightness.GetPtr_Worklist(pWorklist);
	
// 	m_list_IRFilterManual.GetPtr_Worklist(pWorklist);
//	m_list_PatternNoise.GetPtr_Worklist(pWorklist);
// 	m_list_Particle.GetPtr_Worklist(pWorklist);
// 	m_list_FFT.GetPtr_Worklist(pWorklist);

}