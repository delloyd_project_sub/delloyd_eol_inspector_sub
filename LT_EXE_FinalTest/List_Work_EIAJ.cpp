﻿// List_Work_EIAJent.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_EIAJ.h"

// CList_Work_EIAJ
IMPLEMENT_DYNAMIC(CList_Work_EIAJ, CListCtrl)

CList_Work_EIAJ::CList_Work_EIAJ()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_EIAJ::~CList_Work_EIAJ()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Work_EIAJ, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_Work_EIAJ 메시지 처리기입니다.
int CList_Work_EIAJ::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);
	InitHeader();
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:44
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	//CRect rc;
	//GetClientRect(&rc);

	//for (int nCol = EIAJ_W_Time; nCol < EIAJ_W_MaxCol; nCol++)
	//{
	//	SetColumnWidth(nCol, (rc.Width() - iHeaderWidth_EIAJ_Worklist[EIAJ_W_Recode]) / (12 - EIAJ_W_Time));
	//}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
BOOL CList_Work_EIAJ::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Header_MaxNum
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
UINT CList_Work_EIAJ::Header_MaxNum()
{
	return (UINT)EIAJ_W_MaxCol;
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::InitHeader()
{
	for (int nCol = 0; nCol < EIAJ_W_MaxCol; nCol++)
		InsertColumn(nCol, g_lpszHeader_EIAJ_Worklist[nCol], iListAglin_EIAJ_Worklist[nCol], iHeaderWidth_EIAJ_Worklist[nCol]);
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;
	int iNewCount = GetItemCount();


	InsertItem(iNewCount, _T(""));

	SetRectRow(iNewCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	CString strText;

	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, EIAJ_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, EIAJ_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, EIAJ_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, EIAJ_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, EIAJ_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, EIAJ_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, EIAJ_W_Barcode, strText);

	//strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	//SetItemText(nRow, EIAJ_W_Operator, strText);

	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stEIAJ[m_nTestIndex].stEIAJData.nResult].szText);
	SetItemText(nRow, EIAJ_W_Result, strText);

	for (UINT nIdx = 0; nIdx < EIAJ_W_MaxCol - EIAJ_W_C1; nIdx++)
	{
		if (pstCamInfo->stEIAJ[m_nTestIndex].stEIAJData.bUse[nIdx + 2] == TRUE)
			strText.Format(_T("%d"), pstCamInfo->stEIAJ[m_nTestIndex].stEIAJData.nValueResult[nIdx+2]);
		else
			strText.Format(_T("X"));

		SetItemText(nRow, EIAJ_W_C1 + nIdx, strText);
	}
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:47
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = EIAJ_W_MaxCol;
	CString temp[EIAJ_W_MaxCol];
	for (int t = 0; t < EIAJ_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
