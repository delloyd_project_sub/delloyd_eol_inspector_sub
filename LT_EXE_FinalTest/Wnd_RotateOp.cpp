﻿// Wnd_RotateOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_RotateOp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_RotateOp
typedef enum RotateOptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_RotateOp, CWnd)

CWnd_RotateOp::CWnd_RotateOp()
{
	m_pstModelInfo = NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_RotateOp::~CWnd_RotateOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_RotateOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_RotateOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_RotateOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_ROT_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szRotateStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < BTN_ROT_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szRotateButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_ROT_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_ROT_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
		//m_cb_Item[nIdex].InsertString(nIdex, _T(""));
	}
		
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_RotateOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt  = RoOp_ItemNum;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iList_H  = iHeaderH + iIdxCnt * 15;

	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = iWidth / 4;
	int iSTHeight = 25;

	m_st_Item[STI_ROT_MinSpc].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	
	iLeft += iSTWidth + 1;
	m_ed_Item[EDT_ROT_MinSpc].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);
	
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ROT_MaxSpc].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 1;
	m_ed_Item[EDT_ROT_MaxSpc].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ROT_Offset].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 1;
	m_ed_Item[EDT_ROT_Offset].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_ROT_TEST].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft = iMargin;
	iTop += iSpacing + iSTHeight;
	m_st_Item[STI_ROT_ROI].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;

	iLeft = iMargin;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_RotateOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_RotateOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (NULL == m_pstModelInfo)
		return;

	if (TRUE == bShow)
	{
		m_pstModelInfo->nTestMode = TIID_Rotation;
		m_pstModelInfo->nTestCnt = m_nTestItemCnt;
		m_pstModelInfo->nPicItem = PIC_Rotate;
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_RotateOp::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
	m_pstModelInfo->stRotate[m_nTestItemCnt].bTestMode = FALSE;
	switch (nIdex)
	{
	case BTN_ROT_TEST:
		GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_Rotation);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_RotateOp::SetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	CString strValue;

	m_List.SetPtr_Rotate(&m_pstModelInfo->stRotate[m_nTestItemCnt]);
	m_List.SetImageSize(m_pstModelInfo->dwWidth, m_pstModelInfo->dwHeight);
	m_List.InsertFullData();

	strValue.Format(_T("%.2f"), m_pstModelInfo->stRotate[m_nTestItemCnt].stRotateOpt.dbMinSpc);
	m_ed_Item[EDT_ROT_MinSpc].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), m_pstModelInfo->stRotate[m_nTestItemCnt].stRotateOpt.dbMaxSpc);
	m_ed_Item[EDT_ROT_MaxSpc].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), m_pstModelInfo->stRotate[m_nTestItemCnt].stRotateOpt.dbOffset);
	m_ed_Item[EDT_ROT_Offset].SetWindowText(strValue);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:05
// Desc.		:
//=============================================================================
void CWnd_RotateOp::GetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	CString strValue;

	m_List.GetCellData();

	m_ed_Item[EDT_ROT_MinSpc].GetWindowText(strValue);
	m_pstModelInfo->stRotate[m_nTestItemCnt].stRotateOpt.dbMinSpc = _ttof(strValue);

	m_ed_Item[EDT_ROT_MaxSpc].GetWindowText(strValue);
	m_pstModelInfo->stRotate[m_nTestItemCnt].stRotateOpt.dbMaxSpc = _ttof(strValue);
	
	m_ed_Item[EDT_ROT_Offset].GetWindowText(strValue);
	m_pstModelInfo->stRotate[m_nTestItemCnt].stRotateOpt.dbOffset = _ttof(strValue);
}