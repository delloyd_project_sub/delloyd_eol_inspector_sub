﻿#ifndef List_BrightnessResult_h__
#define List_BrightnessResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_BrightnessResult
{
	BrResult_Object = 0,
	BrResult_Value,
	BrResult_MinSpec,
	BrResult_MaxSpec,
	BrResult_Result,
	BrResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_BrightnessResult[] =
{
	_T(""),
	_T("Value"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_BrightnessResult
{
	BrResult_ItemNum = 4,
};

static LPCTSTR	g_lpszItem_BrightnessResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_BrightnessResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_BrightnessResult[] =
{
	80,
	80,
	80,
	80,
	80,
};
// List_BrightnessInfo

class CList_BrightnessResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_BrightnessResult)

public:
	CList_BrightnessResult();
	virtual ~CList_BrightnessResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Brightness(ST_LT_TI_Brightness* pstBrightness)
	{
		if (pstBrightness == NULL)
			return;

		m_pstBrightness = pstBrightness;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Brightness*  m_pstBrightness;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_BrightnessInfo_h__
