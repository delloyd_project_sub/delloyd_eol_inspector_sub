#ifndef List_DefectFixelResult_h__
#define List_DefectFixelResult_h__

#pragma once

#include "Def_DataStruct.h"


typedef enum enListNum_DefectFixelResult
{
	DfResult_Object = 0,
	DfResult_Value,
	DfResult_MinSpec,
	DfResult_MaxSpec,
	DfResult_Result,
	DfResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_DefectFixelResult[] =
{
	_T(""),
	_T("Value"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_DefectFixelResult
{
	DfResult_ItemNum = 2,
};

static LPCTSTR	g_lpszItem_DefectFixelResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_DefectFixelResult[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_DefectFixelResult[] =
{
	60,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
};
// List_DefectFixelInfo	

class CList_DefectPixelResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_DefectPixelResult)

public:
	CList_DefectPixelResult();
	virtual ~CList_DefectPixelResult();

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);
	void GetCellData();

	void SetPtr_DefectFixel(ST_LT_TI_DefectPixel* pstDefectPixel)
	{
		if (pstDefectPixel == NULL)
			return;

		m_pstDefectPixel = pstDefectPixel;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:

	ST_LT_TI_DefectPixel*  m_pstDefectPixel;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData(UINT nRow, UINT nCol, double dBValue);

public:

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void	OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif //List_DefectFixelResult_h__
