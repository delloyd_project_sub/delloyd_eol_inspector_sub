﻿#ifndef List_Work_CenterPoint_h__
#define List_Work_CenterPoint_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_CP_Worklist
{
	CP_W_Recode,
	CP_W_Time,
	CP_W_Equipment,
	CP_W_Model,
	CP_W_SWVersion,
	//CP_W_CameraType,
	CP_W_LOTNum,
	CP_W_Barcode,
	//CP_W_Operator,
	CP_W_Result,
	CP_W_CenterX,
	CP_W_CenterY,
	CP_W_OffsetX,
	CP_W_OffsetY,
// 	CP_W_WriteX,
// 	CP_W_WriteY,
	CP_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_CP_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	_T("Center X"),
	_T("Center Y"),
	_T("Offset X"),
	_T("Offset Y"),
// 	_T("Write X"),
// 	_T("Write Y"),
	NULL,
};

const int	iListAglin_CP_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_CP_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_CenterPoint

class CList_Work_CenterPoint : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_CenterPoint)

public:
	CList_Work_CenterPoint();
	virtual ~CList_Work_CenterPoint();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};


#endif // List_Work_CenterPoint_h__
