﻿//*****************************************************************************
// Filename	: 	File_Maintenance.h
// Created	:	2017/10/10 - 10:53
// Modified	:	2017/10/10 - 10:53
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_Maintenance_h__
#define File_Maintenance_h__

#pragma once

#include "Def_DataStruct.h"

//-----------------------------------------------------------------------------
// CFile_Maintenance
//-----------------------------------------------------------------------------
class CFile_Maintenance 
{
public:
	CFile_Maintenance();
	virtual ~CFile_Maintenance();

	//// 공통 사항 유지
	//virtual BOOL	LoadMaintenanceFile		(__in LPCTSTR szPath, __out ST_MaintenanceInfo& stMaintenanceInfo);
	//virtual BOOL	SaveMaintenanceFile		(__in LPCTSTR szPath, __in const ST_MaintenanceInfo* pstMaintenanceInfo);

	//// 광원 설정값
	//virtual BOOL	LoadLightFile			(__in LPCTSTR szPath, __out ST_LightInfo& stLightInfo);
	//virtual BOOL	SaveLightFile			(__in LPCTSTR szPath, __in const ST_LightInfo* pstLightInfo);

	//// 파워서플라이 설정값
	//virtual BOOL	LoadPowerSupplyFile		(__in LPCTSTR szPath, __out ST_PowerInfo& stPowerInfo);
	//virtual BOOL	SavePowerSupplyFile		(__in LPCTSTR szPath, __in const ST_PowerInfo* pstPowerInfo);

	// 모터 티칭 설정값
	virtual	BOOL	LoadTeachFile			(__in LPCTSTR szPath, __out ST_TeachInfo& stTeachInfo);
	virtual	BOOL	SaveTeachFile			(__in LPCTSTR szPath, __in const ST_TeachInfo* pstTeachInfo);
};

#endif // File_Maintenance_h__
