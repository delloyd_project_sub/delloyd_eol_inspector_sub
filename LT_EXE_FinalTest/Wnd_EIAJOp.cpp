﻿// Wnd_CurrentOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_EIAJOp.h"
#include "resource.h"
// CWnd_EIAJOp

typedef enum CurrentOp_ID
{
	IDC_LIST_CURRENTOP = 1001,
	IDC_BTN_RESOLUTION_TEST,
	IDC_BTN_RESOLUTION_GRAPH,
};

IMPLEMENT_DYNAMIC(CWnd_EIAJOp, CWnd)

CWnd_EIAJOp::CWnd_EIAJOp()
{

	m_nTestItemCnt = 0;
	m_pstModelInfo = NULL;

	m_bTest_Flag = FALSE;

	VERIFY(m_font_Data.CreateFont(
		15,						// dwHeight
		0,						// dwWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CWnd_EIAJOp::~CWnd_EIAJOp()
{
	m_font_Data.DeleteObject();
	m_bTest_Flag = FALSE;
}

BEGIN_MESSAGE_MAP(CWnd_EIAJOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BTN_RESOLUTION_TEST, OnBnClickedBnTest)
	ON_BN_CLICKED(IDC_BTN_RESOLUTION_GRAPH, OnBnClickedBnGraph)
END_MESSAGE_MAP()

// CWnd_EIAJOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 18:01
// Desc.		:
//=============================================================================
int CWnd_EIAJOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_ListEIAJOp.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_CURRENTOP);

	for (UINT nIdex = 0; nIdex < Btn_EJ_MAXNUM; nIdex++)
	{
		if (nIdex == Btn_EJ_EIAJ_GRAPH)
			m_bn_Item[nIdex].Create(g_szEIAJ_Button[nIdex], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_RESOLUTION_TEST + nIdex);
		else
			m_bn_Item[nIdex].Create(g_szEIAJ_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_RESOLUTION_TEST + nIdex);

		m_bn_Item[nIdex].SetFont(&m_font_Data);
	}

	m_bn_Item[Btn_EJ_EIAJ_GRAPH].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[Btn_EJ_EIAJ_GRAPH].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[Btn_EJ_EIAJ_GRAPH].SizeToContent();
	m_bn_Item[Btn_EJ_EIAJ_GRAPH].SetCheck(BST_UNCHECKED);

	m_bn_Item[Btn_EJ_EIAJ_GRAPH].ShowWindow(SW_HIDE);

	return 0;
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/7/13 - 17:03
// Desc.		:
//=============================================================================
void CWnd_EIAJOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);
	if (NULL == m_pstModelInfo)
		return;

	if (TRUE == bShow)
	{
		//m_pstModelInfo->nTestMode = TIID_EIAJ;
		m_pstModelInfo->nTestCnt = m_nTestItemCnt;
		m_pstModelInfo->nPicItem = PIC_EIAJ;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_EIAJOp::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_EIAJOp::PreTranslateMessage(MSG* pMsg)
{
	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnBnClickedBnTest
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/13 - 17:05
// Desc.		:
//=============================================================================
void CWnd_EIAJOp::OnBnClickedBnTest()
{
	m_bTest_Flag = TRUE;

	GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_EIAJ);

}

//=============================================================================
// Method		: OnBnClickedBnTestStop
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/13 - 17:05
// Desc.		:
//=============================================================================
void CWnd_EIAJOp::OnBnClickedBnTestStop()
{
	m_bTest_Flag = FALSE;
}

//=============================================================================
// Method		: OnBnClickedBnDefaultSet
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/13 - 17:05
// Desc.		:
//=============================================================================
void CWnd_EIAJOp::OnBnClickedBnDefaultSet()
{
}

//=============================================================================
// Method		: OnBnClickedBnGraph
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/28 - 14:09
// Desc.		:
//=============================================================================
void CWnd_EIAJOp::OnBnClickedBnGraph()
{
	if (m_bn_Item[Btn_EJ_EIAJ_GRAPH].GetCheck())
	{
		m_pstModelInfo->stEIAJ[0].stEIAJOp.bGViewMode = TRUE;
	}
	else
	{
		m_pstModelInfo->stEIAJ[0].stEIAJOp.bGViewMode = FALSE;
	}
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_EIAJOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 10;
	int iSpacing = 5;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int iBtnWidth = iWidth / 7;
	int iBtnHeight = 26;

	int Static_W = iWidth / 4;
	int Combo_W = iWidth / 3;

	iLeft = iWidth - iBtnWidth + iMargin;
	m_bn_Item[Btn_EJ_EIAJ_TEST].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	
	iTop += iBtnHeight + iSpacing;
	m_bn_Item[Btn_EJ_EIAJ_GRAPH].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;

	iLeft = iMargin;
	int List_W = iWidth;
	int List_H = iHeight - iTop;

	m_ListEIAJOp.MoveWindow(iLeft, iTop, List_W, List_H);
}

//=============================================================================
// Method		: SetUI
// Access		: public  
// Returns		: void
// Parameter	: 
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_EIAJOp::SetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	m_ListEIAJOp.SetPtr_Resolution(&m_pstModelInfo->stEIAJ[m_nTestItemCnt]);
	m_ListEIAJOp.InsertFullData();
}

//=============================================================================
// Method		: GetUI
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_EIAJOp::GetUpdateData()
{
}

//=============================================================================
// Method		: IsTest
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/13 - 17:41
// Desc.		:
//=============================================================================
BOOL CWnd_EIAJOp::IsTest()
{
	return m_bTest_Flag;
}

//=============================================================================
// Method		: SetStatusEngineerMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2017/7/28 - 14:12
// Desc.		:
//=============================================================================
void CWnd_EIAJOp::SetStatusEngineerMode(__in enPermissionMode InspMode)
{
	if (InspMode == Permission_Engineer || InspMode == Permission_CNC)
	{
		m_bn_Item[Btn_EJ_EIAJ_GRAPH].ShowWindow(SW_SHOW);
		m_ListEIAJOp.SetAccessMode_Change(TRUE);
	}
	else
	{
		m_bn_Item[Btn_EJ_EIAJ_GRAPH].ShowWindow(SW_HIDE);
		m_ListEIAJOp.SetAccessMode_Change(FALSE);
	}
}

