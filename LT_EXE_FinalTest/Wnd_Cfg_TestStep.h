//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestStep.h
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_TestStep_h__
#define Wnd_Cfg_TestStep_h__

#pragma once

#include <afxwin.h>
#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_Enum_Cm.h"
#include "Def_TestItem.h"
#include "List_TestStep.h"
#include "Def_TestDevice.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_TestStep
//-----------------------------------------------------------------------------
class CWnd_Cfg_TestStep : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_TestStep)

public:
	CWnd_Cfg_TestStep();
	virtual ~CWnd_Cfg_TestStep();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnStepItem		(UINT nID);
	afx_msg void	OnBnClickedBnStepCtrl		(UINT nID);
	afx_msg void	OnBnClickedSeqCheck			();
	afx_msg void	OnSelChangeCbTestItem		();

	CFont			m_font;

	enum enStepItem
	{
		SI_Test,			// 테스트 검사 항목
	//	SI_TestCnt,			// 테스트 세부 검사 항목
	//	SI_Retry,
		SI_Delay,
	//	SI_MoveAxis,		// 축 이름
	//	SI_MovePos,			// 축 POS
		SI_MaxEnum,
	};

	enum enStepCtrl
	{
		SC_Add,
		SC_Insert,
		SC_Remove,
		SC_Order_Up,
		SC_Order_Down,
		SC_MaxEnum,
	};

 	// 스텝 목록
 	CList_TestStep	m_lc_StepList;

	// 스텝 항목 추가/삭제/이동
	CMFCButton		m_bn_StepCtrl[SC_MaxEnum];
	
	// 스텝 설정 항목
	CMFCButton		m_chk_StepItem[SI_MaxEnum];
	CMFCMaskedEdit	m_ed_StepItem[SI_MaxEnum];
	
	// 개별 검사 항목
	CComboBox		m_cb_TestItem;

	// 개별 검사 세부 항목
	//CComboBox		m_cb_TestItemCnt;

	// Retry Count
	//CComboBox		m_cb_RetryCnt;

	// Axis
	//CComboBox		m_cb_AxisItem;

	// 시퀀스 확인
	CMFCButton		m_bn_SeqCheck;

	CVGStatic		m_st_Name;
	CVGStatic		m_st_TestName;

	// UI에 설정된 스텝 데이터 구하기
	BOOL	GetTestStepData			(__out ST_StepUnit& stStepUnit);

	// 버튼 제어 함수
	void	Item_Add();
	void	Item_Insert();
	void	Item_Remove();
	void	Item_Up();
	void	Item_Down();
	void	Item_Enable(enStepItem enStep);

	void	Item_TestAddString(enLT_TestItem_ID nTestItem);
	ST_Device*	m_pstDevice;

public:

	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	// 검사기 종류 설정
	void		SetSystemType		(__in enInsptrSysType nSysType);

	// 저장된 Step Info 데이터 불러오기
	void		Set_StepInfo		(__in const ST_StepInfo* pstInStepInfo);
	void		Get_StepInfo		(__out ST_StepInfo& stOutStepInfo);

	// 초기 상태로 설정 (New)
	void		Init_DefaultSet		();

};

#endif // Wnd_Cfg_TestStep_h__


