﻿#ifndef List_Work_Total_h__
#define List_Work_Total_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Total_Worklist
{
	Total_W_Recode,
	Total_W_Time,
	Total_W_Equipment,
	Total_W_Model ,
	Total_W_SWVersion,
	//Total_W_CameraType,
	Total_W_LOTNum,
	Total_W_Barcode,
	//Total_W_Operator,
	Total_W_Result,
	Total_W_Initalize,

	Total_W_Current,
	//Total_W_OperMode,
 	Total_W_CenterPoint,
 	Total_W_Rotate,
	Total_W_Color,
	Total_W_Reverse,
	//Total_W_SFR,
	Total_W_EIAJ,
 	Total_W_Angle,

	Total_W_BlackSpot,
	Total_W_ParticleMA,
	Total_W_DefectPixel,
	//Total_W_LED,	
	Total_W_Brightness,
	//Total_W_PatternNoise,
	Total_W_IRFilter,

// 	Total_W_SFR_Far,
// 	Total_W_SFR_Near,
// 	Total_W_SFR_IR,
// 	Total_W_Particle,

	Total_W_Finalize,
	Total_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_Total_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),

	_T("Initalize"),
	_T("Current"),
	//_T("Oper Mode"),
	_T("CenterPoint"),
	_T("Rotate"),	
	_T("Color"),
	_T("Reverse"),
	//_T("SFR"),
	_T("EIAJ"),
	_T("Angle"),
	_T("Stain"),
	_T("Particle"),
	_T("DefectPixel"),
	//_T("LED"),

	_T("Brightness"),
	//_T("Pattern Noise"),
	_T("IR Filter"),
	_T("Finalize"),
	NULL
};

const int	iListAglin_Total_Worklist[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,

};

const int	iHeaderWidth_Total_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};
// CList_Total

class CList_Work_Total : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_Total)

public:
	CList_Work_Total();
	virtual ~CList_Work_Total();
	CFont		m_Font;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	UINT Header_MaxNum();

	void InitHeader();
	void InsertFullData(__in const ST_CamInfo* pstCamInfo);
	void SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo);

	void GetData(UINT nRow, UINT &DataNum, CString *Data);
};


#endif // List_Work_Total_h__
