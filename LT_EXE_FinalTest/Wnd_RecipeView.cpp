﻿//*****************************************************************************
// Filename	: Wnd_RecipeView.cpp
// Created	: 2016/03/18
// Modified	: 2016/03/18
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_RecipeView.h"
#include "Dlg_ChkPassword.h"

#include "Dlg_RefBarcodeOp.h"
#include "Dlg_UserConfigOp.h"
#include "Dlg_LabelPrinter.h"

#pragma comment(lib, "winmm")
#include <mmsystem.h>


#define		IDC_STATIC_VIDEO		1000
#define		IDC_STATIC_IMAGE		1001
#define		IDC_ED_OUPUTVOLT		1050
#define		IDC_BN_NEW				1001
#define		IDC_BN_SAVE				1002
#define		IDC_BN_SAVE_AS			1003
#define		IDC_BN_LOAD				1004
#define		IDC_BN_REFRESH			1005
#define		IDC_LSB_MODEL			1006
#define		IDC_BN_BARCODEOP		1007
#define		IDC_BN_OPERATOROP		1008
#define		IDC_BN_PRINTEROP		1009
#define		IDC_RB_TEST_OPT			1021
#define		IDC_RB_MOTOR_OPT		1022
#define		IDC_BN_POGOCNT			1011
#define		IDC_BN_MOTION			1012
#define		IDC_TB_TESTITEM			1013
#define		IDC_TB_RESULTITEM		1014
#define		IDC_TB_IMAGEVIEW		1015

//=============================================================================
// CWnd_RecipeView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_RecipeView, CWnd_BaseView)

//=============================================================================
//
//=============================================================================
CWnd_RecipeView::CWnd_RecipeView()
{
	VERIFY(m_font_Default.CreateFont(
		20,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		26,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pDevice		= NULL;
	m_nPicViewItem	= PIC_Standby;
	m_LoadImage = NULL;
	m_iTabDaqIdx_Eng = 0;
	SetFullPath	(_T(""));
}

CWnd_RecipeView::~CWnd_RecipeView()
{
	m_ImageTest.DeleteMemory(&m_stModelInfo.stEIAJ[0]);
	TRACE(_T("<<< Start ~CWnd_RecipeView >>> \n"));
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_RecipeView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED		(IDC_BN_BARCODEOP,		OnBnClickedBnBarcodeOp)
	ON_BN_CLICKED		(IDC_BN_OPERATOROP,		OnBnClickedBnOperatorOp)
	ON_BN_CLICKED		(IDC_BN_PRINTEROP,		OnBnClickedBnPrinterOp)
	ON_BN_CLICKED		(IDC_BN_NEW,			OnBnClickedBnNew)
	ON_BN_CLICKED		(IDC_BN_SAVE,			OnBnClickedBnSave)
	ON_BN_CLICKED		(IDC_BN_SAVE_AS,		OnBnClickedBnSaveAs)
	ON_BN_CLICKED		(IDC_BN_LOAD,			OnBnClickedBnLoad)
	ON_BN_CLICKED		(IDC_BN_REFRESH,		OnBnClickedBnRefresh)
	ON_LBN_SELCHANGE	(IDC_LSB_MODEL,			OnLbnSelChangeModel)
	ON_MESSAGE			(WM_FILE_MODEL,			OnFileModel)
	ON_MESSAGE			(WM_CHANGED_MODEL,		OnChangeModel)
	ON_MESSAGE			(WM_REFESH_MODEL,		OnRefreshModelList)
	ON_MESSAGE			(WM_MANUAL_TEST,		OnManualTestCmd)
	ON_MESSAGE			(WM_PRINTER_TEST,		OnPrinterTestCmd)
	ON_MESSAGE			(WM_AUTO_SETTING,		OnAutoSettingCmd)
	ON_MESSAGE			(WM_OPTDATA_RESET,		OnOptDataResetCmd)
	ON_MESSAGE			(WM_EDGE_ONOFF,			OnEdgeOnOff)
	ON_MESSAGE			(WM_DISTORTION_CORR,	OnDistortionCorr)
	ON_MESSAGE			(WM_MANUAL_CONTROL,		OnManualCtrlCmd)
	//ON_MESSAGE			(WM_MANUAL_MOTION,		OnManualMotionCmd)
	ON_MESSAGE			(WM_TAB_CHANGE_PIC,		OnTabChangePic)
	ON_MESSAGE			(WM_LIST_ROI_UPDATE,	OnUpdateDataRoiList)
	ON_MESSAGE			(WM_MODEL_SAVE,			OnModelSave)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

//=============================================================================
// CWnd_RecipeView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_RecipeView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/9 - 0:05
// Desc.		:
//=============================================================================
int CWnd_RecipeView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	UINT nTabCnt = 0;

	m_st_BackColor.SetBackColor_COLORREF(RGB(160, 160, 160));
	m_st_BackColor.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Video.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Video.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Video.Create(_T("Video View"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Manual.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Manual.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Manual.Create(_T("Manual Control"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_File.SetStaticStyle(CVGStatic::StaticStyle_Title_Alt);
	m_st_File.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_File.SetFont_Gdip(L"Arial", 11.0F);

	m_st_Location.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Location.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_Location.SetFont_Gdip(L"Arial", 11.0F);

	m_st_File.Create(_T("Model Files"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_Location.Create(_T("Location"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_lst_ModelList.Create(dwStyle | WS_HSCROLL | LBS_STANDARD | LBS_SORT | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, IDC_LSB_MODEL);
	m_bn_Refresh.Create(_T("Refresh File List"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_REFRESH);

	m_bn_New.Create(_T("New"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_NEW);
	m_bn_New.SetMouseCursorHand();

	m_bn_Save.Create(_T("Save (Apply)"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);
	m_bn_Save.SetMouseCursorHand();

	m_bn_SaveAs.Create(_T("Save As"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE_AS);
	m_bn_SaveAs.SetMouseCursorHand();

	m_bn_Load.Create(_T("Load"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_LOAD);
	m_bn_Load.SetMouseCursorHand();

	m_bn_BarcodeOp.Create(_T("Barcode Config"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_BARCODEOP);
	m_bn_BarcodeOp.SetMouseCursorHand();

	//m_bn_OperatorOp.Create(_T("Operator Config"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_OPERATOROP);
	//m_bn_OperatorOp.SetMouseCursorHand();

	//m_bn_PrintOp.Create(_T("Label Printer"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_PRINTEROP);
	//m_bn_PrintOp.SetMouseCursorHand();

	// 탭 컨트롤
	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D_SCROLLED, rectDummy, this, IDC_TB_TESTITEM, CMFCTabCtrl::LOCATION_TOP);
 	
 	m_wnd_ModelCfg.SetOwner(this);
 	m_wnd_ModelCfg.Create(NULL, _T("Model"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 10);
 	
	m_wnd_TestStepCfg.SetOwner(this);
	m_wnd_TestStepCfg.SetPtr_Device(m_pDevice);
	m_wnd_TestStepCfg.Create(NULL, _T("Test Sequence"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 11);

	m_wnd_TestItemCfg.SetOwner(this);
	m_wnd_TestItemCfg.SetPtr_Device(m_pDevice);
	m_wnd_TestItemCfg.Create(NULL, _T("Test Item"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 12);

	//m_wnd_BarcodeInfoCfg.SetOwner(this);
	//m_wnd_BarcodeInfoCfg.Create(NULL, _T("Barcode Config"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 13);

	m_wnd_PogoCfg.SetOwner(GetOwner());
	m_wnd_PogoCfg.Create(NULL, _T("Pogo"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 14);

	//m_wnd_DAQCfg.SetOwner(GetOwner());
	//m_wnd_DAQCfg.Create(NULL, _T("DAQ"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 15);

	//m_wnd_EachTestStep.SetOwner(GetOwner());
	//m_wnd_EachTestStep.SetPtr_Device(m_pDevice);
	//m_wnd_EachTestStep.Create(NULL, _T("Each Test Sequence"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 16);

  	m_tc_Option.AddTab(&m_wnd_ModelCfg,			_T("Model"),				nTabCnt,	FALSE);
	m_tc_Option.AddTab(&m_wnd_TestStepCfg,		_T("Test Sequence"),	nTabCnt++,	FALSE);
	//m_tc_Option.AddTab(&m_wnd_EachTestStep,		_T("Each Test Sequence"),	nTabCnt++,	FALSE);
	m_tc_Option.AddTab(&m_wnd_TestItemCfg,		_T("Test Item"),			nTabCnt++,	FALSE);
  	m_tc_Option.AddTab(&m_wnd_PogoCfg,			_T("Pogo"),			nTabCnt++,	FALSE);
  	//m_tc_Option.AddTab(&m_wnd_DAQCfg,			_T("Digital Grabber"),		nTabCnt++,	FALSE);

	m_iTabDaqIdx_Eng = nTabCnt;

	m_tc_Option.EnableTabSwap(FALSE);
	m_tc_Option.SetActiveTab(0);
 
 	m_lst_ModelList.SetFont(&m_font_Default);

 	// 파일 감시 쓰레드 설정
 	m_ModelWatch.SetOwner(GetSafeHwnd(), WM_REFESH_MODEL);
 	if (!m_strModelPath.IsEmpty())
 	{
 		m_ModelWatch.SetWatchOption(m_strModelPath, MODEL_FILE_EXT);
 		m_ModelWatch.BeginWatchThrFunc();
 
 		m_ModelWatch.RefreshList();
 
 		RefreshModelFileList(m_ModelWatch.GetFileList());
 	}
 
	m_tc_Result.Create(CMFCTabCtrl::STYLE_3D_SCROLLED, rectDummy, this, IDC_TB_RESULTITEM, CMFCTabCtrl::LOCATION_TOP);

	m_wnd_ManualCtrl.SetOwner(this);
	m_wnd_ManualCtrl.Create(NULL, _T(""), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Result, 100);
	m_wnd_ManualCtrl.SetCtrlID(WM_MANUAL_CONTROL);

	m_wnd_ManualMotion.SetOwner(this);
	m_wnd_ManualMotion.SetPtr_Device(m_pDevice);
	m_wnd_ManualMotion.Create(NULL, _T(""), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Result, 100);
	m_wnd_ManualMotion.SetCtrlID(WM_MANUAL_CONTROL);

	m_wnd_EachTestResult.SetOwner(this);
	m_wnd_EachTestResult.SetPtr_Modelinfo(&m_stModelInfo);
	m_wnd_EachTestResult.Create(NULL, _T(""), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Result, 101);

	m_tc_Result.AddTab(&m_wnd_EachTestResult,	_T("Test Result"),		0, FALSE);
	m_tc_Result.AddTab(&m_wnd_ManualCtrl,		_T("Manual Control"),	1, FALSE);
	m_tc_Result.AddTab(&m_wnd_ManualMotion,		_T("Motion Control"),	2, FALSE);
	m_tc_Result.EnableTabSwap(FALSE);
	m_tc_Result.SetActiveTab(0);

//	m_tc_ImageView.Create(CMFCTabCtrl::STYLE_3D_SCROLLED, rectDummy, this, IDC_TB_IMAGEVIEW, CMFCTabCtrl::LOCATION_TOP);

	m_wnd_VideoView.SetOwner(this);
	m_wnd_VideoView.Create(NULL, _T("Video View"), dwStyle | WS_BORDER, rectDummy, this, IDC_STATIC_VIDEO);
	m_wnd_VideoView.SetMasueControl(TRUE);

	m_wnd_ImageView.SetOwner(this);
	m_wnd_ImageView.Create(NULL, _T("Image View"), dwStyle | WS_BORDER, rectDummy, this, IDC_STATIC_IMAGE);
	m_wnd_ImageView.SetMasueControl(TRUE);

	//m_tc_ImageView.AddTab(&m_wnd_VideoView, _T("Video View"), 0, FALSE);
	//m_tc_ImageView.AddTab(&m_wnd_ImageView, _T("Image View"), 1, FALSE);

	
//	m_tc_ImageView.EnableTabSwap(FALSE);
//	m_tc_ImageView.SetActiveTab(0);

// 	m_wnd_VideoView.ShowWindow(SW_HIDE);
// 	m_wnd_ImageView.ShowWindow(SW_SHOW);

	m_wnd_VideoView.ShowWindow(SW_SHOW);
	m_wnd_ImageView.ShowWindow(SW_HIDE);

	ChangeImageMode(ImageMode_LiveCam);

	return 0;
}

//=============================================================================
// Method		: CWnd_RecipeView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2015/12/9 - 0:05
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	int iVideoWidth  = m_stModelInfo.dwWidth;
	int iVideoHeight = m_stModelInfo.dwHeight;

	if ((0 == cx) || (0 == cy))
		return;

	if (iVideoWidth <= 0 || iVideoHeight <= 0)
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iCtrlHeight = 35;
	int iCtrlWidth = 147;

	int iCateWidth = (iWidth - (iCateSpacing * 4)) / 5;
	int iHalfWidth = iCateWidth + iCateSpacing + iCateWidth;
	int iLstHeight = iHeight - (iSpacing * 2) - (iCtrlHeight * 2);

	// 파일 리스트
	iLeft = iMagrin;
	iTop = iMagrin;
	m_st_File.MoveWindow(iLeft, iTop, iCateWidth, iCtrlHeight);

	iLeft += iCateWidth + iCateSpacing;
	iWidth = iWidth - iCateWidth - iCateSpacing;
	m_st_Location.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);

	iLeft = iMagrin;
	iTop = iMagrin + iCtrlHeight + iSpacing;
	m_lst_ModelList.MoveWindow(iLeft, iTop, iCateWidth, iCtrlHeight * 2 + iSpacing);

	iLeft = cx - iMagrin - iCtrlWidth;
	m_bn_Load.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft -= (iCtrlWidth + iSpacing);
	m_bn_SaveAs.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft -= (iCtrlWidth + iSpacing);
	m_bn_Save.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft -= (iCtrlWidth + iSpacing);
	m_bn_New.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

// 	iLeft -= (iCtrlWidth + iSpacing);
// 	m_bn_BarcodeOp.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	//iLeft -= (iCtrlWidth + iSpacing);
	//m_bn_OperatorOp.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	//iLeft -= (iCtrlWidth + iSpacing);
	//m_bn_PrintOp.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing + iCtrlHeight + iSpacing;
	iLeft = iMagrin;

	//int iVideo_W = (int)(iWidth / 3 * 2);
	//int iVideo_H = (int)(cy - iTop - iSpacing - 21) * 3 / 4;
	int iVideo_W = 720;
	int iVideo_H =480;

	iLeft += iVideo_W + iMagrin;
	iHeight = cy - iSpacing - iTop;

	int iTabW = cx - iLeft - iMagrin;

	m_tc_Option.MoveWindow(iLeft, iTop, iTabW, iHeight);

	iLeft = iMagrin;

	// m_tc_ImageView.MoveWindow(iLeft, iTop, iVideo_W, iVideo_H);
	m_wnd_VideoView.MoveWindow(iLeft, iTop, iVideo_W, iVideo_H);
	m_wnd_ImageView.MoveWindow(iLeft, iTop, iVideo_W, iVideo_H);

	iTop += iVideo_H + iSpacing;
	iHeight = cy - iSpacing - iTop;
	m_tc_Result.MoveWindow(iLeft, iTop, iVideo_W, iHeight);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2016/5/28 - 22:31
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_BaseView::OnShowWindow(bShow, nStatus);

	
}

//=============================================================================
// Method		: CWnd_RecipeView::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/9 - 0:05
// Desc.		:
//=============================================================================
BOOL CWnd_RecipeView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}


void CWnd_RecipeView::OnBnClickedBnBarcodeOp()
{
	CFile_Model FileLoad;
	CString szBarcodeOpPath;
	szBarcodeOpPath.Format(_T("%s\\refBarcode.ini"), m_strBarcodePath);

	if (m_fileModel.Load_RefBarcodeInfo(szBarcodeOpPath, *m_pstBarcodeInfo))
	{
		;
	}

	CDlg_RefBarcodeOp DlgBarcodeOp;
	DlgBarcodeOp.SetPath(m_strModelPath, m_strBarcodePath);
	DlgBarcodeOp.SetReferenceSeperator(m_strRefBarcode);
	DlgBarcodeOp.SetPtr_BarcodeInfo(m_pstBarcodeInfo);

	DlgBarcodeOp.DoModal();
}


void CWnd_RecipeView::OnBnClickedBnOperatorOp()
{
	CFile_Model FileLoad;
	CString szOperatorOpPath;
	szOperatorOpPath.Format(_T("%s\\OperatorConfig.ini"), m_strUserConfigPath);

	if (m_fileModel.Load_UserConfigInfo(szOperatorOpPath, *m_pstUserConfigInfo))
	{
		;
	}

	CDlg_UserConfigOp DlgUserConfigOp;
	DlgUserConfigOp.SetPath(m_strUserConfigPath);
	DlgUserConfigOp.SetPtr_UserConfigInfo(m_pstUserConfigInfo);

	DlgUserConfigOp.DoModal();
}

void CWnd_RecipeView::OnBnClickedBnPrinterOp()
{
	CDlg_LabelPrinter DlgLabelPrinterOp;
	DlgLabelPrinterOp.SetOwner(this);
	DlgLabelPrinterOp.SetPath(m_strPrnPath);
	DlgLabelPrinterOp.SetPtr_Device(m_pDevice);
	DlgLabelPrinterOp.SetModelInfo(&m_stModelInfo);

	DlgLabelPrinterOp.DoModal();
}

//=============================================================================
// Method		: OnBnClickedBnNew
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/6 - 13:10
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnBnClickedBnNew()
{
	if (IsRecipeTest())
	{
		MessageView(_T("Inspection is Running.\r\nRetry after the work is finished."));
		return;
	}
	
	New_Model();
}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/14 - 16:28
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnBnClickedBnSave()
{
	if (IsRecipeTest())
	{
		MessageView(_T("Inspection is Running.\r\nRetry after the work is finished."));
		return;
	}

	Save_Model();
}

//=============================================================================
// Method		: OnBnClickedBnSaveAs
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/14 - 16:28
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnBnClickedBnSaveAs()
{
	if (IsRecipeTest())
	{
		MessageView(_T("Inspection is Running.\r\nRetry after the work is finished.."));
		return;
	}

	SaveAs_Model();
}

//=============================================================================
// Method		: OnBnClickedBnLoad
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/14 - 16:28
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnBnClickedBnLoad()
{
	if (IsRecipeTest())
	{
		MessageView(_T("Inspection is Running.\r\nRetry after the work is finished."));
		return;
	}
	
	Load_Model();
}

//=============================================================================
// Method		: OnBnClickedBnRefresh
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/14 - 16:28
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnBnClickedBnRefresh()
{
	RefreshModelFileList(m_ModelWatch.GetFileList());
}

//=============================================================================
// Method		: OnLbnSelChangeModel
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/27 - 12:54
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnLbnSelChangeModel()
{
	CString strValue;
	int iSel = -1;
	if (0 <= (iSel = m_lst_ModelList.GetCurSel()))
	{		
		m_lst_ModelList.GetText(iSel, strValue);

		// 현재 편집중인 모델파일과 같으면 리턴
		if (0 == strValue.Compare(m_stModelInfo.szModelFile))
			return;

		m_stModelInfo.szModelFile = strValue;

// 		CString strFullPath;
// 
// 		strFullPath.Format(_T("%s%s.%s"), m_strModelPath, m_stModelInfo.szModelFile, MODEL_FILE_EXT);
// 
// 		// 파일 불러오기
//  		if (m_fileModel.LoadModelFile(strFullPath, m_stModelInfo))
//  		{
//  			SetFullPath(m_stModelInfo.szModelFile);
//  
//  			// UI에 세팅
//  			SetModelInfo();
//  		}


		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
		m_stModelInfo.szModelFile.ReleaseBuffer();
	}
}

//=============================================================================
// Method		: OnFileModel
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/17 - 16:51
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnFileModel(WPARAM wParam, LPARAM lParam)
{
	UINT nType = (UINT)wParam;

	switch (nType)
	{
	case ID_FILE_NEW:
		New_Model();
		break;

	case ID_FILE_SAVE:
		Save_Model();
		break;

	case ID_FILE_SAVE_AS:
		SaveAs_Model();
		break;

	case ID_FILE_OPEN:
		Load_Model();
		break;

	default:
		break;
	}

	return 0;
}

//=============================================================================
// Method		: OnChangeModel
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/18 - 14:16
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnChangeModel(WPARAM wParam, LPARAM lParam)
{
	CString strModel = (LPCTSTR)wParam;
	CString strFullPath;

	strFullPath.Format(_T("%s%s.%s"), m_strModelPath, strModel, MODEL_FILE_EXT);

	// 파일 불러오기
	if (m_fileModel.LoadModelFile(strFullPath, m_stModelInfo))
	{
		//m_stModelInfo.szModelName = strModel;

		// UI에 세팅
		SetModelInfo();
	}

	return 0;
}

//=============================================================================
// Method		: OnRefreshModelList
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/18 - 15:18
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnRefreshModelList(WPARAM wParam, LPARAM lParam)
{
	RefreshModelFileList(m_ModelWatch.GetFileList());

	return 0;
}

//=============================================================================
// Method		: OnChangeTab
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/2/13 - 17:05
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnTabChangePic(WPARAM wParam, LPARAM lParam)
{
	m_stModelInfo.nPicItem = (UINT)wParam;

	return 0;
}

//=============================================================================
// Method		: OnManualTestCmd
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/7/13 - 17:10
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnManualTestCmd(WPARAM wParam, LPARAM lParam)
{
	enLT_TestItem_ID enTestID = (enLT_TestItem_ID)lParam;
	UINT nTestCount = (UINT)wParam;
	UINT nTestResult = 0;
	LRESULT lReturn= RC_OK;
	
	m_stModelInfo.nTestCnt = nTestCount;

	//if (OnEachTest_Motion(enTestID) != RCA_OK)
	//{
	//	return TRUE;
	//}

	for (int i = 0; i < 50; i++)
	{
		DoEvents(33);
	}

	switch (enTestID)
	{
	case TIID_Current:
		m_stModelInfo.nPicItem = PIC_Current;
		OnEachTest_Current(enTestID, nTestCount);
		break;

	//case TIID_OperationMode:
	//	m_stModelInfo.nPicItem = PIC_OperMode;
	//	OnEachTest_OperationMode(enTestID, nTestCount);
	//	break;

	case TIID_CenterPoint:
		m_stModelInfo.nPicItem = PIC_CenterPoint;

		switch (m_stModelInfo.stCenterPoint[nTestCount].stCenterPointOpt.nTestMode)
		{
		case CP_Mode_Measurement:
			lReturn = OnEachTest_CenterPoint(enTestID, nTestCount);
			break;

// 			
			break;

		default:
			break;
		}
		break;

	case TIID_Rotation:
		m_stModelInfo.nPicItem = PIC_Rotate;
		lReturn = OnEachTest_Rotation(enTestID, nTestCount);
		break;

	case TIID_EIAJ:
		m_stModelInfo.nPicItem = PIC_EIAJ;
		lReturn = OnEachTest_EIAJ(enTestID, nTestCount);
		break;

	//case TIID_SFR:
	//	m_stModelInfo.nPicItem = PIC_SFR;
	//	OnEachTest_SFR(enTestID, nTestCount);
	//	break;

	case TIID_FOV:
		m_stModelInfo.nPicItem = PIC_Angle;
		lReturn = OnEachTest_Angle(enTestID, nTestCount);
		break;

	case TIID_Color:
		m_stModelInfo.nPicItem = PIC_Color;
		lReturn = OnEachTest_Color(enTestID, nTestCount);
		break;

	case TIID_Reverse:
		m_stModelInfo.nPicItem = PIC_Reverse;
		lReturn = OnEachTest_Reverse(enTestID, nTestCount);
		break;

	//case TIID_LEDTest:
	//	m_stModelInfo.nPicItem = PIC_LEDTest;
	//	OnEachTest_LEDTest(enTestID, nTestCount);
	//	break;

	case TIID_ParticleManual:
		m_stModelInfo.nPicItem = PIC_ParticleManual;
		lReturn = OnEachTest_ParticleManual(enTestID, nTestCount);
		break;

	case TIID_BlackSpot:
		m_stModelInfo.nPicItem = PIC_BlackSpot;
		lReturn = OnEachTest_Particle(enTestID, nTestCount);
		break;
		
	case TIID_DefectPixel:
		m_stModelInfo.nPicItem = PIC_DefectPixel;
		lReturn = OnEachTest_DefectPixel(enTestID, nTestCount);
		break;

	//case TIID_PatternNoise:
	//	m_stModelInfo.nPicItem = PIC_PatternNoise;
	//	OnEachTest_PatternNoise(enTestID, nTestCount);
	//	break;

	case TIID_IRFilter:
		m_stModelInfo.nPicItem = PIC_IRFilter;
		lReturn = OnEachTest_IRFilter(enTestID, nTestCount);
		break;

	case TIID_Brightness:
		m_stModelInfo.nPicItem = PIC_Brightness;
		lReturn = OnEachTest_Brightness(enTestID, nTestCount);
		break;

	default:
		break;
	}

	//// TempFolder에 저장한다.
	switch (enTestID)
	{
		// 저장.
	//case TIID_Current:
	case TIID_CenterPoint:
	case TIID_Rotation:
	case TIID_EIAJ:
		//	case TIID_SFR:
	case TIID_FOV:
	case TIID_Color:
	case TIID_Reverse:
	case TIID_BlackSpot:
	case TIID_ParticleManual:
	case TIID_Brightness:
	case TIID_IRFilter:
		//	case TIID_PatternNoise:

		if (*m_pnImageSaveType == ImageSaveType_ALL)
		{
			AfxGetApp()->GetMainWnd()->SendMessage(WM_MANUAL_TEST_IMAGESAVE, (LPARAM)m_TestImage, enTestID);

			for (int i = 0; i < 30; i++)
			{
				DoEvents(33);
			}
		}
		else if (*m_pnImageSaveType == ImageSaveType_NG)
		{
			if (lReturn == TER_Fail)
				AfxGetApp()->GetMainWnd()->SendMessage(WM_MANUAL_TEST_IMAGESAVE, (LPARAM)m_TestImage, enTestID);

			for (int i = 0; i < 30; i++)
			{
				DoEvents(33);
			}
		}


		break;

		// 저장 안함.
		//	case TIID_LEDTest:
	case TIID_TestFinalize:
	case TIID_TestInitialize:
		//	case TIID_OperationMode:
		break;

	default:
		break;
	}


	m_tc_Result.SetActiveTab(0);
	m_wnd_EachTestResult.SetTestItemResult(enTestID, nTestCount);

	return TRUE;
}


LRESULT CWnd_RecipeView::OnPrinterTestCmd(WPARAM wParam, LPARAM lParam)
{
	int iPrintingCount = (int)wParam;
	int iSerialCount = (int)lParam;

	if (iPrintingCount < 0)
	{
		return 1;
	}

	for (int j = 0; j <= iSerialCount; j++)
	{
		ST_ZebraPrinterFont stFontData[enLabelDataType_Max];
		for (int i = 0; i < enLabelDataType_Max; i++)
		{
			int iposx = m_stModelInfo.stPrinter.iPosX[i];
			int iposy = m_stModelInfo.stPrinter.iPosY[i];
			int iwidth = m_stModelInfo.stPrinter.iWidthSz[i];
			int iheight = m_stModelInfo.stPrinter.iHeightSz[i];
			CString szText = m_stModelInfo.stPrinter.szText[i];

			if (i == enLabelDataType_Barcode)
			{
				stFontData[i].MakePrintingToFont(1, 1, iposx, iposy, iwidth, iheight, (CStringA)szText);
			}
			else
			{
				if (i == enLabelDataType_Date)
				{
					stFontData[i].MakePrintingToFont(0, 1, iposx, iposy, iwidth, iheight, (CStringA)szText, j);
				}
				else
				{
					stFontData[i].MakePrintingToFont(0, 0, iposx, iposy, iwidth, iheight, (CStringA)szText);
				}
			}

			m_pDevice->Printer.SettingToFontData(i, stFontData[i]);
		}

		CString szPrnPath = m_strPrnPath + _T("\\") + m_stModelInfo.stPrinter.szPrnFileName + _T(".prn");
		m_pDevice->Printer.SendToLabelOutput(iPrintingCount, szPrnPath);

		Sleep(500);
	}

	

	return 1;
}


LRESULT CWnd_RecipeView::OnAutoSettingCmd(WPARAM wParam, LPARAM lParam)
{
	enLT_TestItem_ID enTestID = (enLT_TestItem_ID)lParam;
	UINT nTestCount = (UINT)wParam;

	m_stModelInfo.nTestCnt = nTestCount;

	switch (enTestID)
	{
	case TIID_Current:
		m_stModelInfo.nPicItem = PIC_Current;
		break;

	case TIID_EIAJ:
		m_stModelInfo.nPicItem = PIC_EIAJ;
		break;

	//case TIID_SFR:
	//	m_stModelInfo.nPicItem = PIC_SFR;
	//	break;

	case TIID_Reverse:
		m_stModelInfo.nPicItem = PIC_Reverse;
		break;

	case TIID_Color:
		m_stModelInfo.nPicItem = PIC_Color;
		OnAutoSettingColorTest(nTestCount);
		break;

	case TIID_FOV:
		m_stModelInfo.nPicItem = PIC_Angle;
		OnAutoSettingAngleTest(nTestCount);
		break;

	default:
		break;
	}

	m_tc_Result.SetActiveTab(0);
	m_wnd_EachTestResult.SetTestItemResult(enTestID, nTestCount);

	return TRUE;
}


LRESULT CWnd_RecipeView::OnOptDataResetCmd(WPARAM wParam, LPARAM lParam)
{
	SetModelInfo();

	return TRUE;
}

LRESULT CWnd_RecipeView::OnEdgeOnOff(WPARAM wParam, LPARAM lParam)
{
	EdgeOnOff((BOOL)wParam);
	
	return TRUE;
}

LRESULT CWnd_RecipeView::OnDistortionCorr(WPARAM wParam, LPARAM lParam)
{
	DistortionCorr((BOOL)wParam);

	return TRUE;
}

//=============================================================================
// Method		: OnManualCtrlCmd
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/7/13 - 17:10
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnManualCtrlCmd(WPARAM wParam, LPARAM lParam)
{
	enManual_Commend enCommend = (enManual_Commend)wParam;
	BOOL bLive = (BOOL)lParam;

	if (MUL_ImageLoad == enCommend)
	{
		SetLiveVideo(!bLive);
	}

	return TRUE;
}

//=============================================================================
// Method		: OnManualMotionCmd
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/7/13 - 17:10
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnManualMotionCmd(WPARAM wParam, LPARAM lParam)
{
// 	enManual_Commend enCommend = (enManual_Commend)lParam;
// 
// 	switch (enCommend)
// 	{
// 	case MUL_MOT_Load:
// 		GetOwner()->SendNotifyMessage(WM_MOTOR_LOADING, 0, 0);
// 		break;
// 
// 	case MUL_MOT_Insp:
// 		GetOwner()->SendNotifyMessage(WM_MOTOR_INSPECTION, 0, 0);
// 		break;
// 
// 	case MUL_MOT_Par_Load:
// 		GetOwner()->SendNotifyMessage(WM_MOTOR_PAR_LOAD, 0, 0);
// 		break;
// 
// 	case MUL_MOT_Par_Insp:
// 		GetOwner()->SendNotifyMessage(WM_MOTOR_PAR_INSPECT, 0, 0);
// 		break;
// 
// 	case MUL_MOT_Cyl_In:
// 		GetOwner()->SendNotifyMessage(WM_CYLINDER_PAR_IN, 0, 0);
// 		break;
// 
// 	case  MUL_MOT_Cyl_Out:
// 		GetOwner()->SendNotifyMessage(WM_CYLINDER_PAR_OUT, 0, 0);
// 		break;
// 	
// 	default:
// 		break;
// 	}

	return TRUE;
}

//=============================================================================
// Method		: OnUpdateDataRoiList
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/1/12 - 11:33
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnUpdateDataRoiList(WPARAM wParam, LPARAM lParam)
{
	m_wnd_TestItemCfg.SetModelInfo(&m_stModelInfo);
	return 0;
}

LRESULT CWnd_RecipeView::OnModelSave(WPARAM wParam, LPARAM lParam)
{
	Save_Model();
	return 0;
}

//=============================================================================
// Method		: New_Model
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 16:51
// Desc.		:
//=============================================================================
void CWnd_RecipeView::New_Model()
{
	if (IDYES != AfxMessageBox(_T("Create a New Model file?"), MB_YESNO))
	{
		return;
	}

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);

	CFileDialog fileDlg(FALSE, MODEL_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strModelPath;

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFileTitle = fileDlg.GetFileTitle();
		m_stModelInfo.Reset();

		// 저장	 		
		if (m_fileModel.SaveModelFile(strFullPath, &m_stModelInfo))
		{
			// 리스트 모델 갱신
			m_stModelInfo.szModelFile = strFileTitle;
			SetFullPath(m_stModelInfo.szModelFile);
			SetModelInfo();
			RedrawWindow();
		}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
		m_stModelInfo.szModelFile.ReleaseBuffer();
	}
}

//=============================================================================
// Method		: Save_Model
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 17:15
// Desc.		:
//=============================================================================
void CWnd_RecipeView::Save_Model()
{
	// UI상의 데이터 얻기
	GetModelInfo();
	
	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);

 	if (m_stModelInfo.szModelFile.IsEmpty())
 	{
 	 	CFileDialog fileDlg(FALSE, MODEL_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
 	 	fileDlg.m_ofn.lpstrInitialDir = m_strModelPath;
 	 
 	 	if (fileDlg.DoModal() == IDOK)
 	 	{
 	 		strFullPath = fileDlg.GetPathName();
 	 		strFileTitle = fileDlg.GetFileTitle();	 		
 	 
 	 		// 저장	 		
 			if (m_fileModel.SaveModelFile(strFullPath, &m_stModelInfo))
 			{				
 				// 리스트 모델 갱신
 				m_stModelInfo.szModelFile = strFileTitle;

				SetFullPath(m_stModelInfo.szModelFile);
				SetModel(m_stModelInfo.szModelFile);
				RedrawWindow();
			}

			// 모델 데이터 불러오기
			GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
			m_stModelInfo.szModelFile.ReleaseBuffer();
 	 	}
 	}
 	else
 	{
 		strFullPath.Format(_T("%s%s.%s"), m_strModelPath, m_stModelInfo.szModelFile, MODEL_FILE_EXT);

		//if (m_stModelInfo.nTestMode == TIID_SFR)
		//{
		//	// ---------------------------------------------현재 광축 저장.
		//	
		//	DWORD dwWidth = 0;
		//	DWORD dwHeight = 0;
		//	UINT nChannel = 0;

		//	BOOL bCheckfrm = FALSE;

		//	for (int i = 0; i < 30; i++)
		//	{
		//		  m_TestImage = GetImageBuffer(m_bLive);
		//		Sleep(10);

		//		if (NULL != m_TestImage)
		//		{
		//			int iCheckImage = 0;

		//			for (int y = 0; y < dwHeight; y++)
		//			{
		//				for (int x = 0; x < dwWidth; x++)
		//				{
		//					BYTE byCheckR = m_TestImage->imageData[y * m_TestImage->widthStep + x * 3 + 0];
		//					BYTE byCheckG = m_TestImage->imageData[y * m_TestImage->widthStep + x * 3 + 1];
		//					BYTE byCheckB = m_TestImage->imageData[y * m_TestImage->widthStep + x * 3 + 2];

		//					if (byCheckR == 0x00 && byCheckG == 0x00 && byCheckB == 0x00)
		//					{
		//						iCheckImage++;
		//					}
		//				}
		//			}

		//			if (80 <= ((double)iCheckImage / (double)(dwWidth * dwHeight)) * 100.0)
		//			{
		//			}
		//			else
		//			{
		//				bCheckfrm = TRUE;
		//				break;
		//			}
		//		}
		//	}

		//	if (bCheckfrm == TRUE)
		//	{
		//		for (int i = 0; i < TICnt_SFR; i++)
		//			m_ImageTest.OnTestProcessSFRROI_Auto_Center(FALSE, m_TestImage, &m_stModelInfo.stSFR[i].stSFROpt);
		//		// ---------------------------------------------현재 광축 저장.
		//	}
		//}
		

 	 	// 저장	 	
 	 	if (m_fileModel.SaveModelFile(strFullPath, &m_stModelInfo))
		{
			SetModel(m_stModelInfo.szModelFile);
			RedrawWindow();
		}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
		m_stModelInfo.szModelFile.ReleaseBuffer();
 	}

}

//=============================================================================
// Method		: SaveAs_Model
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 17:15
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SaveAs_Model()
{
	// UI상의 데이터 얻기
	GetModelInfo();

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);

	CFileDialog fileDlg(FALSE, MODEL_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strModelPath;

 	if (fileDlg.DoModal() == IDOK)
 	{
 		strFullPath = fileDlg.GetPathName();
 		strFileTitle = fileDlg.GetFileTitle();
 
 		// 저장	 		
 		if (m_fileModel.SaveModelFile(strFullPath, &m_stModelInfo))
 		{
 			// 리스트 모델 갱신
 			m_stModelInfo.szModelFile = strFileTitle; 			
			SetFullPath(m_stModelInfo.szModelFile);
			RedrawWindow();
 		}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
		m_stModelInfo.szModelFile.ReleaseBuffer();
 	}
}

//=============================================================================
// Method		: Load_Model
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 17:16
// Desc.		:
//=============================================================================
void CWnd_RecipeView::Load_Model()
{
	if (IDNO == AfxMessageBox(_T("Data being edited will be deleted.\r\n Do you want to Continue?"), MB_YESNO))
	{
		return;
	}

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);
	CString strFileSel;
	strFileSel.Format(_T("*.%s"), MODEL_FILE_EXT); 

	// 파일 불러오기
	CFileDialog fileDlg(TRUE, MODEL_FILE_EXT, strFileSel, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strModelPath;
	 
	if (fileDlg.DoModal() == IDOK)
	{
	 	strFullPath = fileDlg.GetPathName();
	 	strFileTitle = fileDlg.GetFileTitle();	 	
	 
 		if (m_fileModel.LoadModelFile(strFullPath, m_stModelInfo))	 	
 	 	{
 	 		// UI에 세팅
 	 		SetModelInfo();
			RedrawWindow();

			m_stModelInfo.szModelFile = strFileTitle;
			SetFullPath(m_stModelInfo.szModelFile);
		}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
		m_stModelInfo.szModelFile.ReleaseBuffer();
	}
}

//=============================================================================
// Method		: GetModelInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 16:58
// Desc.		:
//=============================================================================
void CWnd_RecipeView::GetModelInfo()
{	
	m_wnd_ModelCfg.GetModelInfo(m_stModelInfo);

	m_wnd_TestStepCfg.Get_StepInfo(m_stModelInfo.stStepInfo);
	//m_wnd_EachTestStep.Get_ModelInfo(m_stModelInfo);

	//m_wnd_BarcodeInfoCfg.Get_BarcodeInfo(m_stModelInfo.stBarcodeInfo);

	//m_wnd_DAQCfg.GetModelInfo(m_stModelInfo);

	// 검사 기준 정보
	m_wnd_TestItemCfg.GetModelInfo();
	
	// Pogo
	m_wnd_PogoCfg.SavePogoSetting();
	m_stModelInfo.szPogoName = m_wnd_PogoCfg.GetPogoName();

//	m_wnd_LightCtrlCfg.GetLightInfo(m_stModelInfo.stLightInfo);
}


//=============================================================================
// Method		: SaveMasterSet
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/28 - 17:53
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SaveMasterSet()
{
	SetModelInfo();
	Save_Model();
}

//=============================================================================
// Method		: GetVideoPicStatus
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/7/7 - 9:38
// Desc.		:
//=============================================================================
UINT CWnd_RecipeView::GetVideoPicStatus()
{
	return m_stModelInfo.nPicItem;
}

//=============================================================================
// Method		: IsRecipeTest
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/13 - 17:38
// Desc.		:
//=============================================================================
BOOL CWnd_RecipeView::IsRecipeTest()
{
	return m_wnd_TestItemCfg.IsTestAll();
}


//=============================================================================
// Method		: SetModelInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 16:58
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetModelInfo()
{
	m_wnd_VideoView.SetModelInfo(&m_stModelInfo);
	m_wnd_ImageView.SetModelInfo(&m_stModelInfo);

	m_wnd_ModelCfg.SetModelInfo(&m_stModelInfo);
	m_wnd_ModelCfg.SetPath(m_strI2CPath);

	m_wnd_TestStepCfg.Set_StepInfo(&m_stModelInfo.stStepInfo);
	//m_wnd_EachTestStep.Set_ModelInfo(&m_stModelInfo);
	//m_wnd_BarcodeInfoCfg.Set_BarcodeInfo(&m_stModelInfo.stBarcodeInfo);

	//m_wnd_DAQCfg.SetModelInfo(&m_stModelInfo);

 	// 검사 기준 정보	
 	m_wnd_TestItemCfg.SetModelInfo(&m_stModelInfo);
 	
 	// Pogo
 	m_wnd_PogoCfg.SetPogoFile(m_stModelInfo.szPogoName);

	// 광원 보드
//	m_wnd_LightCtrlCfg.SetLightInfo(&m_stModelInfo.stLightInfo);

	RedrawWindow();
}

//=============================================================================
// Method		: SetFullPath
// Access		: protected  
// Returns		: void
// Parameter	: __in LPCTSTR szModelName
// Qualifier	:
// Last Update	: 2016/5/27 - 11:33
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetFullPath(__in LPCTSTR szModelName)
{
	m_strModelFullPath.Format(_T("%s%s.%s"), m_strModelPath, szModelName, MODEL_FILE_EXT);
	
	m_st_Location.SetText(m_strModelFullPath);
}

//=============================================================================
// Method		: ChangePath
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR lpszModelPath
// Qualifier	:
// Last Update	: 2016/6/24 - 13:58
// Desc.		:
//=============================================================================
void CWnd_RecipeView::ChangePath(__in LPCTSTR lpszModelPath)
{
	if (NULL != lpszModelPath)
		m_strModelPath = lpszModelPath;

	m_ModelWatch.SetWatchOption(m_strModelPath, MODEL_FILE_EXT);
	m_ModelWatch.EndWatchThrFunc();
	m_ModelWatch.BeginWatchThrFunc();
}

//=============================================================================
// Method		: SetModel
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szModel
// Qualifier	:
// Last Update	: 2016/3/18 - 16:43
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetModel(__in LPCTSTR szModel)
{
	CString strFullPath;
	strFullPath.Format(_T("%s%s.%s"), m_strModelPath, szModel, MODEL_FILE_EXT);

	if (m_fileModel.LoadModelFile(strFullPath, m_stModelInfo))
	{
	 	// UI에 세팅
	 	SetModelInfo();
		
		m_stModelInfo.szModelFile = szModel;
		if (!m_stModelInfo.szModelFile.IsEmpty())
		{
			int iSel = m_lst_ModelList.FindStringExact(0, m_stModelInfo.szModelFile);

			if (0 <= iSel)
			{
				m_lst_ModelList.SetCurSel(iSel);
			}

			SetFullPath(m_stModelInfo.szModelFile);
		}
	}
}

//=============================================================================
// Method		: RefreshModelFileList
// Access		: public  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2016/6/25 - 14:24
// Desc.		:
//=============================================================================
void CWnd_RecipeView::RefreshModelFileList(__in const CStringList* pFileList)
{
	m_lst_ModelList.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();
	 
	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_lst_ModelList.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_stModelInfo.szModelFile.IsEmpty())
	{
		int iSel = m_lst_ModelList.FindStringExact(0, m_stModelInfo.szModelFile);

		if (0 <= iSel)
		{
			m_lst_ModelList.SetCurSel(iSel);
		}
	}
}

//=============================================================================
// Method		: InitOptionView
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/25 - 14:37
// Desc.		:
//=============================================================================
void CWnd_RecipeView::InitOptionView()
{
	//SetOptionView(OPTV_TEST);

	m_tc_Option.SetActiveTab(0);
}

//=============================================================================
// Method		: SavePogoCount
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/18 - 15:42
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SavePogoCount()
{
	m_wnd_PogoCfg.SavePogoSetting();
}

//=============================================================================
// Method		: RedrawWindow
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/16 - 19:26
// Desc.		:
//=============================================================================
void CWnd_RecipeView::RedrawWindow()
{
	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	}
}

//=============================================================================
// Method		: MessageView
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2017/7/13 - 17:18
// Desc.		:
//=============================================================================
BOOL CWnd_RecipeView::MessageView(__in CString szText, __in BOOL bMode)
{
	CWnd_MessageView*	m_pwndMessageView;
	m_pwndMessageView = new CWnd_MessageView;

	BOOL bResult = FALSE;

	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);

	m_pwndMessageView->CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 10)), _T("Message Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL);
	m_pwndMessageView->EnableWindow(TRUE);
	m_pwndMessageView->CenterWindow();
	m_pwndMessageView->SetWarringMessage(szText);

	if (m_pwndMessageView->DoModal(bMode) == TRUE)
		bResult = TRUE;
	else
		bResult = FALSE;

	AfxGetApp()->GetMainWnd()->EnableWindow(TRUE);

	delete m_pwndMessageView;

	return bResult;
}

UINT CWnd_RecipeView::OnEachTest_Motion(__in UINT nTestItemID)
{
	UINT lReturn = RCA_OK;
//
//	switch (nTestItemID)
//	{
//	case TIID_TestFinalize:
//		//lReturn = Motion_StageY_Initial();
//		lReturn = m_pDevice->MotionSequence.OnAction_Load();
//		//lReturn = m_pDevice->MotionSequence.OnAction_Par_Cylinder_Out(); => 조건 필요
//		break;
//
//	case TIID_TestInitialize:
//		lReturn = m_pDevice->MotionSequence.OnAction_Load();
//		//lReturn = m_pDevice->MotionSequence.OnAction_Par_Cylinder_Out();
//		break;
//	case TIID_Current:
//		break;
//// 	case TIID_VideoSignal:
//// 		break;
//// 	case TIID_Reset:
//// 		break;
//
//	case TIID_CenterPoint:
//	case TIID_Rotation:
//	//case TIID_SFR:
//	case TIID_FOV:
//	case TIID_Color:
//	case TIID_Reverse:
//		//lReturn = Motion_StageY_BlemishZone_Out();
//		//lReturn = m_pDevice->MotionSequence.OnAction_Par_Load();
//
//		if (lReturn == RCA_OK)
//		{
//			lReturn = Motion_StageY_CL_Distance(m_stModelInfo.dbTestDistance);
//		}
//		break;
//
//	//case TIID_LEDTest:
//	case TIID_ParticleManual:
//	case TIID_BlackSpot:
//	case TIID_DefectPixel:
//	case TIID_Brightness:
//		lReturn = m_pDevice->MotionSequence.OnAction_LED_ON();
//	case TIID_IRFilter:
//	//case TIID_PatternNoise:
//		//lReturn = m_pDevice->MotionSequence();
//		//lReturn = Motion_StageY_BlemishZone_In(nTestItemID);
//		lReturn = m_pDevice->MotionSequence.OnAction_Par_Load();
//		lReturn = m_pDevice->MotionSequence.OnAction_Par_Cylinder_In();
//
//		lReturn = m_pDevice->MotionSequence.OnAction_Par_Inspect();
//		break;
//
//	default:
//		break;
//	}
//
//	if (nTestItemID == TIID_IRFilter)
//	{
//		lReturn = m_pDevice->MotionSequence.OnAction_IR_LED_ON();
//	}
//
//	if (lReturn != RCA_OK)
//	{
//		AfxMessageBox(_T("MACHINE ERROR!!"));
//	}
//
	return lReturn;
}

void CWnd_RecipeView::TestProcessCurrent(__in ST_Current_Opt* pstOpt, __out ST_Current_Result &stResult)
{
	if (pstOpt == NULL)
		return;

	BOOL bResult = TRUE;
	double iCurrent[Def_CamBrd_Channel] = { 0, };

	// 1. 측정
	if (RCA_OK == m_pDevice->PCBCamBrd[0].Send_GetCurrent(iCurrent))
	{
		// 2. 옵셋
		for (int iCh = 0; iCh < Def_CamBrd_Channel; iCh++)
		{
			stResult.iValue[iCh] = iCurrent[iCh];
			stResult.iValue[iCh] *= pstOpt->dbOffset[iCh];
			//stResult.iValue[iCh] *= 0.1;

			// 3. 결과	 
			if (pstOpt->nSpecMin[iCh] <= stResult.iValue[iCh] && pstOpt->nSpecMax[iCh] >= stResult.iValue[iCh])
			{
				stResult.nEachResult[iCh] = TER_Pass;
			}
			else
			{
				stResult.nEachResult[iCh] = TER_Fail;
				bResult = FALSE;
			}
		}

		if (bResult == TRUE)
		{
			stResult.nResult = TER_Pass;
		}
		else
		{
			stResult.nResult = TER_Fail;
		}
		
	}
}

void CWnd_RecipeView::TestProcessVideoSignal(__in UINT nCh, __out UINT& nResult)
{
	if (m_pDevice == NULL)
	{
		nResult = RCA_NG;
		return;
	}

// 	UINT nTestResult = RCA_OK;
// 	BYTE ReadData[1] = { 0, };
// 
// 	// Lock 신호 Read
// 	if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x44, 1, 0x00, 1, ReadData) == TRUE)
// 	{
// 		if (ReadData[0] == 0xFF)
// 		{
// 			nTestResult = RCA_OK;
// 		}
// 		else
// 		{
// 			nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		nTestResult = RCA_NG;
// 	}

//	nResult = nTestResult;
}

void CWnd_RecipeView::TestProcessReset(__in UINT nCh, __in UINT nResetDelay, __out UINT& nResult)
{
	if (m_pDevice == NULL)
	{
		nResult = RCA_NG;
		return;
	}

	BOOL bResult = TRUE;
	UINT nTestResult = RCA_OK;

	BYTE ReadData[1] = { 0, };

	BYTE WriteData1[1] = { 0x00 };
	BYTE WriteData2[1] = { 0x00 };
	BYTE WriteData3[1] = { 0x01 };

	// Reset Sequence


	nResult = nTestResult;

}

void CWnd_RecipeView::TestProcessFrameCheck(__in UINT nCh, __in UINT nMode, __out UINT& nResult)
{
	if (m_pDevice == NULL)
	{
		nResult = RCA_NG;
		return;
	}

	DWORD dwFrameRate = 0;
	

	switch (nMode)
	{
	case 0: // 15fps
		if (dwFrameRate >= 15)
		{
			nResult = RCA_OK;
		}
		break;

	case 1: // 30fps
		if (dwFrameRate >= 30)
		{
			nResult = RCA_OK;
		}
		break;

	default:
		break;
	}
}

LRESULT CWnd_RecipeView::OnEachTest_Angle(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	// TEST
	LRESULT lResult = RC_OK;
	double dValue[IDX_AL_Max] = { 0, };

	int iCount = 0;

	for (int i = 0; i < 30; i++)
	{
		m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}

	if (m_TestImage != NULL)
	{
		m_stModelInfo.stAngle[nTestIdx].bTestMode = TRUE;
		m_ImageTest.OnTestProcessAngle(
			m_TestImage,
			&m_stModelInfo.stAngle[nTestIdx].stAngleOpt,
			m_stModelInfo.stAngle[nTestIdx].stAngleResult);

		lResult = m_stModelInfo.stAngle[nTestIdx].stAngleResult.nResult;
	}
	else{
		lResult =TER_Fail;

	}
	return lResult;
}


//=============================================================================
// Method		: OnEachTest_CenterPoint
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/4/19 - 15:43
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnEachTest_CenterPoint(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;

	int iValueX = 0;
	int iValueY = 0;

	for (int i = 0; i < 30; i++)
	{
		m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}

	if (m_TestImage != NULL)
	{
		//for (int i = 0; i < 30; i++)
		{
			m_ImageTest.OnSetCameraType(m_stModelInfo.nCameraType);
			m_ImageTest.OnTestProcessCenterPoint(m_TestImage, &m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointOpt, m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointResult);
		}	

		lResult = m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.nResult;
	}
	else{
		m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.nResult = TER_Fail;
		lResult = TER_Fail;
	}

	return lResult;
}

//=============================================================================
// Method		: OnEachTest_CenterPoint_Manual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/4/19 - 15:42
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnEachTest_CenterPoint_Manual(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	OnPopupCenterPtTunningResult();
}

//=============================================================================
// Method		: OnEachTest_CenterPoint_Auto
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/4/19 - 15:42
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnEachTest_CenterPoint_Auto(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	

	int iValueX = 0;
	int iValueY = 0;

	for (UINT nCnt = 0; nCnt < m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointOpt.nTryCnt; nCnt++)
	{
		// 초기 위치
		OpticalCenter_Adj_Horizon(0, 0);
		OpticalCenter_Adj_Vertical(0, 0);

		for (int i = 0; i < 50; i++)
		{
			DoEvents(33);
		}

		for (int i = 0; i < 30; i++)
		{
			  m_TestImage = GetImageBuffer(m_bLive);
			Sleep(33);

			if (m_TestImage != NULL)
				break;
		}

		if (m_TestImage != NULL)
		{
			m_ImageTest.OnSetCameraType(m_stModelInfo.nCameraType);
			m_ImageTest.OnTestProcessCenterPoint(m_TestImage, &m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointOpt, m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointResult);

			// 광축 조정
			OpticalCenter_Adj_Horizon(0, m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetX);
			OpticalCenter_Adj_Vertical(0, m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointResult.iResultOffsetY);

			for (int i = 0; i < 50; i++)
			{
				DoEvents(33);
			}

		}

		for (int i = 0; i < 30; i++)
		{
			  m_TestImage = GetImageBuffer(m_bLive);
			Sleep(33);

			if (m_TestImage != NULL)
				break;
		}

		if (m_TestImage != NULL)
		{
			m_ImageTest.OnSetCameraType(m_stModelInfo.nCameraType);
			m_ImageTest.OnTestProcessCenterPoint(m_TestImage, &m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointOpt, m_stModelInfo.stCenterPoint[nTestIdx].stCenterPointResult);
		}
 	}

	OpticalCenter_Adj_AllWrite(0);

	// Cam On
// 	CameraRegToFunctionChange(0, 1, 1, 1);
// 
// 	for (int i = 0; i < 50; i++)
// 	{
// 		DoEvents(33);
// 	}

	// Cam On
	CameraOnOff(VideoView_Ch_1, m_stModelInfo.nGrabType, 0, 1, 1, 1, OFF);
	CameraOnOff(VideoView_Ch_1, m_stModelInfo.nGrabType, 0, 1, 1, 1, ON);

}

//=============================================================================
// Method		: OnEachTest_Color
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestIdx
// Qualifier	:
// Last Update	: 2018/4/19 - 15:51
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnEachTest_Color(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	
	LRESULT lResult = RC_OK;
	int iRed[ROI_CL_Max] = { 0, };
	int iGreen[ROI_CL_Max] = { 0, };
	int iBlue[ROI_CL_Max] = { 0, };

	int iCount = 0;

	for (int i = 0; i < 30; i++)
	{
		  m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}
	m_stModelInfo.stColor[nTestIdx].bTestMode = TRUE;
	if (m_TestImage != NULL)
	{
		m_ImageTest.OnTestProcessColor(
			m_TestImage,
			&m_stModelInfo.stCenterPoint[0].stCenterPointOpt,
			&m_stModelInfo.stColor[nTestIdx].stColorOpt,
			m_stModelInfo.stColor[nTestIdx].stColorResult);
		lResult = m_stModelInfo.stColor[nTestIdx].stColorResult.nResult;
	}
	else{
		lResult = TER_Fail;
		m_stModelInfo.stColor[nTestIdx].stColorResult.nResult = lResult;
	}

	return lResult;
}

void CWnd_RecipeView::OnEachTest_Current(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	TestProcessCurrent(&m_stModelInfo.stCurrent[nTestIdx].stCurrentOpt, m_stModelInfo.stCurrent[nTestIdx].stCurrentResult);
}


void CWnd_RecipeView::OnEachTest_OperationMode(__in UINT nfpsMode, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
//	float fVoltageOn[2] = { m_stModelInfo.fVoltage[0], 0 };
//	float fVoltageOff[2] = { 0, 0 };
//
//	// 15fps
//	nfpsMode = 0;
//	m_stModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Fail;
//	m_stModelInfo.stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Fail;
//	m_stModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] = TER_Fail;
//	m_stModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] = TER_Fail;
//	m_stModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode].Reset();
//
//	if (CameraRegToFunctionChange(0, nfpsMode, 1, 1))
// 	{
//		for (int i = 0; i < 50; i++)
//			DoEvents(33);
//
//		TestProcessVideoSignal(VideoView_Ch_1, m_stModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode]);
//		if (m_stModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
//		{
//			TestProcessFrameCheck(VideoView_Ch_1, nfpsMode, m_stModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode]);
//			if (m_stModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass)
//			{
//				TestProcessCurrent(&m_stModelInfo.stOperMode[nTestIdx].stCurrOpt[nfpsMode], m_stModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode]);
//			}
//		}
//
//		m_stModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Pass;
//		//TestProcessReset(VideoView_Ch_1,
//		//	m_stModelInfo.stOperMode[nTestIdx].nResetDelay, m_stModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode]);
//
// 	}
//
//	if (m_stModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode].nEachResult[0] == TER_Pass
//		&& m_stModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] == TER_Pass
//		&& m_stModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass
//		&& m_stModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
//	{
//		m_stModelInfo.stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Pass;
//	}
//
//	// 30fps
//	nfpsMode = 1;
//	m_stModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Fail;
//	m_stModelInfo.stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Fail;
//	m_stModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] = TER_Fail;
//	m_stModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] = TER_Fail;
//	m_stModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode].Reset();
//
//	if (CameraRegToFunctionChange(0, nfpsMode, 1, 1))
//	{
//		for (int i = 0; i < 50; i++)
//			DoEvents(33);
//
//		TestProcessVideoSignal(VideoView_Ch_1, m_stModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode]);
//		if (m_stModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
//		{
//			TestProcessFrameCheck(VideoView_Ch_1, nfpsMode, m_stModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode]);
//			if (m_stModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass)
//			{
//				TestProcessCurrent(&m_stModelInfo.stOperMode[nTestIdx].stCurrOpt[nfpsMode], m_stModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode]);
//			}
//		}
//
//		m_stModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Pass;
//		//TestProcessReset(VideoView_Ch_1,
//		//	m_stModelInfo.stOperMode[nTestIdx].nResetDelay, m_stModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode]);
//
//	}
//
//	//CameraOnOff(VideoView_Ch_1, m_stModelInfo.nGrabType, 0, 1, 1, 1, OFF);
//	//CameraOnOff(VideoView_Ch_1, m_stModelInfo.nGrabType, 0, 1, 1, 1, ON);
//
//
//	if (m_stModelInfo.stOperMode[nTestIdx].stCurrResult[nfpsMode].nEachResult[0] == TER_Pass
//		&& m_stModelInfo.stOperMode[nTestIdx].nResetResult[nfpsMode] == TER_Pass
//		&& m_stModelInfo.stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass
//		&& m_stModelInfo.stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
//	{
//		m_stModelInfo.stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Pass;
//	}
//
//	if (m_stModelInfo.stOperMode[nTestIdx].nEachResult[0] == TER_Pass
//		&& m_stModelInfo.stOperMode[nTestIdx].nEachResult[1] == TER_Pass)
//	{
//		m_stModelInfo.stOperMode[nTestIdx].nResult = RCA_OK;
//	}
//	else
//	{
//		m_stModelInfo.stOperMode[nTestIdx].nResult = RCA_NG;
//	}
}

LRESULT CWnd_RecipeView::OnEachTest_EIAJ(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{

	LRESULT lResult = RC_OK;
	for (int i = 0; i < 30; i++)
	{
		  m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}

	if (m_TestImage != NULL)
	{
		m_ImageTest.OnTestProcessEIAJ(m_TestImage, &m_stModelInfo.stEIAJ[nTestIdx]);
		lResult = m_stModelInfo.stEIAJ[nTestIdx].stEIAJData.nResult;
	}
	else{
		lResult = m_stModelInfo.stEIAJ[nTestIdx].stEIAJData.nResult = TER_Fail;
	}

	return lResult;
}


void CWnd_RecipeView::OnEachTest_SFR(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	//

	//m_stModelInfo.bI2CFile_1 = m_stModelInfo.stSFR[nTestIdx].stSFROpt.bIICFile_1;
	//if (m_stModelInfo.bI2CFile_1 == TRUE)
	//{
	//	m_stModelInfo.szI2CFile_1 = m_stModelInfo.stSFR[nTestIdx].stSFROpt.szIICFile_1;

	//	CameraOnOff(VideoView_Ch_1, m_stModelInfo.nGrabType, 0, 1, 1, 1, OFF);
	//	CameraOnOff(VideoView_Ch_1, m_stModelInfo.nGrabType, 0, 1, m_stModelInfo.stSFR[nTestIdx].stSFROpt.bDistortion, m_stModelInfo.stSFR[nTestIdx].stSFROpt.bEdge, ON);
	//}
	//else
	//{
	//	EdgeOnOff(m_stModelInfo.stSFR[nTestIdx].stSFROpt.bEdge);
	//	for (int i = 0; i < 30; i++)
	//	{
	//		DoEvents(33);
	//	}

	//	DistortionCorr(m_stModelInfo.stSFR[nTestIdx].stSFROpt.bDistortion);
	//	for (int i = 0; i < 30; i++)
	//	{
	//		DoEvents(33);
	//	}
	//}

	//for (int i = 0; i < 30; i++)
	//{
	//	  m_TestImage = GetImageBuffer(m_bLive);
	//	Sleep(33);

	//	if (m_TestImage != NULL)
	//		break;
	//}

	//if (m_TestImage != NULL)
	//{
	//	m_ImageTest.OnTestProcessSFR(
	//		m_TestImage,
	//		&m_stModelInfo.stSFR[nTestIdx].stSFROpt,
	//		m_stModelInfo.stSFR[nTestIdx].stSFRResult);
	//}

	//if (m_stModelInfo.bI2CFile_1 == TRUE)
	//{
	//	float fVoltageOn[2] = { m_stModelInfo.fVoltage[0], 0 };
	//	float fVoltageOff[2] = { 0, 0 };

	//	// OFF
	//	CameraOnOff(VideoView_Ch_1, m_stModelInfo.nGrabType, 0, 1, 1, 1, OFF);
	//	SetLuriCamBrd_Volt(fVoltageOff);
	//	for (int i = 0; i < 10; i++)
	//		DoEvents(33);

	//	// ON
	//	SetLuriCamBrd_Volt(fVoltageOn);
	//	for (int i = 0; i < 10; i++)
	//		DoEvents(33);
	//	CameraOnOff(VideoView_Ch_1, m_stModelInfo.nGrabType, 0, 1, 1, 1, ON);
	//}
	//else
	//{
	//	DistortionCorr(TRUE);
	//	for (int i = 0; i < 30; i++)
	//	{
	//		DoEvents(33);
	//	}

	//	EdgeOnOff(TRUE);
	//	for (int i = 0; i < 30; i++)
	//	{
	//		DoEvents(33);
	//	}
	//}


}



LRESULT CWnd_RecipeView::OnEachTest_Reverse(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;

	for (int i = 0; i < 30; i++)
	{
		  m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}

	if (m_TestImage != NULL)
	{
		m_stModelInfo.stReverse[nTestIdx].bTestMode = TRUE;
		m_ImageTest.OnTestProcessReverseColor(
			m_TestImage,
			&m_stModelInfo.stReverse[nTestIdx].stReverseOpt,
			m_stModelInfo.stReverse[nTestIdx].stReverseResult);
		lResult = m_stModelInfo.stReverse[nTestIdx].stReverseResult.nResult;
	}
	else{
		lResult = m_stModelInfo.stReverse[nTestIdx].stReverseResult.nResult = TER_Fail;
	}

	return lResult;
}


void CWnd_RecipeView::OnEachTest_LEDTest(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	// LED Pop Up Test
	//m_stModelInfo.stLED[nTestIdx].stLEDCurrResult.nResetCount = 0;
	//OnPopupLEDTestResult(nTestIdx, m_stModelInfo.stLED[nTestIdx].stLEDCurrResult.nResult);
}


LRESULT CWnd_RecipeView::OnEachTest_ParticleManual(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	OnPopupParticleManualResult(m_stModelInfo.stParticleMA[nTestIdx].stParticleResult.nResult);

	return TRUE;
}


void CWnd_RecipeView::OnEachTest_VideoSignal(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	//m_stModelInfo.stVideoSignal[nTestIdx].nResult = TER_Fail;

	//TestProcessVideoSignal(VideoView_Ch_1, 
	//	m_stModelInfo.stVideoSignal[nTestIdx].nResult);
}


void CWnd_RecipeView::OnEachTest_Reset(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	//m_stModelInfo.stReset[nTestIdx].nResult = TER_Fail;

	//TestProcessReset(VideoView_Ch_1,
	//	m_stModelInfo.stReset[nTestIdx].nDelay, m_stModelInfo.stReset[nTestIdx].nResult);
}


void CWnd_RecipeView::OnEachTest_PatternNoise(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	//

	//for (int i = 0; i < 30; i++)
	//{
	//	  m_TestImage = GetImageBuffer(m_bLive);
	//	Sleep(33);

	//	if (m_TestImage != NULL)
	//		break;
	//}

	//if (m_TestImage != NULL)
	//{
	//	m_ImageTest.OnTestProcessPatternNoise(
	//		m_TestImage,
	//		&m_stModelInfo.stPatternNoise[nTestIdx].stPatternNoiseOpt,
	//		m_stModelInfo.stPatternNoise[nTestIdx].stPatternNoiseResult);
	//}
}


LRESULT CWnd_RecipeView::OnEachTest_IRFilter(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;

	for (int i = 0; i < 30; i++)
	{
		  m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}

	if (m_TestImage != NULL)
	{
		m_ImageTest.OnTestProcessIRFilter(
			m_TestImage,
			&m_stModelInfo.stIRFilter[nTestIdx].stIRFilterOpt,
			m_stModelInfo.stIRFilter[nTestIdx].stIRFilterResult);

		lResult = m_stModelInfo.stIRFilter[nTestIdx].stIRFilterResult.nResult;
	}
	else{
		lResult = m_stModelInfo.stIRFilter[nTestIdx].stIRFilterResult.nResult = TER_Fail;
	}

	return lResult;
}


void CWnd_RecipeView::OnEachTest_IRFilterManual(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
// 	
// 
// 	for (int i = 0; i < 30; i++)
// 	{
// 		  m_TestImage = GetImageBuffer(m_bLive);
// 		Sleep(10);
// 
// 		if (m_TestImage != NULL)
// 			break;
// 	}
// 
// 	if (m_TestImage != NULL)
	//{
	//	OnPopupParticleManualResult(m_stModelInfo.stIRFilterManual[nTestIdx].stIRFilterResult.nResult);
	//}

}

LRESULT CWnd_RecipeView::OnEachTest_Particle(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{

	LRESULT lResult = RC_OK;


	for (int i = 0; i < 30; i++)
	{
		m_TestImage = GetImageBuffer(m_bLive);
		Sleep(10);

		if (m_TestImage != NULL)
			break;
	}
	if (m_TestImage != NULL)
	{
		m_ImageTest.OnTestProcessParticle(
			m_TestImage,
			&m_stModelInfo.stBlackSpot[nTestIdx].stParticleOpt,
			m_stModelInfo.stBlackSpot[nTestIdx].stParticleResult);

		lResult = m_stModelInfo.stIRFilter[nTestIdx].stIRFilterResult.nResult;
	}
	else{
		lResult = m_stModelInfo.stIRFilter[nTestIdx].stIRFilterResult.nResult = TER_Fail;
	}

	return lResult;
 
}


LRESULT CWnd_RecipeView::OnEachTest_DefectPixel(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;


	for (int i = 0; i < 30; i++)
	{
		m_TestImage = GetImageBuffer(m_bLive);
		Sleep(10);

		if (m_TestImage != NULL)
			break;
	}
	if (m_TestImage != NULL)
	{
		m_ImageTest.OnTestProcessDefectPixel(
			m_TestImage,
			&m_stModelInfo.stDefectPixel[nTestIdx].stDefectPixelOpt,
			m_stModelInfo.stDefectPixel[nTestIdx].stDefectPixelData);

		lResult = m_stModelInfo.stIRFilter[nTestIdx].stIRFilterResult.nResult;
	}
	else{
		lResult = m_stModelInfo.stIRFilter[nTestIdx].stIRFilterResult.nResult = TER_Fail;
	}

	return lResult;
}

LRESULT CWnd_RecipeView::OnEachTest_Brightness(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;


	double dValue = 0;
	int iCount = 0;

	for (int i = 0; i < 30; i++)
	{
		  m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}

	if (m_TestImage != NULL)
	{
		m_ImageTest.OnTestProcessBrightness(m_TestImage,
			&m_stModelInfo.stBrightness[nTestIdx].stBrightnessOpt,
			m_stModelInfo.stBrightness[nTestIdx].stBrightnessResult);
		lResult = m_stModelInfo.stBrightness[nTestIdx].stBrightnessResult.nResult;
	}
	else{
		lResult = m_stModelInfo.stBrightness[nTestIdx].stBrightnessResult.nResult = TER_Fail;
	}

	return lResult;
}

LRESULT CWnd_RecipeView::OnEachTest_Rotation(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	
	LRESULT lResult = RC_OK;

	double dValue = 0;
	int iCount = 0;

	for (int i = 0; i < 30; i++)
	{
		  m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}
	m_stModelInfo.stRotate[nTestIdx].bTestMode = TRUE;
	if (m_TestImage != NULL)
	{
		m_ImageTest.OnTestProcessRotate(
			m_TestImage,
			&m_stModelInfo.stCenterPoint[0].stCenterPointOpt,
			&m_stModelInfo.stRotate[nTestIdx].stRotateOpt,
			m_stModelInfo.stRotate[nTestIdx].stRotateResult);
		lResult = m_stModelInfo.stRotate[nTestIdx].stRotateResult.nResult;
	}
	else{
		lResult = m_stModelInfo.stRotate[nTestIdx].stRotateResult.nResult = TER_Fail;
	}

	return lResult;
}

UINT CWnd_RecipeView::OnEachTestMotion(__in UINT nAxisItemID, __in double dbPos)
{
	if (m_pDevice == NULL)
		return RCA_MachineCheck;

	// Exception Process...
	switch (nAxisItemID)
	{
	case AX_StageDistance:

// 		// Y축의 지령 펄스가 0 ~ 100000.0 사이이면,
// 		if (120000.0 < dbPos
// 			&& 250000.0 >= dbPos)
// 		{
// 			// X축의 위치가 0 보다 크면 예외처리.
// 			if (40000.0 < m_pDevice->MotionManager.GetCurrentPos(AX_StageX))
// 			{
// 				if (!m_pDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, AX_StageX, 0))
// 				{
// 					return RCA_MachineCheck;
// 				}
// 
// 				return RCA_MachineExpcetion;
// 			}
// 		}
// 
// 		if (!m_pDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, nAxisItemID, dbPos))
// 		{
// 			return RCA_MachineCheck;
// 		}
// 
// 		break;

		if (!m_pDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, nAxisItemID, dbPos))
		{
			return RCA_MachineCheck;
		}

	default:

		if (!m_pDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, nAxisItemID, dbPos))
		{
			return RCA_MachineCheck;
		}

		break;
	}

	return RCA_OK;
}

UINT CWnd_RecipeView::OnEachTestIOSignal(__in UINT nIO, __in UINT nSignal)
{
	if (m_pDevice == NULL)
		return RCA_MachineCheck;

	if (!m_pDevice->DigitalIOCtrl.Set_DO_Status(nIO, (enum_IO_SignalType)nSignal))
		return RCA_MachineCheck;

	return RCA_OK;
}

void CWnd_RecipeView::OnAutoSettingAngleTest(__in UINT nTestIdx)
{
	

	for (int i = 0; i < 30; i++)
	{
		  m_TestImage = GetImageBuffer(m_bLive);
		Sleep(10);
		if (m_TestImage != NULL)
			break;
	}
	if (m_TestImage != NULL)
	{
		m_ImageTest.OnAutoSettingProcessAngle(m_TestImage, &m_stModelInfo.stAngle[nTestIdx].stAngleOpt, m_stModelInfo.stAngle[nTestIdx].stAngleResult);

		SetModelInfo();
	}
	
}

void CWnd_RecipeView::OnAutoSettingColorTest(__in UINT nTestIdx)
{
	

	for (int i = 0; i < 30; i++)
	{
		  m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}
	if (m_TestImage != NULL)
	{
		m_ImageTest.OnAutoSettingProcessColor(m_TestImage,
			&m_stModelInfo.stCenterPoint[0].stCenterPointOpt,
			&m_stModelInfo.stColor[nTestIdx].stColorOpt, m_stModelInfo.stColor[nTestIdx].stColorResult);

		SetModelInfo();
	}
}

void CWnd_RecipeView::OnPopupParticleManualResult(__out UINT& nTestResult)
{

	for (int i = 0; i < 30; i++)
	{
		m_TestImage = GetImageBuffer(m_bLive);
		Sleep(33);

		if (m_TestImage != NULL)
			break;
	}
	if (m_TestImage != NULL)
	{
		m_DlgParticle.SetOwner(this);
		m_DlgParticle.SetPtr_Device(m_pDevice);
		m_DlgParticle.SetPtr_Modelinfo(&m_stModelInfo);
		m_DlgParticle.SetVideoChannel(VideoView_Ch_1);

		if (m_DlgParticle.DoModal() == IDOK)
		{
			nTestResult = TER_Pass;
		}
		else
		{
			nTestResult = TER_Fail;
		}
	}

}

void CWnd_RecipeView::OnPopupCenterPtTunningResult()
{
	m_DlgCenterPtTunning.SetOwner(this);
	m_DlgCenterPtTunning.SetPtr_Device(m_pDevice);
	m_DlgCenterPtTunning.SetPtr_Modelinfo(&m_stModelInfo);
	m_DlgCenterPtTunning.SetVideoChannel(VideoView_Ch_1);
	m_DlgCenterPtTunning.DoModal();
}

void CWnd_RecipeView::OnPopupLEDTestResult(__in UINT nTestIdx, __out UINT& nTestResult)
{
	//m_DlgLEDTest.SetOwner(this);
	//m_DlgLEDTest.SetPtr_Device(m_pDevice);
	//m_DlgLEDTest.SetPtr_Modelinfo(&m_stModelInfo);
	//m_DlgLEDTest.SetVideoChannel(VideoView_Ch_1);
	//
	//if (m_DlgLEDTest.DoModal() == IDOK)
	//{
	//	if (m_stModelInfo.stLED[nTestIdx].stLEDCurrResult.nEachResult[0] == TER_Pass)
	//	{
	//		nTestResult = TER_Pass;
	//	}
	//	else
	//	{
	//		nTestResult = TER_Fail;
	//	}
	//}
	//else
	//{
	//	nTestResult = TER_Fail;
	//}
}

//=============================================================================
// Method		: OnAutoSettingOpticalCenter
// Access		: protected  
// Returns		: void
// Parameter	: __in int iCenterX
// Parameter	: __in int iCenterY
// Parameter	: __in int iTargerX
// Parameter	: __in int iTargerY
// Qualifier	:
// Last Update	: 2018/4/19 - 15:47
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnAutoSettingOpticalCenter(__in int iCenterX, __in int iCenterY, __in int iTargerX, __in int iTargerY)
{
	;

}


UINT CWnd_RecipeView::OpticalCenter_Adj(__in UINT nAxis, __in UINT nCh, __in int iOffset)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffset[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, };

	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC = 0x00;

	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	int iZeroOffset = 0;

	switch (nAxis)
	{
	case 0:
		if (iOffset < 0)
		{
			WriteDataOffset[5] = 0x16;
			iZeroOffset = 43;

			WriteDataCRC = (iZeroOffset + iOffset) - 1;

			WriteDataOffset[6] = (BYTE)(256 + iOffset);
			WriteDataOffset[7] = WriteDataMark[1];
			WriteDataOffset[8] = WriteDataCRC;
		}
		else
		{
			WriteDataOffset[5] = 0x16;
			iZeroOffset = 43;

			WriteDataCRC = iZeroOffset + iOffset;

			WriteDataOffset[6] = (BYTE)iOffset;
			WriteDataOffset[7] = WriteDataMark[0];
			WriteDataOffset[8] = WriteDataCRC;
		}

		break;

	case 1:
		if (iOffset < 0)
		{
			WriteDataOffset[5] = 0x14;
			iZeroOffset = 41;

			WriteDataCRC = (iZeroOffset + iOffset) - 1;

			WriteDataOffset[6] = (BYTE)(256 + iOffset);
			WriteDataOffset[7] = WriteDataMark[1];
			WriteDataOffset[8] = WriteDataCRC;
		}
		else
		{
			WriteDataOffset[5] = 0x14;
			iZeroOffset = 41;

			WriteDataCRC = iZeroOffset + iOffset;

			WriteDataOffset[6] = (BYTE)iOffset;
			WriteDataOffset[7] = WriteDataMark[0];
			WriteDataOffset[8] = WriteDataCRC;
		}

		break;

	default:
		break;
	}

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffset) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				//return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	// Serial NOR Flash All Write
// 	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
// 	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };
// 
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, FlachAllWriteData1) == TRUE)
// 	{
// 		if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x05, 4, FlachAllWriteData2) == TRUE)
// 		{
// 			BYTE ReadCheck[256] = { 0x0, };
// 			BYTE ConfirmCheck[5] = { 0xff, 0xff, 0xff, 0xff, 0xff };
// 			if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 			{
// 				int iCheckFailCnt = 0;
// 				for (int i = 0; i < 5; i++) // Check Data 5 Count
// 				{
// 					if (ReadCheck[i] != ConfirmCheck[i])
// 					{
// 						iCheckFailCnt++;
// 					}
// 				}
// 
// 				if (iCheckFailCnt > 0)
// 				{
// 					//return nTestResult = RCA_NG;
// 				}
// 			}
// 			else
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}


	return nTestResult;
}

UINT CWnd_RecipeView::OpticalCenter_Adj_Horizon(__in UINT nCh, __in int iOffset)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffset[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x14 };

	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC = 0x00;

	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	if (iOffset < 0)
	{
		WriteDataCRC = (41 + iOffset) - 1;
		WriteDataOffset[6] = (BYTE)(256 + iOffset);
		WriteDataOffset[7] = WriteDataMark[1];
		WriteDataOffset[8] = WriteDataCRC;
	}
	else
	{
		WriteDataCRC = (41 + iOffset);
		WriteDataOffset[6] = (BYTE)iOffset;
		WriteDataOffset[7] = WriteDataMark[0];
		WriteDataOffset[8] = WriteDataCRC;
	}

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffset) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 5, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}

// 			if (iCheckFailCnt > 0)
// 			{
// 				return nTestResult = RCA_NG;
// 			}
//		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}


	return nTestResult;
}

UINT CWnd_RecipeView::OpticalCenter_Adj_Vertical(__in UINT nCh, __in int iOffset)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffset[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x16 };

	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC = 0x01;

	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	if (iOffset < 0)
	{
		WriteDataCRC = (43 + iOffset) - 1;
		WriteDataOffset[6] = (BYTE)(256 + iOffset);
		WriteDataOffset[7] = WriteDataMark[1];
		WriteDataOffset[8] = WriteDataCRC;
	}
	else
	{
		WriteDataCRC = (43 + iOffset);
		WriteDataOffset[6] = (BYTE)iOffset;
		WriteDataOffset[7] = WriteDataMark[0];
		WriteDataOffset[8] = WriteDataCRC;
	}

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffset) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 5, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}

// 			if (iCheckFailCnt > 0)
// 			{
// 				return nTestResult = RCA_NG;
// 			}
//		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}


	return nTestResult;
}

UINT CWnd_RecipeView::OpticalCenter_Adj_AllWrite(__in UINT nCh)
{
	UINT nTestResult = RCA_OK;

	// Serialize Disable
// 	BYTE Serialize_Disable[1] = { 0x43 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x40 << 1, 1, 0x04, 1, Serialize_Disable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(1000);
// 
// 	// Default Set 0fps
// 	BYTE Default_Set_0fps_1[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x00, 0x13 };
// 	BYTE Default_Set_0fps_2[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x00, 0x14 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, Default_Set_0fps_1) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, Default_Set_0fps_2) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	// Serial NOR Flash All Write
//   	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
//   	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };
//   
//   	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, FlachAllWriteData1) == TRUE)
//   	{
//   		if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x05, 4, FlachAllWriteData2) == TRUE)
//   		{
//   			BYTE ReadCheck[256] = { 0x0, };
//   			BYTE ConfirmCheck[5] = { 0x0, };
//   			if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x09, 256, ReadCheck) == TRUE)
//   			{
//   				int iCheckFailCnt = 0;
//   				for (int i = 0; i < 5; i++) // Check Data 5 Count
//   				{
//   					if (ReadCheck[i] != ConfirmCheck[i])
//   					{
//   						iCheckFailCnt++;
//   					}
//   				}
//   
//   // 				if (iCheckFailCnt > 0)
// //  	{
// //  		return nTestResult = RCA_NG;
// //  	}
//   			}
//   			else
//   			{
//   				return nTestResult = RCA_NG;
//   			}
//   		}
//   	}
//   	else
//   	{
//   		return nTestResult = RCA_NG;
//   	}


	return nTestResult;
}

UINT CWnd_RecipeView::EdgeOnOff(__in BOOL bOnOff)
{
	UINT nTestResult = RCA_OK;

// 	if (bOnOff == TRUE)
// 	{
// 		BYTE EdgeDataOn[8] = { 0x01, 0x06, 0x02, 0x59, 0x00, 0x14, 0x01, 0x80 };
// 
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, EdgeDataOn) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		BYTE EdgeDataOff[8] = { 0x01, 0x06, 0x02, 0x59, 0x00, 0x14, 0x00, 0x7f };
// 
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, EdgeDataOff) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}

	return nTestResult;
}

UINT CWnd_RecipeView::DistortionCorr(__in BOOL bOnOff)
{
	UINT nTestResult = RCA_OK;

// 	if (bOnOff == TRUE)
// 	{
// 		BYTE DistortionDataOff[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x01, 0x2f };
// 		BYTE DistortionDataOn[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x00, 0x2e };
// 
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, DistortionDataOff) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		BYTE DistortionDataOff[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x01, 0x2f };
// 		BYTE DistortionDataOn[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x00, 0x2e };
// 
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, DistortionDataOn) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}


	return nTestResult;
}

UINT CWnd_RecipeView::OperationModeChange(__in UINT nMode)
{
	UINT nTestResult = RCA_OK;

	// Serialize Disable Setting
// 	BYTE SerializeDisable[1] = { 0x43 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeDisable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(200);

	// Fps Mode Output
// 	if (nMode == 0) // 15fps
// 	{
// 		BYTE IspSetting_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x01, 0x14 };
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(100);
// 
// 		BYTE IspSetting_F_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x80, 0x94 };
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_F_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(500);
// 	}
// 	else if (nMode == 1) // 30fps
// 	{
// 		BYTE IspSetting_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x30, 0x43 };
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(100);
// 
// 		BYTE IspSetting_F_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x80, 0x94 };
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_F_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(500);
// 	}

	// Serialize Enable Setting
// 	BYTE SerializeEnable[1] = { 0x83 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeEnable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(100);

	return nTestResult;
}

UINT CWnd_RecipeView::Motion_StageY_CL_Distance(__in double dDistance)
{
	UINT nTestResult = RCA_OK;

//	OnEachTestIOSignal(DO_ChartLight, (enum_IO_SignalType)IO_SignalT_SetOn);

	double dbPuls = (g_dbMotor_AxisLeadMax[AX_StageDistance] - dDistance) * g_dbMotor_AxisResolution[AX_StageDistance];

	nTestResult = OnEachTestMotion(AX_StageDistance, dbPuls);

	return nTestResult;
}


UINT CWnd_RecipeView::Motion_StageY_BlemishZone_In(__in UINT nTestItemID)
{
	UINT nTestResult = RCA_OK;

	switch (nTestItemID)
	{
	case TIID_Brightness:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOn);
		break;

	case TIID_IRFilter:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOff);
		break;

//	case TIID_PatternNoise:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOff);
//		break;

//	case TIID_LEDTest:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOn);
//		break;

	case TIID_ParticleManual:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOn);
		break;

	default:
		break;
	}

	//yhm
	double dbPuls_Step1 = 80000;
	double dbPuls_Step2 = 129000;

	// In 상태라면 그대로 리턴.
	if (m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) == dbPuls_Step2)
	{
// 		if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_In) == TRUE)
// 		{
			return nTestResult;
//		}
	}

	// 현재 위치 확인 - pulse 값 : 스테이지가 다크유닛을 지나쳐 있거나 근처에 있다면
	if (m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) >= dbPuls_Step1)
	{
		// 다크유닛 빠지고,
		BOOL bStatus = FALSE;

// 		OnEachTestIOSignal(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOff);

//		nTestResult = OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOn);
		for (int i = 0; i < 100; i++)
		{
// 			if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_Out) == TRUE)
// 			{
				bStatus = TRUE;
				break;
//			}
			DoEvents(33);
		}

		if (bStatus == TRUE)
		{
			nTestResult = RCA_OK;
		}
		else
		{
			nTestResult = RCA_MachineCheck;
		}

	}

	// 1 Step
	if (nTestResult == RCA_OK)
	{
		nTestResult = OnEachTestMotion(AX_StageDistance, dbPuls_Step1);
	}

	// 다크유닛 들어옴.
	if (nTestResult == RCA_OK)
	{
		BOOL bStatus = FALSE;

// 		OnEachTestIOSignal(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOff);

// 		nTestResult = OnEachTestIOSignal(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		for (int i = 0; i < 100; i++)
// 		{
// 			if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_In) == TRUE)
// 			{
// 				bStatus = TRUE;
// 				break;
//			}

// 			DoEvents(33);
// 		}

		if (bStatus == TRUE)
		{
			nTestResult = RCA_OK;
		}
		else
		{
			nTestResult = RCA_MachineCheck;
		}
	}

	// 2 Step
	if (nTestResult == RCA_OK)
	{
		nTestResult = OnEachTestMotion(AX_StageDistance, dbPuls_Step2);
	}

	return nTestResult;
}

UINT CWnd_RecipeView::Motion_StageY_BlemishZone_Out()
{
	UINT nTestResult = RCA_OK;

// 	OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOff);
// 	OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);

	double dbPuls_Step1 = 80000;
	double dbPuls_Step2 = 0;

	// Out 상태라면 그대로 리턴.
// 	if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_Out) == TRUE)
// 	{
// 		return nTestResult;
// 	}

	// 현재 위치 확인 - pulse 값 : 스테이지가 다크유닛을 지나쳐 있거나 근처에 있다면
	if (m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) >= dbPuls_Step1)
	{
		// 1 Step
		if (nTestResult == RCA_OK)
		{
			nTestResult = OnEachTestMotion(AX_StageDistance, dbPuls_Step1);
		}

		// 다크유닛 빠지고,
		BOOL bStatus = FALSE;

// 		OnEachTestIOSignal(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOff);

// 		nTestResult = OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		for (int i = 0; i < 100; i++)
// 		{
// 			if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_Out) == TRUE)
// 			{
// 				bStatus = TRUE;
// 				break;
// 			}
// 			DoEvents(33);
// 		}

		if (bStatus == TRUE)
		{
			nTestResult = RCA_OK;
		}
		else
		{
			nTestResult = RCA_MachineCheck;
		}

	}

	return nTestResult;
}

UINT CWnd_RecipeView::Motion_StageY_Initial()
{
	UINT nTestResult = RCA_OK;
	nTestResult = OnEachTestMotion(AX_StageDistance, 0);

	return nTestResult;
}


UINT CWnd_RecipeView::SetLuriCamBrd_Volt(__in float* fVolt, __in UINT nPort /*= 0*/)
{
	UINT lReturn = RCA_OK;

	lReturn = m_pDevice->PCBCamBrd[nPort].Send_Volt(fVolt);

	return lReturn;
}

BOOL CWnd_RecipeView::CameraOnOff(__in UINT nCh, __in UINT nGrabType, __in UINT nSWVer, __in UINT nFpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff, __in BOOL bOnOff)
{
	BOOL bResult = FALSE;

	switch (m_stModelInfo.nGrabType)
	{
	case GrabType_NTSC:

		bResult = TRUE;

		break;

// 	case GrabType_LVDS:
// 
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.dwClock = 1000000;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nDataMode = DAQ_Data_16bit_Mode;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nHsyncPolarity = DAQ_HsyncPol_Inverse;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nPClockPolarity = DAQ_PclkPol_RisingEdge;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nDvalUse = DAQ_DeUse_HSync_Use;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nVideoMode = DAQ_Video_Signal_Mode;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nMIPILane = DAQMIPILane_1;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nClockSelect = DAQClockSelect_FixedClock;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nClockUse = DAQClockUse_Off;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.dWidthMultiple = 1.0;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.dHeightMultiple = 1.0;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nConvFormat = Conv_CbYCrY_GRBG;
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.nSensorType = enImgSensorType_YCBCR;
// 
// 		if (m_stModelInfo.bI2CFile_1 == TRUE)
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.szIICPath_1 = m_strI2CPath + m_stModelInfo.szI2CFile_1 + _T(".") + I2C_FILE_EXT;
// 		else
// 			m_stModelInfo.stLVDSInfo.stLVDSOption.szIICPath_1.Empty();
// 
// 		if (m_stModelInfo.bI2CFile_2 == TRUE)
// 		m_stModelInfo.stLVDSInfo.stLVDSOption.szIICPath_2 = m_strI2CPath + m_stModelInfo.szI2CFile_2 + _T(".") + I2C_FILE_EXT;
// 		else
// 			m_stModelInfo.stLVDSInfo.stLVDSOption.szIICPath_2.Empty();
// 
// 		m_pDevice->DAQCtrl.SetDAQOption(nCh, m_stModelInfo.stLVDSInfo.stLVDSOption);
// 
// 		if (bOnOff == ON)
// 		{
// 			CameraRegToFunctionChange(nSWVer, nFpsMode, nDistortion, nEdgeOnOff);
// 			m_pDevice->DAQCtrl.Capture_Start(nCh);
// 		}
// 		else if (bOnOff == OFF)
// 		{
// 			m_pDevice->DAQCtrl.Capture_Stop(nCh);
// 		}
// 
// 		Sleep(m_stModelInfo.nCameraDelay);
// 
// 		bResult = TRUE;
// 
// 		break;

	default:
		break;
	}

	return bResult;
}

UINT CWnd_RecipeView::CameraRegToFunctionChange(__in UINT nSWVer, __in UINT nfpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff)
{
	UINT nTestResult = RCA_OK;

	// Serialize Disable Setting
// 	BYTE SerializeDisable[1] = { 0x43 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeDisable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(100);
// 
// 	// ISP Reset
// 	BYTE ISP_LOW[1] = { 0x0f };
// 	BYTE ISP_HIGH[1] = { 0x8f };
// 	m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x0d, 1, ISP_LOW);
// 	Sleep(100);
// 	m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x0d, 1, ISP_HIGH);
// 	Sleep(1000);

	// Distortion
	switch (nDistortion)
	{
	case 0:
		DistortionCorr(FALSE);
		break;
	case 1:
		DistortionCorr(TRUE);
		break;
	default:
		break;
	}

	// Edge
	switch (nEdgeOnOff)
	{
	case 0:
		EdgeOnOff(FALSE);
		break;
	case 1:
		EdgeOnOff(TRUE);
		break;
	default:
		break;
	}

	// Fps Setting
	switch (nfpsMode)
	{
	case 0: // 15
		OperationModeChange(0);
		break;
	case 1: // 30
		OperationModeChange(1);
		break;
	default:
		break;
	}

	Sleep(1000);

	// Serialize Enable Setting
	BYTE SerializeEnable[1] = { 0x83 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeEnable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	Sleep(100);

	return nTestResult;
}

void CWnd_RecipeView::OnPopImageLoad()
{
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.bmp)| *.bmp|"));
	CString strFileSel;
	strFileSel.Format(_T("*.bmp"));

	// 파일 불러오기
	CFileDialog fileDlg(TRUE, strFileSel, strFileSel, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFileExt);


	if (IDOK == fileDlg.DoModal())
	{
		Stop_ImageView_Mon();
		Sleep(100);
		CString strFilePath = fileDlg.GetPathName();

		m_pstImageMode->szImagePath = strFilePath;
		OnImage_LoadDisplay(strFilePath);
		ChangeImageMode(ImageMode_StillShotImage);
		Start_ImageView_Mon();
	}
}


void CWnd_RecipeView::OnImage_LoadDisplay(CString strPath)
{

	if (m_LoadImage != NULL)
	{
		cvReleaseImage(&m_LoadImage);
		m_LoadImage = NULL;
	}

	if (strPath.IsEmpty())
	{
		return;
	}

	IplImage *testImage = cvLoadImage((CStringA)strPath);

	if (testImage == NULL || testImage->width < 1 || testImage->height < 1)
	{
		return;
	}
	m_LoadImage = cvCreateImage(cvSize(testImage->width, testImage->height), IPL_DEPTH_8U, 3);




	cvCopyImage(testImage, m_LoadImage);

	cvReleaseImage(&testImage);
}

BOOL CWnd_RecipeView::Start_ImageView_Mon()
{
	if (NULL != m_hThr_ImageViewMon)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_ImageViewMon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			//AfxMessageBox(_T("Image 모니터링 쓰레드가 동작 중 입니다."), MB_SYSTEMMODAL);
			return FALSE;
		}
	}

	if (NULL != m_hThr_ImageViewMon)
	{
		CloseHandle(m_hThr_ImageViewMon);
		m_hThr_ImageViewMon = NULL;
	}
	m_bFlag_ImageViewMon = TRUE;
	m_hThr_ImageViewMon = HANDLE(_beginthreadex(NULL, 0, Thread_ImageViewMon, this, 0, NULL));

	return TRUE;
}
BOOL CWnd_RecipeView::Stop_ImageView_Mon()
{
	m_bFlag_ImageViewMon = FALSE;

	if (NULL != m_hThr_ImageViewMon)
	{
		WaitForSingleObject(m_hThr_ImageViewMon, m_dwImageViewMonCycle);

		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_ImageViewMon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThr_ImageViewMon, dwExitCode);
			WaitForSingleObject(m_hThr_ImageViewMon, WAIT_ABANDONED);
			CloseHandle(m_hThr_ImageViewMon);
			m_hThr_ImageViewMon = NULL;
		}

		//NoSignal_Ch(Para_Left);
		//m_bGrabImageStatus = FALSE;

	}

	return TRUE;
}
UINT WINAPI CWnd_RecipeView::Thread_ImageViewMon(__in LPVOID lParam)
{
	ASSERT(NULL != lParam);

	CWnd_RecipeView* pThis = (CWnd_RecipeView*)lParam;

	DWORD dwEvent = 0;

	__try
	{
		while (pThis->m_bFlag_ImageViewMon)
		{
			//    // 종료 이벤트, 연결해제 이벤트 체크
			if (NULL != pThis->m_hExternalExitEvent)
			{
				dwEvent = WaitForSingleObject(pThis->m_hExternalExitEvent, pThis->m_dwImageViewMonCycle);

				switch (dwEvent)
				{
				case WAIT_OBJECT_0: // Exit Program
					TRACE(_T(" -- 프로그램 종료 m_hExternalExitEvent 이벤트 감지 \n"));
					pThis->m_bFlag_ImageViewMon = FALSE;
					break;

				case WAIT_TIMEOUT:
					pThis->OnLoadImageDisplay();

					break;
				}
			}
			else
			{
				pThis->OnLoadImageDisplay();

				DoEvents(pThis->m_dwImageViewMonCycle);
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_RecipeView::Thread_ImageViewMon()\n"));
	}

	TRACE(_T("쓰레드 종료 : CWnd_RecipeView::Thread_ImageViewMon Loop Exit\n"));
	return TRUE;
}

void CWnd_RecipeView::OnLoadImageDisplay(){

// 	if (m_bGrabImageStatus == TRUE)
// 	{
// 		return;
// 	}
// 	m_bGrabImageStatus = TRUE;
// 	if (m_LoadImage != NULL)
// 	{
// 		IplImage *TestImage = cvCreateImage(cvSize(m_LoadImage->width, m_LoadImage->height), IPL_DEPTH_8U, 3);
// 
// 		cvCopy(m_LoadImage, TestImage);
// 		ShowVideo_Overlay(0, TestImage, TestImage->width, TestImage->height, m_stRecipeInfo.nGideLineUseState);
// 
// 		if (*m_pbFlag_PicSave)
// 		{
// 
// 			if (TRUE == m_pstImageMode->bImageSaveMode)
// 			{
// 				CString szFile;
// 
// 				SYSTEMTIME tmLocal;
// 				GetLocalTime(&tmLocal);
// 
// 				szFile.Format(_T("%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
// 
// 				CString szPath = m_pstImageMode->szImageCommonPath;
// 
// 				// 날짜별로 생성 폴더 생성
// 				szPath.Format(_T("%s/%02d%02d%02d"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
// 				MakeDirectory(szPath);
// 
// 				CString strFile;
// 				strFile.Format(_T("%s/%s_%s.png"), szPath, szFile, g_szTestItem[m_nTestItem]);
// 
// 				cvSaveImage(CT2A(strFile), TestImage);
// 			}
// 			*m_pbFlag_PicSave = FALSE;
// 		}
// 
// 		cvReleaseImage(&TestImage);
// 
// 	}
// 	m_bGrabImageStatus = FALSE;
}
void CWnd_RecipeView::ChangeImageMode(enImageMode eImageMode)
{
	if (eImageMode == ImageMode_LiveCam)
	{
	
		Stop_ImageView_Mon();
		m_pstImageMode->szImagePath.Empty();
		m_wnd_ManualCtrl.ImageLoadMode(FALSE);
	}
	else{
		//m_rb_ImageMode[ImageMode_LiveCam].SetCheck(BST_UNCHECKED);
		//m_rb_ImageMode[ImageMode_StillShotImage].SetCheck(BST_CHECKED);
		m_wnd_ManualCtrl.ImageLoadMode(TRUE);
	}
	m_pstImageMode->eImageMode = eImageMode;
	//dSet_CameraSelect(Para_Left);
	SetLiveVideo((BOOL)1-eImageMode);
}

void CWnd_RecipeView::SetLiveVideo(__in BOOL bLive)
{
	BOOL bRet = TRUE;
	m_bLive = bLive;

	if (m_bLive)
	{
		m_wnd_VideoView.ShowWindow(SW_SHOW);
		m_wnd_ImageView.ShowWindow(SW_HIDE);
	}
	else
	{
		m_wnd_VideoView.ShowWindow(SW_HIDE);
		m_wnd_ImageView.ShowWindow(SW_SHOW);

		// 이미지 로드
		bRet = m_wnd_ImageView.OnUpdataLoadImage();
	}

	if (bRet == FALSE)
		m_wnd_ManualCtrl.ImageLoadMode(FALSE);

}

//=============================================================================
// Method		: SetStatusEngineerMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2017/7/7 - 11:03
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetStatusEngineerMode(__in enPermissionMode InspMode)
{
	// DAQ 옵션 
	if (InspMode == Permission_Engineer)
	{
		m_tc_Option.ShowTab(m_iTabDaqIdx_Eng, SW_SHOW, TRUE, TRUE);
	}

	else
	{
		m_tc_Option.ShowTab(m_iTabDaqIdx_Eng, SW_SHOW, TRUE, TRUE);
	}

	m_wnd_TestItemCfg.SetStatusEngineerMode(InspMode);
}



void CWnd_RecipeView::ReportImageSave(__in enLT_TestItem_ID nTestItem, __in UINT nTestCount, __in SYSTEMTIME* pTime, __in UINT nEachResult)
{
	CString szTestitemFolder;

// 	IplImage* pBufImage = NULL;
// 	pBufImage = m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.GetFrameImageBuffer();
// 
// 	if (pBufImage == NULL)
// 		return;
// 
// 	IplImage* pPicImage = cvCreateImage(cvSize(pBufImage->width, pBufImage->height), IPL_DEPTH_8U, 3);
// 	cvCopy(pBufImage, pPicImage);
// 
// 	BOOL bSaveFlag = FALSE;
// 
// 
// 	// ...\Default_A\2016-11-17\ 
// 	CString szDatePath;
// 	szDatePath.Format(_T("%s%04d-%02d-%02d\\%s"),//%s\\%s\\"), //!SH _190210: lot이랑 not lot 구분
// 		m_stInspInfo.Path.szImage, pTime->wYear, pTime->wMonth, pTime->wDay,
// 		m_stInspInfo.szModelName);//, szTotalResult, pstWorklist->Time);
// 	CString szPath = szDatePath;
// 
// 	if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
// 	{
// 		szPath += _T("\\LotMode");
// 
// 		if (!m_stInspInfo.LotInfo.szLotName.IsEmpty())
// 			szPath += _T("\\") + m_stInspInfo.LotInfo.szLotName;
// 	}
// 	else
// 	{
// 		szPath += _T("\\Not_LotMode");
// 	}
// 
// 	MakeDirectory(szPath);
// 	CString szBarcode = m_stInspInfo.szBarcodeBuf;
// 	if (m_stInspInfo.szBarcodeBuf.IsEmpty() || m_stInspInfo.szBarcodeBuf == _T("No Barcode"))
// 	{
// 		szBarcode = _T("_NoBarcode");
// 	}
// 	else{
// 		szBarcode = m_stInspInfo.CamInfo.szBarcode;
// 	}
// 
// 
// 
// 
// 
// 	szTestitemFolder.Format(_T("%04d%02d%02d_%02d%02d%02d_%s_"), pTime->wYear, pTime->wMonth, pTime->wDay, pTime->wHour, pTime->wMinute, pTime->wSecond, szBarcode);
// 
// 
// 
// 
// 	switch (nTestItem)
// 	{
// 	case TIID_TestInitialize:
// 	case TIID_TestFinalize:
// 	case TIID_Current:
// 		//szTestitemFolder.Format(_T("%s"), g_szLT_TestItem_Name[nTestItem]);
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		bSaveFlag = FALSE;
// 		break;
// 
// 	case TIID_CenterPoint:
// 		//szTestitemFolder.Format(_T("%s"), g_szLT_TestItem_Name[nTestItem]);
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicCenterPoint(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	case TIID_Rotation:
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicRotate(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	case TIID_EIAJ:
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicEIAJ(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	case TIID_FOV:
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicAngle(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	case TIID_Color:
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicColor(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	case TIID_Reverse:
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicReverse(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	case TIID_BlackSpot:
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicParticle(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	case TIID_ParticleManual:
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicParticle(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	case TIID_Brightness:
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicBrightness(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	case TIID_IRFilter:
// 		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicIRFilter(pPicImage);
// 		bSaveFlag = TRUE;
// 		break;
// 
// 	default:
// 		break;
// 	}
// 
// 	//원본은 m_TestImage 이거 저장
// 
// 	if (bSaveFlag == TRUE)
// 	{
// 		CString strFile;
// 		strFile.Format(_T("%s\\%s_Pic.bmp"), szPath, szTestitemFolder);
// 		cvSaveImage(CT2A(strFile), pPicImage);
// 
// 		strFile.Format(_T("%s\\%s_Origin.bmp"), szPath, szTestitemFolder);
// 		cvSaveImage(CT2A(strFile), m_TestImage);
// 
// 		bSaveFlag = FALSE;
// 	}

	
//	cvReleaseImage(&pPicImage);
}


