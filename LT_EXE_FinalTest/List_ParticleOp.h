﻿#ifndef List_ParticleOp_h__
#define List_ParticleOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_PtOp
{
	PtOp_Object,
//	PtOp_PosX,
//	PtOp_PosY,
//	PtOp_Width,
//	PtOp_Height,
	PtOp_BruiseConc,
	PtOp_BruiseSize,
	PtOp_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_PtOp[] =
{
	_T(""),
	//_T("PosX"),
	//_T("PosY"),
	//_T("Width"),
	//_T("Height"),
	_T("Conc"),
	_T("Size"),
	NULL
};

typedef enum enListItemNum_PtOp
{
	PtOp_Edge,
	PtOp_Vertical,
	PtOp_Horizon,
	PtOp_Center,
	PtOp_ItemNum,
};

static const TCHAR*	g_lpszItem_PtOp[] =
{
	_T("Edge"),
	_T("Vertical"),
	_T("Horizon"),
	_T("Center"),
	NULL
};

const int	iListAglin_PtOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
//	LVCFMT_CENTER,
//	LVCFMT_CENTER,
//	LVCFMT_CENTER,
//	LVCFMT_CENTER,
};

const int	iHeaderWidth_PtOp[] =
{
	70,
	70,
	70,
//	70,
//	70,
//	70,
//	70,
};
// CList_ParticleOp

class CList_ParticleOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ParticleOp)

public:
	CList_ParticleOp();
	virtual ~CList_ParticleOp();
	
	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Particle(ST_LT_TI_BlackSpot* pstParticle)
	{
		if (pstParticle == NULL)
			return;

		m_pstParticle = pstParticle;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth  = nWidth;
		m_nHeight = nHeight;
	};


protected:

	ST_LT_TI_BlackSpot*  m_pstParticle;

	DECLARE_MESSAGE_MAP()

	CFont	m_Font;
	CEdit	m_ed_CellEdit;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData		(UINT nRow, UINT nCol, double dbValue);

public:

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusEdit();
};

#endif // List_Particle_h__
