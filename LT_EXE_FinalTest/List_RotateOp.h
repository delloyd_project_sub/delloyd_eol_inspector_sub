﻿#ifndef List_RotateOp_h__
#define List_RotateOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_RotateOp
{
	RoOp_Object = 0,
	RoOp_PosX,
	RoOp_PosY,
	RoOp_Width,
	RoOp_Height,
	RoOp_Mark,
	RoOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_RotateOp[] =
{
	_T(""),
	_T("CenterX"),
	_T("CenterY"),
	_T("Width"),
	_T("Height"),
	_T("Mark"),
	NULL 
};

typedef enum enListItemNum_RotateOp
{
	RoOp_ItemNum = ROI_RT_Max,
};

static LPCTSTR	g_lpszItem_RotateOp[] =
{
	NULL
};

const int	iListAglin_RotateOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER, 
};

const int	iHeaderWidth_RotateOp[] =
{
	80,
	80,
	80,
	80,
	80,
	80,
};
// List_RotateInfo

class CList_RotateOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_RotateOp)

public:
	CList_RotateOp();
	virtual ~CList_RotateOp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Rotate(ST_LT_TI_Rotate* pstRotate)
	{
		if (pstRotate == NULL)
			return;

		m_pstRotate = pstRotate;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth  = nWidth;
		m_nHeight = nHeight;
	};

protected:

	ST_LT_TI_Rotate*  m_pstRotate;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);
	
	afx_msg void	OnEnKillFocusEdit		();
	afx_msg void	OnEnKillFocusCombo		();
	afx_msg void	OnEnSelectFocusCombo	();
};

#endif // List_RotateInfo_h__
