﻿#ifndef Wnd_BaseColorOp_h__
#define Wnd_BaseColorOp_h__

#pragma once

#include "Wnd_ColorOp.h"

// CWnd_BaseColorOp

class CWnd_BaseColorOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseColorOp)

public:
	CWnd_BaseColorOp();
	virtual ~CWnd_BaseColorOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_ColorOp m_wndColorOp[TICnt_Color];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseColorOp_h__
