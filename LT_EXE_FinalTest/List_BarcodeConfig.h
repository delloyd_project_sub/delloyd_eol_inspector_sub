//*****************************************************************************
// Filename	: 	List_BarcodeConfig.h
// Created	:	2017/9/24 - 16:30
// Modified	:	2017/9/24 - 16:30
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef List_BarcodeConfig_h__
#define List_BarcodeConfig_h__

#pragma once

#include "Def_Enum.h"
#include "Def_TestItem.h"
#include "Def_Motion.h"

//-----------------------------------------------------------------------------
// CList_BarcodeConfig
//-----------------------------------------------------------------------------
class CList_BarcodeConfig : public CListCtrl
{
	DECLARE_DYNAMIC(CList_BarcodeConfig)

public:
	CList_BarcodeConfig();
	virtual ~CList_BarcodeConfig();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick			(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	CFont			m_Font;

	virtual void	InitHeader			();
	void			AdjustColWidth		();

	BOOL				m_bIntiHeader		= FALSE;
	const int*			m_pHeadWidth		= NULL;

	// 스텝 정보 구조체
	ST_BarcodeRefInfo	m_stBarcodeRefInfo;

	// 넘버링 다시 설정
	void		ResetOrderingNumbers	();

	// 아이템 선택
	void		SetSelectItem			(__in int nItem);

	// 원하는 위치에 아이템 삽입
	void		InsertBarcodeItem		(__in int nItem, __in const ST_BarcodeRef* pBarcodeRef);
	// 아이템 추가
	void		AddBarcodeItem			(__in const ST_BarcodeRef* pBarcodeRef);

public:

	// 새로운 스텝 정보를 리스트에 넣음
	void		Set_BarcodeRefInfo		(__in const ST_BarcodeRefInfo* pstBarcodeRefInfo);	
	void		Get_BarcodeRefInfo		(__out ST_BarcodeRefInfo& stBarcodeRefInfo);

	// 항목 추가
	void		Item_Add				(__in ST_BarcodeRef& stBarcodeRef);
	// 항목 선택된 위치에 삽입
	void		Item_Insert				(__in ST_BarcodeRef& stBarcodeRef);
	// 항목 삭제
	void		Item_Remove				();
	// 항목 위로 이동
	void		Item_Up					();
	// 항목 아래로 이동
	void		Item_Down				();
};

#endif // List_BarcodeConfig_h__


