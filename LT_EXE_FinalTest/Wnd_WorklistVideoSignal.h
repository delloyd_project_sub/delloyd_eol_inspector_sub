﻿//*****************************************************************************
// Filename	: Wnd_WorklistVideoSignal.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistVideoSignal_h__
#define Wnd_WorklistVideoSignal_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_VideoSignal.h"

//=============================================================================
// Wnd_WorklistVideoSignal
//=============================================================================
class CWnd_WorklistVideoSignal : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistVideoSignal)

public:
	CWnd_WorklistVideoSignal();
	virtual ~CWnd_WorklistVideoSignal();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

//	CList_Work_VideoSignal				m_list_VideoSignal[TICnt_VideoSignal];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistVideoSignal_h__


