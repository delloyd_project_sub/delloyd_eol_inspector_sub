﻿// List_CenterPointOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_CenterPointOp.h"

#define IRtOp_ED_CELLEDIT			5001
#define IRtOp_CB_CELLCOMBO_TYPE		5002

// CList_CenterPointOp

IMPLEMENT_DYNAMIC(CList_CenterPointOp, CListCtrl)

CList_CenterPointOp::CList_CenterPointOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	
	m_nEditCol = 0;
	m_nEditRow = 0;

	m_nWidth  = 640;
	m_nHeight = 480;

	m_pstCenterPoint = NULL;
}

CList_CenterPointOp::~CList_CenterPointOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_CenterPointOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_CenterPointOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_CenterPointOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_CenterPointOp::OnEnKillFocusEdit)
	ON_CBN_KILLFOCUS(IRtOp_CB_CELLCOMBO_TYPE, &CList_CenterPointOp::OnEnKillFocusCombo)
	ON_CBN_SELCHANGE(IRtOp_CB_CELLCOMBO_TYPE, &CList_CenterPointOp::OnEnSelectFocusCombo)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_CenterPointOp 메시지 처리기입니다.
int CList_CenterPointOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IRtOp_CB_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdex = 0; NULL != g_szCenterPointMarkColor[nIdex]; nIdex++)
	{
		m_cb_Type.InsertString(nIdex, g_szCenterPointMarkColor[nIdex]);
	}
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[CpOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = CpOp_PosX; nCol < CpOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_CenterPointOp[CpOp_Object]) / (CpOp_MaxCol - CpOp_PosX);
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_CenterPointOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_CenterPointOp::InitHeader()
{
	for (int nCol = 0; nCol < CpOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_CenterPointOp[nCol], iListAglin_CenterPointOp[nCol], iHeaderWidth_CenterPointOp[nCol]);
	}

	for (int nCol = 0; nCol < CpOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_CenterPointOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_CenterPointOp::InsertFullData()
{
	if (NULL == m_pstCenterPoint)
		return;

 	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < CpOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_CenterPointOp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstCenterPoint)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_lpszItem_CenterPointOp[nRow]);
	SetItemText(nRow, CpOp_Object, strValue);

	strValue.Format(_T("%d"), m_pstCenterPoint->stCenterPointOpt.rtRoi.CenterPoint().x);
	SetItemText(nRow, CpOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstCenterPoint->stCenterPointOpt.rtRoi.CenterPoint().y);
	SetItemText(nRow, CpOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstCenterPoint->stCenterPointOpt.rtRoi.Width());
	SetItemText(nRow, CpOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstCenterPoint->stCenterPointOpt.rtRoi.Height());
	SetItemText(nRow, CpOp_Height, strValue);

	strValue.Format(_T("%s"), g_szCenterPointMarkColor[m_pstCenterPoint->stCenterPointOpt.nMarkColor]);
	SetItemText(nRow, CpOp_Mark, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (NULL == m_pstCenterPoint)
	{
		*pResult = 1;
		return;
	}

	m_pstCenterPoint->stCenterPointOpt.iSelectROI = pNMItemActivate->iItem;
	m_pstCenterPoint->stCenterPointResult.iSelectROI = -1;

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < CpOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == CpOp_Mark)
			{
				m_cb_Type.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Type.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Type.SetFocus();
				m_cb_Type.SetCurSel(m_pstCenterPoint->stCenterPointOpt.nMarkColor);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_CenterPointOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (m_pstCenterPoint == NULL)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstCenterPoint->stCenterPointOpt.rtRoi;

	switch (nCol)
	{
	case CpOp_PosX:
		m_pstCenterPoint->stCenterPointOpt.RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case CpOp_PosY:
		m_pstCenterPoint->stCenterPointOpt.RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case CpOp_Width:
		m_pstCenterPoint->stCenterPointOpt.RectPosWH(iValue, rtTemp.Height());
		break;
	case CpOp_Height:
		m_pstCenterPoint->stCenterPointOpt.RectPosWH(rtTemp.Width(), iValue);
		break;
	default:
		break;
	}

	if (m_pstCenterPoint->stCenterPointOpt.rtRoi.left < 0)
	{
		CRect rtTemp = m_pstCenterPoint->stCenterPointOpt.rtRoi;

		m_pstCenterPoint->stCenterPointOpt.rtRoi.left = 0;
		m_pstCenterPoint->stCenterPointOpt.rtRoi.right = m_pstCenterPoint->stCenterPointOpt.rtRoi.left + rtTemp.Width();
	}

	if (m_pstCenterPoint->stCenterPointOpt.rtRoi.right > m_nWidth)
	{
		CRect rtTemp = m_pstCenterPoint->stCenterPointOpt.rtRoi;

		m_pstCenterPoint->stCenterPointOpt.rtRoi.right = m_nWidth;
		m_pstCenterPoint->stCenterPointOpt.rtRoi.left = m_pstCenterPoint->stCenterPointOpt.rtRoi.right - rtTemp.Width();
	}

	if (m_pstCenterPoint->stCenterPointOpt.rtRoi.top < 0)
	{
		CRect rtTemp = m_pstCenterPoint->stCenterPointOpt.rtRoi;

		m_pstCenterPoint->stCenterPointOpt.rtRoi.top = 0;
		m_pstCenterPoint->stCenterPointOpt.rtRoi.bottom = rtTemp.Height() + m_pstCenterPoint->stCenterPointOpt.rtRoi.top;
	}

	if (m_pstCenterPoint->stCenterPointOpt.rtRoi.bottom > m_nHeight)
	{
		CRect rtTemp = m_pstCenterPoint->stCenterPointOpt.rtRoi;

		m_pstCenterPoint->stCenterPointOpt.rtRoi.bottom = m_nHeight;
		m_pstCenterPoint->stCenterPointOpt.rtRoi.top = m_pstCenterPoint->stCenterPointOpt.rtRoi.bottom - rtTemp.Height();
	}

	if (m_pstCenterPoint->stCenterPointOpt.rtRoi.Height() <= 0)
		m_pstCenterPoint->stCenterPointOpt.RectPosWH(m_pstCenterPoint->stCenterPointOpt.rtRoi.Width(), 1);

	if (m_pstCenterPoint->stCenterPointOpt.rtRoi.Height() >= m_nHeight)
		m_pstCenterPoint->stCenterPointOpt.RectPosWH(m_pstCenterPoint->stCenterPointOpt.rtRoi.Width(), m_nHeight);

	if (m_pstCenterPoint->stCenterPointOpt.rtRoi.Width() <= 0)
		m_pstCenterPoint->stCenterPointOpt.RectPosWH(1, m_pstCenterPoint->stCenterPointOpt.rtRoi.Height());

	if (m_pstCenterPoint->stCenterPointOpt.rtRoi.Width() >= m_nWidth)
		m_pstCenterPoint->stCenterPointOpt.RectPosWH(m_nWidth, m_pstCenterPoint->stCenterPointOpt.rtRoi.Height());

	CString strValue;

	switch (nCol)
	{
	case CpOp_PosX:
		strValue.Format(_T("%d"), m_pstCenterPoint->stCenterPointOpt.rtRoi.CenterPoint().x);
		break;
	case CpOp_PosY:
		strValue.Format(_T("%d"), m_pstCenterPoint->stCenterPointOpt.rtRoi.CenterPoint().y);
		break;
	case CpOp_Width:
		strValue.Format(_T("%d"), m_pstCenterPoint->stCenterPointOpt.rtRoi.Width());
		break;
	case CpOp_Height:
		strValue.Format(_T("%d"), m_pstCenterPoint->stCenterPointOpt.rtRoi.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_CenterPointOp::UpdateCelldbData(UINT nRow, UINT nCol, double dValue)
{
	CString str;
	str.Format(_T("%.2f"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_CenterPointOp::GetCellData()
{
	return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_CenterPointOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue  = _ttof(strText);

		iValue = iValue + ((zDelta / 120));
		dbValue = dbValue + ((zDelta / 120)*0.1);

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (dbValue > 2.0)
			dbValue = 2.0;
		
		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusCombo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/3 - 0:12
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnEnKillFocusCombo()
{
	SetItemText(m_nEditRow, CpOp_Mark, g_szRotateMarkColor[m_pstCenterPoint->stCenterPointOpt.nMarkColor]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectFocusCombo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/2 - 20:36
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnEnSelectFocusCombo()
{
	m_pstCenterPoint->stCenterPointOpt.nMarkColor = m_cb_Type.GetCurSel();
}
