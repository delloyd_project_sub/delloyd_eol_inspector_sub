﻿#ifndef Wnd_BaseDefectPixelOp_h__
#define Wnd_BaseDefectPixelOp_h__

#pragma once

#include "Wnd_DefectPixelOp.h"

// CWnd_BaseDefectPixelOp

class CWnd_BaseDefectPixelOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseDefectPixelOp)

public:
	CWnd_BaseDefectPixelOp();
	virtual ~CWnd_BaseDefectPixelOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_DefectPixelOp m_wndDefectPixelOp[TICnt_DefectPixel];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseDefectPixelOp_h__
