﻿// Wnd_CenterPointOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_CenterPointOp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_CenterPointOp
typedef enum CenterPointOptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_CenterPointOp, CWnd)

CWnd_CenterPointOp::CWnd_CenterPointOp()
{
	m_pstModelInfo	= NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_CenterPointOp::~CWnd_CenterPointOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_CenterPointOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_CenterPointOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_CenterPointOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_CTP_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szCenterPointStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < BTN_CTP_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szCenterPointButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_CTP_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	/*for (UINT nIdex = 0; nIdex < CMB_CTP_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}*/

	//for (UINT nIdx = 0; NULL != g_szCenterPointMode[nIdx]; nIdx++)
	//{
	//	m_cb_Item[CMB_CTP_TESTMODE].AddString(g_szCenterPointMode[nIdx]);
	//}
	//m_cb_Item[CMB_CTP_TESTMODE].SetCurSel(0);

	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt  = CpOp_ItemNum;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 43;
	int iList_H  = iHeaderH + iIdxCnt * 12;
	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = iWidth / 4;
	int iSTHeight = 25;

	for (UINT nIdx = 0; nIdx < STI_CTP_MAX; nIdx++)
	{
		iLeft = iMargin;
		m_st_Item[STI_CTP_REFAXIS_X + nIdx].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
		
		iTop += iCtrl_H + iSpacing;
	}

	iTop = iMargin;
	for (UINT nIdx = 0; nIdx < EDT_CTP_MAX; nIdx++)
	{
		iLeft = iSTWidth + iMargin + 1;
		m_ed_Item[EDT_CTP_REFAXIS_X + nIdx].MoveWindow(iLeft, iTop, iSTWidth * 2 - 10, iSTHeight);

		iTop += iCtrl_H + iSpacing;
	}

// 	iLeft = iMargin;
// 	iLeft += iSTWidth + 1;
// 	m_cb_Item[CMB_CTP_TESTMODE].MoveWindow(iLeft, iTop, iSTWidth * 2 - 10, 150);
	
	iLeft = cx - iMargin - iSTWidth;
	m_bn_Item[BTN_CTP_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	
	iLeft = iMargin;
	iTop += iCtrl_H + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_CenterPointOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (NULL == m_pstModelInfo)
		return;

	if (TRUE == bShow)
	{
		m_pstModelInfo->nTestMode = TIID_CenterPoint;
		m_pstModelInfo->nTestCnt = m_nTestItemCnt;
		m_pstModelInfo->nPicItem = PIC_CenterPoint;
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_CTP_TEST:
		GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_CenterPoint);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::SetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	CString strValue;

	m_List.SetImageSize(m_pstModelInfo->dwWidth, m_pstModelInfo->dwHeight);
	m_List.SetPtr_CenterPoint(&m_pstModelInfo->stCenterPoint[m_nTestItemCnt]);
	m_List.InsertFullData();

	strValue.Format(_T("%d"), m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.nRefAxisX);
	m_ed_Item[EDT_CTP_REFAXIS_X].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.nRefAxisY);
	m_ed_Item[EDT_CTP_REFAXIS_Y].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.iDevOffsetX);
	m_ed_Item[EDT_CTP_DEVOFFSET_X].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.iDevOffsetY);
	m_ed_Item[EDT_CTP_DEVOFFSET_Y].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.iOffsetX);
	m_ed_Item[EDT_CTP_OFFSET_X].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.iOffsetY);
	m_ed_Item[EDT_CTP_OFFSET_Y].SetWindowText(strValue);

	//m_cb_Item[CMB_CTP_TESTMODE].SetCurSel(m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.nTestMode);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::GetUpdateData()
{
	CString strValue;

	m_List.GetCellData();

	m_ed_Item[EDT_CTP_REFAXIS_X].GetWindowText(strValue);
	m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.nRefAxisX = _ttoi(strValue);

	m_ed_Item[EDT_CTP_REFAXIS_Y].GetWindowText(strValue);
	m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.nRefAxisY = _ttoi(strValue);

	m_ed_Item[EDT_CTP_DEVOFFSET_X].GetWindowText(strValue);
	m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.iDevOffsetX = _ttoi(strValue);

	m_ed_Item[EDT_CTP_DEVOFFSET_Y].GetWindowText(strValue);
	m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.iDevOffsetY = _ttoi(strValue);

	m_ed_Item[EDT_CTP_OFFSET_X].GetWindowText(strValue);
	m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.iOffsetX = _ttoi(strValue);

	m_ed_Item[EDT_CTP_OFFSET_Y].GetWindowText(strValue);
	m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.iOffsetY = _ttoi(strValue);

//	m_pstModelInfo->stCenterPoint[m_nTestItemCnt].stCenterPointOpt.nTestMode = m_cb_Item[CMB_CTP_TESTMODE].GetCurSel();
}