﻿// List_Work_Brightness.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_Brightness.h"

// CList_Work_Brightness
IMPLEMENT_DYNAMIC(CList_Work_Brightness, CListCtrl)

CList_Work_Brightness::CList_Work_Brightness()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_Brightness::~CList_Work_Brightness()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Work_Brightness, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_Work_Brightness 메시지 처리기입니다.
int CList_Work_Brightness::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	InitHeader();
	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:44
// Desc.		:
//=============================================================================
void CList_Work_Brightness::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
BOOL CList_Work_Brightness::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_Brightness::InitHeader()
{
	for (int nCol = 0; nCol < Br_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_Br_Worklist[nCol], iListAglin_Br_Worklist[nCol], iHeaderWidth_Br_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_Brightness::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));
	SetRectRow(iNewCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_Brightness::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

 	CString strText;
 
	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, Br_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, Br_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, Br_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, Br_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, Br_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, Br_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, Br_W_Barcode, strText);

	//strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	//SetItemText(nRow, Br_W_Operator, strText);

// 	strText.Format(_T("%s"), pstCamInfo->szCamType);
// 	SetItemText(nRow, Br_W_CameraType, strText);

	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stBrightness[m_nTestIndex].stBrightnessResult.nResult].szText);
	SetItemText(nRow, Br_W_Result, strText);

	if (pstCamInfo->stBrightness[m_nTestIndex].stBrightnessResult.nResult == TER_Init)
	{
		for (UINT nRoi = ROI_BR_Side_1; nRoi < ROI_BR_Max; nRoi++)
		{
			strText.Format(_T("X"));
			SetItemText(nRow, Br_W_Side1 + (nRoi - 1), strText);
		}
	}
	else
	{
		for (UINT nRoi = ROI_BR_Side_1; nRoi < ROI_BR_Max; nRoi++)
		{
			strText.Format(_T("%0.2f"), pstCamInfo->stBrightness[m_nTestIndex].stBrightnessResult.dbValue[nRoi]);
			SetItemText(nRow, Br_W_Side1 + (nRoi - 1), strText);
		}
	}
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:47
// Desc.		:
//=============================================================================
void CList_Work_Brightness::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = Br_W_MaxCol;
	CString temp[Br_W_MaxCol];
	for (int t = 0; t < Br_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
