//*****************************************************************************
// Filename	: 	Wnd_Cfg_UserConfig.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_UserConfig.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_UserConfig.h"
#include "resource.h"

// CWnd_Cfg_UserConfig
#define IDC_LC_USERLIST			1000
#define IDC_BN_USERCTRL_S		1100
#define IDC_BN_USERCTRL_E		IDC_BN_USERCTRL_S + UCC_MaxEnum - 1
#define IDC_CHK_USERITEM_S		1200
#define IDC_CHK_USERITEM_E		IDC_CHK_USERITEM_S + UCI_MaxEnum - 1
#define IDC_ED_USERITEM_S		1300
#define IDC_ED_USERITEM_E		IDC_ED_USERITEM_S + UCI_MaxEnum - 1


static LPCTSTR g_szBarcodeList[] =
{
	_T("Operator ID"),			// UCI_OperatorID			
	NULL
};

static LPCTSTR g_szBarcodeCtrl[] =
{
	_T("Add"),		// BCC_Add
	_T("Insert"),	// BCC_Insert
	_T("Remove"),	// BCC_Remove
	_T("Up"),		// BCC_Order_Up
	_T("Down"),		// BCC_Order_Down
	NULL
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_UserConfig, CWnd_BaseView)

CWnd_Cfg_UserConfig::CWnd_Cfg_UserConfig()
{
	VERIFY(m_font.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_UserConfig::~CWnd_Cfg_UserConfig()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_UserConfig, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_CHK_USERITEM_S, IDC_CHK_USERITEM_E, OnBnClickedBnUserItem)
	ON_COMMAND_RANGE(IDC_BN_USERCTRL_S, IDC_BN_USERCTRL_E, OnBnClickedBnOperatorlistCtrl)
END_MESSAGE_MAP()


// CWnd_Cfg_UserConfig message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/24 - 16:23
// Desc.		:
//=============================================================================
int CWnd_Cfg_UserConfig::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 리스트
	m_lc_Operatorlist.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LC_USERLIST);
	
	for (UINT nIdx = 0; nIdx < UCC_MaxEnum; nIdx++)
	{
		m_bn_ListCtrl[nIdx].m_bTransparent = TRUE;
		m_bn_ListCtrl[nIdx].Create(g_szBarcodeCtrl[nIdx], dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_USERCTRL_S + nIdx);
		m_bn_ListCtrl[nIdx].SetMouseCursorHand();
	}
	
	// 항목 조건
	for (UINT nIdx = 0; nIdx < UCI_MaxEnum; nIdx++)
	{
		m_chk_UserConfigItem[nIdx].Create(g_szBarcodeList[nIdx], dwStyle | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_USERITEM_S + nIdx);
		m_ed_UserConfigItem[nIdx].Create(dwStyle | ES_CENTER | WS_BORDER, rectDummy, this, IDC_ED_USERITEM_S + nIdx);

		m_chk_UserConfigItem[nIdx].SetMouseCursorHand();
		m_chk_UserConfigItem[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_UserConfigItem[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_UserConfigItem[nIdx].SizeToContent();

		m_ed_UserConfigItem[nIdx].EnableWindow(FALSE);
		m_ed_UserConfigItem[nIdx].SetFont(&m_font);

		m_ed_UserConfigItem[nIdx].SetWindowText(_T(""));
	}
	
	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Name.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Name.Create(_T("OPERATOR INFO ADD"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_TestName.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_TestName.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_TestName.SetFont_Gdip(L"Arial", 9.0F);
	m_st_TestName.Create(_T("OPERATOR ITEM INFO"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);


	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/24 - 16:23
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iCateSpacing= 10;
	int iMagin		= 10;
	int iLeft		= iMagin;
	int iTop		= iMagin;
	int iWidth		= cx - (iMagin * 2);
	int iHeight		= cy - (iMagin * 2);
	int iHalfWidth	= (iWidth - iCateSpacing) / 3;
	int iHalfHeight	= (iHeight - iCateSpacing)  / 2;
	int iCapWidth	= (iWidth - iSpacing * 4 ) / 5;
	int iValWidth	= 0;
	int iCtrlHeight	= 25;
	int iLeftSub	= 0;

	// 리스트
	m_lc_Operatorlist.MoveWindow(iLeft, iTop, iWidth, iHalfHeight);

	// 스텝 추가/삭제/이동
	iLeft = iMagin;
	iLeftSub = iLeft + iCapWidth + iSpacing;
	iTop += iHalfHeight + iCateSpacing;

	m_st_Name.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
	
	iTop += iCtrlHeight + iSpacing;

	for (UINT nIdx = 0; nIdx < UCC_MaxEnum; nIdx++)
	{
		m_bn_ListCtrl[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);

		iLeft += iCapWidth + iSpacing;
	}
	
	iTop += iCtrlHeight + iCateSpacing;
	iLeft = iMagin;
	m_st_TestName.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);

	iHalfWidth = (iWidth - iCateSpacing) / 2;
	iLeft = iMagin;

	iCapWidth = iHalfWidth - iLeft;
	iLeftSub = iLeft + iCapWidth + iSpacing;

	iValWidth = iWidth - iLeftSub + iMagin;

	iTop += iCtrlHeight + iSpacing;
// 	m_chk_UserConfigItem[UCI_RegistrationTime].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 	m_ed_UserConfigItem[UCI_RegistrationTime].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
// 
// 	iTop += iCtrlHeight + iSpacing;
	m_chk_UserConfigItem[UCI_OperatorID].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_ed_UserConfigItem[UCI_OperatorID].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);


}

//=============================================================================
// Method		: OnBnClickedBnStepItem
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/9/25 - 9:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::OnBnClickedBnUserItem(UINT nID)
{
	enUserConfigItem nIDIdx = (enUserConfigItem)(nID - IDC_CHK_USERITEM_S);
	
	BOOL bEnable = TRUE;

	if (BST_CHECKED == m_chk_UserConfigItem[nIDIdx].GetCheck())
	{
		bEnable = TRUE;
	}
	else
	{
		bEnable = FALSE;
	}
	
	m_ed_UserConfigItem[nIDIdx].EnableWindow(bEnable);
	
	switch (nIDIdx)
	{
	case CWnd_Cfg_UserConfig::UCI_OperatorID:

		if (TRUE == bEnable)
		{
			for (int i = 0; i < UCI_MaxEnum; i++)
			{
				m_chk_UserConfigItem[i].SetCheck(0);
			}

			m_chk_UserConfigItem[UCI_OperatorID].SetCheck(1);

			Item_Enable(nIDIdx);
		}
		else
		{
			m_chk_UserConfigItem[UCI_OperatorID].SetCheck(0);

			m_ed_UserConfigItem[UCI_OperatorID].EnableWindow(FALSE);
		}

		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnBnClickedBnStepCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/9/25 - 9:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::OnBnClickedBnOperatorlistCtrl(UINT nID)
{
	enOperatorListCtrl nIDIdx = (enOperatorListCtrl)(nID - IDC_BN_USERCTRL_S);

	switch (nIDIdx)
	{
	case CWnd_Cfg_UserConfig::UCC_Add:
		Item_Add();
		break;

	case CWnd_Cfg_UserConfig::UCC_Insert:
		Item_Insert();
		break;

	case CWnd_Cfg_UserConfig::UCC_Remove:
		Item_Remove();
		break;

	case CWnd_Cfg_UserConfig::UCC_Order_Up:
		Item_Up();
		break;

	case CWnd_Cfg_UserConfig::UCC_Order_Down:
		Item_Down();
		break;

	default:
		break;
	}
}


//=============================================================================
// Method		: GetTestStepData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __out ST_StepUnit & stStepUnit
// Qualifier	:
// Last Update	: 2017/9/25 - 17:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_UserConfig::GetUserConfigInfoData(__out ST_UserConfig& stUserConfig)
{
	CString		szText;

	// Reference Barcode
	if (BST_CHECKED == m_chk_UserConfigItem[UCI_OperatorID].GetCheck())
	{
		m_ed_UserConfigItem[UCI_OperatorID].GetWindowText(szText);

		if (szText.IsEmpty())
		{
			return FALSE;
		}

		stUserConfig.szOperatorID = szText;

		SYSTEMTIME	ConfigTime;
		GetLocalTime(&ConfigTime);

		szText.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),
			ConfigTime.wYear, ConfigTime.wMonth, ConfigTime.wDay, ConfigTime.wHour, ConfigTime.wMinute, ConfigTime.wSecond);

		stUserConfig.szRegistrationTime = szText;
	}
	else
	{
		stUserConfig.szRegistrationTime.Empty();
		stUserConfig.szOperatorID.Empty();

		return FALSE;
	}



	return TRUE;
}

//=============================================================================
// Method		: Item_Add
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 10:25
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::Item_Add()
{
	ST_UserConfig stUserConfig;
	if (GetUserConfigInfoData(stUserConfig))
	{
		m_lc_Operatorlist.Item_Add(stUserConfig);
	}
}

//=============================================================================
// Method		: Item_Insert
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::Item_Insert()
{
	ST_UserConfig stUserConfig;
	if (GetUserConfigInfoData(stUserConfig))
	{
		m_lc_Operatorlist.Item_Insert(stUserConfig);
	}
}

//=============================================================================
// Method		: Item_Remove
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::Item_Remove()
{
	m_lc_Operatorlist.Item_Remove();
}

//=============================================================================
// Method		: Item_Up
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::Item_Up()
{
	m_lc_Operatorlist.Item_Up();
}

//=============================================================================
// Method		: Item_Down
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::Item_Down()
{
	m_lc_Operatorlist.Item_Down();
}


void CWnd_Cfg_UserConfig::Item_Enable(enUserConfigItem enUserConfig)
{
	switch (enUserConfig)
	{
	case CWnd_Cfg_UserConfig::UCI_OperatorID:

		m_ed_UserConfigItem[UCI_OperatorID].EnableWindow(TRUE);
		m_ed_UserConfigItem[UCI_OperatorID].SetWindowText(_T(""));

		break;

	default:
		break;
	}
}



//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::SetSystemType(__in enInsptrSysType nSysType)
{
}

//=============================================================================
// Method		: Set_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepInfo * pstInStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::Set_UserConfigInfo(__in const ST_UserConfigInfo* pstUserConfigInfo)
{
	m_lc_Operatorlist.Set_UserConfigInfo(pstUserConfigInfo);

	
}

//=============================================================================
// Method		: Get_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_StepInfo & stOutStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:38
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::Get_UserConfigInfo(__out ST_UserConfigInfo& stUserConfigInfo)
{
	m_lc_Operatorlist.Get_UserConfigInfo(stUserConfigInfo);
}

//=============================================================================
// Method		: Init_DefaultSet
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 20:56
// Desc.		:
//=============================================================================
void CWnd_Cfg_UserConfig::Init_DefaultSet()
{
	// 리스트 초기화
	m_lc_Operatorlist.DeleteAllItems();

	// 체크 버튼 초기화
	for (UINT nIdx = 0; nIdx < UCI_MaxEnum; nIdx++)
	{
		m_chk_UserConfigItem[nIdx].SetCheck(BST_UNCHECKED);
		m_ed_UserConfigItem[nIdx].EnableWindow(FALSE);
	}
}
