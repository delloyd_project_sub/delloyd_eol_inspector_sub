﻿// List_Work_OperMode.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_OperMode.h"

// CList_Work_OperMode
IMPLEMENT_DYNAMIC(CList_Work_OperMode, CListCtrl)

CList_Work_OperMode::CList_Work_OperMode()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_OperMode::~CList_Work_OperMode()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Work_OperMode, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_Work_OperMode 메시지 처리기입니다.
int CList_Work_OperMode::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	InitHeader();
	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:44
// Desc.		:
//=============================================================================
void CList_Work_OperMode::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
BOOL CList_Work_OperMode::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_OperMode::InitHeader()
{
	for (int nCol = 0; nCol < OperMode_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_OperMode_Worklist[nCol], iListAglin_OperMode_Worklist[nCol], iHeaderWidth_OperMode_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_OperMode::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iCount = GetItemCount();

	InsertItem(iCount, _T(""));

	SetRectRow(iCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_OperMode::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

 	CString strText;
 
	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, OperMode_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, OperMode_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, OperMode_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, OperMode_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, OperMode_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, OperMode_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, OperMode_W_Barcode, strText);

	//strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	//SetItemText(nRow, OperMode_W_Operator, strText);

	//strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stOperMode[m_nTestIndex].nResult].szText);
	//SetItemText(nRow, OperMode_W_Result, strText);

	//if (pstCamInfo->stOperMode[m_nTestIndex].nResult == TER_Init)
	//{
	//	strText.Format(_T("X"));
	//	SetItemText(nRow, OperMode_W_Reset_15, strText);

	//	strText.Format(_T("X"));
	//	SetItemText(nRow, OperMode_W_FpsCheck_15, strText);

	//	strText.Format(_T("X"));
	//	SetItemText(nRow, OperMode_W_FpsSignal_15, strText);

	//	strText.Format(_T("X"));
	//	SetItemText(nRow, OperMode_W_FpsCurrent_15, strText);

	//	strText.Format(_T("X"));
	//	SetItemText(nRow, OperMode_W_Reset_30, strText);

	//	strText.Format(_T("X"));
	//	SetItemText(nRow, OperMode_W_FpsCheck_30, strText);

	//	strText.Format(_T("X"));
	//	SetItemText(nRow, OperMode_W_FpsSignal_30, strText);

	//	strText.Format(_T("X"));
	//	SetItemText(nRow, OperMode_W_FpsCurrent_30, strText);
	//}
	//else
	//{
	//	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stOperMode[m_nTestIndex].nResetResult[0]].szText);
	//	SetItemText(nRow, OperMode_W_Reset_15, strText);

	//	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stOperMode[m_nTestIndex].nFPSResult[0]].szText);
	//	SetItemText(nRow, OperMode_W_FpsCheck_15, strText);

	//	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stOperMode[m_nTestIndex].nVideoResult[0]].szText);
	//	SetItemText(nRow, OperMode_W_FpsSignal_15, strText);

	//	strText.Format(_T("%.2f"), pstCamInfo->stOperMode[m_nTestIndex].stCurrResult[0].iValue[0]);
	//	SetItemText(nRow, OperMode_W_FpsCurrent_15, strText);

	//	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stOperMode[m_nTestIndex].nResetResult[1]].szText);
	//	SetItemText(nRow, OperMode_W_Reset_30, strText);

	//	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stOperMode[m_nTestIndex].nFPSResult[1]].szText);
	//	SetItemText(nRow, OperMode_W_FpsCheck_30, strText);

	//	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stOperMode[m_nTestIndex].nVideoResult[1]].szText);
	//	SetItemText(nRow, OperMode_W_FpsSignal_30, strText);

	//	strText.Format(_T("%.2f"), pstCamInfo->stOperMode[m_nTestIndex].stCurrResult[1].iValue[0]);
	//	SetItemText(nRow, OperMode_W_FpsCurrent_30, strText);
	//}

}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:47
// Desc.		:
//=============================================================================
void CList_Work_OperMode::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = OperMode_W_MaxCol;
	CString temp[OperMode_W_MaxCol];
	for (int t = 0; t < OperMode_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
