﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Pogo.h
// Created	:	2016/10/31 - 20:17
// Modified	:	2016/10/31 - 20:17
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_Pogo_h__
#define Wnd_Cfg_Pogo_h__

#pragma once

#include "Wnd_BaseView.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "File_Model.h"
#include "File_WatchList.h"

class CWnd_Cfg_Pogo : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_Pogo)

public:
	CWnd_Cfg_Pogo();
	virtual ~CWnd_Cfg_Pogo();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);

	afx_msg void	OnCbnSelendokFile			();
	afx_msg void	OnBnClickedBnSave			();
	afx_msg void	OnBnClickedReset			(UINT nID);	
	afx_msg void	OnBnClickedRegisterationDate(UINT nID);
	afx_msg void	OnEnKillFocusEdMaxCnt		();

	afx_msg LRESULT	OnRefreshFileList		(WPARAM wParam, LPARAM lParam);

	CFont			m_font_Default;

	CVGStatic		m_st_File;
	CComboBox		m_cb_File;
	CButton			m_bn_NewFile;

	CVGStatic		m_stCount_Max[USE_CHANNEL_CNT];
	CVGStatic		m_stCount[USE_CHANNEL_CNT];	
	CEdit			m_ed_Count_Max[USE_CHANNEL_CNT];
	CEdit			m_ed_Count[USE_CHANNEL_CNT];	
	CButton			m_bn_ResetCount[USE_CHANNEL_CNT];


	CVGStatic		m_st_DayConfig[USE_CHANNEL_CNT];
	CVGStatic		m_st_DayCount[USE_CHANNEL_CNT];
	CEdit			m_ed_DayConfig[USE_CHANNEL_CNT];
	CEdit			m_ed_DayCount[USE_CHANNEL_CNT];
	CButton			m_bn_ConfigDay[USE_CHANNEL_CNT];

	CButton			m_bn_Cancel;

	CString			m_strPogoPath;
	ST_PogoInfo*	m_pstPogoCnt;
	CFile_Model		m_fileModel;

	// .ini 파일 변화 감지를 위한 클래스
	CFile_WatchList	m_IniWatch;

	// 선택한 Pogo 파일
	CString		m_szPogoFile;

	// Pogo 데이터 저장 및 가져오기
	BOOL		LoadIniFile		(__in LPCTSTR szFile);
	BOOL		SaveIniFile		(__in LPCTSTR szFile);

	// 콤보 박스의 Pogo 파일 선택 완료시 사용할 함수
	void		SelectEndPogoFile(__in LPCTSTR szFile);

public:	

	// Pogo 설정 파일이 있는 경로
	void		SetPogoPath		(__in LPCTSTR szPath)
	{
		m_strPogoPath = szPath;
		m_IniWatch.SetWatchOption(m_strPogoPath, POGO_FILE_EXT);
		m_IniWatch.BeginWatchThrFunc();
	};

	void	SetPtr_PogoCnt(__in ST_PogoInfo* pstPogoCnt)
	{
		if (pstPogoCnt == NULL)
			return;

		m_pstPogoCnt = pstPogoCnt;
	};


	// 콤보 박스의 Pogo 파일 목록 갱신
	void		RefreshFileList	(__in const CStringList* pFileList);

	// 콤보 박스의 Pogo 파일 선택
	void		SetPogoFile		(__in LPCTSTR szFile);
	ST_PogoInfo	GetPogoInfo		();

	CString		GetPogoName		();
	BOOL		SavePogoSetting	();
};

#endif // Wnd_Cfg_Pogo_h__
