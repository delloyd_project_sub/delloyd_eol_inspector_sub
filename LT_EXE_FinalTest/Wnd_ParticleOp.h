﻿#ifndef Wnd_ParticleOp_h__
#define Wnd_ParticleOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "List_ParticleOp.h"
#include "Def_DataStruct.h"

// CWnd_ParticleOp

enum enParticleStatic
{
	STI_PAR_SENSITIVITY = 0,
	STI_PAR_EDGE_W,
	STI_PAR_EDGE_H,
	STI_PAR_OPT,
	STI_PAR_MAX,
};

static LPCTSTR	g_szParticleStatic[] =
{
	_T("Sensitivity"),
	_T("Edge Width"),
	_T("Edge Height"),
	_T("OPTION LIST"),
	NULL
};

enum enParticleButton
{
	BTN_PAR_TEST = 0,
	BTN_PAR_MAX,
};

static LPCTSTR	g_szParticleButton[] =
{
	_T("TEST"),
	NULL
};

enum enParticleComobox
{
	CMB_PAR_MAX = 1,
};

enum enParticleEdit
{
	EDT_PAR_SENSITIVITY = 0,
	EDT_PAR_EDGE_W,
	EDT_PAR_EDGE_H,
	EDT_PAR_MAX,
};

class CWnd_ParticleOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ParticleOp)

public:
	CWnd_ParticleOp();
	virtual ~CWnd_ParticleOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo		*m_pstModelInfo;
	CList_ParticleOp	m_List;

	CFont				m_font;
	
	CVGStatic			m_st_Item[STI_PAR_MAX];
	CButton				m_bn_Item[BTN_PAR_MAX];
	CComboBox			m_cb_Item[CMB_PAR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_PAR_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};

#endif // Wnd_ParticleOp_h__
