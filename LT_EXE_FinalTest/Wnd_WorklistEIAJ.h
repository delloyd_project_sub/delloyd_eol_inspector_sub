﻿//*****************************************************************************
// Filename	: Wnd_WorklistEIAJ.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistEIAJ_h__
#define Wnd_WorklistEIAJ_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_EIAJ.h"

//=============================================================================
// Wnd_WorklistEIAJ
//=============================================================================
class CWnd_WorklistEIAJ : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistEIAJ)

public:
	CWnd_WorklistEIAJ();
	virtual ~CWnd_WorklistEIAJ();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	CList_Work_EIAJ				m_list_EIAJ[TICnt_EIAJ];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistEIAJ_h__


