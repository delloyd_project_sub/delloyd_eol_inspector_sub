﻿
// List_ParticleOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ParticleOp.h"

// CList_ParticleOp
#define IPtOp_ED_CELLEDIT		5001

IMPLEMENT_DYNAMIC(CList_ParticleOp, CListCtrl)

CList_ParticleOp::CList_ParticleOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol	= 0;
	m_nEditRow	= 0;

	m_nWidth	= 640;
	m_nHeight	= 480;

	m_pstParticle = NULL;
}

CList_ParticleOp::~CList_ParticleOp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_ParticleOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_ParticleOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_ParticleOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IPtOp_ED_CELLEDIT, &CList_ParticleOp::OnEnKillFocusEdit)
END_MESSAGE_MAP()

// CList_ParticleOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/12 - 19:05
// Desc.		:
//=============================================================================
int CList_ParticleOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER /*| LVS_EX_CHECKBOXES*/);

	InitHeader();

	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IPtOp_ED_CELLEDIT);

	this->GetHeaderCtrl()->EnableWindow(FALSE);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/3/12 - 19:05
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	//int iColWidth[PtOp_MaxCol] = { 0, };
	//int iColDivide = 0;
	//int iUnitWidth = 0;
	//int iMisc = 0;

	//CRect rectClient;
	//GetClientRect(rectClient);

	//for (int nCol = PtOp_PosX; nCol < PtOp_MaxCol; nCol++)
	//{
	//	iUnitWidth = (rectClient.Width() - iHeaderWidth_PtOp[PtOp_Object]) / (PtOp_MaxCol - PtOp_PosX);
	//	SetColumnWidth(nCol, iUnitWidth);
	//}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/3/12 - 19:06
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 18:56
// Desc.		:
//=============================================================================
void CList_ParticleOp::InitHeader()
{
	for (int nCol = 0; nCol < PtOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_PtOp[nCol], iListAglin_PtOp[nCol], iHeaderWidth_PtOp[nCol]);
	}

	for (int nCol = 0; nCol < PtOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_PtOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 19:07
// Desc.		:
//=============================================================================
void CList_ParticleOp::InsertFullData()
{
	DeleteAllItems();

	for (int i = 0; i < PtOp_ItemNum; i++)
	{
		InsertItem(i, _T(""));
		SetRectRow(i);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/3/12 - 19:10
// Desc.		:
//=============================================================================
void CList_ParticleOp::SetRectRow(UINT nRow)
{
	CString strText;

	strText.Format(_T("%s"), g_lpszItem_PtOp[nRow]);
	SetItemText(nRow, PtOp_Object, strText);

	//if (m_pstParticle->stParticleOpt.stRegionOp[nRow].bEllipse == TRUE)
	//	SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	//else
	//	SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	//strText.Format(_T("%d"), m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
	//SetItemText(nRow, PtOp_PosX, strText);

	//strText.Format(_T("%d"),  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
	//SetItemText(nRow, PtOp_PosY, strText);

	//strText.Format(_T("%d"),  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Width());
	//SetItemText(nRow, PtOp_Width, strText);

	//strText.Format(_T("%d"),  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Height());
	//SetItemText(nRow, PtOp_Height, strText);

	strText.Format(_T("%.2f"),  m_pstParticle->stParticleOpt.stRegionOp[nRow].dbBruiseConc);
	SetItemText(nRow, PtOp_BruiseConc, strText);

	strText.Format(_T("%.2f"), m_pstParticle->stParticleOpt.stRegionOp[nRow].dbBruiseSize);
	SetItemText(nRow, PtOp_BruiseSize, strText);

}

//=============================================================================
// Method		: OnNMClick
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/3/12 - 19:11
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;

	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer;

		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		//if (nBuffer == 0x2000)
		//	 m_pstParticle->stParticleOpt.stRegionOp[pNMItemActivate->iItem].bEllipse = FALSE;

		//if (nBuffer == 0x1000)
		//	 m_pstParticle->stParticleOpt.stRegionOp[pNMItemActivate->iItem].bEllipse = TRUE;
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/3/12 - 19:11
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem == PtOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				//if (nBuffer == 0x2000)
				//	 m_pstParticle->stParticleOpt.stRegionOp[pNMItemActivate->iItem].bEllipse = FALSE;

				//if (nBuffer == 0x1000)
				//	 m_pstParticle->stParticleOpt.stRegionOp[pNMItemActivate->iItem].bEllipse = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < PtOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}


	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEPtOpellEdit
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 19:11
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == PtOp_BruiseConc || m_nEditCol == PtOp_BruiseSize)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/3/12 - 19:12
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	//if (m_pstParticle == NULL)
	//	return FALSE;

	//if (iValue < 0)
	//	iValue = 0;

	//CRect rtTemp =  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi;

	//switch (nCol)
	//{
	//case PtOp_PosX:
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
	//	break;
	//case PtOp_PosY:
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
	//	break;
	//case PtOp_Width:
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].RectPosWH(iValue, rtTemp.Height());
	//	break;
	//case PtOp_Height:
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].RectPosWH(rtTemp.Width(), iValue);
	//	break;
	//default:
	//	break;
	//}

	//if ( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.left < 0)
	//{
	//	CRect rtTemp =  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi;

	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.left = 0;
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.right =  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.left + rtTemp.Width();
	//}

	//if ( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.right > CAM_IMAGE_WIDTH)
	//{
	//	CRect rtTemp =  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi;

	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.right = CAM_IMAGE_WIDTH;
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.left =  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.right - rtTemp.Width();
	//}

	//if ( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.top < 0)
	//{
	//	CRect rtTemp =  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi;

	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.top = 0;
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.bottom = rtTemp.Height() +  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.top;
	//}

	//if ( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.bottom > CAM_IMAGE_HEIGHT)
	//{
	//	CRect rtTemp =  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi;

	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.bottom = CAM_IMAGE_HEIGHT;
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.top =  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.bottom - rtTemp.Height();
	//}

	//if ( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Height() <= 0)
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].RectPosWH( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Width(), 1);

	//if ( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Height() >= CAM_IMAGE_HEIGHT)
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].RectPosWH( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Width(), CAM_IMAGE_HEIGHT);

	//if ( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Width() <= 0)
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].RectPosWH(1,  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Height());

	//if ( m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Width() >= CAM_IMAGE_WIDTH)
	//	 m_pstParticle->stParticleOpt.stRegionOp[nRow].RectPosWH(CAM_IMAGE_WIDTH,  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Height());

	//CString strValue;

	//switch (nCol)
	//{
	//case PtOp_PosX:
	//	strValue.Format(_T("%d"),  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
	//	break;
	//case PtOp_PosY:
	//	strValue.Format(_T("%d"),  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
	//	break;
	//case PtOp_Width:
	//	strValue.Format(_T("%d"),  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Width());
	//	break;
	//case PtOp_Height:
	//	strValue.Format(_T("%d"),  m_pstParticle->stParticleOpt.stRegionOp[nRow].rtRoi.Height());
	//	break;
	//default:
	//	break;
	//}

	//m_ed_CellEdit.SetWindowText(strValue);
	//SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/3/17 - 13:05
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	case PtOp_BruiseConc:
		 m_pstParticle->stParticleOpt.stRegionOp[nRow].dbBruiseConc = dbValue;
		break;
	case PtOp_BruiseSize:
		m_pstParticle->stParticleOpt.stRegionOp[nRow].dbBruiseSize = dbValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 19:13
// Desc.		:
//=============================================================================
void CList_ParticleOp::GetCellData()
{
	if (m_pstParticle == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/3/17 - 14:48
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue  = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120) * 0.01);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 1000)
			iValue = 1000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (m_nEditCol == PtOp_BruiseConc || m_nEditCol == PtOp_BruiseSize)
		{
			UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
