﻿// Wnd_FFTOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_FFTOp.h"

// CWnd_FFTOp

typedef enum CurrentOp_ID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_FFTOp, CWnd)

CWnd_FFTOp::CWnd_FFTOp()
{
	m_pstModelInfo = NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CWnd_FFTOp::~CWnd_FFTOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_FFTOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_FFTOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 18:01
// Desc.		:
//=============================================================================
int CWnd_FFTOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_FFT_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szFFTStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < EDT_FFT_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < BTN_FFT_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szFFTButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < CMB_FFT_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	if (!m_szWavePath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szWavePath, WAVE_FILE_EXT);
		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());
		m_cb_Item[CMB_FFT_WAVE_SEL].SetCurSel(0);
	}

	return 0;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_FFTOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/8/13 - 14:59
// Desc.		:
//=============================================================================
void CWnd_FFTOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	//CWnd::OnShowWindow(bShow, nStatus);

	//if (NULL == m_pstModelInfo)
	//	return;

	//if (TRUE == bShow)
	//{
	//	m_pstModelInfo->nTestMode = TIID_FFT;
	//	m_pstModelInfo->nTestCnt = m_nTestItemCnt;
	//	m_pstModelInfo->nPicItem = PIC_FFT;
	//}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/1/2 - 19:15
// Desc.		:
//=============================================================================
void CWnd_FFTOp::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_FFT_TEST:
		//GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_FFT);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_FFTOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt = FFTOp_ItemNum;
	int iMargin = 10;
	int iSpacing = 5;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int iHeaderH = 40;
	int iList_H = iHeaderH + iIdxCnt * 12;
	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = iWidth / 4;
	int iSTHeight = 25;

	m_st_Item[STI_FFT_WAVE_SEL].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 1;
	m_cb_Item[CMB_FFT_WAVE_SEL].MoveWindow(iLeft, iTop, iSTWidth * 1.5, iSTHeight);

	iLeft = iMargin;
	iTop += iCtrl_H + iSpacing;
	m_st_Item[STI_FFT_WAVE_VOLUME].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 1;
	m_ed_Item[EDT_FFT_VOLUME].MoveWindow(iLeft, iTop, iSTWidth * 1.5, iSTHeight);

	iLeft = cx - iMargin - iSTWidth;
	m_bn_Item[BTN_FFT_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft = iMargin;
	iTop += iCtrl_H + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);
}

//=============================================================================
// Method		: SetUpdateUI
// Access		: public  
// Returns		: void
// Parameter	: 
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_FFTOp::SetUpdateData()
{
	//if (m_pstModelInfo == NULL)
	//	return;

	//CString strValue;
	//strValue.Format(_T("%d"), m_pstModelInfo->stFFT[m_nTestItemCnt].stFFTOpt.iVolume);
	//m_ed_Item[EDT_FFT_VOLUME].SetWindowText(strValue);

	//m_List.SetPtr_FFT(&m_pstModelInfo->stFFT[m_nTestItemCnt]);
	//m_List.InsertFullData();

	//strValue = m_szWavePath + m_pstModelInfo->stFFT[m_nTestItemCnt].stFFTOpt.szWaveFile;

	//if (!m_szWavePath.IsEmpty())
	//{
	//	m_IniWatch.SetWatchOption(m_szWavePath, WAVE_FILE_EXT);
	//	m_IniWatch.RefreshList();

	//	RefreshFileList(m_IniWatch.GetFileList());
	//}

	//if (!m_pstModelInfo->stFFT[m_nTestItemCnt].stFFTOpt.szWaveFile.IsEmpty())
	//{
	//	int iSel = m_cb_Item[CMB_FFT_WAVE_SEL].FindStringExact(0, m_pstModelInfo->stFFT[m_nTestItemCnt].stFFTOpt.szWaveFile);

	//	if (0 <= iSel)
	//		m_cb_Item[CMB_FFT_WAVE_SEL].SetCurSel(iSel);
	//}
}

//=============================================================================
// Method		: GetUpdateUI
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_FFTOp::GetUpdateData()
{
	//m_List.GetCellData();

	//CString strValue;
	//m_cb_Item[CMB_FFT_WAVE_SEL].GetWindowText(strValue);
	//m_pstModelInfo->stFFT[m_nTestItemCnt].stFFTOpt.szWaveFile = strValue;

	//m_ed_Item[EDT_FFT_VOLUME].GetWindowText(strValue);

	//int iVal = _ttoi(strValue);

	//if (iVal > 100)
	//	iVal = 100;
	//else if (iVal < 0)
	//	iVal = 0;

	//m_pstModelInfo->stFFT[m_nTestItemCnt].stFFTOpt.iVolume = iVal;
}

//=============================================================================
// Method		: RefreshFileList
// Access		: public  
// Returns		: void
// Parameter	: __in const CStringList* pFileList
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_FFTOp::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_Item[CMB_FFT_WAVE_SEL].ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_Item[CMB_FFT_WAVE_SEL].AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_szWavePath.IsEmpty())
	{
		int iSel = m_cb_Item[CMB_FFT_WAVE_SEL].FindStringExact(0, m_szWavePath);

		if (0 <= iSel)
		{
			m_cb_Item[CMB_FFT_WAVE_SEL].SetCurSel(iSel);
		}
	}
}
