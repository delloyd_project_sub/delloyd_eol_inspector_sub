﻿#ifndef List_FFTResult_h__
#define List_FFTResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_FFTResult
{
	FFTResult_Object = 0,
	FFTResult_Value,
	FFTResult_MinSpec,
	FFTResult_MaxSpec,
	FFTResult_Result,
	FFTResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_FFTResult[] =
{
	_T(""),
	_T("Value"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_FFTResult
{
	FFTResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_FFTResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_FFTResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_FFTResult[] =
{
	80,
	80,
	80,
	80,
	80,
};
// List_FFTInfo

class CList_FFTResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_FFTResult)

public:
	CList_FFTResult();
	virtual ~CList_FFTResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_FFT(ST_LT_TI_FFT* pstFFT)
	{
		if (pstFFT == NULL)
			return;

		m_pstFFT = pstFFT;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_FFT*  m_pstFFT;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_FFTInfo_h__
