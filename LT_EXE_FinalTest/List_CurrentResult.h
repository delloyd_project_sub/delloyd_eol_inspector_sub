﻿#ifndef List_CurrentResult_h__
#define List_CurrentResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_CurrentResult
{
	CurrentResult_Object = 0,
	CurrentResult_Value,
	CurrentResult_MinSpec,
	CurrentResult_MaxSpec,
	CurrentResult_Result,
	CurrentResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_CurrentResult[] =
{
	_T(""),
	_T("Value"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_CurrentResult
{
	CurrentResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_CurrentResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_CurrentResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_CurrentResult[] =
{
	80,
	80,
	80,
	80,
	80,
};
// List_CurrentInfo

class CList_CurrentResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_CurrentResult)

public:
	CList_CurrentResult();
	virtual ~CList_CurrentResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Current(ST_LT_TI_Current* pstCurrent)
	{
		if (pstCurrent == NULL)
			return;

		m_pstCurrent = pstCurrent;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Current*  m_pstCurrent;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_CurrentInfo_h__
