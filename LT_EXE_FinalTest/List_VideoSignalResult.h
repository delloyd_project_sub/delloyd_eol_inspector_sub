﻿#ifndef List_VideoSignalResult_h__
#define List_VideoSignalResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_VideoSignalResult
{
	VideoSignalResult_Object = 0,
	VideoSignalResult_Result,
	VideoSignalResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_VideoSignalResult[] =
{
	_T(""),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_VideoSignalResult
{
	VideoSignalResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_VideoSignalResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_VideoSignalResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_VideoSignalResult[] =
{
	80,
	80,
};

// List_VideoSignalInfo

class CList_VideoSignalResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_VideoSignalResult)

public:
	CList_VideoSignalResult();
	virtual ~CList_VideoSignalResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_VideoSignal(ST_LT_TI_VideoSignal* pstVideoSignal)
	{
		if (pstVideoSignal == NULL)
			return;

		m_pstVideoSignal = pstVideoSignal;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_VideoSignal*  m_pstVideoSignal;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_VideoSignalInfo_h__
