﻿//*****************************************************************************
// Filename	: Wnd_RecipeView.h
// Created	: 2015/12/9 - 0:05
// Modified	: 2015/12/9 - 0:05
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Wnd_RecipeView_h__
#define Wnd_RecipeView_h__

enum enMouseEvent
{
	enMouseEvent_Empty = 0,
	enMouseEvent_LeftDown,
	enMouseEvent_RightDown,
};

#pragma once

#include "VGStatic.h"
#include "Def_Enum.h"
#include "File_Model.h"
#include "File_WatchList.h"
#include "Wnd_Cfg_Model.h"
#include "Wnd_Cfg_Base.h"
#include "Wnd_Cfg_TestStep.h"
#include "Wnd_Cfg_TestItem.h"
#include "Wnd_Cfg_Pogo.h"
#include "Wnd_Cfg_LightCtrl.h"
//#include "Wnd_Cfg_DAQ.h"
#include "Wnd_ManualCtrl.h"
#include "Wnd_ManualMotion.h"
#include "Wnd_EachTestResult.h"
#include "Wnd_MessageView.h"

#include "Def_TestDevice.h"
#include "TI_PicControl.h"
#include "TI_Processing.h"

#include "Wnd_ImageView.h"
#include "Wnd_VideoView.h"
#include "TestProcess_Image.h"

#include "Dlg_ParticleManual.h"
#include "Dlg_CenterPtTunning.h"
#include "Dlg_LEDTest.h"

//=============================================================================
// CWnd_RecipeView
//=============================================================================
class CWnd_RecipeView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_RecipeView)

public:
	CWnd_RecipeView();
	virtual ~CWnd_RecipeView();

	CMFCTabCtrl			m_tc_Option;
	CMFCTabCtrl			m_tc_Result;
	// CMFCTabCtrl			m_tc_ImageView;
	UINT				m_nPicViewItem;

	int					m_iTabDaqIdx_Eng;

	void SetLTOption(stLT_Option* pstOption)
	{
		if (pstOption == NULL)
			return;

		m_pstOption = pstOption;
	};

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg void	OnBnClickedBnBarcodeOp	();
	afx_msg void	OnBnClickedBnOperatorOp	();
	afx_msg void	OnBnClickedBnPrinterOp	();
	afx_msg void	OnBnClickedBnNew		();
	afx_msg void	OnBnClickedBnSave		();
	afx_msg void	OnBnClickedBnSaveAs		();
	afx_msg void	OnBnClickedBnLoad		();
	afx_msg void	OnBnClickedBnRefresh	();
	afx_msg void	OnLbnSelChangeModel		();	

	afx_msg LRESULT	OnFileModel				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeModel			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnRefreshModelList		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnTabChangePic			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnManualTestCmd			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnPrinterTestCmd		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnAutoSettingCmd		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnOptDataResetCmd		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnEdgeOnOff				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnDistortionCorr		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnManualCtrlCmd			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnManualMotionCmd(WPARAM wParam, LPARAM lParam);
	LRESULT	OnUpdateDataRoiList		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnModelSave				(WPARAM wParam, LPARAM lParam);
	
//
	CFont				m_font_Default;
	CFont				m_font_Data;

	// 모델 리스트	
	CVGStatic			m_st_Manual;
	CVGStatic			m_st_Video;
	CVGStatic			m_st_File;
	CVGStatic			m_st_Location;
	CListBox			m_lst_ModelList;
	CButton				m_bn_Refresh;

	// New, Save, Save As, Load	
	CMFCButton			m_bn_New;
	CMFCButton			m_bn_Save;
	CMFCButton			m_bn_SaveAs;
	CMFCButton			m_bn_Load;
	CMFCButton			m_bn_BarcodeOp;
	//CMFCButton			m_bn_OperatorOp;
	//CMFCButton			m_bn_PrintOp;

	// Tab Windows
	CWnd_Cfg_Model			m_wnd_ModelCfg;
	CWnd_Cfg_TestStep		m_wnd_TestStepCfg;
	CWnd_Cfg_TestItem		m_wnd_TestItemCfg;
	CWnd_Cfg_Pogo			m_wnd_PogoCfg;
	//CWnd_Cfg_DAQ			m_wnd_DAQCfg;
	
	CWnd_ManualCtrl		m_wnd_ManualCtrl;

	CWnd_EachTestResult	m_wnd_EachTestResult;
	
	CVGStatic			m_st_BackColor;

	ST_ModelInfo		m_stModelInfo;
	CFile_Model			m_fileModel;
	CFile_WatchList		m_ModelWatch;

	// 이미지 테스트
	CTestProcess_Image	m_ImageTest;

	CDlg_ParticleManual m_DlgParticle;
	CDlg_CenterPtTunning m_DlgCenterPtTunning;
	CDlg_LEDTest		m_DlgLEDTest;
	
	ST_Device*			m_pDevice;
	ST_BarcodeRefInfo*  m_pstBarcodeInfo;
	ST_UserConfigInfo*  m_pstUserConfigInfo;
	stLT_Option*		m_pstOption;

	// New, Save, Save as, Load
	void		New_Model();
	void		SaveAs_Model();
	void		Load_Model();
	void		Save_Model();

	void		SetModelInfo();
	void		GetModelInfo();

	CString		m_strModelFullPath;
	CString		m_strModelPath;
	CString		m_strImageFilePath;
	CString		m_strPogoPath;
	CString		m_strI2CPath;
	CString		m_strWavPath;
	CString		m_strBarcodePath;
	CString		m_strUserConfigPath;
	CString		m_strPrnPath;
	CString		m_strRefBarcode;

	void		SetFullPath		(__in LPCTSTR szModelName);
	void		RedrawWindow	();
	BOOL		MessageView		(__in CString szText, __in BOOL bMode = FALSE);

	// 검사항목 별 모션 제어
	UINT		OnEachTest_Motion(__in UINT nTestItemID);

	void		TestProcessCurrent(__in ST_Current_Opt* pstOpt, __out ST_Current_Result &stResult);
	void		TestProcessReset(__in UINT nCh, __in UINT nResetDelay, __out UINT& nResult);
	void		TestProcessVideoSignal(__in UINT nCh, __out UINT& nResult);
	void		TestProcessFrameCheck(__in UINT nCh, __in UINT nMode, __out UINT& nResult);

	// 사용
	void		OnEachTest_Current				(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void		OnEachTest_OperationMode		(__in UINT nfpsMode, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_CenterPoint (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void		OnEachTest_CenterPoint_Manual	(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void		OnEachTest_CenterPoint_Auto		(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_Rotation (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_EIAJ (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_Angle (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_Color (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_Reverse (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void		OnEachTest_LEDTest				(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_ParticleManual (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void		OnEachTest_VideoSignal			(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void		OnEachTest_Reset				(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	
	// 사용 안함
	void		OnEachTest_FFT					(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_Particle (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_DefectPixel (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_Brightness (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void		OnEachTest_SFR					(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void		OnEachTest_PatternNoise			(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	LRESULT OnEachTest_IRFilter (__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void		OnEachTest_IRFilterManual		(__in UINT nStepIdx, __in UINT nTestIdx = 0);

	UINT		OnEachTestMotion(__in UINT nAxisItemID, __in double	dbPos);
	UINT		OnEachTestIOSignal(__in UINT nIO, __in UINT	nSignal);

	void		OnAutoSettingAngleTest(__in UINT nTestIdx);
	void		OnAutoSettingColorTest(__in UINT nTestIdx);
	void		OnPopupParticleManualResult(__out UINT& nTestResult);
	void		OnPopupCenterPtTunningResult();
	void		OnPopupLEDTestResult(__in UINT nTestIdx, __out UINT& nTestResult);

	void		OnAutoSettingOpticalCenter	(__in int iCenterX, __in int iCenterY, __in int iTargerX, __in int iTargerY);
	
	// 광축 조정
	UINT		OpticalCenter_Adj(__in UINT nAxis, __in UINT nCh, __in int iOffset);

	UINT		OpticalCenter_Adj_Horizon(__in UINT nCh, __in int iOffset);
	UINT		OpticalCenter_Adj_Vertical(__in UINT nCh, __in int iOffset);
	UINT		OpticalCenter_Adj_AllWrite(__in UINT nCh);

	// Edge On/Off
	UINT		EdgeOnOff(__in BOOL bOnOff);

	// Distortion Corret
	UINT		DistortionCorr(__in BOOL bOnOff);

	// Mode Change
	UINT		OperationModeChange(__in UINT nMode);

	// Motion
	UINT		Motion_StageY_CL_Distance(__in double dDistance);
	UINT		Motion_StageY_BlemishZone_In(__in UINT nTestItemID);
	UINT		Motion_StageY_BlemishZone_Out();
	UINT		Motion_StageY_Initial();

	// Power On / Off

	UINT		SetLuriCamBrd_Volt(__in  float* fVolt, __in UINT nPort = 0);
	BOOL		CameraOnOff(__in UINT nCh, __in UINT nGrabType, __in UINT nSWVer, __in UINT nFpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff, __in BOOL bOnOff);
	UINT		CameraRegToFunctionChange(__in UINT nSWVer, __in UINT nfpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff);

	HANDLE   m_hExternalExitEvent = NULL; // 외부의 통신 접속 쓰레드 종료 이벤트

	BOOL   m_bFlag_ImageViewMon = FALSE;
	HANDLE   m_hThr_ImageViewMon = NULL; // PLC 모니터링 쓰레드 핸들 
	DWORD   m_dwImageViewMonCycle = 50; // 모니터링 주기

	BOOL   Start_ImageView_Mon();
	BOOL   Stop_ImageView_Mon();
	static UINT WINAPI Thread_ImageViewMon(__in LPVOID lParam);

	ST_ImageMode* m_pstImageMode;


	IplImage *m_LoadImage =NULL;
	BOOL m_bGrabImageStatus = FALSE;
	void OnPopImageLoad();
	void OnLoadImageDisplay();
	void OnImage_LoadDisplay(CString strPath);
	void ChangeImageMode(enImageMode eImageMode);

	void SetLiveVideo(__in BOOL bLive);

public:
	BOOL m_bLive = FALSE;

	// 영상 뷰
	CWnd_VideoView		m_wnd_VideoView;
	CWnd_ImageView		m_wnd_ImageView;
	CWnd_Manual_Motion	m_wnd_ManualMotion;

	IplImage *m_TestImage;

	UINT *m_pnImageSaveType;

	void SetPtr_ImageSaveType(UINT *bMode){
		m_pnImageSaveType = bMode;
	};
	

	void SetPath(
		__in LPCTSTR szModelPath,
		__in LPCTSTR szImagePath, 
		__in LPCTSTR szPogoPath, 
		__in LPCTSTR szI2CPath, 
		__in LPCTSTR szWavPath, 
		__in LPCTSTR szBarcodePath, 
		__in LPCTSTR szOperatorPath,
		__in LPCTSTR szPrnPath
		)
	{
		if (NULL != szModelPath)
			m_strModelPath = szModelPath;

		if (NULL != szImagePath)
			m_strImageFilePath = szImagePath;
		
		if (NULL != szPogoPath)
			m_strPogoPath	 = szPogoPath;

		if (NULL != szI2CPath)
			m_strI2CPath = szI2CPath;

		if (NULL != szWavPath)
			m_strWavPath = szWavPath;

		if (NULL != szBarcodePath)
			m_strBarcodePath = szBarcodePath;

		if (NULL != szOperatorPath)
			m_strUserConfigPath = szOperatorPath;

		if (NULL != szPrnPath)
			m_strPrnPath = szPrnPath;

		m_wnd_ModelCfg.SetPath(szI2CPath);
		m_wnd_PogoCfg.SetPogoPath(szPogoPath);
		m_wnd_TestItemCfg.SetPath(szI2CPath);

		m_ModelWatch.SetWatchOption(m_strModelPath, MODEL_FILE_EXT);
		m_ModelWatch.BeginWatchThrFunc();
	};

	void SetPtr_ImageMode(__in ST_ImageMode *stImageMode)
	{
		m_pstImageMode = stImageMode;
	};
	void ChangePath(__in LPCTSTR lpszModelPath);
		 
	void SetModel(__in LPCTSTR szModel);
		 
	void RefreshModelFileList(__in const CStringList* pFileList);
		 
	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pDevice = pstDevice;
	};

	void SetPtr_BarcodeInfo(__in ST_BarcodeRefInfo*  pstBarcodeInfo, __in CString szRefBarcode)
	{
		if (pstBarcodeInfo == NULL)
			return;

		m_pstBarcodeInfo = pstBarcodeInfo;
		m_strRefBarcode = szRefBarcode;
	};

	void SetPtr_UserConfigInfo(__in ST_UserConfigInfo*  pstUserConfigInfo)
	{
		if (pstUserConfigInfo == NULL)
			return;

		m_pstUserConfigInfo = pstUserConfigInfo;
	};
	
	void SetPtr_PogoCnt		(__in ST_PogoInfo* pstPogoCnt)
	{
		if (pstPogoCnt == NULL)
			return;

		m_wnd_PogoCfg.SetPtr_PogoCnt(pstPogoCnt);
	};

	IplImage* GetImageBuffer(__in BOOL bLive)
	{
		IplImage* pGetImageBuffer = NULL;

		if (bLive)
		{
			pGetImageBuffer = m_wnd_VideoView.GetFrameImageBuffer();
		}
		else
		{
			pGetImageBuffer = m_wnd_ImageView.GetLoadImageBuffer();
		}
		
		return pGetImageBuffer;
	};

	IplImage* GetImagePicBuffer(__in UINT nImageType)
	{
		IplImage* pGetImageBuffer = NULL;

		switch (nImageType)
		{
		case 0:
			pGetImageBuffer = m_wnd_VideoView.GetPicImageBuffer();
			break;
		case 1:
			pGetImageBuffer = m_wnd_ImageView.GetLoadImageBuffer();
			break;
		default:
			break;
		}

		return pGetImageBuffer;
	};

	void		InitOptionView			();

	void		SavePogoCount			();

	void		SaveMasterSet			();

	UINT		GetVideoPicStatus		();

	BOOL		IsRecipeTest			();

	void		SetStatusEngineerMode(__in enPermissionMode InspMode);


	void ReportImageSave(__in enLT_TestItem_ID nTestItem, __in UINT nTestCount, __in SYSTEMTIME* pTime, __in UINT nEachResult);
};

#endif // Wnd_RecipeView_h__

