﻿// List_SFROp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_SFROp.h"
#include "Def_WindowMessage_Cm.h"

#define IRtOp_ED_CELLEDIT		5001
#define IRtOp_CB_FONT			5002

// CList_SFROp


IMPLEMENT_DYNAMIC(CList_SFROp, CListCtrl)

CList_SFROp::CList_SFROp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);

	m_nEditCol	= 0;
	m_nEditRow	= 0;

	m_nWidth	= 640;
	m_nHeight	= 480;

	m_pstSFR	= NULL;
}

CList_SFROp::~CList_SFROp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_SFROp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_SFROp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_SFROp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_SFROp::OnEnKillFocusEdit)
	ON_CBN_KILLFOCUS(IRtOp_CB_FONT, &CList_SFROp::OnEnKillFocusESfrFontCombo)
	ON_CBN_SELCHANGE(IRtOp_CB_FONT, &CList_SFROp::OnEnSelectESfrFontCombo)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_SFROp 메시지 처리기입니다.
int CList_SFROp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader(); 
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);
	
	m_cb_Font.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IRtOp_CB_FONT);
	for (UINT nIdx = 0; nIdx < SFR_Mode_F_Max; nIdx++)
	{
		m_cb_Font.AddString(g_szSFR_FontMode[nIdx]);
	}
	m_cb_Font.SetCurSel(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_SFROp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[SfOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = SfOp_PosX; nCol < SfOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_SFROp[SfOp_Object]) / (SfOp_MaxCol - SfOp_PosX);
		SetColumnWidth(nCol, iUnitWidth);
	}

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_SFROp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_SFROp::InitHeader()
{
	for (int nCol = 0; nCol < SfOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_SFROp[nCol], iListAglin_SFROp[nCol], iHeaderWidth_SFROp[nCol]);
	}

	for (int nCol = 0; nCol < SfOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_SFROp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_SFROp::InsertFullData()
{
	if (m_pstSFR == NULL)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < SfOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_SFROp::SetRectRow(UINT nRow)
{
	if (m_pstSFR == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%02d"), nRow + 1);
	SetItemText(nRow, SfOp_Object, strValue);

	if (m_pstSFR->stSFROpt.stRegionOp[nRow].bEnable == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	strValue.Format(_T("%d"), m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
	SetItemText(nRow, SfOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
	SetItemText(nRow, SfOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Width());
	SetItemText(nRow, SfOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Height());
	SetItemText(nRow, SfOp_Height, strValue);

	strValue.Format(_T("%.2f"), m_pstSFR->stSFROpt.stRegionOp[nRow].dbMinSpc);
	SetItemText(nRow, SfOp_MinSpc, strValue);

	strValue.Format(_T("%.2f"), m_pstSFR->stSFROpt.stRegionOp[nRow].dbMaxSpc);
	SetItemText(nRow, SfOp_MaxSpc, strValue);

	strValue.Format(_T("%.2f"), m_pstSFR->stSFROpt.stRegionOp[nRow].dbOffset);
	SetItemText(nRow, SfOp_Offset, strValue);

	strValue.Format(_T("%.2f"), m_pstSFR->stSFROpt.stRegionOp[nRow].dbLinePair);
	SetItemText(nRow, SfOp_Linepare, strValue);

	strValue.Format(_T("%s"), g_szSFR_FontMode[m_pstSFR->stSFROpt.stRegionOp[nRow].nFont]);
	SetItemText(nRow, SfOp_Font, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_SFROp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;
	
	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer;

		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_pstSFR->stSFROpt.stRegionOp[pNMItemActivate->iItem].bEnable = FALSE;

		if (nBuffer == 0x1000)
			m_pstSFR->stSFROpt.stRegionOp[pNMItemActivate->iItem].bEnable = TRUE;
	}

	if (m_pstSFR->stSFROpt.stRegionOp[pNMItemActivate->iItem].bEnable == TRUE)
	{
		m_pstSFR->stSFROpt.iSelectROI = pNMItemActivate->iItem;
		m_pstSFR->stSFRResult.iSelectROI = -1;
	}
	else
	{
		m_pstSFR->stSFROpt.iSelectROI = -1;
		m_pstSFR->stSFRResult.iSelectROI = -1;
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_SFROp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem == SfOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				if (nBuffer == 0x2000)
					m_pstSFR->stSFROpt.stRegionOp[pNMItemActivate->iItem].bEnable = FALSE;

				if (nBuffer == 0x1000)
					m_pstSFR->stSFROpt.stRegionOp[pNMItemActivate->iItem].bEnable = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < SfOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == SfOp_Font)
			{
				m_cb_Font.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Font.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Font.SetFocus();
				m_cb_Font.SetCurSel(m_pstSFR->stSFROpt.stRegionOp[pNMItemActivate->iItem].nFont);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusECpOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == SfOp_MinSpc || m_nEditCol == SfOp_MaxSpc || m_nEditCol == SfOp_Offset ||  m_nEditCol == SfOp_Linepare)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

void CList_SFROp::OnEnKillFocusESfrFontCombo()
{
	SetItemText(m_nEditRow, SfOp_Font, g_szSFR_FontMode[m_pstSFR->stSFROpt.stRegionOp[m_nEditRow].nFont]);
	m_cb_Font.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

void CList_SFROp::OnEnSelectESfrFontCombo()
{
	m_pstSFR->stSFROpt.stRegionOp[m_nEditRow].nFont = m_cb_Font.GetCurSel();

	SetItemText(m_nEditRow, SfOp_Font, g_szSFR_FontMode[m_pstSFR->stSFROpt.stRegionOp[m_nEditRow].nFont]);
	m_cb_Font.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_SFROp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (m_pstSFR == NULL)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi;

	switch (nCol)
	{
	case SfOp_PosX:
		m_pstSFR->stSFROpt.stRegionOp[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case SfOp_PosY:
		m_pstSFR->stSFROpt.stRegionOp[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case SfOp_Width:
		m_pstSFR->stSFROpt.stRegionOp[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case SfOp_Height:
		m_pstSFR->stSFROpt.stRegionOp[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	case SfOp_MinSpc:
		m_pstSFR->stSFROpt.stRegionOp[nRow].dbMinSpc = (double)iValue;
		break;
	case SfOp_MaxSpc:
		m_pstSFR->stSFROpt.stRegionOp[nRow].dbMaxSpc = (double)iValue;
		break;
	case SfOp_Offset:
		m_pstSFR->stSFROpt.stRegionOp[nRow].dbOffset = (double)iValue;
		break;
	default:
		break;
	}

	if (m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.left < 0)
	{
		CRect rtTemp = m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi;

		m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.left = 0;
		m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.right = m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.left + rtTemp.Width();
	}

	if (m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.right > CAM_IMAGE_WIDTH)
	{
		CRect rtTemp = m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi;

		m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.right = CAM_IMAGE_WIDTH;
		m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.left = m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.right - rtTemp.Width();
	}

	if (m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.top < 0)
	{
		CRect rtTemp = m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi;

		m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.top = 0;
		m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.bottom = rtTemp.Height() + m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.top;
	}

	if (m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.bottom > CAM_IMAGE_HEIGHT)
	{
		CRect rtTemp = m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi;

		m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.bottom = CAM_IMAGE_HEIGHT;
		m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.top = m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.bottom - rtTemp.Height();
	}

	if (m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Height() <= 0)
		m_pstSFR->stSFROpt.stRegionOp[nRow].RectPosWH(m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Width(), 1);

	if (m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Height() >= CAM_IMAGE_HEIGHT)
		m_pstSFR->stSFROpt.stRegionOp[nRow].RectPosWH(m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Width(), CAM_IMAGE_HEIGHT);

	if (m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Width() <= 0)
		m_pstSFR->stSFROpt.stRegionOp[nRow].RectPosWH(1, m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Height());

	if (m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Width() >= CAM_IMAGE_WIDTH)
		m_pstSFR->stSFROpt.stRegionOp[nRow].RectPosWH(CAM_IMAGE_WIDTH, m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Height());

	CString strValue;

	switch (nCol)
	{
	case SfOp_PosX:
		strValue.Format(_T("%d"), m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
		break;
	case SfOp_PosY:
		strValue.Format(_T("%d"), m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
		break;
	case SfOp_Width:
		strValue.Format(_T("%d"), m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Width());
		break;
	case SfOp_Height:
		strValue.Format(_T("%d"), m_pstSFR->stSFROpt.stRegionOp[nRow].rtRoi.Height());
		break;
	case SfOp_MinSpc:
		strValue.Format(_T("%.2f"), m_pstSFR->stSFROpt.stRegionOp[nRow].dbMinSpc);
		break;
	case SfOp_MaxSpc:
		strValue.Format(_T("%.2f"), m_pstSFR->stSFROpt.stRegionOp[nRow].dbMaxSpc);
		break;
	case SfOp_Offset:
		strValue.Format(_T("%.2f"), m_pstSFR->stSFROpt.stRegionOp[nRow].dbOffset);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_SFROp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	case SfOp_Linepare:
		m_pstSFR->stSFROpt.stRegionOp[nRow].dbLinePair = dbValue;
		break;
	case SfOp_MinSpc:
		m_pstSFR->stSFROpt.stRegionOp[nRow].dbMinSpc = dbValue;
		break;
	case SfOp_MaxSpc:
		m_pstSFR->stSFROpt.stRegionOp[nRow].dbMaxSpc = dbValue;
		break;
	case SfOp_Offset:
		m_pstSFR->stSFROpt.stRegionOp[nRow].dbOffset = dbValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_SFROp::GetCellData()
{
	if (m_pstSFR == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_SFROp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue	= _ttof(strText);

		if (0 < zDelta)
		{
			iValue	= iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120) * 0.01);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (m_nEditCol == SfOp_Linepare 
			|| m_nEditCol == SfOp_MinSpc 
			|| m_nEditCol == SfOp_MaxSpc 
			|| m_nEditCol == SfOp_Offset)
		{
			UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
