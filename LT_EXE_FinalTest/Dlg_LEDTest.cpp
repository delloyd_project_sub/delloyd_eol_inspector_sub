// Dlg_LEDTest.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_LEDTest.h"
#include "afxdialogex.h"


CWinThread* pThread_LED = NULL;

static volatile bool isThreadRunning_LED;

UINT MyThread_LED(LPVOID ipParam)
{
	CDlg_LEDTest* pClass = (CDlg_LEDTest*)ipParam;
	int iReturn = pClass->ThreadFunction();
	return 0L;
}

// CDlg_LEDTest 대화 상자입니다.

#define		IDC_WND_IMAGEVIEW		1000
#define		IDC_BN_LED_ON			2000
#define		IDC_BN_LED_OFF			3000
#define		IDC_BN_CURR_TEST		4000
#define		IDC_BN_OK				5000
#define		IDC_BN_NG				6000

IMPLEMENT_DYNAMIC(CDlg_LEDTest, CDialogEx)

CDlg_LEDTest::CDlg_LEDTest(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_LEDTest::IDD, pParent)
{
	m_nResult = 0;
	m_pDevice = NULL;

	VERIFY(m_font.CreateFont(
		30,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	for (UINT nIdx = 0; nIdx < DI_NotUseBit_Max; nIdx++)
		m_bFlag_Butten[nIdx] = FALSE;
}

CDlg_LEDTest::~CDlg_LEDTest()
{
	//KillTimer(100);
	isThreadRunning_LED = false;
	DestroyThread();
	KillTimer(99);
}

void CDlg_LEDTest::CreateThread(UINT _method)
{
	if (pThread_LED != NULL)
	{
		return;
	}

	pThread_LED = AfxBeginThread(MyThread_LED, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);

	if (pThread_LED == NULL)
	{
		//cout<<"Fail to create camera thread!!";
	}

	pThread_LED->m_bAutoDelete = FALSE;
	pThread_LED->ResumeThread();
}

bool CDlg_LEDTest::DestroyThread()
{
	if (NULL != pThread_LED)
	{
		DWORD dwResult = ::WaitForSingleObject(pThread_LED->m_hThread, INFINITE);

		if (dwResult == WAIT_TIMEOUT)
		{
			//cout<<"time out!"<<endl;
		}
		else if (dwResult == WAIT_OBJECT_0)
		{
			//cout<<"Thread END"<<endl;
		}

		delete pThread_LED;

		pThread_LED = NULL;
	}
	return true;
}

int CDlg_LEDTest::ThreadFunction()
{
	while (isThreadRunning_LED)
	{
		if (!isThreadRunning_LED)
			break;

		Sleep(33);

		if (IsCameraConnect(m_nViewChannel, m_pModelinfo->nGrabType))
		{
			LPBYTE pFrameImage = NULL;
			IplImage *pImage = NULL;

			DWORD dwWidth = 0;
			DWORD dwHeight = 0;
			UINT nChannel = 0;

			pFrameImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);

			if (pFrameImage != NULL)
			{
				pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

				if (pImage != NULL)
				{
					if (m_pModelinfo->nGrabType == GrabType_NTSC)
					{
						for (int y = 0; y < dwHeight; y++)
						{
							for (int x = 0; x < dwWidth; x++)
							{
								pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
								pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
								pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
							}
						}
					}

					m_wndVideoView.SetFrameImageBuffer(pImage);
					cvReleaseImage(&pImage);
				}
			}
		}
	}

	return 0;
}

void CDlg_LEDTest::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlg_LEDTest, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BN_LED_ON, OnBnClickedBnLedOn)
	ON_BN_CLICKED(IDC_BN_LED_OFF, OnBnClickedBnLedOff)
	ON_BN_CLICKED(IDC_BN_OK, OnBnClickedBnOK)
	ON_BN_CLICKED(IDC_BN_NG, OnBnClickedBnNG)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CDlg_LEDTest 메시지 처리기입니다.


int CDlg_LEDTest::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	//if (CDialogEx::OnCreate(lpCreateStruct) == -1)
	//	return -1;

	//// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	//DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	//CRect rectDummy;
	//rectDummy.SetRectEmpty();

	//m_wndVideoView.SetOwner(GetParent());
	//m_wndVideoView.SetModelInfo(m_pModelinfo);
	//m_wndVideoView.Create(NULL, _T(""), dwStyle, rectDummy, this, IDC_WND_IMAGEVIEW);

	//m_bn_LEDOn.Create(_T("LED On"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_LED_ON);
	//m_bn_LEDOff.Create(_T("LED Off"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_LED_OFF);
	//m_bn_CurrTest.Create(_T("Current Test"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_CURR_TEST);
	//m_bn_OK.Create(_T("Pass"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_OK);
	//m_bn_NG.Create(_T("Fail"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_NG);

	//m_bn_LEDOn.SetMouseCursorHand();
	//m_bn_LEDOff.SetMouseCursorHand();
	//m_bn_CurrTest.SetMouseCursorHand();
	//m_bn_OK.SetMouseCursorHand();
	//m_bn_NG.SetMouseCursorHand();

	//m_bn_LEDOn.SetFont(&m_font);
	//m_bn_LEDOff.SetFont(&m_font);
	//m_bn_CurrTest.SetFont(&m_font);
	//m_bn_OK.SetFont(&m_font);
	//m_bn_NG.SetFont(&m_font);

	//m_bn_OK.EnableWindow(FALSE);
	//m_bn_NG.EnableWindow(FALSE);
	//
	//isThreadRunning_LED = true;
	//CreateThread(isThreadRunning_LED);

	//SetTimer(99, 100, NULL);

	//m_nResetDelay = m_pModelinfo->stLED[m_pModelinfo->nTestCnt].nResetDelay;
	//m_nResetCount = m_pModelinfo->stLED[m_pModelinfo->nTestCnt].nResetCnt;

	//if (m_nResetDelay < 1000)
	//	m_nResetDelay = 1000;

	//if (m_nResetCount < 1)
	//	m_nResetCount = 1;

	//SetTimer(101, 100, NULL);

	//
	return 0;
}


void CDlg_LEDTest::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	int iSpacing = 1;
	int iCateSpacing = 10;
	int iMagin = 10;
	int iMagin2 = 5;
	int iLeft = iMagin;
	int iTop = iMagin;
	int iWidth = cx - (iMagin * 2);
	int iHeight = cy - (iMagin * 2);

// 	m_wndVideoView.MoveWindow(iLeft, iTop, iWidth, iHeight * 5 / 6);
// 
// 	iTop += iHeight * 5 / 6 + iMagin2;
// 	m_bn_LEDOn.MoveWindow(iLeft, iTop, iWidth / 4 - 4, iHeight / 6);
// 
// 	iLeft += iWidth / 4 + iSpacing;
// 	m_bn_LEDOff.MoveWindow(iLeft, iTop, iWidth / 4 - 4, iHeight / 6);
// 
// 	iTop = iMagin;
// 	iLeft += iWidth / 4 + iMagin2;
// 	iTop += iHeight * 5 / 6 + iMagin2;
// 	m_bn_OK.MoveWindow(iLeft, iTop, iWidth / 4 - 4, iHeight / 6);
// 
// 	iLeft += iWidth / 4 + iSpacing;
// 	m_bn_NG.MoveWindow(iLeft, iTop, iWidth / 4 - 4, iHeight / 6);

	m_wndVideoView.MoveWindow(iLeft, iTop, iWidth, iHeight * 5 / 6);

	iTop += iHeight * 5 / 6 + iSpacing;
	m_bn_OK.MoveWindow(iLeft, iTop, iWidth / 2, iHeight / 6);

	iLeft += iWidth / 2;
	m_bn_NG.MoveWindow(iLeft, iTop, iWidth / 2, iHeight / 6);
}


void CDlg_LEDTest::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	lpMMI->ptMaxTrackSize.x = 1000;
	lpMMI->ptMaxTrackSize.y = 800;

	lpMMI->ptMinTrackSize.x = 1000;
	lpMMI->ptMinTrackSize.y = 800;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


void CDlg_LEDTest::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	LPBYTE pFrameImage = NULL;
	IplImage *pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	switch (nIDEvent)
	{
	case 99:
		if (m_pDevice->DigitalIOCtrl.AXTState() == TRUE)
		{
			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StartBtn))
			{
				if (m_bFlag_Butten[DI_StartBtn] == FALSE)
				{
					m_bFlag_Butten[DI_StartBtn] = TRUE;

					if (m_bn_OK.IsWindowEnabled())
						OnBnClickedBnOK();

					m_bFlag_Butten[DI_StartBtn] = FALSE;
				}
			}

			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StopBtn))
			{
				if (m_bFlag_Butten[DI_StopBtn] == FALSE)
				{
					m_bFlag_Butten[DI_StopBtn] = TRUE;

					if (m_bn_NG.IsWindowEnabled())
						OnBnClickedBnNG();

					m_bFlag_Butten[DI_StopBtn] = FALSE;
				}
			}
		}
		

		break;

	case 100:

		if (IsCameraConnect(m_nViewChannel, m_pModelinfo->nGrabType))
		{
			pFrameImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);

			if (pFrameImage != NULL)
			{
				pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

				if (m_pModelinfo->nGrabType == GrabType_NTSC)
				{
					for (int y = 0; y < dwHeight; y++)
					{
						for (int x = 0; x < dwWidth; x++)
						{
							pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
							pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
							pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
						}
					}
				}

				m_wndVideoView.SetFrameImageBuffer(pImage);
				cvReleaseImage(&pImage);
			}
		}

		break;

	case 101:
		KillTimer(101);

		for (int i = 0; i < m_nResetCount; i++)
		{
			OnBnClickedBnLedOn();
			Sleep(m_nResetDelay);
			OnBnClickedBnLedOff();
		}

		m_bn_OK.EnableWindow(TRUE);
		m_bn_NG.EnableWindow(TRUE);


		break;


	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_LEDTest::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreCreateWindow(cs);
}


BOOL CDlg_LEDTest::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDlg_LEDTest::OnBnClickedBnLedOn()
{
	//if (m_pDevice == NULL)
	//	return;

	//float fVoltOn[CamBrd_Channel] = { m_pModelinfo->fVoltage[0], m_pModelinfo->fVoltage[1] };

	//m_pDevice->PCBCamBrd[0].Send_Volt(fVoltOn);

	//m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrResult.nResetCount++;

	//Sleep(300);

	//LEDCurrentTest();
}

void CDlg_LEDTest::OnBnClickedBnLedOff()
{
	if (m_pDevice == NULL)
		return;

	if (m_pModelinfo == NULL)
		return;
	
	float fVoltOff[CamBrd_Channel] = { m_pModelinfo->fVoltage[0], 0 };

	m_pDevice->PCBCamBrd[0].Send_Volt(fVoltOff);
}


void CDlg_LEDTest::OnBnClickedBnOK()
{
	isThreadRunning_LED = false;
	DestroyThread();

	KillTimer(99);
	KillTimer(101);
	Sleep(500);
	OnBnClickedBnLedOff();

	OnOK();
}

void CDlg_LEDTest::OnBnClickedBnNG()
{
	isThreadRunning_LED = false;
	DestroyThread();

	KillTimer(99);
	KillTimer(101);
	Sleep(500);
	OnBnClickedBnLedOff();

	OnCancel();
}

BOOL CDlg_LEDTest::IsCameraConnect(__in UINT nCh, __in UINT nGrabType)
{
	switch (nGrabType)
	{
	case GrabType_NTSC:

		if (m_pDevice->Cat3DCtrl.IsConnect() == FALSE)
			return FALSE;

		if (m_pDevice->Cat3DCtrl.GetStatus(nCh) == FALSE)
			return FALSE;

		break;
			
	default:
		break;
	}

	return TRUE;
}

LPBYTE CDlg_LEDTest::GetImageBuffer(__in UINT nViewCh, __in UINT nGrabType, __out DWORD& dwWidth, __out DWORD& dwHeight, __out UINT& nChannel)
{
	// 카메라 연결 상태
	if (IsCameraConnect(nViewCh, nGrabType) == FALSE)
		return NULL;

	// Result Buffer
	LPBYTE pRGBDATA = NULL;

	// NTSC Buffer
	ST_VideoRGB_NTSC* pNTSCRGB = NULL;

	if (nGrabType == GrabType_NTSC)
	{
		pNTSCRGB = m_pDevice->Cat3DCtrl.GetRecvRGBData(nViewCh);
		pRGBDATA = (LPBYTE)((m_pDevice->Cat3DCtrl.GetRecvRGBData(nViewCh))->m_pMatRGB[0]);

		dwWidth = pNTSCRGB->m_dwWidth;
		dwHeight = pNTSCRGB->m_dwHeight;
		nChannel = 4;
	}
	
	return pRGBDATA;
}

void CDlg_LEDTest::LEDCurrentTest()
{
	//if (m_pDevice == NULL)
	//	return;

	//if (m_pModelinfo == NULL)
	//	return;

	//double iCurrent[CamBrd_Channel] = { 0, };

	//m_pDevice->PCBCamBrd[0].Send_GetCurrent(iCurrent);
	//m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrResult.iValue[0] = (double)iCurrent[1] * 0.1;
	//m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrResult.iValue[0] *= m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrOpt.dbOffset[0];

	//if (m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrResult.iValue[0] <= m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrOpt.nSpecMax[0]
	//	&& m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrResult.iValue[0] >= m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrOpt.nSpecMin[0])
	//{
	//	m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrResult.nEachResult[0] = TER_Pass;
	//}
	//else
	//{
	//	m_pModelinfo->stLED[m_pModelinfo->nTestCnt].stLEDCurrResult.nEachResult[0] = TER_Fail;
	//}
}


void CDlg_LEDTest::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//KillTimer(100);

	isThreadRunning_LED = false;
	DestroyThread();

	KillTimer(99);
	CDialogEx::OnClose();
}
