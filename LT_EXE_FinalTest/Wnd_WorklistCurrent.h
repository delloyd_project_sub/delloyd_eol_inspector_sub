﻿//*****************************************************************************
// Filename	: Wnd_WorklistCurrent.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistCurrent_h__
#define Wnd_WorklistCurrent_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_Current.h"

//=============================================================================
// Wnd_WorklistCurrent
//=============================================================================
class CWnd_WorklistCurrent : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistCurrent)

public:
	CWnd_WorklistCurrent();
	virtual ~CWnd_WorklistCurrent();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	CList_Work_Current			m_list_Current[TICnt_Current];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistCurrent_h__


