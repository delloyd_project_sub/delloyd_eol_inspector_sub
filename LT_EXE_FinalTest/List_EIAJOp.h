﻿#ifndef List_EIAJOp_h__
#define List_EIAJOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_ReOp
{
	ReOp_Object,
	ReOp_PosX,
	ReOp_PosY,
	ReOp_Width,
	ReOp_Height,
	ReOp_MTF,
	ReOp_Mode,
	ReOp_MinBon,
	ReOp_MaxBon,
	ReOp_MinSpc,
	ReOp_MaxSpc,
	ReOp_Offset,
	ReOp_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_ReOp[] =
{
	_T(""),
	_T("X"),
	_T("Y"),
	_T("W"),
	_T("H"),
	_T("MTF"),
	_T("Mode"),
	_T("Min"),
	_T("Max"),
	_T("MinS"), //_T("Min Spc 本"),
	_T("MaxS"), //_T("Max Spc 本"),
	_T("Offset"),
	NULL
};

const int	iListAglin_ReOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
};

const int	iHeaderWidth_ReOp[] =
{
	50,
	30,
	30,
	30,
	30,
	40,
	45,
	40,
	40,
	45,
	45,
	0,
};

const int	iHeaderWidth_ReOp_offset[] =
{
	50,
	30,
	30,
	30,
	30,
	40,
	45,
	40,
	40,
	45,
	45,
	40,
};
// List_EIAJOp

class CList_EIAJOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_EIAJOp)

public:
	CList_EIAJOp();
	virtual ~CList_EIAJOp();

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);
	BOOL m_bOffsetMode;
	int m_nScrollPosition;

	void SetPtr_Resolution(ST_LT_TI_EIAJ *pstEIAJ)
	{
		if (pstEIAJ == NULL)
			return;

		m_pstEIAJ = pstEIAJ;
	};
	void SetAccessMode_Change(UINT nMode);

protected:

	ST_LT_TI_EIAJ		*m_pstEIAJ;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CComboBox	m_cb_Mode;
	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL			UpdateCellData(UINT nRow, UINT nCol, int  iValue);
	BOOL			UpdateCellData_double(UINT nRow, UINT nCol, double dValue);
	BOOL			CheckRectValue(__in const CRect* pRegionz);

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnEnKillFocusEReOpellEdit();
	afx_msg void	OnEnKillFocusEReOpellCombo();
	afx_msg void	OnEnSelectEReOpellCombo();

	afx_msg BOOL	OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	BOOL Change_DATA_CHECK	(UINT nIdx);

};


#endif // List_CurrentInfo_h__
