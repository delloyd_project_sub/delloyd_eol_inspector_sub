﻿//*****************************************************************************
// Filename	: 	Wnd_EachTestResult.cpp
// Created	:	2017/1/4 - 13:32
// Modified	:	2017/1/4 - 13:32
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_EachTestResult.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_EachTestResult.h"

// CWnd_EachTestResult

IMPLEMENT_DYNAMIC(CWnd_EachTestResult, CWnd)

CWnd_EachTestResult::CWnd_EachTestResult()
{
	m_pModelinfo = NULL;
	m_nTestItemID = 0;
	m_nTestCount = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CWnd_EachTestResult::~CWnd_EachTestResult()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_EachTestResult, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_EachTestResult message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/4 - 13:39
// Desc.		:
//=============================================================================
int CWnd_EachTestResult::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	UINT nID = 1000;

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, nID++, CMFCTabCtrl::LOCATION_TOP);

	m_listResultCurrent.SetOwner(GetOwner());
	m_listResultCurrent.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

	m_listResultCenterPoint.SetOwner(GetOwner());
	m_listResultCenterPoint.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

	m_listResultRotate.SetOwner(GetOwner());
	m_listResultRotate.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

	m_listResultColor.SetOwner(GetOwner());
	m_listResultColor.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

	m_listResultReverse.SetOwner(GetOwner());
	m_listResultReverse.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);
	
	m_listResultEIAJ.SetOwner(GetOwner());
	m_listResultEIAJ.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

	m_listResultFOV.SetOwner(GetOwner());
	m_listResultFOV.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

	m_listResultParticle.SetOwner(GetOwner());
	m_listResultParticle.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);
	
	m_listResultParticleMA.SetOwner(GetOwner());
	m_listResultParticleMA.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);
	
	m_listResultBrightness.SetOwner(GetOwner());
	m_listResultBrightness.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);
		
	m_listResultDefectPixel.SetOwner(GetOwner());
	m_listResultDefectPixel.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

	m_listResultIRFilter.SetOwner(GetOwner());
	m_listResultIRFilter.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

//	m_listResultFFT.SetOwner(GetOwner());
//	m_listResultFFT.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);
	
//	m_listResultSFR.SetOwner(GetOwner());
//	m_listResultSFR.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);
	
//	m_listResultPatternNoise.SetOwner(GetOwner());
//	m_listResultPatternNoise.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

//	m_listResultIRFilterMA.SetOwner(GetOwner());
//	m_listResultIRFilterMA.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

//	m_listResultLED.SetOwner(GetOwner());
//	m_listResultLED.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

//	m_listResultVideoSignal.SetOwner(GetOwner());
//	m_listResultVideoSignal.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

//	m_listResultReset.SetOwner(GetOwner());
//	m_listResultReset.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

//	m_listResultOperMode.SetOwner(GetOwner());
//	m_listResultOperMode.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, nID++);

	UINT nItemCnt = 0;
	m_tc_Option.AddTab(&m_listResultCurrent,		g_szLT_TestItem_Name[TIID_Current],			nItemCnt++, FALSE);
//	m_tc_Option.AddTab(&m_listResultOperMode,		g_szLT_TestItem_Name[TIID_OperationMode],	nItemCnt++, FALSE);
//	m_tc_Option.AddTab(&m_listResultVideoSignal,	g_szLT_TestItem_Name[TIID_VideoSignal],		nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultCenterPoint,	g_szLT_TestItem_Name[TIID_CenterPoint],		nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultRotate,			g_szLT_TestItem_Name[TIID_Rotation],		nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultColor,			g_szLT_TestItem_Name[TIID_Color],			nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultReverse,		g_szLT_TestItem_Name[TIID_Reverse],			nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultEIAJ,			g_szLT_TestItem_Name[TIID_EIAJ],			nItemCnt++, FALSE);
//	m_tc_Option.AddTab(&m_listResultSFR,			g_szLT_TestItem_Name[TIID_SFR],				nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultFOV,			g_szLT_TestItem_Name[TIID_FOV],				nItemCnt++, FALSE);
 	m_tc_Option.AddTab(&m_listResultParticleMA,		g_szLT_TestItem_Name[TIID_ParticleManual],	nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultParticle,		g_szLT_TestItem_Name[TIID_BlackSpot], nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultDefectPixel, g_szLT_TestItem_Name[TIID_DefectPixel], nItemCnt++, FALSE);
	
//	m_tc_Option.AddTab(&m_listResultLED,			g_szLT_TestItem_Name[TIID_LEDTest],			nItemCnt++, FALSE);
//	m_tc_Option.AddTab(&m_listResultReset,			g_szLT_TestItem_Name[TIID_Reset],			nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultBrightness,		g_szLT_TestItem_Name[TIID_Brightness],		nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_listResultIRFilter,		g_szLT_TestItem_Name[TIID_IRFilter],		nItemCnt++, FALSE);
//	m_tc_Option.AddTab(&m_listResultPatternNoise,	g_szLT_TestItem_Name[TIID_PatternNoise],	nItemCnt++, FALSE);


// 	m_tc_Option.AddTab(&m_listResultIRFilterMA, g_szLT_TestItem_Name[TIID_IRFilterManual], nItemCnt++, FALSE);
	//m_tc_Option.AddTab(&m_listResultCenterPoint,	g_szLT_TestItem_Name[TIID_CenterPoint], nItemCnt++, FALSE);
	//m_tc_Option.AddTab(&m_listResultRotate,			g_szLT_TestItem_Name[TIID_Rotation],	nItemCnt++, FALSE);
	//m_tc_Option.AddTab(&m_listResultColor,			g_szLT_TestItem_Name[TIID_Color],		nItemCnt++, FALSE);
	//m_tc_Option.AddTab(&m_listResultFOV,			g_szLT_TestItem_Name[TIID_Angle],		nItemCnt++,	FALSE);
	//m_tc_Option.AddTab(&m_listResultParticle,		g_szLT_TestItem_Name[TIID_Particle],	nItemCnt++, FALSE);
	//m_tc_Option.AddTab(&m_listResultFFT,			g_szLT_TestItem_Name[TIID_FFT],			nItemCnt++, FALSE);

	m_tc_Option.EnableTabSwap(FALSE);
	m_tc_Option.SetActiveTab(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
void CWnd_EachTestResult::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iLeft = 0;
	int iTop = 0;
	int iWidth = cx;
	int iHeight = cy;

	m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iHeight);

}


//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
BOOL CWnd_EachTestResult::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}
