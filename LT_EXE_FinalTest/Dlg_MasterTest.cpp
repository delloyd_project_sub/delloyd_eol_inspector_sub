// Dlg_MasterTest.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_MasterTest.h"
#include "afxdialogex.h"
#include "CommonFunction.h"


CWinThread* pThread_Master = NULL;

static volatile bool isThreadRunning_Master;

UINT MyThread_Master(LPVOID ipParam)
{
	CDlg_MasterTest* pClass = (CDlg_MasterTest*)ipParam;
	int iReturn = pClass->ThreadFunction();
	return 0L;
}

#define		IDC_WND_IMAGEVIEW		1000
#define		IDC_CB_EACH_TEST_SEL	2000
#define		IDC_BTN_START			3000
#define		IDC_BTN_STOP			4000

typedef enum enEachTestSel
{
	enEachTestSel_NG,
	enEachTestSel_OK,
	enEachTestSel_Skip,
	enEachTestSel_Max
};

static LPCTSTR g_szEachTestSel[] =
{
	_T("NG"),
	_T("OK"),
	_T("NO TEST"),
	NULL
};

typedef enum enTotalTestStatus
{
	enTotalTestStatus_Standby,
	enTotalTestStatus_MasterCheck,
	enTotalTestStatus_MasterNG,
	enTotalTestStatus_MasterOK,
	enTotalTestStatus_UserStop,
	enTotalTestStatus_Max
};

static LPCTSTR g_szTotalTestStatus[] =
{
	_T("STAND BY"),
	_T("MASTER CHECK.."),
	_T("MASTER NG"),
	_T("MASTER OK"),
	_T("USER STOP"),
	NULL
};

// CDlg_MasterTest 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_MasterTest, CDialogEx)

CDlg_MasterTest::CDlg_MasterTest(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_MasterTest::IDD, pParent)
{
	m_nResult = 0;
	m_bStopFlag = FALSE;
	m_pDevice = NULL;

	VERIFY(m_font.CreateFont(
		30,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	for (UINT nIdx = 0; nIdx < DI_NotUseBit_Max; nIdx++)
		m_bFlag_Butten[nIdx] = TRUE;
}

CDlg_MasterTest::~CDlg_MasterTest()
{
	//KillTimer(100);
	//KillTimer(99);

	isThreadRunning_Master = false;
	DestroyThread();
}


void CDlg_MasterTest::CreateThread(UINT _method)
{
	if (pThread_Master != NULL)
	{
		return;
	}

	pThread_Master = AfxBeginThread(MyThread_Master, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);

	if (pThread_Master == NULL)
	{
		//cout<<"Fail to create camera thread!!";
	}

	pThread_Master->m_bAutoDelete = FALSE;
	pThread_Master->ResumeThread();
}


bool CDlg_MasterTest::DestroyThread()
{
	if (NULL != pThread_Master)
	{
		DWORD dwResult = ::WaitForSingleObject(pThread_Master->m_hThread, INFINITE);

		if (dwResult == WAIT_TIMEOUT)
		{
			//cout<<"time out!"<<endl;
		}
		else if (dwResult == WAIT_OBJECT_0)
		{
			//cout<<"Thread END"<<endl;
		}

		delete pThread_Master;

		pThread_Master = NULL;
	}
	return true;
}


int CDlg_MasterTest::ThreadFunction()
{
	while (isThreadRunning_Master)
	{
		if (!isThreadRunning_Master)
			break;

		Sleep(33);

		if (IsCameraConnect(m_nViewChannel, m_pModelinfo->nGrabType))
		{
			LPBYTE pFrameImage = NULL;
			IplImage *pImage = NULL;

			DWORD dwWidth = 0;
			DWORD dwHeight = 0;
			UINT nChannel = 0;

			pFrameImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);

			if (pFrameImage != NULL)
			{
				pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

				if (pImage != NULL)
				{
					if (m_pModelinfo->nGrabType == GrabType_NTSC)
					{
						for (int y = 0; y < dwHeight; y++)
						{
							for (int x = 0; x < dwWidth; x++)
							{
								pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
								pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
								pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
							}
						}
					}

					m_wndVideoView.SetFrameImageBuffer(pImage);
					cvReleaseImage(&pImage);
				}
			}
		}
	}

	return 0;
}


void CDlg_MasterTest::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}




BEGIN_MESSAGE_MAP(CDlg_MasterTest, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_START, OnBnClickedBnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, OnBnClickedBnStop)

	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CDlg_MasterTest 메시지 처리기입니다.


int CDlg_MasterTest::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_wndVideoView.SetOwner(GetParent());
	m_wndVideoView.SetModelInfo(m_pModelinfo);
	m_wndVideoView.Create(NULL, _T(""), dwStyle, rectDummy, this, IDC_WND_IMAGEVIEW);

	m_stTotalTestStatus.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_stTotalTestStatus.SetColorStyle(CVGStatic::ColorStyle_Green);
	m_stTotalTestStatus.SetFont_Gdip(L"Arial", 35.0F);
	m_stTotalTestStatus.Create(g_szTotalTestStatus[enTotalTestStatus_Standby], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	for (int i = 0; i < TIID_MaxEnum; i++)
	{
		m_stTestItem[i].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stTestItem[i].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_stTestItem[i].SetFont_Gdip(L"Arial", 10.0F);
		m_stTestItem[i].Create(g_szLT_TestItem_Name[i], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

		m_stEachTestStatus[i].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stEachTestStatus[i].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_stEachTestStatus[i].SetFont_Gdip(L"Arial", 10.0F);
		m_stEachTestStatus[i].Create(_T(""), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

		m_cbEachTestSel[i].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_EACH_TEST_SEL + i);

		for (int j = 0; j < enEachTestSel_Max; j++)
		{
			m_cbEachTestSel[i].InsertString(j, g_szEachTestSel[j]);
		}

		if (m_nMode == 1)
		{
			m_cbEachTestSel[i].SetCurSel(enEachTestSel_NG);
		}
		else
		{
			m_cbEachTestSel[i].SetCurSel(enEachTestSel_OK);
		}

		if (m_nPermisionMode == Permission_Administrator || m_nPermisionMode == Permission_Engineer || m_nPermisionMode == Permission_CNC)
			m_cbEachTestSel[i].EnableWindow(TRUE);
		else
			m_cbEachTestSel[i].EnableWindow(FALSE);

	}

	m_bnStart.Create(_T("START"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_START);
	m_bnStart.SetMouseCursorHand();
	m_bnStart.SetFont(&m_font);

	m_bnStop.Create(_T("STOP"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_STOP);
	m_bnStop.SetMouseCursorHand();
	m_bnStop.SetFont(&m_font);

	//SetTimer(99, 100, NULL);
	//SetTimer(100, 33, NULL);

	isThreadRunning_Master = true;
	CreateThread(isThreadRunning_Master);

	return 0;
}


void CDlg_MasterTest::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	int iSpacing = 1;
	int iCateSpacing = 10;
	int iMagin = 10;
	int iLeft = iMagin;
	int iTop = iMagin;
	int iWidth = cx - (iMagin * 2);
	int iHeight = cy - (iMagin * 2);

	int iTotalStatusW = iWidth / 2 - (iMagin / 2);
	int iTotalStatusH = iHeight / 5 - iMagin;

	int iVideoViewW = iTotalStatusW;
	int iVideoViewH = iHeight - iTotalStatusH - iMagin;
	
	int iBtnW = iTotalStatusW / 2;
	int iBtnH = iHeight / 7 - iMagin;
	
	int iEachTestItemW = 205;
	int iEachTestItemH = 24;

	int iEachTestStatusW = 305;
	int iEachTestStatusH = 24;

	int iEachTestItemSelW = 105;
	int iEachTestItemSelH = 100;
	
	int iNextTop = iTop;
	
	m_stTotalTestStatus.MoveWindow(iLeft, iTop, iTotalStatusW, iTotalStatusH);
	
	iTop += iTotalStatusH + iMagin;
	m_wndVideoView.MoveWindow(iLeft, iTop, iVideoViewW, iVideoViewH);

	iLeft += iTotalStatusW + iMagin;

	ST_StepInfo* const pStepInfo = &m_pModelinfo->stStepInfo;
	INT_PTR iStepCnt = m_pModelinfo->stStepInfo.GetCount();

	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		if (TRUE == pStepInfo->StepList[nStepIdx].bTestUse)
		{
			if (//pStepInfo->StepList[nStepIdx].nTestItem == TIID_LEDTest
				//||
				pStepInfo->StepList[nStepIdx].nTestItem == TIID_ParticleManual
				|| pStepInfo->StepList[nStepIdx].nTestItem == TIID_TestInitialize
				|| pStepInfo->StepList[nStepIdx].nTestItem == TIID_TestFinalize)
			{
				;
			}
			else
			{
				if (m_nMode == 1)
				{
					if (TRUE == pStepInfo->StepList[nStepIdx].bMasterUse)
					{
						if (pStepInfo->StepList[nStepIdx].nTestItem == TIID_CenterPoint)
						{
							if (pStepInfo->StepList[nStepIdx].nTestItemCnt > 0)
								continue;
						}

						int iNextLeft = iLeft;
						m_stTestItem[pStepInfo->StepList[nStepIdx].nTestItem].MoveWindow(iLeft, iNextTop, iEachTestItemW, iEachTestItemH);

						iNextLeft += iEachTestItemW;
						m_stEachTestStatus[pStepInfo->StepList[nStepIdx].nTestItem].MoveWindow(iNextLeft, iNextTop, iEachTestStatusW, iEachTestStatusH);

						iNextLeft += iEachTestStatusW;
						m_cbEachTestSel[pStepInfo->StepList[nStepIdx].nTestItem].MoveWindow(iNextLeft, iNextTop, iEachTestItemSelW, iEachTestItemSelH);

						iNextTop += iEachTestItemH - 2;
					}
				} 
				else
				{
					if (pStepInfo->StepList[nStepIdx].nTestItem == TIID_CenterPoint)
					{
						if (pStepInfo->StepList[nStepIdx].nTestItemCnt > 0)
							continue;
					}

					int iNextLeft = iLeft;
					m_stTestItem[pStepInfo->StepList[nStepIdx].nTestItem].MoveWindow(iLeft, iNextTop, iEachTestItemW, iEachTestItemH);

					iNextLeft += iEachTestItemW;
					m_stEachTestStatus[pStepInfo->StepList[nStepIdx].nTestItem].MoveWindow(iNextLeft, iNextTop, iEachTestStatusW, iEachTestStatusH);

					iNextLeft += iEachTestStatusW;
					m_cbEachTestSel[pStepInfo->StepList[nStepIdx].nTestItem].MoveWindow(iNextLeft, iNextTop, iEachTestItemSelW, iEachTestItemSelH);

					iNextTop += iEachTestItemH - 2;
				}
			}
		}
	}

// 	for (int i = TIID_Current; i < TIID_MaxEnum; i++)
// 	{
// 		int iNextLeft = iLeft;
// 		m_stTestItem[i].MoveWindow(iLeft, iNextTop, iEachTestItemW, iEachTestItemH);
// 		
// 		iNextLeft += iEachTestItemW;
// 		m_stEachTestStatus[i].MoveWindow(iNextLeft, iNextTop, iEachTestStatusW, iEachTestStatusH);
// 
// 		iNextLeft += iEachTestStatusW;
// 		m_cbEachTestSel[i].MoveWindow(iNextLeft, iNextTop, iEachTestItemSelW, iEachTestItemSelH);
// 
// 		iNextTop += iEachTestItemH - 2;
// 	}

	iTop = cy - iMagin - iBtnH;
	m_bnStart.MoveWindow(iLeft, iTop, iBtnW, iBtnH);
	m_bnStop.MoveWindow(iLeft + iBtnW, iTop, iBtnW, iBtnH);

}


void CDlg_MasterTest::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	lpMMI->ptMaxTrackSize.x = 1280;
	lpMMI->ptMaxTrackSize.y = 540;

	lpMMI->ptMinTrackSize.x = 1280;
	lpMMI->ptMinTrackSize.y = 540;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


void CDlg_MasterTest::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	LPBYTE pFrameImage = NULL;
	IplImage *pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	switch (nIDEvent)
	{
	case 99:

		if (m_pDevice->DigitalIOCtrl.AXTState() == TRUE)
		{
			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StartBtn))
			{
				if (m_bFlag_Butten[DI_StartBtn] == FALSE)
				{
					m_bFlag_Butten[DI_StartBtn] = TRUE;

					

					m_bFlag_Butten[DI_StartBtn] = FALSE;
				}
			}

			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StopBtn))
			{
				if (m_bFlag_Butten[DI_StopBtn] == FALSE)
				{
					m_bFlag_Butten[DI_StopBtn] = TRUE;



					m_bFlag_Butten[DI_StopBtn] = FALSE;
				}
			}
		}
		break;

	case 100:

		if (IsCameraConnect(m_nViewChannel, m_pModelinfo->nGrabType))
		{
			pFrameImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);

			if (pFrameImage != NULL)
			{
				pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

			 if (m_pModelinfo->nGrabType == GrabType_NTSC)
				{
					for (int y = 0; y < dwHeight; y++)
					{
						for (int x = 0; x < dwWidth; x++)
						{
							pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
							pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
							pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
						}
					}
				}

				m_wndVideoView.SetFrameImageBuffer(pImage);

				cvReleaseImage(&pImage);
			}
		}

		break;

	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_MasterTest::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreCreateWindow(cs);
}


BOOL CDlg_MasterTest::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CDlg_MasterTest::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CDlg_MasterTest::OnBnClickedBnStart()
{
	if (m_bnStart.IsWindowEnabled() == TRUE)
	{
		m_nResult = 0;

		m_bnStart.EnableWindow(FALSE);

		m_nResult = StartTest();

		m_bnStart.EnableWindow(TRUE);
	}

}

void CDlg_MasterTest::OnBnClickedBnStop()
{
	StopTest();
	m_bnStart.EnableWindow(TRUE);
}

BOOL CDlg_MasterTest::IsCameraConnect(__in UINT nCh, __in UINT nGrabType)
{
	switch (nGrabType)
	{
	case GrabType_NTSC:

		if (m_pDevice->Cat3DCtrl.IsConnect() == FALSE)
			return FALSE;

		if (m_pDevice->Cat3DCtrl.GetStatus(nCh) == FALSE)
			return FALSE;

		break;
			
	default:
		break;
	}

	return TRUE;
}

LPBYTE CDlg_MasterTest::GetImageBuffer(__in UINT nViewCh, __in UINT nGrabType, __out DWORD& dwWidth, __out DWORD& dwHeight, __out UINT& nChannel)
{
	// 카메라 연결 상태
	if (IsCameraConnect(nViewCh, nGrabType) == FALSE)
		return NULL;

	// Result Buffer
	LPBYTE pRGBDATA = NULL;

	// NTSC Buffer
	ST_VideoRGB_NTSC* pNTSCRGB = NULL;

	if (nGrabType == GrabType_NTSC)
	{
		pNTSCRGB = m_pDevice->Cat3DCtrl.GetRecvRGBData(nViewCh);
		pRGBDATA = (LPBYTE)((m_pDevice->Cat3DCtrl.GetRecvRGBData(nViewCh))->m_pMatRGB[0]);

		dwWidth = pNTSCRGB->m_dwWidth;
		dwHeight = pNTSCRGB->m_dwHeight;
		nChannel = 4;
	}
	
	return pRGBDATA;
}

UINT CDlg_MasterTest::SetLuriCamBrd_Volt(__in float* fVolt, __in UINT nPort /*= 0*/)
{
	UINT lReturn = RCA_OK;

	lReturn = m_pDevice->PCBCamBrd[nPort].Send_Volt(fVolt);

	return lReturn;
}

BOOL CDlg_MasterTest::CameraOnOff(__in UINT nCh, __in UINT nGrabType, __in UINT nSWVer, __in UINT nFpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff, __in BOOL bOnOff)
{
	BOOL bResult = FALSE;

	switch (m_pModelinfo->nGrabType)
	{
	case GrabType_NTSC:

		bResult = TRUE;

		break;

// 	case GrabType_LVDS:
// 
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.dwClock = 1000000;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nDataMode = DAQ_Data_16bit_Mode;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nHsyncPolarity = DAQ_HsyncPol_Inverse;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nPClockPolarity = DAQ_PclkPol_RisingEdge;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nDvalUse = DAQ_DeUse_HSync_Use;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nVideoMode = DAQ_Video_Signal_Mode;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nMIPILane = DAQMIPILane_1;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nClockSelect = DAQClockSelect_FixedClock;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nClockUse = DAQClockUse_Off;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.dWidthMultiple = 1.0;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.dHeightMultiple = 1.0;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nConvFormat = Conv_CbYCrY_GRBG;
// 		m_pModelinfo->stLVDSInfo.stLVDSOption.nSensorType = enImgSensorType_YCBCR;
// 
// 		if (m_pModelinfo->bI2CFile_1 == TRUE)
// 			m_pModelinfo->stLVDSInfo.stLVDSOption.szIICPath_1 = m_strI2CPath + m_pModelinfo->szI2CFile_1 + _T(".") + I2C_FILE_EXT;
// 		else
// 			m_pModelinfo->stLVDSInfo.stLVDSOption.szIICPath_1.Empty();
// 
// 		if (m_pModelinfo->bI2CFile_2 == TRUE)
// 			m_pModelinfo->stLVDSInfo.stLVDSOption.szIICPath_2 = m_strI2CPath + m_pModelinfo->szI2CFile_2 + _T(".") + I2C_FILE_EXT;
// 		else
// 			m_pModelinfo->stLVDSInfo.stLVDSOption.szIICPath_2.Empty();
// 
// 		m_pDevice->DAQCtrl.SetDAQOption(nCh, m_pModelinfo->stLVDSInfo.stLVDSOption);
// 
// 		if (bOnOff == ON)
// 		{
// 			CameraRegToFunctionChange(nSWVer, nFpsMode, nDistortion, nEdgeOnOff);
// 			m_pDevice->DAQCtrl.Capture_Start(nCh);
// 		}
// 		else if (bOnOff == OFF)
// 		{
// 			m_pDevice->DAQCtrl.Capture_Stop(nCh);
// 		}
// 
// 		Sleep(m_pModelinfo->nCameraDelay);
// 
// 		bResult = TRUE;
// 
// 		break;

	default:
		break;
	}

	return bResult;
}

UINT CDlg_MasterTest::CameraRegToFunctionChange(__in UINT nSWVer, __in UINT nfpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff)
{
	UINT nTestResult = RCA_OK;

	// Serialize Disable Setting
// 	BYTE SerializeDisable[1] = { 0x43 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeDisable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(100);
// 
// 	// ISP Reset
// 	BYTE ISP_LOW[1] = { 0x0f };
// 	BYTE ISP_HIGH[1] = { 0x8f };
// 	m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x0d, 1, ISP_LOW);
// 	Sleep(100);
// 	m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x0d, 1, ISP_HIGH);
// 	Sleep(1000);
// 
// 	// Distortion
// 	switch (nDistortion)
// 	{
// 	case 0:
// 		DistortionCorr(FALSE);
// 		break;
// 	case 1:
// 		DistortionCorr(TRUE);
// 		break;
// 	default:
// 		break;
// 	}
// 
// 	// Edge
// 	switch (nEdgeOnOff)
// 	{
// 	case 0:
// 		EdgeOnOff(FALSE);
// 		break;
// 	case 1:
// 		EdgeOnOff(TRUE);
// 		break;
// 	default:
// 		break;
// 	}
// 
// 	// Fps Setting
// 	switch (nfpsMode)
// 	{
// 	case 0: // 15
// 		OperationModeChange(0);
// 		break;
// 	case 1: // 30
// 		OperationModeChange(1);
// 		break;
// 	default:
// 		break;
// 	}
// 
// 	Sleep(1000);
// 
// 	// Serialize Enable Setting
// 	BYTE SerializeEnable[1] = { 0x83 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeEnable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	Sleep(100);

	return nTestResult;
}

UINT CDlg_MasterTest::EdgeOnOff(__in BOOL bOnOff)
{
	UINT nTestResult = RCA_OK;

// 	if (bOnOff == TRUE)
// 	{
// 		BYTE EdgeDataOn[8] = { 0x01, 0x06, 0x02, 0x59, 0x00, 0x14, 0x01, 0x80 };
// 
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, EdgeDataOn) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		BYTE EdgeDataOff[8] = { 0x01, 0x06, 0x02, 0x59, 0x00, 0x14, 0x00, 0x7f };
// 
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, EdgeDataOff) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}

	return nTestResult;
}

UINT CDlg_MasterTest::DistortionCorr(__in BOOL bOnOff)
{
	UINT nTestResult = RCA_OK;

// 	if (bOnOff == TRUE)
// 	{
// 		BYTE DistortionDataOff[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x01, 0x2f };
// 		BYTE DistortionDataOn[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x00, 0x2e };
// 
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, DistortionDataOff) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		BYTE DistortionDataOff[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x01, 0x2f };
// 		BYTE DistortionDataOn[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x00, 0x2e };
// 
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, DistortionDataOn) == FALSE)
// 		{
// 			//	nTestResult = RCA_NG;
// 		}
// 	}


	return nTestResult;
}

UINT CDlg_MasterTest::OperationModeChange(__in UINT nMode)
{
	UINT nTestResult = RCA_OK;

	// Serialize Disable Setting
// 	BYTE SerializeDisable[1] = { 0x43 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeDisable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(200);

	// Fps Mode Output
// 	if (nMode == 0) // 15fps
// 	{
// 		BYTE IspSetting_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x01, 0x14 };
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(100);
// 
// 		BYTE IspSetting_F_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x80, 0x94 };
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_F_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(500);
// 	}
// 	else if (nMode == 1) // 30fps
// 	{
// 		BYTE IspSetting_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x30, 0x43 };
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(100);
// 
// 		BYTE IspSetting_F_0[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x80, 0x94 };
// 		if (m_pDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, IspSetting_F_0) == FALSE)
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 
// 		Sleep(500);
// 	}

	// Serialize Enable Setting
// 	BYTE SerializeEnable[1] = { 0x83 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeEnable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(100);

	return nTestResult;
}



void CDlg_MasterTest::TestProcessCurrent(__in ST_Current_Opt* pstOpt, __out ST_Current_Result &stResult)
{
	if (pstOpt == NULL)
		return;

	BOOL bResult = TRUE;
	double iCurrent[Def_CamBrd_Channel] = { 0, };

	// 1. 측정
	if (RCA_OK == m_pDevice->PCBCamBrd[0].Send_GetCurrent(iCurrent))
	{
		// 2. 옵셋
		for (int iCh = 0; iCh < Def_CamBrd_Channel; iCh++)
		{
			stResult.iValue[iCh] = iCurrent[iCh];
			stResult.iValue[iCh] *= pstOpt->dbOffset[iCh];
			//stResult.iValue[iCh] *= 0.1;

			// 3. 결과	 
			if (pstOpt->nSpecMin[iCh] <= stResult.iValue[iCh] && pstOpt->nSpecMax[iCh] >= stResult.iValue[iCh])
			{
				stResult.nEachResult[iCh] = TER_Pass;
			}
			else
			{
				stResult.nEachResult[iCh] = TER_Fail;
				bResult = FALSE;
			}
		}

		if (bResult == TRUE)
		{
			stResult.nResult = TER_Pass;
		}
		else
		{
			stResult.nResult = TER_Fail;
		}

	}
}


void CDlg_MasterTest::TestProcessVideoSignal(__in UINT nCh, __out UINT& nResult)
{
	if (m_pDevice == NULL)
	{
		nResult = RCA_NG;
		return;
	}

	UINT nTestResult = RCA_OK;
	BYTE ReadData[1] = { 0, };

	// Lock 신호 Read
// 	if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x44, 1, 0x00, 1, ReadData) == TRUE)
// 	{
// 		if (ReadData[0] == 0xFF)
// 		{
// 			nTestResult = RCA_OK;
// 		}
// 		else
// 		{
// 			nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		nTestResult = RCA_NG;
// 	}

	nResult = nTestResult;
}


void CDlg_MasterTest::TestProcessFrameCheck(__in UINT nCh, __in UINT nMode, __out UINT& nResult)
{
	if (m_pDevice == NULL)
	{
		nResult = RCA_NG;
		return;
	}

	DWORD dwFrameRate = 0;
	//m_pDevice->DAQCtrl.LVDS_GetFrameRate(nCh, &dwFrameRate);

	switch (nMode)
	{
	case 0: // 15fps
		if (dwFrameRate >= 15)
		{
			nResult = RCA_OK;
		}
		break;

	case 1: // 30fps
		if (dwFrameRate >= 30)
		{
			nResult = RCA_OK;
		}
		break;

	default:
		break;
	}
}


UINT CDlg_MasterTest::OnEachTestMotion(__in UINT nAxisItemID, __in double dbPos)
{
	if (m_pDevice == NULL)
		return RCA_MachineCheck;

	// Exception Process...
	switch (nAxisItemID)
	{
	case AX_StageDistance:

		// 		// Y축의 지령 펄스가 0 ~ 100000.0 사이이면,
		// 		if (120000.0 < dbPos
		// 			&& 250000.0 >= dbPos)
		// 		{
		// 			// X축의 위치가 0 보다 크면 예외처리.
		// 			if (40000.0 < m_pDevice->MotionManager.GetCurrentPos(AX_StageX))
		// 			{
		// 				if (!m_pDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, AX_StageX, 0))
		// 				{
		// 					return RCA_MachineCheck;
		// 				}
		// 
		// 				return RCA_MachineExpcetion;
		// 			}
		// 		}
		// 
		// 		if (!m_pDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, nAxisItemID, dbPos))
		// 		{
		// 			return RCA_MachineCheck;
		// 		}
		// 
		// 		break;

		if (!m_pDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, nAxisItemID, dbPos))
		{
			return RCA_MachineCheck;
		}

	default:

		if (!m_pDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, nAxisItemID, dbPos))
		{
			return RCA_MachineCheck;
		}

		break;
	}

	return RCA_OK;
}

UINT CDlg_MasterTest::OnEachTestIOSignal(__in UINT nIO, __in UINT nSignal)
{
	if (m_pDevice == NULL)
		return RCA_MachineCheck;

	if (!m_pDevice->DigitalIOCtrl.Set_DO_Status(nIO, (enum_IO_SignalType)nSignal))
		return RCA_MachineCheck;

	return RCA_OK;
}

UINT CDlg_MasterTest::OnEachTest_Motion(__in UINT nTestItemID)
{
	UINT lReturn = RCA_OK;
//
//	switch (nTestItemID)
//	{
//	case TIID_TestFinalize:
//		lReturn = Motion_StageY_Initial();
//		break;
//
//	case TIID_TestInitialize:
//		break;
//	case TIID_Current:
//		break;
//		// 	case TIID_VideoSignal:
//		// 		break;
//		// 	case TIID_Reset:
//		// 		break;
//
//	case TIID_CenterPoint:
//	case TIID_Rotation:
////	case TIID_SFR:
//	case TIID_FOV:
//	case TIID_Color:
//	case TIID_Reverse:
//		lReturn = Motion_StageY_BlemishZone_Out();
//		if (lReturn == RCA_OK)
//		{
//			lReturn = Motion_StageY_CL_Distance(m_pModelinfo->dbTestDistance);
//		}
//		break;
//
////	case TIID_LEDTest:
//	case TIID_Brightness:
//	case TIID_IRFilter:
////	case TIID_PatternNoise:
//	case TIID_ParticleManual:
//		lReturn = Motion_StageY_BlemishZone_In(nTestItemID);
//		break;
//
//	default:
//		break;
//	}
//
//	if (lReturn != RCA_OK)
//	{
//		// AfxMessageBox(_T("MACHINE ERROR!!"));
//	}
//
	return lReturn;
}

UINT CDlg_MasterTest::Motion_StageY_CL_Distance(__in double dDistance)
{
	UINT nTestResult = RCA_OK;

//	OnEachTestIOSignal(DO_ChartLight, (enum_IO_SignalType)IO_SignalT_SetOn);

	double dbPuls = (g_dbMotor_AxisLeadMax[AX_StageDistance] - dDistance) * g_dbMotor_AxisResolution[AX_StageDistance];

	nTestResult = OnEachTestMotion(AX_StageDistance, dbPuls);

	return nTestResult;
}

UINT CDlg_MasterTest::Motion_StageY_BlemishZone_In(__in UINT nTestItemID)
{
	UINT nTestResult = RCA_OK;

	switch (nTestItemID)
	{
	case TIID_Brightness:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOn);
		break;

	case TIID_IRFilter:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOff);
		break;

//	case TIID_PatternNoise:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOff);
//		break;

//	case TIID_LEDTest:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOn);
//		break;

	case TIID_ParticleManual:
// 		OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOn);
		break;

	default:
		break;
	}

	//yhm
	double dbPuls_Step1 = 80000;
	double dbPuls_Step2 = 129000;

	// In 상태라면 그대로 리턴.
	if (m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) == dbPuls_Step2)
	{
// 		if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_In) == TRUE)
// 		{
// 			return nTestResult;
// 		}
	}

	// 현재 위치 확인 - pulse 값 : 스테이지가 다크유닛을 지나쳐 있거나 근처에 있다면
	if (m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) >= dbPuls_Step1)
	{
		// 다크유닛 빠지고,
		BOOL bStatus = FALSE;

// 		OnEachTestIOSignal(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOff);

// 		nTestResult = OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		for (int i = 0; i < 100; i++)
// 		{
// 			if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_Out) == TRUE)
// 			{
// 				bStatus = TRUE;
// 				break;
// 			}
// 			DoEvents(33);
// 		}

		if (bStatus == TRUE)
		{
			nTestResult = RCA_OK;
		}
		else
		{
			nTestResult = RCA_MachineCheck;
		}

	}

	// 1 Step
	if (nTestResult == RCA_OK)
	{
		nTestResult = OnEachTestMotion(AX_StageDistance, dbPuls_Step1);
	}

	// 다크유닛 들어옴.
	if (nTestResult == RCA_OK)
	{
		BOOL bStatus = FALSE;

// 		OnEachTestIOSignal(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOff);

// 		nTestResult = OnEachTestIOSignal(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		for (int i = 0; i < 100; i++)
// 		{
// 			if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_In) == TRUE)
// 			{
// 				bStatus = TRUE;
// 				break;
// 			}
// 
// 			DoEvents(33);
// 		}

		if (bStatus == TRUE)
		{
			nTestResult = RCA_OK;
		}
		else
		{
			nTestResult = RCA_MachineCheck;
		}
	}

	// 2 Step
	if (nTestResult == RCA_OK)
	{
		nTestResult = OnEachTestMotion(AX_StageDistance, dbPuls_Step2);
	}

	return nTestResult;
}

UINT CDlg_MasterTest::Motion_StageY_BlemishZone_Out()
{
	UINT nTestResult = RCA_OK;

// 	OnEachTestIOSignal(DO_BlemishLight, (enum_IO_SignalType)IO_SignalT_SetOff);
// 	OnEachTestIOSignal(DO_BlemishIR, (enum_IO_SignalType)IO_SignalT_SetOff);

	double dbPuls_Step1 = 80000;
	double dbPuls_Step2 = 0;

	// Out 상태라면 그대로 리턴.
// 	if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_Out) == TRUE)
// 	{
// 		return nTestResult;
// 	}

	// 현재 위치 확인 - pulse 값 : 스테이지가 다크유닛을 지나쳐 있거나 근처에 있다면
	if (m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) >= dbPuls_Step1)
	{
		// 1 Step
		if (nTestResult == RCA_OK)
		{
			nTestResult = OnEachTestMotion(AX_StageDistance, dbPuls_Step1);
		}

		// 다크유닛 빠지고,
		BOOL bStatus = FALSE;

// 		OnEachTestIOSignal(DO_CylBlemish_In, (enum_IO_SignalType)IO_SignalT_SetOff);
// 		OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOff);

// 		nTestResult = OnEachTestIOSignal(DO_CylBlemish_Out, (enum_IO_SignalType)IO_SignalT_SetOn);
// 		for (int i = 0; i < 100; i++)
// 		{
// 			if (m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_CylSensorBlemish_Out) == TRUE)
// 			{
// 				bStatus = TRUE;
// 				break;
// 			}
// 			DoEvents(33);
// 		}

		if (bStatus == TRUE)
		{
			nTestResult = RCA_OK;
		}
		else
		{
			nTestResult = RCA_MachineCheck;
		}

	}

	return nTestResult;
}

UINT CDlg_MasterTest::Motion_StageY_Initial()
{
	UINT nTestResult = RCA_OK;
	nTestResult = OnEachTestMotion(AX_StageDistance, 0);

	return nTestResult;
}

UINT CDlg_MasterTest::StartTest()
{
	UINT nResult = RCA_NG;

	m_bStopFlag = FALSE;

	OnSetUI_TotalTestStatus(enTotalTestStatus_MasterCheck);
	OnSetUI_EachTestReset();

	// Loading
	float fVoltageOn[2] = { m_pModelinfo->fVoltage[0], 0 };
	float fVoltageOff[2] = { 0, 0 };

	// Power On
	SetLuriCamBrd_Volt(fVoltageOn);
	CameraOnOff(VideoView_Ch_1, m_pModelinfo->nGrabType, 0, 1, 1, 1, ON);

	for (int i = 0; i < 50; i++)
		DoEvents(33);

	ST_StepInfo* const pStepInfo = &m_pModelinfo->stStepInfo;
	INT_PTR iStepCnt = m_pModelinfo->stStepInfo.GetCount();

	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		if (m_bStopFlag == TRUE)
		{
			nResult = 2;
			break;
		}

		if (TRUE == pStepInfo->StepList[nStepIdx].bTestUse)
		{
			if (//pStepInfo->StepList[nStepIdx].nTestItem == TIID_LEDTest
				//|| 
				pStepInfo->StepList[nStepIdx].nTestItem == TIID_ParticleManual
				|| pStepInfo->StepList[nStepIdx].nTestItem == TIID_TestInitialize
				|| pStepInfo->StepList[nStepIdx].nTestItem == TIID_TestFinalize)
			{
				;
			}
			else
			{
				if (m_nMode == 1)
				{
					if (TRUE == pStepInfo->StepList[nStepIdx].bMasterUse)
					{
						if (m_cbEachTestSel[pStepInfo->StepList[nStepIdx].nTestItem].GetCurSel() == enEachTestSel_Skip)
							continue;

						if (pStepInfo->StepList[nStepIdx].nTestItem == TIID_CenterPoint)
						{
							if (pStepInfo->StepList[nStepIdx].nTestItemCnt > 0)
								continue;
						}

						// Test Item - Motion
						//if (StartTest_TestItem_Motion(pStepInfo->StepList[nStepIdx].nTestItem) != RCA_OK)
						//{
						//	AfxMessageBox(_T("Machine Error!"));
						//	break;
						//}

						// Test Item - Image
						StartTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestItemCnt, nStepIdx, pStepInfo->StepList[nStepIdx].nRetryCnt);

						// Result UI
						OnSetUI_EachTestStatus(0, pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestItemCnt);
					}

				} 
				else
				{
					if (m_cbEachTestSel[pStepInfo->StepList[nStepIdx].nTestItem].GetCurSel() == enEachTestSel_Skip)
						continue;

					if (pStepInfo->StepList[nStepIdx].nTestItem == TIID_CenterPoint)
					{
						if (pStepInfo->StepList[nStepIdx].nTestItemCnt > 0)
							continue;
					}

					// Test Item - Motion
					//if (StartTest_TestItem_Motion(pStepInfo->StepList[nStepIdx].nTestItem) != RCA_OK)
					//{
					//	AfxMessageBox(_T("Machine Error!"));
					//	break;
					//}

					// Test Item - Image
					StartTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestItemCnt, nStepIdx, pStepInfo->StepList[nStepIdx].nRetryCnt);

					// Result UI
					OnSetUI_EachTestStatus(0, pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestItemCnt);
				}

				
			}
		}
	}

	// Unloading, Power Off
	CameraOnOff(VideoView_Ch_1, m_pModelinfo->nGrabType, 0, 1, 1, 1, OFF);
	SetLuriCamBrd_Volt(fVoltageOff);

	OnFunc_Motion_Initial();

	// Master Check : 2 이면 Stop
	if (nResult != 2)
		nResult = MasterCheck();

	OnSetUI_TotalTestStatus(enTotalTestStatus_MasterNG + nResult);


	return nResult;
}

UINT CDlg_MasterTest::StopTest()
{
	m_bStopFlag = TRUE;

	return 1;
}

UINT CDlg_MasterTest::StartTest_TestItem(__in UINT nTestItemID, __in UINT nTestIdx, __in UINT nStepIdx, __in int iRetryCount)
{
	UINT lReturn = TER_Init;

	m_pModelinfo->nTestCnt = 0;

	switch (nTestItemID)
	{
	case TIID_Current:
		m_pModelinfo->nPicItem = PIC_Current;
		OnEachTest_Current(nStepIdx, nTestIdx);
		break;

	//case TIID_OperationMode:
	//	m_pModelinfo->nPicItem = PIC_OperMode;
	//	OnEachTest_OperationMode(nStepIdx, nTestIdx);
	//	break;

	case TIID_CenterPoint:
		m_pModelinfo->nPicItem = PIC_CenterPoint;
		OnEachTest_CenterPoint(nStepIdx, nTestIdx);
		break;

	case TIID_Rotation:
		m_pModelinfo->nPicItem = PIC_Rotate;
		OnEachTest_Rotation(nStepIdx, nTestIdx);
		break;

	//case TIID_SFR:
	//	m_pModelinfo->nPicItem = PIC_SFR;
	//	OnEachTest_SFR(nStepIdx, nTestIdx);
	//	break;

	case TIID_EIAJ:
		m_pModelinfo->nPicItem = PIC_EIAJ;
		//OnEachTest_EIAJ(nStepIdx, nTestIdx);
		break;

	case TIID_FOV:
		m_pModelinfo->nPicItem = PIC_Angle;
		OnEachTest_Angle(nStepIdx, nTestIdx);
		break;

	case TIID_Color:
		m_pModelinfo->nPicItem = PIC_Color;
		OnEachTest_Color(nStepIdx, nTestIdx);
		break;

	case TIID_Reverse:
		m_pModelinfo->nPicItem = PIC_Reverse;
		OnEachTest_Reverse(nStepIdx, nTestIdx);
		break;

	case TIID_Brightness:
		m_pModelinfo->nPicItem = PIC_Brightness;
		OnEachTest_Brightness(nStepIdx, nTestIdx);
		break;

	case TIID_IRFilter:
		m_pModelinfo->nPicItem = PIC_IRFilter;
		OnEachTest_IRFilter(nStepIdx, nTestIdx);
		break;

	//case TIID_PatternNoise:
	//	m_pModelinfo->nPicItem = PIC_PatternNoise;
	//	OnEachTest_PatternNoise(nStepIdx, nTestIdx);
	//	break;

	default:
		break;
	}

	return 1;
}

UINT CDlg_MasterTest::StartTest_TestItem_Motion(__in UINT nTestItemID)
{
	UINT lReturn = RCA_OK;
//
//	switch (nTestItemID)
//	{
//	case TIID_TestFinalize:
//
//	//	OnEachTestIOSignal(DO_ChartLight, (enum_IO_SignalType)IO_SignalT_SetOff);
//
//		lReturn = Motion_StageY_BlemishZone_Out();
//		if (lReturn == RCA_OK)
//		{
//			lReturn = Motion_StageY_Initial();
//		}
//
//		break;
//
////	case TIID_OperationMode:
//	case TIID_Current:
//		break;
//
//	case TIID_TestInitialize:
//	case TIID_CenterPoint:
//	case TIID_Rotation:
////	case TIID_SFR:
//	case TIID_FOV:
//	case TIID_Color:
//	case TIID_Reverse:
//		lReturn = Motion_StageY_BlemishZone_Out();
//		if (lReturn == RCA_OK)
//		{
//			lReturn = Motion_StageY_CL_Distance(m_pModelinfo->dbTestDistance);
//		}
//		break;
//
////	case TIID_LEDTest:
//	case TIID_ParticleManual:
//	case TIID_IRFilter:
//	case TIID_Brightness:
////	case TIID_PatternNoise:
//		lReturn = Motion_StageY_BlemishZone_In(nTestItemID);
//		break;
//
//	default:
//		break;
//	}
//
	return lReturn;
}

void CDlg_MasterTest::OnFunc_Motion_Initial()
{
	if (Motion_StageY_BlemishZone_Out() == RCA_OK)
	{
		Motion_StageY_Initial();
	}
}

void CDlg_MasterTest::OnEachTest_Current(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	TestProcessCurrent(&m_pModelinfo->stCurrent[nTestIdx].stCurrentOpt, m_pModelinfo->stCurrent[nTestIdx].stCurrentResult);
}


void CDlg_MasterTest::OnEachTest_OperationMode(__in UINT nfpsMode, __in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	//float fVoltageOn[2] = { m_pModelinfo->fVoltage[0], 0 };
	//float fVoltageOff[2] = { 0, 0 };

	//// 15fps
	//nfpsMode = 0;
	//m_pModelinfo->stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Fail;
	//m_pModelinfo->stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Fail;
	//m_pModelinfo->stOperMode[nTestIdx].nFPSResult[nfpsMode] = TER_Fail;
	//m_pModelinfo->stOperMode[nTestIdx].nVideoResult[nfpsMode] = TER_Fail;
	//m_pModelinfo->stOperMode[nTestIdx].stCurrResult[nfpsMode].Reset();

	//if (CameraRegToFunctionChange(0, nfpsMode, 1, 1))
	//{
	//	for (int i = 0; i < 50; i++)
	//		DoEvents(33);

	//	TestProcessVideoSignal(VideoView_Ch_1, m_pModelinfo->stOperMode[nTestIdx].nVideoResult[nfpsMode]);
	//	if (m_pModelinfo->stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
	//	{
	//		TestProcessFrameCheck(VideoView_Ch_1, nfpsMode, m_pModelinfo->stOperMode[nTestIdx].nFPSResult[nfpsMode]);
	//		if (m_pModelinfo->stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass)
	//		{
	//			TestProcessCurrent(&m_pModelinfo->stOperMode[nTestIdx].stCurrOpt[nfpsMode], m_pModelinfo->stOperMode[nTestIdx].stCurrResult[nfpsMode]);
	//		}
	//	}

	//	m_pModelinfo->stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Pass;
	//	//TestProcessReset(VideoView_Ch_1,
	//	//	m_pModelinfo->stOperMode[nTestIdx].nResetDelay, m_pModelinfo->stOperMode[nTestIdx].nResetResult[nfpsMode]);

	//}

	//if (m_pModelinfo->stOperMode[nTestIdx].stCurrResult[nfpsMode].nEachResult[0] == TER_Pass
	//	&& m_pModelinfo->stOperMode[nTestIdx].nResetResult[nfpsMode] == TER_Pass
	//	&& m_pModelinfo->stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass
	//	&& m_pModelinfo->stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
	//{
	//	m_pModelinfo->stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Pass;
	//}

	//// 30fps
	//nfpsMode = 1;
	//m_pModelinfo->stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Fail;
	//m_pModelinfo->stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Fail;
	//m_pModelinfo->stOperMode[nTestIdx].nFPSResult[nfpsMode] = TER_Fail;
	//m_pModelinfo->stOperMode[nTestIdx].nVideoResult[nfpsMode] = TER_Fail;
	//m_pModelinfo->stOperMode[nTestIdx].stCurrResult[nfpsMode].Reset();

	//if (CameraRegToFunctionChange(0, nfpsMode, 1, 1))
	//{
	//	for (int i = 0; i < 50; i++)
	//		DoEvents(33);

	//	TestProcessVideoSignal(VideoView_Ch_1, m_pModelinfo->stOperMode[nTestIdx].nVideoResult[nfpsMode]);
	//	if (m_pModelinfo->stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
	//	{
	//		TestProcessFrameCheck(VideoView_Ch_1, nfpsMode, m_pModelinfo->stOperMode[nTestIdx].nFPSResult[nfpsMode]);
	//		if (m_pModelinfo->stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass)
	//		{
	//			TestProcessCurrent(&m_pModelinfo->stOperMode[nTestIdx].stCurrOpt[nfpsMode], m_pModelinfo->stOperMode[nTestIdx].stCurrResult[nfpsMode]);
	//		}
	//	}

	//	m_pModelinfo->stOperMode[nTestIdx].nResetResult[nfpsMode] = TER_Pass;
	//	//TestProcessReset(VideoView_Ch_1,
	//	//	m_pModelinfo->stOperMode[nTestIdx].nResetDelay, m_pModelinfo->stOperMode[nTestIdx].nResetResult[nfpsMode]);

	//}

	////CameraOnOff(VideoView_Ch_1, m_pModelinfo->nGrabType, 0, 1, 1, 1, OFF);
	////CameraOnOff(VideoView_Ch_1, m_pModelinfo->nGrabType, 0, 1, 1, 1, ON);


	//if (m_pModelinfo->stOperMode[nTestIdx].stCurrResult[nfpsMode].nEachResult[0] == TER_Pass
	//	&& m_pModelinfo->stOperMode[nTestIdx].nResetResult[nfpsMode] == TER_Pass
	//	&& m_pModelinfo->stOperMode[nTestIdx].nFPSResult[nfpsMode] == TER_Pass
	//	&& m_pModelinfo->stOperMode[nTestIdx].nVideoResult[nfpsMode] == TER_Pass)
	//{
	//	m_pModelinfo->stOperMode[nTestIdx].nEachResult[nfpsMode] = TER_Pass;
	//}

	//if (m_pModelinfo->stOperMode[nTestIdx].nEachResult[0] == TER_Pass
	//	&& m_pModelinfo->stOperMode[nTestIdx].nEachResult[1] == TER_Pass)
	//{
	//	m_pModelinfo->stOperMode[nTestIdx].nResult = RCA_OK;
	//}
	//else
	//{
	//	m_pModelinfo->stOperMode[nTestIdx].nResult = RCA_NG;
	//}
}

void CDlg_MasterTest::OnEachTest_CenterPoint(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	IplImage* pFrameImage = NULL;
	LPBYTE pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	for (int i = 0; i < 30; i++)
	{
		pImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);
		Sleep(33);

		if (pImage != NULL)
			break;
	}

	if (pImage != NULL)
	{
		pFrameImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

		if (m_pModelinfo->nGrabType == GrabType_NTSC)
		{
			for (int y = 0; y < dwHeight; y++)
			{
				for (int x = 0; x < dwWidth; x++)
				{
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 0] = pImage[y * dwWidth * nChannel + x * nChannel + 0];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 1] = pImage[y * dwWidth * nChannel + x * nChannel + 1];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 2] = pImage[y * dwWidth * nChannel + x * nChannel + 2];
				}
			}
		}

		if (pFrameImage != NULL)
		{
			//for (int i = 0; i < 30; i++)
			{
				m_ImageTest.OnSetCameraType(m_pModelinfo->nCameraType);
				m_ImageTest.OnTestProcessCenterPoint(pFrameImage, &m_pModelinfo->stCenterPoint[nTestIdx].stCenterPointOpt, m_pModelinfo->stCenterPoint[nTestIdx].stCenterPointResult);
			}

			cvReleaseImage(&pFrameImage);
		}
	}
	
}

void CDlg_MasterTest::OnEachTest_Rotation(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	IplImage* pFrameImage = NULL;
	LPBYTE pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	for (int i = 0; i < 30; i++)
	{
		pImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);
		Sleep(33);

		if (pImage != NULL)
			break;
	}

	if (pImage != NULL)
	{
		pFrameImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	 if (m_pModelinfo->nGrabType == GrabType_NTSC)
		{
			for (int y = 0; y < dwHeight; y++)
			{
				for (int x = 0; x < dwWidth; x++)
				{
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 0] = pImage[y * dwWidth * nChannel + x * nChannel + 0];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 1] = pImage[y * dwWidth * nChannel + x * nChannel + 1];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 2] = pImage[y * dwWidth * nChannel + x * nChannel + 2];
				}
			}
		}

		if (pFrameImage != NULL)
		{
			m_ImageTest.OnTestProcessRotate(
				pFrameImage,
				&m_pModelinfo->stCenterPoint[0].stCenterPointOpt,
				&m_pModelinfo->stRotate[nTestIdx].stRotateOpt,
				m_pModelinfo->stRotate[nTestIdx].stRotateResult);

			cvReleaseImage(&pFrameImage);
		}
	}
	
}

void CDlg_MasterTest::OnEachTest_Angle(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	// TEST
	IplImage* pFrameImage = NULL;
	LPBYTE pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	for (int i = 0; i < 30; i++)
	{
		pImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);
		Sleep(33);

		if (pImage != NULL)
			break;
	}

	if (pImage != NULL)
	{
		pFrameImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);
				
		if (m_pModelinfo->nGrabType == GrabType_NTSC)
		{
			for (int y = 0; y < dwHeight; y++)
			{
				for (int x = 0; x < dwWidth; x++)
				{
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 0] = pImage[y * dwWidth * nChannel + x * nChannel + 0];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 1] = pImage[y * dwWidth * nChannel + x * nChannel + 1];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 2] = pImage[y * dwWidth * nChannel + x * nChannel + 2];
				}
			}
		}

		if (pFrameImage != NULL)
		{
			m_ImageTest.OnTestProcessAngle(
				pFrameImage,
				&m_pModelinfo->stAngle[nTestIdx].stAngleOpt,
				m_pModelinfo->stAngle[nTestIdx].stAngleResult);

			cvReleaseImage(&pFrameImage);
		}
	}

	
}

void CDlg_MasterTest::OnEachTest_Color(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	IplImage* pFrameImage = NULL;

	LPBYTE pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	for (int i = 0; i < 30; i++)
	{
		pImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);
		Sleep(33);

		if (pImage != NULL)
			break;
	}

	if (pImage != NULL)
	{
		pFrameImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

		 if (m_pModelinfo->nGrabType == GrabType_NTSC)
		{
			for (int y = 0; y < dwHeight; y++)
			{
				for (int x = 0; x < dwWidth; x++)
				{
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 0] = pImage[y * dwWidth * nChannel + x * nChannel + 0];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 1] = pImage[y * dwWidth * nChannel + x * nChannel + 1];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 2] = pImage[y * dwWidth * nChannel + x * nChannel + 2];
				}
			}
		}

		if (pFrameImage != NULL)
		{
			m_ImageTest.OnTestProcessColor(
				pFrameImage,
				&m_pModelinfo->stCenterPoint[0].stCenterPointOpt,
				&m_pModelinfo->stColor[nTestIdx].stColorOpt,
				m_pModelinfo->stColor[nTestIdx].stColorResult);

			cvReleaseImage(&pFrameImage);
		}
	}
	
}

void CDlg_MasterTest::OnEachTest_Reverse(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	IplImage* pFrameImage = NULL;

	LPBYTE pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	for (int i = 0; i < 30; i++)
	{
		pImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);
		Sleep(33);

		if (pImage != NULL)
			break;
	}

	if (pImage != NULL)
	{
		pFrameImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

		if (m_pModelinfo->nGrabType == GrabType_NTSC)
		{
			for (int y = 0; y < dwHeight; y++)
			{
				for (int x = 0; x < dwWidth; x++)
				{
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 0] = pImage[y * dwWidth * nChannel + x * nChannel + 0];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 1] = pImage[y * dwWidth * nChannel + x * nChannel + 1];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 2] = pImage[y * dwWidth * nChannel + x * nChannel + 2];
				}
			}
		}

		if (pFrameImage != NULL)
		{
			m_ImageTest.OnTestProcessReverseColor(
				pFrameImage,
				&m_pModelinfo->stReverse[nTestIdx].stReverseOpt,
				m_pModelinfo->stReverse[nTestIdx].stReverseResult);

			cvReleaseImage(&pFrameImage);
		}
	}
	
}

void CDlg_MasterTest::OnEachTest_Brightness(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	IplImage* pFrameImage = NULL;

	LPBYTE pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	for (int i = 0; i < 30; i++)
	{
		pImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);
		Sleep(33);

		if (pImage != NULL)
			break;
	}

	if (pImage != NULL)
	{
		pFrameImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

		if (m_pModelinfo->nGrabType == GrabType_NTSC)
		{
			for (int y = 0; y < dwHeight; y++)
			{
				for (int x = 0; x < dwWidth; x++)
				{
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 0] = pImage[y * dwWidth * nChannel + x * nChannel + 0];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 1] = pImage[y * dwWidth * nChannel + x * nChannel + 1];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 2] = pImage[y * dwWidth * nChannel + x * nChannel + 2];
				}
			}
		}

		if (pFrameImage != NULL)
		{
			m_ImageTest.OnTestProcessBrightness(pFrameImage,
				&m_pModelinfo->stBrightness[nTestIdx].stBrightnessOpt,
				m_pModelinfo->stBrightness[nTestIdx].stBrightnessResult);

			cvReleaseImage(&pFrameImage);
		}
	}
	
}

void CDlg_MasterTest::OnEachTest_SFR(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	//IplImage* pFrameImage = NULL;

	//m_pModelinfo->bI2CFile_1 = m_pModelinfo->stSFR[nTestIdx].stSFROpt.bIICFile_1;
	//if (m_pModelinfo->bI2CFile_1 == TRUE)
	//{
	//	m_pModelinfo->szI2CFile_1 = m_pModelinfo->stSFR[nTestIdx].stSFROpt.szIICFile_1;

	//	CameraOnOff(VideoView_Ch_1, m_pModelinfo->nGrabType, 0, 1, 1, 1, OFF);
	//	CameraOnOff(VideoView_Ch_1, m_pModelinfo->nGrabType, 0, 1, m_pModelinfo->stSFR[nTestIdx].stSFROpt.bDistortion, m_pModelinfo->stSFR[nTestIdx].stSFROpt.bEdge, ON);
	//}
	//else
	//{
	//	EdgeOnOff(m_pModelinfo->stSFR[nTestIdx].stSFROpt.bEdge);
	//	Sleep(1000);

	//	DistortionCorr(m_pModelinfo->stSFR[nTestIdx].stSFROpt.bDistortion);
	//	Sleep(1000);
	//}

	//LPBYTE pImage = NULL;

	//DWORD dwWidth = 0;
	//DWORD dwHeight = 0;
	//UINT nChannel = 0;

	//for (int i = 0; i < 30; i++)
	//{
	//	pImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);
	//	Sleep(33);

	//	if (pImage != NULL)
	//		break;
	//}

	//if (pImage != NULL)
	//{
	//	pFrameImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	//	if (m_pModelinfo->nGrabType == GrabType_NTSC)
	//	{
	//		for (int y = 0; y < dwHeight; y++)
	//		{
	//			for (int x = 0; x < dwWidth; x++)
	//			{
	//				pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 0] = pImage[y * dwWidth * nChannel + x * nChannel + 0];
	//				pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 1] = pImage[y * dwWidth * nChannel + x * nChannel + 1];
	//				pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 2] = pImage[y * dwWidth * nChannel + x * nChannel + 2];
	//			}
	//		}
	//	}

	//	if (pFrameImage != NULL)
	//	{
	//		m_ImageTest.OnTestProcessSFR(
	//			pFrameImage,
	//			&m_pModelinfo->stSFR[nTestIdx].stSFROpt,
	//			m_pModelinfo->stSFR[nTestIdx].stSFRResult);

	//		cvReleaseImage(&pFrameImage);
	//	}
	//}
	//

	//if (m_pModelinfo->bI2CFile_1 == TRUE)
	//{
	//	float fVoltageOn[2] = { m_pModelinfo->fVoltage[0], 0 };
	//	float fVoltageOff[2] = { 0, 0 };

	//	// OFF
	//	CameraOnOff(VideoView_Ch_1, m_pModelinfo->nGrabType, 0, 1, 1, 1, OFF);
	//	CameraOnOff(VideoView_Ch_1, m_pModelinfo->nGrabType, 0, 1, 1, 1, ON);
	//}
	//else
	//{
	//	DistortionCorr(TRUE);
	//	Sleep(1000);

	//	EdgeOnOff(TRUE);
	//	Sleep(1000);
	//}

}

void CDlg_MasterTest::OnEachTest_PatternNoise(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	//IplImage* pFrameImage = NULL;

	//LPBYTE pImage = NULL;

	//DWORD dwWidth = 0;
	//DWORD dwHeight = 0;
	//UINT nChannel = 0;

	//for (int i = 0; i < 30; i++)
	//{
	//	pImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);
	//	Sleep(33);

	//	if (pImage != NULL)
	//		break;
	//}

	//if (pImage != NULL)
	//{
	//	pFrameImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	//	if (m_pModelinfo->nGrabType == GrabType_NTSC)
	//	{
	//		for (int y = 0; y < dwHeight; y++)
	//		{
	//			for (int x = 0; x < dwWidth; x++)
	//			{
	//				pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 0] = pImage[y * dwWidth * nChannel + x * nChannel + 0];
	//				pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 1] = pImage[y * dwWidth * nChannel + x * nChannel + 1];
	//				pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 2] = pImage[y * dwWidth * nChannel + x * nChannel + 2];
	//			}
	//		}
	//	}

	//	if (pFrameImage != NULL)
	//	{
	//		m_ImageTest.OnTestProcessPatternNoise(
	//			pFrameImage,
	//			&m_pModelinfo->stPatternNoise[nTestIdx].stPatternNoiseOpt,
	//			m_pModelinfo->stPatternNoise[nTestIdx].stPatternNoiseResult);

	//		cvReleaseImage(&pFrameImage);
	//	}
	//}

}

void CDlg_MasterTest::OnEachTest_IRFilter(__in UINT nStepIdx, __in UINT nTestIdx /*= 0*/)
{
	IplImage* pFrameImage = NULL;

	LPBYTE pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	for (int i = 0; i < 30; i++)
	{
		pImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);
		Sleep(33);

		if (pImage != NULL)
			break;
	}

	if (pImage != NULL)
	{
		pFrameImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	if (m_pModelinfo->nGrabType == GrabType_NTSC)
		{
			for (int y = 0; y < dwHeight; y++)
			{
				for (int x = 0; x < dwWidth; x++)
				{
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 0] = pImage[y * dwWidth * nChannel + x * nChannel + 0];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 1] = pImage[y * dwWidth * nChannel + x * nChannel + 1];
					pFrameImage->imageData[y * pFrameImage->widthStep + x * 3 + 2] = pImage[y * dwWidth * nChannel + x * nChannel + 2];
				}
			}
		}

		if (pFrameImage != NULL)
		{
			m_ImageTest.OnTestProcessIRFilter(
				pFrameImage,
				&m_pModelinfo->stIRFilter[nTestIdx].stIRFilterOpt,
				m_pModelinfo->stIRFilter[nTestIdx].stIRFilterResult);

			cvReleaseImage(&pFrameImage);
		}
	}

	
}

BOOL CDlg_MasterTest::MasterCheck()
{
	BOOL nMasterResult = TRUE;

	ST_StepInfo* const pStepInfo = &m_pModelinfo->stStepInfo;
	INT_PTR iStepCnt = m_pModelinfo->stStepInfo.GetCount();

	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		if (TRUE == pStepInfo->StepList[nStepIdx].bTestUse)
		{
			if (//pStepInfo->StepList[nStepIdx].nTestItem == TIID_LEDTest
				//|| 
				pStepInfo->StepList[nStepIdx].nTestItem == TIID_ParticleManual
				|| pStepInfo->StepList[nStepIdx].nTestItem == TIID_TestInitialize
				|| pStepInfo->StepList[nStepIdx].nTestItem == TIID_TestFinalize)
			{
				;
			}
			else
			{

				if (m_nMode == 1)
				{
					if (TRUE == pStepInfo->StepList[nStepIdx].bMasterUse)
					{
						if (pStepInfo->StepList[nStepIdx].nTestItem == TIID_CenterPoint)
						{
							if (pStepInfo->StepList[nStepIdx].nTestItemCnt > 0)
								continue;
						}

						UINT nTestItem_Master = m_cbEachTestSel[pStepInfo->StepList[nStepIdx].nTestItem].GetCurSel();

						switch (pStepInfo->StepList[nStepIdx].nTestItem)
						{
						case TIID_Current:
							if (nTestItem_Master != enEachTestSel_Skip)
							{
								if (m_pModelinfo->stCurrent[pStepInfo->StepList[nStepIdx].nTestItemCnt].stCurrentResult.nResult != nTestItem_Master)
									nMasterResult = FALSE;
							}

							break;
						//case TIID_OperationMode:
						//	if (nTestItem_Master != enEachTestSel_Skip)
						//	{
						//		if (m_pModelinfo->stOperMode[pStepInfo->StepList[nStepIdx].nTestItemCnt].nResult != nTestItem_Master)
						//			nMasterResult = FALSE;
						//	}
						//	break;
						case TIID_CenterPoint:
							if (nTestItem_Master != enEachTestSel_Skip)
							{
								if (m_pModelinfo->stCenterPoint[pStepInfo->StepList[nStepIdx].nTestItemCnt].stCenterPointResult.nResult != nTestItem_Master)
									nMasterResult = FALSE;
							}
							break;
						case TIID_Rotation:
							if (nTestItem_Master != enEachTestSel_Skip)
							{
								if (m_pModelinfo->stRotate[pStepInfo->StepList[nStepIdx].nTestItemCnt].stRotateResult.nResult != nTestItem_Master)
									nMasterResult = FALSE;
							}
							break;
						case TIID_EIAJ:
							if (nTestItem_Master != enEachTestSel_Skip)
							{
								if (m_pModelinfo->stEIAJ[pStepInfo->StepList[nStepIdx].nTestItemCnt].stEIAJData.nResult != nTestItem_Master)
									nMasterResult = FALSE;
							}
							break;
						//case TIID_SFR:
						//	if (nTestItem_Master != enEachTestSel_Skip)
						//	{
						//		if (m_pModelinfo->stSFR[pStepInfo->StepList[nStepIdx].nTestItemCnt].stSFRResult.nResult != nTestItem_Master)
						//			nMasterResult = FALSE;
						//	}
						//	break;
						case TIID_FOV:
							if (nTestItem_Master != enEachTestSel_Skip)
							{
								if (m_pModelinfo->stAngle[pStepInfo->StepList[nStepIdx].nTestItemCnt].stAngleResult.nResult != nTestItem_Master)
									nMasterResult = FALSE;
							}
							break;
						case TIID_Color:
							if (nTestItem_Master != enEachTestSel_Skip)
							{
								if (m_pModelinfo->stColor[pStepInfo->StepList[nStepIdx].nTestItemCnt].stColorResult.nResult != nTestItem_Master)
									nMasterResult = FALSE;
							}
							break;
						case TIID_Reverse:
							if (nTestItem_Master != enEachTestSel_Skip)
							{
								if (m_pModelinfo->stReverse[pStepInfo->StepList[nStepIdx].nTestItemCnt].stReverseResult.nResult != nTestItem_Master)
									nMasterResult = FALSE;
							}
							break;
						case TIID_Brightness:
							if (nTestItem_Master != enEachTestSel_Skip)
							{
								if (m_pModelinfo->stBrightness[pStepInfo->StepList[nStepIdx].nTestItemCnt].stBrightnessResult.nResult != nTestItem_Master)
									nMasterResult = FALSE;
							}
							break;
						case TIID_IRFilter:
							if (nTestItem_Master != enEachTestSel_Skip)
							{
								if (m_pModelinfo->stIRFilter[pStepInfo->StepList[nStepIdx].nTestItemCnt].stIRFilterResult.nResult != nTestItem_Master)
									nMasterResult = FALSE;
							}
							break;
						//case TIID_PatternNoise:
						//	if (nTestItem_Master != enEachTestSel_Skip)
						//	{
						//		if (m_pModelinfo->stPatternNoise[pStepInfo->StepList[nStepIdx].nTestItemCnt].stPatternNoiseResult.nResult != nTestItem_Master)
						//			nMasterResult = FALSE;
						//	}
						//	break;
						default:
							break;
						}
					}
				}
				else
				{
					if (pStepInfo->StepList[nStepIdx].nTestItem == TIID_CenterPoint)
					{
						if (pStepInfo->StepList[nStepIdx].nTestItemCnt > 0)
							continue;
					}

					UINT nTestItem_Master = m_cbEachTestSel[pStepInfo->StepList[nStepIdx].nTestItem].GetCurSel();

					switch (pStepInfo->StepList[nStepIdx].nTestItem)
					{
					case TIID_Current:
						if (nTestItem_Master != enEachTestSel_Skip)
						{
							if (m_pModelinfo->stCurrent[pStepInfo->StepList[nStepIdx].nTestItemCnt].stCurrentResult.nResult != nTestItem_Master)
								nMasterResult = FALSE;
						}

						break;
					//case TIID_OperationMode:
					//	if (nTestItem_Master != enEachTestSel_Skip)
					//	{
					//		if (m_pModelinfo->stOperMode[pStepInfo->StepList[nStepIdx].nTestItemCnt].nResult != nTestItem_Master)
					//			nMasterResult = FALSE;
					//	}
					//	break;
					case TIID_CenterPoint:
						if (nTestItem_Master != enEachTestSel_Skip)
						{
							if (m_pModelinfo->stCenterPoint[pStepInfo->StepList[nStepIdx].nTestItemCnt].stCenterPointResult.nResult != nTestItem_Master)
								nMasterResult = FALSE;
						}
						break;
					case TIID_Rotation:
						if (nTestItem_Master != enEachTestSel_Skip)
						{
							if (m_pModelinfo->stRotate[pStepInfo->StepList[nStepIdx].nTestItemCnt].stRotateResult.nResult != nTestItem_Master)
								nMasterResult = FALSE;
						}
						break;

					case TIID_EIAJ:
						if (nTestItem_Master != enEachTestSel_Skip)
						{
							if (m_pModelinfo->stEIAJ[pStepInfo->StepList[nStepIdx].nTestItemCnt].stEIAJData.nResult != nTestItem_Master)
								nMasterResult = FALSE;
						}
						break;
					//case TIID_SFR:
					//	if (nTestItem_Master != enEachTestSel_Skip)
					//	{
					//		if (m_pModelinfo->stSFR[pStepInfo->StepList[nStepIdx].nTestItemCnt].stSFRResult.nResult != nTestItem_Master)
					//			nMasterResult = FALSE;
					//	}
					//	break;
					case TIID_FOV:
						if (nTestItem_Master != enEachTestSel_Skip)
						{
							if (m_pModelinfo->stAngle[pStepInfo->StepList[nStepIdx].nTestItemCnt].stAngleResult.nResult != nTestItem_Master)
								nMasterResult = FALSE;
						}
						break;
					case TIID_Color:
						if (nTestItem_Master != enEachTestSel_Skip)
						{
							if (m_pModelinfo->stColor[pStepInfo->StepList[nStepIdx].nTestItemCnt].stColorResult.nResult != nTestItem_Master)
								nMasterResult = FALSE;
						}
						break;
					case TIID_Reverse:
						if (nTestItem_Master != enEachTestSel_Skip)
						{
							if (m_pModelinfo->stReverse[pStepInfo->StepList[nStepIdx].nTestItemCnt].stReverseResult.nResult != nTestItem_Master)
								nMasterResult = FALSE;
						}
						break;
					case TIID_Brightness:
						if (nTestItem_Master != enEachTestSel_Skip)
						{
							if (m_pModelinfo->stBrightness[pStepInfo->StepList[nStepIdx].nTestItemCnt].stBrightnessResult.nResult != nTestItem_Master)
								nMasterResult = FALSE;
						}
						break;
					case TIID_IRFilter:
						if (nTestItem_Master != enEachTestSel_Skip)
						{
							if (m_pModelinfo->stIRFilter[pStepInfo->StepList[nStepIdx].nTestItemCnt].stIRFilterResult.nResult != nTestItem_Master)
								nMasterResult = FALSE;
						}
						break;
					//case TIID_PatternNoise:
					//	if (nTestItem_Master != enEachTestSel_Skip)
					//	{
					//		if (m_pModelinfo->stPatternNoise[pStepInfo->StepList[nStepIdx].nTestItemCnt].stPatternNoiseResult.nResult != nTestItem_Master)
					//			nMasterResult = FALSE;
					//	}
					//	break;
					default:
						break;
					}
				}

				
			}
		}
	}


	return nMasterResult;
}

void CDlg_MasterTest::OnSetUI_TotalTestStatus(__in UINT nStatus)
{

	switch (nStatus)
	{
	case enTotalTestStatus_Standby:
		m_stTotalTestStatus.SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_stTotalTestStatus.SetColorStyle(CVGStatic::ColorStyle_Green);
		m_stTotalTestStatus.SetFont_Gdip(L"Arial", 35.0F);
		m_stTotalTestStatus.SetText(g_szTotalTestStatus[nStatus]);
		break;
	case enTotalTestStatus_MasterCheck:
		m_stTotalTestStatus.SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_stTotalTestStatus.SetColorStyle(CVGStatic::ColorStyle_Yellow);
		m_stTotalTestStatus.SetFont_Gdip(L"Arial", 35.0F);
		m_stTotalTestStatus.SetText(g_szTotalTestStatus[nStatus]);
		break;
	case enTotalTestStatus_MasterOK:
		m_stTotalTestStatus.SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_stTotalTestStatus.SetColorStyle(CVGStatic::ColorStyle_Blue);
		m_stTotalTestStatus.SetFont_Gdip(L"Arial", 35.0F);
		m_stTotalTestStatus.SetText(g_szTotalTestStatus[nStatus]);
		break;
	case enTotalTestStatus_MasterNG:
		m_stTotalTestStatus.SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_stTotalTestStatus.SetColorStyle(CVGStatic::ColorStyle_Red);
		m_stTotalTestStatus.SetFont_Gdip(L"Arial", 35.0F);
		m_stTotalTestStatus.SetText(g_szTotalTestStatus[nStatus]);
		break;
	case enTotalTestStatus_UserStop:
		m_stTotalTestStatus.SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_stTotalTestStatus.SetColorStyle(CVGStatic::ColorStyle_Red);
		m_stTotalTestStatus.SetFont_Gdip(L"Arial", 35.0F);
		m_stTotalTestStatus.SetText(g_szTotalTestStatus[nStatus]);
		break;
	default:
		break;
	}
}

void CDlg_MasterTest::OnSetUI_EachTestStatus(__in UINT nStatus, __in UINT nTestItem, __in UINT nTestItemCnt)
{
	CString szResultData[2] = { _T("NG"), _T("OK") };

	switch (nTestItem)
	{
	case TIID_Current:
		m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stCurrent[nTestItemCnt].stCurrentResult.nResult]);
		break;
	//case TIID_OperationMode:
	//	m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stOperMode[nTestItemCnt].nResult]);
	//	break;
	case TIID_CenterPoint:
		m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stCenterPoint[nTestItemCnt].stCenterPointResult.nResult]);
		break;
	case TIID_Rotation:
		m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stRotate[nTestItemCnt].stRotateResult.nResult]);
		break;
	//case TIID_SFR:
	//	m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stSFR[nTestItemCnt].stSFRResult.nResult]);
	//	break;
	case TIID_EIAJ:
		m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stEIAJ[nTestItemCnt].stEIAJData.nResult]);
		break;
	case TIID_FOV:
		m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stAngle[nTestItemCnt].stAngleResult.nResult]);
		break;
	case TIID_Color:
		m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stColor[nTestItemCnt].stColorResult.nResult]);
		break;
	case TIID_Reverse:
		m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stReverse[nTestItemCnt].stReverseResult.nResult]);
		break;
	case TIID_Brightness:
		m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stBrightness[nTestItemCnt].stBrightnessResult.nResult]);
		break;
	case TIID_IRFilter:
		m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stIRFilter[nTestItemCnt].stIRFilterResult.nResult]);
		break;
	//case TIID_PatternNoise:
	//	m_stEachTestStatus[nTestItem].SetText(szResultData[m_pModelinfo->stPatternNoise[nTestItemCnt].stPatternNoiseResult.nResult]);
	//	break;
	default:
		break;
	}
}

void CDlg_MasterTest::OnSetUI_EachTestReset()
{
	for (int i = 0; i < TIID_MaxEnum; i++)
		m_stEachTestStatus[i].SetText(_T(""));
}


void CDlg_MasterTest::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	isThreadRunning_Master = false;
	DestroyThread();

	CDialogEx::OnClose();
}
