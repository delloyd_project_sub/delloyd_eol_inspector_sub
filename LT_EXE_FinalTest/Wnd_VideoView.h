﻿#ifndef Wnd_VideoView_h__
#define Wnd_VideoView_h__

#pragma once



#include "cv.h"
#include "highgui.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"

typedef enum enOverlayMode_2
{
	OvrMode_LINE,		// 직선
	OvrMode_RECTANGLE,	// 직사각형
	OvrMode_CIRCLE,		// 원
	OvrMode_TXT,		// 글씨
	OvrMode_MaxNum,
};


// CWnd_VideoView

class CWnd_VideoView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_VideoView)

public:
	CWnd_VideoView();
	virtual ~CWnd_VideoView();
	
	BOOL m_bTestmode = TRUE;
	CVGStatic		m_stImage;

	void SetMasueControl (__in BOOL bMasueMode)
	{
		m_bMasueModeUse = bMasueMode;
	};

	void SetModelInfo (__in ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	};

	void SetImagePath (__in CString szImagePath)
	{
		m_strImagePath = szImagePath;
	};

	void SetFrameImageBuffer(__in IplImage* pImage)
	{
		if (pImage == NULL)
			return;

		if (m_pFrameImage == NULL)
		{
			m_pFrameImage = cvCreateImage(cvSize(pImage->width, pImage->height), IPL_DEPTH_8U, 3);

			CRect rc;
			GetClientRect(&rc);
			OnSize(0, rc.Width(), rc.Height());
		}
		else
		{
			if (pImage->imageSize != m_pFrameImage->imageSize)
			{
				if (m_pFrameImage != NULL)
				{
					cvReleaseImage(&m_pFrameImage);
					m_pFrameImage = NULL;
				}

				m_pFrameImage = cvCreateImage(cvSize(pImage->width, pImage->height), IPL_DEPTH_8U, 3);

				CRect rc;
				GetClientRect(&rc);
				OnSize(0, rc.Width(), rc.Height());
			}
		}

		cvCopy(pImage, m_pFrameImage);

		m_bflg_ImageData = TRUE;
	};

	void SetEmptyImageBuffer()
	{
// 		if (m_pFrameImage != NULL)
// 		{
// 			cvReleaseImage(&m_pFrameImage);
// 			m_pFrameImage = NULL; 
		m_bflg_ImageData = FALSE;
	
	};

	IplImage* GetFrameImageBuffer()
	{
		return m_pFrameImage;
	};

	IplImage* GetPicImageBuffer()
	{
		return m_pPicImage;
	};

protected:

	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnLButtonDown		(UINT nFlags, CPoint point);
	afx_msg void	OnLButtonUp			(UINT nFlags, CPoint point);
	afx_msg void	OnMouseMove			(UINT nFlags, CPoint point);
	afx_msg BOOL	OnSetCursor			(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);

	BOOL			m_bMasueModeUse;

	ST_ModelInfo*	m_pstModelInfo;

	CFont			m_font;

	CString			m_strImagePath;

	IplImage		*m_pFrameImage		= NULL;
	IplImage		*m_pPicImage		= NULL;
	BITMAPINFO		m_BitmapInfo;

	BOOL			m_bflg_MauseMode	= 0;
	BOOL			m_bflg_ImageData	= FALSE;
	
	int				m_iROInum			= -1;
	UINT			m_nROICntMax		= 0;
	UINT			m_nMouseEdit		= 0;
	CPoint			m_ptEdit;
	CPoint			m_ptInit;

	//	타이머
	HANDLE			m_hTimerViewCheck	= NULL;
	HANDLE			m_hTimerQueue		= NULL;

	static VOID CALLBACK TimerRoutineViewCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);

	void	CreateTimerQueue_Mon	();
	void	DeleteTimerQueue_Mon	();
	void	CreateTimerViewCheck	();
	void	DeleteTimerViewCheck	();

	void	OnImageView				();

	void	SetROIData				(UINT nROI, CRect rtROI);
	CRect	GetROIData				(UINT nROI);

	// EIAJ Use Function
	int		Overlay_PeakData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI);
	int		Overlay_StandarData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI);
	int		Overlay_StandardPeakData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI);

public:
	void	PicCenterPoint			(IplImage* lpImage);
	void	PicRotate				(IplImage* lpImage);
	void	PicSFR					(IplImage* lpImage);
	void	PicAngle				(IplImage* lpImage);
	void	PicBrightness			(IplImage* lpImage);
	void	PicParticle				(IplImage* lpImage);
	void	PicDefectPixel			(IplImage * lpImage);
	void	PicCurrent				(IplImage* lpImage);
	void	PicColor				(IplImage* lpImage);
	void	PicFFT					(IplImage* lpImage);
	void	PicOperMode				(IplImage* lpImage);

	void	PicEIAJ					(IplImage* lpImage);
	void	PicReverse				(IplImage* lpImage);
	void	PicPatternNoise			(IplImage* lpImage);
	void	PicLED					(IplImage* lpImage);
	void	PicReset				(IplImage* lpImage);
	void	PicIRFilter				(IplImage* lpImage);

	void	OnDisplayPIC			(IplImage* lpImage, UINT nMode, int iLeft, int iTop, int iRight, int iBottom, int iRed, int iGreen, int iBlue, int iTickness, float fFontSize = 1, CString strText = _T(""));
	void Overlay_Process(__inout IplImage* lpImage, __in enOverlayMode_2 enMode, __in CRect rtROI, __in COLORREF clrLineColor = RGB(255, 200, 0), __in int iLineSize = 1, __in double dbFontSize = 1.0, __in CString szText = _T(""));
};


#endif // Wnd_VideoView_h__
