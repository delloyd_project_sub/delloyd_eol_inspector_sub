﻿#ifndef List_BrightnessOp_h__
#define List_BrightnessOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_BrightnessOp
{
	BrOp_Object = 0,
	BrOp_PosX,
	BrOp_PosY,
	BrOp_Width,
	BrOp_Height,
	BrOp_MinSpec,
	BrOp_MaxSpec,
	BrOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_BrightnessOp[] =
{
	_T(""),
	_T("CenterX"),
	_T("CenterY"),
	_T("Width"),
	_T("Height"),
	_T("MinSpec"),
	_T("MaxSpec"),
	NULL 
};

typedef enum enListItemNum_BrightnessOp
{
	BrOp_ItemNum = ROI_BR_Max,
};

static LPCTSTR	g_lpszItem_BrightnessOp[] =
{
	NULL
};

const int	iListAglin_BrightnessOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER, 
};

const int	iHeaderWidth_BrightnessOp[] =
{
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
};
// List_RotateInfo

class CList_BrightnessOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_BrightnessOp)

public:
	CList_BrightnessOp();
	virtual ~CList_BrightnessOp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Brightenss(ST_LT_TI_Brightness* pstBrightness)
	{
		if (pstBrightness == NULL)
			return;

		m_pstBrightness = pstBrightness;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth  = nWidth;
		m_nHeight = nHeight;
	};

protected:

	ST_LT_TI_Brightness*  m_pstBrightness;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);
	
	afx_msg void	OnEnKillFocusEdit		();
	//afx_msg void	OnEnKillFocusCombo		();
	//afx_msg void	OnEnSelectFocusCombo	();
};

#endif // List_RotateInfo_h__
