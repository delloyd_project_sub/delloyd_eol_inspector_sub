// Dlg_CenterPtTunning.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_CenterPtTunning.h"
#include "afxdialogex.h"

CWinThread* pThread_center = NULL;

static volatile bool isThreadRunning_center;

UINT MyThread_center(LPVOID ipParam)
{
	CDlg_CenterPtTunning* pClass = (CDlg_CenterPtTunning*)ipParam;
	int iReturn = pClass->ThreadFunction();
	return 0L;
}

#define		IDC_WND_IMAGEVIEW		1000
#define		IDC_BN_UP				2000
#define		IDC_BN_DOWN				3000
#define		IDC_BN_LEFT				4000
#define		IDC_BN_RIGHT			5000
#define		IDC_BN_OK				6000
#define		IDC_BN_INIT				7000
#define		IDC_ED_HOR				8000
#define		IDC_ED_VER				9000

// CDlg_CenterPtTunning 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_CenterPtTunning, CDialogEx)

CDlg_CenterPtTunning::CDlg_CenterPtTunning(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_CenterPtTunning::IDD, pParent)
{
	m_iOffsetXSum = 0;
	m_iOffsetYSum = 0;
	m_nResult = 0;
	m_pDevice = NULL;

	VERIFY(m_font.CreateFont(
		30,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	for (UINT nIdx = 0; nIdx < DI_NotUseBit_Max; nIdx++)
		m_bFlag_Butten[nIdx] = TRUE;
}

CDlg_CenterPtTunning::~CDlg_CenterPtTunning()
{
	//KillTimer(100);

	isThreadRunning_center = false;
	DestroyThread();
	KillTimer(99);
}



void CDlg_CenterPtTunning::CreateThread(UINT _method)
{
	if (pThread_center != NULL)
	{
		return;
	}

	pThread_center = AfxBeginThread(MyThread_center, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);

	if (pThread_center == NULL)
	{
		//cout<<"Fail to create camera thread!!";
	}

	pThread_center->m_bAutoDelete = FALSE;
	pThread_center->ResumeThread();
}

bool CDlg_CenterPtTunning::DestroyThread()
{
	if (NULL != pThread_center)
	{
		DWORD dwResult = ::WaitForSingleObject(pThread_center->m_hThread, INFINITE);

		if (dwResult == WAIT_TIMEOUT)
		{
			//cout<<"time out!"<<endl;
		}
		else if (dwResult == WAIT_OBJECT_0)
		{
			//cout<<"Thread END"<<endl;
		}

		delete pThread_center;

		pThread_center = NULL;
	}
	return true;
}

int CDlg_CenterPtTunning::ThreadFunction()
{
	while (isThreadRunning_center)
	{
		if (!isThreadRunning_center)
			break;

		Sleep(33);

		if (IsCameraConnect(m_nViewChannel, m_pModelinfo->nGrabType))
		{
			LPBYTE pFrameImage = NULL;
			IplImage *pImage = NULL;

			DWORD dwWidth = 0;
			DWORD dwHeight = 0;
			UINT nChannel = 0;

			pFrameImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);

			if (pFrameImage != NULL)
			{
				pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

				if (m_pModelinfo->nGrabType == GrabType_NTSC)
				{
					for (int y = 0; y < dwHeight; y++)
					{
						for (int x = 0; x < dwWidth; x++)
						{
							pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
							pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
							pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
						}
					}
				}

				m_wndVideoView.SetFrameImageBuffer(pImage);

				// Center Point Detect Function
				m_ImageTest.OnSetCameraType(m_pModelinfo->nCameraType);

				m_ImageTest.OnTestProcessCenterPoint(
					pImage,
					&m_pModelinfo->stCenterPoint[m_pModelinfo->nTestCnt].stCenterPointOpt,
					m_pModelinfo->stCenterPoint[m_pModelinfo->nTestCnt].stCenterPointResult);

				cvReleaseImage(&pImage);
			}
		}
	}

	return 0;
}

void CDlg_CenterPtTunning::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}



BEGIN_MESSAGE_MAP(CDlg_CenterPtTunning, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BN_OK, OnBnClickedBnOK)
	ON_BN_CLICKED(IDC_BN_UP, OnBnClickedBnUp)
	ON_BN_CLICKED(IDC_BN_DOWN, OnBnClickedBnDown)
	ON_BN_CLICKED(IDC_BN_LEFT, OnBnClickedBnLeft)
	ON_BN_CLICKED(IDC_BN_RIGHT, OnBnClickedBnRight)
	ON_BN_CLICKED(IDC_BN_INIT, OnBnClickedBnInit)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CDlg_CenterPtTunning 메시지 처리기입니다.


int CDlg_CenterPtTunning::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_wndVideoView.SetOwner(GetParent());
	m_wndVideoView.SetModelInfo(m_pModelinfo);
	m_wndVideoView.Create(NULL, _T(""), dwStyle, rectDummy, this, IDC_WND_IMAGEVIEW);

	m_stAxisX.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_stAxisX.SetColorStyle(CVGStatic::ColorStyle_Black);
	m_stAxisX.SetFont_Gdip(L"Arial", 15.0F);
	m_stAxisX.Create(_T("Horizon"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_stAxisY.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_stAxisY.SetColorStyle(CVGStatic::ColorStyle_Black);
	m_stAxisY.SetFont_Gdip(L"Arial", 15.0F);
	m_stAxisY.Create(_T("Vertical"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_bn_Up.Create(_T("↑"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_UP);
	m_bn_Down.Create(_T("↓"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_DOWN);
	m_bn_Left.Create(_T("←"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_LEFT);
	m_bn_Right.Create(_T("→"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_RIGHT);
	m_bn_OK.Create(_T("OK"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_OK);
	m_bn_Init.Create(_T("Init"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_INIT);

	m_Ed_Hor.Create(dwStyle | ES_LEFT | WS_BORDER, rectDummy, this, IDC_ED_HOR);
	m_Ed_Hor.SetFont(&m_font);
	m_Ed_Hor.SetWindowText(_T("0"));

	m_Ed_Ver.Create(dwStyle | ES_LEFT | WS_BORDER, rectDummy, this, IDC_ED_VER);
	m_Ed_Ver.SetFont(&m_font);
	m_Ed_Ver.SetWindowText(_T("0"));

	m_bn_Up.SetMouseCursorHand();
	m_bn_Down.SetMouseCursorHand();
	m_bn_Left.SetMouseCursorHand();
	m_bn_Right.SetMouseCursorHand();
	m_bn_OK.SetMouseCursorHand();
	m_bn_Init.SetMouseCursorHand();

	m_bn_Up.SetFont(&m_font);
	m_bn_Down.SetFont(&m_font);
	m_bn_Left.SetFont(&m_font);
	m_bn_Right.SetFont(&m_font);
	m_bn_OK.SetFont(&m_font);
	m_bn_Init.SetFont(&m_font);

	SetTimer(99, 100, NULL);
	//SetTimer(100, 33, NULL);

	isThreadRunning_center = true;
	CreateThread(isThreadRunning_center);

	return 0;
}


void CDlg_CenterPtTunning::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	int iSpacing = 1;
	int iCateSpacing = 10;
	int iMagin = 10;
	int iLeft = iMagin;
	int iTop = iMagin;
	int iWidth = cx - (iMagin * 2);
	int iHeight = cy - (iMagin * 2);

	m_wndVideoView.MoveWindow(iLeft, iTop, iWidth, iHeight * 5 / 6);

	iTop += iHeight * 5 / 6 + iSpacing;
	m_stAxisX.MoveWindow(iLeft, iTop, iWidth / 4, iHeight / 12);

	iLeft += iWidth / 4 + iSpacing;
	m_bn_Left.MoveWindow(iLeft, iTop, iWidth / 4, iHeight / 12);

	iLeft += iWidth / 4 + iSpacing;
	m_bn_Right.MoveWindow(iLeft, iTop, iWidth / 4, iHeight / 12);

	iLeft = iMagin;
	iTop += iHeight / 12 + iSpacing;
	m_stAxisY.MoveWindow(iLeft, iTop, iWidth / 4, iHeight / 12);

	iLeft += iWidth / 4 + iSpacing;
	m_bn_Up.MoveWindow(iLeft, iTop, iWidth / 4, iHeight / 12);

	iLeft += iWidth / 4 + iSpacing;
	m_bn_Down.MoveWindow(iLeft, iTop, iWidth / 4, iHeight / 12);

	iTop = iMagin;
	iTop += iHeight * 5 / 6 + iSpacing;
	iLeft += iWidth / 4 + 5;
	m_bn_Init.MoveWindow(iLeft, iTop, iWidth / 4 - 6, iHeight / 12);

	iTop += iHeight / 12 + iSpacing;
	m_bn_OK.MoveWindow(iLeft, iTop, iWidth / 4 - 6, iHeight / 12);
}


void CDlg_CenterPtTunning::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	lpMMI->ptMaxTrackSize.x = 1000;
	lpMMI->ptMaxTrackSize.y = 800;

	lpMMI->ptMinTrackSize.x = 1000;
	lpMMI->ptMinTrackSize.y = 800;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


void CDlg_CenterPtTunning::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	LPBYTE pFrameImage = NULL;
	IplImage *pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	switch (nIDEvent)
	{
	case 99:

		if (m_pDevice->DigitalIOCtrl.AXTState() == TRUE)
		{
			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StartBtn))
			{
				if (m_bFlag_Butten[DI_StartBtn] == FALSE)
				{
					m_bFlag_Butten[DI_StartBtn] = TRUE;

					OnBnClickedBnOK();

					m_bFlag_Butten[DI_StartBtn] = FALSE;
				}
			}

			//if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StopBtn))
			//{
			//	if (m_bFlag_Butten[DI_StopBtn] == FALSE)
			//	{
			//		m_bFlag_Butten[DI_StopBtn] = TRUE;

			//		

			//		m_bFlag_Butten[DI_StopBtn] = FALSE;
			//	}
			//}
		}
		break;

	case 100:

		if (IsCameraConnect(m_nViewChannel, m_pModelinfo->nGrabType))
		{
			pFrameImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);

			if (pFrameImage != NULL)
			{
				pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

				if (m_pModelinfo->nGrabType == GrabType_NTSC)
				{
					for (int y = 0; y < dwHeight; y++)
					{
						for (int x = 0; x < dwWidth; x++)
						{
							pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
							pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
							pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
						}
					}
				}

				m_wndVideoView.SetFrameImageBuffer(pImage);

				// Center Point Detect Function
				m_ImageTest.OnSetCameraType(m_pModelinfo->nCameraType);

				m_ImageTest.OnTestProcessCenterPoint(
					pImage,
					&m_pModelinfo->stCenterPoint[m_pModelinfo->nTestCnt].stCenterPointOpt,
					m_pModelinfo->stCenterPoint[m_pModelinfo->nTestCnt].stCenterPointResult);

				cvReleaseImage(&pImage);
			}
		}

		break;

	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_CenterPtTunning::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreCreateWindow(cs);
}


BOOL CDlg_CenterPtTunning::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CDlg_CenterPtTunning::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_iOffsetXSum = 0;
	m_iOffsetYSum = 0;
	OpticalCenter_Adj_Horizon(0, 0);
	OpticalCenter_Adj_Vertical(0, 0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

 
void CDlg_CenterPtTunning::OnBnClickedBnUp()
{
	int iBuf = m_iOffsetYSum + -2;
	if (OpticalCenter_Adj_Vertical(0, iBuf) == RCA_OK)
	{
		m_iOffsetYSum = iBuf;
	}
}

void CDlg_CenterPtTunning::OnBnClickedBnDown()
{
	int iBuf = m_iOffsetYSum + 2;
	if (OpticalCenter_Adj_Vertical(0, iBuf) == RCA_OK)
	{
		m_iOffsetYSum = iBuf;
	}
}

void CDlg_CenterPtTunning::OnBnClickedBnLeft()
{
	int iBuf = m_iOffsetXSum + -2;
	if (OpticalCenter_Adj_Horizon(0, iBuf) == RCA_OK)
	{
		m_iOffsetXSum = iBuf;
	}
}

void CDlg_CenterPtTunning::OnBnClickedBnRight()
{
	int iBuf = m_iOffsetXSum + 2;
	if (OpticalCenter_Adj_Horizon(0, iBuf) == RCA_OK)
	{
		m_iOffsetXSum = iBuf;
	}
}

void CDlg_CenterPtTunning::OnBnClickedBnOK()
{
	// Serial Flash All Write
	if (OpticalCenter_Adj_AllWrite(0) != RCA_OK)
	{
		AfxMessageBox(_T("Write Fail!"));
	}

	m_pModelinfo->stCenterPoint[m_pModelinfo->nTestCnt].stCenterPointResult.iWriteX = m_iOffsetXSum;
	m_pModelinfo->stCenterPoint[m_pModelinfo->nTestCnt].stCenterPointResult.iWriteY = m_iOffsetYSum;

	//KillTimer(100);
	isThreadRunning_center = false;
	DestroyThread();

	KillTimer(99);

	Sleep(500);

	OnOK();
}


void CDlg_CenterPtTunning::OnBnClickedBnInit()
{
	m_iOffsetXSum = 0;
	m_iOffsetYSum = 0;
	OpticalCenter_Adj_Horizon(0, 0);
	OpticalCenter_Adj_Vertical(0, 0);
}

BOOL CDlg_CenterPtTunning::IsCameraConnect(__in UINT nCh, __in UINT nGrabType)
{
	switch (nGrabType)
	{
	case GrabType_NTSC:

		if (m_pDevice->Cat3DCtrl.IsConnect() == FALSE)
			return FALSE;

		if (m_pDevice->Cat3DCtrl.GetStatus(nCh) == FALSE)
			return FALSE;

		break;

	default:
		break;
	}

	return TRUE;
}

LPBYTE CDlg_CenterPtTunning::GetImageBuffer(__in UINT nViewCh, __in UINT nGrabType, __out DWORD& dwWidth, __out DWORD& dwHeight, __out UINT& nChannel)
{
	// 카메라 연결 상태
	if (IsCameraConnect(nViewCh, nGrabType) == FALSE)
		return NULL;

	// Result Buffer
	LPBYTE pRGBDATA = NULL;

	// NTSC Buffer
	ST_VideoRGB_NTSC* pNTSCRGB = NULL;

	if (nGrabType == GrabType_NTSC)
	{
		pNTSCRGB = m_pDevice->Cat3DCtrl.GetRecvRGBData(nViewCh);
		pRGBDATA = (LPBYTE)((m_pDevice->Cat3DCtrl.GetRecvRGBData(nViewCh))->m_pMatRGB[0]);

		dwWidth = pNTSCRGB->m_dwWidth;
		dwHeight = pNTSCRGB->m_dwHeight;
		nChannel = 4;
	}
	
	return pRGBDATA;
}

UINT CDlg_CenterPtTunning::OpticalCenter_Adj_initial(__in UINT nCh)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffsetX[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x14, 0x00, 0x00, 0x29 };
	BYTE WriteDataOffsetY[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x16, 0x00, 0x00, 0x2b };

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffsetX) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffsetY) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	// Serial NOR Flash All Write
// 	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
// 	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };
// 
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 8, FlachAllWriteData1) == TRUE)
// 	{
// 		if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 4, FlachAllWriteData2) == TRUE)
// 		{
// 			BYTE ReadCheck[256] = { 0x0, };
// 			BYTE ConfirmCheck[5] = { 0xff, 0xff, 0xff, 0xff, 0xff };
// 			if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 256, ReadCheck) == TRUE)
// 			{
// 				int iCheckFailCnt = 0;
// 				for (int i = 0; i < 5; i++) // Check Data 5 Count
// 				{
// 					if (ReadCheck[i] != ConfirmCheck[i])
// 					{
// 						iCheckFailCnt++;
// 					}
// 				}
// 
// 				if (iCheckFailCnt > 0)
// 				{
// 					return nTestResult = RCA_NG;
// 				}
// 			}
// 			else
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	return nTestResult;
}

UINT CDlg_CenterPtTunning::OpticalCenter_Adj(__in UINT nCh, __in int iCenterX, __in int iCenterY, __in int iTargerX, __in int iTargerY)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffsetX[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x14, };
	BYTE WriteDataOffsetY[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x16, };

	int iZeroOffsetX = 41;
	int iZeroOffsetY = 43;

	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC_HV[2] = { 0x00, };
	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	int iMoveX = 0;
	int iMoveY = 0;

	int iReadOffsetX = 0;
	int iReadOffsetY = 0;

	iMoveX = iCenterX - iTargerX;
	iMoveX /= 2;
	iMoveX *= 2;

	iMoveY = iCenterY - iTargerY;
	iMoveY /= 2;
	iMoveY *= 2;

	// Horizon
	if (iMoveX >= 0) // 양수
	{
		WriteDataCRC_HV[0] = iZeroOffsetX + iMoveX;

		WriteDataOffsetX[6] = (BYTE)iMoveX;
		WriteDataOffsetX[7] = WriteDataMark[0];
	}
	else // 음수
	{
		WriteDataCRC_HV[0] = (iZeroOffsetX + iMoveX) - 1;

		WriteDataOffsetX[6] = (BYTE)(256 + iMoveX);
		WriteDataOffsetX[7] = WriteDataMark[1];
	}

	WriteDataOffsetX[8] = WriteDataCRC_HV[0];

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18, 1, 0x0a, 9, WriteDataOffsetX) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	// Vertical
	if (iMoveY >= 0) // 양수
	{
		WriteDataCRC_HV[1] = iZeroOffsetY + iMoveY;

		WriteDataOffsetY[6] = (BYTE)iMoveY;
		WriteDataOffsetY[7] = WriteDataMark[0];
	}
	else // 음수
	{
		WriteDataCRC_HV[1] = (iZeroOffsetY + iMoveY) - 1;

		WriteDataOffsetY[6] = (BYTE)(256 + iMoveY);
		WriteDataOffsetY[7] = WriteDataMark[1];
	}

	WriteDataOffsetY[8] = WriteDataCRC_HV[1];

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18, 1, 0x0a, 9, WriteDataOffsetY) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18, 1, 0x0a, 256, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	// Serial NOR Flash All Write
	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18, 1, 0x0a, 8, FlachAllWriteData1) == TRUE)
// 	{
// 		if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18, 1, 0x0a, 4, FlachAllWriteData2) == TRUE)
// 		{
// 			BYTE ReadCheck[256] = { 0x0, };
// 			BYTE ConfirmCheck[5] = { 0xff, 0xff, 0xff, 0xff, 0xff };
// 			if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18, 1, 0x0a, 256, ReadCheck) == TRUE)
// 			{
// 				int iCheckFailCnt = 0;
// 				for (int i = 0; i < 5; i++) // Check Data 5 Count
// 				{
// 					if (ReadCheck[i] != ConfirmCheck[i])
// 					{
// 						iCheckFailCnt++;
// 					}
// 				}
// 
// 				if (iCheckFailCnt > 0)
// 				{
// 					return nTestResult = RCA_NG;
// 				}
// 			}
// 			else
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	return nTestResult;
}


UINT CDlg_CenterPtTunning::OpticalCenter_Adj(__in enAdjMode nAdjMode, __in UINT nCh, __in int iOffset)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffset[9] = { 0x01, 0x07, 0x02, 0x01, 0x00,  };
	
	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC = 0x00;
	
	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	int iZeroOffset = 0;
	
	switch (nAdjMode)
	{
	case CDlg_CenterPtTunning::eAdjMode_Up:
		WriteDataOffset[5] = 0x16;
		iZeroOffset = 43;

		WriteDataCRC = (iZeroOffset + iOffset) - 1;

		WriteDataOffset[6] = (BYTE)(256 + iOffset);
		WriteDataOffset[7] = WriteDataMark[1];
		WriteDataOffset[8] = WriteDataCRC;
		break;

	case CDlg_CenterPtTunning::eAdjMode_Down:
		WriteDataOffset[5] = 0x16;
		iZeroOffset = 43;

		WriteDataCRC = iZeroOffset + iOffset;

		WriteDataOffset[6] = (BYTE)iOffset;
		WriteDataOffset[7] = WriteDataMark[0];
		WriteDataOffset[8] = WriteDataCRC;
		break;

	case CDlg_CenterPtTunning::eAdjMode_Left:
		WriteDataOffset[5] = 0x14;
		iZeroOffset = 41;

		WriteDataCRC = (iZeroOffset + iOffset) - 1;

		WriteDataOffset[6] = (BYTE)(256 + iOffset);
		WriteDataOffset[7] = WriteDataMark[1];
		WriteDataOffset[8] = WriteDataCRC;
		break;

	case CDlg_CenterPtTunning::eAdjMode_Right:
		WriteDataOffset[5] = 0x14;
		iZeroOffset = 41;

		WriteDataCRC = iZeroOffset + iOffset;

		WriteDataOffset[6] = (BYTE)iOffset;
		WriteDataOffset[7] = WriteDataMark[0];
		WriteDataOffset[8] = WriteDataCRC;
		break;

	default:
		break;
	}

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffset) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 5, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// 			if (iCheckFailCnt > 0)
// 			{
// 				//return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}

	// Serial NOR Flash All Write
	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, FlachAllWriteData1) == TRUE)
// 	{
// 		if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x05, 4, FlachAllWriteData2) == TRUE)
// 		{
// 			BYTE ReadCheck[256] = { 0x0, };
// 			BYTE ConfirmCheck[5] = { 0xff, 0xff, 0xff, 0xff, 0xff };
// 			if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 5, ReadCheck) == TRUE)
// 			{
// 				int iCheckFailCnt = 0;
// 				for (int i = 0; i < 5; i++) // Check Data 5 Count
// 				{
// 					if (ReadCheck[i] != ConfirmCheck[i])
// 					{
// 						iCheckFailCnt++;
// 					}
// 				}
// 
// 				if (iCheckFailCnt > 0)
// 				{
// 					//return nTestResult = RCA_NG;
// 				}
// 			}
// 			else
// 			{
// 				return nTestResult = RCA_NG;
// 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}


	return nTestResult;
}

UINT CDlg_CenterPtTunning::OpticalCenter_Adj_Horizon(__in UINT nCh, __in int iOffset)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffset[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x14};

	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC = 0x00;

	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	if (iOffset < 0)
	{
		WriteDataCRC = (41 + iOffset) - 1;
		WriteDataOffset[6] = (BYTE)(256 + iOffset);
		WriteDataOffset[7] = WriteDataMark[1];
		WriteDataOffset[8] = WriteDataCRC;
	} 
	else
	{
		WriteDataCRC = (41 + iOffset);
		WriteDataOffset[6] = (BYTE)iOffset;
		WriteDataOffset[7] = WriteDataMark[0];
		WriteDataOffset[8] = WriteDataCRC;
	}
	
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffset) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 5, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// // 			if (iCheckFailCnt > 0)
// // 			{
// // 				return nTestResult = RCA_NG;
// // 			}
// 			 
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}


	return nTestResult;
}

UINT CDlg_CenterPtTunning::OpticalCenter_Adj_Vertical(__in UINT nCh, __in int iOffset)
{
	UINT nTestResult = RCA_OK;

	BYTE WriteDataOffset[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x16 };

	// 0:Hor, 1:Ver
	BYTE  WriteDataCRC = 0x01;

	// 0:+, 1:- 
	BYTE  WriteDataMark[2] = { 0x00, 0xff };

	if (iOffset < 0)
	{
		WriteDataCRC = (43 + iOffset) - 1;
		WriteDataOffset[6] = (BYTE)(256 + iOffset);
		WriteDataOffset[7] = WriteDataMark[1];
		WriteDataOffset[8] = WriteDataCRC;
	}
	else
	{
		WriteDataCRC = (43 + iOffset);
		WriteDataOffset[6] = (BYTE)iOffset;
		WriteDataOffset[7] = WriteDataMark[0];
		WriteDataOffset[8] = WriteDataCRC;
	}

// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x0a, 9, WriteDataOffset) == TRUE)
// 	{
// 		BYTE ReadCheck[256] = { 0x0, };
// 		BYTE ConfirmCheck[5] = { 0x05, 0x01, 0x02, 0x01, 0x09 };
// 		if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x0a, 5, ReadCheck) == TRUE)
// 		{
// 			int iCheckFailCnt = 0;
// 			for (int i = 0; i < 5; i++) // Check Data 5 Count
// 			{
// 				if (ReadCheck[i] != ConfirmCheck[i])
// 				{
// 					iCheckFailCnt++;
// 				}
// 			}
// 
// // 			if (iCheckFailCnt > 0)
// // 			{
// // 				return nTestResult = RCA_NG;
// // 			}
// 		}
// 		else
// 		{
// 			return nTestResult = RCA_NG;
// 		}
// 	}
// 	else
// 	{
// 		return nTestResult = RCA_NG;
// 	}


	return nTestResult;
}


UINT CDlg_CenterPtTunning::OpticalCenter_Adj_AllWrite(__in UINT nCh)
{
	UINT nTestResult = RCA_OK;

	// Serialize Disable
	BYTE Serialize_Disable[1] = { 0x43 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x40 << 1, 1, 0x04, 1, Serialize_Disable) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 	Sleep(1000);
// 
// 	// Default Set 0fps
// 	BYTE Default_Set_0fps_1[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x00, 0x00, 0x13 };
// 	BYTE Default_Set_0fps_2[8] = { 0x01, 0x06, 0x02, 0x01, 0x00, 0x01, 0x00, 0x14 };
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, Default_Set_0fps_1) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
// 	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, Default_Set_0fps_2) == FALSE)
// 	{
// 		return nTestResult = RCA_NG;
// 	}
// 
//  	// Serial NOR Flash All Write
//   	BYTE FlachAllWriteData1[8] = { 0x01, 0x06, 0x00, 0x57, 0x52, 0x45, 0x4e, 0x4c };
//   	BYTE FlachAllWriteData2[4] = { 0x01, 0x02, 0x05, 0x0d };
//   
//   	if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x09, 8, FlachAllWriteData1) == TRUE)
//   	{
//   		if (m_pDevice->DAQCtrl.I2C_Write(nCh, 0x18 << 1, 1, 0x05, 4, FlachAllWriteData2) == TRUE)
//   		{
//   			BYTE ReadCheck[256] = { 0x0, };
//   			BYTE ConfirmCheck[5] = { 0x0, };
//   			if (m_pDevice->DAQCtrl.I2C_Read(nCh, 0x18 << 1, 1, 0x09, 256, ReadCheck) == TRUE)
//   			{
//   				int iCheckFailCnt = 0;
//   				for (int i = 0; i < 5; i++) // Check Data 5 Count
//   				{
//   					if (ReadCheck[i] != ConfirmCheck[i])
//   					{
//   						iCheckFailCnt++;
//   					}
//   				}
//   
//   // 				if (iCheckFailCnt > 0)
//  //	{
//  //		return nTestResult = RCA_NG;
//  //	}
//   			}
//   			else
//   			{
//   				return nTestResult = RCA_NG;
//   			}
//   		}
//   	}
//   	else
//   	{
//   		return nTestResult = RCA_NG;
//   	}

	
	return nTestResult;
}


void CDlg_CenterPtTunning::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//KillTimer(100);
	isThreadRunning_center = false;
	DestroyThread();

	KillTimer(99);
	CDialogEx::OnClose();
}
