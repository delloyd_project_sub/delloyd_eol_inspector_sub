﻿//*****************************************************************************
// Filename	: Wnd_WorklistParticle.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistDefectPixel_h__
#define Wnd_WorklistDefectPixel_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_DefectPixel.h"

//=============================================================================
// Wnd_WorklistParticle
//=============================================================================
class CWnd_WorklistDefectPixel : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistDefectPixel)

public:
	CWnd_WorklistDefectPixel();
	virtual ~CWnd_WorklistDefectPixel();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	CList_Work_DefectPixel		m_list_DefectPixel[TICnt_DefectPixel];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistParticle_h__


