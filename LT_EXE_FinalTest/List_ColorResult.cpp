﻿// List_ColorResult.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ColorResult.h"

// CList_ColorResult



IMPLEMENT_DYNAMIC(CList_ColorResult, CListCtrl)

CList_ColorResult::CList_ColorResult()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	
	m_nWidth	= 640;
	m_nHeight	= 480;

	m_pstColor	= NULL;
}

CList_ColorResult::~CList_ColorResult()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_ColorResult, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_ColorResult::OnNMClick)
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CList_ColorResult::OnNMCustomdraw)
END_MESSAGE_MAP()

// CList_ColorResult 메시지 처리기입니다.
int CList_ColorResult::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ColorResult::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < ColorResult_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, rectClient.Width() / ColorResult_MaxCol);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ColorResult::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ColorResult::InitHeader()
{
	for (int nCol = 0; nCol < ColorResult_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_ColorResult[nCol], iListAglin_ColorResult[nCol], iHeaderWidth_ColorResult[nCol]);
	}

	for (int nCol = 0; nCol < ColorResult_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_ColorResult[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_ColorResult::InsertFullData()
{
	if (m_pstColor == NULL)
		return;

 	DeleteAllItems();
 

	for (UINT nIdx = 0; nIdx < ColorResult_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_ColorResult::SetRectRow(UINT nRow)
{
	if (m_pstColor == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szRoiColor[nRow]);
	SetItemText(nRow, ColorResult_Object, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorResult.iRed[nRow]);
	SetItemText(nRow, ColorResult_ValueR, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcR);
	SetItemText(nRow, ColorResult_MinSpecR, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcR);
	SetItemText(nRow, ColorResult_MaxSpecR, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorResult.iGreen[nRow]);
	SetItemText(nRow, ColorResult_ValueG, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcG);
	SetItemText(nRow, ColorResult_MinSpecG, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcG);
	SetItemText(nRow, ColorResult_MaxSpecG, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorResult.iBlue[nRow]);
	SetItemText(nRow, ColorResult_ValueB, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcB);
	SetItemText(nRow, ColorResult_MinSpecB, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcB);
	SetItemText(nRow, ColorResult_MaxSpecB, strValue);

	strValue.Format(_T("%s"), g_lpszItem_ColorResult[m_pstColor->stColorResult.nEachResult[nRow]]);
	SetItemText(nRow, ColorResult_Result, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ColorResult::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

// 	m_pstColor->stColorResult.iSelectROI = pNMItemActivate->iItem;
// 	m_pstColor->stColorOpt.iSelectROI = -1;

	*pResult = 0;
}


void CList_ColorResult::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	int nRow = 0, nSub = 0;
	switch (lplvcd->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;          // 아이템외에 일반적으로 처리하는 부분
		lplvcd->clrTextBk = RGB(0, 0, 255);
		break;
	case CDDS_ITEMPREPAINT:                          // 행 아이템에 대한 처리를 할 경우
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
		break;
	case CDDS_ITEMPREPAINT | CDDS_SUBITEM:  // 행과 열 아이템에 대한 처리를 할 경우
		nRow = (int)lplvcd->nmcd.dwItemSpec;         // 행 인덱스를 가져옴
		nSub = (int)lplvcd->iSubItem;                       // 열 인덱스를 가져옴

		if (nSub >= ColorResult_ValueR)
		{
			lplvcd->clrTextBk = RGB(250, 236, 197);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}

		if (nSub > ColorResult_ValueR)
		{
			lplvcd->clrTextBk = RGB(255, 255, 255);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}

		if (nSub >= ColorResult_ValueG)
		{
			lplvcd->clrTextBk = RGB(250, 236, 197);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}

		if (nSub > ColorResult_ValueG)
		{
			lplvcd->clrTextBk = RGB(255, 255, 255);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}

		if (nSub >= ColorResult_ValueB)
		{
			lplvcd->clrTextBk = RGB(250, 236, 197);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}

		if (nSub > ColorResult_ValueB)
		{
			lplvcd->clrTextBk = RGB(255, 255, 255);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}

		if (nSub == ColorResult_Result)
		{
			if (m_pstColor->stColorResult.nEachResult[nRow] == TRUE)
			{
				lplvcd->clrTextBk = RGB(0, 0, 255);           // 해당 행, 열 아이템의 배경색을 지정한다.
				lplvcd->clrText = RGB(255, 255, 255);                      // 해당 행, 열 아이템의 글자색을 지정한다.
			}
			else
			{
				lplvcd->clrTextBk = RGB(255, 0, 0);           // 해당 행, 열 아이템의 배경색을 지정한다.
				lplvcd->clrText = RGB(255, 255, 255);                      // 해당 행, 열 아이템의 글자색을 지정한다.
			}
			
		}

		break;
	default:
		*pResult = CDRF_DODEFAULT;

		break;
	}
}
