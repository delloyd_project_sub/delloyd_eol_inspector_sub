﻿#ifndef Wnd_BaseAngleOp_h__
#define Wnd_BaseAngleOp_h__

#pragma once

#include "Wnd_AngleOp.h"

// CWnd_BaseAngleOp

class CWnd_BaseAngleOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseAngleOp)

public:
	CWnd_BaseAngleOp();
	virtual ~CWnd_BaseAngleOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_AngleOp m_wndAngleOp[TICnt_FOV];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseAngleOp_h__
