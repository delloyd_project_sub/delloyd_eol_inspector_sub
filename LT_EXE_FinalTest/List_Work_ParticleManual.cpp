﻿// List_Work_ParticleManual.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_ParticleManual.h"

// CList_Work_ParticleManual
IMPLEMENT_DYNAMIC(CList_Work_ParticleManual, CListCtrl)

CList_Work_ParticleManual::CList_Work_ParticleManual()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_ParticleManual::~CList_Work_ParticleManual()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Work_ParticleManual, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_Work_ParticleManual 메시지 처리기입니다.
int CList_Work_ParticleManual::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	InitHeader();
	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:44
// Desc.		:
//=============================================================================
void CList_Work_ParticleManual::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
BOOL CList_Work_ParticleManual::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_ParticleManual::InitHeader()
{
	for (int nCol = 0; nCol < ParticleManual_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_ParticleManual_Worklist[nCol], iListAglin_ParticleManual_Worklist[nCol], iHeaderWidth_ParticleManual_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_ParticleManual::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iCount = GetItemCount();

	InsertItem(iCount, _T(""));

	SetRectRow(iCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_ParticleManual::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

 	CString strText;
 
	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, ParticleManual_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, ParticleManual_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, ParticleManual_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, ParticleManual_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, ParticleManual_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, ParticleManual_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, ParticleManual_W_Barcode, strText);

	//strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	//SetItemText(nRow, ParticleManual_W_Operator, strText);

// 	strText.Format(_T("%s"), pstCamInfo->szCamType);
// 	SetItemText(nRow, ParticleManual_W_CameraType, strText);

	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stParticleMA[m_nTestIndex].stParticleResult.nResult].szText);
	SetItemText(nRow, ParticleManual_W_Result, strText);
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:47
// Desc.		:
//=============================================================================
void CList_Work_ParticleManual::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = ParticleManual_W_MaxCol;
	CString temp[ParticleManual_W_MaxCol];
	for (int t = 0; t < ParticleManual_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
