﻿//*****************************************************************************
// Filename	: 	TI_PicControl.h
// Created	:	2016/6/9 - 10:16
// Modified	:	2016/6/9 - 10:16
//
// Author	:	Caroline
//	
// Purpose	:	
//*****************************************************************************
#ifndef TI_PicControl_h__
#define TI_PicControl_h__

#pragma once

#define WHITE_COLOR		RGB(255,255,255)
#define BLACK_COLOR		RGB(0,0,0)
#define BLUE_COLOR		RGB(0,0,255)
#define BLUE_DK_COLOR	RGB(0,0,128)
#define RED_COLOR		RGB(255,0,0)
#define RED_DK_COLOR	RGB(128,0,0)
#define GREEN_COLOR		RGB(0,255,0)
#define GREEN_DK_COLOR	RGB(0,128,0)
#define GRAY_COLOR		RGB(192,192,192)
#define GRAY_DK_COLOR	RGB(128,128,128)
#define GRAY_LT_COLOR	RGB(230,230,230)
#define YELLOW_COLOR	RGB(255,255,0)
#define MINOR_COLOR		RGB(255,128,64)
#define PINK_COLOR		RGB(255,0,255)
#define MCYAN			RGB(0,255,255)

#include "Def_DataStruct.h"
// CTI_PicControl

class CTI_PicControl
{

public:
	CTI_PicControl();
	virtual ~CTI_PicControl();

	void	SetPtr_ModelInfo(ST_ModelInfo *pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	};

	// Current
	void	CurrentPic			(CDC *cdc);

	// 광축
	void	CenterPointPic		(CDC *cdc);

	// EIAJ
	void	EIAJPic				(CDC *cdc);
	void	EIAJPic				(CDC *cdc, int NUM);
	int		GetStandardData		(int NUM);
	int		GetStandardPeakData	(int NUM);
	void	DrawSFRGraph		(CDC *cdc, int NUM);
	
	//	로테이트
	void	RotatePic			(CDC *cdc);
	void	RotatePic			(CDC *cdc, int NUM);

	void	SFRPic				(CDC *cdc);
	void	SFRPic				(CDC *pcdc, int iNum);

	// 이물
	void	ParticlePic			(CDC *cdc);
	void	ParticlePic			(CDC *cdc, int NUM);
	void	ParticleErrPic		(CDC *cdc, int NUM);


	// Align
	void	AlignPic			(CDC *cdc);

protected:
	
	ST_ModelInfo*		m_pstModelInfo;

};


#endif // TI_PicControl_h__
