﻿#ifndef List_Work_PatternNoise_h__
#define List_Work_PatternNoise_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_PatternNoise_Worklist
{
	PatternNoise_W_Recode,
	PatternNoise_W_Time,
	PatternNoise_W_Equipment,
	PatternNoise_W_Model,
	PatternNoise_W_SWVersion,
	PatternNoise_W_LOTNum,
	PatternNoise_W_Barcode,
	//PatternNoise_W_Operator,
	PatternNoise_W_Result,
	PatternNoise_W_Side1,
	PatternNoise_W_Side2,
	PatternNoise_W_Side3,
	PatternNoise_W_Side4,
	PatternNoise_W_Side5,
	PatternNoise_W_Side6,
	PatternNoise_W_Side7,
	PatternNoise_W_Side8,
	PatternNoise_W_Side9,
	PatternNoise_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_PatternNoise_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),
	_T("S5"),
	_T("S6"),
	_T("S7"),
	_T("S8"),
	_T("S9"),
	NULL
};

const int	iListAglin_PatternNoise_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_PatternNoise_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_PatternNoise

class CList_Work_PatternNoise : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_PatternNoise)

public:
	CList_Work_PatternNoise();
	virtual ~CList_Work_PatternNoise();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;

	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	};

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_PatternNoise_h__
