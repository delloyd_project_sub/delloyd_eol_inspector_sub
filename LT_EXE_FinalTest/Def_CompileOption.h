﻿//*****************************************************************************
// Filename	: Def_CompileOption.h
// Created	: 2012/10/30
// Modified	: 2016/08/11
//
// Author	: PiRing
//	
// Purpose	: LGIT : 5종 검사 장비 빌드 옵션
//*****************************************************************************
#ifndef Def_CompileOption_h__
#define Def_CompileOption_h__

#include "Def_Enum_Cm.h"
//=============================================================================
// 라인 구분 
//=============================================================================

//=============================================================================
// 언어 설정
//=============================================================================
//#define	LANG_KR	//한국어
//#define	LANG_EN	//영어
//#define	LANG_CN	//중국어

//=============================================================================
// 검사기 종류
//=============================================================================

//=============================================================================
// 검사기 선택
//=============================================================================

//=============================================================================
// 검사기 선택
//=============================================================================
#ifdef SET_INSPECTOR
#undef SET_INSPECTOR
#endif

#define		SET_INSPECTOR		SYS_FINALTEST
//#define		SET_EQUP_TEACH		enInsptrSysTeach_SW
#define		SET_EQUP_TEACH		enInsptrSysTeach_EOL

//=============================================================================
// 프로그램 환경설정 레지스트리 주소
//=============================================================================
#define REG_PATH_BASE			_T("Software\\Luritech")
//#define REG_PATH_OPTION_BASE	_T("Software\\Luritech\\Option")

//=============================================================================
// 기본 사용 장치 선택
//=============================================================================

#define		REG_PATH_APP_BASE		_T("Software\\Luritech\\Delloyd\\Final\\Environment")
#define		REG_PATH_OPTION_BASE	_T("Software\\Luritech\\Delloyd\\Final\\Environment\\Option")

//=============================================================================
// 프로그램 테스트 모드로 빌드시 장치 사용 여부 
//=============================================================================

#define	USE_MASTER_SET		1	// 0 : 마스터 셋 사용안함, 1 : 마스터 셋 사용.

#define	USE_TEST_MODE		1	// 0 -> 테스트 모드

#define	ENGINEER_PASSWORD	_T("luritechinit")
#define	CNC_PASSWORD	_T("C&C")
#define	CNC_PASSWORD_2	_T("c&c")

//#define USE_HW_LOCK_KEY	// 하드웨어 락키 사용여부 (기본: 사용)

//#define	USE_EVMS_MODE
//#define	USE_LIGHTBRD_MODE

// #ifdef USE_EVMS_MODE
// // 	#define		USE_EVMS_EXE_CHECK
// #endif

#endif // Def_CompileOption_h__
