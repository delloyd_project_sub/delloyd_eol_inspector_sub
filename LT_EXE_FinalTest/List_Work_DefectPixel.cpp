﻿// List_Work_Particle.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_DefectPixel.h"


// CList_Work_Particle

IMPLEMENT_DYNAMIC(CList_Work_DefectPixel, CListCtrl)

CList_Work_DefectPixel::CList_Work_DefectPixel()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_DefectPixel::~CList_Work_DefectPixel()
{
	m_Font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CList_Work_DefectPixel, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CList_Work_Particle 메시지 처리기입니다.
int CList_Work_DefectPixel::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	InitHeader();
	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_DefectPixel::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	int iColWidth[Defect_W_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol <Defect_W_MaxCol; nCol++)
		iColDivide += iHeaderWidth_Defect_Worklist[nCol];

	for (int nCol = 0; nCol < Defect_W_MaxCol; nCol++)
		SetColumnWidth(nCol, iHeaderWidth_Defect_Worklist[nCol]);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
BOOL CList_Work_DefectPixel::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Header_MaxNum
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
UINT CList_Work_DefectPixel::Header_MaxNum()
{
	return (UINT)Defect_W_MaxCol;
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_DefectPixel::InitHeader()
{
	for (int nCol = 0; nCol < Defect_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_Defect_Worklist[nCol], iListAglin_Defect_Worklist[nCol], iHeaderWidth_Defect_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_DefectPixel::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));
	SetRectRow(iNewCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_DefectPixel::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

 	CString strText;

	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, Defect_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, Defect_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, Defect_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, Defect_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, Defect_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, Defect_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, Defect_W_Barcode, strText);

	strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	SetItemText(nRow, Defect_W_Operator, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stDefectPixel[m_nTestIndex].stDefectPixelData.nResult].szText);
	SetItemText(nRow, Defect_W_Result, strText);

	if (pstCamInfo->stDefectPixel[m_nTestIndex].stDefectPixelData.nResult == TER_Init)
	{
		strText.Format(_T("X"));
		SetItemText(nRow, Defect_W_Count, strText);
		SetItemText(nRow, Defect_W_Count2, strText);
	}
	else
	{
		strText.Format(_T("%d"), pstCamInfo->stDefectPixel[m_nTestIndex].stDefectPixelData.nFailCount[0]);
		SetItemText(nRow, Defect_W_Count, strText);

		strText.Format(_T("%d"), pstCamInfo->stDefectPixel[m_nTestIndex].stDefectPixelData.nFailCount[1]);
		SetItemText(nRow, Defect_W_Count2, strText);
	}
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:43
// Desc.		:
//=============================================================================
void CList_Work_DefectPixel::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = Defect_W_MaxCol;
	CString temp[Defect_W_MaxCol];
	for (int t = 0; t < Defect_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
