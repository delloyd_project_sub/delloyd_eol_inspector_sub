﻿// List_ReverseOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ReverseOp.h"

#define IRtOp_ED_CELLEDIT			5001

// CList_ReverseOp

IMPLEMENT_DYNAMIC(CList_ReverseOp, CListCtrl)

CList_ReverseOp::CList_ReverseOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	
	m_nWidth	= 720;
	m_nHeight	= 480;

	m_pstReverse	= NULL;
}

CList_ReverseOp::~CList_ReverseOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_ReverseOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_ReverseOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_ReverseOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_ReverseOp::OnEnKillFocusEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_ReverseOp 메시지 처리기입니다.
int CList_ReverseOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);
	

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ReverseOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[ReverseOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = ReverseOp_PosX; nCol < ReverseOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_ReverseOp[ReverseOp_Object]) / (ReverseOp_MaxCol - ReverseOp_PosX);
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ReverseOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ReverseOp::InitHeader()
{
	for (int nCol = 0; nCol < ReverseOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_ReverseOp[nCol], iListAglin_ReverseOp[nCol], iHeaderWidth_ReverseOp[nCol]);
	}

	for (int nCol = 0; nCol < ReverseOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_ReverseOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_ReverseOp::InsertFullData()
{
	if (m_pstReverse == NULL)
		return;

 	DeleteAllItems();
 

	for (UINT nIdx = 0; nIdx < ReverseOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_ReverseOp::SetRectRow(UINT nRow)
{
	if (m_pstReverse == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szRoiReverse[nRow]);
	SetItemText(nRow, ReverseOp_Object, strValue);

	strValue.Format(_T("%d"), m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
	SetItemText(nRow, ReverseOp_PosX, strValue);

	strValue.Format(_T("%d"),  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
	SetItemText(nRow, ReverseOp_PosY, strValue);

	strValue.Format(_T("%d"),  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Width());
	SetItemText(nRow, ReverseOp_Width, strValue);

	strValue.Format(_T("%d"),  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Height());
	SetItemText(nRow, ReverseOp_Height, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ReverseOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	m_pstReverse->bTestMode = FALSE;
	m_pstReverse->stReverseOpt.iSelectROI = pNMItemActivate->iItem;
	m_pstReverse->stReverseResult.iSelectROI = -1;

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ReverseOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < ReverseOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;
			m_pstReverse->bTestMode = FALSE;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_ReverseOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_ReverseOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (m_pstReverse == NULL)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp =  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi;

	switch (nCol)
	{
	case ReverseOp_PosX:
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case ReverseOp_PosY:
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case ReverseOp_Width:
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case ReverseOp_Height:
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	default:
		break;
	}

	if ( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.left < 0)
	{
		CRect rtTemp =  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi;

		 m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.left = 0;
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.right =  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.left + rtTemp.Width();
	}

	if ( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.right > m_nWidth)
	{
		CRect rtTemp =  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi;

		 m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.right = m_nWidth;
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.left =  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.right - rtTemp.Width();
	}

	if ( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.top < 0)
	{
		CRect rtTemp =  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi;

		 m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.top = 0;
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.bottom = rtTemp.Height() +  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.top;
	}

	if ( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.bottom > m_nHeight)
	{
		CRect rtTemp =  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi;

		 m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.bottom = m_nHeight;
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.top =  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.bottom - rtTemp.Height();
	}

	if ( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Height() <= 0)
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].RectPosWH( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Width(), 1);

	if ( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Height() >= m_nHeight)
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].RectPosWH( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Width(), m_nHeight);

	if ( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Width() <= 0)
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].RectPosWH(1,  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Height());

	if ( m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Width() >= m_nWidth)
		 m_pstReverse->stReverseOpt.stRegionOp[nRow].RectPosWH(m_nWidth,  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Height());

	CString strValue;

	switch (nCol)
	{
	case ReverseOp_PosX:
		strValue.Format(_T("%d"),  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
		break;
	case ReverseOp_PosY:
		strValue.Format(_T("%d"),  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
		break;
	case ReverseOp_Width:
		strValue.Format(_T("%d"),  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Width());
		break;
	case ReverseOp_Height:
		strValue.Format(_T("%d"),  m_pstReverse->stReverseOpt.stRegionOp[nRow].rtRoi.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/10/14 - 17:46
// Desc.		:
//=============================================================================
BOOL CList_ReverseOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	CString str;
	str.Format(_T("%.1f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ReverseOp::GetCellData()
{
	if (m_pstReverse == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ReverseOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue  = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120)*0.1);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120)*0.1);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (dbValue > 2.0)
			dbValue = 2.0;
		
		UpdateCellData(m_nEditRow, m_nEditCol, iValue);

	}
	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}



void CList_ReverseOp::OnEnSelectFocusCamTypeCombo()
{
	m_pstReverse->stReverseOpt.stRegionOp[m_nEditRow].nCamState = m_cb_CamType.GetCurSel();
}
