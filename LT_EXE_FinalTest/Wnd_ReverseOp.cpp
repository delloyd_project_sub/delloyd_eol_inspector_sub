﻿// Wnd_ReverseOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_ReverseOp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_ReverseOp
typedef enum ReverseOptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
	IDC_LIST_ITEMSUB = 5001,
	IDC_CMB_STATE = 6001,
};

IMPLEMENT_DYNAMIC(CWnd_ReverseOp, CWnd)

CWnd_ReverseOp::CWnd_ReverseOp()
{
	m_pstModelInfo	= NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_ReverseOp::~CWnd_ReverseOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_ReverseOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_ReverseOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_ReverseOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < BTN_REVERSE_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szReverseButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}


	for (UINT nIdex = 0; nIdex < CMB_REVERSE_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
		//m_cb_Item[nIdex].InsertString(nIdex, _T(""));
	}

	for (UINT nIdex = 0; nIdex < STI_REVERSE_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szReverseStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}


	for (UINT nIdex = 0; nIdex < CMB_REVERSE_MAX; nIdex++)
	{
		m_cb_State[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_STATE + nIdex);
		//m_cb_State[nIdex].InsertString(nIdex, _T(""));
	}
	for (UINT nIdx = 0; NULL != g_szSatateType[nIdx]; nIdx++)
	{
		m_cb_State[Com_REVERSE_STATE].InsertString(nIdx, g_szSatateType[nIdx]);
	}
	m_cb_State[Com_REVERSE_STATE].SetCurSel(0);

		
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);
	m_ListSub.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEMSUB);
	

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_ReverseOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt = ReverseOp_ItemNum;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iList_H  = iHeaderH + iIdxCnt * 15;

	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = iWidth / 4;
	int iSTHeight = 25;

	iTop = iMargin;

	iLeft = iMargin;
	m_st_Item[STI_REVERSE_STATE].MoveWindow(iLeft, iTop, iSTWidth, iCtrl_H);

	iLeft += iSTWidth + 2;
	m_cb_State[Com_REVERSE_STATE].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	//
	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_REVERSE_TEST].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft = cx - iMargin - iCtrl_W;
	iTop += iSpacing + iCtrl_H;
	m_bn_Item[BTN_REVERSE_RESET].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);
	
	iLeft = iMargin;
	iTop += iSpacing + iSTHeight;
	m_st_Item[STI_REVERSE_ROI].MoveWindow(iLeft, iTop, iWidth, iSTHeight);
	
	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	iLeft = iMargin;
	iTop += iSpacing + iSTHeight;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);
	
	iTop += iSpacing + iList_H;
	iList_H = iHeaderH + iIdxCnt * 15;
	m_st_Item[STI_REVERSE_OPT].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	if (iIdxCnt <= 0 || iList_H > iHeight - iTop)
	{
		iList_H = iHeight;
	}	

	iLeft = iMargin;
	iTop += iSpacing + iSTHeight;
	m_ListSub.MoveWindow(iLeft, iTop, iWidth, iList_H);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_ReverseOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_ReverseOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (NULL == m_pstModelInfo)
		return;

	if (TRUE == bShow)
	{
		m_pstModelInfo->nTestMode = TIID_Reverse;
		m_pstModelInfo->nTestCnt = m_nTestItemCnt;
		m_pstModelInfo->nPicItem = PIC_Reverse;
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_ReverseOp::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
	m_pstModelInfo->stReverse[m_nTestItemCnt].bTestMode = FALSE;
	switch (nIdex)
	{
	case BTN_REVERSE_TEST:
		GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_Reverse);
		break;

	case BTN_REVERSE_RESET:
		m_pstModelInfo->stReverse[m_nTestItemCnt].stReverseOpt = m_pstModelInfo->stReverse[m_nTestItemCnt].stInitReverseOpt;
		GetOwner()->SendMessage(WM_OPTDATA_RESET, 0, 0);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_ReverseOp::SetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	m_List.SetPtr_Reverse(&m_pstModelInfo->stReverse[m_nTestItemCnt]);
	m_List.SetImageSize(m_pstModelInfo->dwWidth, m_pstModelInfo->dwHeight);
	m_List.InsertFullData();

	m_ListSub.SetPtr_ReverseSub(&m_pstModelInfo->stReverse[m_nTestItemCnt]);
	m_ListSub.SetImageSize(m_pstModelInfo->dwWidth, m_pstModelInfo->dwHeight);
	m_ListSub.InsertFullData();

	m_cb_State[Com_REVERSE_STATE].SetCurSel(m_pstModelInfo->stReverse[m_nTestItemCnt].stReverseOpt.nStateType);

}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:01
// Desc.		:
//=============================================================================
void CWnd_ReverseOp::GetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	CString strValue;
	UINT	nData = 0;

	nData = m_cb_State[Com_REVERSE_STATE].GetCurSel();
	m_pstModelInfo->stReverse[m_nTestItemCnt].stReverseOpt.nStateType = nData;
}