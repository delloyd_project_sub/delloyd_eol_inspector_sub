#pragma once
#include "resource.h"

#include "Def_TestDevice.h"
#include "Wnd_VideoView.h"
#include "TestProcess_Image.h"
#include "VGStatic.h"

// CDlg_CenterPtTunning 대화 상자입니다.

class CDlg_CenterPtTunning : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_CenterPtTunning)

public:
	CDlg_CenterPtTunning(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_CenterPtTunning();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_CENTERPOINT_TUNNING };

	void CreateThread(UINT _method);
	bool DestroyThread();
	int	 ThreadFunction();

	enum enAdjMode
	{
		eAdjMode_Up,
		eAdjMode_Down,
		eAdjMode_Left,
		eAdjMode_Right,
		eAdjMode_All
	};

	UINT m_nResult;
	UINT m_nViewChannel;
	BOOL m_bFlag_Butten[DI_NotUseBit_Max];

	ST_Device* m_pDevice;
	ST_ModelInfo* m_pModelinfo;

	UINT GetResult()
	{
		return m_nResult;
	};

	void SetPtr_Device(__in ST_Device* pDevice)
	{
		if (pDevice == NULL)
			return;

		m_pDevice = pDevice;
	};

	void SetPtr_Modelinfo(__in ST_ModelInfo* pModelinfo)
	{
		if (pModelinfo == NULL)
			return;

		m_pModelinfo = pModelinfo;
	};

	void SetVideoChannel(__in UINT nCH)
	{
		m_nViewChannel = nCH;
	};

	CWnd_VideoView m_wndVideoView;
	CTestProcess_Image	m_ImageTest;

protected:
	CFont m_font;

	CVGStatic m_stAxisX;
	CVGStatic m_stAxisY;

	CMFCMaskedEdit m_Ed_Hor;
	CMFCMaskedEdit m_Ed_Ver;


	CMFCButton m_bn_Up;
	CMFCButton m_bn_Down;
	CMFCButton m_bn_Left;
	CMFCButton m_bn_Right;
	CMFCButton m_bn_OK;
	CMFCButton m_bn_Init;

	BOOL IsCameraConnect(__in UINT nCh, __in UINT nGrabType);
	LPBYTE GetImageBuffer(__in UINT nViewCh, __in UINT nGrabType, __out DWORD& dwWidth, __out DWORD& dwHeight, __out UINT& nChannel);

	UINT OpticalCenter_Adj_initial(__in UINT nCh);
	UINT OpticalCenter_Adj(__in UINT nCh, __in int iCenterX, __in int iCenterY, __in int iTargerX, __in int iTargerY);
	UINT OpticalCenter_Adj(__in enAdjMode nAdjMode, __in UINT nCh, __in int iOffset);

	UINT OpticalCenter_Adj_Horizon(__in UINT nCh, __in int iOffset);
	UINT OpticalCenter_Adj_Vertical(__in UINT nCh, __in int iOffset);
	UINT OpticalCenter_Adj_AllWrite(__in UINT nCh);

	int m_iOffsetXSum;
	int m_iOffsetYSum;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnBnClickedBnUp();
	afx_msg void OnBnClickedBnDown();
	afx_msg void OnBnClickedBnLeft();
	afx_msg void OnBnClickedBnRight();
	afx_msg void OnBnClickedBnOK();
	afx_msg void OnBnClickedBnInit();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
};
