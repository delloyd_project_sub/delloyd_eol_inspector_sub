﻿#ifndef List_EIAJResult_h__
#define List_EIAJResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_EIAJResult
{
	EIAJResult_Object = 0,
	EIAJResult_Value,
	EIAJResult_MinSpec,
	EIAJResult_MaxSpec,
	EIAJResult_Result,
	EIAJResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_EIAJResult[] =
{
	_T(""),
	_T("Value"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_EIAJResult
{
	EIAJResult_ItemNum = ReOp_ItemNum - ReOp_CenterLeft,
};

static LPCTSTR	g_lpszItem_EIAJResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_EIAJResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_EIAJResult[] =
{
	80,
	80,
	80,
	80,
	80,
};
// List_EIAJInfo

class CList_EIAJResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_EIAJResult)

public:
	CList_EIAJResult();
	virtual ~CList_EIAJResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_EIAJ(ST_LT_TI_EIAJ* pstEIAJ)
	{
		if (pstEIAJ == NULL)
			return;

		m_pstEIAJ = pstEIAJ;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_EIAJ*  m_pstEIAJ;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_EIAJInfo_h__
