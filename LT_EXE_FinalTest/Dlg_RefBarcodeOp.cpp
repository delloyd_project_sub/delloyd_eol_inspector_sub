// Dlg_RefBarcodeOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_RefBarcodeOp.h"
#include "afxdialogex.h"

#include "File_Model.h"

#define		IDC_WND_BARCODEOP	1000
#define		IDC_BN_SAVE			2000

// CDlg_RefBarcodeOp 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_RefBarcodeOp, CDialogEx)

CDlg_RefBarcodeOp::CDlg_RefBarcodeOp(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_RefBarcodeOp::IDD, pParent)
{

}

CDlg_RefBarcodeOp::~CDlg_RefBarcodeOp()
{
}


void CDlg_RefBarcodeOp::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}



BEGIN_MESSAGE_MAP(CDlg_RefBarcodeOp, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_SAVE, OnBnClickedBnSave)

	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CDlg_RefBarcodeOp 메시지 처리기입니다.


int CDlg_RefBarcodeOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_wndBarcodeConfig.SetOwner(this);
	m_wndBarcodeConfig.SetReferenceSeperator(m_szRefBarcode);
	m_wndBarcodeConfig.Create(NULL, _T("Barcode Config"), dwStyle /*| WS_BORDER*/, rectDummy, this, IDC_WND_BARCODEOP);

	m_bn_Save.Create(_T("Save (Apply)"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);
	m_bn_Save.SetMouseCursorHand();

	Set_BarcodeInfo(m_pstBarcodeInfo);

	return 0;
}


void CDlg_RefBarcodeOp::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 5;
	int iCateSpacing = 10;
	int iMagin = 10;
	int iLeft = iMagin;
	int iTop = iMagin;
	int iWidth = cx - (iMagin * 2);
	int iHeight = cy - (iMagin * 2);

	int iCtrlHeight = 35;
	int iCtrlWidth = 147;

	iLeft = cx - iMagin - iCtrlWidth;
	m_bn_Save.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft = iMagin;
	iTop += iCtrlHeight + iSpacing;
	m_wndBarcodeConfig.MoveWindow(iLeft, iTop, iWidth, iHeight - (iCtrlHeight + iSpacing));
}

BOOL CDlg_RefBarcodeOp::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreCreateWindow(cs);
}


BOOL CDlg_RefBarcodeOp::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if (pMsg->message == WM_KEYDOWN 
		&& pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN 
		&& pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlg_RefBarcodeOp::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	lpMMI->ptMaxTrackSize.x = 650;
	lpMMI->ptMaxTrackSize.y = 650;

	lpMMI->ptMinTrackSize.x = 650;
	lpMMI->ptMinTrackSize.y = 650;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

void CDlg_RefBarcodeOp::OnBnClickedBnSave()
{
	Get_BarcodeInfo(*m_pstBarcodeInfo);

	CFile_Model	m_fileModel;

	CString szBarcodeOpPath;
	szBarcodeOpPath.Format(_T("%s\\refBarcode.ini"), m_szBarcodeOpPath);

	if (m_fileModel.Save_RefBarcodeInfo(szBarcodeOpPath, m_pstBarcodeInfo))
	{
		Set_BarcodeInfo(m_pstBarcodeInfo);
	}

	AfxMessageBox(_T("Reference Barcode was saved."));
}

void CDlg_RefBarcodeOp::Set_BarcodeInfo(__in const ST_BarcodeRefInfo* pstBarcodeInfo)
{
	m_wndBarcodeConfig.Set_BarcodeInfo(pstBarcodeInfo);
}
	
void CDlg_RefBarcodeOp::Get_BarcodeInfo(__out ST_BarcodeRefInfo& stBarcodeInfo)
{
	m_wndBarcodeConfig.Get_BarcodeInfo(stBarcodeInfo);
}

void CDlg_RefBarcodeOp::SetPath(__in LPCTSTR szModelPath, __in LPCTSTR szBarcodePath)
{
	m_szBarcodeOpPath = szBarcodePath;
	m_wndBarcodeConfig.SetPath(szModelPath);
}