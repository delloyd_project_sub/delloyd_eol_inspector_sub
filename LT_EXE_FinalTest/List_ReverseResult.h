﻿#ifndef List_ReverseResult_h__
#define List_ReverseResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_ReverseResult
{
	ReverseResult_Object = 0,
	ReverseResult_CamState,
	ReverseResult_MasterCamState,
	ReverseResult_Result,
	ReverseResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_ReverseResult[] =
{
	_T(""),
	_T("Camera State"),
	_T("Master Type"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_ReverseResult
{
	ReverseResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_ReverseResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_ReverseResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ReverseResult[] =
{
	80,
	80,
	80,
	80,
};

// List_ReverseInfo

class CList_ReverseResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ReverseResult)

public:
	CList_ReverseResult();
	virtual ~CList_ReverseResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Reverse(ST_LT_TI_Reverse* pstReverse)
	{
		if (pstReverse == NULL)
			return;

		m_pstReverse = pstReverse;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Reverse*  m_pstReverse;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_ReverseInfo_h__
