﻿//*****************************************************************************
// Filename	: 	Wnd_LotInfo.h
// Created	:	2016/7/7 - 16:21
// Modified	:	2016/7/7 - 16:21
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_LotInfo_h__
#define Wnd_LotInfo_h__

#pragma once

#include "VGGroupWnd.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "Grid_LotInfo.h"
#include "Grid_MESInfo.h"
#include "Grid_Yield.h"

class CWnd_LotInfo : public CWnd
{
	DECLARE_DYNAMIC(CWnd_LotInfo)

public:
	CWnd_LotInfo();
	virtual ~CWnd_LotInfo();

protected:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()

	CGrid_LotInfo	m_grid_LotInfo;
	//CGrid_MESInfo	m_grid_MESInfo;
	CGrid_Yield		m_grid_Yield;

	CMFCTabCtrl		m_tc_Info;

public:

	void	SetInspectionMode			(__in enPermissionMode InspMode);
	void	SetLotInifo					(__in const ST_LOTInfo* pstLotInfo);
	void	SetMESInifo					(__in const ST_MES_TotalResult* pstMESInfo);
	void	SetYield					(__in const ST_Yield* pstYield);
};

#endif // Wnd_LotInfo_h__
