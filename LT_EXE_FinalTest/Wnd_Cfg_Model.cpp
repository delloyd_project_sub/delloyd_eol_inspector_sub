﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Model.cpp
// Created	:	2016/3/14 - 10:57
// Modified	:	2016/3/14 - 10:57
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_Model.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_Model.h"
#include "resource.h"
#include "Def_WindowMessage.h"

typedef enum Model_ID
{
	IDC_EDT_ModelName = 1001,
	IDC_EDT_Voltage,
	//IDC_EDT_Voltage2,
	IDC_EDT_Cam_Delay,
	IDC_EDT_Cam_Width,
	IDC_EDT_Cam_Height,
	IDC_EDT_Test_Distance,
	IDC_COB_Cam_Type,
	//IDC_COB_Cam_I2c_File,
	//IDC_BTN_Cam_Distortion,
	IDC_EDT_Test_X,
	IDC_EDT_Test_Y,
};

// CWnd_Cfg_Model
IMPLEMENT_DYNAMIC(CWnd_Cfg_Model, CWnd_BaseView)

CWnd_Cfg_Model::CWnd_Cfg_Model()
{
	VERIFY(m_font.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_Model::~CWnd_Cfg_Model()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_Model, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//ON_BN_CLICKED(IDC_BTN_Cam_Distortion, OnBnClickedBnDistortionSet)
END_MESSAGE_MAP()

// CWnd_Cfg_Model message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
int CWnd_Cfg_Model::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_ML_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szModel_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

//	for (UINT nIdex = 0; nIdex < BTN_ML_MAXNUM; nIdex++)
//	{
// 		if (BTN_ML_IIC_Check1 == nIdex || BTN_ML_IIC_Check2 == nIdex)
// 		{
// 			m_bn_Item[nIdex].Create(_T(""), dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_Cam_Distortion + nIdex);
// 		}
// 		else
//		{
//			m_bn_Item[nIdex].Create(_T("Distortion Correction"), dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_Cam_Distortion + nIdex);
//		}
//	}

// 	m_bn_Item[BTN_ML_Distortion].SetImage(IDB_UNCHECKED_16);
// 	m_bn_Item[BTN_ML_Distortion].SetCheckedImage(IDB_CHECKED_16);
// 	m_bn_Item[BTN_ML_Distortion].SizeToContent();
// 	m_bn_Item[BTN_ML_Distortion].SetCheck(BST_UNCHECKED);

// 	m_bn_Item[BTN_ML_IIC_Check1].SetImage(IDB_UNCHECKED_16);
// 	m_bn_Item[BTN_ML_IIC_Check1].SetCheckedImage(IDB_CHECKED_16);
// 	m_bn_Item[BTN_ML_IIC_Check1].SizeToContent();
// 	m_bn_Item[BTN_ML_IIC_Check1].SetCheck(BST_UNCHECKED);
// 
// 	m_bn_Item[BTN_ML_IIC_Check2].SetImage(IDB_UNCHECKED_16);
// 	m_bn_Item[BTN_ML_IIC_Check2].SetCheckedImage(IDB_CHECKED_16);
// 	m_bn_Item[BTN_ML_IIC_Check2].SizeToContent();
// 	m_bn_Item[BTN_ML_IIC_Check2].SetCheck(BST_UNCHECKED);

	for (UINT nIdex = 0; nIdex < COB_ML_MAXNUM; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_COB_Cam_Type + nIdex);
	}

//	for (UINT nIdx = 0; NULL != g_szGrabType[nIdx]; nIdx++)
//	{
//		m_cb_Item[COB_ML_GrabType].InsertString(nIdx, g_szGrabType[nIdx]);
//	}
//	m_cb_Item[COB_ML_GrabType].SetCurSel(0);

	for (UINT nIdx = 0; NULL != g_szCamState[nIdx]; nIdx++)
	{
		m_cb_Item[COB_ML_CameraType].InsertString(nIdx, g_szCamState[nIdx]);
	}
	m_cb_Item[COB_ML_CameraType].SetCurSel(0);

	// OpenShort
	for (UINT nIdx = 0; NULL != g_szUseNotUse[nIdx]; nIdx++)
	{
		m_cb_Item[COB_ML_UseOpenShort].InsertString(nIdx, g_szUseNotUse[nIdx]);
	}
	m_cb_Item[COB_ML_UseOpenShort].SetCurSel(0);


	for (UINT nIdx = 0; nIdx < EDT_ML_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ModelName + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font);
		
		if (nIdx != EDT_ML_ModelName)
		{
			m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
		}
	}

//  	if (!m_szI2CPath.IsEmpty())
//  	{
// 		m_IniWatch.SetWatchOption(m_szI2CPath, I2C_FILE_EXT);
//  		m_IniWatch.RefreshList();
//  
// 		RefreshFileList(COB_ML_I2CFile_1, m_IniWatch.GetFileList());
// 		m_cb_Item[COB_ML_I2CFile_1].SetCurSel(0);
//  	}
// 
// 	if (!m_szI2CPath.IsEmpty())
// 	{
// 		m_IniWatch.SetWatchOption(m_szI2CPath, I2C_FILE_EXT);
// 		m_IniWatch.RefreshList();
// 
// 		RefreshFileList(COB_ML_I2CFile_2, m_IniWatch.GetFileList());
// 		m_cb_Item[COB_ML_I2CFile_2].SetCurSel(0);
// 	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 5;

	int iLeft	= iMagrin;
	int iTop	= iMagrin;
	int iWidth	= cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iCtrlWidth	= 0;
	int iCtrlHeight = 25;
	int iHalfWidth	= (iWidth - iCateSpacing- iCateSpacing) / 3;
	int iLeftSub  	= 0;
	int iTempWidth  = 35;

	int iCapWidth	= 140;

	iCtrlWidth = (iHalfWidth - iCateSpacing) / 2;

	for (UINT nIdx = 0; nIdx < STI_ML_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		
		iLeftSub = iLeft + iCapWidth + iCateSpacing;

		switch (nIdx)
		{
		case STI_ML_ModelName:
			m_ed_Item[EDT_ML_ModelName].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
			break;
		case STI_ML_Cam_Delay:
			m_ed_Item[EDT_ML_CameraDelay].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
			break;
		case STI_ML_Cam_Width:
			m_ed_Item[EDT_ML_CameraWidth].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
			break;
		case STI_ML_Cam_Height:
			m_ed_Item[EDT_ML_CameraHeight].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
			break;
		case STI_ML_Cam_Volt:
			m_ed_Item[EDT_ML_Voltage].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
			break;
		case STI_ML_Cam_Type:
			m_cb_Item[COB_ML_CameraType].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
			break;
		case STI_ML_TestX:
			m_ed_Item[EDT_ML_TestX].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
		case STI_ML_TestY:
			m_ed_Item[EDT_ML_TestY].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
		case STI_ML_Test_Distance:
			m_ed_Item[EDT_ML_TestDistance].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
			break;
		case STI_ML_UseOpenShort:
			m_cb_Item[COB_ML_UseOpenShort].MoveWindow(iLeftSub, iTop, iHalfWidth * 2, iCtrlHeight);
			break;
		default:
			break;
		}

		iTop += iCtrlHeight + iCateSpacing;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Model::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Model::PreTranslateMessage(MSG* pMsg)
{
	return CWnd_BaseView::PreTranslateMessage(pMsg);
}


void CWnd_Cfg_Model::OnBnClickedBnDistortionSet()
{
	//BOOL bCheck = m_bn_Item[BTN_ML_Distortion].GetCheck();

	//GetOwner()->SendNotifyMessage(WM_DISTORTION_CORR, bCheck, 0);
}


//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 13:13
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::GetUIData(__out ST_ModelInfo& stModelInfo)
{
	CString strValue;	
	UINT	nData = 0;

	// 모델명
	m_ed_Item[EDT_ML_ModelName].GetWindowText(strValue);
	stModelInfo.szModelCode = strValue;

	m_ed_Item[EDT_ML_Voltage].GetWindowText(strValue);
	stModelInfo.fVoltage[0] = (FLOAT)_ttof(strValue.GetBuffer());
	strValue.ReleaseBuffer();

	//m_ed_Item[EDT_ML_Voltage2].GetWindowText(strValue);
	//stModelInfo.fVoltage[1] = (FLOAT)_ttof(strValue.GetBuffer());
	//strValue.ReleaseBuffer();

	m_ed_Item[EDT_ML_CameraDelay].GetWindowText(strValue);
	stModelInfo.nCameraDelay = _ttoi(strValue.GetBuffer());
	strValue.ReleaseBuffer();

	m_ed_Item[EDT_ML_CameraWidth].GetWindowText(strValue);
	stModelInfo.dwWidth = _ttoi(strValue.GetBuffer());
	strValue.ReleaseBuffer();

	m_ed_Item[EDT_ML_CameraHeight].GetWindowText(strValue);
	stModelInfo.dwHeight = _ttoi(strValue.GetBuffer());
	strValue.ReleaseBuffer();

	m_ed_Item[EDT_ML_TestDistance].GetWindowText(strValue);
	stModelInfo.dbTestDistance = _ttof(strValue.GetBuffer());
	strValue.ReleaseBuffer();

	stModelInfo.bUseOpenShort = m_cb_Item[COB_ML_UseOpenShort].GetCurSel();

	//m_ed_Item[EDT_ML_TestX].GetWindowText(strValue);
	//stModelInfo.dbTestAxisX = _ttof(strValue.GetBuffer());
	//strValue.ReleaseBuffer();

	//m_ed_Item[EDT_ML_TestY].GetWindowText(strValue);
	//stModelInfo.dbTestAxisY = _ttof(strValue.GetBuffer());
	//strValue.ReleaseBuffer();
	
	m_ed_Item[EDT_ML_TestX].GetWindowText(strValue);
	stModelInfo.dbTestAxisX = _ttof(strValue.GetBuffer());
	strValue.ReleaseBuffer();

	m_ed_Item[EDT_ML_TestY].GetWindowText(strValue);
	stModelInfo.dbTestAxisY = _ttof(strValue.GetBuffer());
	strValue.ReleaseBuffer();

	//nData = m_cb_Item[COB_ML_GrabType].GetCurSel();
	//stModelInfo.nGrabType = nData;

	nData = m_cb_Item[COB_ML_CameraType].GetCurSel();
	stModelInfo.nCameraType = nData;
// 
// 	nData = m_cb_Item[COB_ML_VideoType].GetCurSel();
// 	stModelInfo.nVideoType = nData;

// 	nData = m_cb_Item[COB_ML_CurtainType].GetCurSel();
// 	stModelInfo.nCurtainType = nData;

// 	m_cb_Item[COB_ML_I2CFile_1].GetWindowText(strValue);
// 	stModelInfo.szI2CFile_1 = strValue;
// 
// 	m_cb_Item[COB_ML_I2CFile_2].GetWindowText(strValue);
// 	stModelInfo.szI2CFile_2 = strValue;
// 
// 	//stModelInfo.nCameraDistortion = m_bn_Item[BTN_ML_Distortion].GetCheck();
// 	stModelInfo.bI2CFile_1 = m_bn_Item[BTN_ML_IIC_Check1].GetCheck();
// 	stModelInfo.bI2CFile_2 = m_bn_Item[BTN_ML_IIC_Check2].GetCheck();

}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::SetUIData(__in const ST_ModelInfo* pModelInfo)
{
	CString strValue;
	UINT	nData = 0;

	// 모델명
	m_ed_Item[EDT_ML_ModelName].SetWindowText(pModelInfo->szModelCode);

	strValue.Format(_T("%.1f"), pModelInfo->fVoltage[0]);
	m_ed_Item[EDT_ML_Voltage].SetWindowText(strValue);

	//strValue.Format(_T("%.1f"), pModelInfo->fVoltage[1]);
	//m_ed_Item[EDT_ML_Voltage2].SetWindowText(strValue);

	strValue.Format(_T("%d"), pModelInfo->nCameraDelay);
	m_ed_Item[EDT_ML_CameraDelay].SetWindowText(strValue);

	strValue.Format(_T("%d"), pModelInfo->dwWidth);
	m_ed_Item[EDT_ML_CameraWidth].SetWindowText(strValue);

	strValue.Format(_T("%d"), pModelInfo->dwHeight);
	m_ed_Item[EDT_ML_CameraHeight].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), pModelInfo->dbTestDistance);
	m_ed_Item[EDT_ML_TestDistance].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), pModelInfo->dbTestAxisX);
	m_ed_Item[EDT_ML_TestX].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), pModelInfo->dbTestAxisY);
	m_ed_Item[EDT_ML_TestY].SetWindowText(strValue);

	m_cb_Item[COB_ML_UseOpenShort].SetCurSel(pModelInfo->bUseOpenShort);

	//m_cb_Item[COB_ML_GrabType].SetCurSel(pModelInfo->nGrabType);

	m_cb_Item[COB_ML_CameraType].SetCurSel(pModelInfo->nCameraType);

	//m_bn_Item[BTN_ML_Distortion].SetCheck(pModelInfo->nCameraDistortion);

// 	m_bn_Item[BTN_ML_IIC_Check1].SetCheck(pModelInfo->bI2CFile_1);
// 
// 	m_bn_Item[BTN_ML_IIC_Check2].SetCheck(pModelInfo->bI2CFile_2);

// 	m_cb_Item[COB_ML_VideoType].SetCurSel(pModelInfo->nVideoType);

//	m_cb_Item[COB_ML_CurtainType].SetCurSel(pModelInfo->nCurtainType);

// 	strValue = m_szI2CPath + pModelInfo->szI2CFile_1;
// 
// 	if (!m_szI2CPath.IsEmpty())
// 	{
// 		m_IniWatch.SetWatchOption(m_szI2CPath, I2C_FILE_EXT);
// 		m_IniWatch.RefreshList();
// 
// 		RefreshFileList(COB_ML_I2CFile_1, m_IniWatch.GetFileList());
// 	}
// 
// 	if (!pModelInfo->szI2CFile_1.IsEmpty())
// 	{
// 		int iSel = m_cb_Item[COB_ML_I2CFile_1].FindStringExact(0, pModelInfo->szI2CFile_1);
// 
// 		if (0 <= iSel)
// 			m_cb_Item[COB_ML_I2CFile_1].SetCurSel(iSel);
// 	}
// 
// 	strValue = m_szI2CPath + pModelInfo->szI2CFile_2;
// 
// 	if (!m_szI2CPath.IsEmpty())
// 	{
// 		m_IniWatch.SetWatchOption(m_szI2CPath, I2C_FILE_EXT);
// 		m_IniWatch.RefreshList();
// 
// 		RefreshFileList(COB_ML_I2CFile_2, m_IniWatch.GetFileList());
// 	}
// 
// 	if (!pModelInfo->szI2CFile_1.IsEmpty())
// 	{
// 		int iSel = m_cb_Item[COB_ML_I2CFile_2].FindStringExact(0, pModelInfo->szI2CFile_2);
// 
// 		if (0 <= iSel)
// 			m_cb_Item[COB_ML_I2CFile_2].SetCurSel(iSel);
// 	}
}

//=============================================================================
// Method		: RefreshFileList
// Access		: protected  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/7/6 - 9:47
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::RefreshFileList(__in UINT nComboID, __in const CStringList* pFileList)
{
	m_cb_Item[nComboID].ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_Item[nComboID].AddString(pFileList->GetNext(pos));
	}

	// 초기화
	if (0 < pFileList->GetCount())
	{
		m_cb_Item[nComboID].SetCurSel(0);
	}
}

//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_ModelInfo * pModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::SetModelInfo(__in const ST_ModelInfo* pModelInfo)
{
	SetUIData(pModelInfo);
}

//=============================================================================
// Method		: GetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 10:42
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::GetModelInfo(__out ST_ModelInfo& stModelInfo)
{
	GetUIData(stModelInfo);
}
