﻿// List_Work_CenterPoint.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_CenterPoint.h"


// CList_Work_CenterPoint

IMPLEMENT_DYNAMIC(CList_Work_CenterPoint, CListCtrl)

CList_Work_CenterPoint::CList_Work_CenterPoint()
{

	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_CenterPoint::~CList_Work_CenterPoint()
{
	m_Font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CList_Work_CenterPoint, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CList_Work_CenterPoint 메시지 처리기입니다.
int CList_Work_CenterPoint::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	InitHeader();
	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
BOOL CList_Work_CenterPoint::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::InitHeader()
{
	for (int nCol = 0; nCol < CP_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_CP_Worklist[nCol], iListAglin_CP_Worklist[nCol], iHeaderWidth_CP_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));
	SetRectRow(iNewCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

 	CString strText;

	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, CP_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, CP_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, CP_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, CP_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, CP_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, CP_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, CP_W_Barcode, strText);

	//strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	//SetItemText(nRow, CP_W_Operator, strText);

// 	strText.Format(_T("%s"), pstCamInfo->szCamType);
// 	SetItemText(nRow, CP_W_CameraType, strText);

	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stCenterPoint[m_nTestIndex].stCenterPointResult.nResult].szText);
	SetItemText(nRow, CP_W_Result, strText);

	if (pstCamInfo->stCenterPoint[m_nTestIndex].stCenterPointResult.nResult == TER_Init)
	{
		strText.Format(_T("X"));
		SetItemText(nRow, CP_W_CenterX, strText);

		strText.Format(_T("X"));
		SetItemText(nRow, CP_W_CenterY, strText);

		strText.Format(_T("X"));
		SetItemText(nRow, CP_W_OffsetX, strText);

		strText.Format(_T("X"));
		SetItemText(nRow, CP_W_OffsetY, strText);

// 		strText.Format(_T("X"));
// 		SetItemText(nRow, CP_W_WriteX, strText);
// 
// 		strText.Format(_T("X"));
// 		SetItemText(nRow, CP_W_WriteY, strText);
	}
	else
	{
		strText.Format(_T("%d"), pstCamInfo->stCenterPoint[m_nTestIndex].stCenterPointResult.iValueX);
		SetItemText(nRow, CP_W_CenterX, strText);

		strText.Format(_T("%d"), pstCamInfo->stCenterPoint[m_nTestIndex].stCenterPointResult.iValueY);
		SetItemText(nRow, CP_W_CenterY, strText);

		strText.Format(_T("%d"), pstCamInfo->stCenterPoint[m_nTestIndex].stCenterPointResult.iResultOffsetX);
		SetItemText(nRow, CP_W_OffsetX, strText);

		strText.Format(_T("%d"), pstCamInfo->stCenterPoint[m_nTestIndex].stCenterPointResult.iResultOffsetY);
		SetItemText(nRow, CP_W_OffsetY, strText);

// 		strText.Format(_T("%d"), pstCamInfo->stCenterPoint[m_nTestIndex].stCenterPointResult.iWriteX);
// 		SetItemText(nRow, CP_W_WriteX, strText);
// 
// 		strText.Format(_T("%d"), pstCamInfo->stCenterPoint[m_nTestIndex].stCenterPointResult.iWriteY);
// 		SetItemText(nRow, CP_W_WriteY, strText);
	}
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:43
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = CP_W_MaxCol;
	CString temp[CP_W_MaxCol];
	for (int t = 0; t < CP_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
