﻿#ifndef List_Work_OperMode_h__
#define List_Work_OperMode_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_OperMode_Worklist
{
	OperMode_W_Recode,
	OperMode_W_Time,
	OperMode_W_Equipment,
	OperMode_W_Model,
	OperMode_W_SWVersion,
	OperMode_W_LOTNum,
	OperMode_W_Barcode,
	//OperMode_W_Operator,
	OperMode_W_Result,

	OperMode_W_Reset_15,
	OperMode_W_FpsCheck_15,
	OperMode_W_FpsSignal_15,
	OperMode_W_FpsCurrent_15,
	OperMode_W_Reset_30,
	OperMode_W_FpsCheck_30,
	OperMode_W_FpsSignal_30,
	OperMode_W_FpsCurrent_30,
	
	OperMode_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_OperMode_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),

	_T("15 fps Reset"),
	_T("15 fps Check"),
	_T("15 fps Signal"),
	_T("15 fps mA"),
	_T("30 fps Reset"),
	_T("30 fps Check"),
	_T("30 fps Signal"),
	_T("30 fps mA"),

	NULL
};

const int	iListAglin_OperMode_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_OperMode_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_OperMode

class CList_Work_OperMode : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_OperMode)

public:
	CList_Work_OperMode();
	virtual ~CList_Work_OperMode();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_OperMode_h__
