﻿//*****************************************************************************
// Filename	: Wnd_WorklistAngle.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistAngle_h__
#define Wnd_WorklistAngle_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_Angle.h"

//=============================================================================
// Wnd_WorklistAngle
//=============================================================================
class CWnd_WorklistAngle : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistAngle)

public:
	CWnd_WorklistAngle();
	virtual ~CWnd_WorklistAngle();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;


public:
	
	CList_Work_Angle			m_list_Angle[TICnt_FOV];
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistAngle_h__


