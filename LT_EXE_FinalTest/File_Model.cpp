﻿#include "stdafx.h"
#include "File_Model.h"
#include "Def_Enum.h"
#include "CommonFunction.h"


#define		Model_AppName		_T("Model")

#define		TotalStepInfo_AppName		_T("TotalStepInfo")
#define		AngleStepInfo_AppName		_T("AngleStepInfo")
#define		CenterPtStepInfo_AppName	_T("CenterPtStepInfo")
#define		ColorStepInfo_AppName		_T("ColorStepInfo")
#define		CurrentStepInfo_AppName		_T("CurrentStepInfo")
#define		FFTStepInfo_AppName			_T("FFTStepInfo")
#define		ParticleStepInfo_AppName	_T("ParticleStepInfo")
#define		BrightnessStepInfo_AppName	_T("BrightnessStepInfo")
#define		RotationStepInfo_AppName	_T("RotationStepInfo")
#define		SFRStepInfo_AppName			_T("SFRStepInfo")


#define		BarcodeInfo_AppName		_T("BarcodeInfo")
#define		UserConfigInfo_AppName	_T("OperatorInfo")

#define		Common_AppName			_T("Common")
#define		ModelCode_KeyName		_T("ModelCode")
#define		DaqGrab_AppName			_T("DAQ")
#define		Light_AppName			_T("Light")

#define		Angle_AppName			_T("Angle")
#define		CenterPoint_AppName		_T("CenterPoint")
#define		Color_AppName			_T("Color")
#define		Current_AppName			_T("Current")
#define		LED_AppName				_T("LED")
#define		FFT_AppName				_T("FFT")
#define		Particle_AppName		_T("Particle")
#define		DefectPixel_AppName		_T("DefectPixel")
#define		Brightness_AppName		_T("Brightness")
#define		SFR_AppName				_T("SFR")
#define		Rotate_AppName			_T("Rotate")

#define		EIAJ_AppName			_T("EIAJ")
#define		Reverse_AppName			_T("Reverse")
#define		PatternNoise_AppName	_T("PatternNoise")
#define		IRFilter_AppName		_T("IRFilter")
#define		Reset_AppName			_T("Reset")
#define		OperationMode_AppName	_T("OperationMode")
#define		LabelPrinter_AppName	_T("LabelPrinter")


CFile_Model::CFile_Model()
{
}

CFile_Model::~CFile_Model()
{
}

//=============================================================================
// Method		: LoadModelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 11:18
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadModelFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (!PathFileExists(szPath))
		return FALSE;

	BOOL bReturn = TRUE;

	bReturn = Load_Common(szPath, stModelInfo);
	bReturn &= Load_StepInfo(TotalStepInfo_AppName, szPath, stModelInfo.stStepInfo);

//	bReturn &= Load_RefBarcodeInfo(szPath, stModelInfo.stBarcodeInfo);

	bReturn &= LoadLightBrdFile(szPath, stModelInfo.stLightInfo);
	bReturn &= LoadLabelPrinterFile(szPath, stModelInfo);

	bReturn &= LoadCurrentOpFile(szPath, stModelInfo);
	bReturn &= LoadOperModeOpFile(szPath, stModelInfo);
	bReturn &= LoadCenterPointOpFile(szPath, stModelInfo);
	bReturn &= LoadRotateFile(szPath, stModelInfo);
	bReturn &= LoadEIAJOpFile(szPath, stModelInfo);
	bReturn &= LoadAngleOpFile(szPath, stModelInfo);
	bReturn &= LoadColorOpFile(szPath, stModelInfo);
	bReturn &= LoadReverseOpFile(szPath, stModelInfo);
	bReturn &= LoadResetOpFile(szPath, stModelInfo);
	bReturn &= LoadLEDOpFile(szPath, stModelInfo);


//	bReturn &= LoadParticleManualOpFile(szPath, stModelInfo);
	bReturn &= LoadPatternNoiseOpFile(szPath, stModelInfo);
	bReturn &= LoadIRFilterOpFile(szPath, stModelInfo);
// 	bReturn &= LoadFFTOpFile(szPath, stModelInfo);
 	bReturn &= LoadParticleOpFile(szPath, stModelInfo);
	bReturn &= LoadDefectPixelOpFile(szPath, stModelInfo);
 	bReturn &= LoadBrightnessOpFile(szPath, stModelInfo);
 	bReturn &= LoadSFRFile(szPath, stModelInfo);
	
	return TRUE;
}

//=============================================================================
// Method		: SaveModelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 11:26
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveModelFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstModelInfo)
		return FALSE;

	::DeleteFile(szPath);

	CString strValue;
	CString strKey;

	BOOL bReturn = TRUE;

	bReturn  = Save_Common(szPath, pstModelInfo);
	bReturn &= Save_StepInfo(TotalStepInfo_AppName, szPath, &pstModelInfo->stStepInfo);

	bReturn &= SaveLightBrdFile(szPath, &pstModelInfo->stLightInfo);
	bReturn &= SaveLabelPrinterFile(szPath, pstModelInfo);

	bReturn &= SaveCurrentOpFile(szPath, pstModelInfo);
	bReturn &= SaveOperModeOpFile(szPath, pstModelInfo);
	bReturn &= SaveCenterPointOpFile(szPath, pstModelInfo);
	bReturn &= SaveRotateFile(szPath, pstModelInfo);
	bReturn &= SaveEIAJOpFile(szPath, pstModelInfo);
	bReturn &= SaveAngleOpFile(szPath, pstModelInfo);
	bReturn &= SaveColorOpFile(szPath, pstModelInfo);
	bReturn &= SaveReverseOpFile(szPath, pstModelInfo);
	bReturn &= SaveResetOpFile(szPath, pstModelInfo);
	bReturn &= SaveLEDOpFile(szPath, pstModelInfo);

// 	bReturn &= SaveParticleManualOpFile(szPath, pstModelInfo);
	bReturn &= SavePatternNoiseOpFile(szPath, pstModelInfo);
	bReturn &= SaveIRFilterOpFile(szPath, pstModelInfo);
// 	bReturn &= SaveFFTOpFile(szPath, pstModelInfo);
 	bReturn &= SaveParticleOpFile(szPath, pstModelInfo);
	bReturn &= SaveDefectPixelOpFile(szPath, pstModelInfo);
 	bReturn &= SaveBrightnessOpFile(szPath, pstModelInfo);
 	bReturn &= SaveSFRFile(szPath, pstModelInfo);

	return TRUE;
}

//=============================================================================
// Method		: Load_Default
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:55
// Desc.		:
//=============================================================================
BOOL CFile_Model::Load_Common(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue;
	CString strKeyName;

	// 모델명
	GetPrivateProfileString(Common_AppName, ModelCode_KeyName, _T("Default"), inBuff, 255, szPath);
	stModelInfo.szModelCode = inBuff;

	// 카메라 전압 
	GetPrivateProfileString(Common_AppName, _T("CameraVoltage_1"), _T("12"), inBuff, 80, szPath);
	stModelInfo.fVoltage[0] = (FLOAT)_ttof(inBuff);

	GetPrivateProfileString(Common_AppName, _T("CameraVoltage_2"), _T("9"), inBuff, 80, szPath);
	stModelInfo.fVoltage[1] = (FLOAT)_ttof(inBuff);

	// 카메라 Cover 사용
	GetPrivateProfileString(Common_AppName, _T("CoverUse"), _T("0"), inBuff, 255, szPath);
	stModelInfo.nCurtainType = _ttoi(inBuff);

	// 카메라 안정화 시간 
	GetPrivateProfileString(Common_AppName, _T("CameraDelay"), _T("0"), inBuff, 80, szPath);
	stModelInfo.nCameraDelay = _ttoi(inBuff);

	// Use Open Short
	GetPrivateProfileString(Common_AppName, _T("UseOpenShort"), _T("1"), inBuff, 80, szPath);
	stModelInfo.bUseOpenShort = _ttoi(inBuff);

	// Bin File
	GetPrivateProfileString(Common_AppName, _T("BinFile"), _T(""), inBuff, 255, szPath);
	stModelInfo.szBinFile = inBuff;


	// I2C File
	GetPrivateProfileString(Common_AppName, _T("szI2CFile_1"), _T(""), inBuff, 255, szPath);
	stModelInfo.szI2CFile_1 = inBuff;

	GetPrivateProfileString(Common_AppName, _T("szI2CFile_2"), _T(""), inBuff, 255, szPath);
	stModelInfo.szI2CFile_2 = inBuff;

	// Pogo 설정 파일
	GetPrivateProfileString(Common_AppName, _T("PogoFile"), _T(""), inBuff, 255, szPath);
	stModelInfo.szPogoName = inBuff;

	// 카메라 역상 상태
	GetPrivateProfileString(Common_AppName, _T("CameraType"), _T("1"), inBuff, 255, szPath);
	stModelInfo.nCameraType = _ttoi(inBuff);

	// 아날로그 영상 주파수 방식
	GetPrivateProfileString(Common_AppName, _T("VideoType"), _T("0"), inBuff, 255, szPath);
	stModelInfo.nVideoType = _ttoi(inBuff);

	// 아날로그, 디지털 그래버 종류
	GetPrivateProfileString(Common_AppName, _T("GrabType"), _T("0"), inBuff, 255, szPath);
	stModelInfo.nGrabType = _ttoi(inBuff);

	// 카메라 Width
	GetPrivateProfileString(Common_AppName, _T("Width"), _T("720"), inBuff, 255, szPath);
	stModelInfo.dwWidth = _ttoi(inBuff);

	// 카메라 Height
	GetPrivateProfileString(Common_AppName, _T("Height"), _T("480"), inBuff, 255, szPath);
	stModelInfo.dwHeight = _ttoi(inBuff);

	// 검사 거리
	GetPrivateProfileString(Common_AppName, _T("dbTestDistance"), _T("0"), inBuff, 255, szPath);
	stModelInfo.dbTestDistance = _ttof(inBuff);	
	// 검사 X 축
	GetPrivateProfileString(Common_AppName, _T("dbTestAxisX"), _T("0"), inBuff, 255, szPath);
	stModelInfo.dbTestAxisX = _ttof(inBuff);
	// 검사 Y 측
	GetPrivateProfileString(Common_AppName, _T("dbTestAxisY"), _T("0"), inBuff, 255, szPath);
	stModelInfo.dbTestAxisY = _ttof(inBuff);

	// 왜곡 보정
	GetPrivateProfileString(Common_AppName, _T("nCameraDistortion"), _T("0"), inBuff, 255, szPath);
	stModelInfo.nCameraDistortion = _ttoi(inBuff);

	// 테스트 항목
	stModelInfo.TestItemz.RemoveAll();

	GetPrivateProfileString(_T("TestItem"), _T("TestCount"), _T("0"), inBuff, 80, szPath);
	INT iCnt = _ttoi(inBuff);
	UINT iTestID = 0;
	for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
	{
		strKeyName.Format(_T("Test_%02d"), nIdx);

		GetPrivateProfileString(_T("TestItem"), strKeyName, _T(""), inBuff, 80, szPath);
		iTestID = (UINT)_ttoi(inBuff);
	
		stModelInfo.TestItemz.Add(iTestID);
	}

	GetPrivateProfileString(Common_AppName, _T("MasterSpcX"), _T("5"), inBuff, 80, szPath);
	stModelInfo.nMasterSpcX = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("MasterSpcY"), _T("5"), inBuff, 80, szPath);
	stModelInfo.nMasterSpcY = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("MasterSpcR"), _T("0.5"), inBuff, 80, szPath);
	stModelInfo.dbMasterSpcR = _ttof(inBuff);

	GetPrivateProfileString(Common_AppName, _T("MasterInfoX"), _T("0"), inBuff, 80, szPath);
	stModelInfo.iMasterInfoX = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("MasterInfoY"), _T("0"), inBuff, 80, szPath);
	stModelInfo.iMasterInfoY = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("MasterInfoR"), _T("0.0"), inBuff, 80, szPath);
	stModelInfo.dbMasterInfoR = _ttof(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: Save_Default
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:55
// Desc.		:
//=============================================================================
BOOL CFile_Model::Save_Common(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strKeyName;

	// Version, Format
	strValue.Format(_T("%s %s"), g_szProgramName[SET_INSPECTOR], g_szInsptrSysType[SET_INSPECTOR]);
	WritePrivateProfileString(_T("VersionInfo"), _T("Equipment"), strValue, szPath);
	strValue.Format(_T("%s (%s)"), GetVersionInfo(_T("ProductVersion")), GetVersionInfo(_T("FileVersion")));
	WritePrivateProfileString(_T("VersionInfo"), _T("SWVersion"), strValue, szPath);

	// 모델명
	strValue = pstModelInfo->szModelCode;
	WritePrivateProfileString(Common_AppName, ModelCode_KeyName, strValue, szPath);

	// 카메라 전압 
	strValue.Format(_T("%.2f"), pstModelInfo->fVoltage[0]);
	WritePrivateProfileString(Common_AppName, _T("CameraVoltage_1"), strValue, szPath);

	// 카메라 전압 
	strValue.Format(_T("%.2f"), pstModelInfo->fVoltage[1]);
	WritePrivateProfileString(Common_AppName, _T("CameraVoltage_2"), strValue, szPath);

	// 카메라 Cover 사용
	strValue.Format(_T("%d"), pstModelInfo->nCurtainType);
	WritePrivateProfileString(Common_AppName, _T("CoverUse"), strValue, szPath);

	// 카메라 안정화 시간 
	strValue.Format(_T("%d"), pstModelInfo->nCameraDelay);
	WritePrivateProfileString(Common_AppName, _T("CameraDelay"), strValue, szPath);

	// Use Open Short
	strValue.Format(_T("%d"), pstModelInfo->bUseOpenShort);
	WritePrivateProfileString(Common_AppName, _T("UseOpenShort"), strValue, szPath);

	// Bin File
	strValue = pstModelInfo->szBinFile;
	WritePrivateProfileString(Common_AppName, _T("BinFile"), strValue, szPath);

	// I2C File
	strValue = pstModelInfo->szI2CFile_1;
	WritePrivateProfileString(Common_AppName, _T("szI2CFile_1"), strValue, szPath);

	strValue = pstModelInfo->szI2CFile_2;
	WritePrivateProfileString(Common_AppName, _T("szI2CFile_2"), strValue, szPath);

	// Pogo 설정 파일
	strValue = pstModelInfo->szPogoName;
	WritePrivateProfileString(Common_AppName, _T("PogoFile"), strValue, szPath);

	// 카메라 역상 상태
	strValue.Format(_T("%d"), pstModelInfo->nCameraType);
	WritePrivateProfileString(Common_AppName, _T("CameraType"), strValue, szPath);

	// 아날로그 영상 주파수 방식
	strValue.Format(_T("%d"), pstModelInfo->nVideoType);
	WritePrivateProfileString(Common_AppName, _T("VideoType"), strValue, szPath);

	// 아날로그, 디지털 그래버 종류
	strValue.Format(_T("%d"), pstModelInfo->nGrabType);
	WritePrivateProfileString(Common_AppName, _T("GrabType"), strValue, szPath);

	// 카메라 Width
	strValue.Format(_T("%d"), pstModelInfo->dwWidth);
	WritePrivateProfileString(Common_AppName, _T("Width"), strValue, szPath);

	// 카메라 Height
	strValue.Format(_T("%d"), pstModelInfo->dwHeight);
	WritePrivateProfileString(Common_AppName, _T("Height"), strValue, szPath);

	// 검사 거리
	strValue.Format(_T("%.2f"), pstModelInfo->dbTestDistance);
	WritePrivateProfileString(Common_AppName, _T("dbTestDistance"), strValue, szPath);
	// 검사 X 축
	strValue.Format(_T("%.2f"), pstModelInfo->dbTestAxisX);
	WritePrivateProfileString(Common_AppName, _T("dbTestAxisX"), strValue, szPath);
	// 검사 Y 축
	strValue.Format(_T("%.2f"), pstModelInfo->dbTestAxisY);
	WritePrivateProfileString(Common_AppName, _T("dbTestAxisY"), strValue, szPath);

	// 검사 거리
	strValue.Format(_T("%d"), pstModelInfo->nCameraDistortion);
	WritePrivateProfileString(Common_AppName, _T("nCameraDistortion"), strValue, szPath);

	// 테스트 항목
	INT_PTR iCnt = pstModelInfo->TestItemz.GetCount();

	strValue.Format(_T("%d"), iCnt);
	WritePrivateProfileString(_T("TestItem"), _T("TestCount"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
	{
		strKeyName.Format(_T("Test_%02d"), nIdx);
		strValue.Format(_T("%d"), pstModelInfo->TestItemz.GetAt(nIdx));

		WritePrivateProfileString(_T("TestItem"), strKeyName, strValue, szPath);
	}

	strValue.Format(_T("%d"), pstModelInfo->nMasterSpcX);
	WritePrivateProfileString(Common_AppName, _T("MasterSpcX"), strValue, szPath);

	strValue.Format(_T("%d"), pstModelInfo->nMasterSpcY);
	WritePrivateProfileString(Common_AppName, _T("MasterSpcY"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstModelInfo->dbMasterSpcR);
	WritePrivateProfileString(Common_AppName, _T("MasterSpcR"), strValue, szPath);

	strValue.Format(_T("%d"), pstModelInfo->iMasterInfoX);
	WritePrivateProfileString(Common_AppName, _T("MasterInfoX"), strValue, szPath);

	strValue.Format(_T("%d"), pstModelInfo->iMasterInfoY);
	WritePrivateProfileString(Common_AppName, _T("MasterInfoY"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstModelInfo->dbMasterInfoR);
	WritePrivateProfileString(Common_AppName, _T("MasterInfoR"), strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_StepInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_StepInfo & stStepInfo
// Qualifier	:
// Last Update	: 2018/1/2 - 16:00
// Desc.		:
//=============================================================================
BOOL CFile_Model::Load_StepInfo(__in LPCTSTR szSaveItem, __in LPCTSTR szPath, __out ST_StepInfo& stStepInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (!PathFileExists(szPath))
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strAppName;
	CString strKeyName;

	// 기존 스텝 정보 삭제
	stStepInfo.RemoveAll();

	GetPrivateProfileString(szSaveItem, _T("ItemCount"), _T(""), inBuff, 255, szPath);
	int iItemCount = _ttoi(inBuff);

	for (INT nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		strAppName.Format(_T("%s_%03d"), szSaveItem, nIdx);

		ST_StepUnit stStepUnit;

		GetPrivateProfileString(strAppName, _T("MasterUse"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bMasterUse = _ttoi(inBuff);

		// Test Enable
		GetPrivateProfileString(strAppName, _T("TestUse"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bTestUse = _ttoi(inBuff);

		// Test Item
		GetPrivateProfileString(strAppName, _T("TestItem"), _T(""), inBuff, 255, szPath);
		stStepUnit.nTestItem = _ttoi(inBuff);

		// Test Item
		GetPrivateProfileString(strAppName, _T("TestItemCnt"), _T(""), inBuff, 255, szPath);
		stStepUnit.nTestItemCnt = _ttoi(inBuff);

		// Retry Count
		GetPrivateProfileString(strAppName, _T("RetryCnt"), _T("0"), inBuff, 255, szPath);
		stStepUnit.nRetryCnt = _ttoi(inBuff);

		// Delay
		GetPrivateProfileString(strAppName, _T("Delay"), _T("0"), inBuff, 255, szPath);
		stStepUnit.dwDelay = _ttoi(inBuff);

		// Axis Enable
		GetPrivateProfileString(strAppName, _T("AxisUse"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bAxisUse = _ttoi(inBuff);

		// Move Axis
		GetPrivateProfileString(strAppName, _T("Axis"), _T("0"), inBuff, 255, szPath);
		stStepUnit.nAxisItem = _ttoi(inBuff);

		// Move Pos
		GetPrivateProfileString(strAppName, _T("MovePos"), _T("0"), inBuff, 255, szPath);
		stStepUnit.dbMovePos = _ttof(inBuff);

		// IO Enable
		GetPrivateProfileString(strAppName, _T("IOUse"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bIOUse = _ttoi(inBuff);

		// IO Port
		GetPrivateProfileString(strAppName, _T("IOPort"), _T("0"), inBuff, 255, szPath);
		stStepUnit.nIOItem = _ttoi(inBuff);

		// IO Signal
		GetPrivateProfileString(strAppName, _T("IOSignal"), _T("0"), inBuff, 255, szPath);
		stStepUnit.nIOSignal = _ttof(inBuff);

		stStepInfo.StepList.Add(stStepUnit);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_StepInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_StepInfo * pstStepInfo
// Qualifier	:
// Last Update	: 2018/1/2 - 16:00
// Desc.		:
//=============================================================================
BOOL CFile_Model::Save_StepInfo(__in LPCTSTR szSaveItem, __in LPCTSTR szPath, __in const ST_StepInfo* pstStepInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	INT_PTR iItemCount = pstStepInfo->StepList.GetCount();
	strValue.Format(_T("%d"), iItemCount);
	WritePrivateProfileString(szSaveItem, _T("ItemCount"), strValue, szPath);

	for (INT_PTR nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		strAppName.Format(_T("%s_%03d"), szSaveItem, nIdx);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bMasterUse);
		WritePrivateProfileString(strAppName, _T("MasterUse"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bTestUse);
		WritePrivateProfileString(strAppName, _T("TestUse"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nTestItem);
		WritePrivateProfileString(strAppName, _T("TestItem"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nTestItemCnt);
		WritePrivateProfileString(strAppName, _T("TestItemCnt"), strValue, szPath);

		// Retry Count
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nRetryCnt);
		WritePrivateProfileString(strAppName, _T("RetryCnt"), strValue, szPath);

		// Delay
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].dwDelay);
		WritePrivateProfileString(strAppName, _T("Delay"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bAxisUse);
		WritePrivateProfileString(strAppName, _T("AxisUse"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nAxisItem);
		WritePrivateProfileString(strAppName, _T("Axis"), strValue, szPath);

		strValue.Format(_T("%.1f"), pstStepInfo->StepList[nIdx].dbMovePos);
		WritePrivateProfileString(strAppName, _T("MovePos"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bIOUse);
		WritePrivateProfileString(strAppName, _T("IOUse"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nIOItem);
		WritePrivateProfileString(strAppName, _T("IOPort"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nIOSignal);
		WritePrivateProfileString(strAppName, _T("IOSignal"), strValue, szPath);
	}

	return TRUE;
}

BOOL CFile_Model::Load_RefBarcodeInfo(__in LPCTSTR szPath, __out ST_BarcodeRefInfo& stBarcodeInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (!PathFileExists(szPath))
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strAppName;
	CString strKeyName;

	// 기존 스텝 정보 삭제
	stBarcodeInfo.RemoveAll();

	GetPrivateProfileString(BarcodeInfo_AppName, _T("ItemCount"), _T(""), inBuff, 255, szPath);
	int iItemCount = _ttoi(inBuff);

	for (INT nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		strAppName.Format(_T("%s_%03d"), BarcodeInfo_AppName, nIdx);

		ST_BarcodeRef stBarcodeRef;

		// Test Enable
		GetPrivateProfileString(strAppName, _T("ModelName"), _T(""), inBuff, 255, szPath);
		stBarcodeRef.szModelName = inBuff;

		// Test Item
		GetPrivateProfileString(strAppName, _T("RefBarcode"), _T(""), inBuff, 255, szPath);
		stBarcodeRef.szrefBarcode = inBuff;

		stBarcodeInfo.BarcodeList.Add(stBarcodeRef);
	}

	return TRUE;
}

BOOL CFile_Model::Save_RefBarcodeInfo(__in LPCTSTR szPath, __in const ST_BarcodeRefInfo* pstBarcodeInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	INT_PTR iItemCount = pstBarcodeInfo->BarcodeList.GetCount();

	strValue.Format(_T("%d"), iItemCount);
	WritePrivateProfileString(BarcodeInfo_AppName, _T("ItemCount"), strValue, szPath);

	for (INT_PTR nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		strAppName.Format(_T("%s_%03d"), BarcodeInfo_AppName, nIdx);

		strValue.Format(_T("%s"), pstBarcodeInfo->BarcodeList[nIdx].szrefBarcode);
		WritePrivateProfileString(strAppName, _T("RefBarcode"), strValue, szPath);

		strValue.Format(_T("%s"), pstBarcodeInfo->BarcodeList[nIdx].szModelName);
		WritePrivateProfileString(strAppName, _T("ModelName"), strValue, szPath);
		
	}

	return TRUE;
}


BOOL CFile_Model::Load_UserConfigInfo(__in LPCTSTR szPath, __out ST_UserConfigInfo& stUserConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (!PathFileExists(szPath))
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strAppName;
	CString strKeyName;

	// 기존 스텝 정보 삭제
	stUserConfigInfo.RemoveAll();

	GetPrivateProfileString(UserConfigInfo_AppName, _T("ItemCount"), _T(""), inBuff, 255, szPath);
	int iItemCount = _ttoi(inBuff);

	for (INT nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		strAppName.Format(_T("%s_%03d"), UserConfigInfo_AppName, nIdx);

		ST_UserConfig stUserConfig;

		GetPrivateProfileString(strAppName, _T("RegistrationTime"), _T(""), inBuff, 255, szPath);
		stUserConfig.szRegistrationTime = inBuff;

		GetPrivateProfileString(strAppName, _T("OperatorID"), _T(""), inBuff, 255, szPath);
		stUserConfig.szOperatorID = inBuff;

		stUserConfigInfo.Operatorlist.Add(stUserConfig);
	}

	return TRUE;
}

BOOL CFile_Model::Save_UserConfigInfo(__in LPCTSTR szPath, __in const ST_UserConfigInfo* pstUserConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	INT_PTR iItemCount = pstUserConfigInfo->Operatorlist.GetCount();

	strValue.Format(_T("%d"), iItemCount);
	WritePrivateProfileString(UserConfigInfo_AppName, _T("ItemCount"), strValue, szPath);

	for (INT_PTR nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		strAppName.Format(_T("%s_%03d"), UserConfigInfo_AppName, nIdx);

		strValue.Format(_T("%s"), pstUserConfigInfo->Operatorlist[nIdx].szRegistrationTime);
		WritePrivateProfileString(strAppName, _T("RegistrationTime"), strValue, szPath);

		strValue.Format(_T("%s"), pstUserConfigInfo->Operatorlist[nIdx].szOperatorID);
		WritePrivateProfileString(strAppName, _T("OperatorID"), strValue, szPath);

	}

	return TRUE;
}


//=============================================================================
// Method		: LoadCurrentOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/12/31 - 19:19
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadCurrentOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_Current; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), Current_AppName, nItemCnt);

		for (int iCh = 0; iCh < Def_CamBrd_Channel; iCh++)
		{
			strKeyName.Format(_T("Channel_%d_Min_%d"), iCh + 1, nItemCnt);
			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
			stModelInfo.stCurrent[nItemCnt].stCurrentOpt.nSpecMin[iCh] = _ttof(inBuff);

			strKeyName.Format(_T("Channel_%d_Max_%d"), iCh + 1, nItemCnt);
			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
			stModelInfo.stCurrent[nItemCnt].stCurrentOpt.nSpecMax[iCh] = _ttof(inBuff);

			strKeyName.Format(_T("Channel_%d_Offset_%d"), iCh + 1, nItemCnt);
			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
			stModelInfo.stCurrent[nItemCnt].stCurrentOpt.dbOffset[iCh] = _ttof(inBuff);
		}

	}

	return TRUE;
}

//=============================================================================
// Method		: SaveCurrentOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2017/12/31 - 19:28
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveCurrentOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_Current; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), Current_AppName, nItemCnt);

		for (int iCh = 0; iCh < Def_CamBrd_Channel; iCh++)
		{
			strKeyName.Format(_T("Channel_%d_Min_%d"), iCh + 1, nItemCnt);
			strValue.Format(_T("%.1f"), pstModelInfo->stCurrent[nItemCnt].stCurrentOpt.nSpecMin[iCh]);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("Channel_%d_Max_%d"), iCh + 1, nItemCnt);
			strValue.Format(_T("%.1f"), pstModelInfo->stCurrent[nItemCnt].stCurrentOpt.nSpecMax[iCh]);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("Channel_%d_Offset_%d"), iCh + 1, nItemCnt);
			strValue.Format(_T("%.2f"), pstModelInfo->stCurrent[nItemCnt].stCurrentOpt.dbOffset[iCh]);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
		}
	}

	return TRUE;
}

BOOL CFile_Model::LoadOperModeOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	//if (NULL == szPath)
	//	return FALSE;

	//TCHAR   inBuff[255] = { 0, };

	//CString strValue;
	//CString strAppName;
	//CString strKeyName;

	//for (UINT nItemCnt = 0; nItemCnt < TICnt_OperationMode; nItemCnt++)
	//{
	//	strAppName.Format(_T("%s_%d"), OperationMode_AppName, nItemCnt);

	//	GetPrivateProfileString(strAppName, _T("nResetDelay"), _T("1000"), inBuff, 255, szPath);
	//	stModelInfo.stOperMode[nItemCnt].nResetDelay = _ttoi(inBuff);

	//	for (int iCh = 0; iCh < 2; iCh++)
	//	{
	//		strKeyName.Format(_T("Channel_%d_Min_%d"), iCh + 1, nItemCnt);
	//		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
	//		stModelInfo.stOperMode[nItemCnt].stCurrOpt[iCh].nSpecMin[0] = _ttof(inBuff);

	//		strKeyName.Format(_T("Channel_%d_Max_%d"), iCh + 1, nItemCnt);
	//		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
	//		stModelInfo.stOperMode[nItemCnt].stCurrOpt[iCh].nSpecMax[0] = _ttof(inBuff);

	//		strKeyName.Format(_T("Channel_%d_Offset_%d"), iCh + 1, nItemCnt);
	//		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
	//		stModelInfo.stOperMode[nItemCnt].stCurrOpt[iCh].dbOffset[0] = _ttof(inBuff);
	//	}

	//}

	return TRUE;
}

BOOL CFile_Model::SaveOperModeOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	//if (NULL == szPath)
	//	return FALSE;

	//CString strValue;
	//CString strAppName;
	//CString strKeyName;

	//for (UINT nItemCnt = 0; nItemCnt < TICnt_OperationMode; nItemCnt++)
	//{
	//	strAppName.Format(_T("%s_%d"), OperationMode_AppName, nItemCnt);

	//	strValue.Format(_T("%d"), pstModelInfo->stOperMode[nItemCnt].nResetDelay);
	//	WritePrivateProfileString(strAppName, _T("nResetDelay"), strValue, szPath);

	//	for (int iCh = 0; iCh < 2; iCh++)
	//	{
	//		strKeyName.Format(_T("Channel_%d_Min_%d"), iCh + 1, nItemCnt);
	//		strValue.Format(_T("%.1f"), pstModelInfo->stOperMode[nItemCnt].stCurrOpt[iCh].nSpecMin[0]);
	//		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

	//		strKeyName.Format(_T("Channel_%d_Max_%d"), iCh + 1, nItemCnt);
	//		strValue.Format(_T("%.1f"), pstModelInfo->stOperMode[nItemCnt].stCurrOpt[iCh].nSpecMax[0]);
	//		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

	//		strKeyName.Format(_T("Channel_%d_Offset_%d"), iCh + 1, nItemCnt);
	//		strValue.Format(_T("%.2f"), pstModelInfo->stOperMode[nItemCnt].stCurrOpt[iCh].dbOffset[0]);
	//		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
	//	}
	//}

	return TRUE;
}

BOOL CFile_Model::LoadLEDOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	//if (NULL == szPath)
	//	return FALSE;

	//TCHAR   inBuff[255] = { 0, };

	//CString strValue;
	//CString strAppName;
	//CString strKeyName;

	//for (UINT nItemCnt = 0; nItemCnt < TICnt_LEDTest; nItemCnt++)
	//{
	//	strAppName.Format(_T("%s_%d"), LED_AppName, nItemCnt);
	//	
	//	GetPrivateProfileString(strAppName, _T("nResetCnt"), strValue, inBuff, 255, szPath);
	//	stModelInfo.stLED[nItemCnt].nResetCnt = _ttoi(inBuff);

	//	GetPrivateProfileString(strAppName, _T("nResetDelay"), strValue, inBuff, 255, szPath);
	//	stModelInfo.stLED[nItemCnt].nResetDelay = _ttoi(inBuff);

	//	for (int iCh = 0; iCh < Def_CamBrd_Channel; iCh++)
	//	{
	//		strKeyName.Format(_T("Channel_%d_Min_%d"), iCh + 1, nItemCnt);
	//		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
	//		stModelInfo.stLED[nItemCnt].stLEDCurrOpt.nSpecMin[iCh] = _ttof(inBuff);

	//		strKeyName.Format(_T("Channel_%d_Max_%d"), iCh + 1, nItemCnt);
	//		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
	//		stModelInfo.stLED[nItemCnt].stLEDCurrOpt.nSpecMax[iCh] = _ttof(inBuff);

	//		strKeyName.Format(_T("Channel_%d_Offset_%d"), iCh + 1, nItemCnt);
	//		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
	//		stModelInfo.stLED[nItemCnt].stLEDCurrOpt.dbOffset[iCh] = _ttof(inBuff);
	//	}

	//}

	return TRUE;
}

BOOL CFile_Model::SaveLEDOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	/*if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_LEDTest; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), LED_AppName, nItemCnt);

		strValue.Format(_T("%d"), pstModelInfo->stLED[nItemCnt].nResetCnt);
		WritePrivateProfileString(strAppName, _T("nResetCnt"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stLED[nItemCnt].nResetDelay);
		WritePrivateProfileString(strAppName, _T("nResetDelay"), strValue, szPath);

		for (int iCh = 0; iCh < Def_CamBrd_Channel; iCh++)
		{
			strKeyName.Format(_T("Channel_%d_Min_%d"), iCh + 1, nItemCnt);
			strValue.Format(_T("%.1f"), pstModelInfo->stLED[nItemCnt].stLEDCurrOpt.nSpecMin[iCh]);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("Channel_%d_Max_%d"), iCh + 1, nItemCnt);
			strValue.Format(_T("%.1f"), pstModelInfo->stLED[nItemCnt].stLEDCurrOpt.nSpecMax[iCh]);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("Channel_%d_Offset_%d"), iCh + 1, nItemCnt);
			strValue.Format(_T("%.2f"), pstModelInfo->stLED[nItemCnt].stLEDCurrOpt.dbOffset[iCh]);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
		}
	}
*/
	return TRUE;
}

BOOL CFile_Model::LoadEIAJOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if(NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue;
	CString strAppName;
	CString strKeyName;



	for (UINT nItemCnt = 0; nItemCnt < TICnt_EIAJ; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), EIAJ_AppName, nItemCnt);
		GetPrivateProfileString(strAppName, _T("GViewMode"), _T("0"), inBuff, 80, szPath);
		stModelInfo.stEIAJ[nItemCnt].stEIAJOp.bGViewMode = _ttoi(inBuff);
		for (UINT nROI = 0; nROI < ReOp_ItemNum; nROI++)
		{
			strKeyName.Format(_T("%d_Enable"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].bUse = _ttoi(inBuff);
			stModelInfo.stEIAJ[nItemCnt].stEIAJData.bUse[nROI] = _ttoi(inBuff);

			strKeyName.Format(_T("%d_left"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].RegionList.left = _ttoi(inBuff);

			strKeyName.Format(_T("%d_top"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].RegionList.top = _ttoi(inBuff);

			strKeyName.Format(_T("%d_right"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].RegionList.right = _ttoi(inBuff);

			strKeyName.Format(_T("%d_bottom"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].RegionList.bottom = _ttoi(inBuff);

			strKeyName.Format(_T("%d_MTF"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].i_MtfRatio = _ttoi(inBuff);

			strKeyName.Format(_T("%d_MinSpc"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].i_Threshold_Min = _ttoi(inBuff);

			strKeyName.Format(_T("%d_MaxSpc"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].i_Threshold_Max = _ttoi(inBuff);

			strKeyName.Format(_T("%d_MinRange"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].i_Range_Min = _ttoi(inBuff);

			strKeyName.Format(_T("%d_MaxRange"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].i_Range_Max = _ttoi(inBuff);

			strKeyName.Format(_T("%d_Offset"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].i_offset = _ttoi(inBuff);

			strKeyName.Format(_T("%d_TestMode"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI].i_Mode = _ttoi(inBuff);

			stModelInfo.stEIAJ[nItemCnt].stEIAJOp.rectData[nROI] = stModelInfo.stEIAJ[nItemCnt].stEIAJOp.StdrectData[nROI];
		}

	}

	return TRUE;
}

BOOL CFile_Model::SaveEIAJOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_EIAJ; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), EIAJ_AppName, nItemCnt);

		for (UINT nROI = 0; nROI < ReOp_ItemNum; nROI++)
		{
			strKeyName.Format(_T("%d_Enable"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].bUse);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_left"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].RegionList.left);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_top"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].RegionList.top);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_right"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].RegionList.right);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_bottom"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].RegionList.bottom);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);


			strKeyName.Format(_T("%d_MTF"), nROI);
			strValue.Format(_T("%.d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].i_MtfRatio);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_MinSpc"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].i_Threshold_Min);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_MaxSpc"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].i_Threshold_Max);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_MinRange"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].i_Range_Min);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_MaxRange"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].i_Range_Max);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_Offset"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].i_offset);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_TestMode"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stEIAJ[nItemCnt].stEIAJOp.rectData[nROI].i_Mode);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
		}
	}


	return TRUE;
}

BOOL CFile_Model::LoadReverseOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_Reverse; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), Reverse_AppName, nItemCnt);

		strKeyName.Format(_T("STATE"));
		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
		stModelInfo.stReverse[nItemCnt].stReverseOpt.nStateType = _ttoi(inBuff);

		for (UINT nROI = 0; nROI < ROI_Rv_Max; nROI++)
		{
			strKeyName.Format(_T("%d_left"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].rtRoi.left = _ttoi(inBuff);

			strKeyName.Format(_T("%d_top"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].rtRoi.top = _ttoi(inBuff);

			strKeyName.Format(_T("%d_right"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].rtRoi.right = _ttoi(inBuff);

			strKeyName.Format(_T("%d_bottom"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].rtRoi.bottom = _ttoi(inBuff);

			strKeyName.Format(_T("%d_MarkColor"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].nMarkColor = _ttoi(inBuff);

			strKeyName.Format(_T("%d_CamState"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].nCamState = _ttoi(inBuff);

			stModelInfo.stReverse[nItemCnt].stReverseOpt.stTestRegionOp[nROI] = stModelInfo.stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI];

		}

		for (int i = 0; i < ROI_Rv_resultMax; i++)
		{
			for (int j = 0; j < ReverseRGB_Max; j++)
			{
				strKeyName.Format(_T("%d_%d_RGBType"), i,j);
				GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
				stModelInfo.stReverse[nItemCnt].stReverseOpt.stRegionSubOp[i].nRGBType[j] = _ttoi(inBuff);
			}
		}

		stModelInfo.stReverse[nItemCnt].stInitReverseOpt = stModelInfo.stReverse[nItemCnt].stReverseOpt;
	}

	return TRUE;
}

BOOL CFile_Model::SaveReverseOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_Reverse; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), Reverse_AppName, nItemCnt);

		strKeyName.Format(_T("STATE"));
		strValue.Format(_T("%d"), pstModelInfo->stReverse[nItemCnt].stReverseOpt.nStateType);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		for (UINT nROI = 0; nROI < ROI_Rv_Max; nROI++)
		{
			strKeyName.Format(_T("%d_left"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].rtRoi.left);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_top"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].rtRoi.top);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_right"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].rtRoi.right);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_bottom"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].rtRoi.bottom);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_MarkColor"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].nMarkColor);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_CamState"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stReverse[nItemCnt].stReverseOpt.stRegionOp[nROI].nCamState);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			
		}

		for (int i = 0; i < ROI_Rv_resultMax; i++)
		{
			for (int j = 0; j < ReverseRGB_Max; j++)
			{
				strKeyName.Format(_T("%d_%d_RGBType"), i, j);
				strValue.Format(_T("%d"), pstModelInfo->stReverse[nItemCnt].stReverseOpt.stRegionSubOp[i].nRGBType[j]);
				WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
			}
		}
	}

	return TRUE;
}


BOOL CFile_Model::LoadResetOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	//if (NULL == szPath)
	//	return FALSE;

	//TCHAR   inBuff[255] = { 0, };

	//CString strValue;
	//CString strAppName;
	//CString strKeyName;

	//for (UINT nItemCnt = 0; nItemCnt < TICnt_Reset; nItemCnt++)
	//{
	//	strAppName.Format(_T("%s_%d"), Reset_AppName, nItemCnt);

	//	strKeyName.Format(_T("nDelay"));
	//	GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
	//	stModelInfo.stReset[nItemCnt].nDelay = _ttoi(inBuff);

	//}

	return TRUE;
}


BOOL CFile_Model::SaveResetOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	//if (NULL == szPath)
	//	return FALSE;

	//CString strValue;
	//CString strAppName;
	//CString strKeyName;

	//for (UINT nItemCnt = 0; nItemCnt < TICnt_Reset; nItemCnt++)
	//{
	//	strAppName.Format(_T("%s_%d"), Reset_AppName, nItemCnt);

	//	strKeyName.Format(_T("nDelay"));
	//	strValue.Format(_T("%d"), pstModelInfo->stReset[nItemCnt].nDelay);
	//	WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
	//}

	return TRUE;
}


// BOOL CFile_Model::LoadParticleManualOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
// {
// 	if (NULL == szPath)
// 		return FALSE;
// 
// 	return TRUE;
// }
// 
// BOOL CFile_Model::SaveParticleManualOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
// {
// 	if (NULL == szPath)
// 		return FALSE;
// 
// 	return TRUE;
// }

BOOL CFile_Model::LoadPatternNoiseOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	//if (NULL == szPath)
	//	return FALSE;

	//TCHAR   inBuff[255] = { 0, };

	//CString strValue;
	//CString strAppName;
	//CString strKeyName;

	//for (UINT nItemCnt = 0; nItemCnt < TICnt_PatternNoise; nItemCnt++)
	//{
	//	strAppName.Format(_T("%s_%d"), PatternNoise_AppName, nItemCnt);

	//	GetPrivateProfileString(strAppName, _T("Thr_dB"), _T("0"), inBuff, 80, szPath);
	//	stModelInfo.stPatternNoise[nItemCnt].stPatternNoiseOpt.dThr_dB = _ttof(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Thr_Env"), _T("0"), inBuff, 80, szPath);
	//	stModelInfo.stPatternNoise[nItemCnt].stPatternNoiseOpt.dThr_Env = _ttof(inBuff);

	//	for (UINT nROI = 0; nROI < ROI_PN_MaxEnum; nROI++)
	//	{
	//		strKeyName.Format(_T("%d_left"), nROI);
	//		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
	//		stModelInfo.stPatternNoise[nItemCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi.left = _ttoi(inBuff);

	//		strKeyName.Format(_T("%d_top"), nROI);
	//		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
	//		stModelInfo.stPatternNoise[nItemCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi.top = _ttoi(inBuff);

	//		strKeyName.Format(_T("%d_right"), nROI);
	//		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
	//		stModelInfo.stPatternNoise[nItemCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi.right = _ttoi(inBuff);

	//		strKeyName.Format(_T("%d_bottom"), nROI);
	//		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
	//		stModelInfo.stPatternNoise[nItemCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi.bottom = _ttoi(inBuff);
	//	}

	//	stModelInfo.stPatternNoise[nItemCnt].stInitPatternNoiseOpt = stModelInfo.stPatternNoise[nItemCnt].stPatternNoiseOpt;
	//}

	return TRUE;
}

BOOL CFile_Model::SavePatternNoiseOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	//if (NULL == szPath)
	//	return FALSE;

	//CString strValue;
	//CString strAppName;
	//CString strKeyName;

	//for (UINT nItemCnt = 0; nItemCnt < TICnt_PatternNoise; nItemCnt++)
	//{
	//	strAppName.Format(_T("%s_%d"), PatternNoise_AppName, nItemCnt);

	//	strValue.Format(_T("%.2f"), pstModelInfo->stPatternNoise[nItemCnt].stPatternNoiseOpt.dThr_dB);
	//	WritePrivateProfileString(strAppName, _T("Thr_dB"), strValue, szPath);

	//	strValue.Format(_T("%.2f"), pstModelInfo->stPatternNoise[nItemCnt].stPatternNoiseOpt.dThr_Env);
	//	WritePrivateProfileString(strAppName, _T("Thr_Env"), strValue, szPath);

	//	for (UINT nROI = 0; nROI < ROI_PN_MaxEnum; nROI++)
	//	{
	//		strKeyName.Format(_T("%d_left"), nROI);
	//		strValue.Format(_T("%d"), pstModelInfo->stPatternNoise[nItemCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi.left);
	//		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

	//		strKeyName.Format(_T("%d_top"), nROI);
	//		strValue.Format(_T("%d"), pstModelInfo->stPatternNoise[nItemCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi.top);
	//		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

	//		strKeyName.Format(_T("%d_right"), nROI);
	//		strValue.Format(_T("%d"), pstModelInfo->stPatternNoise[nItemCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi.right);
	//		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

	//		strKeyName.Format(_T("%d_bottom"), nROI);
	//		strValue.Format(_T("%d"), pstModelInfo->stPatternNoise[nItemCnt].stPatternNoiseOpt.stRegionOp[nROI].rtRoi.bottom);
	//		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

	//	}
	//}

	return TRUE;
}

BOOL CFile_Model::LoadIRFilterOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_IRFilter; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), IRFilter_AppName, nItemCnt);

		GetPrivateProfileString(strAppName, _T("Thr_Test"), _T("0"), inBuff, 80, szPath);
		stModelInfo.stIRFilter[nItemCnt].stIRFilterOpt.dThreshold = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Thr_Env"), _T("0"), inBuff, 80, szPath);
		stModelInfo.stIRFilter[nItemCnt].stIRFilterOpt.dThr_Env = _ttof(inBuff);

	}

	return TRUE;
}

BOOL CFile_Model::SaveIRFilterOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_IRFilter; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), IRFilter_AppName, nItemCnt);

		strValue.Format(_T("%.2f"), pstModelInfo->stIRFilter[nItemCnt].stIRFilterOpt.dThreshold);
		WritePrivateProfileString(strAppName, _T("Thr_Test"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstModelInfo->stIRFilter[nItemCnt].stIRFilterOpt.dThr_Env);
		WritePrivateProfileString(strAppName, _T("Thr_Env"), strValue, szPath);

	}

	return TRUE;
}

 
 //=============================================================================
 // Method		: LoadAngleOpFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __out ST_ModelInfo & stModelInfo
 // Qualifier	:
 // Last Update	: 2017/12/31 - 17:17
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::LoadAngleOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	TCHAR   inBuff[255] = { 0, };
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_FOV; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Angle_AppName, nItemCnt);
 

		GetPrivateProfileString(strAppName, _T("SPEC"), strValue, inBuff, 255, szPath);
		stModelInfo.stAngle[nItemCnt].stAngleOpt.nSpecDev = _ttoi(inBuff);


 		for (UINT nIdx = 0; nIdx < IDX_AL_Max; nIdx++)
 		{
 			strKeyName.Format(_T("Min_%d"), nIdx);
 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
 			stModelInfo.stAngle[nItemCnt].stAngleOpt.nMinSpc[nIdx] = _ttoi(inBuff);
 
 			strKeyName.Format(_T("Max_%d"), nIdx);
 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
 			stModelInfo.stAngle[nItemCnt].stAngleOpt.nMaxSpc[nIdx] = _ttoi(inBuff);
 		}
 
 		for (UINT nROI = 0; nROI < ROI_AL_Max; nROI++)
 		{
 			strKeyName.Format(_T("%s_MarkColor"), g_szRoiAngle[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].nMarkColor = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_left"), g_szRoiAngle[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].rtRoi.left = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_top"), g_szRoiAngle[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].rtRoi.top = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_right"), g_szRoiAngle[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].rtRoi.right = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_bottom"), g_szRoiAngle[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].rtRoi.bottom = _ttoi(inBuff);
			stModelInfo.stAngle[nItemCnt].stAngleOpt.stTestRegionOp[nROI] = stModelInfo.stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI];
 		}
 	}
 
 	return TRUE;
 }
 
 //=============================================================================
 // Method		: SaveAngleOpFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __in const ST_ModelInfo * pstModelInfo
 // Qualifier	:
 // Last Update	: 2017/12/31 - 17:35
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::SaveAngleOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_FOV; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Angle_AppName, nItemCnt);
 
		strValue.Format(_T("%d"), pstModelInfo->stAngle[nItemCnt].stAngleOpt.nSpecDev);
		WritePrivateProfileString(strAppName, _T("SPEC"), strValue, szPath);

 		for (UINT nIdx = 0; nIdx < IDX_AL_Max; nIdx++)
 		{
 			strKeyName.Format(_T("Min_%d"), nIdx);
 			strValue.Format(_T("%d"), pstModelInfo->stAngle[nItemCnt].stAngleOpt.nMinSpc[nIdx]);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("Max_%d"), nIdx);
 			strValue.Format(_T("%d"), pstModelInfo->stAngle[nItemCnt].stAngleOpt.nMaxSpc[nIdx]);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 		}
 
 		for (UINT nROI = 0; nROI < ROI_AL_Max; nROI++)
 		{
 			strKeyName.Format(_T("%s_MarkColor"), g_szRoiAngle[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].nMarkColor);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_left"), g_szRoiAngle[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].rtRoi.left);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_top"), g_szRoiAngle[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].rtRoi.top);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_right"), g_szRoiAngle[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].rtRoi.right);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_bottom"), g_szRoiAngle[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stAngle[nItemCnt].stAngleOpt.stRegionOp[nROI].rtRoi.bottom);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 		}
 	}
 
 	return TRUE;
 }
 

//=============================================================================
// Method		: LoadCenterPointOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/12/31 - 17:42
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadCenterPointOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_CenterPoint; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), CenterPoint_AppName, nItemCnt);

		strKeyName.Format(_T("TestMode"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.nTestMode = _ttoi(inBuff);

		strKeyName.Format(_T("MarkColor"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.nMarkColor = _ttoi(inBuff);

		strKeyName.Format(_T("RefAxisX"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.nRefAxisX = _ttoi(inBuff);

		strKeyName.Format(_T("RefAxisY"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.nRefAxisY = _ttoi(inBuff);

		strKeyName.Format(_T("DevOffsetX"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.iDevOffsetX = _ttoi(inBuff);

		strKeyName.Format(_T("DevOffsetY"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.iDevOffsetY = _ttoi(inBuff);

		strKeyName.Format(_T("OffsetX"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.iOffsetX = _ttoi(inBuff);

		strKeyName.Format(_T("OffsetY"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.iOffsetY = _ttoi(inBuff);

		strKeyName.Format(_T("TargetX"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.iTargetX = _ttoi(inBuff);

		strKeyName.Format(_T("TargetY"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.iTargetY = _ttoi(inBuff);

		strKeyName.Format(_T("TryCount"));
		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.nTryCnt = _ttoi(inBuff);

		strKeyName.Format(_T("left"));
		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.rtRoi.left = _ttoi(inBuff);

		strKeyName.Format(_T("top"));
		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.rtRoi.top = _ttoi(inBuff);

		strKeyName.Format(_T("right"));
		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.rtRoi.right = _ttoi(inBuff);

		strKeyName.Format(_T("bottom"));
		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
		stModelInfo.stCenterPoint[nItemCnt].stCenterPointOpt.rtRoi.bottom = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveCenterPointOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2017/12/31 - 17:49
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveCenterPointOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	for (UINT nItemCnt = 0; nItemCnt < TICnt_CenterPoint; nItemCnt++)
	{
		strAppName.Format(_T("%s_%d"), CenterPoint_AppName, nItemCnt);

		strKeyName.Format(_T("TestMode"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.nTestMode);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("MarkColor"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.nMarkColor);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("RefAxisX"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.nRefAxisX);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("RefAxisY"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.nRefAxisY);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("DevOffsetX"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.iDevOffsetX);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("DevOffsetY"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.iDevOffsetY);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("OffsetX"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.iOffsetX);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("OffsetY"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.iOffsetY);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("TargetX"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.iTargetX);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("TargetY"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.iTargetY);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("TryCount"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.nTryCnt);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("left"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.rtRoi.left);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("top"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.rtRoi.top);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("right"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.rtRoi.right);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strKeyName.Format(_T("bottom"));
		strValue.Format(_T("%d"), pstModelInfo->stCenterPoint[nItemCnt].stCenterPointOpt.rtRoi.bottom);
		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
	}

	return TRUE;
}

 
 //=============================================================================
 // Method		: LoadColorOpFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __out ST_ModelInfo & stModelInfo
 // Qualifier	:
 // Last Update	: 2017/12/31 - 18:25
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::LoadColorOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	TCHAR   inBuff[255] = { 0, };
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_Color; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Color_AppName, nItemCnt);
 
		GetPrivateProfileString(strAppName, _T("SPEC"), strValue, inBuff, 255, szPath);
		stModelInfo.stColor[nItemCnt].stColorOpt.nSpecDev = _ttoi(inBuff);

 		for (UINT nROI = 0; nROI < ROI_CL_Max; nROI++)
 		{
 			strKeyName.Format(_T("%s_MinR"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
 			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMinSpcR = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_MaxR"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
 			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMaxSpcR = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_MinG"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
 			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMinSpcG = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_MaxG"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
 			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMaxSpcG = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_MinB"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
 			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMinSpcB = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_MaxB"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
 			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMaxSpcB = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_left"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].rtRoi.left = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_top"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].rtRoi.top = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_right"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].rtRoi.right = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_bottom"), g_szRoiColor[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI].rtRoi.bottom = _ttoi(inBuff);
			stModelInfo.stColor[nItemCnt].stColorOpt.stTestRegionOp[nROI]=stModelInfo.stColor[nItemCnt].stColorOpt.stRegionOp[nROI];
 		}
 	}
 
 	return TRUE;
 }
 
 //=============================================================================
 // Method		: SaveColorOpFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __in const ST_ModelInfo * pstModelInfo
 // Qualifier	:
 // Last Update	: 2017/12/31 - 18:53
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::SaveColorOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_Color; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Color_AppName, nItemCnt);

		strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.nSpecDev);
		WritePrivateProfileString(strAppName, _T("SPEC"), strValue, szPath);

 
 		for (UINT nROI = 0; nROI < ROI_CL_Max; nROI++)
 		{
 			strKeyName.Format(_T("%s_MinR"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMinSpcR);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_MaxR"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMaxSpcR);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_MinG"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMinSpcG);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_MaxG"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMaxSpcG);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_MinB"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMinSpcB);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_MaxB"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].nMaxSpcB);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_left"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].rtRoi.left);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_top"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].rtRoi.top);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_right"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].rtRoi.right);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_bottom"), g_szRoiColor[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stColor[nItemCnt].stColorOpt.stRegionOp[nROI].rtRoi.bottom);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 		}
 	}
 
 	return TRUE;
 }
 


 // BOOL CFile_Model::LoadFFTOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
// {
// 	if (NULL == szPath)
// 		return FALSE;
// 
// 	TCHAR   inBuff[255] = { 0, };
// 
// 	CString strValue;
// 	CString strAppName;
// 	CString strKeyName;
// 
// 	for (UINT nItemCnt = 0; nItemCnt < TICnt_FFT; nItemCnt++)
// 	{
// 		strAppName.Format(_T("%s_%d"), FFT_AppName, nItemCnt);
// 
// 		for (int iCh = 0; iCh < FFTCh_Max; iCh++)
// 		{
// 			strKeyName.Format(_T("Channel_%d_Min_%d"), iCh + 1, nItemCnt);
// 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
// 			stModelInfo.stFFT[nItemCnt].stFFTOpt.dSpecMin[iCh] = _ttof(inBuff);
// 
// 			strKeyName.Format(_T("Channel_%d_Max_%d"), iCh + 1, nItemCnt);
// 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
// 			stModelInfo.stFFT[nItemCnt].stFFTOpt.dSpecMax[iCh] = _ttof(inBuff);
// 
// 			strKeyName.Format(_T("Channel_%d_Offset_%d"), iCh + 1, nItemCnt);
// 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
// 			stModelInfo.stFFT[nItemCnt].stFFTOpt.dbOffset[iCh] = _ttof(inBuff);
// 
// 			strKeyName.Format(_T("Channel_%d_TestTime_%d"), iCh + 1, nItemCnt);
// 			GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
// 			stModelInfo.stFFT[nItemCnt].stFFTOpt.nTestTime[iCh] = _ttoi(inBuff);
// 		}
// 		
// 
// 		strKeyName.Format(_T("TestVolume_%d"), nItemCnt);
// 		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
// 		stModelInfo.stFFT[nItemCnt].stFFTOpt.iVolume = _ttoi(inBuff);
// 
// 		strKeyName.Format(_T("WavFile_%d"), nItemCnt);
// 		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
// 		stModelInfo.stFFT[nItemCnt].stFFTOpt.szWaveFile = inBuff;
// 	}
// 
// 	return TRUE;
// }
// 
// BOOL CFile_Model::SaveFFTOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
// {
// 	if (NULL == szPath)
// 		return FALSE;
// 
// 	CString strValue;
// 	CString strAppName;
// 	CString strKeyName;
// 
// 	for (UINT nItemCnt = 0; nItemCnt < TICnt_FFT; nItemCnt++)
// 	{
// 		strAppName.Format(_T("%s_%d"), FFT_AppName, nItemCnt);
// 
// 		for (int iCh = 0; iCh < FFTCh_Max; iCh++)
// 		{
// 			strKeyName.Format(_T("Channel_%d_Min_%d"), iCh + 1, nItemCnt);
// 			strValue.Format(_T("%.2f"), pstModelInfo->stFFT[nItemCnt].stFFTOpt.dSpecMin[iCh]);
// 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
// 
// 			strKeyName.Format(_T("Channel_%d_Max_%d"), iCh + 1, nItemCnt);
// 			strValue.Format(_T("%.2f"), pstModelInfo->stFFT[nItemCnt].stFFTOpt.dSpecMax[iCh]);
// 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
// 
// 			strKeyName.Format(_T("Channel_%d_Offset_%d"), iCh + 1, nItemCnt);
// 			strValue.Format(_T("%.2f"), pstModelInfo->stFFT[nItemCnt].stFFTOpt.dbOffset[iCh]);
// 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
// 
// 			strKeyName.Format(_T("Channel_%d_TestTime_%d"), iCh + 1, nItemCnt);
// 			strValue.Format(_T("%d"), pstModelInfo->stFFT[nItemCnt].stFFTOpt.nTestTime[iCh]);
// 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
// 		}
// 		
// 
// 		strKeyName.Format(_T("TestVolume_%d"), nItemCnt);
// 		strValue.Format(_T("%d"), pstModelInfo->stFFT[nItemCnt].stFFTOpt.iVolume);
// 		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
// 
// 		strKeyName.Format(_T("WavFile_%d"), nItemCnt);
// 		strValue.Format(_T("%s"), pstModelInfo->stFFT[nItemCnt].stFFTOpt.szWaveFile);
// 		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
// 	}
// 
// 	return TRUE;
// }
// 
 //=============================================================================
 // Method		: LoadParticleOpFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __out ST_ModelInfo & stModelInfo
 // Qualifier	:
 // Last Update	: 2018/1/2 - 9:57
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::LoadParticleOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	TCHAR   inBuff[255] = { 0, };
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_BlackSpot; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Particle_AppName, nItemCnt);
 
 		strKeyName.Format(_T("DustDis"));
 		GetPrivateProfileString(strAppName, strKeyName, strValue, inBuff, 255, szPath);
 		stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.fDustDis = (float)_ttof(inBuff);
 
		GetPrivateProfileString(strAppName, _T("EdgeW"), _T("0"), inBuff, 255, szPath);
		stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.iEdgeW = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("EdgeH"), _T("0"), inBuff, 255, szPath);
		stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.iEdgeH = _ttoi(inBuff);

 		for (UINT nROI = 0; nROI < Part_ROI_Max; nROI++)
 		{
 			//strKeyName.Format(_T("%s_Ellipse"), g_szPaticleRegion[nROI]);
 			//GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			//stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].bEllipse = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_BruiseConc"), g_szPaticleRegion[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].dbBruiseConc = _ttof(inBuff);
 
 			strKeyName.Format(_T("%s_BruiseSize"), g_szPaticleRegion[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].dbBruiseSize = _ttof(inBuff);
 
 			strKeyName.Format(_T("%s_left"), g_szPaticleRegion[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].rtRoi.left = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_top"), g_szPaticleRegion[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].rtRoi.top = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_right"), g_szPaticleRegion[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].rtRoi.right = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_bottom"), g_szPaticleRegion[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].rtRoi.bottom = _ttoi(inBuff);
 		}
 	}
 
 	return TRUE;

 }

 BOOL CFile_Model::LoadDefectPixelOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stConfigInfo)
 {
	 if (NULL == szPath)
		 return FALSE;

	 TCHAR   inBuff[255] = { 0, };

	 CString	strAppName = DefectPixel_AppName;
	// CString	str;
	 for (UINT nItemCnt = 0; nItemCnt < TICnt_DefectPixel; nItemCnt++)
	 {
		 GetPrivateProfileString(strAppName, _T("BlockSize"), _T("0"), inBuff, 255, szPath);
		 stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.iBlockSize = _ttoi(inBuff);

		 GetPrivateProfileString(strAppName, _T("Margin_Left"), _T("0"), inBuff, 255, szPath);
		 stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.nmagin_left = _ttoi(inBuff);

		 GetPrivateProfileString(strAppName, _T("Margin_Right"), _T("0"), inBuff, 255, szPath);
		 stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.nmagin_rightt = _ttoi(inBuff);

		 for (UINT nIdx = 0; nIdx < Threshold_Defect_Max; nIdx++)
		 {
			 strAppName.Format(_T("%s_Threshold_%d"), DefectPixel_AppName, nIdx);

				 GetPrivateProfileString(strAppName, _T("Threshold"), _T("0"), inBuff, 255, szPath);
				 stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.fThreshold[nIdx] = _ttof(inBuff);
		 }

		 for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
		 {
			 strAppName.Format(_T("%s_SPEC_%d"), DefectPixel_AppName, nIdx);

			 //str.Format(_T("%s_MIN_ENABLE"), g_szDefectPixel_Spec[nIdx]);
			 GetPrivateProfileString(strAppName, _T("MIN_ENABLE"), _T("0"), inBuff, 255, szPath);
			 stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.bEnableMin[nIdx] = _ttoi(inBuff);
			 
			 GetPrivateProfileString(strAppName, _T("MAX_ENABLE"), _T("0"), inBuff, 255, szPath);
			 stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.bEnableMax[nIdx] = _ttoi(inBuff);

			 GetPrivateProfileString(strAppName, _T("MIN"), _T("0"), inBuff, 255, szPath);
			 stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.iSpecMin[nIdx] = _ttoi(inBuff);
			 
			 GetPrivateProfileString(strAppName, _T("MAX"), _T("0"), inBuff, 255, szPath);
			 stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.iSpecMax[nIdx] = _ttoi(inBuff);

			 //GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
			 //stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

			 //GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
			 //stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

			 //GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
			 //stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

			 //GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
			 //stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

			 //GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
			 //stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

			 //GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
			 //stConfigInfo.stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
		 }
	 }
	 return TRUE;
 }
 
 BOOL CFile_Model::SaveDefectPixelOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstConfigInfo)
 {
	 if (NULL == szPath)
		 return FALSE;

	 if (NULL == pstConfigInfo)
		 return FALSE;

	 CString strValue;
	// CString str;
	 CString	strAppName = DefectPixel_AppName;
	 for (UINT nItemCnt = 0; nItemCnt < TICnt_DefectPixel; nItemCnt++)
	 {
		 strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.iBlockSize);
		 WritePrivateProfileString(strAppName, _T("BlockSize"), strValue, szPath);

		 strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.nmagin_left);
		 WritePrivateProfileString(strAppName, _T("Margin_Left"), strValue, szPath);

		 strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.nmagin_rightt);
		 WritePrivateProfileString(strAppName, _T("Margin_Right"), strValue, szPath);

		 for (UINT nIdx = 0; nIdx < Threshold_Defect_Max; nIdx++)
		 {
			 strAppName.Format(_T("%s_Threshold_%d"), DefectPixel_AppName, nIdx);

			 strValue.Format(_T("%.2f"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.fThreshold[nIdx]);
			 WritePrivateProfileString(strAppName, _T("Threshold"), strValue, szPath);
		 }

		 for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
		 {
			 strAppName.Format(_T("%s_SPEC_%d"), DefectPixel_AppName, nIdx);

			// str.Format(_T("%s_MIN_ENABLE"), g_szDefectPixel_Spec[nIdx]);
			 strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.bEnableMin[nIdx]);
			 WritePrivateProfileString(strAppName, _T("MIN_ENABLE"), strValue, szPath);

			// str.Format(_T("%s_MAX_ENABLE"), g_szDefectPixel_Spec[nIdx]);
			 strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.bEnableMax[nIdx]);
			 WritePrivateProfileString(strAppName, _T("MAX_ENABLE"), strValue, szPath);

			// str.Format(_T("%s_MIN"), g_szDefectPixel_Spec[nIdx]);
			 strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.iSpecMin[nIdx]);
			 WritePrivateProfileString(strAppName, _T("MIN"), strValue, szPath);

			 // str.Format(_T("%s_MAX"), g_szDefectPixel_Spec[nIdx]);
			  strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.iSpecMax[nIdx]);
			 WritePrivateProfileString(strAppName, _T("MAX"), strValue, szPath);
			 
			 //strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Min[nIdx].bEnable);
			 //WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

			 //strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Min[nIdx].iValue);
			 //WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

			 //strValue.Format(_T("%.2f"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Min[nIdx].dbValue);
			 //WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

			 //strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Max[nIdx].bEnable);
			 //WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

			 //strValue.Format(_T("%d"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Max[nIdx].iValue);
			 //WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

			 //strValue.Format(_T("%.2f"), pstConfigInfo->stDefectPixel[nItemCnt].stDefectPixelOpt.stSpec_Max[nIdx].dbValue);
			 //WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
		 }
	 }
	 return TRUE;
 }

 //=============================================================================
 // Method		: SaveParticleOpFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __in const ST_ModelInfo * pstModelInfo
 // Qualifier	:
 // Last Update	: 2018/1/2 - 10:03
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::SaveParticleOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_BlackSpot; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Particle_AppName, nItemCnt);
 
 		strKeyName.Format(_T("DustDis"));
 		strValue.Format(_T("%.2f"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.fDustDis);
 		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.iEdgeW);
		WritePrivateProfileString(strAppName, _T("EdgeW"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.iEdgeH);
		WritePrivateProfileString(strAppName, _T("EdgeH"), strValue, szPath);
 
 		for (UINT nROI = 0; nROI < Part_ROI_Max; nROI++)
 		{
 			//strKeyName.Format(_T("%s_Ellipse"), g_szPaticleRegion[nROI]);
 			//strValue.Format(_T("%d"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].bEllipse);
 			//WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_BruiseConc"), g_szPaticleRegion[nROI]);
 			strValue.Format(_T("%.2f"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].dbBruiseConc);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_BruiseSize"), g_szPaticleRegion[nROI]);
 			strValue.Format(_T("%.2f"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].dbBruiseSize);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_left"), g_szPaticleRegion[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].rtRoi.left);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_top"), g_szPaticleRegion[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].rtRoi.top);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_right"), g_szPaticleRegion[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].rtRoi.right);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_bottom"), g_szPaticleRegion[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stBlackSpot[nItemCnt].stParticleOpt.stRegionOp[nROI].rtRoi.bottom);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 		}
 	}
 
 	return TRUE;
 }
 
 
 BOOL CFile_Model::LoadBrightnessOpFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	TCHAR   inBuff[255] = { 0, };
 
 	//CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_Brightness; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Brightness_AppName, nItemCnt);
 
 		for (UINT nROI = 0; nROI < ROI_BR_Max; nROI++)
 		{
 			strKeyName.Format(_T("%s_left"), g_szRoiBrightness[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi.left = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_top"), g_szRoiBrightness[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi.top = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_right"), g_szRoiBrightness[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi.right = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_bottom"), g_szRoiBrightness[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi.bottom = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%s_MinSpec"), g_szRoiBrightness[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("60"), inBuff, 80, szPath);
 			stModelInfo.stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].dMinSpec = _ttof(inBuff);
 
 			strKeyName.Format(_T("%s_MaxSpec"), g_szRoiBrightness[nROI]);
 			GetPrivateProfileString(strAppName, strKeyName, _T("80"), inBuff, 80, szPath);
 			stModelInfo.stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].dMaxSpec = _ttof(inBuff);
 		}
 	}
 
 	return TRUE;
 }
 
 BOOL CFile_Model::SaveBrightnessOpFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_Brightness; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Brightness_AppName, nItemCnt);
 
 		for (UINT nROI = 0; nROI < ROI_BR_Max; nROI++)
 		{
 			strKeyName.Format(_T("%s_left"), g_szRoiBrightness[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi.left);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_top"), g_szRoiBrightness[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi.top);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_right"), g_szRoiBrightness[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi.right);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_bottom"), g_szRoiBrightness[nROI]);
 			strValue.Format(_T("%d"), pstModelInfo->stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].rtRoi.bottom);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_MinSpec"), g_szRoiBrightness[nROI]);
 			strValue.Format(_T("%0.1f"), pstModelInfo->stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].dMinSpec);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%s_MaxSpec"), g_szRoiBrightness[nROI]);
 			strValue.Format(_T("%0.1f"), pstModelInfo->stBrightness[nItemCnt].stBrightnessOpt.stRegionOp[nROI].dMaxSpec);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 		}
 	}
 
 	return TRUE;
 }
 
 //=============================================================================
 // Method		: LoadSFRFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __out ST_ModelInfo & stModelInfo
 // Qualifier	:
 // Last Update	: 2018/1/2 - 10:08
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::LoadSFRFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
 {
 /*	if (NULL == szPath)
 		return FALSE;
 
 	TCHAR   inBuff[255] = { 0, };
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_SFR; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), SFR_AppName, nItemCnt);
 
 		GetPrivateProfileString(strAppName, _T("PixelSize"), _T("0"), inBuff, 80, szPath);
 		stModelInfo.stSFR[nItemCnt].stSFROpt.dPixelSz = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("bField"), _T("0"), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.bField = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("bEdge"), _T("0"), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.bEdge = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("bDistortion"), _T("0"), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.bDistortion = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("nResultType"), _T("0"), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.nResultType = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("iMasterAxisX"), _T("960"), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.iMasterAxisX = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("iMasterAxisY"), _T("540"), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.iMasterAxisY = _ttoi(inBuff);
 
		GetPrivateProfileString(strAppName, _T("szIICFile_1"), _T(""), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.szIICFile_1 = inBuff;

		GetPrivateProfileString(strAppName, _T("szIICFile_2"), _T(""), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.szIICFile_2 = inBuff;

		GetPrivateProfileString(strAppName, _T("bIICFile_1"), _T("0"), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.bIICFile_1 = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("bIICFile_2"), _T("0"), inBuff, 80, szPath);
		stModelInfo.stSFR[nItemCnt].stSFROpt.bIICFile_2 = _ttoi(inBuff);
 
 		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
 		{
 			strKeyName.Format(_T("%d_Enable"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].bEnable = _ttoi(inBuff);
 			
 			strKeyName.Format(_T("%d_MinSpc"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].dbMinSpc = _ttof(inBuff);
 
 			strKeyName.Format(_T("%d_MaxSpc"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].dbMaxSpc = _ttof(inBuff);
 
 			strKeyName.Format(_T("%d_Offset"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].dbOffset = _ttof(inBuff);
 
 			strKeyName.Format(_T("%d_LinePair"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].dbLinePair = _ttof(inBuff);

			strKeyName.Format(_T("%d_nFont"), nROI);
			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].nFont = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%d_left"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].rtRoi.left = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%d_top"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].rtRoi.top = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%d_right"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].rtRoi.right = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%d_bottom"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].rtRoi.bottom = _ttoi(inBuff);
 		}
 
 		stModelInfo.stSFR[nItemCnt].stInitSFROpt = stModelInfo.stSFR[nItemCnt].stSFROpt;
 	}
 
 	
 */
 	return TRUE;
 }
 
 //=============================================================================
 // Method		: SaveSFRFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __in const ST_ModelInfo * pstModelInfo
 // Qualifier	:
 // Last Update	: 2018/1/2 - 10:12
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::SaveSFRFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
 {
 /*	if (NULL == szPath)
 		return FALSE;
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_SFR; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), SFR_AppName, nItemCnt);
 
 		strValue.Format(_T("%.2f"), pstModelInfo->stSFR[nItemCnt].stSFROpt.dPixelSz);
 		WritePrivateProfileString(strAppName, _T("PixelSize"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.bField);
		WritePrivateProfileString(strAppName, _T("bField"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.bEdge);
		WritePrivateProfileString(strAppName, _T("bEdge"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.bDistortion);
		WritePrivateProfileString(strAppName, _T("bDistortion"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.nResultType);
		WritePrivateProfileString(strAppName, _T("nResultType"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.iMasterAxisX);
		WritePrivateProfileString(strAppName, _T("iMasterAxisX"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.iMasterAxisY);
		WritePrivateProfileString(strAppName, _T("iMasterAxisY"), strValue, szPath);
 
		strValue.Format(_T("%s"), pstModelInfo->stSFR[nItemCnt].stSFROpt.szIICFile_1);
		WritePrivateProfileString(strAppName, _T("szIICFile_1"), strValue, szPath);

		strValue.Format(_T("%s"), pstModelInfo->stSFR[nItemCnt].stSFROpt.szIICFile_2);
		WritePrivateProfileString(strAppName, _T("szIICFile_2"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.bIICFile_1);
		WritePrivateProfileString(strAppName, _T("bIICFile_1"), strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.bIICFile_2);
		WritePrivateProfileString(strAppName, _T("bIICFile_2"), strValue, szPath);
 
 		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
 		{
 			strKeyName.Format(_T("%d_Enable"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].bEnable);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_MinSpc"), nROI);
 			strValue.Format(_T("%.2f"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].dbMinSpc);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_MaxSpc"), nROI);
 			strValue.Format(_T("%.2f"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].dbMaxSpc);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_Offset"), nROI);
 			strValue.Format(_T("%.2f"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].dbOffset);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_LinePair"), nROI);
 			strValue.Format(_T("%.2f"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].dbLinePair);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);

			strKeyName.Format(_T("%d_nFont"), nROI);
			strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].nFont);
			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_left"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].rtRoi.left);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_top"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].rtRoi.top);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_right"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].rtRoi.right);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_bottom"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stSFR[nItemCnt].stSFROpt.stRegionOp[nROI].rtRoi.bottom);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 		}
 	}
 */
 	return TRUE;
 }
 
 
 //=============================================================================
 // Method		: LoadRotateFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __out ST_ModelInfo & stModelInfo
 // Qualifier	:
 // Last Update	: 2018/1/2 - 10:17
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::LoadRotateFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	TCHAR   inBuff[255] = { 0, };
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_Rotation; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Rotate_AppName, nItemCnt);
 
 		strKeyName.Format(_T("TestMode"));
 		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 		stModelInfo.stRotate[nItemCnt].stRotateOpt.nTestMode = _ttoi(inBuff);
 
 		strKeyName.Format(_T("MinSpc"));
 		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 		stModelInfo.stRotate[nItemCnt].stRotateOpt.dbMinSpc = _ttof(inBuff);
 
 		strKeyName.Format(_T("MaxSpc"));
 		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 		stModelInfo.stRotate[nItemCnt].stRotateOpt.dbMaxSpc = _ttof(inBuff);
 
 		strKeyName.Format(_T("Offset"));
 		GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 		stModelInfo.stRotate[nItemCnt].stRotateOpt.dbOffset = _ttof(inBuff);
 
 		for (UINT nROI = 0; nROI < ROI_RT_Max; nROI++)
 		{
 			strKeyName.Format(_T("%d_MarkColor"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].nMarkColor = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%d_left"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].rtRoi.left = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%d_top"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].rtRoi.top = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%d_right"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
 			stModelInfo.stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].rtRoi.right = _ttoi(inBuff);
 
 			strKeyName.Format(_T("%d_bottom"), nROI);
 			GetPrivateProfileString(strAppName, strKeyName, _T("0"), inBuff, 80, szPath);
			stModelInfo.stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].rtRoi.bottom = _ttoi(inBuff);
			stModelInfo.stRotate[nItemCnt].stRotateOpt.stTestRegionOp[nROI]=stModelInfo.stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI];
 		}
 	}
 
 	return TRUE;
 }
 
 //=============================================================================
 // Method		: SaveRotateFile
 // Access		: public  
 // Returns		: BOOL
 // Parameter	: __in LPCTSTR szPath
 // Parameter	: __in const ST_ModelInfo * pstModelInfo
 // Qualifier	:
 // Last Update	: 2018/1/2 - 10:25
 // Desc.		:
 //=============================================================================
 BOOL CFile_Model::SaveRotateFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
 {
 	if (NULL == szPath)
 		return FALSE;
 
 	CString strValue;
 	CString strAppName;
 	CString strKeyName;
 
 	for (UINT nItemCnt = 0; nItemCnt < TICnt_Rotation; nItemCnt++)
 	{
 		strAppName.Format(_T("%s_%d"), Rotate_AppName, nItemCnt);
 
 		strKeyName.Format(_T("TestMode"));
 		strValue.Format(_T("%d"), pstModelInfo->stRotate[nItemCnt].stRotateOpt.nTestMode);
 		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 		strKeyName.Format(_T("MinSpc"));
 		strValue.Format(_T("%.2f"), pstModelInfo->stRotate[nItemCnt].stRotateOpt.dbMinSpc);
 		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 		strKeyName.Format(_T("MaxSpc"));
 		strValue.Format(_T("%.2f"), pstModelInfo->stRotate[nItemCnt].stRotateOpt.dbMaxSpc);
 		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 		strKeyName.Format(_T("Offset"));
 		strValue.Format(_T("%.2f"), pstModelInfo->stRotate[nItemCnt].stRotateOpt.dbOffset);
 		WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 		for (UINT nROI = 0; nROI < ROI_RT_Max; nROI++)
 		{
 			strKeyName.Format(_T("%d_MarkColor"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].nMarkColor);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_left"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].rtRoi.left);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_top"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].rtRoi.top);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_right"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].rtRoi.right);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 
 			strKeyName.Format(_T("%d_bottom"), nROI);
 			strValue.Format(_T("%d"), pstModelInfo->stRotate[nItemCnt].stRotateOpt.stRegionOp[nROI].rtRoi.bottom);
 			WritePrivateProfileString(strAppName, strKeyName, strValue, szPath);
 		}
 	}
 
 	return TRUE;
 }

//=============================================================================
// Method		: LoadPogoIniFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_PogoInfo & stPogoInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 10:13
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadPogoIniFile(__in LPCTSTR szPath, __out ST_PogoInfo& stPogoInfo)
{
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	TCHAR   inBuff[80] = { 0, };

	//DWORD		dwCount_Max[MAX_SITE_CNT];
	//DWORD		dwCount[MAX_SITE_CNT];

	CString strKey;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		// Max Count
		strKey.Format(_T("PogoCnt_Max_%d"), nIdx);
		GetPrivateProfileString(_T("COUNT"), strKey, _T("50000"), inBuff, 80, szPath);
		stPogoInfo.dwCount_Max[nIdx] = _ttoi(inBuff);

		// Channel Count	
		strKey.Format(_T("PogoCnt_%d"), nIdx);
		GetPrivateProfileString(_T("COUNT"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwCount[nIdx] = _ttoi(inBuff);

		// Restration Year	
		strKey.Format(_T("Reg_Year_%d"), nIdx);
		GetPrivateProfileString(_T("Registration"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwYear[nIdx] = _ttoi(inBuff);

		// Restration Month	
		strKey.Format(_T("Reg_Month_%d"), nIdx);
		GetPrivateProfileString(_T("Registration"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwMonth[nIdx] = _ttoi(inBuff);

		// Restration Day	
		strKey.Format(_T("Reg_Day_%d"), nIdx);
		GetPrivateProfileString(_T("Registration"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwDay[nIdx] = _ttoi(inBuff);

		// Restration Hour	
		strKey.Format(_T("Reg_Hour_%d"), nIdx);
		GetPrivateProfileString(_T("Registration"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwHour[nIdx] = _ttoi(inBuff);

		// Restration Minute	
		strKey.Format(_T("Reg_Minute_%d"), nIdx);
		GetPrivateProfileString(_T("Registration"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwMinute[nIdx] = _ttoi(inBuff);

		// Restration Second	
		strKey.Format(_T("Reg_Second_%d"), nIdx);
		GetPrivateProfileString(_T("Registration"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwSecond[nIdx] = _ttoi(inBuff);

		// Restration MilliSeconds	
		strKey.Format(_T("Reg_MilliSeconds_%d"), nIdx);
		GetPrivateProfileString(_T("Registration"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwMilliseconds[nIdx] = _ttoi(inBuff);

		// Restration Day Count	
		strKey.Format(_T("Reg_DayCount_%d"), nIdx);
		GetPrivateProfileString(_T("Registration"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwDateCount[nIdx] = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: SavePogoIniFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_PogoInfo * pstPogoInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 10:14
// Desc.		:
//=============================================================================
BOOL CFile_Model::SavePogoIniFile(__in LPCTSTR szPath, __in const ST_PogoInfo* pstPogoInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstPogoInfo)
		return FALSE;

	::DeleteFile(szPath);

	CString strValue;
	CString strKey;

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		// Max Count
		strKey.Format(_T("PogoCnt_Max_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwCount_Max[nIdx]);
		WritePrivateProfileString(_T("COUNT"), strKey, strValue, szPath);

		// Channel Count
		strKey.Format(_T("PogoCnt_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwCount[nIdx]);
		WritePrivateProfileString(_T("COUNT"), strKey, strValue, szPath);

		// Restration Year	
		strKey.Format(_T("Reg_Year_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwYear[nIdx]);
		WritePrivateProfileString(_T("Registration"), strKey, strValue, szPath);

		// Restration Month	
		strKey.Format(_T("Reg_Month_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwMonth[nIdx]);
		WritePrivateProfileString(_T("Registration"), strKey, strValue, szPath);

		// Restration Day	
		strKey.Format(_T("Reg_Day_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwDay[nIdx]);
		WritePrivateProfileString(_T("Registration"), strKey, strValue, szPath);

		// Restration Hour	
		strKey.Format(_T("Reg_Hour_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwHour[nIdx]);
		WritePrivateProfileString(_T("Registration"), strKey, strValue, szPath);

		// Restration Minute	
		strKey.Format(_T("Reg_Minute_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwMinute[nIdx]);
		WritePrivateProfileString(_T("Registration"), strKey, strValue, szPath);

		// Restration Second	
		strKey.Format(_T("Reg_Second_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwSecond[nIdx]);
		WritePrivateProfileString(_T("Registration"), strKey, strValue, szPath);

		// Restration MilliSeconds
		strKey.Format(_T("Reg_MilliSeconds_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwMilliseconds[nIdx]);
		WritePrivateProfileString(_T("Registration"), strKey, strValue, szPath);

		// Restration DayCount
		strKey.Format(_T("Reg_DayCount_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwDateCount[nIdx]);
		WritePrivateProfileString(_T("Registration"), strKey, strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: SavePogoCount
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nPogoIdx
// Parameter	: __in DWORD dwCount
// Qualifier	:
// Last Update	: 2017/1/6 - 10:58
// Desc.		:
//=============================================================================
BOOL CFile_Model::SavePogoCount(__in LPCTSTR szPath, __in UINT nPogoIdx, __in DWORD dwCount)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strKey;

	// Channel Count
	strKey.Format(_T("PogoCnt_%d"), nPogoIdx);
	strValue.Format(_T("%d"), dwCount);
	WritePrivateProfileString(_T("COUNT"), strKey, strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: LoadPogoCount
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nPogoIdx
// Parameter	: __out DWORD & dwCount
// Qualifier	:
// Last Update	: 2017/1/6 - 10:58
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadPogoCount(__in LPCTSTR szPath, __in UINT nPogoIdx, __out DWORD& dwCount)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[80] = { 0, };
	CString strValue;
	CString strKey;

	// Channel Count
	strKey.Format(_T("PogoCnt_%d"), nPogoIdx);
	strValue.Format(_T("%d"), dwCount);
	GetPrivateProfileString(_T("COUNT"), strKey, _T("0"), inBuff, 80, szPath);
	dwCount = _ttoi(inBuff);

	return TRUE;
}


//=============================================================================
// Method		: LoadLightBrdFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_SlotVolt & stLightInfo
// Qualifier	:
// Last Update	: 2017/6/29 - 14:43
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadLightBrdFile(__in LPCTSTR szPath, __out ST_SlotVolt& stLightInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[80] = { 0, };

	CString strValue;
	CString strAppName;

	for (UINT nIdx = 0; nIdx < Slot_Max; nIdx++)
	{
		strAppName.Format(_T("Volt_%d"), nIdx + 1);
		GetPrivateProfileString(Light_AppName, strAppName, _T("12.0"), inBuff, 80, szPath);
		stLightInfo.fVolt[nIdx] = (float)_ttof(inBuff);

		strAppName.Format(_T("Step_%d"), nIdx + 1);
		GetPrivateProfileString(Light_AppName, strAppName, _T("0.0"), inBuff, 80, szPath);
		stLightInfo.wCurrent[nIdx] = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveLightBrdFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_SlotVolt * pstLightInfo
// Qualifier	:
// Last Update	: 2017/6/29 - 14:43
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveLightBrdFile(__in LPCTSTR szPath, __in const ST_SlotVolt* pstLightInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstLightInfo)
		return FALSE;

	CString strValue;
	CString strAppName;

	for (UINT nIdx = 0; nIdx < Slot_Max; nIdx++)
	{
		strValue.Format(_T("%.2f"), pstLightInfo->fVolt[nIdx]);
		strAppName.Format(_T("Volt_%d"), nIdx + 1);
		WritePrivateProfileString(Light_AppName, strAppName, strValue, szPath);

		strValue.Format(_T("%d"), pstLightInfo->wCurrent[nIdx]);
		strAppName.Format(_T("Step_%d"), nIdx + 1);
		WritePrivateProfileString(Light_AppName, strAppName, strValue, szPath);
	}

	return TRUE;
}


BOOL CFile_Model::LoadLabelPrinterFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[80] = { 0, };

	CString strValue;
	CString strAppName;

	strAppName = _T("szPrnFileName");
	GetPrivateProfileString(LabelPrinter_AppName, strAppName, _T(""), inBuff, 80, szPath);
	stModelInfo.stPrinter.szPrnFileName = inBuff;

	for (UINT nIdx = 0; nIdx < enLabelDataType_Max; nIdx++)
	{
		strAppName.Format(_T("iPosX_%d"), nIdx + 1);
		GetPrivateProfileString(LabelPrinter_AppName, strAppName, _T("0"), inBuff, 80, szPath);
		stModelInfo.stPrinter.iPosX[nIdx] = (float)_ttoi(inBuff);

		strAppName.Format(_T("iPosY_%d"), nIdx + 1);
		GetPrivateProfileString(LabelPrinter_AppName, strAppName, _T("0"), inBuff, 80, szPath);
		stModelInfo.stPrinter.iPosY[nIdx] = (float)_ttoi(inBuff);

		strAppName.Format(_T("iWidthSz_%d"), nIdx + 1);
		GetPrivateProfileString(LabelPrinter_AppName, strAppName, _T("0"), inBuff, 80, szPath);
		stModelInfo.stPrinter.iWidthSz[nIdx] = (float)_ttoi(inBuff);

		strAppName.Format(_T("iHeightSz_%d"), nIdx + 1);
		GetPrivateProfileString(LabelPrinter_AppName, strAppName, _T("0"), inBuff, 80, szPath);
		stModelInfo.stPrinter.iHeightSz[nIdx] = (float)_ttoi(inBuff);

		strAppName.Format(_T("szText_%d"), nIdx + 1);
		GetPrivateProfileString(LabelPrinter_AppName, strAppName, _T(""), inBuff, 80, szPath);
		stModelInfo.stPrinter.szText[nIdx] = inBuff;
		
	}

	return TRUE;
}

BOOL CFile_Model::SaveLabelPrinterFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstModelInfo)
		return FALSE;

	CString strValue;
	CString strAppName;

	strValue.Format(_T("%s"), pstModelInfo->stPrinter.szPrnFileName);
	strAppName = _T("szPrnFileName");
	WritePrivateProfileString(LabelPrinter_AppName, strAppName, strValue, szPath);

	for (UINT nIdx = 0; nIdx < enLabelDataType_Max; nIdx++)
	{
		strValue.Format(_T("%d"), pstModelInfo->stPrinter.iPosX[nIdx]);
		strAppName.Format(_T("iPosX_%d"), nIdx + 1);
		WritePrivateProfileString(LabelPrinter_AppName, strAppName, strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stPrinter.iPosY[nIdx]);
		strAppName.Format(_T("iPosY_%d"), nIdx + 1);
		WritePrivateProfileString(LabelPrinter_AppName, strAppName, strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stPrinter.iWidthSz[nIdx]);
		strAppName.Format(_T("iWidthSz_%d"), nIdx + 1);
		WritePrivateProfileString(LabelPrinter_AppName, strAppName, strValue, szPath);

		strValue.Format(_T("%d"), pstModelInfo->stPrinter.iHeightSz[nIdx]);
		strAppName.Format(_T("iHeightSz_%d"), nIdx + 1);
		WritePrivateProfileString(LabelPrinter_AppName, strAppName, strValue, szPath);

		strValue.Format(_T("%s"), pstModelInfo->stPrinter.szText[nIdx]);
		strAppName.Format(_T("szText_%d"), nIdx + 1);
		WritePrivateProfileString(LabelPrinter_AppName, strAppName, strValue, szPath);
	}

	return TRUE;
}
