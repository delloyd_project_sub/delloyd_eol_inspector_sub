#pragma once
#include "resource.h"

#include "Def_TestDevice.h"
#include "Wnd_VideoView.h"
#include "TestProcess_Image.h"
#include "VGStatic.h"

// CDlg_MasterTest 대화 상자입니다.

class CDlg_MasterTest : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_MasterTest)

public:
	CDlg_MasterTest(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_MasterTest();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_MASTER_TEST };

	void CreateThread(UINT _method);
	bool DestroyThread();
	int	 ThreadFunction();


	BOOL m_bStopFlag;
	UINT m_nResult;
	UINT m_nViewChannel;
	BOOL m_bFlag_Butten[DI_NotUseBit_Max];

	CString m_strI2CPath;

	UINT m_nMode;
	UINT m_nPermisionMode;

	ST_Device* m_pDevice;
	ST_ModelInfo* m_pModelinfo;

	UINT GetResult()
	{
		return m_nResult;
	};

	void SetPtr_Device(__in ST_Device* pDevice)
	{
		if (pDevice == NULL)
			return;

		m_pDevice = pDevice;
	};

	void SetPtr_Modelinfo(__in ST_ModelInfo* pModelinfo)
	{
		if (pModelinfo == NULL)
			return;

		m_pModelinfo = pModelinfo;
	};

	void SetVideoChannel(__in UINT nCH)
	{
		m_nViewChannel = nCH;
	};

	void SetPath(__in CString szI2CPath)
	{
		m_strI2CPath = szI2CPath;
	};

	void SetMode(__in UINT nMode)
	{
		m_nMode = nMode;
	};

	void SetPermisionMode(__in UINT nMode)
	{
		m_nPermisionMode = nMode;
	};

	CWnd_VideoView m_wndVideoView;
	CTestProcess_Image	m_ImageTest;

protected:
	CFont m_font;

	CVGStatic m_stTotalTestStatus;
	CVGStatic m_stTestItem[TIID_MaxEnum];
	CVGStatic m_stEachTestStatus[TIID_MaxEnum];

	CComboBox m_cbEachTestSel[TIID_MaxEnum];

	CMFCButton m_bnStart;
	CMFCButton m_bnStop;

	BOOL IsCameraConnect(__in UINT nCh, __in UINT nGrabType);
	LPBYTE GetImageBuffer(__in UINT nViewCh, __in UINT nGrabType, __out DWORD& dwWidth, __out DWORD& dwHeight, __out UINT& nChannel);

	UINT SetLuriCamBrd_Volt(__in  float* fVolt, __in UINT nPort = 0);
	BOOL CameraOnOff(__in UINT nCh, __in UINT nGrabType, __in UINT nSWVer, __in UINT nFpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff, __in BOOL bOnOff);
	UINT CameraRegToFunctionChange(__in UINT nSWVer, __in UINT nfpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff);

	// Edge On/Off
	UINT EdgeOnOff(__in BOOL bOnOff);

	// Distortion Corret
	UINT DistortionCorr(__in BOOL bOnOff);

	// Mode Change
	UINT OperationModeChange(__in UINT nMode);

	// Current
	void TestProcessCurrent(__in ST_Current_Opt* pstOpt, __out ST_Current_Result &stResult);
	// Video Signal
	void TestProcessVideoSignal(__in UINT nCh, __out UINT& nResult);
	// Frame Check
	void TestProcessFrameCheck(__in UINT nCh, __in UINT nMode, __out UINT& nResult);

	// Motion
	UINT OnEachTestMotion(__in UINT nAxisItemID, __in double dbPos);
	UINT OnEachTestIOSignal(__in UINT nIO, __in UINT nSignal);
	UINT OnEachTest_Motion(__in UINT nTestItemID);
	UINT Motion_StageY_CL_Distance(__in double dDistance);
	UINT Motion_StageY_BlemishZone_In(__in UINT nTestItemID);
	UINT Motion_StageY_BlemishZone_Out();
	UINT Motion_StageY_Initial();

	// Total Test Sequence
	UINT StartTest();
	UINT StopTest();
	UINT StartTest_TestItem(__in UINT nTestItemID, __in UINT nTestIdx, __in UINT nStepIdx, __in int iRetryCount);
	UINT StartTest_TestItem_Motion(__in UINT nTestItemID);
	void OnFunc_Motion_Initial();

	// Each Test
	void OnEachTest_Current(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_OperationMode(__in UINT nfpsMode, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_CenterPoint(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_Rotation(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_Angle(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_Color(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_Reverse(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_Brightness(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_SFR(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_PatternNoise(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	void OnEachTest_IRFilter(__in UINT nStepIdx, __in UINT nTestIdx = 0);

	// Master Check
	BOOL MasterCheck();

	// UI
	void OnSetUI_TotalTestStatus(__in UINT nStatus);
	void OnSetUI_EachTestStatus(__in UINT nStatus, __in UINT nTestItem, __in UINT nTestItemCnt);
	void OnSetUI_EachTestReset();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnBnClickedBnStart();
	afx_msg void OnBnClickedBnStop();

// 	afx_msg void OnBnClickedBnLeft();
// 	afx_msg void OnBnClickedBnRight();
// 	afx_msg void OnBnClickedBnOK();
// 	afx_msg void OnBnClickedBnInit();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
};
