//*****************************************************************************
// Filename	: 	PageOpt_SerialDevice.cpp
// Created	:	2015/12/16 - 15:23
// Modified	:	2015/12/16 - 15:23
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_SerialDevice.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"

#include <memory>
#include "CustomProperties.h"

IMPLEMENT_DYNAMIC(CPageOpt_SerialDevice, CPageOption)

//=============================================================================
//
//=============================================================================
CPageOpt_SerialDevice::CPageOpt_SerialDevice(LPCTSTR lpszCaption /*= NULL*/) : CPageOption (lpszCaption)
{
}

CPageOpt_SerialDevice::CPageOpt_SerialDevice(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{

}

//=============================================================================
//
//=============================================================================
CPageOpt_SerialDevice::~CPageOpt_SerialDevice(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_SerialDevice, CPageOption)	
END_MESSAGE_MAP()

// CPageOpt_SerialDevice 메시지 처리기입니다.
//=============================================================================
// Method		: CPageOpt_SerialDevice::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_SerialDevice::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_SerialDevice::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_SerialDevice::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_SerialDevice::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_SerialDevice::InitPropList()
{
	CPageOption::InitPropList();

	//--------------------------------------------------------
	// 통신 포트 설정
	//--------------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_LaserSensor(new CMFCPropertyGridProperty(_T("변위 센서")));
	InitPropList_Comport(apGroup_LaserSensor);
	m_wndPropList.AddProperty(apGroup_LaserSensor.release());


	std::auto_ptr<CMFCPropertyGridProperty> apGroup_VisionLight(new CMFCPropertyGridProperty(_T("비전 조명")));
	InitPropList_Comport(apGroup_VisionLight);
	m_wndPropList.AddProperty(apGroup_VisionLight.release());
}

//=============================================================================
// Method		: CPageOpt_SerialDevice::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_SerialDevice::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption	= GetOption ();

	m_pLT_Option->SaveOption_SerialDevice(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_SerialDevice::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_SerialDevice::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_SerialDevice(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_SerialDevice::GetOption
// Access		: protected 
// Returns		: Luritech_Option::stOpt_5KPower
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_SerialDevice CPageOpt_SerialDevice::GetOption()
{
	UINT nGroupIndex = 0;
	UINT nSubItemIndex = 0;
	UINT nIndex = 0;

	//---------------------------------------------------------------
	// 통신 포트 설정
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);

	GetOption_ComPort(pPropertyGroup, m_stOption.LaserSensor);

	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	GetOption_ComPort(pPropertyGroup, m_stOption.VisionLightBrd);

	return m_stOption;
}

//=============================================================================
// Method		: CPageOpt_SerialDevice::SetOption
// Access		: protected 
// Returns		: void
// Parameter	: stOpt_5KPower stOption
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_SerialDevice::SetOption( stOpt_SerialDevice stOption )
{
	UINT nGroupIndex	= 0;

	//---------------------------------------------------------------
	// 통신 포트 설정
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);

	SetOption_ComPort(pPropertyGroup, m_stOption.LaserSensor);

	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	SetOption_ComPort(pPropertyGroup, m_stOption.VisionLightBrd);
}