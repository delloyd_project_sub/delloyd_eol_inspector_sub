﻿//*****************************************************************************
// Filename	: LT_Option.h
// Created	: 2015/12/16
// Modified	: 2015/12/16
//
// Author	: PiRing
//	
// Purpose	: 옵션 처리를 위한 클래스
//*****************************************************************************
#ifndef LT_Option_h__
#define LT_Option_h__

#pragma once

#include "Define_Option.h"
#include "Def_Enum_Cm.h"

using namespace Luritech_Option;

//=============================================================================
//
//=============================================================================
class CLT_Option
{
public:
	CLT_Option();
	~CLT_Option();

	CLT_Option& operator= (CLT_Option& ref)
	{
		m_stOption			= ref.m_stOption;
		m_strRegPath_Base	= ref.m_strRegPath_Base;
		m_InsptrType		= ref.m_InsptrType;

		return *this;
	};

protected:
	// 옵션 구조체
	stLT_Option			m_stOption;

	// 레지스트리 저장 경로
	CString				m_strRegPath_Base;

	// 검사기 설정
	enInsptrSysType		m_InsptrType;

	// EVMS 사용
	BOOL				m_bUseEVMS;

public:
	void		SetRegistryPath			(__in LPCTSTR lpszRegPath);

	// 기본 검사 설정
	void		SaveOption_Inspector	(__in stOpt_Insp stOption);
	BOOL		LoadOption_Inspector	(__out stOpt_Insp& stOption);

	// 바코드 리더기 설정
	void		SaveOption_BCR			(__in stOpt_BCR stOption);
	BOOL		LoadOption_BCR			(__out stOpt_BCR& stOption);

	// 카메라 보드 설정
	void		SaveOption_PCB			(__in stOpt_PCB stOption);
	BOOL		LoadOption_PCB			(__out stOpt_PCB& stOption);

	// 광원 보드 설정
	void		SaveOption_Light		(__in stOpt_PCB stOption);
	BOOL		LoadOption_Light		(__out stOpt_PCB& stOption);

	// 기타 설정
	void		SaveOption_Misc			(__in stOpt_Misc stOption);
	BOOL		LoadOption_Misc			(__out stOpt_Misc& stOption);

	// 변위 센서, 비전 조명
	void		SaveOption_SerialDevice	(__in stOpt_SerialDevice stOption);
	BOOL		LoadOption_SerialDevice	(__out stOpt_SerialDevice& stOption);

	// 비전 카메라
	void		SaveOption_VisionCam	(__in stOpt_VisionCam stOption);
	BOOL		LoadOption_VisionCam	(__out stOpt_VisionCam& stOption);

	// 전체 옵션 저장/불러오기
	void		SaveOption_All			(__in stLT_Option stOption);
	BOOL		LoadOption_All			(__out stLT_Option& stOption);

	// 검사기 구분
	void		SetInspectorType		(__in enInsptrSysType nInsptrType);
	
	// EVMS
	void		SetUseEVMS				(__in BOOL bUseEVMS);

};

#endif // LT_Option_h__

