﻿//*****************************************************************************
// Filename	: PageOpt_Insp.cpp
// Created	: 2010/9/6
// Modified	: 2010/9/6 - 15:51
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_Insp.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"

#include <memory>
#include "CustomProperties.h"

IMPLEMENT_DYNAMIC(CPageOpt_Insp, CPageOption)

//=============================================================================
//
//=============================================================================
CPageOpt_Insp::CPageOpt_Insp(LPCTSTR lpszCaption /*= NULL*/) : CPageOption (lpszCaption)
{
	
}

CPageOpt_Insp::CPageOpt_Insp(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{
	
}

//=============================================================================
//
//=============================================================================
CPageOpt_Insp::~CPageOpt_Insp(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_Insp, CPageOption)	
END_MESSAGE_MAP()

// CPageOpt_Insp 메시지 처리기입니다.

//=============================================================================
// Method		: CPageOpt_Insp::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_Insp::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_Insp::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_Insp::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_Insp::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_Insp::InitPropList()
{
	CPageOption::InitPropList();

	CMFCPropertyGridProperty* pProp = NULL;

	//-----------------------------------------------------
	// 검사기 설정
	//-----------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Inspector(new CMFCPropertyGridProperty(_T("System Setting")));

	pProp = new CMFCPropertyGridProperty(_T("Use Master Check"), lpszUsableTable[0], _T("Use Master Check"));
	pProp->AddOption(lpszUsableTable[0]);
	pProp->AddOption(lpszUsableTable[1]);
	pProp->AllowEdit(FALSE);
	apGroup_Inspector->AddSubItem(pProp);

	pProp = new CMFCPropertyGridProperty(_T("Use Barcode"), lpszUsableTable[0], _T("Use Barcode"));
	pProp->AddOption(lpszUsableTable[0]);
	pProp->AddOption(lpszUsableTable[1]);
	pProp->AllowEdit(FALSE);
	apGroup_Inspector->AddSubItem(pProp);

	pProp = new CMFCPropertyGridProperty(_T("Use FailBox"), lpszUsableTable[0], _T("Use FailBox"));
	pProp->AddOption(lpszUsableTable[0]);
	pProp->AddOption(lpszUsableTable[1]);
	pProp->AllowEdit(FALSE);
	apGroup_Inspector->AddSubItem(pProp);

	//pProp = new CMFCPropertyGridProperty(_T("Use Printer"), lpszUsableTable[0], _T("프린터 사용 유무"));
	//pProp->AddOption(lpszUsableTable[0]);
	//pProp->AddOption(lpszUsableTable[1]);
	//pProp->AllowEdit(FALSE);
	//apGroup_Inspector->AddSubItem(pProp);

	// Door Open 오류 처리 사용여부
	pProp = new CMFCPropertyGridProperty(_T("Use Error Checking : Door Sensor"), lpszUsableTable[0], _T("Set whether the fluorescent light or motor stop function is used when the door is opened."));
	pProp->AddOption(lpszUsableTable[0]);
	pProp->AddOption(lpszUsableTable[1]);
	pProp->AllowEdit(FALSE);
	apGroup_Inspector->AddSubItem(pProp);

	pProp = new CMFCPropertyGridProperty(_T("Use Error Checking : Area Sensor"), lpszUsableTable[0], _T("Set whether the motor stop function is used when a safety sensor is detected."));
	pProp->AddOption(lpszUsableTable[0]);
	pProp->AddOption(lpszUsableTable[1]);
	pProp->AllowEdit(FALSE);
	apGroup_Inspector->AddSubItem(pProp);



	// 설비 코드
	apGroup_Inspector->AddSubItem(new CMFCPropertyGridProperty(_T("Equipment Code"), _T(""), _T("Settings Equipment Code")));


	m_wndPropList.AddProperty(apGroup_Inspector.release());

	//-----------------------------------------------------
	// 경로 설정
	//-----------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Path(new CMFCPropertyGridProperty(_T("Path Setting")));

	//apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("MES Path (Receive)"), _T("D:\\system\\MES\\Receive")));
	//apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("MES Path (Send)"), _T("D:\\system\\MES\\Send")));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Log Path"), _T("D:\\system\\Log")));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Report Path"), _T("D:\\system\\Report")));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Model Path"), _T("D:\\system\\Model")));

	if (g_InspectorTable[m_InsptrType].bPCIMotion == TRUE)
		apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Motor Path"), _T("D:\\system\\Motor")));
	
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Cable Path"), _T("D:\\system\\Cable")));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Image Path"), _T("D:\\system\\Image")));
	//apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("I2C Path"), _T("D:\\system\\I2C")));
	//apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Wav Path"), _T("D:\\system\\Wav")));
// 	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Barcode Path"), _T("D:\\system\\Barcode")));
	//apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Operator Path"), _T("D:\\system\\Operator")));
	//apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Prn Path"), _T("D:\\system\\Printer")));

	m_wndPropList.AddProperty(apGroup_Path.release());
//	
//#ifdef _DEBUG
//	//-----------------------------------------------------
//	// 프로그램 운영 설정
//	//-----------------------------------------------------
//	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Comm(new CMFCPropertyGridProperty(_T("Operation Setting")));
//
//	// 자동 재 실행 사용 여부
//	pProp = new CMFCPropertyGridProperty(_T("비정상 종료 시 자동 재 실행 사용 여부"), lpszUsableTable[0], OPT_DESC_USE_AUTO_RESTART);
//	pProp->AddOption(lpszUsableTable[0]);
//	pProp->AddOption(lpszUsableTable[1]);
//	pProp->AllowEdit(FALSE);
//	apGroup_Comm->AddSubItem(pProp);
//
//	// 장치 연결상태 표시 윈도우 사용여부
//	pProp = new CMFCPropertyGridProperty(_T("장치 연결상태 표시 윈도우 사용여부"), lpszUsableTable[1], OPT_DESC_USE_DEVICE_INFO_PANE);
//	pProp->AddOption(lpszUsableTable[0]);
//	pProp->AddOption(lpszUsableTable[1]);
//	pProp->AllowEdit(FALSE);
//	apGroup_Comm->AddSubItem(pProp);
//
//	// 자동 통신 연결 사용 여부
//	pProp = new CMFCPropertyGridProperty(_T("자동 통신 연결 사용 여부"), lpszUsableTable[0], OPT_DESC_USE_AUTO_CONN);
//	pProp->AddOption(lpszUsableTable[0]);
//	pProp->AddOption(lpszUsableTable[1]);
//	pProp->AllowEdit(FALSE);
//	apGroup_Comm->AddSubItem(pProp);
//	
//	m_wndPropList.AddProperty(apGroup_Comm.release());
//#endif
//
	//-----------------------------------------------------
	// 기타 설정
	//-----------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Etc(new CMFCPropertyGridProperty(_T("Etc Setting")));
	apGroup_Etc->AddSubItem(new CPasswordProp(_T("Password Administrator"), _T(""), _T("Settings Editable administrator password")));
	//apGroup_Etc->AddSubItem(new CMFCPropertyGridProperty(_T("Refence Barcode Seperator"), _T("ref:"), _T("Settings Editable Refence Barcode Seperator")));
	
	pProp = new CMFCPropertyGridProperty(_T("Report Image Save Type"), lpszImageSaveType[ImageSaveType_Notuse], _T("Report Image Save Type."));
	pProp->AddOption(lpszImageSaveType[ImageSaveType_Notuse]);
	pProp->AddOption(lpszImageSaveType[ImageSaveType_NG]);
	pProp->AddOption(lpszImageSaveType[ImageSaveType_ALL]);
	pProp->AllowEdit(FALSE);
	apGroup_Etc->AddSubItem(pProp);

	// Logo Type
	pProp = new CMFCPropertyGridProperty(_T("Logo Type"), lpszLogoType[0], _T("Choose Logo Type"));
	for (UINT nIndex = 0; NULL != lpszLogoType[nIndex]; nIndex++)
		pProp->AddOption(lpszLogoType[nIndex]);
	pProp->AllowEdit(FALSE);
	apGroup_Etc->AddSubItem(pProp);

	m_wndPropList.AddProperty(apGroup_Etc.release());
}

//=============================================================================
// Method		: CPageOpt_Insp::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_Insp::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption	= GetOption ();

	m_pLT_Option->SaveOption_Inspector(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_Insp::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_Insp::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_Inspector(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_Insp::GetOption
// Access		: protected 
// Returns		: Luritech_Option::stOption_Inspector
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_Insp CPageOpt_Insp::GetOption()
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	COleVariant rVariant;
	VARIANT		varData;
	CString		strValue;

	//---------------------------------------------------------------
	// 기본 검사 설정
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;

	USES_CONVERSION;

	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// 마스터 사용 여부
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
	{
		if (lpszUsableTable[nIndex] == strValue)
			break;
	}
	m_stOption.bUseMasterCheck = (BOOL)nIndex;

	// 바코드 사용 여부
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
	{
		if (lpszUsableTable[nIndex] == strValue)
			break;
	}
	m_stOption.bUseBarcode = (BOOL)nIndex;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
	{
		if (lpszUsableTable[nIndex] == strValue)
			break;
	}
	m_stOption.bUseFailbox = (BOOL)nIndex;


	// 프린터 사용 여부
	//rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	//varData = rVariant.Detach();
	//ASSERT(varData.vt == VT_BSTR);
	//strValue = OLE2A(varData.bstrVal);
	//for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
	//{
	//	if (lpszUsableTable[nIndex] == strValue)
	//		break;
	//}
	//m_stOption.bUsePrinter = (BOOL)nIndex;

	// Door Open 오류 처리 사용여부
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
	{
		if (lpszUsableTable[nIndex] == strValue)
			break;
	}
	m_stOption.bUseDoorOpen_Err = (BOOL)nIndex;

	// 안전 센서 처리 사용여부
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
	{
		if (lpszUsableTable[nIndex] == strValue)
			break;
	}
	m_stOption.bUseAreaSen_Err = (BOOL)nIndex;

	
	// 설비코드
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);

	m_stOption.EqpCode = strValue;


	//-----------------------------------------------------
	// 경로 설정
	//-----------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Log = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Report = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Model = strValue;

	if ((TRUE == g_InspectorTable[m_InsptrType].bPCIMotion))
	{
		rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
		varData = rVariant.Detach();
		ASSERT(varData.vt == VT_BSTR);
		strValue = OLE2A(varData.bstrVal);
		m_stOption.szPath_Motor = strValue;
	}

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Pogo = strValue;
	
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Image = strValue;

	//rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	//varData = rVariant.Detach();
	//ASSERT(varData.vt == VT_BSTR);
	//strValue = OLE2A(varData.bstrVal);
	//m_stOption.szPath_I2CFile = strValue;

// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	m_stOption.szPath_WavFile = strValue;

// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	m_stOption.szPath_BarcodeFile = strValue;

	//rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	//varData = rVariant.Detach();
	//ASSERT(varData.vt == VT_BSTR);
	//strValue = OLE2A(varData.bstrVal);
	//m_stOption.szPath_OperatorFile = strValue;

	//rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	//varData = rVariant.Detach();
	//ASSERT(varData.vt == VT_BSTR);
	//strValue = OLE2A(varData.bstrVal);
	//m_stOption.szPath_PrnFile = strValue;
	
#ifdef _DEBUG
	//---------------------------------------------------------------
	// 프로그램 운영 설정
	//---------------------------------------------------------------
// 	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
// 	iSubItemCount = pPropertyGroup->GetSubItemsCount();
// 	nSubItemIndex = 0;
// 
// 	// 자동 재 실행 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UseAutoRestart = (BOOL)nIndex;
// 
// 	// 장치 연결상태 표시 윈도우 사용여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UseDeviceInfoPane = (BOOL)nIndex;
// 
// 	// 자동 통신 연결 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UseAutoConnection = (BOOL)nIndex;

#endif

	//---------------------------------------------------------------
	// 기타 설정
	//---------------------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// Password -----------------------------
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData	 = rVariant.Detach();
	ASSERT (varData.vt == VT_BSTR);	
	strValue = OLE2A(varData.bstrVal);

	m_stOption.Password_Admin = strValue;

	// Refence Barcode Seperator -----------------------------
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	m_stOption.szBarcodeRef = strValue;

	// 이미지 저장 방식
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	for (nIndex = 0; NULL != lpszImageSaveType[nIndex]; nIndex++)
	{
		if (lpszImageSaveType[nIndex] == strValue)
			break;
	}
	m_stOption.nImageSaveType = (UINT)nIndex;

	// Logo Type
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	for (nIndex = 0; NULL != lpszLogoType[nIndex]; nIndex++)
	{
		if (lpszLogoType[nIndex] == strValue)
			break;
	}
	m_stOption.nLogoType = nIndex;

	return m_stOption;
}

//=============================================================================
// Method		: CPageOpt_Insp::SetOption
// Access		: protected 
// Returns		: void
// Parameter	: stOption_Inspector stOption
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
void CPageOpt_Insp::SetOption( stOpt_Insp stOption )
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	//---------------------------------------------------------------
	// 기본 검사 설정
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;

	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// 마스터 사용 여부
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseMasterCheck]);

	// 바코드 사용 여부
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseBarcode]);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseFailbox]);

	// 프린터 사용 여부
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUsePrinter]);

	//Door Open 오류 처리 사용여부
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseDoorOpen_Err]);
	
	//안전 센서 처리 사용여부
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseAreaSen_Err]);

	// 설비 코드
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.EqpCode);

	//-----------------------------------------------------
	// 경로 설정
	//-----------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Log);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Report);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Model);

	if (g_InspectorTable[m_InsptrType].bPCIMotion == TRUE)
		(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Motor);

	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Pogo);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Image);
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_I2CFile);
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_WavFile);
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_BarcodeFile);
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_OperatorFile);
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_PrnFile);
	
// #ifdef _DEBUG
// 	//---------------------------------------------------------------
// 	// 프로그램 운영 설정
// 	//---------------------------------------------------------------
// 	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
// 	iSubItemCount = pPropertyGroup->GetSubItemsCount();
// 	nSubItemIndex = 0;
// 
// 	// 자동 재 실행 사용 여부
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UseAutoRestart]);
// 	// 장치 연결상태 표시 윈도우 사용여부
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UseDeviceInfoPane]);
// 	// 자동 통신 연결 사용 여부
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UseAutoConnection]);
// #endif

	//---------------------------------------------------------------
	// 기타 설정
	//---------------------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// Password -----------------------------
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.Password_Admin);
	// Reference Barcode Seperator -----------------------------
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szBarcodeRef);
	// 이미지 저장 방식
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszImageSaveType[m_stOption.nImageSaveType]);

	// Logo Type
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszLogoType[m_stOption.nLogoType]);
}