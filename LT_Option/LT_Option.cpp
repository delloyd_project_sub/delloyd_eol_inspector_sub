﻿//*****************************************************************************
// Filename	: LT_Option.cpp
// Created	: 2016/03/09
// Modified	: 2016/03/09
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "stdafx.h"
#include "LT_Option.h"
#include "Define_OptionDescription.h"
#include "Registry.h"

#define		REG_PATH_OPTION_DEFALT	_T("Software\\Luritech\\Delloyd\\Final\\Environment\\Option")

//=============================================================================
//
//=============================================================================
CLT_Option::CLT_Option()
{	
	m_strRegPath_Base = REG_PATH_OPTION_DEFALT;

	m_InsptrType	= enInsptrSysType::Sys_FinalTest;
	m_bUseEVMS		= FALSE;
}

//=============================================================================
//
//=============================================================================
CLT_Option::~CLT_Option()
{

}

//=============================================================================
// Method		: CLT_Option::SetRegistryPath
// Access		: public 
// Returns		: void
// Parameter	: LPCTSTR lpszRegPath
// Qualifier	:
// Last Update	: 2016/03/09
// Desc.		: 옵션이 저장될 레지스트리의 경로 설정
//=============================================================================
void CLT_Option::SetRegistryPath( LPCTSTR lpszRegPath )
{
	if (NULL == lpszRegPath)
		AfxMessageBox(_T("lpszRegPath is NULL"));

	m_strRegPath_Base = lpszRegPath;
}

//=============================================================================
// Method		: CLT_Option::SaveOption_Inspector
// Access		: public 
// Returns		: void
// Parameter	: stOpt_Insp stOption
// Qualifier	:
// Last Update	: 2016/03/09
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_Inspector( stOpt_Insp stOption )
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\Inspector");
	CString		strValue;
	CString		strPgmFlag;
	
	m_stOption.Inspector = stOption;
	strPgmFlag = g_szInsptrSysType[m_InsptrType];

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 마스터 사용 여부
		reg.WriteDWORD(strPgmFlag + _T("_Use_MasterCheck"), (DWORD)m_stOption.Inspector.bUseMasterCheck);

		// 바코드 사용 여부
		reg.WriteDWORD(strPgmFlag + _T("_Use_Barcode"), (DWORD)m_stOption.Inspector.bUseBarcode);
		reg.WriteDWORD(strPgmFlag + _T("_Use_FailBox"), (DWORD)m_stOption.Inspector.bUseFailbox);

		// 프린터 사용 여부
		reg.WriteDWORD(strPgmFlag + _T("_Use_Printer"), (DWORD)m_stOption.Inspector.bUsePrinter);

 		// Door Open 오류 처리 사용여부
		reg.WriteDWORD(strPgmFlag + _T("_Use_DoorOpenErr"), (DWORD)m_stOption.Inspector.bUseDoorOpen_Err);

 		// 안전 센서처리 사용여부
		reg.WriteDWORD(strPgmFlag + _T("_Use_AreaSenErr"), (DWORD)m_stOption.Inspector.bUseAreaSen_Err);

		// 설비 코드
		reg.WriteString(strPgmFlag + _T("_EqpCode"), m_stOption.Inspector.EqpCode);

// 		// 인터락 사용
// 		reg.WriteDWORD(_T("Use_Interlock"), (DWORD)m_stOption.Inspector.bUseInterlock);
// 		// Start Master Set Time
// 		reg.WriteDWORD(_T("StartMasterSetTime"), (DWORD)m_stOption.Inspector.dwStartMasterSetTime); // 분 단위
// 		// Chart Monitor Index
// 		reg.WriteDWORD(OPT_KEY_CHARTMONITOR_INDEX, (DWORD)m_stOption.Inspector.nChartMonitorIndex);
// 
// 		// 알람 사용 여부
// 		reg.WriteDWORD(_T("Alarm_Usable"), (DWORD)m_stOption.Inspector.bUseAlarm);
// 		// 알람 지속시간
// 		reg.WriteDWORD(_T("Alarm_Duration"), m_stOption.Inspector.dwAlarmDuration);

		// Log 경로
		reg.WriteString(strPgmFlag + _T("_Path_Log"), m_stOption.Inspector.szPath_Log);
		// Report 경로
		reg.WriteString(strPgmFlag + _T("_Path_Report"), m_stOption.Inspector.szPath_Report);
		// Model 경로
		reg.WriteString(strPgmFlag + _T("_Path_Model"), m_stOption.Inspector.szPath_Model);
		// Motor 경로
		reg.WriteString(strPgmFlag + _T("_Path_Motor"), m_stOption.Inspector.szPath_Motor);
		// Pogo 경로
		reg.WriteString(strPgmFlag + _T("_Path_Pogo"), m_stOption.Inspector.szPath_Pogo);
		// Image 경로
		reg.WriteString(strPgmFlag + _T("_Path_Image"), m_stOption.Inspector.szPath_Image);
		// I2C 경로
		reg.WriteString(strPgmFlag + _T("_Path_I2C"), m_stOption.Inspector.szPath_I2CFile);
		// WAV 경로
		reg.WriteString(strPgmFlag + _T("_Path_WAV"), m_stOption.Inspector.szPath_WavFile);
		// 표준 바코드 경로
		reg.WriteString(strPgmFlag + _T("_Path_BARCODE"), m_stOption.Inspector.szPath_BarcodeFile);
		// 작업자 정보 경로
		reg.WriteString(strPgmFlag + _T("_Path_Operator"), m_stOption.Inspector.szPath_OperatorFile);
		// 프린터 정보 경로
		reg.WriteString(strPgmFlag + _T("_Path_Printer"), m_stOption.Inspector.szPath_PrnFile);

		// 표준 바코드 구분자
		reg.WriteString(strPgmFlag + _T("_Reference_Barcode"), m_stOption.Inspector.szBarcodeRef);

		// 레포트 이미지 저장 방식
		reg.WriteDWORD(strPgmFlag + _T("_ImageSave_Type"), (DWORD)m_stOption.Inspector.nImageSaveType);

		// 자동 재 실행 사용 여부
		reg.WriteDWORD(OPT_KEY_USE_AUTO_RESTART, (DWORD)m_stOption.Inspector.UseAutoRestart);
		// 장치 연결상태 표시 윈도우 사용여부
		reg.WriteDWORD(OPT_KEY_USE_DEVICE_INIFO_PANE, (DWORD)m_stOption.Inspector.UseDeviceInfoPane);
		// 자동 통신 연결 사용 여부
		reg.WriteDWORD(OPT_KEY_USE_AUTO_CONN, (DWORD)m_stOption.Inspector.UseAutoConnection);
		// Chart Monitor Index
	}

	reg.Close();

	if (reg.Open(HKEY_CURRENT_USER, m_strRegPath_Base))
	{
		//Password
		reg.WriteString(OPT_KEY_PASSWORD, m_stOption.Inspector.Password_Admin);
		//reg.WriteString(_T("Password_Mgr"), m_stOption.Inspector.Password_ReadOnly);

		// Logo Type
		reg.WriteDWORD(_T("Logo_Type"), (DWORD)m_stOption.Inspector.nLogoType);

		reg.Close();
	}
}

//=============================================================================
// Method		: CLT_Option::LoadOption_Inspector
// Access		: public 
// Returns		: BOOL
// Parameter	: stOpt_Insp & stOption
// Qualifier	:
// Last Update	: 2016/03/09
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_Inspector(stOpt_Insp& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\Inspector");
	DWORD		dwValue		= 0;
	CString		strValue;
	CString		strPgmFlag;

	strPgmFlag = g_szInsptrSysType[m_InsptrType];

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 마스터 사용 여부
		if (reg.ReadDWORD(strPgmFlag + _T("_Use_MasterCheck"), dwValue))
			m_stOption.Inspector.bUseMasterCheck = (BOOL)dwValue;

		// 바코드 사용 여부
		if (reg.ReadDWORD(strPgmFlag + _T("_Use_Barcode"), dwValue))
			m_stOption.Inspector.bUseBarcode = (BOOL)dwValue;

		if (reg.ReadDWORD(strPgmFlag + _T("_Use_FailBox"), dwValue))
			m_stOption.Inspector.bUseFailbox = (BOOL)dwValue;
		// 프린터 사용 여부
		if (reg.ReadDWORD(strPgmFlag + _T("_Use_Printer"), dwValue))
			m_stOption.Inspector.bUsePrinter = (BOOL)dwValue;

 		// Door Open 오류 처리 사용여부
		if (reg.ReadDWORD(strPgmFlag + _T("_Use_DoorOpenErr"), dwValue))
  			m_stOption.Inspector.bUseDoorOpen_Err = (BOOL)dwValue;
 
 		// 안전 센서처리 사용여부
		if (reg.ReadDWORD(strPgmFlag + _T("_Use_AreaSenErr"), dwValue))
			m_stOption.Inspector.bUseAreaSen_Err = (BOOL)dwValue;

		// 설비 코드
		if (reg.ReadString(strPgmFlag + _T("_EqpCode"), strValue))
			m_stOption.Inspector.EqpCode = strValue;
// 
// 		// 인터락 사용
// 		if (reg.ReadDWORD(_T("Use_Interlock"), dwValue))
// 			m_stOption.Inspector.bUseInterlock = (BOOL)dwValue;
// 		
// 		// Start Master Set Time
// 		if (reg.ReadDWORD(_T("StartMasterSetTime"), dwValue))
// 			m_stOption.Inspector.dwStartMasterSetTime = dwValue; // 분 단위
// 
// 		// Chart Monitor Index
// 		if (reg.ReadDWORD(OPT_KEY_CHARTMONITOR_INDEX, dwValue))
// 			m_stOption.Inspector.nChartMonitorIndex = (UINT)dwValue;
// 
// 		// 알람 사용 여부
// 		if (reg.ReadDWORD(_T("Alarm_Usable"), dwValue))
// 			m_stOption.Inspector.bUseAlarm = (BOOL)dwValue;
// 
// 		// 알람 지속시간
// 		if (reg.ReadDWORD(_T("Alarm_Duration"), dwValue))
// 			m_stOption.Inspector.dwAlarmDuration = dwValue;

		// Log 경로
		reg.ReadString(strPgmFlag + _T("_Path_Log"), m_stOption.Inspector.szPath_Log);
		// Report 경로
		reg.ReadString(strPgmFlag + _T("_Path_Report"), m_stOption.Inspector.szPath_Report);
		// Model 경로
		reg.ReadString(strPgmFlag + _T("_Path_Model"), m_stOption.Inspector.szPath_Model);
		// Motor 경로
		reg.ReadString(strPgmFlag + _T("_Path_Motor"), m_stOption.Inspector.szPath_Motor);
		// Pogo 경로
		reg.ReadString(strPgmFlag + _T("_Path_Pogo"), m_stOption.Inspector.szPath_Pogo);
		// 영상 Image 경로
		reg.ReadString(strPgmFlag + _T("_Path_Image"), m_stOption.Inspector.szPath_Image);
		// I2C 경로
		reg.ReadString(strPgmFlag + _T("_Path_I2C"), m_stOption.Inspector.szPath_I2CFile);
		// WAV 경로
		reg.ReadString(strPgmFlag + _T("_Path_WAV"), m_stOption.Inspector.szPath_WavFile);
		// 표준 바코드 경로
		reg.ReadString(strPgmFlag + _T("_Path_BARCODE"), m_stOption.Inspector.szPath_BarcodeFile);
		// 작업자 정보 경로
		reg.ReadString(strPgmFlag + _T("_Path_Operator"), m_stOption.Inspector.szPath_OperatorFile);
		// 작업자 정보 경로
		reg.ReadString(strPgmFlag + _T("_Path_Printer"), m_stOption.Inspector.szPath_PrnFile);
		// 표준 바코드 구분자
		reg.ReadString(strPgmFlag + _T("_Reference_Barcode"), m_stOption.Inspector.szBarcodeRef);

		// 레포트 이미지 저장 방식
		if (reg.ReadDWORD(strPgmFlag + _T("_ImageSave_Type"), dwValue))
			m_stOption.Inspector.nImageSaveType = (UINT)dwValue;

// 		// BinFile 경로
// 		reg.ReadString(_T("Path_BinFile"), m_stOption.Inspector.szPath_BinFile);
// 		// 센서 Bin 저장 경로
// 		reg.ReadString(_T("Path_Sensor"), m_stOption.Inspector.szPath_Sensor);
// 		// 파워 Hex 저장 경로
// 		reg.ReadString(_T("Path_Power"), m_stOption.Inspector.szPath_Power);

// 		// 자동 재 실행 사용 여부
// 		if (reg.ReadDWORD(OPT_KEY_USE_AUTO_RESTART, dwValue))
// 			m_stOption.Inspector.UseAutoRestart = (BOOL)dwValue;
// 		
// 		// 장치 연결상태 표시 윈도우 사용여부
// 		if (reg.ReadDWORD(OPT_KEY_USE_DEVICE_INIFO_PANE, dwValue))
// 			m_stOption.Inspector.UseDeviceInfoPane = (BOOL)dwValue;
// 
// 		// 자동 통신 연결 사용 여부
// 		if (reg.ReadDWORD(OPT_KEY_USE_AUTO_CONN, dwValue))
// 			m_stOption.Inspector.UseAutoConnection = (BOOL) dwValue;
	}
	else
	{
		return FALSE;
	}

	reg.Close();

	if (reg.Open(HKEY_CURRENT_USER, m_strRegPath_Base))
	{
		//Password
		if (reg.ReadString(OPT_KEY_PASSWORD, strValue))
			m_stOption.Inspector.Password_Admin = strValue;

// 		if (reg.ReadString(_T("Password_Mgr"), strValue))
// 			m_stOption.Inspector.Password_ReadOnly = strValue;

		// Logo Type
		if (reg.ReadDWORD(_T("Logo_Type"), dwValue))
			m_stOption.Inspector.nLogoType = (int)dwValue;

		reg.Close();
	}

	stOption = m_stOption.Inspector;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_BCR
// Access		: public  
// Returns		: void
// Parameter	: stOpt_BCR stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:58
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_BCR( stOpt_BCR stOption )
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\BCR");
	CString		strValue;
	CString		szKey;

	m_stOption.BCR = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Barcode Reader
		// 통신 포트
		reg.WriteDWORD(OPT_KEY_COMPORT, (DWORD)m_stOption.BCR.ComPort.Port);

		// Baud Rate 
		reg.WriteDWORD(OPT_KEY_BAUDRATE, m_stOption.BCR.ComPort.BaudRate);

		// ByteSize 
		reg.WriteDWORD(OPT_KEY_BYTESIZE, (DWORD)m_stOption.BCR.ComPort.ByteSize);

		// Parity 
		reg.WriteDWORD(OPT_KEY_PARITY, (DWORD)m_stOption.BCR.ComPort.Parity);

		// StopBits 
		reg.WriteDWORD(OPT_KEY_STOPBITS, (DWORD)m_stOption.BCR.ComPort.StopBits);


		// Label Printer
		// 통신 포트
		szKey.Format(_T("Label_%s"), OPT_KEY_COMPORT);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.ComPort_Label.Port);

		// Baud Rate 
		szKey.Format(_T("Label_%s"), OPT_KEY_BAUDRATE);
		reg.WriteDWORD(szKey, m_stOption.BCR.ComPort_Label.BaudRate);

		// ByteSize 
		szKey.Format(_T("Label_%s"), OPT_KEY_BYTESIZE);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.ComPort_Label.ByteSize);

		// Parity 
		szKey.Format(_T("Label_%s"), OPT_KEY_PARITY);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.ComPort_Label.Parity);

		// StopBits 
		szKey.Format(_T("Label_%s"), OPT_KEY_STOPBITS);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.ComPort_Label.StopBits);


		if (m_InsptrType == g_InspectorTable[m_InsptrType].SysType)
		{
			for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].nIndigatorCnt; nIdx++)
			{
				// 통신 포트
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_COMPORT, nIdx + 1);
				reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.ComPort_Motor[nIdx].Port);

				// Baud Rate 
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_BAUDRATE, nIdx + 1);
				reg.WriteDWORD(szKey, m_stOption.BCR.ComPort_Motor[nIdx].BaudRate);

				// ByteSize 
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_BYTESIZE, nIdx + 1);
				reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.ComPort_Motor[nIdx].ByteSize);

				// Parity 
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_PARITY, nIdx + 1);
				reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.ComPort_Motor[nIdx].Parity);

				// StopBits 
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_STOPBITS, nIdx + 1);
				reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.ComPort_Motor[nIdx].StopBits);
			}
		}
	}

	reg.Close();
}

//=============================================================================
// Method		: LoadOption_BCR
// Access		: public  
// Returns		: BOOL
// Parameter	: stOpt_BCR & stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:58
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_BCR(stOpt_BCR& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\BCR");
	DWORD		dwValue		= 0;
	CString		strValue;
	CString		szKey;

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Barcode Reader
		// 통신 포트 
		if (reg.ReadDWORD(OPT_KEY_COMPORT, dwValue))
			m_stOption.BCR.ComPort.Port = (BYTE)dwValue;

		// Baud Rate 
		if (reg.ReadDWORD(OPT_KEY_BAUDRATE, dwValue))
			m_stOption.BCR.ComPort.BaudRate = dwValue;

		// ByteSize 
		if (reg.ReadDWORD(OPT_KEY_BYTESIZE, dwValue))
			m_stOption.BCR.ComPort.ByteSize = (BYTE)dwValue;

		// Parity 
		if (reg.ReadDWORD(OPT_KEY_PARITY, dwValue))
			m_stOption.BCR.ComPort.Parity = (BYTE)dwValue;

		// StopBits 
		if (reg.ReadDWORD(OPT_KEY_STOPBITS, dwValue))
			m_stOption.BCR.ComPort.StopBits = (BYTE)dwValue;


		// Label Printer
		// 통신 포트 
		szKey.Format(_T("Label_%s"), OPT_KEY_COMPORT);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.ComPort_Label.Port = (BYTE)dwValue;

		// Baud Rate 
		szKey.Format(_T("Label_%s"), OPT_KEY_BAUDRATE);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.ComPort_Label.BaudRate = dwValue;

		// ByteSize 
		szKey.Format(_T("Label_%s"), OPT_KEY_BYTESIZE);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.ComPort_Label.ByteSize = (BYTE)dwValue;

		// Parity 
		szKey.Format(_T("Label_%s"), OPT_KEY_PARITY);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.ComPort_Label.Parity = (BYTE)dwValue;

		// StopBits 
		szKey.Format(_T("Label_%s"), OPT_KEY_STOPBITS);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.ComPort_Label.StopBits = (BYTE)dwValue;


		if (m_InsptrType == g_InspectorTable[m_InsptrType].SysType)
		{
			for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].nIndigatorCnt; nIdx++)
			{
				// 통신 포트 
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_COMPORT, nIdx + 1);
				if (reg.ReadDWORD(szKey, dwValue))
					m_stOption.BCR.ComPort_Motor[nIdx].Port = (BYTE)dwValue;

				// Baud Rate 
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_BAUDRATE, nIdx + 1);
				if (reg.ReadDWORD(szKey, dwValue))
					m_stOption.BCR.ComPort_Motor[nIdx].BaudRate = dwValue;

				// ByteSize 
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_BYTESIZE, nIdx + 1);
				if (reg.ReadDWORD(szKey, dwValue))
					m_stOption.BCR.ComPort_Motor[nIdx].ByteSize = (BYTE)dwValue;

				// Parity 
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_PARITY, nIdx + 1);
				if (reg.ReadDWORD(szKey, dwValue))
					m_stOption.BCR.ComPort_Motor[nIdx].Parity = (BYTE)dwValue;

				// StopBits 
				szKey.Format(_T("Motor_%s_%02d"), OPT_KEY_STOPBITS, nIdx + 1);
				if (reg.ReadDWORD(szKey, dwValue))
					m_stOption.BCR.ComPort_Motor[nIdx].StopBits = (BYTE)dwValue;
			}
		}
	}
	else
	{
		return FALSE;
	}

	reg.Close();

	stOption = m_stOption.BCR;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_PCB
// Access		: public  
// Returns		: void
// Parameter	: stOpt_PCB stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:58
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_PCB(stOpt_PCB stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\PCB");

	m_stOption.PCB = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// PCB ---------------------------------------		
		CString szKey;
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].nPCBCamCnt; nIdx++)
		{
			// 통신 포트
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_COMPORT, nIdx + 1);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_CamBrd[nIdx].Port);

			// Baud Rate 
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_BAUDRATE, nIdx + 1);
			reg.WriteDWORD(szKey, m_stOption.PCB.ComPort_CamBrd[nIdx].BaudRate);

			// ByteSize 
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_BYTESIZE, nIdx + 1);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_CamBrd[nIdx].ByteSize);

			// Parity 
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_PARITY, nIdx + 1);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_CamBrd[nIdx].Parity);

			// StopBits 
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_STOPBITS, nIdx + 1);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_CamBrd[nIdx].StopBits);
		}
	}
	reg.Close();
}

//=============================================================================
// Method		: LoadOption_PCB
// Access		: public  
// Returns		: BOOL
// Parameter	: stOpt_PCB & stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:58
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_PCB(stOpt_PCB& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\PCB");
	DWORD		dwValue = 0;

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// PCB ---------------------------------------
		CString szKey;
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].nPCBCamCnt; nIdx++)
		{
			// 통신 포트 
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_COMPORT, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CamBrd[nIdx].Port = (BYTE)dwValue;

			// Baud Rate 
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_BAUDRATE, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CamBrd[nIdx].BaudRate = dwValue;

			// ByteSize 
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_BYTESIZE, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CamBrd[nIdx].ByteSize = (BYTE)dwValue;

			// Parity 
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_PARITY, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CamBrd[nIdx].Parity = (BYTE)dwValue;

			// StopBits 
			szKey.Format(_T("CAM_%s_%02d"), OPT_KEY_STOPBITS, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CamBrd[nIdx].StopBits = (BYTE)dwValue;
		}
	}
	else
	{
		return FALSE;
	}
	reg.Close();

	stOption = m_stOption.PCB;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_Light
// Access		: public  
// Returns		: void
// Parameter	: __in stOpt_PCB stOption
// Qualifier	:
// Last Update	: 2017/6/29 - 10:53
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_Light(__in stOpt_PCB stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\PCB");

	m_stOption.PCB = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 광원 ---------------------------------------		
		CString szKey;
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].nLightBrdCount; nIdx++)
		{
			// 통신 포트
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_COMPORT, nIdx + 1);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightBrd[nIdx].Port);

			// Baud Rate 
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_BAUDRATE, nIdx + 1);
			reg.WriteDWORD(szKey, m_stOption.PCB.ComPort_LightBrd[nIdx].BaudRate);

			// ByteSize 
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_BYTESIZE, nIdx + 1);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightBrd[nIdx].ByteSize);

			// Parity 
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_PARITY, nIdx + 1);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightBrd[nIdx].Parity);

			// StopBits 
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_STOPBITS, nIdx + 1);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightBrd[nIdx].StopBits);
		}

	}
	reg.Close();
}

//=============================================================================
// Method		: LoadOption_Light
// Access		: public  
// Returns		: BOOL
// Parameter	: __out stOpt_PCB & stOption
// Qualifier	:
// Last Update	: 2017/6/29 - 10:53
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_Light(__out stOpt_PCB& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\PCB");
	DWORD		dwValue = 0;

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 광원 ---------------------------------------
		CString szKey;
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].nLightBrdCount; nIdx++)
		{
			// 통신 포트 
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_COMPORT, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].Port = (BYTE)dwValue;

			// Baud Rate 
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_BAUDRATE, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].BaudRate = dwValue;

			// ByteSize 
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_BYTESIZE, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].ByteSize = (BYTE)dwValue;

			// Parity 
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_PARITY, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].Parity = (BYTE)dwValue;

			// StopBits 
			szKey.Format(_T("LIGHT_%s_%02d"), OPT_KEY_STOPBITS, nIdx + 1);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].StopBits = (BYTE)dwValue;
		}
	}
	else
	{
		return FALSE;
	}
	reg.Close();

	stOption = m_stOption.PCB;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_Misc
// Access		: public  
// Returns		: void
// Parameter	: stOpt_Misc stOption
// Qualifier	:
// Last Update	: 2016/9/26 - 16:12
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_Misc(stOpt_Misc stOption)
{
// 	CRegistry	reg;
// 	CString		strRegPath = m_strRegPath_Base + _T("\\Misc");
// 	CString		strValue;
// 
// 	m_stOption.Misc = stOption;
// 
// 	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
// 	{
// 		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
// 	}
// 
// 	CString szkey;
// 	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
// 	{
// 
// 	}
// 
// 	reg.Close();
}

//=============================================================================
// Method		: LoadOption_Misc
// Access		: public  
// Returns		: BOOL
// Parameter	: stOpt_Misc & stOption
// Qualifier	:
// Last Update	: 2016/9/26 - 16:12
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_Misc(stOpt_Misc& stOption)
{
// 	CRegistry	reg;
// 	CString		strRegPath = m_strRegPath_Base + _T("\\Misc");
// 	DWORD		dwValue = 0;
// 	CString		strValue;
// 
// 	CString szkey;
// 	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
// 	{
// 
// 	}
// 	else
// 	{
// 		return FALSE;
// 	}
// 
// 	reg.Close();
// 
// 	stOption = m_stOption.Misc;

	return TRUE;
}


//=============================================================================
// Method		: SaveOption_SerialDevice
// Access		: public  
// Returns		: void
// Parameter	: __in stOpt_SerialDevice stOption
// Qualifier	:
// Last Update	: 2017/9/29 - 10:17
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_SerialDevice(__in stOpt_SerialDevice stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\SerialDevice");
	CString		strValue;

	m_stOption.SerialDevice = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	CString szkey;
	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 통신 포트
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_COMPORT);
		reg.WriteDWORD(szkey, (DWORD)m_stOption.SerialDevice.LaserSensor.Port);
		// Baud Rate 
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_BAUDRATE);
		reg.WriteDWORD(szkey, m_stOption.SerialDevice.LaserSensor.BaudRate);
		// ByteSize 
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_BYTESIZE);
		reg.WriteDWORD(szkey, (DWORD)m_stOption.SerialDevice.LaserSensor.ByteSize);
		// Parity 
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_PARITY);
		reg.WriteDWORD(szkey, (DWORD)m_stOption.SerialDevice.LaserSensor.Parity);
		// StopBits 
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_STOPBITS);
		reg.WriteDWORD(szkey, (DWORD)m_stOption.SerialDevice.LaserSensor.StopBits);

// 		// 통신 포트
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_COMPORT);
		reg.WriteDWORD(szkey, (DWORD)m_stOption.SerialDevice.VisionLightBrd.Port);
		// Baud Rate 
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_BAUDRATE);
		reg.WriteDWORD(szkey, m_stOption.SerialDevice.VisionLightBrd.BaudRate);
		// ByteSize 
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_BYTESIZE);
		reg.WriteDWORD(szkey, (DWORD)m_stOption.SerialDevice.VisionLightBrd.ByteSize);
		// Parity 
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_PARITY);
		reg.WriteDWORD(szkey, (DWORD)m_stOption.SerialDevice.VisionLightBrd.Parity);
		// StopBits 
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_STOPBITS);
		reg.WriteDWORD(szkey, (DWORD)m_stOption.SerialDevice.VisionLightBrd.StopBits);
	}

	reg.Close();
}


//=============================================================================
// Method		: LoadOption_SerialDevice
// Access		: public  
// Returns		: BOOL
// Parameter	: __out stOpt_SerialDevice & stOption
// Qualifier	:
// Last Update	: 2017/9/29 - 10:17
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_SerialDevice(__out stOpt_SerialDevice& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\SerialDevice");
	DWORD		dwValue = 0;
	CString		strValue;

	CString szkey;
	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// LaserSensor ----------------------------------------------
		// 통신 포트 
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_COMPORT);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.LaserSensor.Port = (BYTE)dwValue;

		// Baud Rate 
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_BAUDRATE);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.LaserSensor.BaudRate = dwValue;

		// ByteSize 
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_BYTESIZE);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.LaserSensor.ByteSize = (BYTE)dwValue;

		// Parity 
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_PARITY);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.LaserSensor.Parity = (BYTE)dwValue;

		// StopBits 
		szkey.Format(_T("LaserSensor_%s"), OPT_KEY_STOPBITS);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.LaserSensor.StopBits = (BYTE)dwValue;


		// Vision Light ----------------------------------------------------
		// 통신 포트 
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_COMPORT);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.VisionLightBrd.Port = (BYTE)dwValue;

		// Baud Rate 
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_BAUDRATE);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.VisionLightBrd.BaudRate = dwValue;

		// ByteSize 
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_BYTESIZE);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.VisionLightBrd.ByteSize = (BYTE)dwValue;

		// Parity 
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_PARITY);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.VisionLightBrd.Parity = (BYTE)dwValue;

		// StopBits 
		szkey.Format(_T("VisionLight_%s"), OPT_KEY_STOPBITS);
		if (reg.ReadDWORD(szkey, dwValue))
			m_stOption.SerialDevice.VisionLightBrd.StopBits = (BYTE)dwValue;
	}
	else
	{
		return FALSE;
	}

	reg.Close();

	stOption = m_stOption.SerialDevice;

	return TRUE;
}


//=============================================================================
// Method		: SaveOption_VisionCam
// Access		: public  
// Returns		: void
// Parameter	: __in stOpt_VisionCam stOption
// Qualifier	:
// Last Update	: 2017/9/29 - 10:17
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_VisionCam(__in stOpt_VisionCam stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\VisionCam");
	CString		strValue;

	m_stOption.VisionCam = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	CString strKey;
	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Vision Camera 1
		strKey.Format(_T("Cam1_%s"), OPT_KEY_IP_ADDRESS);
		reg.WriteDWORD(strKey, m_stOption.VisionCam.Address_Cam.dwAddress);

		// Image 경로
		reg.WriteString(_T("Path_VImage"), m_stOption.VisionCam.szPath_VImage);

	}

	reg.Close();
}


//=============================================================================
// Method		: LoadOption_VisionCam
// Access		: public  
// Returns		: BOOL
// Parameter	: __out stOpt_VisionCam & stOption
// Qualifier	:
// Last Update	: 2017/9/29 - 10:17
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_VisionCam(__out stOpt_VisionCam& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\VisionCam");
	DWORD		dwValue = 0;
	CString		strValue;

	CString strKey;
	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Vision Camera 1
		strKey.Format(_T("Cam1_%s"), OPT_KEY_IP_ADDRESS);
		if (reg.ReadDWORD(strKey, dwValue))
			m_stOption.VisionCam.Address_Cam.dwAddress = dwValue;

		// Image 경로
		reg.ReadString(_T("Path_VImage"), m_stOption.VisionCam.szPath_VImage);

	}
	else
	{
		return FALSE;
	}

	reg.Close();

	stOption = m_stOption.VisionCam;
	return TRUE;
}

//=============================================================================
// Method		: SaveOption_All
// Access		: public  
// Returns		: void
// Parameter	: stLT_Option stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 16:01
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_All( stLT_Option stOption )
{
	SaveOption_Inspector	(stOption.Inspector);
	SaveOption_BCR			(stOption.BCR);
	SaveOption_PCB			(stOption.PCB);
	SaveOption_Light		(stOption.PCB);
	SaveOption_Misc			(stOption.Misc);
	SaveOption_SerialDevice	(stOption.SerialDevice);
	SaveOption_VisionCam	(stOption.VisionCam);
}

//=============================================================================
// Method		: LoadOption_All
// Access		: public  
// Returns		: BOOL
// Parameter	: stLT_Option & stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 16:00
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_All( stLT_Option& stOption )
{
	BOOL bReturn = TRUE;

	bReturn &= LoadOption_Inspector		(stOption.Inspector);
	bReturn &= LoadOption_BCR			(stOption.BCR);
	bReturn &= LoadOption_PCB			(stOption.PCB);
	bReturn &= LoadOption_Light			(stOption.PCB);
	bReturn &= LoadOption_Misc			(stOption.Misc);
	bReturn &= LoadOption_SerialDevice	(stOption.SerialDevice);
	bReturn &= LoadOption_VisionCam		(stOption.VisionCam);

	return bReturn;
}

//=============================================================================
// Method		: SetInspectorType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nInsptrType
// Qualifier	:
// Last Update	: 2016/9/26 - 18:01
// Desc.		:
//=============================================================================
void CLT_Option::SetInspectorType(__in enInsptrSysType nInsptrType)
{
	m_InsptrType = nInsptrType;
}

//=============================================================================
// Method		: SetUseEVMS
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUseEVMS
// Qualifier	:
// Last Update	: 2016/11/21 - 15:47
// Desc.		:
//=============================================================================
void CLT_Option::SetUseEVMS(__in BOOL bUseEVMS)
{
	m_bUseEVMS = bUseEVMS;
}
