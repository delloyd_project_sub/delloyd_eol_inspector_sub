// *****************************************************************************
//  Filename	: 	TI_DefectPixel.cpp
//  Created	:	2018/2/11 - 16:17
//  Modified	:	2018/2/11 - 16:17
// 
//  Author	:	Luritech
// 	
//  Purpose	:	
// ****************************************************************************
#include "stdafx.h"
#include "TI_DefectPixel.h"


CTI_DefectPixel::CTI_DefectPixel()
{

}

CTI_DefectPixel::~CTI_DefectPixel()
{

}
//
////=============================================================================
//// Method		: Initialize
//// Access		: virtual public  
//// Returns		: void
//// Parameter	: BYTE * pScanBuf
//// Parameter	: int iWidth
//// Parameter	: int iHeight
//// Qualifier	:
//// Last Update	: 2018/11/8 - 15:01
//// Desc.		:
////=============================================================================
//void CTI_DefectPixel::Initialize(BYTE* pScanBuf, int iWidth, int iHeight)
//{
//	m_byScanBuf = pScanBuf;
//	m_iWidth	= iWidth;
//	m_iHeight	= iHeight;
//}
//
////=============================================================================
//// Method		: Inspection
//// Access		: public  
//// Returns		: ST_SO_DefectPixel_Result
//// Parameter	: ST_SO_DefectPixel_Opt stOption
//// Qualifier	:
//// Last Update	: 2018/11/8 - 15:01
//// Desc.		:
////=============================================================================
//ST_SO_DefectPixel_Result CTI_DefectPixel::Inspection(ST_SO_DefectPixel_Opt stOption)
//{
//	ST_SO_DefectPixel_Result	stResult;
//
//	stResult.stHeader.iTestItem		= stOption.stHeader.iTestItem;
//	stResult.wResultCode			= RC_AI_OK;
//
//#ifdef DEBUG_PRINT_LOG
//	printf("CTI_DefectPixel::Inspection()\n");
//	printf("CTI_DefectPixel::Inspection() : Very Hot Pixel Threshold : %.2f\n",		stOption.fVeryHot_Thres		);
//	printf("CTI_DefectPixel::Inspection() : Hot Pixel Threshold : %.2f\n",			stOption.fHot_Thres			);
//	printf("CTI_DefectPixel::Inspection() : Very Bright Pixel Threshold : %.2f\n",	stOption.fVeryBright_Thres	);
//	printf("CTI_DefectPixel::Inspection() : Bright Pixel Threshold : %.2f\n",		stOption.fBright_Thres		);
//	printf("CTI_DefectPixel::Inspection() : Very Dark Pixel Threshold : %.2f\n",	stOption.fVeryDark_Thres	);
//	printf("CTI_DefectPixel::Inspection() : Dark Pixel Threshold : %.2f\n",			stOption.fDark_Thres		);
//#endif
//
//	for (int k = 0; k < DefectPixel_F_Max; k++)
//	{
//		stResult.byFailType[k]		= 6;// Type 6 = None
//		stResult.sPointX[k]			= 0;
//		stResult.sPointY[k]			= 0;
//		stResult.fConcentration[k]	= 0.0;
//	}
//
//	// TEST
//	DefectPixel_Test(stOption.bType, stOption.byBlockSize, stOption.fVeryHot_Thres, stOption.fHot_Thres, stOption.fVeryBright_Thres, stOption.fBright_Thres, stOption.fVeryDark_Thres, stOption.fDark_Thres, stResult.byIndexCnt, stResult.sPointX, stResult.sPointY, stResult.fConcentration, stResult.byFailType);
//	
//	// Result
//	stResult.wResultCode = RC_AI_OK;
//
//#ifdef DEBUG_PRINT_LOG
//
//	for (int i = 0; i < stResult.byIndexCnt; i++)
//	{
//		printf("CTI_DefectPixel : Result ROI[%d] [ X:%d, Y:%d] \n", i, stResult.sPointX[i], stResult.sPointY[i]);
//	}
//
//
//	printf("CTI_DefectPixel::Inspection()\n");
//#endif
//
//	return stResult;
//}

//=============================================================================
// Method		: DefectPixel_Test
// Access		: protected  
// Returns		: void
// Parameter	: LPBYTE pImageBuf
// Parameter	: BOOL bTestMode
// Parameter	: BYTE byDP_BlockSize
// Parameter	: float fVeryHot_Tres
// Parameter	: float fHot_Tres
// Parameter	: float fVeryBright_Tres
// Parameter	: float fBright_Tres
// Parameter	: float fVeryDark_Tres
// Parameter	: float fDark_Tres
// Parameter	: BYTE & byIndexCount
// Parameter	: short * sPointX
// Parameter	: short * sPointY
// Parameter	: float * fConcentration
// Parameter	: BYTE * byFailType
// Qualifier	:
// Last Update	: 2018/12/03 - 15:13
// Desc.		:
//=============================================================================
void CTI_DefectPixel::DefectPixel_Test(LPBYTE pImageBuf, BOOL bTestMode, BYTE byDP_BlockSize
	, float fVeryHot_Tres, float fHot_Tres, float fVeryBright_Tres, float fBright_Tres, float fVeryDark_Tres, float fDark_Tres
	, BYTE& byIndexCount, short* sPointX, short* sPointY, float* fConcentration, BYTE* byFailType, int m_iWidth, int m_iHeight, int DefectPixel_F_Max, UINT nMargin_Left, UINT nMargin_Right)
{
	int iScalerW = m_iWidth - nMargin_Left - nMargin_Right;
	int iScalerH = m_iHeight ;

	if (iScalerW == 0 || iScalerH == 0)
	{
		printf("CTI_DefectPixel::DefectPixel_New() : [ERR] Input  'iScalerW' of 'iScalerH' Value  is Zero, Can Not Inspection");
		return;
	}
	if (byDP_BlockSize <= 2)
	{
		printf("CTI_DefectPixel::DefectPixel_New() : [ERR] Input  'byDP_BlockSize'  Value  is Zero, Can Not Inspection");
		return;
	}
	//if (fVeryHot_Tres == 0 || fHot_Tres == 0 || fVeryBright_Tres == 0 || fBright_Tres == 0 || fVeryDark_Tres == 0 || fDark_Tres == 0)
	//{
	//	printf("CTI_DefectPixel::DefectPixel_New() : [ERR] Input  'fTreshold'  Value  is Zero, Can Not Inspection");
	//	return;
	//}
	if (/*fVeryHot_Tres == 0 || fHot_Tres == 0 || fVeryBright_Tres == 0 || fBright_Tres == 0 || fVeryDark_Tres == 0 ||*/ fDark_Tres == 0)
	{
		printf("CTI_DefectPixel::DefectPixel_New() : [ERR] Input  'fTreshold'  Value  is Zero, Can Not Inspection");
		return;
	}

	// 1. bTestMode에 따라 ,,, 0 (조명OFF) : Hot Pixel ,,, 1 (조명On) : Dead Pixel 
	enum { Test_Hot, Test_Dead };
	enum { /*VERY_DARKPIXEL,*/ DARKPIXEL, CLUSTER /*, VERY_HOTPIXEL, HOTPIXEL, VERY_BRIGHTPIXEL, BRIGHTPIXEL, NONE_TYPE */};

	UINT Block_Size = byDP_BlockSize;
	if (Block_Size % 2 == 0)
		Block_Size++;

	// Input Image Test
	BYTE* wImageBuf = new BYTE[iScalerW*iScalerH];
	double dbFullIMG_Avg = 0.0;
	// 0. 알고리즘 내부에서 쓸 이미지 Buff Copy  
	for (int y = 0; y < iScalerH; y++)
	{
		for (int x = 0; x < iScalerW; x++)
		{
			// 이미지 데이터 접근 용도
			wImageBuf[y*iScalerW + x] = pImageBuf[y*(m_iWidth) + x + 3];

			// 평균값 구하는 용도
			dbFullIMG_Avg += wImageBuf[y*iScalerW + x];
		}
	}

	// 영상 전체 평균
	dbFullIMG_Avg /= (iScalerW*iScalerH);

	int StX = 0, StY = 0;
	int EndX = 0, EndY = 0;
	double Avg_Block = 0.0;
	double Conc_Pixel = 0.0;
	WORD Pixel_Y = 0;
	UINT BlockPixelCnt = (Block_Size) * (Block_Size) -1; // W x H 
	int _c = 0;
	UINT WandHSize_Half = (UINT)(byDP_BlockSize / 2);
	int Gap_of_Bright = 0;

	// VERY_BRIGHTPIXEL, BRIGHTPIXEL, VERY_DARKPIXEL, DARKPIXEL 구분하는 for문 ::: Test Mode = Test_Hot, Test_Dead 둘다 진입
	for (int j = 0; j < iScalerH ; j++)
	{
		for (int i = 0; i < iScalerW ; i++)
		{
			StX = i - WandHSize_Half;
			StY = j - WandHSize_Half;
			EndX = i + WandHSize_Half;
			EndY = j + WandHSize_Half;

			// 이미지 경계선 처리
			if (StX < 0){StX = 0;EndX = Block_Size - 1;}
			if (EndX > iScalerW - 1){EndX = iScalerW - 1;StX = EndX - (Block_Size-1);}
			if (StY < 0){StY = 0;EndY = Block_Size - 1;}
			if (EndY > iScalerH - 1){EndY = iScalerH - 1;StY = EndY - (Block_Size - 1);}
				
			Avg_Block = 0.0;
			_c = 0;
			for (int y = StY; y <= EndY; y++)
			{
				for (int x = StX; x <= EndX; x++)
				{
					if (y == j && x == i)
					{
						__null;
						// 검사하려는 픽셀이면 평균에 포함시키지 않는다.
					}
					else
					{
						Avg_Block += wImageBuf[y*iScalerW + x];
						_c++;
					}

				}
			}
			// 블럭 내부 평균값 (검사하는 Pixel 제외)
			Avg_Block = Avg_Block / _c;
			// 검사하는 Pixel의 밝기값
			Pixel_Y = wImageBuf[j*iScalerW + i];
			// 위 두개의 편차 -> 주변보다 어두우면 마이너스값, 밝으면 플러스 값
			Conc_Pixel = ((double)Pixel_Y - Avg_Block) / Avg_Block * 100;
			
			if (Conc_Pixel <= fDark_Tres)
			{
				byFailType[byIndexCount] = DARKPIXEL;

				sPointX[byIndexCount] = (short)i; //!SH _190206: x좌표 다시 +3 또는 다른 연산 값 취해주어야 한다.
				sPointY[byIndexCount] = (short)j;
				fConcentration[byIndexCount] = (float)Conc_Pixel;

				byIndexCount++;
			}

			//// 1. VERY_BRIGHTPIXEL
			//if (Conc_Pixel >= fVeryBright_Tres)
			//{
			//	byFailType[byIndexCount] = VERY_BRIGHTPIXEL;

			//	sPointX[byIndexCount] = (short)i;
			//	sPointY[byIndexCount] = (short)j;
			//	fConcentration[byIndexCount] = (float)Conc_Pixel;

			//	byIndexCount++;
			//}
			//// 2. BRIGHTPIXEL
			//else if (Conc_Pixel < fVeryBright_Tres && Conc_Pixel >= fBright_Tres)
			//{
			//	byFailType[byIndexCount] = BRIGHTPIXEL;

			//	sPointX[byIndexCount] = (short)i;
			//	sPointY[byIndexCount] = (short)j;
			//	fConcentration[byIndexCount] = (float)Conc_Pixel;

			//	byIndexCount++;
			//}
			//// 3. VERY_DARKTPIXEL
			//else if (Conc_Pixel <= fVeryDark_Tres )
			//{
			//	byFailType[byIndexCount] = VERY_DARKPIXEL;

			//	sPointX[byIndexCount] = (short)i; //!SH _190206: x좌표 다시 +3 또는 다른 연산 값 취해주어야 한다.
			//	sPointY[byIndexCount] = (short)j;
			//	fConcentration[byIndexCount] = (float)Conc_Pixel;

			//	byIndexCount++;
			//}
			//// 4. DARKTPIXEL
			//else if (Conc_Pixel > fVeryDark_Tres && Conc_Pixel <= fDark_Tres)
			//{
			//	byFailType[byIndexCount] = DARKPIXEL;

			//	sPointX[byIndexCount] = (short)i;
			//	sPointY[byIndexCount] = (short)j;
			//	fConcentration[byIndexCount] = (float)Conc_Pixel;

			//	byIndexCount++;
			//}

			//// VERY_HOTPIXEL, HOTPIXEL 구분  ::: Test Mode = Test_Hot 일때만 Hot 검사 추가함
			//// 해당 구문에서 위쪽에서 이미 VeryBrightPixel이라고 정의 되었더라도  VERY HOT, HOT 픽셀 기준에 부합하면 VERY HOT, HOT 로 Type이 변경 된다.
			//if (bTestMode == Test_Hot)
			//{
			//	Gap_of_Bright = (int)Pixel_Y - (int)dbFullIMG_Avg;

			//	if (Gap_of_Bright >= fVeryHot_Tres)
			//	{
			//		byFailType[byIndexCount] = VERY_HOTPIXEL;

			//		sPointX[byIndexCount] = (short)i;
			//		sPointY[byIndexCount] = (short)j;
			//		fConcentration[byIndexCount] = (float)Pixel_Y;

			//		byIndexCount++;
			//	}
			//	else if (Gap_of_Bright < fVeryHot_Tres && Gap_of_Bright >= fHot_Tres)
			//	{
			//		byFailType[byIndexCount] = HOTPIXEL;

			//		sPointX[byIndexCount] = (short)i;
			//		sPointY[byIndexCount] = (short)j;
			//		fConcentration[byIndexCount] = (float)Pixel_Y;

			//		byIndexCount++;
			//	}
			//}
	
			// DefectPixel_F_Max 지정 개수 초과 시 종료
			if (DefectPixel_F_Max <= byIndexCount)
			{
				printf("CTI_DefectPixel::DefectPixel_New() : Over Count __ Defect Pixel\n");

				delete[] wImageBuf;
				
				return;
			}
		}
	}


// 클러스터 구분 삭제 --- 20181108
 	// Pixel 클러스터 찾기
 	UINT X1=0, Y1=0, X2=0, Y2=0;
 	BYTE bType_1 = 0, bType_2 = 0;
 	double Dist_p2p = 0.0;
 	for (int t1 = 0; t1 < byIndexCount-1; t1++)
 	{
 		for (int t2 = t1 + 1; t2 < byIndexCount; t2++)
 		{
 			bType_1 = byFailType[t1];
 			bType_2 = byFailType[t2];
 			X1 = sPointX[t1];
 			Y1 = sPointY[t1];
 			X2 = sPointX[t2];
 			Y2 = sPointY[t2];
 
 			//if (bType_1 == bType_2)
 			{
 				Dist_p2p = sqrt((double)((X1 - X2) * (X1 - X2) + (Y1 - Y2) * (Y1 - Y2)));
 				if (Dist_p2p < 1.1)
 				{
 					byFailType[t1] = CLUSTER;
 					byFailType[t2] = CLUSTER;
 				}
 			}
 		}
 	}
	delete[] wImageBuf;

}