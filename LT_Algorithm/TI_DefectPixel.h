//#include "TI_BaseAlgorithm.h"
// *****************************************************************************
//  Filename	: 	TI_DefectPixel.h
//  Created	:	2018/2/11 - 15:47
//  Modified	:	2018/2/11 - 15:47
// 
//  Author	:	Luritech
// 	
//  Purpose	:	
// ****************************************************************************
#ifndef TI_DefectPixel_h__
#define TI_DefectPixel_h__

#pragma once

#include <windows.h>
#include "cv.h"
#include "highgui.h"
#include "Def_AI_Common.h"

//class CTI_DefectPixel : CTI_BaseAlgorithm
// =============================================================================
//  CTI_DefectPixel
// =============================================================================
class CTI_DefectPixel
{

public:
	//virtual void				Initialize(BYTE* pScanBuf, int iWidth, int iHeight);
	//ST_SO_DefectPixel_Result	Inspection(ST_SO_DefectPixel_Opt stOption);
	
	CTI_DefectPixel();

	virtual ~CTI_DefectPixel();

public:
	//__in 
	// 0. Image Data : Input 8 Bit Gray Scale Image
	// 1. BOOL bTestMode : DarkRoom의 모드 : Hot 검사: 0(조명 OFF), Dead 검사: 1(조명 ON)
	// 2. BYTE byDP_BlockSize : 영역별 밝기 평균을 산출하기위한 블럭 크기 (예 : byDP_BlockSize = 11  --- 11x11 단위 블럭에서 DP를 산출)
	// 3. float fVeryHot_Tres ~ fDark_Thres: Defect Pixel의 양불 기준, 예) fDark_Thres = -15  -> 11x11블럭 내부 평균 밝기와 해당 Pixel의 비율이 15%이하로 떨어지는 어두운 Pixel은 불량
	//__out
	// 4. BYTE& byIndexCount : 불량 개수의 총 합 개수
	// 5. WORD* sPointX : 순서대로 저장되는 불량 Pixel의 X 좌표 값
	// 6. WORD* sPointY : 순서대로 저장되는 불량 Pixel의 Y 좌표 값 
	// 7. float* fConcentration : 순서대로 저장되는 불량 Pixel의 평균 밝기 대비 불량 Pixel의 밝기 편차비율 :::: Value = (해당 픽셀 밝기 - 블럭 내부 평균) / 블럭 내부 평균 * 100;
	// 8. BYTE byFailType : 순서대로 저장되는 불량 Pixel의 Type ::: Value = 0 : Hot Pixel, 1: Dead Pixel, 2: Cluster (죽은 Pixel끼리 뭉침)
	//
	// 5 ~ 8번 예시 설명
	// sPointX[n] : n 번째로 출력된 불량의 X 좌표
	// sPointY[n] : n 번째로 출력된 불량의 Y 좌표
	// fConcentration[n] : n 번째로 출력된 불량의 결과값
	// byFailType[n] : n번째 불량의 타입
	void	DefectPixel_Test(LPBYTE pImageBuf, BOOL bTestMode, BYTE byDP_BlockSize, float fVeryHot_Tres, float fHot_Tres, float fVeryBright_Tres, float fBright_Tres, float fVeryDark_Tres, float fDark_Tres, BYTE& byIndexCount, short* sPointX, short* sPointY, float* fConcentration, BYTE* byFailType, int m_iWidth, int m_iHeight, int DefectPixel_F_Max, UINT nMargin_Left, UINT nMargin_Right);

};

#endif // TI_DefectPixel_h__