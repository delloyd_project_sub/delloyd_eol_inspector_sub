﻿#pragma once

#include <afxwin.h>

#include "cv.h"
#include "highgui.h"
#include "Def_AI_Common.h"

class CTI_CenterPoint
{
public:
	CTI_CenterPoint();
	virtual ~CTI_CenterPoint();

	CPoint 	CenterPoint_Test(CRect rtROI, UINT nMarkColor, IplImage* pImageBuf);
	double	GetDistance	(int ix1, int iy1, int ix2, int iy2);

	int		FiducialMark_Test(IN IplImage* pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN CRect rtROI, OUT POINT& ptOUT_Center);

};

