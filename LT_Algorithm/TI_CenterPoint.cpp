﻿#include "stdafx.h"
#include "TI_CenterPoint.h"

#define _USE_MATH_DEFINES
#include <math.h>

CTI_CenterPoint::CTI_CenterPoint()
{
}

CTI_CenterPoint::~CTI_CenterPoint()
{
}

//=============================================================================
// Method		: CenterPoint_Test
// Access		: public  
// Returns		: CPoint
// Parameter	: CRect rtROI
// Parameter	: UINT nMarkColor
// Parameter	: IplImage * pImageBuf
// Qualifier	:
// Last Update	: 2018/1/9 - 13:19
// Desc.		:
//=============================================================================
CPoint CTI_CenterPoint::CenterPoint_Test(CRect rtROI, UINT nMarkColor, IplImage* pImageBuf)
{
	CPoint ptCenter;

	// 초기값
	ptCenter.x = -1;
	ptCenter.y = -1;

	if (NULL == pImageBuf)
		return ptCenter;

	IplImage *RGBOriginImage = cvCreateImage(cvSize(pImageBuf->width, pImageBuf->height), IPL_DEPTH_8U, 3);
	IplImage *OriginImage = cvCreateImage(cvSize(pImageBuf->width, pImageBuf->height), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(pImageBuf->width, pImageBuf->height), IPL_DEPTH_8U, 1);

	cvSetZero(OriginImage);
	cvSetZero(DilateImage);


	cvCopy(pImageBuf, RGBOriginImage);
	// ROI Image Cut
	//cvSetImageROI(pImageBuf, cvRect(rtROI.left, rtROI.top, rtROI.Width(), rtROI.Height()));

	// 3채널 -> 1채널로 변환
	cvCvtColor(pImageBuf, OriginImage, CV_RGB2GRAY);

	// 2진화
	cvThreshold(OriginImage, DilateImage, 100, 255, CV_THRESH_BINARY);

	// 반전
	if (AI_Mark_B == nMarkColor)
	{
		cvNot(DilateImage, DilateImage);
	}

	cvErode(DilateImage, DilateImage);

	CvSeq *Contour = 0;

	//cvSaveImage("C:\\CenterPoint.bmp", DilateImage);

	CvMemStorage* CvStorage = cvCreateMemStorage(0);
	cvFindContours(DilateImage, CvStorage, &Contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double dbOldDistance = 999999;

	int iCenterX = 0;
	int iCenterY = 0;

	CvRect rtTemp;
	CvRect rtFin;

	for (; Contour != 0; Contour = Contour->h_next)
	{
		rtTemp	 = cvContourBoundingRect(Contour, 1);

		double dbCircularity = (4.0 * M_PI * cvContourArea(Contour, CV_WHOLE_SEQ)) / (cvArcLength(Contour, CV_WHOLE_SEQ, -1) * cvArcLength(Contour, CV_WHOLE_SEQ, -1));

		// 값이 작을 수록 찌그러진 것도 잡는다.
		if (dbCircularity > 0.6)
		{
			if (rtROI.left < rtTemp.x 
				&& rtROI.top < rtTemp.y
				&& rtROI.right > rtTemp.x + rtTemp.width
				&& rtROI.bottom > rtTemp.y + rtTemp.height)
			{
				iCenterX = rtTemp.x + rtTemp.width / 2;
				iCenterY = rtTemp.y + rtTemp.height / 2;

				if (rtTemp.width < pImageBuf->width / 2 && rtTemp.height < pImageBuf->height / 2 && rtTemp.width > 10 && rtTemp.height > 10)
				{
					double dbDistance = GetDistance(rtTemp.x + rtTemp.width / 2, rtTemp.y + rtTemp.height / 2, OriginImage->width / 2, OriginImage->height / 2);

					if (dbDistance < dbOldDistance)
					{
						ptCenter.x = iCenterX;
						ptCenter.y = iCenterY;
						dbOldDistance = dbDistance;
						rtFin = rtTemp;
						cvRectangle(RGBOriginImage, cvPoint(rtTemp.x, rtTemp.y), cvPoint(rtTemp.x + rtTemp.width, rtTemp.y + rtTemp.height), CV_RGB(0, 255, 0), 1, 8);
					}
				}
			}
		}
	}

	// 원본 이미지 Reset
	//cvRectangle(RGBOriginImage, cvPoint(rtFin.x, rtFin.y), cvPoint(rtFin.x + rtFin.width, rtFin.y + rtFin.height), CV_RGB(255, 255, 0), 1, 8);
	//cvResetImageROI(pImageBuf);

	cvReleaseImage(&RGBOriginImage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseMemStorage(&CvStorage);

	return ptCenter;
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: int x1
// Parameter	: int y1
// Parameter	: int x2
// Parameter	: int y2
// Qualifier	:
// Last Update	: 2017/11/2 - 18:58
// Desc.		:
//=============================================================================
double CTI_CenterPoint::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt((double)((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));

	return result;
}
//=============================================================================
// Method		: FiducialMark_Test
// Access		: public  
// Returns		: int
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN CRect rtROI
// Parameter	: IN BYTE byMarkColor
// Parameter	: IN BYTE byBrightness
// Parameter	: OUT POINT & ptOUT_Center
// Qualifier	:
// Last Update	: 2018/2/11 - 15:45
// Desc.		:
//=============================================================================
int CTI_CenterPoint::FiducialMark_Test(IN IplImage* pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN CRect rtROI, OUT POINT& ptOUT_Center)
{
	if (nullptr == pImageBuf)
		return -1;

	POINT ptCenter;

	// 초기값
	ptCenter.x = -1;
	ptCenter.y = -1;

	int ROI_Left = rtROI.left;
	int ROI_Top = rtROI.top;

	int ROI_W = rtROI.right - rtROI.left;
	int ROI_H = rtROI.bottom - rtROI.top;

	IplImage *OriginImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);
	if (pImageBuf->nChannels == 3)
		cvCvtColor(pImageBuf, OriginImage, CV_RGB2GRAY);
	else
		cvCopy(pImageBuf,OriginImage);


	int Offset_X = 1, Offset_Y = 1;



	cvAdaptiveThreshold(OriginImage, DilateImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 501, 3.0);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvRect *rectArray = new CvRect[2];

	int iCounter = 0;
	CvRect Rect;
	CvSeq *contour = 0;

	cvNot(DilateImage, DilateImage);

	cvErode(DilateImage, DilateImage);

	IplImage *RGBResultImage = cvCreateImage(cvSize(ROI_W, ROI_H), IPL_DEPTH_8U, 3);

	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double Rate;
	double old_dist = 99999.0;
	double old_dist2 = 99999.0;
	for (; contour != 0; contour = contour->h_next)
	{
		Rect = cvContourBoundingRect(contour, 1);
		Rate = (double)Rect.width / (double)Rect.height;

		if ((Rect.width > 4) && (Rect.width <= (ROI_W / 2)) && Rect.height > 4 && (Rect.height <= (ROI_H / 2)) && Rate > 0.5 && Rate < 2.5)
		{
			double distance = GetDistance(Rect.x + Rect.width / 2, Rect.y + Rect.height / 2, dwWidth / 2, dwHeight / 2);

			if (distance < old_dist || distance < old_dist2)
			{
				if (distance >= old_dist && distance < old_dist2)
				{ // 두 거리 사이의 거리가 들어오면 
					rectArray[1] = Rect;		//두번째 거리에 있는 Rect 에 입력
					old_dist2 = distance;
				}
				else // 최소 거리가 들어오면 
				{
					rectArray[1] = rectArray[0];// 가지고 있던 Rect 저장 ( 두번째로 가까운 Rect )
					rectArray[0] = Rect;		// 새로운 Rect 저장		 ( 제일 가까운 Rect )
					old_dist2 = old_dist;
					old_dist = distance;
				}

				iCounter++;
			}
		}
	}

	if (iCounter < 2)
	{
		ptCenter.x = -1;
		ptCenter.y = -1;

		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&OriginImage);
		delete[]rectArray;

		ptOUT_Center = ptCenter;

		return 1;
	}

	ptCenter.x = (int)((double)(rectArray[0].x + (rectArray[0].x + rectArray[0].width) + rectArray[1].x + (rectArray[1].x + rectArray[1].width)) / 4.0);
	ptCenter.y = (int)((double)(rectArray[0].y + (rectArray[0].y + rectArray[0].height) + rectArray[1].y + (rectArray[1].y + rectArray[1].height)) / 4.0);

// 	ptCenter.x += rtROI.left;
// 	ptCenter.y += rtROI.top;

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&OriginImage);

	delete[]rectArray;

	ptOUT_Center = ptCenter;

	return 1;
}