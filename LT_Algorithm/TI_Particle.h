﻿// *****************************************************************************
//  Filename	: 	TI_Particle.h
//  Created	:	2018/2/11 - 15:47
//  Modified	:	2018/2/11 - 15:47
// 
//  Author	:	Luritech
// 	
//  Purpose	:	
// ****************************************************************************
#ifndef TI_Particle_h__
#define TI_Particle_h__

#pragma once

#include <windows.h>
#include "cv.h"
#include "highgui.h"
#include "Def_AI_Common.h"

// -----------------------------------------------------------------------------
//  Particle
// -----------------------------------------------------------------------------

#define		MAX_Particle_ResultROI_Count		2000	// Max는 60개 고정



typedef enum enParticle_ROI
{
	Particle_ROI_Image = 0,
	Particle_ROI_Side,
	Particle_ROI_Center,
	Particle_ROI_Max,
};

typedef enum enParticle__Al_Type
{
	Pr_Type_Stain = 0,
	Pr_Type_Max,
};

typedef enum enPaticleRegion //HTH
{
	ROI_Pr_Edge = 0,
	ROI_Pr_VerEdge,
	ROI_Pr_HorEdge,
	ROI_Pr_Center,
	ROI_Pr_Max,
};

typedef struct _tag_CAN_Particle_Opt
{
	ST_ROI			stROI[ROI_Pr_Max];
	//bool			bEllipse[ROI_Pr_Max];				// 영역 모양(사각 or 원)

	int				iEgdeW;
	int				iEgdeH;

	float			fThreConcentration[ROI_Pr_Max];
	float			fThreSize[ROI_Pr_Max];

	float			fSensitivity;								// 민감도

	_tag_CAN_Particle_Opt()
	{
		fSensitivity = 0.0f;
		iEgdeW = 0;
		iEgdeH = 0;

		for (int i = 0; i < ROI_Pr_Max; i++)
		{
			//bEllipse[i] = true;
			fThreConcentration[i] = 0.0f;
			fThreSize[i] = 0.0f;
		}
	};

}ST_CAN_Particle_Opt;

typedef struct _tag_CAN_Particle_Result
{
	WORD			wResultCode;

	BYTE			byRoiCnt;
	CRect			stROI[MAX_Particle_ResultROI_Count];			// 발견한 오브젝트
	BYTE			byType[MAX_Particle_ResultROI_Count];			// 발견한 오브젝트의 타입(Stain)
	float			fConcentration[MAX_Particle_ResultROI_Count];	// 농도

	_tag_CAN_Particle_Result()
	{
		byRoiCnt = 0;

		for (UINT nIdx = 0; nIdx < MAX_Particle_ResultROI_Count; nIdx++)
		{
			byType[nIdx] = Pr_Type_Max;
			fConcentration[nIdx] = 0.0f;
		}
	};

}ST_CAN_Particle_Result;

// =============================================================================
//  CTI_Particle
// =============================================================================
class CTI_Particle
{
public:
	CTI_Particle();
	virtual ~CTI_Particle();

protected:

	UINT	m_nMax_ParticleCount	= 20;	//#define MAX_PARTICLE_COUNT	20


	//int			m_iCounter;
	//CvRect*		m_pContour;
	//char**		m_Area;	//char	m_Area[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];

	void	AssignMem_Area		(IN DWORD dwSizeX, IN DWORD dwSizeY);
	void	ReleaseMem_Area		();


	double	GetDistance			(int x1, int y1, int x2, int y2);
	double	EllipseDistanceSum	(int XC, int YC, double A, double B, int X1, int Y1);

public:

	//=============================================================================
	// Method		: SetTestArea
	// Access		: public  
	// Returns		: void
	// Parameter	: IN UINT dwWidth
	// Parameter	: IN UINT dwHeight
	// Parameter	: IN ST_ROI stROI_Side
	// Parameter	: IN ST_ROI stROI_Middle
	// Parameter	: IN ST_ROI stROI_Center
	// Parameter	: IN bool bEllipse_Side
	// Parameter	: IN bool bEllipse_Middle
	// Parameter	: IN bool bEllipse_Center
	// Parameter	: OUT char * * ppchOUT_Area
	// Qualifier	:
	// Last Update	: 2018/2/12 - 11:44
	// Desc.		:
	//=============================================================================

	//void	SetTestArea(IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI_Side, IN ST_ROI stROI_Middle, IN ST_ROI stROI_Center, IN bool bEllipse_Side, IN bool bEllipse_Middle, IN bool bEllipse_Center, OUT char** ppchOUT_Area);
	void	SetTestArea(IN UINT dwWidth, IN UINT dwHeight, IN UINT iROIWidth, IN UINT iROIheight, OUT char** ppchOUT_Area);

	void	Lump_Detection(IN LPBYTE pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN float fSensitivity, IN float* fThreSize, OUT int& iOUT_Counter, OUT char** ppchOUT_Area, OUT UINT* Particle_Region);


	void	ParticleCluster(IN int nParticleCount, IN ST_ROI* pROI, IN float* pConcentration, OUT bool* pbOUT_Judge, OUT LPBYTE lpbyOUT_ParticleType);
	//void	ParticleCluster	(ST_ROI* pROI, float* pConcentration, int nParticleCount, bool* pPassIndex, ST_CAN_Particle_Result &ParticleResult);	


	void	Particle_Test(IN LPBYTE pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_CAN_Particle_Opt ParticleOpt, OUT ST_CAN_Particle_Result &ParticleResult);

	//전역으로 씀
	CRect m_rtParticle[MAX_Particle_ResultROI_Count];

};

#endif // TI_Particle_h__