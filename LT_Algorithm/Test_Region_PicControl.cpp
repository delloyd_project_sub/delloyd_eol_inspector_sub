﻿// Test_Region_PicControl.cpp : 구현 파일입니다.
//

#include "stdafx.h"
//#include "Project_CameraTest.h"
//#include "Project_CameraTestDlg.h"
#include "Test_Region_PicControl.h"


// CTest_Region_PicControl

IMPLEMENT_DYNAMIC(CTest_Region_PicControl, CWnd)

CTest_Region_PicControl::CTest_Region_PicControl()
{
	m_pstParticle		= NULL;
	m_pstResolution		= NULL;
	m_pstCenterPoint	= NULL;
	m_pstRotate			= NULL;
}

CTest_Region_PicControl::~CTest_Region_PicControl()
{
}

BEGIN_MESSAGE_MAP(CTest_Region_PicControl, CWnd)
END_MESSAGE_MAP()

// CTest_Region_PicControl 메시지 처리기입니다.

void CTest_Region_PicControl::SetPtr_Particle(ST_Particle *pstParticle)
{
	m_pstParticle = pstParticle;
}

void CTest_Region_PicControl::SetPtr_Resolution(ST_Resolution *pstResolution)
{
	m_pstResolution = pstResolution;
}

void CTest_Region_PicControl::SetPtr_CameraOp(ST_CameraOp * pstCameraOp)
{
	m_pstCameraOpData	= pstCameraOp;
	m_pstResolution		= &pstCameraOp->st_ResolutionData;
	m_pstRotate			= &pstCameraOp->st_RotateData;
	m_pstCenterPoint	= &pstCameraOp->st_CenterData;
	m_pstParticle		= &pstCameraOp->st_ParticleData;
}

void CTest_Region_PicControl::ParticlePic(CDC *cdc)
{
	for (int t = 0; t < Region_MaxEnum; t++)
	{
		ParticlePic(cdc, t);
	}

	for (int t = 0; t < m_pstParticle->Result_cnt; t++)
	{
		ParticleErrPic(cdc, t);
	}
}

void CTest_Region_PicControl::ParticlePic(CDC *cdc, int NUM)
{
	if (m_pstParticle == NULL)
	{
		return;
	}
	//if(EIJARect.Chkdata() == FALSE){return 0;}//재대로 된 데이터가 아니면 바로 빠져나간다.

	CPen	my_Pan, *old_pan;
	CString TEXTDATA;
	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	if (NUM == 0)
	{
		my_Pan.CreatePen(PS_SOLID, 2, WHITE_COLOR);
	}
	else if (NUM == 1)
	{
		my_Pan.CreatePen(PS_SOLID, 2, GREEN_COLOR);
	}
	else
	{
		my_Pan.CreatePen(PS_SOLID, 2, YELLOW_COLOR);
	}

	if (m_pstParticle->rectData[NUM].bEllipse == TRUE)
	{
		old_pan = cdc->SelectObject(&my_Pan);

		CBrush oldBrush;
		oldBrush.CreateStockObject(NULL_BRUSH);
		CBrush *poldBrush = cdc->SelectObject(&oldBrush);
		CRect Data = m_pstParticle->rectData[NUM].RegionList;
		cdc->Ellipse(Data.left, Data.top, Data.right, Data.bottom);
		cdc->SelectObject(old_pan);
		cdc->SelectObject(oldBrush);

		my_Pan.DeleteObject();
		oldBrush.DeleteObject();
	}
	else
	{
		old_pan = cdc->SelectObject(&my_Pan);
		CRect Data = m_pstParticle->rectData[NUM].RegionList;

		cdc->MoveTo(Data.left, Data.top);
		cdc->LineTo(Data.right, Data.top);
		cdc->LineTo(Data.right, Data.bottom);
		cdc->LineTo(Data.left, Data.bottom);
		cdc->LineTo(Data.left, Data.top);
		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();
	}

}

void CTest_Region_PicControl::ParticleErrPic(CDC *cdc, int NUM)
{
	CPen	my_Pan, *old_pan;
	CString TEXTDATA;
	::SetBkMode(cdc->m_hDC, TRANSPARENT);
	if (m_pstParticle->bFailCheck == TRUE)
	{
		if (m_pstParticle->bResult == FALSE)
		{
			my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->SetTextColor(WHITE_COLOR);

			cdc->MoveTo(m_pstParticle->ErrRegionList[NUM].RegionList.left, m_pstParticle->ErrRegionList[NUM].RegionList.top);
			cdc->LineTo(m_pstParticle->ErrRegionList[NUM].RegionList.right, m_pstParticle->ErrRegionList[NUM].RegionList.top);
			cdc->LineTo(m_pstParticle->ErrRegionList[NUM].RegionList.right, m_pstParticle->ErrRegionList[NUM].RegionList.bottom);
			cdc->LineTo(m_pstParticle->ErrRegionList[NUM].RegionList.left, m_pstParticle->ErrRegionList[NUM].RegionList.bottom);
			cdc->LineTo(m_pstParticle->ErrRegionList[NUM].RegionList.left, m_pstParticle->ErrRegionList[NUM].RegionList.top);
			
			cdc->SelectObject(old_pan);
			my_Pan.DeleteObject();


			CFont font, *old_font;
			CString TxtData;

			font.CreatePointFont(150, _T("Arial"));
			old_font = cdc->SelectObject(&font);

			TEXTDATA.Format(_T("%6.2f%"), m_pstParticle->ErrRegionList[NUM].Y_result);
			cdc->TextOut(m_pstParticle->ErrRegionList[NUM].RegionList.left, m_pstParticle->ErrRegionList[NUM].RegionList.top-20, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

			cdc->SelectObject(old_font);
			font.DeleteObject();


		}
	}

}

void CTest_Region_PicControl::DrawSFRGraph(CDC *cdc, int NUM)
{
//	if ((NUM < 2) && (NUM >5))
//	{
//		return;
//	}

	if (m_pstResolution->bGViewMode == FALSE){
		return;
	}

	CString TEXTDATA;
	CPen my_pen, *old_pan, bk_pen, *old_bk_pen;

	ST_RegionResolution RectData = m_pstResolution->rectData[NUM];
	
	if (m_pstResolution->rectData[NUM].bResult)
	{
		bk_pen.CreatePen(PS_SOLID, 2, BLUE_COLOR);
		old_bk_pen = cdc->SelectObject(&bk_pen);
		cdc->SetTextColor(BLUE_COLOR);
	}
	else
	{
		bk_pen.CreatePen(PS_SOLID, 2, RED_COLOR);
		old_bk_pen = cdc->SelectObject(&bk_pen);
		cdc->SetTextColor(RED_COLOR);
	}

	if (NUM == 2)
	{
		cdc->Rectangle(RectData.RegionList.left, RectData.RegionList.top - 10, RectData.RegionList.right, RectData.RegionList.top - 110);

		for (int i = 0; i < RectData.SFR_Width - 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.left + i, RectData.RegionList.top - 10 - (int)RectData.SFR_MTF_Value[i]);
			cdc->LineTo(RectData.RegionList.left + (i + 1), RectData.RegionList.top - 10 - (int)RectData.SFR_MTF_Value[i + 1]);
		}
		cdc->SelectObject(old_bk_pen);
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < RectData.SFR_Width - 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.left + i, RectData.RegionList.top - 10 - (int)RectData.SFR_MTF_LineFitting_Value[i]);
			cdc->LineTo(RectData.RegionList.left + (i + 1), RectData.RegionList.top - 10 - (int)RectData.SFR_MTF_LineFitting_Value[i + 1]);
		}
		cdc->SelectObject(old_pan);
		my_pen.DeleteObject();
	}
	else if (NUM == 3)
	{
		cdc->Rectangle(RectData.RegionList.right, RectData.RegionList.bottom + 10, RectData.RegionList.left, RectData.RegionList.bottom + 110);

		for (int i = RectData.SFR_Width - 1; i > 0; i--)
		{
			cdc->MoveTo(RectData.RegionList.left + i, RectData.RegionList.bottom + 10 + (int)RectData.SFR_MTF_Value[i]);
			cdc->LineTo(RectData.RegionList.left + (i - 1), RectData.RegionList.bottom + 10 + (int)RectData.SFR_MTF_Value[i - 1]);
		}

		cdc->SelectObject(old_bk_pen);
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = RectData.SFR_Width - 1; i > 0; i--)
		{
			cdc->MoveTo(RectData.RegionList.left + i, RectData.RegionList.bottom + 10 + (int)RectData.SFR_MTF_LineFitting_Value[i]);
			cdc->LineTo(RectData.RegionList.left + (i - 1), RectData.RegionList.bottom + 10 + (int)RectData.SFR_MTF_LineFitting_Value[i - 1]);
		}

		cdc->SelectObject(old_pan);
		my_pen.DeleteObject();
	}
	else if (NUM == 4)
	{
		cdc->Rectangle(RectData.RegionList.right + 10, RectData.RegionList.top, RectData.RegionList.right + 110, RectData.RegionList.bottom);

		for (int i = 0; i < RectData.SFR_Height - 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.right + 10 + (int)RectData.SFR_MTF_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.right + 10 + (int)RectData.SFR_MTF_Value[i + 1], RectData.RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < RectData.SFR_Height - 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.right + 10 + (int)RectData.SFR_MTF_LineFitting_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.right + 10 + (int)RectData.SFR_MTF_LineFitting_Value[i + 1], RectData.RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_pan);
		my_pen.DeleteObject();
	}
	else if (NUM == 5)
	{
		cdc->Rectangle(RectData.RegionList.left - 10, RectData.RegionList.top, RectData.RegionList.left  - 110, RectData.RegionList.bottom);

		for (int i = 0; i < RectData.SFR_Height- 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.left - 10 - (int)RectData.SFR_MTF_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.left - 10 - (int)RectData.SFR_MTF_Value[i + 1], RectData.RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < RectData.SFR_Height- 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.left  - 10 - (int)RectData.SFR_MTF_LineFitting_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.left  - 10 - (int)RectData.SFR_MTF_LineFitting_Value[i + 1], RectData.RegionList.top + (i + 1));
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();
	}
	else if (NUM == 7)
	{
		cdc->Rectangle(RectData.RegionList.left - 10, RectData.RegionList.top, RectData.RegionList.left - 110, RectData.RegionList.bottom);

		for (int i = 0; i < RectData.SFR_Height - 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.left - 10 - (int)RectData.SFR_MTF_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.left - 10 - (int)RectData.SFR_MTF_Value[i + 1], RectData.RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < RectData.SFR_Height - 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.left - 10 - (int)RectData.SFR_MTF_LineFitting_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.left - 10 - (int)RectData.SFR_MTF_LineFitting_Value[i + 1], RectData.RegionList.top + (i + 1));
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();
	}
	else if (NUM == 11)
	{
		cdc->Rectangle(RectData.RegionList.right + 10, RectData.RegionList.top, RectData.RegionList.right + 110, RectData.RegionList.bottom);

		for (int i = 0; i < RectData.SFR_Height - 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.right + 10 + (int)RectData.SFR_MTF_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.right + 10 + (int)RectData.SFR_MTF_Value[i + 1], RectData.RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < RectData.SFR_Height - 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.right + 10 + (int)RectData.SFR_MTF_LineFitting_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.right + 10 + (int)RectData.SFR_MTF_LineFitting_Value[i + 1], RectData.RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_pan);
		my_pen.DeleteObject();
	}
	else if (NUM == 13)
	{
		cdc->Rectangle(RectData.RegionList.left  - 10, RectData.RegionList.top, RectData.RegionList.left  - 110, RectData.RegionList.bottom);

		for (int i = 0; i < RectData.SFR_Height- 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.left  - 10 - (int)RectData.SFR_MTF_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.left  - 10 - (int)RectData.SFR_MTF_Value[i + 1], RectData.RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < RectData.SFR_Height- 1; i++)
		{
			cdc->MoveTo(RectData.RegionList.left  - 10 - (int)RectData.SFR_MTF_LineFitting_Value[i], RectData.RegionList.top + i);
			cdc->LineTo(RectData.RegionList.left  - 10 - (int)RectData.SFR_MTF_LineFitting_Value[i + 1], RectData.RegionList.top + (i + 1));
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();
	}
	else
	{
		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();
	}
}

void CTest_Region_PicControl::ResolutionPic(CDC *cdc)
{
	for (int t = 0; t < Region_Resol_MaxEnum; t++)
	{
		ResolutionPic(cdc, t);
	}
}

/*해상력 Pic*/
void CTest_Region_PicControl::ResolutionPic(CDC *cdc, int NUM)
{
	if (m_pstResolution->rectData[NUM].bUse == FALSE)///사용하지 않은 roi는 그리지 않음.
	{
		return;
	}

	CPen	my_Pan, *old_pan;
	CString TEXTDATA;
	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	ST_RegionResolution RectData = m_pstResolution->rectData[NUM];

	/*Roi 영역 이름 표시*/
	/*단 Reference B/ W 제외*/


	if (NUM > 1)
	{
		cdc->SetTextColor(GREEN_COLOR);
		CString RectName;
		RectName.Format(_T("%s"), g_szResolutionRegion[NUM]);
		cdc->TextOut(RectData.RegionList.left, RectData.RegionList.top, RectName.GetBuffer(0), RectName.GetLength());

		if (RectData.bResult == TRUE)
		{
			my_Pan.CreatePen(PS_SOLID, 2, BLUE_COLOR);
			cdc->SetTextColor(BLUE_COLOR);
		}
		else
		{
			my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
			cdc->SetTextColor(RED_COLOR);
		}
	}
	else
	{
		my_Pan.CreatePen(PS_SOLID, 2, GRAY_DK_COLOR);
		cdc->SetTextColor(GRAY_DK_COLOR);
	}

	/*Roi 영역 틀*/
	old_pan = cdc->SelectObject(&my_Pan);
	cdc->MoveTo(RectData.RegionList.left, RectData.RegionList.top);
	cdc->LineTo(RectData.RegionList.right, RectData.RegionList.top);
	cdc->LineTo(RectData.RegionList.right, RectData.RegionList.bottom);
	cdc->LineTo(RectData.RegionList.left, RectData.RegionList.bottom);
	cdc->LineTo(RectData.RegionList.left, RectData.RegionList.top);
	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	if (NUM < 2)
	{
		return;
	}

	/*TEST 시*/
	int iMode = RectData.i_Mode;
	if (iMode == Resol_Mode_Bottom_Top || iMode == Resol_Mode_Top_Bottom)
	{
		int m_ConstValue = RectData.RegionList.top + RectData.CheckLine;
		int m_ResultValue = RectData.RegionList.top;
		my_Pan.CreatePen(PS_SOLID, 2, YELLOW_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->MoveTo(RectData.RegionList.left, m_ConstValue);//고정좌표
		cdc->LineTo(RectData.RegionList.right, m_ConstValue);
		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		my_Pan.CreatePen(PS_SOLID, 2, GREEN_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		int offset1, offset2;
		offset1 = RectData.RegionList.Height() - RectData.PatternStartPos.y;
		offset2 = RectData.PatternEndPos.y;
		cdc->MoveTo(RectData.RegionList.left, RectData.RegionList.bottom - offset1);
		cdc->LineTo(RectData.RegionList.right, RectData.RegionList.bottom - offset1);
		cdc->MoveTo(RectData.RegionList.left, RectData.RegionList.top + offset2);
		cdc->LineTo(RectData.RegionList.right, RectData.RegionList.top + offset2);
		int ThresholdPosition = GetStandardData(NUM);
		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		int ThresholdValue, PeakThresholdValue;
		if (iMode == Resol_Mode_Bottom_Top)
		{
			ThresholdValue = RectData.RegionList.bottom - offset1 - ThresholdPosition;
			PeakThresholdValue = RectData.RegionList.bottom - offset1 - GetStandardPeakData(NUM);
		}

		if (iMode == Resol_Mode_Top_Bottom)
		{
			ThresholdValue = RectData.RegionList.top + RectData.PatternStartPos.y + ThresholdPosition;
			PeakThresholdValue = RectData.RegionList.top + RectData.PatternStartPos.y + GetStandardPeakData(NUM);
		}

		my_Pan.CreatePen(PS_SOLID, 2, PINK_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);

		if (abs(RectData.RegionList.left - RectData.RegionList.right) != 0)
		{
			cdc->MoveTo(RectData.RegionList.left, ThresholdValue);//기준점
			cdc->LineTo(RectData.RegionList.right, ThresholdValue);
		}

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		my_Pan.CreatePen(PS_SOLID, 2, RGB(200, 0, 200));
		old_pan = cdc->SelectObject(&my_Pan);
		if (abs(RectData.RegionList.left - RectData.RegionList.right) != 0)
		{
			cdc->MoveTo(RectData.RegionList.left, PeakThresholdValue);//기준점
			cdc->LineTo(RectData.RegionList.right, PeakThresholdValue);
		}

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		/*Data 값 출력*/
		if (RectData.bResult == TRUE)
		{
			my_Pan.CreatePen(PS_SOLID, 2, BLUE_COLOR);
			cdc->SetTextColor(BLUE_COLOR);
		}
		else
		{
			my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
			cdc->SetTextColor(RED_COLOR);
		}

		old_pan = cdc->SelectObject(&my_Pan);

		if (m_pstResolution->b190Mode == TRUE && NUM > 5)
		{
			int resultX = (RectData.RegionList.left + RectData.RegionList.right) / 2;
			int resultY = (RectData.RegionList.top + RectData.RegionList.bottom) / 2;
			int W = CAM_IMAGE_WIDTH / 2;
			int H = CAM_IMAGE_HEIGHT / 2;
			double standard_dist = GetDistance(W, H, CAM_IMAGE_WIDTH, 0);
			double dist = GetDistance(W, H, resultX, resultY);
			TEXTDATA.Format(_T("(%6.2f)F"), dist / standard_dist);
			cdc->TextOut(RectData.RegionList.left - 30, m_ResultValue + 30, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
		}

		TEXTDATA.Format(_T("%d"), RectData.Result_data);
		cdc->TextOut(RectData.RegionList.left - 30, m_ResultValue, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();


		DrawSFRGraph(cdc, NUM);
	}
	else if (iMode == Resol_Mode_Right_Left || iMode == Resol_Mode_Left_Right)
	{
		int m_ConstValue = RectData.RegionList.left + RectData.CheckLine;
		int m_ResultValue = RectData.RegionList.left;

		my_Pan.CreatePen(PS_SOLID, 2, YELLOW_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->MoveTo(m_ConstValue, RectData.RegionList.top);//고정좌표
		cdc->LineTo(m_ConstValue, RectData.RegionList.bottom);
		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		my_Pan.CreatePen(PS_SOLID, 2, GREEN_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		int offset1, offset2;
		offset1 = RectData.RegionList.Width() - RectData.PatternEndPos.x;
		offset2 = RectData.PatternStartPos.x;
		cdc->MoveTo(RectData.RegionList.left + offset2, RectData.RegionList.top);
		cdc->LineTo(RectData.RegionList.left + offset2, RectData.RegionList.bottom);
		cdc->MoveTo(RectData.RegionList.right - offset1, RectData.RegionList.top);
		cdc->LineTo(RectData.RegionList.right - offset1, RectData.RegionList.bottom);
		int ThresholdPosition = GetStandardData(NUM);
		int PeakThresholdValue = GetStandardPeakData(NUM);
		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		int ThresholdValue, ThresholdValue_Peak;
		if (iMode == Resol_Mode_Left_Right)
		{
			ThresholdValue = RectData.RegionList.left + (RectData.PatternStartPos.x) + ThresholdPosition;
			ThresholdValue_Peak = RectData.RegionList.left + (RectData.PatternStartPos.x) + PeakThresholdValue;
		}

		if (iMode == Resol_Mode_Right_Left)
		{
			ThresholdValue = RectData.RegionList.right - (RectData.RegionList.Width() - RectData.PatternStartPos.x) - ThresholdPosition;
			ThresholdValue_Peak = RectData.RegionList.right - (RectData.RegionList.Width() - RectData.PatternStartPos.x) - PeakThresholdValue;
		}

		my_Pan.CreatePen(PS_SOLID, 2, PINK_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);

		if (abs(RectData.RegionList.top - RectData.RegionList.bottom) != 0)
		{
			cdc->MoveTo(ThresholdValue, RectData.RegionList.top);//기준점
			cdc->LineTo(ThresholdValue, RectData.RegionList.bottom);
		}

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		my_Pan.CreatePen(PS_SOLID, 2, RGB(200, 0, 200));
		old_pan = cdc->SelectObject(&my_Pan);

		if (abs(RectData.RegionList.top - RectData.RegionList.bottom) != 0)
		{
			cdc->MoveTo(ThresholdValue_Peak, RectData.RegionList.top);//기준점
			cdc->LineTo(ThresholdValue_Peak, RectData.RegionList.bottom);
		}

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		/*Data 값 출력*/
		if (RectData.bResult == TRUE)
		{
			my_Pan.CreatePen(PS_SOLID, 2, BLUE_COLOR);
			cdc->SetTextColor(BLUE_COLOR);
		}
		else
		{
			my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
			cdc->SetTextColor(RED_COLOR);
		}
		old_pan = cdc->SelectObject(&my_Pan);

		if (m_pstResolution->b190Mode == TRUE && NUM > 5)
		{
			int  resultX = (RectData.RegionList.left + RectData.RegionList.right) / 2;
			int  resultY = (RectData.RegionList.top + RectData.RegionList.bottom) / 2;

			double standard_dist = GetDistance(CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT / 2, CAM_IMAGE_WIDTH, 0);
			double dist = GetDistance(CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT / 2, resultX, resultY);

			//	if( fabs((dist/standard_dist) - 0.7) < 0.01)
			//		TEXTDATA.Format("0.70F");
			//	else
			TEXTDATA.Format(_T("(%6.2f)F"), dist / standard_dist);

			cdc->TextOut(m_ResultValue, RectData.RegionList.top - 50, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
		}

		TEXTDATA.Format(_T("%d"), RectData.Result_data);
		cdc->TextOut(m_ResultValue, RectData.RegionList.top - 20, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		DrawSFRGraph(cdc, NUM);
	}


	CFont font, *old_font;
	CString TxtData;

	font.CreatePointFont(200, _T("Arial"));
	old_font = cdc->SelectObject(&font);

	TEXTDATA.Format(_T("%d"), m_pstResolution->nFocusGuage);
	cdc->TextOut(100, 100, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

	cdc->SelectObject(old_font);
	font.DeleteObject();




}

double CTest_Region_PicControl::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt((double)((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));

	return result;
}

int CTest_Region_PicControl::GetStandardData(int NUM)
{
	double x = 0, y = 0;
	ST_RegionResolution RectData = m_pstResolution->rectData[NUM];

	x = (RectData.i_Threshold_Min - RectData.i_Range_Min);

	y = x / (RectData.i_Range_Max- RectData.i_Range_Min);

	double sx1, sy1, sx2, sy2;
	sx1 = RectData.PatternStartPos.x;
	sy1 = RectData.PatternStartPos.y;
	sx2 = RectData.PatternEndPos.x;
	sy2 = RectData.PatternEndPos.y;

	double dist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int RD_OFFSET = (int)(dist * y);

	return RD_OFFSET;
}

int CTest_Region_PicControl::GetStandardPeakData(int NUM)
{
	double x = 0, y = 0;

	ST_RegionResolution RectData = m_pstResolution->rectData[NUM];
	x = (RectData.i_Threshold_Max - RectData.i_Range_Min);

	y = x / (RectData.i_Range_Max - RectData.i_Range_Min);

	double sx1, sy1, sx2, sy2;
	sx1 = RectData.PatternStartPos.x;
	sy1 = RectData.PatternStartPos.y;
	sx2 = RectData.PatternEndPos.x;
	sy2 = RectData.PatternEndPos.y;


	double dist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int RD_OFFSET = (int)(dist * y);

	return RD_OFFSET;
}

void CTest_Region_PicControl::SetPtr_CenterPoint(ST_CenterPoint *pstCenterPoint)
{
	m_pstCenterPoint = pstCenterPoint;
}

void CTest_Region_PicControl::CenterPointPic(CDC *cdc)
{
	if (m_pstCenterPoint == NULL)
	{
		return;
	}

	int x = m_pstCenterPoint->Result_RealPos_X;
	int y = m_pstCenterPoint->Result_RealPos_Y;

	/* 광축 그리기 */
	CPen my_Pan, *old_pan;

	::SetBkMode(cdc->m_hDC, TRANSPARENT);
	my_Pan.CreatePen(PS_SOLID, 2, RGB(255, 255, 0));

	old_pan = cdc->SelectObject(&my_Pan);
	cdc->MoveTo(x - 10, y);
	cdc->LineTo(x + 10, y);
	cdc->MoveTo(x, y - 10);
	cdc->LineTo(x, y + 10);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	/* 광축 값 표시 */
	CFont font, *old_font;
	CString TxtData;

	font.CreatePointFont(200, _T("Arial"));
	old_font = cdc->SelectObject(&font);

	if (m_pstCenterPoint->bResult == FALSE){
		cdc->SetTextColor(RGB(0, 255, 0));
	}else{
		cdc->SetTextColor(RGB(0, 0, 255));
	}
	if (m_pstCenterPoint->Result_Pos_X > CAM_IMAGE_WIDTH || m_pstCenterPoint->Result_Pos_Y > CAM_IMAGE_HEIGHT)
	{
		TxtData.Format(_T("광축측정실패"));
	}
	else{
		if (m_pstCenterPoint->bToken_State == TRUE)
		{
			TxtData.Format(_T("X:%d Y:%d"), m_pstCenterPoint->Result_Pos_X, m_pstCenterPoint->Result_Pos_Y);
		}
		else
		{
			TxtData.Format(_T("Offset X:%d Offset Y:%d"), m_pstCenterPoint->Result_Offset_X, m_pstCenterPoint->Result_Offset_Y);
		}
	}
	

	cdc->TextOut(20, CAM_IMAGE_HEIGHT-50, TxtData.GetBuffer(0), TxtData.GetLength());

	cdc->SelectObject(old_font);
	font.DeleteObject();
}

void CTest_Region_PicControl::SetPtr_Rotate(ST_Rotate *pstRotate)
{
	m_pstRotate = pstRotate;
}

void CTest_Region_PicControl::RotatePic(CDC *cdc, int NUM)
{
	if (m_pstRotate == NULL)
	{
		return;
	}

	/*ROI 그리기*/
	CPen my_Pan, *old_pan;

	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	int left = m_pstRotate->rectData[NUM].RegionList.left;
	int top = m_pstRotate->rectData[NUM].RegionList.top;
	int right = m_pstRotate->rectData[NUM].RegionList.right;
	int bottom = m_pstRotate->rectData[NUM].RegionList.bottom;

	if (m_pstRotate->bDisplayROI == TRUE)
	{
		my_Pan.CreatePen(PS_SOLID, 2, RGB(200, 200, 200));
		old_pan = cdc->SelectObject(&my_Pan);

		cdc->MoveTo(left, top);
		cdc->LineTo(right, top);
		cdc->LineTo(right, bottom);
		cdc->LineTo(left, bottom);
		cdc->LineTo(left, top);

		//cdc->Rectangle(left, top, right, bottom);

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();
	}

	CFont font, *old_font;
	CString TxtData;

	font.CreatePointFont(200, _T("Arial"));
	old_font = cdc->SelectObject(&font);

	if (m_pstRotate->bResult == FALSE){
		cdc->SetTextColor(RGB(0, 255, 0));
	}
	else{
		cdc->SetTextColor(RGB(0, 0, 255));
	}

	if (m_pstRotate->dbResult_Degree != 90)
	{
		TxtData.Format(_T("%6.2f"), m_pstRotate->dbResult_Degree);
	}
	else{
		TxtData.Format(_T("각도측정실패"));

	}
	
	cdc->TextOut(CAM_IMAGE_WIDTH-200, CAM_IMAGE_HEIGHT - 50, TxtData.GetBuffer(0), TxtData.GetLength());

	cdc->SelectObject(old_font);
	font.DeleteObject();
	


	/*로테이트 각도 그리기*/
	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 127, 0));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(m_pstRotate->rectData[RegionRotate_LeftTop].RegionList.CenterPoint().x,
		m_pstRotate->rectData[RegionRotate_LeftTop].RegionList.CenterPoint().y);

	cdc->LineTo(m_pstRotate->rectData[RegionRotate_LeftBottom].RegionList.CenterPoint().x,
		m_pstRotate->rectData[RegionRotate_LeftBottom].RegionList.CenterPoint().y);

	cdc->LineTo(m_pstRotate->rectData[RegionRotate_RightBottom].RegionList.CenterPoint().x,
		m_pstRotate->rectData[RegionRotate_RightBottom].RegionList.CenterPoint().y);

	cdc->LineTo(m_pstRotate->rectData[RegionRotate_RightTop].RegionList.CenterPoint().x,
		m_pstRotate->rectData[RegionRotate_RightTop].RegionList.CenterPoint().y);

	cdc->LineTo(m_pstRotate->rectData[RegionRotate_LeftTop].RegionList.CenterPoint().x,
		m_pstRotate->rectData[RegionRotate_LeftTop].RegionList.CenterPoint().y);
	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
}

void CTest_Region_PicControl::RotatePic(CDC *cdc)
{
	for (int t = 0; t < RegionRotate_MaxEnum; t++)
	{
		RotatePic(cdc, t);
	}
}