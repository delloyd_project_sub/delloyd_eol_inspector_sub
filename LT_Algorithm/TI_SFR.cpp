﻿#include "stdafx.h"
#include "TI_SFR.h"

using namespace cv;

CTI_SFR::CTI_SFR()
{
}

CTI_SFR::~CTI_SFR()
{
}

//=============================================================================
// Method		: SFR_Test
// Access		: public  
// Returns		: double
// Parameter	: CRect rtROI
// Parameter	: double dbPixelSize
// Parameter	: double dbLinPair
// Parameter	: IplImage * pImageBuf
// Qualifier	:
// Last Update	: 2018/1/9 - 16:49
// Desc.		:
//=============================================================================
double CTI_SFR::SFR_Test(double dbPixelSize, double dbLinPair, IplImage* pImageBuf, UINT nResultType)
{
	double dbValue = 0.0;

	if (NULL == pImageBuf)
		return dbValue;

	BYTE *pImage = new BYTE[pImageBuf->height * pImageBuf->width];
	
	for (int y = 0; y < pImageBuf->height; y++)
	{
		for (int x = 0; x < pImageBuf->width; x++)
		{
			pImage[y * (pImageBuf->width) + x] = pImageBuf->imageData[y * pImageBuf->widthStep + x + 0];
		}
	}

	// SFR 데이터 추출
	dbValue = GetSFRValue(pImage, pImageBuf->width, pImageBuf->height, dbPixelSize, dbLinPair, nResultType);

	delete[] pImage;

	return dbValue;
}

//=============================================================================
// Method		: GetSFRValue
// Access		: protected  
// Returns		: double
// Parameter	: BYTE * pBuf
// Parameter	: int iWidth
// Parameter	: int iHeight
// Parameter	: double dbPixelSize
// Parameter	: double dbLinePair
// Qualifier	:
// Last Update	: 2018/1/9 - 17:37
// Desc.		:
//=============================================================================
double CTI_SFR::GetSFRValue(BYTE *pBuf, int iWidth, int iHeight, double dbPixelSize, double dbLinePair, UINT nMode)
 {
	 int imgWidth = iWidth;
	 int imgHeight = iHeight;

	 double tleft = 0;
	 double tright = 0;

	 double fil1[2] = { 0.5, -0.5 };
	 double fil2[3] = { 0.5, 0, -0.5 };
	 double dbResultSFR = 0;


	 //rotatev2가 들어가야 함.

	 for (int y = 0; y < imgHeight; y++)
	 {
		 for (int x = 0; x < 5; x++)
		 {
			 tleft += (int)pBuf[y * imgWidth + x];
		 }

		 for (int x = imgWidth - 6; x < imgWidth; x++)
		 {
			 tright += (int)pBuf[y * imgWidth + x];
		 }
	 }

	 if (tleft > tright)
	 {
		 fil1[0] = -0.5;
		 fil1[1] = 0.5;
		 fil2[0] = -0.5;
		 fil2[1] = 0;
		 fil2[2] = 0.5;
	 }

	 if (tleft + tright == 0.0)
	 {
		 //	printf("Zero divsion!\n");
		 return 0;
	 }
	 else
	 {
		 double test = fabs((tleft - tright) / (tleft + tright));

		 if (test < 0.1)
		 {
			 //		printf("Edge contrast is less that 20%\n");
			 return 0;
		 }
	 }

	 double n = imgWidth;
	 double mid = (imgWidth + 1) / 2.0;	// MatLab SFRmat3 에서는 +1을 해줌. #Lucas수정1

	 /////////////////////////// ahamming start
	 double wid1 = mid - 1;		// // MatLab SFRmat3 에서는 -1을 해줌. #Lucas수정2
	 double wid2 = n - mid;
	 double wid;
	 if (wid1 > wid2)
		 wid = wid1;
	 else
		 wid = wid2;

	 double arg = 0;

	 double *win1 = new double[imgWidth];

	 for (int i = 0; i < n; i++)
	 {
		 arg = i - mid + 1;	// MatLab은 1 base임으로, 0 base 시 1을 더해줌. #Lucas수정3
		 win1[i] = 0.54 + 0.46*cos((CV_PI*arg / wid));
	 }
	 /////////////////////////// ahamming end // 여기까지 sfrmat3와 동일 결과.

	 //******************************deriv1**********************************************//
	 // todo: #Lucas deriv1의 결과 값이 Matlab과 다름. 디버깅이 필요한데 소스가 달라 복잡함.
	 int d_n = 2;	//필터열갯수..
	 int m = imgWidth + d_n - 1;
	 double sum = 0;
	 double *Deriv_c = new double[imgHeight * m];
	 double *c = new double[imgWidth * imgHeight];

	 for (int k = 0; k < imgWidth * imgHeight; k++)
		 c[k] = 0.0;

	 for (int y = 0; y<imgHeight; y++)
	 {
		 for (int k = 0; k < imgWidth; k++)
		 {
			 sum = 0;

			 for (int d_j = d_n - 1; d_j > -1; d_j--)
			 {
				 if ((k - d_j) > -1 && (k - d_j) < imgWidth)
					 sum += (double)(pBuf[y * imgWidth + (k - d_j)]) * fil1[d_j];
			 }
			 Deriv_c[y * imgWidth + k] = sum;
		 }
	 }

	 for (int y = 0; y < imgHeight; y++)
	 {
		 for (int k = (d_n - 1); k < imgWidth; k++)
			 c[y * imgWidth + k] = Deriv_c[y * imgWidth + k];
		 c[y * imgWidth + d_n - 2] = Deriv_c[y * imgWidth + d_n - 1];
	 }

	 delete[]Deriv_c;
	 Deriv_c = NULL;
	 //************************************************************************************//
	 double *loc = new double[imgHeight];
	 double loc_v = 0, total_v = 0;

	 for (int y = 0; y < imgHeight; y++)
	 {
		 loc_v = 0;
		 total_v = 0;
		 for (int x = 0; x < imgWidth; x++)
		 {
			 loc_v += ((double)c[y * imgWidth + x] * win1[x])*((double)x + 1);	//수정 //MATLAB과 최대한 mid position을 맞춰주자..
			 total_v += (double)c[y * imgWidth + x] * win1[x];
		 }

		 if (total_v == 0 || total_v < 0.0001)
			 loc[y] = 0;
		 else
			 loc[y] = loc_v / total_v - 0.5;
	 }

	 delete[]win1;
	 //*****************************************************************************//	QR-DECOMP

	 //*****************************************************************************//
	 int rM = imgHeight;
	 int cN = 2;

	 int i, j, rm, cn;
	 int count = 0;

	 rm = rM;
	 cn = cN;

	 double **A = new double *[rM];
	 double **pinvA = new double *[cN];

	 for (i = 0; i < rM; i++)
		 A[i] = new double[cN];

	 for (i = 0; i < rM; i++)
	 {
		 A[i][0] = count;
		 A[i][1] = 1.0;
		 count++;
	 }

	 for (i = 0; i < cN; i++)
		 pinvA[i] = new double[rM];

	 pInverse(A, rM, cN, pinvA);

	 double **B = new double *[rM];
	 for (i = 0; i < rM; i++)
		 B[i] = new double[1];

	 for (i = 0; i < rM; i++)
		 B[i][0] = loc[i];

	 double **C = new double *[cN];
	 for (i = 0; i < cN; i++)
		 C[i] = new double[1];

	 MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	 //	printf("MulMatrix C[0][0]: %10.4f\n", C[0][0]);
	 //	printf("MulMatrix C[1][0]: %10.4f\n", C[1][0]);

	 double *place = new double[imgHeight];
	 double *win2 = new double[imgWidth];

	 for (j = 0; j<imgHeight; j++)
	 {
		 //**********hamming Window***************************//
		 place[j] = C[1][0] + C[0][0] * (double)(j + 1);

		 n = imgWidth;
		 mid = place[j];
		 wid1 = mid - 1;
		 wid2 = n - mid;

		 if (wid1 > wid2)
			 wid = wid1;
		 else
			 wid = wid2;

		 arg = 0;

		 for (i = 0; i < n; i++)
		 {
			 arg = (i + 1) - mid;
			 win2[i] = cos(CV_PI*arg / wid);
		 }

		 for (i = 0; i < n; i++)
			 win2[i] = 0.54 + 0.46*win2[i];

		 loc_v = 0;
		 total_v = 0;

		 for (int x = 0; x < imgWidth; x++)
			 total_v += c[j * imgWidth + x] * win2[x];

		 for (int x = 0; x < imgWidth; x++)
		 {
			 loc_v += (c[j * imgWidth + x] * win2[x])*(x + 1); // 수정
		 }

		 if (total_v == 0 || total_v < 0.0001)
			 loc[j] = 0;
		 else
			 loc[j] = loc_v / total_v - 0.5;
	 }

	 delete[]win2;
	 delete[]place;
	 delete[]c;

	 for (i = 0; i < rM; i++)
	 {
		 delete[]A[i];
		 delete[]B[i];
	 }
	 delete[]A;
	 delete[]B;

	 for (i = 0; i < cN; i++)
		 delete[]pinvA[i];
	 delete[]pinvA;

	 //====================centeroid fitting==============// Hamming Window를 통한 값으로 라인 fitting을 한번 더 해준다..(여기서 slope 구함)
	 rM = imgHeight;
	 cN = 2;

	 //	int i, j, rm, cn;
	 count = 0;

	 rm = rM;
	 cn = cN;

	 A = new double *[rM];
	 pinvA = new double *[cN];

	 for (i = 0; i < rM; i++)
		 A[i] = new double[cN];

	 for (i = 0; i < rM; i++)
	 {
		 A[i][0] = count;
		 A[i][1] = 1.0;
		 count++;
	 }

	 for (i = 0; i < cN; i++)
		 pinvA[i] = new double[rM];

	 pInverse(A, rM, cN, pinvA);

	 B = new double *[rM];
	 for (i = 0; i < rM; i++)
		 B[i] = new double[1];

	 for (i = 0; i < rM; i++)
		 B[i][0] = loc[i];

	 MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	 for (i = 0; i < rM; i++)
	 {
		 delete[]A[i];
		 delete[]B[i];
	 }
	 delete[]A;
	 delete[]B;

	 for (i = 0; i < cN; i++)
		 delete[]pinvA[i];
	 delete[]pinvA;

	 //===================================================================================================//
	 //여기까진 OK!
	 int nbin = 4;
	 double nn = imgWidth * nbin;
	 double nn2 = nn / 2 + 1;
	 double *freq = new double[(int)nn];
	 double del = 1.0;

	 for (i = 0; i < (int)nn; i++)
		 freq[i] = (double)nbin*((double)i) / (del*nn);

	 double freqlim = 1.0;
	 double nn2out = (nn2*freqlim / 2);
	 double *win = new double[nbin * imgWidth];

	 //**********hamming Window***************************//	
	 n = nbin * imgWidth;
	 mid = (nbin*imgWidth + 1) / 2.0;
	 wid1 = mid - 1;
	 wid2 = n - mid;

	 if (wid1 > wid2)
		 wid = wid1;
	 else
		 wid = wid2;

	 arg = 0;

	 for (i = 0; i < n; i++)
	 {
		 arg = (double)(i + 1) - mid;
		 win[i] = cos((CV_PI*arg / wid));
	 }

	 for (i = 0; i < n; i++)
		 win[i] = 0.54 + 0.46*win[i];

	 //sfrmat3로 수정===============================================================//

	 double fac = 4.0;
	 double inProject_nn = imgWidth * fac;
	 double slope = C[0][0];

	 double vslope = slope;

	 slope = 1.0 / slope;

	 double slope_deg = 180.0 * atan(abs(vslope)) / CV_PI;

	 if (slope_deg < 2.00)
	 {
		 delete[]freq;
		 delete[]win;

		 for (int i = 0; i < cN; i++)
			 delete[]C[i];
		 delete[]C;

		 delete[]loc;

		 return 0;
		 //resultSFR = 0.0;
		 //return resultSFR;//너무 급경사를 가진 Edge라서 제대로 측정할 수 없음. //예외 처리 구문을 넣어야 할듯..return 등등
	 }

	 double del2 = 0;
	 double delfac;

	 delfac = cos(atan(vslope));
	 del = del * delfac;
	 del2 = del / nbin;

	 //===========================================================================2018-02-20 주석처리하고!!
	 /*	//	double offset = 0;
	 double offset = fac * (0.0 - (((double)imgHeight - 1.0) / slope));

	 del = abs(cvRound(offset));
	 if (offset > 0) offset = 0.0;*/
	 //============================================================================

	 double *dcorr = new double[(int)nn2];
	 double temp_m = 3.0;	//length of difference filter
	 double scale = 1;

	 for (i = 0; i < nn2; i++)
		 dcorr[i] = 1.0;

	 temp_m = temp_m - 1;

	 for (i = 1; i<nn2; i++)
	 {
		 dcorr[i] = abs((CV_PI*(double)(i + 1)*temp_m / (2.0*(nn2 + 1))) / sin(CV_PI * (double)(i + 1)*temp_m / (2.0*(nn2 + 1))));	//필터를 이용하여 낮은 응답 신호 부분을 증폭시키려는 의도인듯....;;
		 dcorr[i] = 1.0 + scale * (dcorr[i] - 1.0);																	//scale로 weight를 조정하는듯..default 1

		 if (dcorr[i] > 10)
			 dcorr[i] = 10.0;
	 }

	 //============================================================================//
	 //sfrmat3 : PER ISO Algorithm
	 imgHeight = cvRound((double)(cvFloor((double)imgHeight * fabs(C[0][0]))) / fabs(C[0][0]));	//수직방향으로 1 Cycle이 이뤄지게하기 위함인듯..

	 for (i = 0; i < cN; i++)
		 delete[]C[i];

	 delete[]C;
	 delete[]loc;

	 //==========================================================================2018-02-20 여기로!!!
	 //	double offset = 0;
	 double offset = fac * (0.0 - (((double)imgHeight) / slope));

	 del = abs(cvRound(offset));
	 if (offset > 0) offset = 0.0;
	 //============================================================================

	 double **barray = new double *[2];
	 for (i = 0; i < 2; i++)
		 barray[i] = new double[(int)inProject_nn + (int)del + 100];

	 for (i = 0; i < 2; i++)
		 for (j = 0; j < ((int)inProject_nn + (int)del + 100); j++)
			 barray[i][j] = 0;

	 int tmp_x, tmp_y;

	 for (tmp_x = 0; tmp_x < imgWidth; tmp_x++)
	 {
		 for (tmp_y = 0; tmp_y < imgHeight; tmp_y++)
		 {
			 double ttt = ((double)(tmp_x)-((double)tmp_y / slope))*fac;
			 double ling = cvCeil(ttt) - (double)offset;
			 barray[0][(int)ling] = barray[0][(int)ling] + 1;
			 barray[1][(int)ling] = barray[1][(int)ling] + (double)pBuf[tmp_y * imgWidth + tmp_x];
		 }
	 }

	 double *point = new double[(int)inProject_nn];
	 int start = cvRound(del*0.5);
	 double nz = 0;
	 int status = 1;

	 for (i = start; i < start + (int)inProject_nn - 1; i++)
	 {
		 if (barray[0][i] == 0)
		 {
			 nz = nz + 1;
			 status = 0;
			 if (i == 0)
				 barray[0][i] = barray[0][i + 1];
			 else
				 barray[0][i] = (barray[0][i - 1] + barray[0][i + 1]) / 2.0;
		 }
	 }

	 if (status != 0)
	 {
		 printf(" Zero count(s) found during projection binning. The edge \n");
		 printf(" angle may be large, or you may need more lines of data. \n");
		 printf(" Execution will continue, but see Users Guide for info. \n");
	 }

	 for (i = -1; i < (int)inProject_nn - 1; i++)
	 {
		 if (barray[0][i + start] != 0)
		 {
			 if (barray[0][(i + 1) + start] == 0)
			 {
				 point[(i + 1)] = 0;
			 }
			 else
				 point[(i + 1)] = barray[1][(i + 1) + start] / barray[0][(i + 1) + start];
		 }
		 else
			 point[(i + 1)] = 0;
	 }

	 for (i = 0; i < 2; i++)
		 delete[]barray[i];
	 delete[]barray;

	 //******************************deriv1**********************************************//
	 m = (int)inProject_nn;
	 d_n = 3;	//필터열갯수..
	 sum = 0;
	 double *pointDeriv = new double[(int)inProject_nn];
	 Deriv_c = new double[(int)inProject_nn + d_n - 1];

	 for (int k = 0; k<(int)inProject_nn; k++)
		 pointDeriv[k] = 0.0;

	 for (int k = 0; k<m + d_n - 1; k++)
	 {
		 sum = 0;

		 for (int d_j = d_n - 1; d_j > -1; d_j--)
		 {
			 if ((k - d_j) > -1 && (k - d_j) < m)
				 sum += point[k - d_j] * fil2[d_j];
		 }

		 Deriv_c[k] = sum;
	 }

	 for (int k = (d_n - 1); k < (int)inProject_nn; k++)		//MATLAB은 배열이 1base이기 때문에 다시 정리해준다..
		 pointDeriv[k] = Deriv_c[k];
	 pointDeriv[d_n - 2] = Deriv_c[d_n - 1];
	 //************************************************************************************//

	 delete[]point;
	 delete[]Deriv_c;

	 //*********************centroid*************************************//
	 loc_v = 0, total_v = 0;

	 loc_v = 0;
	 total_v = 0;
	 for (int x = 0; x < (int)inProject_nn; x++)
	 {
		 loc_v += pointDeriv[x] * x;
		 total_v += pointDeriv[x];
	 }

	 if (total_v == 0 || total_v < 0.0001)
	 {
		 delete[]freq;
		 delete[]win;
		 delete[]pointDeriv;
		 return 0.0;
	 }
	 else
	 {
		 loc_v = loc_v / total_v;
	 }

	 //**************************cent****************************//
	 double *temp = new double[(int)inProject_nn];
	 for (i = 0; i<(int)inProject_nn; i++)
		 temp[i] = 0.0;

	 mid = cvRound((inProject_nn + 1.0) / 2.0);

	 int cent_del = cvRound(cvRound(loc_v) - mid);

	 if (cent_del > 0)
	 {
		 for (i = 0; i < inProject_nn - cent_del; i++)
			 temp[i] = pointDeriv[i + cent_del];
	 }
	 else if (cent_del < 1)
	 {
		 for (i = -cent_del; i < inProject_nn; i++)
			 temp[i] = pointDeriv[i + cent_del];
	 }
	 else
	 {
		 for (i = 0; i < inProject_nn; i++)
			 temp[i] = pointDeriv[i];
	 }

	 for (i = 0; i < (int)inProject_nn; i++)
		 temp[i] = win[i] * temp[i];

	 delete[]win;
	 delete[]pointDeriv;

	 CvMat *sourceFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	 CvMat *resultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	 CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);

	 for (i = 0; i < (int)inProject_nn; i++)
	 {
		 sourceFFT->data.db[i * 2] = temp[i];
		 sourceFFT->data.db[i * 2 + 1] = 0;
	 }

	 delete[]temp;

	 cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);

	 for (i = 0; i < (int)inProject_nn; i++)
	 {
		 if (i != 0)
		 {
			 ConveredResultFFT->data.db[i] = sqrt(pow(resultFFT->data.db[i * 2 - 1], 2) + pow(resultFFT->data.db[i * 2], 2));
		 }
		 else
		 {
			 ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
		 }
	 }
	 double *Resultmtf = new double[(int)inProject_nn];
	 int count_j = 0;

	 memset(Resultmtf, 0, sizeof(Resultmtf));

	 for (int i = 0; i < (int)inProject_nn / 2; i++)
	 {
		 //Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0]); //-> 0으로 나눗셈이 진행되는 문제가 있음
		 Resultmtf[count_j] = (0.0f != ConveredResultFFT->data.db[0]) ? (fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0])) : 0;
		 Resultmtf[count_j] = Resultmtf[count_j] * dcorr[i];
		 count_j++;
	 }

	 delete[]dcorr;	//correct weight 배열 해제

	 cvReleaseMat(&sourceFFT);
	 cvReleaseMat(&resultFFT);
	 cvReleaseMat(&ConveredResultFFT);


	 // 값 산출
	 double nPixel = 1000.0 / dbPixelSize;
	 double CyclePerPixel = dbLinePair / nPixel;

	 double	dbBefore = 0, dbNext = 0;
	 double dbBeforeFreq = 0;
	 double dbNextFreq = 0;

	 int nBefore = 0, nNext = 0;
	 int half_sampling_index = -1;
	 double data, old_data = 1.0;

	 // 산출 방식 분기
	 switch (nMode)
	 {
	 case enAlSFRDataType_MTF:

		 for (int q = 0; q < inProject_nn / 2; q++)
		 {
			 data = fabs(freq[q] - CyclePerPixel);

			 if (old_data > data)
			 {
				 old_data = data;
				 half_sampling_index = q;
			 }
		 }

		 if (Resultmtf[half_sampling_index] < 0 || half_sampling_index == -1)
			 dbResultSFR = 0.0;
		 else
			 dbResultSFR = Resultmtf[half_sampling_index];

		 break;

	 case enAlSFRDataType_CyPx:

		 dbBefore = dbNext = 1.0;
		 nBefore = nNext = 0;
		 for (int i = 0; i < (int)inProject_nn; i++)
		 {
			 nNext = i;
			 dbNext = Resultmtf[i];
			 TRACE(_T("Result[%d] Freq.:MTF--- %f,%f\n"), i, freq[i], dbNext);
			 if (dbNext < CyclePerPixel)
				 break;
			 dbBefore = dbNext;
			 nBefore = nNext;
		 }

		 dbBeforeFreq = freq[nBefore];
		 dbNextFreq = freq[nNext];

		 // 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
		 dbResultSFR = dbNextFreq - (CyclePerPixel - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq);

		 break;

	 case enAlSFRDataType_Cymm:

		 dbBefore = dbNext = 1.0;
		 nBefore = nNext = 0;
		 for (int i = 0; i < (int)inProject_nn; i++)
		 {
			 nNext = i;
			 dbNext = Resultmtf[i];
			 TRACE(_T("Result[%d] Freq.:MTF--- %f,%f\n"), i, freq[i], dbNext);
			 if (dbNext < CyclePerPixel)
				 break;
			 dbBefore = dbNext;
			 nBefore = nNext;
		 }

		 dbBeforeFreq = freq[nBefore];
		 dbNextFreq = freq[nNext];

		 dbResultSFR = dbNextFreq - (CyclePerPixel - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq);

		 // 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
		 dbResultSFR = (dbResultSFR / dbPixelSize) * 1000;

		 break;

	 default:
		 break;
	 }

	 delete[]freq;
	 delete[]Resultmtf;

	 return dbResultSFR;
 }


// double CTI_SFR::GetSFRValue(BYTE *pBuf, int iWidth, int iHeight, double dbPixelSize, double dbLinePair)
// {
// 	int imgWidth = iWidth;
// 	int imgHeight = iHeight;
// 
// 	double tleft = 0;
// 	double tright = 0;
// 
// 	double fil1[2] = { 0.5, -0.5 };
// 	double fil2[3] = { 0.5, 0, -0.5 };
// 
// 	for (int y = 0; y<imgHeight; y++)
// 	{
// 		for (int x = 0; x< 3; x++)
// 		{
// 			tleft += (unsigned char)pBuf[y * imgWidth + x];
// 		}
// 
// 		for (int x = imgWidth - 4; x<imgWidth; x++)
// 		{
// 			tright += (unsigned char)pBuf[y * imgWidth + x];
// 		}
// 	}
// 
// 	if (tleft > tright)
// 	{
// 		fil1[0] = -0.5;
// 		fil1[1] = 0.5;
// 		fil2[0] = -0.5;
// 		fil2[1] = 0;
// 		fil2[2] = 0.5;
// 	}
// 
// 	if ((tleft + tright) == 0.0)
// 	{
// 		//	printf("Zero divsion!\n");
// 		return 0;
// 	}
// 	else
// 	{
// 		double test = fabs((tleft - tright) / (tleft + tright));
// 
// 		if (test < 0.2)
// 		{
// 			//		printf("Edge contrast is less that 20%\n");
// 			return 0;
// 		}
// 	}
// 
// 	double n = imgWidth;
// 	double mid = (imgWidth) / 2.0;
// 	double wid1 = mid;
// 	double wid2 = n - mid;
// 	double wid;
// 
// 	if (wid1 > wid2)
// 		wid = wid1;
// 	else
// 		wid = wid2;
// 
// 	double arg = 0;
// 
// 	double *win1 = new double[imgWidth];
// 
// 	for (int i = 0; i<n; i++)
// 	{
// 		arg = i - mid;
// 		win1[i] = 0.54 + 0.46*cos((3.14*arg / wid));
// 	}
// 
// 	//******************************deriv1**********************************************//
// 	int d_n = 2;	//필터열갯수..
// 	int m = imgWidth + d_n - 1;
// 	double sum = 0;
// 	double *Deriv_c = new double[imgHeight * m];
// 	double *c = new double[imgWidth * imgHeight];
// 
// 	for (int k = 0; k<imgWidth * imgHeight; k++)
// 		c[k] = 0.0;
// 
// 	for (int y = 0; y<imgHeight; y++)
// 	{
// 		for (int k = 0; k<imgWidth; k++)
// 		{
// 			sum = 0;
// 
// 			for (int d_j = d_n - 1; d_j>-1; d_j--)
// 			{
// 				if ((k - d_j) > -1 && (k - d_j) < imgWidth)
// 					sum += (double)((unsigned char)pBuf[y * imgWidth + (k - d_j)])*fil1[d_j];
// 			}
// 
// 			Deriv_c[y * imgWidth + k] = sum;
// 		}
// 	}
// 
// 	for (int y = 0; y<imgHeight; y++)
// 	{
// 		for (int k = (d_n - 1); k<imgWidth; k++)
// 			c[y * imgWidth + k] = Deriv_c[y * imgWidth + k];
// 		c[y * imgWidth + d_n - 2] = Deriv_c[y * imgWidth + d_n - 1];
// 	}
// 
// 	delete[]Deriv_c;
// 	Deriv_c = NULL;
// 	//************************************************************************************//
// 	double *loc = new double[imgHeight];
// 	double loc_v = 0, total_v = 0;
// 
// 	for (int y = 0; y<imgHeight; y++)
// 	{
// 		loc_v = 0;
// 		total_v = 0;
// 		for (int x = 0; x<imgWidth; x++)
// 		{
// 			loc_v += (c[y * imgWidth + x] * win1[x])*(double)x;
// 			total_v += c[y * imgWidth + x] * win1[x];
// 		}
// 
// 		if (total_v == 0 || total_v < 0.0001)
// 			loc[y] = 0;
// 		else
// 			loc[y] = loc_v / total_v - 0.5;
// 	}
// 
// 	delete[]win1;
// 	//*****************************************************************************//	QR-DECOMP
// 
// 	//*****************************************************************************//
// 	int rM = imgHeight;
// 	int cN = 2;
// 
// 	int i, j, rm, cn;
// 	int count = 0;
// 
// 	rm = rM;
// 	cn = cN;
// 
// 	double **A = new double *[rM];
// 	double **pinvA = new double *[cN];
// 
// 	for (i = 0; i<rM; i++)
// 		A[i] = new double[cN];
// 
// 	for (i = 0; i<rM; i++)
// 	{
// 		A[i][0] = count;
// 		A[i][1] = 1.0;
// 		count++;
// 	}
// 
// 	for (i = 0; i<cN; i++)
// 		pinvA[i] = new double[rM];
// 
// 	pInverse(A, rM, cN, pinvA);
// 
// 	double **B = new double *[rM];
// 	for (i = 0; i<rM; i++)
// 		B[i] = new double[1];
// 
// 	for (i = 0; i<rM; i++)
// 		B[i][0] = loc[i];
// 
// 	double **C = new double *[cN];
// 	for (i = 0; i<cN; i++)
// 		C[i] = new double[1];
// 
// 	MulMatrix(pinvA, B, C, cN, 1, rM, 0);
// 
// 
// 	printf("%10.4f\n", C[0][0]);
// 	printf("%10.4f\n", C[1][0]);
// 
// 
// 	double *place = new double[imgHeight];
// 
// 	for (i = 0; i<imgHeight; i++)
// 		place[i] = C[1][0] + C[0][0] * (double)i;
// 
// 	double *win2 = new double[imgWidth];
// 
// 	for (j = 0; j<imgHeight; j++)
// 	{
// 		//**********hamming Window***************************//
// 		n = imgWidth;
// 		mid = place[j];
// 		wid1 = mid - 1;
// 		wid2 = n - mid;
// 		wid;
// 
// 		if (wid1 > wid2)
// 			wid = wid1;
// 		else
// 			wid = wid2;
// 
// 		arg = 0;
// 
// 		for (i = 0; i<n; i++)
// 		{
// 			arg = i - mid;
// 			win2[i] = 0.54 + 0.46*cos((3.14*arg / wid));
// 		}
// 
// 		//	for(int y=0; y<imgHeight; y++)
// 		{
// 			loc_v = 0;
// 			total_v = 0;
// 			for (int x = 0; x<imgWidth; x++)
// 			{
// 				loc_v += (c[j * imgWidth + x] * win2[x])*x;
// 				total_v += c[j * imgWidth + x] * win2[x];
// 			}
// 
// 			if (total_v == 0 || total_v < 0.0001)
// 				loc[j] = 0;
// 			else
// 				loc[j] = loc_v / total_v - 0.5;
// 		}
// 	}
// 
// 
// 
// 	delete[]win2;
// 	delete[]place;
// 	delete[]c;
// 
// 	for (i = 0; i<rM; i++)
// 		B[i][0] = loc[i];
// 
// 	MulMatrix(pinvA, B, C, cN, 1, rM, 0);
// 
// 	for (i = 0; i<rM; i++)
// 	{
// 		delete[]A[i];
// 		delete[]B[i];
// 	}
// 	delete[]A;
// 	delete[]B;
// 
// 	for (i = 0; i<cN; i++)
// 		delete[]pinvA[i];
// 	delete[]pinvA;
// 
// 
// 	int Param_100;
// 	Param_100 = 100;
// 
// 
// 	int nbin = 4;
// 	double nn = imgWidth * nbin;
// 	double nn2 = nn / 2 + 1;
// 	double *freq = new double[(int)nn*Param_100];
// 	double del = 1.0;
// 
// 
// 
// 	for (i = 0; i < (int)nn*Param_100; i++)
// 		freq[i] = (double)nbin*((double)i) / (del*nn*Param_100);
// 
// 	double freqlim = 1.0;
// 	double nn2out = (nn2*freqlim / 2);
// 	double *win = new double[nbin * imgWidth];
// 
// 	//**********hamming Window***************************//	
// 	n = nbin * imgWidth;
// 	mid = (nbin*imgWidth + 1) / 2;
// 	wid1 = mid - 1;
// 	wid2 = n - mid;
// 	wid;
// 
// 	if (wid1 > wid2)
// 		wid = wid1;
// 	else
// 		wid = wid2;
// 
// 	arg = 0;
// 
// 	for (i = 0; i<n; i++)
// 	{
// 		arg = i - mid;
// 		win[i] = 0.54 + 0.46*cos((3.14*arg / wid));
// 	}
// 
// 	double fac = 4.0;
// 	double inProject_nn = imgWidth * fac;
// 	double slope = C[0][0];
// 	//**********hamming Window***************************//
// 	n = inProject_nn;
// 	mid = fac * loc[0];
// 	wid1 = mid - 1;
// 	wid2 = n - mid;
// 	wid;
// 
// 	if (wid1 > wid2)
// 		wid = wid1;
// 	else
// 		wid = wid2;
// 
// 	arg = 0;
// 
// 	for (i = 0; i<n; i++)
// 	{
// 		arg = i - mid;
// 		win[i] = 0.54 + 0.46*cos((3.14*arg / wid));
// 	}
// 	//***************************************************//
// 	slope = 1.0 / slope;
// 
// 	double offset = fac * (0.0 - (((double)imgHeight - 1.0) / slope));
// 
// 	del = abs(cvRound(offset));
// 	if (offset > 0) offset = 0.0;
// 
// 
// 	for (i = 0; i<cN; i++)
// 		delete[]C[i];
// 
// 	delete[]C;
// 	delete[]loc;
// 
// 	double **barray = new double *[2];
// 	for (i = 0; i<2; i++)
// 		barray[i] = new double[(int)inProject_nn + (int)del + 200];
// 
// 
// 
// 	for (i = 0; i<2; i++)
// 		for (j = 0; j<((int)inProject_nn + (int)del + 100); j++)
// 			barray[i][j] = 0;
// 
// 
// 
// 	for (int x = 0; x<imgWidth; x++)
// 	{
// 		for (int y = 0; y<imgHeight; y++)
// 		{
// 			int ling = (int)((double)cvCeil(((double)x - (double)y / slope)*fac) - offset);
// 
// 			double kkk = barray[0][ling];
// 			barray[0][ling] = kkk + 1;
// 			double kk = barray[1][ling];
// 			barray[1][ling] = kk + (unsigned char)pBuf[y * imgWidth + x];
// 		}
// 	}
// 
// 
// 
// 
// 	double *point = new double[(int)inProject_nn];
// 	int start = cvRound(del*0.5);
// 	double nz = 0;
// 	int status = 1;
// 
// 	for (i = start; i<start + (int)inProject_nn - 1; i++)
// 	{
// 		if (barray[0][i] == 0)
// 		{
// 			nz = nz + 1;
// 			status = 0;
// 			if (i == 0)
// 				barray[0][i] = barray[0][i + 1];
// 			else
// 				barray[0][i] = (barray[0][i - 1] + barray[0][i + 1]) / 2.0;
// 		}
// 	}
// 
// 	if (status == 0)
// 	{
// 		printf(" Zero count(s) found during projection binning. The edge \n");
// 		printf(" angle may be large, or you may need more lines of data. \n");
// 		printf(" Execution will continue, but see Users Guide for info. \n");
// 	}
// 
// 	for (i = 0; i<(int)inProject_nn; i++)
// 	{
// 		if (barray[0][i + start] != 0)
// 			point[i] = barray[1][i + start] / barray[0][i + start];
// 		else
// 			point[i] = 0;
// 	}
// 
// 	for (i = 0; i<2; i++)
// 		delete[]barray[i];
// 
// 	delete[]barray;
// 
// 	//	IplImage *esf_image = cvCreateImage(cvSize(inProject_nn, 255), IPL_DEPTH_8U, 1);
// 	//	
// 	//	cvSetZero(esf_image);
// 	//
// 	//	for(i=0; i<(int)inProject_nn-1; i++)
// 	//		cvLine(esf_image, cvPoint(i, point[i]), cvPoint(i+1, point[i+1]), CV_RGB(255, 255, 255), 1, 8);
// 	//	
// 	//	cvFlip(esf_image);
// 	//
// 	////	cvShowImage("esf", esf_image);
// 	////	cvSaveImage("d:\\esf_image.bmp", esf_image);
// 	//	
// 	//	cvReleaseImage(&esf_image);
// 
// 	//******************************deriv1**********************************************//
// 	m = (int)inProject_nn;
// 	d_n = 3;	//필터열갯수..
// 	sum = 0;
// 	double *pointDeriv = new double[(int)inProject_nn];
// 	Deriv_c = new double[(int)inProject_nn + d_n - 1];
// 
// 	for (int k = 0; k<(int)inProject_nn; k++)
// 		pointDeriv[k] = 0.0;
// 
// 	for (int k = 0; k<m + d_n - 1; k++)
// 	{
// 		sum = 0;
// 
// 		for (int d_j = d_n - 1; d_j>-1; d_j--)
// 		{
// 			if ((k - d_j) > -1 && (k - d_j) < m)
// 				sum += point[k - d_j] * fil2[d_j];
// 		}
// 
// 		Deriv_c[k] = sum;
// 	}
// 
// 	for (int k = (d_n - 1); k<(int)inProject_nn; k++)
// 		pointDeriv[k] = Deriv_c[k];
// 	pointDeriv[d_n - 2] = Deriv_c[d_n - 1];
// 	//************************************************************************************//
// 
// 	delete[]point;
// 	delete[]Deriv_c;
// 
// 	//*********************centroid*************************************//
// 	loc_v = 0, total_v = 0;
// 
// 	loc_v = 0;
// 	total_v = 0;
// 	for (int x = 0; x<(int)inProject_nn; x++)
// 	{
// 		loc_v += pointDeriv[x] * x;
// 		total_v += pointDeriv[x];
// 	}
// 
// 	if (total_v == 0 || total_v < 0.0001)
// 	{
// 		delete[] freq;
// 		delete[] win;
// 		delete[] pointDeriv;
// 
// 		return 0.0;
// 	}
// 	else
// 		loc_v = loc_v / total_v;
// 
// 	//**************************cent****************************//
// 	double *temp = new double[(int)inProject_nn];
// 	for (i = 0; i<(int)inProject_nn; i++)
// 		temp[i] = 0.0;
// 
// 	mid = cvRound((inProject_nn + 1) / 2);
// 	int cent_del = cvRound(cvRound(loc_v) - mid);
// 
// 	if (cent_del > 0)
// 	{
// 		for (i = 0; i<inProject_nn - cent_del; i++)
// 			temp[i] = pointDeriv[i + cent_del];
// 	}
// 	else if (cent_del < 1)
// 	{
// 		for (i = -cent_del; i<inProject_nn; i++)
// 			temp[i] = pointDeriv[i + cent_del];
// 	}
// 	else
// 	{
// 		for (i = 0; i<inProject_nn; i++)
// 			temp[i] = pointDeriv[i];
// 	}
// 
// 	for (i = 0; i<(int)inProject_nn; i++)
// 		temp[i] = win[i] * temp[i];
// 
// 	delete[]win;
// 	delete[]pointDeriv;
// 
// 	//	CvMat *sourceFFT = cvCreateMat((int)inProject_nn*2, 1, CV_64FC1);
// 	//	CvMat *resultFFT = cvCreateMat((int)inProject_nn*2, 1, CV_64FC1);
// 	//	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn, 1, CV_64FC1);
// 	//
// 	//	for(i=0; i<(int)inProject_nn; i++)
// 	//	{
// 	//		sourceFFT->data.db[i*2] = temp[i];
// 	//		sourceFFT->data.db[i*2+1] = 0;
// 	//	}
// 	//
// 	//	delete []temp;
// 	//	
// 	//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);	
// 	//	
// 	//	for(i=0; i<(int)inProject_nn; i++)
// 	//	{
// 	//		if( i != 0)
// 	//			ConveredResultFFT->data.db[i] = sqrt ( pow (resultFFT->data.db[i*2-1], 2) + pow (resultFFT->data.db[i*2], 2) ) ;
// 	//		else
// 	//			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
// 	//	}
// 	//	
// 	//	double *Resultmtf = new double[(int)inProject_nn];
// 	//	int count_j=0;
// 	//
// 	//	for(i=0; i<(int)inProject_nn; i++)
// 	//	{
// 	//	//	if( i == 0 || i%2 != 0)
// 	//	//	{
// 	//			Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i])/ fabs(ConveredResultFFT->data.db[0]);
// 	//			count_j++;
// 	//	//	}
// 	//		
// 	//	}
// 	//	
// 	//	cvReleaseMat(&sourceFFT);
// 	//	cvReleaseMat(&resultFFT);
// 	//	cvReleaseMat(&ConveredResultFFT);
// 	//
// 	//	IplImage *SFRGraph_image = cvCreateImage(cvSize(inProject_nn, 150), IPL_DEPTH_8U, 3);
// 	//	cvSetZero(SFRGraph_image);
// 	//	
// 	//	for(int q = 0; q<inProject_nn/2; q++)
// 	//	{
// 	//		cvLine(SFRGraph_image, cvPoint(q, cvRound(Resultmtf[q] * 100.0)), cvPoint( (q+1), cvRound(Resultmtf[q+1] * 100.0)), CV_RGB(255, 255, 255), 1, 8);
// 	//	}
// 	//	
// 	//	cvFlip(SFRGraph_image);
// 	//
// 	//	cvSaveImage("d:\\SFR_GRAPH.bmp", SFRGraph_image);
// 	//
// 	//	cvReleaseImage(&SFRGraph_image);
// 	//
// 	//	double nPixel = 1000.0 / m_pDlg_SFROpt->nFieldWidth;
// 	//	double cyPx = m_pDlg_SFROpt->nLinePerPixel / nPixel;
// 	//
// 	//	int half_sampling_index = -1;
// 	//	double data, old_data= 1.0;	
// 	//	
// 	//	for(int q = 0; q<inProject_nn/2; q++)
// 	//	{
// 	//		data = fabs(freq[q] - cyPx);
// 	//		
// 	//		if(old_data > data)
// 	//		{
// 	//			old_data = data;
// 	//			half_sampling_index = q;
// 	//		}			
// 	//	}	
// 	//		
// 	//	double resultSFR;
// 	//
// 	//	if( Resultmtf[half_sampling_index] < 0  || half_sampling_index == -1)
// 	//		resultSFR = 0.0;
// 	//	else
// 	//		resultSFR = Resultmtf[half_sampling_index] * 100.0;
// 	//
// 	//	
// 	//	delete []freq;
// 	//	delete []Resultmtf;
// 	////	delete []atemp;
// 	//	
// 	//	return resultSFR;
// 
// 	CvMat *sourceFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
// 	CvMat *resultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
// 	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn, 1, CV_64FC1);
// 
// 	for (i = 0; i<(int)inProject_nn; i++)
// 	{
// 		sourceFFT->data.db[i * 2] = temp[i];
// 		sourceFFT->data.db[i * 2 + 1] = 0;
// 	}
// 
// 	delete[]temp;
// 
// 	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);
// 
// 	for (i = 0; i<(int)inProject_nn; i++)
// 	{
// 		if (i != 0)
// 		{
// 			ConveredResultFFT->data.db[i] = sqrt(pow(resultFFT->data.db[i * 2 - 1], 2) + pow(resultFFT->data.db[i * 2], 2));
// 		}
// 		else
// 		{
// 			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
// 		}
// 	}
// 
// 	//	CvMat *sourceFFT = cvCreateMat((int)12, 1, CV_64FC1);
// 	//	CvMat *resultFFT = cvCreateMat((int)12, 1, CV_64FC1);
// 	//	CvMat *ConveredResultFFT = cvCreateMat((int)12, 1, CV_64FC1);
// 	////	for(i=0; i<(int)inProject_nn; i++)
// 	////		sourceFFT->data.db[i] = temp[i];
// 	//	sourceFFT->data.db[0] = 0.0001;
// 	//	sourceFFT->data.db[1] = 0;
// 	//	sourceFFT->data.db[2] = -0.2222;
// 	//	sourceFFT->data.db[3] = 0;
// 	//	sourceFFT->data.db[4] = 0.1111;
// 	//	sourceFFT->data.db[5] = 0;
// 	//	sourceFFT->data.db[6] = 0.4444;
// 	//	sourceFFT->data.db[7] = 0;
// 	//	sourceFFT->data.db[8] = 0.2211;
// 	//	sourceFFT->data.db[9] = 0;
// 	//	sourceFFT->data.db[10] = -1.0000;
// 	//	sourceFFT->data.db[11] = 0;
// 	//	
// 	//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);
// 	//
// 	//	for(i=0; i<6; i++)
// 	//	{
// 	//		if( i != 0)
// 	//		{
// 	//			ConveredResultFFT->data.db[i] = sqrt ( pow (resultFFT->data.db[i*2-1], 2) + pow (resultFFT->data.db[i*2], 2) ) ;
// 	//		}
// 	//		else
// 	//		{
// 	//		//	resultFFT->data.db[i*2] = fabs(resultFFT->data.db[i*2]);
// 	//			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
// 	//		}
// 	//	}
// 
// 	//	delete []temp;
// 
// 	//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);	
// 
// 	//	double k1 = sqrt ( pow (resultFFT->data.db[5], 2) + pow (resultFFT->data.db[6], 2) ) ;
// 
// 	double *Resultmtf = new double[(int)inProject_nn];
// 	int count_j = 0;
// 	int count_i = 0;
// 
// 	for (count_i = 0; count_i < (int)inProject_nn; count_i++)
// 	{
// 		Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[count_i]) / fabs(ConveredResultFFT->data.db[0]);
// 		count_j++;
// 	}
// 
// 	cvReleaseMat(&sourceFFT);
// 	cvReleaseMat(&resultFFT);
// 	cvReleaseMat(&ConveredResultFFT);
// 
// 	double *Resultmtf_Test = new double[(int)inProject_nn * 100];
// 	/// Gap 100배 확대
// 	if (Param_100 == 100){
// 		int Temp_RangeMax = (int)inProject_nn * 100;
// 		for (int i = 0; i < Temp_RangeMax; i++){
// 			int Temp_c = i / 100;
// 			int Temp_Plus_c = i % 100;
// 			double Now_Step = Resultmtf[Temp_c];
// 			double Next_Step = Resultmtf[Temp_c + 1];
// 
// 			double Gap_OneStep = abs(Now_Step - Next_Step) / 100.0;
// 			if (Now_Step > Next_Step){
// 				Resultmtf_Test[i] = Now_Step - (Gap_OneStep * Temp_Plus_c);
// 			}
// 			else{
// 				Resultmtf_Test[i] = Now_Step + (Gap_OneStep * Temp_Plus_c);
// 			}
// 
// 		}
// 	}
// 	else{
// 		for (int i = 0; i < inProject_nn; i++){
// 			Resultmtf_Test[i] = Resultmtf[i];
// 		}
// 	}
// 
// 
// 	double nPixel = 1000.0 / dbPixelSize;
// 	double cyPx = dbLinePair / nPixel;
// 
// 	int half_sampling_index = -1;
// 	double data, old_data = 1.0;
// 
// 	for (int q = 0; q<inProject_nn*Param_100 / 2; q++)
// 	{
// 		data = fabs(freq[q] - cyPx);
// 
// 		if (old_data > data)
// 		{
// 			old_data = data;
// 			half_sampling_index = q;
// 		}
// 	}
// 
// 	double resultSFR;
// 
// 	/*cvLine(SFRGraph_image, cvPoint(half_sampling_index, 0), cvPoint(half_sampling_index, SFRGraph_image->height), CV_RGB(255, 255, 0), 1, 8);
// 
// 	cvSaveImage("C:\\SFR_GRAPH.bmp", SFRGraph_image);
// 
// 	cvReleaseImage(&SFRGraph_image);*/
// 
// 	if (Resultmtf_Test[half_sampling_index] < 0 || half_sampling_index == -1)
// 		resultSFR = 0.0;
// 	else
// 		resultSFR = Resultmtf_Test[half_sampling_index];
// 	//resultSFR = Resultmtf[half_sampling_index] * 100.0;
// 
// 	/*int half_sampling_index = -1;
// 	double data, old_data= 1.0;
// 
// 	for(int q = 0; q<nn2; q++)
// 	{
// 	data = fabs(Resultmtf[q] - 0.5);
// 
// 	if(old_data > data)
// 	{
// 	old_data = data;
// 	half_sampling_index = q;
// 	}
// 	}
// 
// 	double resultSFR;
// 
// 	if( freq[half_sampling_index] < 0  || half_sampling_index == -1)
// 	resultSFR = 0.0;
// 	else
// 	resultSFR = freq[half_sampling_index+1];*/
// 
// 	delete[]freq;
// 	delete[]Resultmtf;
// 	delete[]Resultmtf_Test;
// 	//	delete []atemp;
// 
// 	return resultSFR;
// }

//=============================================================================
// Method		: MulMatrix
// Access		: protected  
// Returns		: void
// Parameter	: double * * A
// Parameter	: double * * B
// Parameter	: double * * C
// Parameter	: unsigned int Row
// Parameter	: unsigned int Col
// Parameter	: unsigned int n
// Parameter	: unsigned int Mode
// Qualifier	:
// Last Update	: 2017/11/3 - 12:31
// Desc.		:
//=============================================================================
void CTI_SFR::MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode)
{
	////  A[Row][n] x B[n][Col] = [Row x n] x [n x Col] = [ Row x Col ] = C[Row][Col]   ///////
	////  Mode :  [ Mode 0 : A x B , Mode 1 : A' x B, Mode 2 : A x B' ] 
	unsigned int i, j, k;

	for (i = 0; i < Row; i++){
		for (j = 0; j < Col; j++){
			C[i][j] = 0;
			for (k = 0; k < n; k++){
				if (Mode == 0)	     C[i][j] += A[i][k] * B[k][j];   // Mode 0 : A  x B
				else if (Mode == 1) C[i][j] += A[k][i] * B[k][j];   // Mode 1 : A' x B
				else if (Mode == 2) C[i][j] += A[i][k] * B[j][k];   // Mode 2 : A  x B' 
			}
		}
	}
}

//=============================================================================
// Method		: pInverse
// Access		: protected  
// Returns		: void
// Parameter	: double * * A
// Parameter	: unsigned int Row
// Parameter	: unsigned int Col
// Parameter	: double * * RtnMat
// Qualifier	:
// Last Update	: 2017/11/3 - 12:31
// Desc.		:
//=============================================================================
void CTI_SFR::pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat)
{

	unsigned int i;//, j;
	double **AxA, **InvAxA;

	AxA = new double *[Col];
	for (i = 0; i < Col; i++)
		AxA[i] = new double[Col];

	InvAxA = new double *[Col];
	for (i = 0; i < Col; i++)
		InvAxA[i] = new double[Col];

	MulMatrix(A, A, AxA, Col, Col, Row, 1);  // Mode 1 : A' * B

	Inverse(AxA, Col, InvAxA);

	MulMatrix(InvAxA, A, RtnMat, Col, Row, Col, 2);

	for (i = 0; i < Col; i++)
		delete[]InvAxA[i];
	delete[]InvAxA;

	for (i = 0; i < Col; i++)
		delete[]AxA[i];

	delete[] AxA;
}

//=============================================================================
// Method		: Inverse
// Access		: protected  
// Returns		: void
// Parameter	: double * * dataMat
// Parameter	: unsigned int n
// Parameter	: double * * MatRtn
// Qualifier	:
// Last Update	: 2017/11/3 - 12:32
// Desc.		:
//=============================================================================
void CTI_SFR::Inverse(double **dataMat, unsigned int n, double **MatRtn)
{
	unsigned int i, j;
	double *dataA;

	CvMat matA;
	CvMat *pMatB = cvCreateMat(n, n, CV_64F);

	dataA = new double[n*n];

	for (i = 0; i < n; i++)
	for (j = 0; j < n; j++)
		dataA[i*n + j] = dataMat[i][j];

	matA = cvMat(n, n, CV_64F, dataA);

	cvInvert(&matA, pMatB);
	//	PrintMat(&matA, "pMatA = Source");    // Mat print

	for (i = 0; i < n; i++)
	for (j = 0; j < n; j++)
		MatRtn[i][j] = cvGetReal2D(pMatB, i, j);  // A = U * W * VT  ,  MatRtn : VT

	cvReleaseMat(&pMatB);
	delete[]dataA;
}

double CTI_SFR::GetDistance(int ix1, int iy1, int ix2, int iy2)
{
	double result;

	result = sqrt((double)((ix2 - ix1)*(ix2 - ix1)) + ((iy2 - iy1)*(iy2 - iy1)));

	return result;
}

void CTI_SFR::SFR_AutoDetectionROI(IplImage* pImageBuf, CRect rtROI, int *iOffSetX, int *iOffSetY)
{
	CvMemStorage* pContourStorage = cvCreateMemStorage(0);
	CvSeq *pContour = 0;
	IplImage* pSrcImage = cvCreateImage(cvGetSize(pImageBuf), IPL_DEPTH_8U, 1);
	IplImage* pBinaryImage = cvCreateImage(cvGetSize(pImageBuf), IPL_DEPTH_8U, 1);

	cvCvtColor(pImageBuf, pSrcImage, CV_RGB2GRAY);

	cvThreshold(pSrcImage, pBinaryImage, 0, 255, CV_THRESH_OTSU);

	cvNot(pBinaryImage, pBinaryImage);
	cvRectangle(pBinaryImage, cvPoint(0, 0), cvPoint(pBinaryImage->width - 3, pBinaryImage->height - 3), CV_RGB(0, 0, 0), 6);

	cvFindContours(pBinaryImage, pContourStorage, &pContour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	CvRect rcContour;

	CvPoint ROICenterPoint;

	ROICenterPoint.x = (rtROI.left + rtROI.right) / 2;
	ROICenterPoint.y = (rtROI.top + rtROI.bottom) / 2;

	// ROI에 근접한 Contours 구하기
	double Old_Dist = 99999;
	CvRect Near_ContP;
	for (; pContour != 0; pContour = pContour->h_next)
	{
		rcContour = cvBoundingRect(pContour, 1);

		double Dist_Contour2ROI = GetDistance(rcContour.x + (rcContour.width / 2), rcContour.y + (rcContour.height / 2), ROICenterPoint.x, ROICenterPoint.y);

		if (Old_Dist > Dist_Contour2ROI && rcContour.width < pImageBuf->width / 2 && rcContour.height < pImageBuf->height / 2)
		{
			Old_Dist = Dist_Contour2ROI;
			Near_ContP.x = rcContour.x - 5; // 여유 +-5 pixel
			Near_ContP.y = rcContour.y - 5; // 여유 +-5 pixel
			Near_ContP.width = rcContour.width + 10;
			Near_ContP.height = rcContour.height + 10;
		}
	}

	// 근접한 Contours 이미지 따기
	IplImage* pContoresImage = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_8U, 1);
	IplImage* eig_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
	IplImage* temp_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);

	if (Near_ContP.x < 0)
		Near_ContP.x = 0;

	if (Near_ContP.y < 0)
		Near_ContP.y = 0;

	if (Near_ContP.x + Near_ContP.width > pImageBuf->width - 1)
		Near_ContP.x = pImageBuf->width - Near_ContP.width - 1;

	if (Near_ContP.y + Near_ContP.height > pImageBuf->height - 1)
		Near_ContP.y = pImageBuf->height - Near_ContP.height - 1;

	cvSetImageROI(pSrcImage, Near_ContP);
	cvCopy(pSrcImage, pContoresImage);
	cvResetImageROI(pSrcImage);

	// 스무딩 처리
	cvDilate(pContoresImage, pContoresImage, 0, 3);
	cvErode(pContoresImage, pContoresImage, 0, 3);
	cvSmooth(pContoresImage, pContoresImage);

	int MAX_CORNERS = 50;
	CvPoint2D32f* corners = new CvPoint2D32f[MAX_CORNERS];

	cvGoodFeaturesToTrack(pContoresImage, eig_image, temp_image, corners, &MAX_CORNERS, 0.10, 5.0, 0, 5, 0, 0.04);

	CvPoint LTpoint, RTpoint, LBpoint, RBpoint;

	// 삭제 예정
	for (int i = 0; i < MAX_CORNERS; i++)
		cvCircle(pSrcImage, cvPoint(corners[i].x, corners[i].y), 5, CV_RGB(255, 0, 0), 1);

	// 이미지의 끝에 근접한 코너점 검출
	double Tmp_Dist = 9999;
	// Left _ Top Point 
	for (int i = 0; i < MAX_CORNERS; i++)
	{
		double Dist = GetDistance(0, 0, corners[i].x, corners[i].y);
		if (Tmp_Dist > Dist)
		{
			Tmp_Dist = Dist;
			LTpoint.x = corners[i].x;
			LTpoint.y = corners[i].y;
		}
	}

	// Right _ Top Point 
	Tmp_Dist = 9999;
	for (int i = 0; i < MAX_CORNERS; i++)
	{
		double Dist = GetDistance(pContoresImage->width, 0, corners[i].x, corners[i].y);
		if (Tmp_Dist > Dist)
		{
			Tmp_Dist = Dist;
			RTpoint.x = corners[i].x;
			RTpoint.y = corners[i].y;
		}
	}

	// Left _ Bottom Point 
	Tmp_Dist = 9999;
	for (int i = 0; i < MAX_CORNERS; i++)
	{
		double Dist = GetDistance(0, pContoresImage->height, corners[i].x, corners[i].y);
		if (Tmp_Dist > Dist)
		{
			Tmp_Dist = Dist;
			LBpoint.x = corners[i].x;
			LBpoint.y = corners[i].y;
		}
	}

	// Right _ Bottom Point 
	Tmp_Dist = 9999;
	for (int i = 0; i < MAX_CORNERS; i++)
	{
		double Dist = GetDistance(pContoresImage->width, pContoresImage->height, corners[i].x, corners[i].y);
		if (Tmp_Dist > Dist)
		{
			Tmp_Dist = Dist;
			RBpoint.x = corners[i].x;
			RBpoint.y = corners[i].y;
		}
	}

	// 각 코너의 중심점 계산

	CvPoint Center_Point[4];
	
	if (MAX_CORNERS > 0)
	{
		//UP
		Center_Point[0].x = ((LTpoint.x + RTpoint.x) / 2) + Near_ContP.x;
		Center_Point[0].y = ((LTpoint.y + RTpoint.y) / 2) + Near_ContP.y;
		//RIGHT
		Center_Point[1].x = ((RTpoint.x + RBpoint.x) / 2) + Near_ContP.x;
		Center_Point[1].y = ((RTpoint.y + RBpoint.y) / 2) + Near_ContP.y;
		//DOWN
		Center_Point[2].x = ((LBpoint.x + RBpoint.x) / 2) + Near_ContP.x;
		Center_Point[2].y = ((LBpoint.y + RBpoint.y) / 2) + Near_ContP.y;
		//LEFT
		Center_Point[3].x = ((LTpoint.x + LBpoint.x) / 2) + Near_ContP.x;
		Center_Point[3].y = ((LTpoint.y + LBpoint.y) / 2) + Near_ContP.y;

		double Max_Dist = 99999;
		int Near_PointX = 0;
		int Near_PointY = 0;
		for (int i = 0; i < 4; i++)
		{
			double Dist_tmp = GetDistance(Center_Point[i].x, Center_Point[i].y, (rtROI.left + rtROI.right) / 2, (rtROI.top + rtROI.bottom) / 2);
			if (Max_Dist > Dist_tmp)
			{
				Max_Dist = Dist_tmp;
				Near_PointX = Center_Point[i].x;
				Near_PointY = Center_Point[i].y;
			}

		}

		*iOffSetX = Near_PointX - (rtROI.left + rtROI.right) / 2;
		*iOffSetY = Near_PointY - (rtROI.top + rtROI.bottom) / 2;
	}

	cvReleaseMemStorage(&pContourStorage);
	cvReleaseImage(&pContoresImage);
	cvReleaseImage(&pSrcImage);
	cvReleaseImage(&pBinaryImage);
	cvReleaseImage(&eig_image);
	cvReleaseImage(&temp_image);
}
