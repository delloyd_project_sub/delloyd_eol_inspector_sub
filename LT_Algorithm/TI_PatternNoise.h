#pragma once
#include "cv.h"
#include "highgui.h"
#include "Def_AI_Common.h"

class CTI_PatternNoise
{
public:
	CTI_PatternNoise();
	~CTI_PatternNoise();

	CRect PN_RECT[9];
	void PatternNoise_Test(IplImage* InputImage, CRect rtROI, int NUM, int nwidth, int nheight, double& Resultavg_origin, double& ResultTotalSNR);
};

