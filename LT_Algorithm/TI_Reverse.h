
#pragma once

#include "cv.h"
#include "highgui.h"
#include "Def_AI_Common.h"

class CTI_Reverse
{
public:
	CTI_Reverse();
	~CTI_Reverse();

	BOOL Reverse_Test(IplImage* InputImage, int iWidth, int iHeight, CRect rtROI, int &iPosX, int &iPosY, int iColor);
	double GetDistance(__in int iSrcX, __in int iSrcY, __in int iDstX, __in int iDstY);
};

