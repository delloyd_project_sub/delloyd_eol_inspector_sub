﻿#pragma once

#include <afxwin.h>

#include "cv.h"
#include "highgui.h"
#include "Def_AI_Common.h"

class CTI_SFR
{
public:
	CTI_SFR();
	~CTI_SFR();

public:

	double SFR_Test(double dbPixelSize, double dbLinPair, IplImage* pImageBuf, UINT nResultType);

protected:

	double	GetSFRValue(BYTE *pBuf, int iWidth, int iHeight, double dbPixelSize, double dbLinePair, UINT nMode);
	void	MulMatrix	(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode);
	void	pInverse	(double **A, unsigned int Row, unsigned int Col, double **RtnMat);
	void	Inverse		(double **dataMat, unsigned int n, double **MatRtn);

	double	GetDistance(int ix1, int iy1, int ix2, int iy2);
	void	SFR_AutoDetectionROI(IplImage* pImageBuf, CRect rtROI, int *iOffSetX, int *iOffSetY); //hth

};