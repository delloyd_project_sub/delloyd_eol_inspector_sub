﻿#pragma once

#include <afxwin.h>

#include "cv.h"
#include "highgui.h"
#include "Def_AI_Common.h"

class CTI_Brightness
{
public:
	CTI_Brightness();
	virtual ~CTI_Brightness();

	void Brightness_Test (__in IplImage* pImageBuf, __out BYTE& ByY);

protected:
};

