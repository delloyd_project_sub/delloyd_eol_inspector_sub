﻿//*****************************************************************************
// Filename	: 	Dlg_Barcode.h
// Created	:	2016/11/1 - 16:59
// Modified	:	2016/11/1 - 16:59
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Dlg_Barcode_h__
#define Dlg_Barcode_h__

#pragma once

#include "resource.h"
#include "Def_Enum_Cm.h"
#include "Def_DataStruct_Cm.h"
#include "VGStatic.h"

//#define		STR_NOTUSE_BARCODE		_T("NotUse")
#define		STR_NO_BARCODE			_T("NoBarcode")

//-----------------------------------------------------------------------------
// CDlg_Barcode dialog
//-----------------------------------------------------------------------------
class CDlg_Barcode : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Barcode)

public:
	CDlg_Barcode(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlg_Barcode();

// Dialog Data
	enum { IDD = IDD_DLG_BARCODE };

protected:
	virtual void	DoDataExchange		(CDataExchange* pDX);    // DDX/DDV support
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void	OnGetMinMaxInfo		(MINMAXINFO* lpMMI);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage	(MSG* pMsg);
	virtual BOOL	OnInitDialog		();
	virtual void	OnOK				();
	
	DECLARE_MESSAGE_MAP()

	
	CFont			m_font_Large;
	CFont			m_font_Default;

	UINT			m_nBarcodeCnt;			// 사용되는 바코드 채널 수
	UINT			m_nBarcodeLength;		// 바코드 길이
	CStringArray	m_szarBarcodez;			// 채널 수 만큼 저장되는 바코드 스트링 버퍼

	CVGStatic		m_st_Site[MAX_SITE_CNT];
	CEdit			m_ed_Barcode[MAX_SITE_CNT];
	CVGStatic		m_st_Status[MAX_SITE_CNT];

	CButton			m_bn_OK;
	CButton			m_bn_Cancel;

	BOOL			m_bUse[MAX_SITE_CNT];		// 각 채널별 사용여부
	BOOL			m_bIsModal;					// 모달 다이얼로그 인가?
	BOOL			m_bChk_BarcodeLength;		// 바코드 길이를 체크 할 것인가?

	// 소켓 바코드를 사용할 경우
	BOOL			m_bUseSelectSocket		= FALSE;	// 소켓바코드 사용여부
	INT				m_iSelectedSocket		= -1;		// 선택된 소켓 인덱스
	CString			m_szBarcodeSocket[MAX_SITE_CNT];
	UINT			m_nInputedBarcodeCnt	= 0;		// 입력완료된 바코드 수
	UINT			m_bUseBarcodeCnt		= MAX_SITE_CNT;

	// 바코드가 정상인지 판별
	enBarcodeChk	CheckBarcode		(__in LPCTSTR szBarcode);
	enBarcodeChk	CheckBarcodeSocket	(__in UINT nSocket, __in LPCTSTR szBarcode);
	// 바코드 입력 상태 표시
	void			SetStatus			(__in UINT nIndex, __in enBarcodeChk nStatus);

public:

	virtual INT_PTR DoModal();

	// 바코드 사용 채널 수 설정
	BOOL			SetBarcodeCount		(__in UINT nCount);
	// 바코드 길이 설정
	BOOL			SetBarcodeLength	(__in UINT nLength, __in BOOL bUse = TRUE);

	// 입력된 바코드들 반환
	void			GetBarcode			(__in CStringArray& szarBarcodez);
	void			GetBarcode			(__in CString& szBarcodez);

	// 바코드 리더기로부터 수신된 바코드를 추가
	BOOL			InsertBarcode		(__in LPCTSTR szBarcode);
	BOOL			InsertBarcodeSocket	(__in UINT nSocket, __in LPCTSTR szBarcode);
	
	// 바코드 버퍼 초기화
	void			ResetBarcode		();

	// 채널(소켓)별 사용여부 설정
	void			SetUsableSite		(__in UINT nIdx, __in BOOL bUse);
	
	// 소켓 선택 바코드 사용?
	void			SetUseSelectSocket	(__in BOOL bUse);
	void			SetSelectSocket		(__in INT nSocket);
	INT				GetSelectedSocket	()
	{
		return m_iSelectedSocket;
	};



};

#endif // Dlg_Barcode_h__
