﻿#ifndef Def_T_Color_h__
#define Def_T_Color_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

typedef enum enROI_Color
{
	ROI_CL_R,
	ROI_CL_G,
	ROI_CL_B,
	ROI_CL_BL,
	ROI_CL_Max,
};

static LPCTSTR g_szRoiColor[] =
{
	_T("Red"),
	_T("Green"),
	_T("Blue"),
	_T("Black"),
	NULL
};

// ROI
typedef struct _tag_RegionColor
{
	CRect	rtRoi;

	int nMinSpcR;
	int nMaxSpcR;
	
	int nMinSpcG;
	int nMaxSpcG;
	
	int nMinSpcB;
	int nMaxSpcB;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp	= rtRoi;

		rtRoi.left		= iPosX - (int)(rtTemp.Width() / 2);
		rtRoi.right		= rtRoi.left + rtTemp.Width();;
		rtRoi.top		= iPosY - (int)(rtTemp.Height() / 2);
		rtRoi.bottom	= rtRoi.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left		= rtTemp.CenterPoint().x - (iWidth / 2);
		rtRoi.right		= rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtRoi.top		= rtTemp.CenterPoint().y - (iHeight / 2);
		rtRoi.bottom	= rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	void Reset()
	{
		rtRoi.SetRectEmpty();

		nMinSpcR = 0;
		nMaxSpcR = 0;

		nMinSpcG = 0;
		nMaxSpcG = 0;

		nMinSpcB = 0;
		nMaxSpcB = 0;
	};

	_tag_RegionColor()
	{
		Reset();
	};

	_tag_RegionColor& operator= (_tag_RegionColor& ref)
	{
		rtRoi = ref.rtRoi;

		nMinSpcR = ref.nMinSpcR;
		nMaxSpcR = ref.nMaxSpcR;

		nMinSpcG = ref.nMinSpcG;
		nMaxSpcG = ref.nMaxSpcG;

		nMinSpcB = ref.nMinSpcB;
		nMaxSpcB = ref.nMaxSpcB;

		return *this;
	};

}ST_RegionColor, *PST_RegionColor;

typedef struct _tag_Color_Opt
{
	ST_RegionColor	stRegionOp[ROI_CL_Max];
	ST_RegionColor	stTestRegionOp[ROI_CL_Max];
	int iSelectROI;

	int nSpecDev;

	_tag_Color_Opt()
	{
		for (UINT nIdx = 0; nIdx < ROI_CL_Max; nIdx++)
		{
			stRegionOp[nIdx].Reset();
			stTestRegionOp[nIdx].Reset();
		}

		iSelectROI = -1;
		nSpecDev = 5;
	};

	_tag_Color_Opt& operator= (_tag_Color_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < ROI_CL_Max; nIdx++)
		{
			stRegionOp[nIdx] = ref.stRegionOp[nIdx];
			stTestRegionOp[nIdx] = ref.stTestRegionOp[nIdx];
		}

		iSelectROI = ref.iSelectROI;
		nSpecDev = ref.nSpecDev;

		return *this;
	};

}ST_Color_Opt, *PST_Color_Opt;

typedef struct _tag_Color_Result
{
	// 최종 결과
	UINT nResult;
	UINT nEachResult[ROI_CL_Max];

	int iRed[ROI_CL_Max];
	int iGreen[ROI_CL_Max];
	int iBlue[ROI_CL_Max];

	CPoint ptCenter[ROI_CL_Max];

	int iSelectROI;

	void Reset()
	{
		nResult = 3;

		for (UINT nIdx = 0; nIdx < ROI_CL_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			iRed[nIdx]		= 0;
			iGreen[nIdx]	= 0;
			iBlue[nIdx]	= 0;
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;
		}

		iSelectROI = -1;
	};

	_tag_Color_Result()
	{
		Reset();
	};

	_tag_Color_Result& operator= (_tag_Color_Result& ref)
	{
		nResult = ref.nResult;

		for (UINT nIdx = 0; nIdx < ROI_CL_Max; nIdx++)
		{
			nEachResult[nIdx] = ref.nEachResult[nIdx];
			iRed[nIdx] = ref.iRed[nIdx];
			iGreen[nIdx]	= ref.iGreen[nIdx];
			iBlue[nIdx]		= ref.iBlue[nIdx];
			ptCenter[nIdx].x = ref.ptCenter[nIdx].x;
			ptCenter[nIdx].y = ref.ptCenter[nIdx].y;
		}

		iSelectROI = ref.iSelectROI;

		return *this;
	};

}ST_Color_Result, *PST_Color_Result;

#pragma pack(pop)

#endif // Def_Color_h__

