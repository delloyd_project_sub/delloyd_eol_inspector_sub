﻿#ifndef Def_CenterPoint_h__
#define Def_CenterPoint_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

enum enCenterPointMode
{
	CP_Mode_Measurement,			// 측정
	CP_Mode_Adjustment_Manual,		// 조정
	CP_Mode_Adjustment_Auto,		// 조정
	CP_Mode_MaxNum,
};

static LPCTSTR	g_szCenterPointMode[] =
{
	_T("Measuring"),
	_T("Adjustment Manual"),
	_T("Adjustment Auto"),
	NULL
};

typedef enum enCenterPointMarkColor
{
	CP_Mark_B = 0,
	CP_Mark_W,
	CP_Mark_Max,
};

static LPCTSTR	g_szCenterPointMarkColor[] =
{
	_T("●"),
	_T("○"),
	NULL
};

typedef enum enCenterPointResultRow
{
	CP_Axis_X = 0,
	CP_Axis_Y,
	CP_Offset_X,
	CP_Offset_Y,
	CP_Axis_Max,
};

static LPCTSTR	g_szCenterPointResultRow[] =
{
	_T("Standard X"),
	_T("Standard Y"),
	_T("Offset X"),
	_T("Offset Y"),
	NULL
};

typedef struct _tag_CenterPoint_Opt
{
	CRect rtRoi;

	// 검사 모드
	UINT nTestMode;

	// 검사 모드
	UINT nMarkColor;

	// 기준 광축
	UINT nRefAxisX;
	UINT nRefAxisY;

	// Offset Deviation X, Y
	int iDevOffsetX;
	int iDevOffsetY;

	// Offset X, Y
	int iOffsetX;
	int iOffsetY;

	// 조정 Target
	int iTargetX;
	int iTargetY;

	// 조정 Count
	UINT nTryCnt;

	int iSelectROI;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp	= rtRoi;

		rtRoi.left		= iPosX - (int)(rtTemp.Width() / 2);
		rtRoi.right		= rtRoi.left + rtTemp.Width();;
		rtRoi.top		= iPosY - (int)(rtTemp.Height() / 2);
		rtRoi.bottom	= rtRoi.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left		= rtTemp.CenterPoint().x - (iWidth / 2);
		rtRoi.right		= rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtRoi.top		= rtTemp.CenterPoint().y - (iHeight / 2);
		rtRoi.bottom	= rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	_tag_CenterPoint_Opt()
	{
		rtRoi.SetRectEmpty();

		nTestMode = CP_Mode_Measurement;
		nMarkColor = CP_Mark_B;

		nRefAxisX = 0;
		nRefAxisY = 0;

		iOffsetX  = 0;
		iOffsetY  = 0;

		iDevOffsetX = 0;
		iDevOffsetY = 0;

		iTargetX  = 0;
		iTargetY  = 0;

		nTryCnt   = 1;

		iSelectROI = -1;
	};

	_tag_CenterPoint_Opt& operator= (_tag_CenterPoint_Opt& ref)
	{
		rtRoi	  = ref.rtRoi;

		nTestMode = ref.nTestMode;
		nMarkColor = ref.nMarkColor;
		
		nRefAxisX = ref.nRefAxisX;
		nRefAxisY = ref.nRefAxisY;
		
		iOffsetX  = ref.iOffsetX;
		iOffsetY  = ref.iOffsetY;

		iDevOffsetX = ref.iDevOffsetX;
		iDevOffsetY = ref.iDevOffsetY;

		iTargetX = ref.iTargetX;
		iTargetY = ref.iTargetY;

		nTryCnt = ref.nTryCnt;
		iSelectROI = ref.iSelectROI;
		
		return *this;
	};

}ST_CenterPoint_Opt, *PST_CenterPoint_Opt;

typedef struct _tag_CenterPoint_Result
{
	// 결과
	UINT nResult;

	UINT nResultX;
	UINT nResultY;

	//  pic 결과 광축
	int iPicValueX;
	int iPicValueY;


	// 결과 광축
	int iValueX;
	int iValueY;

	// 결과 광축
	int iResultOffsetX;
	int iResultOffsetY;

	// 결과 광축
	int iWriteX;
	int iWriteY;

	int iSelectROI;

	void Reset()
	{
		nResult = 3;

		nResultX = TR_Fail;
		nResultY = TR_Fail;

		iValueX = -1;
		iValueY = -1;
		iSelectROI = -1;

		iResultOffsetX = 0;
		iResultOffsetY = 0;

		iWriteX = 0;
		iWriteY = 0;

		iPicValueX = -1;
		iPicValueY = -1;
	};

	// 변수 초기화 함수
	_tag_CenterPoint_Result()
	{
		Reset();
	};

	/*변수 교환 함수*/
	_tag_CenterPoint_Result& operator= (_tag_CenterPoint_Result& ref)
	{
		nResult  = ref.nResult;

		nResultX = ref.nResultX;
		nResultY = ref.nResultY;

		iResultOffsetX = ref.iResultOffsetX;
		iResultOffsetY = ref.iResultOffsetY;

		iValueX  = ref.iValueX;
		iValueY = ref.iValueY;
		iSelectROI = ref.iSelectROI;

		iWriteX = ref.iWriteX;
		iWriteY = ref.iWriteY;
		iPicValueX = ref.iPicValueX;
		iPicValueY = ref.iPicValueY;
		return *this;
	};

}ST_CenterPoint_Result, *PST_CenterPoint_Result;

#pragma pack(pop)

#endif // Def_CenterPoint_h__