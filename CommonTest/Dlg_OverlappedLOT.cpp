﻿//*****************************************************************************
// Filename	: 	Dlg_OverlappedLOT.cpp
// Created	:	2017/1/9 - 15:31
// Modified	:	2017/1/9 - 15:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Dlg_OverlappedLOT.cpp : implementation file
//

#include "stdafx.h"
#include "Dlg_OverlappedLOT.h"
#include "afxdialogex.h"


#define	IDC_BN_RENAME		1001
#define	IDC_BN_NEW			1002
#define	IDC_BN_CONTINUE		1003

static LPCTSTR g_szOverlappedLOT[] =
{
	_T("  I will reset it to a different name."),
	_T("  I delete the existing folder and start from the beginning again."),
	_T("  I will continue my work."),
};

// CDlg_OverlappedLOT dialog

IMPLEMENT_DYNAMIC(CDlg_OverlappedLOT, CDialogEx)

CDlg_OverlappedLOT::CDlg_OverlappedLOT(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_OverlappedLOT::IDD, pParent)
{
	m_iReturnCode = -1;
}

CDlg_OverlappedLOT::~CDlg_OverlappedLOT()
{
}

void CDlg_OverlappedLOT::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlg_OverlappedLOT, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BN_RENAME,	OnBnClickedBnRename)
	ON_BN_CLICKED(IDC_BN_NEW,		OnBnClickedBnNew)
	ON_BN_CLICKED(IDC_BN_CONTINUE,	OnBnClickedBnContinue)
END_MESSAGE_MAP()


// CDlg_OverlappedLOT message handlers
//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2017/1/9 - 17:10
// Desc.		:
//=============================================================================
BOOL CDlg_OverlappedLOT::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		//OnOK();
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/9 - 17:10
// Desc.		:
//=============================================================================
int CDlg_OverlappedLOT::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	m_st_Message.SetBackColor_COLORREF(RGB(0, 0, 0));
	m_st_Message.SetTextColor(Color::White, Color::White);
	m_st_Message.SetFont_Gdip(L"arial", 16.0F);

	m_st_RenameMsg.SetTextAlignment (StringAlignment::StringAlignmentNear);
	m_st_NewMsg.SetTextAlignment (StringAlignment::StringAlignmentNear);
	m_st_ContinueMsg.SetTextAlignment (StringAlignment::StringAlignmentNear);

	m_st_Message.Create(_T("I have a LOT folder with the same name."),	dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_RenameMsg.Create(g_szOverlappedLOT[LotReturnCode::Rename],		dwStyle | SS_LEFT | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_NewMsg.Create(g_szOverlappedLOT[LotReturnCode::DeleteAndNew],	dwStyle | SS_LEFT | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_ContinueMsg.Create(g_szOverlappedLOT[LotReturnCode::Continue], dwStyle | SS_LEFT | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_bn_RenameMsg.Create(_T("Rename"),		dwStyle | WS_TABSTOP, rectDummy, this, IDC_BN_RENAME);
	m_bn_NewMsg.Create(_T("Delete && New"), dwStyle | WS_TABSTOP, rectDummy, this, IDC_BN_NEW);
	m_bn_ContinueMsg.Create(_T("Continue"), dwStyle | WS_TABSTOP, rectDummy, this, IDC_BN_CONTINUE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/9 - 17:10
// Desc.		:
//=============================================================================
void CDlg_OverlappedLOT::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;
	int iCtrlWidth	= 150;
	int iCtrlHeight = 50;
	int iSubLeft	= cx - iMagrin - iCtrlWidth;
	int iTempWidth	= iWidth - iSpacing - iCtrlWidth;

	m_st_Message.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);

	iTop = cy - iMagrin - (iCtrlHeight * 3) - (iSpacing * 2);
	
	m_st_RenameMsg.MoveWindow(iLeft, iTop, iTempWidth, iCtrlHeight);
	m_bn_RenameMsg.MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_st_NewMsg.MoveWindow(iLeft, iTop, iTempWidth, iCtrlHeight);
	m_bn_NewMsg.MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_st_ContinueMsg.MoveWindow(iLeft, iTop, iTempWidth, iCtrlHeight);
	m_bn_ContinueMsg.MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);
}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2017/1/11 - 11:51
// Desc.		:
//=============================================================================
void CDlg_OverlappedLOT::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 620;
	lpMMI->ptMaxTrackSize.y = 320;

	lpMMI->ptMinTrackSize.x = 620;
	lpMMI->ptMinTrackSize.y = 320;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: OnInitDialog
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/1/10 - 10:55
// Desc.		:
//=============================================================================
BOOL CDlg_OverlappedLOT::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	//return FALSE;
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/11 - 13:00
// Desc.		:
//=============================================================================
BOOL CDlg_OverlappedLOT::PreCreateWindow(CREATESTRUCT& cs)
{
	ModifyStyle(0, WS_SIZEBOX);

	return CDialogEx::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnBnClickedBnNew
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/9 - 17:10
// Desc.		:
//=============================================================================
void CDlg_OverlappedLOT::OnBnClickedBnNew()
{
	m_iReturnCode = LotReturnCode::DeleteAndNew;

	CDialogEx::OnOK();
}

//=============================================================================
// Method		: OnBnClickedBnRename
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/9 - 17:10
// Desc.		:
//=============================================================================
void CDlg_OverlappedLOT::OnBnClickedBnRename()
{

	m_iReturnCode = LotReturnCode::Rename;
	CDialogEx::OnOK();
}

//=============================================================================
// Method		: OnBnClickedBnContinue
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/9 - 17:11
// Desc.		:
//=============================================================================
void CDlg_OverlappedLOT::OnBnClickedBnContinue()
{
	m_iReturnCode = LotReturnCode::Continue;

	CDialogEx::OnOK();
}
