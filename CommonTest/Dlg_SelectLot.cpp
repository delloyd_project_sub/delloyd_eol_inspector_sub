﻿//*****************************************************************************
// Filename	: 	Dlg_SelectLot.cpp
// Created	:	2017/1/12 - 11:02
// Modified	:	2017/1/12 - 11:02
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Dlg_SelectLot.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_SelectLot.h"
#include "afxdialogex.h"
#include "Dlg_OverlappedLOT.h"
#include "CommonFunction.h"

#define IDC_ED_LOT			1001
//#define IDC_ED_OPERATOR		1002
#define IDC_CB_OPERATOR		1002
#define IDC_BN_LOT_SAVE		1003
#define IDC_BN_LOT_CANCEL	1004

 #define		TABSTYLE_COUNT			4
 static UINT g_TabOrder[TABSTYLE_COUNT] =
 {
 	1001, 1002, 1003, 1004
 };

// CDlg_SelectLot 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_SelectLot, CDialogEx)

CDlg_SelectLot::CDlg_SelectLot(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_SelectLot::IDD, pParent)
{
	VERIFY(m_font_Default.CreateFont(
		34,					// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Btn.CreateFont(
		25,					// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_bLoadPrevLot = FALSE;
}

CDlg_SelectLot::~CDlg_SelectLot()
{
	m_font_Default.DeleteObject();
	m_font_Btn.DeleteObject();

}

void CDlg_SelectLot::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDlg_SelectLot, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BN_LOT_SAVE,	 &CDlg_SelectLot::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BN_LOT_CANCEL, &CDlg_SelectLot::OnBnClickedButtonCancel)
END_MESSAGE_MAP()


// CDlg_SelectLot 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 9:59
// Desc.		:
//=============================================================================
int CDlg_SelectLot::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_st_Title.SetFont_Gdip(L"Arial", 10.0f, Gdiplus::FontStyleRegular);
	m_st_Title.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Title.SetTextAlignment(StringAlignmentCenter);
	m_st_Title.Create(_T("LOT Start Dialog"), WS_VISIBLE | WS_CHILD | SS_CENTER | SS_CENTERIMAGE, CRect(0, 0, 0, 0), this, 1);

	m_st_LotName.SetFont_Gdip(L"Arial", 10.0f, Gdiplus::FontStyleRegular);
	m_st_LotName.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_LotName.SetTextAlignment(StringAlignmentCenter);
	m_st_LotName.Create(_T("LOT ID"), WS_VISIBLE | WS_CHILD | SS_CENTER | SS_CENTERIMAGE, CRect(0, 0, 0, 0), this, 2);

	m_st_Operator.SetFont_Gdip(L"Arial", 10.0f, Gdiplus::FontStyleRegular);
	m_st_Operator.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Operator.SetTextAlignment(StringAlignmentCenter);
	m_st_Operator.Create(_T("Operator"), WS_VISIBLE | WS_CHILD | SS_CENTER | SS_CENTERIMAGE, CRect(0, 0, 0, 0), this, 3);

	m_ed_LotName.Create(WS_VISIBLE | WS_BORDER | ES_CENTER| WS_TABSTOP , CRect(0, 0, 0, 0), this, IDC_ED_LOT);
	m_ed_LotName.SetFont(&m_font_Default);
	m_ed_LotName.SetWindowText(_T(""));

	m_ed_Operator.Create(WS_VISIBLE | WS_BORDER | ES_CENTER | WS_TABSTOP, CRect(0, 0, 0, 0), this, IDC_ED_LOT);
	m_ed_Operator.SetFont(&m_font_Default);
	m_ed_Operator.SetWindowText(_T(""));

	/*
	m_cb_Operator.Create(WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_CB_OPERATOR);
	m_cb_Operator.SetFont(&m_font_Default);

	for (int i = 0; i < m_pszArrOperator->GetCount(); i++)
		m_cb_Operator.AddString(m_pszArrOperator->GetAt(i));

	int iSel = m_cb_Operator.FindStringExact(0, m_szOperatorName);

	if (0 <= iSel)
	{
		m_cb_Operator.SetCurSel(iSel);
	}
	else
	{
		m_cb_Operator.SetCurSel(0);
	}*/

	m_bn_Save.Create(_T("OK"), WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_PUSHLIKE, CRect(0, 0, 0, 0), this, IDC_BN_LOT_SAVE);
	m_bn_Save.SetFont(&m_font_Btn);
	m_bn_Cancel.Create(_T("Cancel"), WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_PUSHLIKE , CRect(0, 0, 0, 0), this, IDC_BN_LOT_CANCEL);
	m_bn_Cancel.SetFont(&m_font_Btn);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 9:59
// Desc.		:
//=============================================================================
void CDlg_SelectLot::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	int iMargin		= 10;
	int iSpacing	= 5;
	int iCateSpacing= 10;
	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;
	int iCtrlHeight	= 42;
	int iCtrlWidth	= 100;
	int iTempWidth	= iWidth - iCtrlWidth;
	int iSubLeft	= iLeft + iCtrlWidth;

	m_st_Title.MoveWindow(iMargin, iTop, iWidth, iCtrlHeight);

	iTop += iCtrlHeight + iCateSpacing;
	m_st_LotName.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	m_ed_LotName.MoveWindow(iSubLeft, iTop, iTempWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_st_Operator.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	m_ed_Operator.MoveWindow(iSubLeft, iTop, iTempWidth, iCtrlHeight);
	//m_cb_Operator.MoveWindow(iSubLeft, iTop, iTempWidth, iCtrlHeight);

	iLeft = iMargin;
	iTop += iCtrlHeight + iMargin;
	iTempWidth = (iWidth - iCateSpacing) / 2;
	m_bn_Save.MoveWindow(iLeft, iTop, iTempWidth, iCtrlHeight);
	iLeft += iTempWidth + iCateSpacing;
	m_bn_Cancel.MoveWindow(iLeft, iTop, iTempWidth, iCtrlHeight);
}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2017/1/12 - 9:59
// Desc.		:
//=============================================================================
void CDlg_SelectLot::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 520;
	lpMMI->ptMaxTrackSize.y = 360;

	lpMMI->ptMinTrackSize.x = 520;
	lpMMI->ptMinTrackSize.y = 360;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: OnInitDialog
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/1/12 - 9:59
// Desc.		:
//=============================================================================
BOOL CDlg_SelectLot::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_ed_LotName.SetWindowText(m_szLotName);
	

	//GotoDlgCtrl(&m_ed_LotName);

	if ((FALSE == m_szLotName.IsEmpty()) && (m_szOperatorName.IsEmpty()))
	{
		//m_cb_Operator.SetFocus();
		m_ed_Operator.SetFocus();
	}
	else
	{
		m_ed_LotName.SetFocus();
	}

	//return TRUE;  // return TRUE unless you set the focus to a control
	return FALSE;
	// EXCEPTION: OCX Property Pages should return FALSE
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 9:59
// Desc.		:
//=============================================================================
BOOL CDlg_SelectLot::PreCreateWindow(CREATESTRUCT& cs)
{
	ModifyStyle(0, WS_SIZEBOX);

	return CDialogEx::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2017/1/12 - 9:59
// Desc.		:
//=============================================================================
BOOL CDlg_SelectLot::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if (VK_TAB == pMsg->wParam)
		{
			UINT nID = GetFocus()->GetDlgCtrlID();
			for (int iCnt = 0; iCnt < TABSTYLE_COUNT; iCnt++)
			{
				if (nID == g_TabOrder[iCnt])
				{
					if ((TABSTYLE_COUNT - 1) == iCnt)
					{
						nID = g_TabOrder[0];
					}
					else
					{
						nID = g_TabOrder[iCnt + 1];
					}

					break;
				}
			}
			//!SH _190124: Tab error
			//GetDlgItem(nID)->SetFocus();
		}
 		else if (pMsg->wParam == VK_RETURN)
 		{
			UINT nID = GetFocus()->GetDlgCtrlID();
			if ((IDOK != nID) && (IDCANCEL != nID))
			{
				for (int iCnt = 0; iCnt < TABSTYLE_COUNT; iCnt++)
				{
					if (nID == g_TabOrder[iCnt])
					{
						if ((TABSTYLE_COUNT - 1) == iCnt)
						{
							nID = g_TabOrder[0];
						}
						else
						{
							nID = g_TabOrder[iCnt + 1];
						}

						break;
					}
				}

				GetDlgItem(nID)->SetFocus();
			}			
 			return TRUE;
 		}
 		else if (pMsg->wParam == VK_ESCAPE)
 		{
 			// 여기에 ESC키 기능 작성       
 			return TRUE;
 		}
		break;

	default:
		break;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnBnClickedButtonSave
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 9:59
// Desc.		:
//=============================================================================
void CDlg_SelectLot::OnBnClickedButtonSave()
{
	CheckSave();	
}

//=============================================================================
// Method		: OnBnClickedButtonCancel
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 9:59
// Desc.		:
//=============================================================================
void CDlg_SelectLot::OnBnClickedButtonCancel()
{
	CDialogEx::OnCancel();
}

//=============================================================================
// Method		: CheckLOT
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/1/12 - 11:02
// Desc.		:
//=============================================================================
BOOL CDlg_SelectLot::CheckLOT()
{
	// LOT 폴더 체크?
	// 날짜/리포터 폴더/모델명/LOT 이름/....

	CString strPath;
	CString strTime;
	SYSTEMTIME lcTime;
	GetLocalTime(&lcTime);

	strTime.Format(_T("%04d_%02d_%02d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay);
	strPath.Format(_T("%s%s\\%s\\%s\\"), m_szPath, strTime, m_szModel, m_szLotName);

	if (PathFileExists(strPath))
	{
		CDlg_OverlappedLOT dlgLotOver;

		if (IDOK == dlgLotOver.DoModal())
		{
			switch (dlgLotOver.GetReturnCode())
			{
			case CDlg_OverlappedLOT::Rename:
			{
				m_ed_LotName.SetFocus();
				m_ed_LotName.SetSel(0, -1);
				return FALSE;
			}
			break;

			case CDlg_OverlappedLOT::DeleteAndNew:
			{
				// 기존 폴더 삭제
				DeleteAllFiles(strPath, 1);
			}
			break;

			case CDlg_OverlappedLOT::Continue:
			{
				// 기존 LOT 정보 불러오기
				//GetParent()->SendNotifyMessage(WM_LOAD_LOT, 0, 0);
				m_bLoadPrevLot = TRUE;
			}
			break;

			default:
				break;
			}
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckSave
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/6 - 11:35
// Desc.		:
//=============================================================================
void CDlg_SelectLot::CheckSave()
{
	CString szText;

	m_ed_LotName.GetWindowText(szText);
	m_szLotName = szText;

	//m_cb_Operator.GetWindowText(szText);
	m_ed_Operator.GetWindowText(szText);
	m_szOperatorName = szText;

	if (m_szLotName.IsEmpty())
	{
		AfxMessageBox(_T("Input Lot Name!!"));
		m_ed_LotName.SetFocus();
		return;
	}

	if (m_szOperatorName.IsEmpty())
	{
		AfxMessageBox(_T("Input Operator Name!!"));
		m_ed_Operator.SetFocus();
		//m_cb_Operator.SetFocus();
		return;
	}

	if (CheckLOT())
	{
		CDialogEx::OnOK();
	}
}

//=============================================================================
// Method		: SetLotName
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szLotName
// Qualifier	:
// Last Update	: 2017/1/12 - 10:00
// Desc.		:
//=============================================================================
void CDlg_SelectLot::SetLotName(__in LPCTSTR szLotName)
{
	m_szLotName = szLotName;
}

//=============================================================================
// Method		: SetOperator
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szOperator
// Qualifier	:
// Last Update	: 2017/1/12 - 10:00
// Desc.		:
//=============================================================================
void CDlg_SelectLot::SetOperator(__in LPCTSTR szOperator)
{
	m_szOperatorName = szOperator;
}


