﻿#ifndef Def_PatternNoise_h__
#define Def_PatternNoise_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

typedef enum enROI_PatternNoise
{
	ROI_PN_S1,
	ROI_PN_S2,
	ROI_PN_S3,
	ROI_PN_S4,
	ROI_PN_S5,
	ROI_PN_S6,
	ROI_PN_S7,
	ROI_PN_S8,
	ROI_PN_S9,
	ROI_PN_MaxEnum,
};

static LPCTSTR g_szRegionPatternNoise[] =
{
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),
	_T("S5"),
	_T("S6"),
	_T("S7"),
	_T("S8"),
	_T("S9"),

	NULL
};

// ROI
typedef struct _tag_RegionPatternNoise
{
	// ROI 영역 ( 카메라 [Send] )
	CRect	rtRoi;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left = iPosX - (int)(rtTemp.Width() / 2);
		rtRoi.right = rtRoi.left + rtTemp.Width();
		rtRoi.top = iPosY - (int)(rtTemp.Height() / 2);
		rtRoi.bottom = rtRoi.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left = rtTemp.CenterPoint().x - (iWidth / 2);
		rtRoi.right = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtRoi.top = rtTemp.CenterPoint().y - (iHeight / 2);
		rtRoi.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	void Reset()
	{
		rtRoi.SetRectEmpty();

		
	};

	_tag_RegionPatternNoise()
	{
		Reset();
	};

	_tag_RegionPatternNoise& operator= (_tag_RegionPatternNoise& ref)
	{
		rtRoi = ref.rtRoi;
		

		return *this;
	};

}ST_RegionPatternNoise, *PST_RegionPatternNoise;


typedef struct _tag_PatternNoise_Op
{
	ST_RegionPatternNoise	stRegionOp[ROI_PN_MaxEnum];
	int iSelectROI;
	double dThr_dB;
	double dThr_Env;

	_tag_PatternNoise_Op()
	{
		for (UINT nIdx = 0; nIdx < ROI_PN_MaxEnum; nIdx++)
		{
			stRegionOp[nIdx].Reset();
		}

		iSelectROI = -1;
		dThr_dB = 0;
		dThr_Env = 0;
	};

	_tag_PatternNoise_Op& operator= (_tag_PatternNoise_Op& ref)
	{
		for (UINT nIdx = 0; nIdx < ROI_PN_MaxEnum; nIdx++)
		{
			stRegionOp[nIdx] = ref.stRegionOp[nIdx];
		}

		iSelectROI = ref.iSelectROI;
		dThr_dB = ref.dThr_dB;
		dThr_Env = ref.dThr_Env;

		return *this;
	};

}ST_PatternNoise_Opt, *PST_PatternNoise_Opt;

typedef struct _tag_PatternNoise_Data
{
	// 최종 결과
	UINT nResult;

	UINT nEachResult[ROI_PN_MaxEnum];
	double dValue_dB[ROI_PN_MaxEnum];
	double dValue_Env[ROI_PN_MaxEnum];

	int iTotalCnt;
	int iPassCnt;
	int iSelectROI;

	void Reset()
	{
		nResult = 3;
		iSelectROI = -1;
		iTotalCnt = 0;
		iPassCnt = 0;

		for (UINT nIdx = 0; nIdx < ROI_PN_MaxEnum; nIdx++)
		{
			nEachResult[nIdx] = 2;
			dValue_dB[nIdx] = 0.0;
			dValue_Env[nIdx] = 0.0;
		}
	};

	_tag_PatternNoise_Data()
	{
		Reset();
	};

	_tag_PatternNoise_Data& operator= (_tag_PatternNoise_Data& ref)
	{
		nResult = ref.nResult;
		iSelectROI = ref.iSelectROI;
		iTotalCnt = ref.iTotalCnt;
		iPassCnt = ref.iPassCnt;

		for (UINT nIdx = 0; nIdx < ROI_PN_MaxEnum; nIdx++)
		{
			nEachResult[nIdx] = ref.nEachResult[nIdx];
			dValue_dB[nIdx] = ref.dValue_dB[nIdx];
			dValue_Env[nIdx] = ref.dValue_Env[nIdx];
		}

		return *this;
	};

}ST_PatternNoise_Result, *PST_PatternNoise_Result;


#endif // Def_PatternNoise_h__