﻿#ifndef Def_T_Angle_h__
#define Def_T_Angle_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

typedef enum enAngle_MarkColor
{
	AL_Mark_B = 0,
	AL_Mark_W,
	AL_Mark_Max,
};

static LPCTSTR	g_szAngleMarkColor[] =
{
	_T("●"),
	_T("○"),
	NULL
};

typedef enum enIDX_Angle
{
	IDX_AL_LC,
	IDX_AL_RC,
	IDX_AL_TC,
	IDX_AL_BC,
	IDX_AL_Max,
};

static LPCTSTR g_szIDXAngle[] =
{
	_T("L ↔ C"),
	_T("R ↔ C"),
	_T("T ↔ C"),
	_T("B ↔ C"),
	NULL
};

typedef enum enROI_Angle
{
	ROI_AL_Center,
	ROI_AL_Left,
	ROI_AL_Right,
	ROI_AL_Top,
	ROI_AL_Bottom,
	ROI_AL_Max,
};

static LPCTSTR g_szRoiAngle[] =
{
	_T("Center"),
	_T("Left"),
	_T("Right"),
	_T("Top"),
	_T("Bottom"),
	NULL
};

// ROI
typedef struct _tag_RegionAngle
{
	// ROI 영역 ( 카메라 [Send] )
	CRect	rtRoi;
	UINT	nMarkColor;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left	 = iPosX - (int)(rtTemp.Width() / 2);
		rtRoi.right  = rtRoi.left + rtTemp.Width();;
		rtRoi.top	 = iPosY - (int)(rtTemp.Height() / 2);
		rtRoi.bottom = rtRoi.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left   = rtTemp.CenterPoint().x - (iWidth / 2);
		rtRoi.right  = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtRoi.top	 = rtTemp.CenterPoint().y - (iHeight / 2);
		rtRoi.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	void Reset()
	{
		nMarkColor = AL_Mark_B;

		rtRoi.SetRectEmpty();
	};

	_tag_RegionAngle()
	{
		Reset();
	};

	_tag_RegionAngle& operator= (_tag_RegionAngle& ref)
	{
		rtRoi		= ref.rtRoi;
		nMarkColor	= ref.nMarkColor;

		return *this;
	};

}ST_RegionAngle, *PST_RegionAngle;

typedef struct _tag_Angle_Opt
{
	ST_RegionAngle	stRegionOp[ROI_AL_Max];
	ST_RegionAngle	stTestRegionOp[ROI_AL_Max];

	UINT nMinSpc[IDX_AL_Max];
	UINT nMaxSpc[IDX_AL_Max];

	UINT nSpecDev;

	int iSelectROI;

	_tag_Angle_Opt()
	{
		for (UINT nIdx = 0; nIdx < IDX_AL_Max; nIdx++)
		{
			nMinSpc[nIdx] = 0;
			nMaxSpc[nIdx] = 0;
		}

		for (UINT nIdx = 0; nIdx < ROI_AL_Max; nIdx++)
		{
			stRegionOp[nIdx].Reset();
			stTestRegionOp[nIdx].Reset();
		}

		iSelectROI = -1;
		nSpecDev = 5;
	};

	_tag_Angle_Opt& operator= (_tag_Angle_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < IDX_AL_Max; nIdx++)
		{
			nMinSpc[nIdx] = ref.nMinSpc[nIdx];
			nMaxSpc[nIdx] = ref.nMaxSpc[nIdx];
		}

		for (UINT nIdx = 0; nIdx < ROI_AL_Max; nIdx++)
		{
			stRegionOp[nIdx] = ref.stRegionOp[nIdx];
			stTestRegionOp[nIdx] = ref.stTestRegionOp[nIdx];
		}

		iSelectROI = ref.iSelectROI;
		nSpecDev = ref.nSpecDev;

		return *this;
	};

}ST_Angle_Opt, *PST_Angle_Opt;

typedef struct _tag_Angle_Result
{
	// 최종 결과
	UINT nResult;
	UINT nEachResult[IDX_AL_Max];

	double dValue[IDX_AL_Max];

	CPoint ptCenter[ROI_AL_Max];

	int iSelectROI;

	_tag_Angle_Result()
	{
		nResult = 3;
		
		for (UINT nIdx = 0; nIdx < ROI_AL_Max; nIdx++)
		{
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;
		}

		for (UINT nIdx = 0; nIdx < IDX_AL_Max; nIdx++)
		{
			nEachResult[nIdx]	= TR_Pass;
			dValue[nIdx] = 0;
		}

		iSelectROI = -1;
	};

	void Reset()
	{
		nResult = 3;

		for (UINT nIdx = 0; nIdx < ROI_AL_Max; nIdx++)
		{
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;
		}

		for (UINT nIdx = 0; nIdx < IDX_AL_Max; nIdx++)
		{
			nEachResult[nIdx]	= TR_Pass;
			dValue[nIdx] = 0;
		}

		iSelectROI = -1;
	};

	_tag_Angle_Result& operator= (_tag_Angle_Result& ref)
	{
		nResult	 = ref.nResult;

		for (UINT nIdx = 0; nIdx < ROI_AL_Max; nIdx++)
		{
			ptCenter[nIdx].x = ref.ptCenter[nIdx].x;
			ptCenter[nIdx].y = ref.ptCenter[nIdx].y;
		}

		for (UINT nIdx = 0; nIdx < ROI_AL_Max; nIdx++)
		{
			nEachResult[nIdx]   = ref.nEachResult[nIdx];
			dValue[nIdx] = ref.dValue[nIdx];
		}

		iSelectROI = ref.iSelectROI;

		return *this;
	};

}ST_Angle_Result, *PST_Angle_Result;

#pragma pack(pop)

#endif // Def_Angle_h__

