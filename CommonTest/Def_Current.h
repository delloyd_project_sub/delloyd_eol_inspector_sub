﻿#ifndef Def_Current_h__
#define Def_Current_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)
#define Def_CamBrd_Channel	2


typedef enum enCurrent_Channel
{
	CurrentCh1,
	CurrentCh2,
	CurrentCh_Max,
};

static LPCTSTR g_szCurrent_Channel[] =
{
	_T("Camera"),
	_T("Channel 2"),
	NULL
};


typedef struct _tag_Current_Opt
{
	// 양불 기준
	double nSpecMin[Def_CamBrd_Channel];
	double nSpecMax[Def_CamBrd_Channel];

	double dbOffset[Def_CamBrd_Channel];

	// 변수 초기화 함수
	_tag_Current_Opt()
	{
		for (int i = 0; i < Def_CamBrd_Channel; i++)
		{
			nSpecMin[i] = 0;
			nSpecMax[i] = 1;
			dbOffset[i] = 1.0;
		}
		
	};

	// 변수 교환 함수
	_tag_Current_Opt& operator= (_tag_Current_Opt& ref)
	{
		for (int i = 0; i < Def_CamBrd_Channel; i++)
		{
			nSpecMin[i] = ref.nSpecMin[i];
			nSpecMax[i] = ref.nSpecMax[i];
			dbOffset[i] = ref.dbOffset[i];
		}

		return *this;
	};

}ST_Current_Opt, *PST_Current_Opt;

typedef struct _tag_Current_Result
{
	// 결과
	UINT	nResult;
	UINT	nEachResult[Def_CamBrd_Channel];

	UINT	nResetCount;

	// 전류 값
	double 	iValue[Def_CamBrd_Channel];

	void Reset()
	{
		nResetCount = 0;
		nResult = 3;

		for (int iCh = 0; iCh < Def_CamBrd_Channel; iCh++)
		{
			iValue[iCh] = 0;
			nEachResult[iCh] = 0;
		}

	};

	// 변수 초기화 함수
	_tag_Current_Result()
	{
		Reset();
	};

	// 변수 교환 함수
	_tag_Current_Result& operator= (_tag_Current_Result& ref)
	{
		nResetCount = ref.nResetCount;
		nResult = ref.nResult;

		for (int iCh = 0; iCh < Def_CamBrd_Channel; iCh++)
		{
			iValue[iCh] = ref.iValue[iCh];
			nEachResult[iCh] = ref.nEachResult[iCh];
		}

		return *this;
	};

}ST_Current_Result, *PST_Current_Result;

#pragma pack(pop)

#endif // Def_Current_h__