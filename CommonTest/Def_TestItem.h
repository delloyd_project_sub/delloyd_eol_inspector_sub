﻿//*****************************************************************************
// Filename	: 	Def_TestItem.h
// Created	:	2016/12/30 - 12:31
// Modified	:	2016/12/30 - 12:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_TestItem_h__
#define Def_TestItem_h__

#pragma once

#include <afxwin.h>
#include "Def_Enum_Cm.h"
#include "Def_Angle.h"
#include "Def_CenterPoint.h"
#include "Def_Color.h"
#include "Def_Current.h"
#include "Def_Particle.h"
#include "Def_Rotate.h"
#include "Def_SFR.h"
#include "Def_Brightness.h"
#include "Def_FFT.h"
#include "Def_EIAJ.h"
#include "Def_Reverse.h"
#include "Def_PatternNoise.h"
#include "Def_IRFilter.h"
#include "Def_DefectPixel.h"


// 검사 항목 ID 

typedef enum enLT_TestItem_Count
{
	TICnt_Initial			= 1,	
	TICnt_Final				= 1,	

	TICnt_Current			= 1,	
	//TICnt_CenterPoint		= 2,	
	TICnt_CenterPoint = 1,
	TICnt_Rotation			= 1,	
	TICnt_Color				= 1,	
	TICnt_Reverse			= 1,
	//TICnt_SFR				= 1,	
	TICnt_BlackSpot			= 1,	
	TICnt_DefectPixel = 1,
	//TICnt_FFT				= 1,	

	TICnt_EIAJ				= 1,
	TICnt_FOV				= 1,

	TICnt_ParticleManual	= 1,
	//TICnt_BlackSpot = 1,
	//TICnt_PatternNoise		= 1,
	TICnt_Brightness		= 1,
	TICnt_IRFilter			= 1,
	//TICnt_IRFilterManual	= 1,

	//TICnt_LEDTest			= 1,
	//TICnt_VideoSignal		= 1,
	//TICnt_Reset				= 1,
	//TICnt_OperationMode		= 1,

};

typedef enum enLT_TestItem_ID
{
	TIID_TestInitialize,
	TIID_TestFinalize,

	TIID_Current,
	TIID_CenterPoint,
	TIID_Rotation,
	TIID_Color,
	TIID_Reverse,
	TIID_EIAJ,
	TIID_FOV, //FOV

	TIID_ParticleManual,
	TIID_BlackSpot,
	TIID_DefectPixel,
	//TIID_ParticleManual,

	TIID_Brightness,
	TIID_IRFilter,
	TIID_MaxEnum
};
//typedef enum enLT_TestItem_ID
//{
//	TIID_TestInitialize,								
//	TIID_TestFinalize,			
//
//	TIID_Current,
//	TIID_OperationMode,
//  	TIID_CenterPoint,									
//  	TIID_Rotation,										
// 	TIID_SFR,
//	TIID_EIAJ,
//  	TIID_Angle,											
//	TIID_Color,											
//	TIID_Reverse,
// 	TIID_ParticleManual,
//	TIID_LEDTest,
//
//	TIID_Brightness,
//	TIID_IRFilter,
//	TIID_PatternNoise,
//
////	TIID_EIAJ,
//
//// 	TIID_Particle,
//// 	TIID_FFT,
//// 	TIID_IRFilter,
//// TIID_VideoSignal,
//// TIID_Reset,
//
//	TIID_MaxEnum
//}; 

static UINT g_nLT_TestItemCount[TIID_MaxEnum] =
{
	TICnt_Initial,
	TICnt_Final,
	
	TICnt_Current,
	//TICnt_OperationMode,
	TICnt_CenterPoint,
 	TICnt_Rotation,
	//TICnt_SFR,
	TICnt_Color,
	TICnt_Reverse,
	TICnt_EIAJ,
 	TICnt_FOV,	
	TICnt_ParticleManual,
	TICnt_BlackSpot,
	TICnt_DefectPixel,
	//TICnt_LEDTest,

	TICnt_Brightness,
	TICnt_IRFilter,
	//TICnt_PatternNoise,	

// 	TICnt_BlackSpot,
// 	TICnt_FFT,
//	TICnt_EIAJ,			
// TICnt_Reset,
// TICnt_VideoSignal,


};

typedef enum enLT_TestItemInitial_ID
{
	TIInitCnt_1,
};

static LPCTSTR g_szLT_TestItemInitial[TICnt_Initial] =
{
	_T("None"),
};

typedef enum enLT_TestItemFinalize_ID
{
	TIFinalCnt_1,
};

static LPCTSTR g_szLT_TestItemFinal[TICnt_Final] =
{
	_T("None"),
};

//typedef enum enLT_TestItemVideoSignal_ID
//{
//	TIVideoSignalCnt_1,
//};
//
//static LPCTSTR g_szLT_TestItemVideoSignal[TICnt_VideoSignal] =
//{
//	_T("None"),
//};

typedef enum enLT_TestItemCurrent_ID
{
	TICurrentCnt_1,
};

static LPCTSTR g_szLT_TestItemCurrent[TICnt_Current] =
{
	_T("None"),
};


//typedef enum enLT_TestItemOperMode_ID
//{
//	TIOperationModeCnt_1,
//};
//
//static LPCTSTR g_szLT_TestItemOperationMode[TICnt_OperationMode] =
//{
//	_T("None"),
//};

typedef enum enLT_TestItemEIJA_ID
{
	TIEIAJCnt_1,
};

static LPCTSTR g_szLT_TestItemEIAJ[TICnt_EIAJ] =
{
	_T("None"),
};

typedef enum enLT_TestItemReverse_ID
{
	TIReverseCnt_1,
};

static LPCTSTR g_szLT_TestItemReverse[TICnt_Reverse] =
{
	_T("None"),
};

typedef enum enLT_TestItemParticleManual_ID
{
	TIParticleManualCnt_1,
};

static LPCTSTR g_szLT_TestItemParticleManual[TICnt_ParticleManual] =
{
	_T("None"),
};

//typedef enum enLT_TestItemPatternNoise_ID
//{
//	TIPatternNoiseCnt_1,
//};
//
//static LPCTSTR g_szLT_TestItemPatternNoise[TICnt_PatternNoise] =
//{
//	_T("None"),
//};

typedef enum enLT_TestItemIRFilter_ID
{
	TIIRFilterCnt_1,
};

static LPCTSTR g_szLT_TestItemIRFilter[TICnt_IRFilter] =
{
	_T("None"),
};

//typedef enum enLT_TestItemIRFilterManual_ID
//{
//	TIIRFilterManualCnt_1,
//};
//
//static LPCTSTR g_szLT_TestItemIRFilterManual[TICnt_IRFilterManual] =
//{
//	_T("None"),
//};

typedef enum enLT_TestItemAngle_ID
{
	TIAngleCnt_1,
};

static LPCTSTR g_szLT_TestItemAngle[TICnt_FOV] =
{
	_T("None"),
};

typedef enum enLT_TestItemCenterPt_ID
{
	TICenterPtCnt_1,
	//TICenterPtCnt_2,
};

static LPCTSTR g_szLT_TestItemCenterPt[TICnt_CenterPoint] =
{
	_T("None"),
	//_T("1st"),
	//_T("2nd"),
};

typedef enum enLT_TestItemColor_ID
{
	TIColorCnt_1,
};

static LPCTSTR g_szLT_TestItemColor[TICnt_Color] =
{
	_T("None"),
};


//typedef enum enLT_TestItemFFT_ID
//{
//	TIFFTCnt_1,
//};
//
//static LPCTSTR g_szLT_TestItemFFT[TICnt_FFT] =
//{
//	_T("None"),
//};

typedef enum enLT_TestItemBlackSpot_ID
{
	TIBlackSpotCnt_1,
};

static LPCTSTR g_szLT_TestItemBlackSpot[TICnt_BlackSpot] =
{
	_T("None"),
};

typedef enum enLT_TestItemDefectPixel_ID
{
	TIDefectPixelCnt_1,
};

static LPCTSTR g_szLT_TestItemDefectPixel[TICnt_DefectPixel] =
{
	_T("None"),
};


typedef enum enLT_TestItemBrightness_ID
{
	TIBrightnessCnt_1,
};

static LPCTSTR g_szLT_TestItemBrightness[TICnt_Brightness] =
{
	_T("None"),
};

typedef enum enLT_TestItemRotate_ID
{
	TIRotateCnt_1,
};

static LPCTSTR g_szLT_TestItemRotate[TICnt_Rotation] =
{
	_T("None"),
};

//typedef enum enLT_TestItemSFR_ID
//{
//	TISFRCnt_1,
//};
//
//static LPCTSTR g_szLT_TestItemSFR[TICnt_SFR] =
//{
//	_T("None"),
//};

//typedef enum enLT_TestItemLED_ID
//{
//	TILEDCnt_1,
//};
//
//static LPCTSTR g_szLT_TestItemLED[TICnt_LEDTest] =
//{
//	_T("None"),
//};


//typedef enum enLT_TestItemReset_ID
//{
//	TIResetCnt_1,
//};
//
//static LPCTSTR g_szLT_TestItemReset[TICnt_Reset] =
//{
//	_T("None"),
//};

static LPCTSTR g_szLT_TestItem_Name[] =
{
	_T("Test Initialize"),		// loading
	_T("Test Finalize"),		// unloading

	_T("Current"),
	//_T("Operation Mode"),
 	_T("Center Point"),
 	_T("Rotate"),
	_T("Color"),
	_T("Flip"),
	//_T("SFR"),
	_T("EIAJ"),
	_T("FOV"),
	_T("Particle"),
	_T("Stain"),
	_T("DefectPixel"),
	//_T("LED"),

 	_T("Brightness"),
	_T("IR Filter"),
	//_T("Pattern Noise"),

// 	_T("Particle"),
// 	_T("Sound"),
//	_T("EIAJ"),
// _T("Video Signal"),
// _T("Reset"),
	NULL
};

//-----------------------------------------------------------------------------
// 검사 스텝 설정용 기본 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_StepUnit
{
	BOOL		bMasterUse;		// 테스트 진행 여부
	BOOL		bTestUse;		// 테스트 진행 여부
	UINT		nTestItem;		// 테스트 항목 구분
	UINT		nTestItemCnt;	// 테스트 세부 항목 구분
	UINT		nRetryCnt;		// 오류나 NG판정이 추가적으로 재시도하는 횟수
	DWORD		dwDelay;		// 스텝과 스텝 사이의 딜레이

	BOOL		bAxisUse;		// 모터 진행 여부
	UINT		nAxisItem;		//
	double		dbMovePos;		//

	BOOL		bIOUse;			// IO 진행 여부
	UINT		nIOItem;		//
	UINT		nIOSignal;		//

	_tag_StepUnit()
	{
		bMasterUse	= TRUE;
		bTestUse	= TRUE;
		nTestItem	= 0;
		nTestItemCnt = 0;
		nRetryCnt	= 0;
		dwDelay		= 0;
		bAxisUse	= FALSE;
		nAxisItem	= 0;
		dbMovePos	= 0;
		bIOUse		= 0;
		nIOItem		= 0;
		nIOSignal	= 0;
	};

	_tag_StepUnit& operator= (const _tag_StepUnit& ref)
	{
		bMasterUse = ref.bMasterUse;
		bTestUse = ref.bTestUse;
		nTestItem = ref.nTestItem;
		nTestItemCnt = ref.nTestItemCnt;
		nRetryCnt	= ref.nRetryCnt;
		dwDelay		= ref.dwDelay;
		bAxisUse	= ref.bAxisUse;
		nAxisItem	= ref.nAxisItem;
		dbMovePos	= ref.dbMovePos;
		bIOUse		= ref.bIOUse;
		nIOItem		= ref.nIOItem;
		nIOSignal	= ref.nIOSignal;

		return *this;
	};

}ST_StepUnit, *PST_StepUnit;

//-----------------------------------------------------------------------------
// 검사 스텝 설정용 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_StepInfo
{
	CArray <ST_StepUnit, ST_StepUnit&>		StepList;	// 검사 스텝 목록

	_tag_StepInfo()
	{

	};

	_tag_StepInfo& operator= (const _tag_StepInfo& ref)
	{
		StepList.RemoveAll();
		StepList.Copy(ref.StepList);

		return *this;
	};

	// 스텝 갯수
	INT_PTR GetCount() const
	{
		return StepList.GetCount();
	};

	// 모든 스텝 삭제
	virtual void RemoveAll()
	{
		StepList.RemoveAll();
	};

	// 스텝 추가
	virtual void Step_Add(__in ST_StepUnit stTestStep)
	{
		StepList.Add(stTestStep);
	};

	// 스텝 삽입
	virtual void Step_Insert(__in UINT nIdex, __in ST_StepUnit stTestStep)
	{
		if (0 < GetCount())
		{
			StepList.InsertAt(nIdex, stTestStep);
		}
	};

	// 스텝 삭제
	virtual void Step_Remove(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			StepList.RemoveAt(nIdex);
		}
	};

	// 스텝 위로 이동
	virtual void Step_Up(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			// 0번 인덱스는 위로 이동 불가
			if ((0 < nIdex) && (1 < GetCount()))
			{
				ST_StepUnit stStep = StepList.GetAt(nIdex);

				StepList.RemoveAt(nIdex);
				StepList.InsertAt(nIdex - 1, stStep);
			}
		}
	};

	// 스텝 아래로 이동
	virtual void Step_Down(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			// 마지막 인덱스는 아래로 이동 불가
			if ((nIdex < (UINT)(GetCount() - 1)) && (1 < (UINT)GetCount()))
			{
				ST_StepUnit stStep = StepList.GetAt(nIdex);

				StepList.RemoveAt(nIdex);

				// 변경되는 위치가 최하단이면, Insert 대신 Add 사용
				if ((nIdex + 1) < (UINT)(GetCount()))
				{
					StepList.InsertAt(nIdex + 1, stStep);
				}
				else
				{
					StepList.Add(stStep);
				}
			}
		}
	};

	// 마스터 체크
	virtual void Step_MasterCheck(__in UINT nIdex, __in BOOL bCheck)
	{
		if (0 < GetCount())
		{
			StepList.GetAt(nIdex).bMasterUse = bCheck;
		}
	};

}ST_StepInfo, *PST_StepInfo;



//-----------------------------------------------------------------------------
// 표준 바코드 정보
//-----------------------------------------------------------------------------
typedef struct _tag_BarcodeRef
{
	CString szrefBarcode;
	CString szModelName;

	_tag_BarcodeRef()
	{
		szrefBarcode.Empty();
		szModelName.Empty();
	};

	_tag_BarcodeRef& operator= (const _tag_BarcodeRef& ref)
	{
		szrefBarcode = ref.szrefBarcode;
		szModelName = ref.szModelName;
		
		return *this;
	};

}ST_BarcodeRef, *PST_BarcodeRef;

//-----------------------------------------------------------------------------
// 표준 바코드 설정용 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_BarcodeRefInfo
{
	// 표준 바코드 목록
	CArray <ST_BarcodeRef, ST_BarcodeRef&>	BarcodeList;

	_tag_BarcodeRefInfo()
	{

	};

	_tag_BarcodeRefInfo& operator= (const _tag_BarcodeRefInfo& ref)
	{
		BarcodeList.RemoveAll();
		BarcodeList.Copy(ref.BarcodeList);

		return *this;
	};

	// 갯수
	INT_PTR GetCount() const
	{
		return BarcodeList.GetCount();
	};

	// 모든 삭제
	virtual void RemoveAll()
	{
		BarcodeList.RemoveAll();
	};

	// 추가
	virtual void BarcodeRef_Add(__in ST_BarcodeRef stBarcodeRef)
	{
		BarcodeList.Add(stBarcodeRef);
	};

	// 삽입
	virtual void BarcodeRef_Insert(__in UINT nIdex, __in ST_BarcodeRef stBarcodeRef)
	{
		if (0 < GetCount())
		{
			BarcodeList.InsertAt(nIdex, stBarcodeRef);
		}
	};

	// 삭제
	virtual void BarcodeRef_Remove(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			BarcodeList.RemoveAt(nIdex);
		}
	};

	// 위로 이동
	virtual void BarcodeRef_Up(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			// 0번 인덱스는 위로 이동 불가
			if ((0 < nIdex) && (1 < GetCount()))
			{
				ST_BarcodeRef stBarcodeRef = BarcodeList.GetAt(nIdex);

				BarcodeList.RemoveAt(nIdex);
				BarcodeList.InsertAt(nIdex - 1, stBarcodeRef);
			}
		}
	};

	// 아래로 이동
	virtual void BarcodeRef_Down(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			// 마지막 인덱스는 아래로 이동 불가
			if ((nIdex < (UINT)(GetCount() - 1)) && (1 < (UINT)GetCount()))
			{
				ST_BarcodeRef stBarcodeRef = BarcodeList.GetAt(nIdex);

				BarcodeList.RemoveAt(nIdex);

				// 변경되는 위치가 최하단이면, Insert 대신 Add 사용
				if ((nIdex + 1) < (UINT)(GetCount()))
				{
					BarcodeList.InsertAt(nIdex + 1, stBarcodeRef);
				}
				else
				{
					BarcodeList.Add(stBarcodeRef);
				}
			}
		}
	};

}ST_BarcodeRefInfo, *PST_BarcodeRefInfo;


//-----------------------------------------------------------------------------
// 사용자 등록 정보
//-----------------------------------------------------------------------------
typedef struct _tag_UserConfig
{
	CString szRegistrationTime;
	CString szOperatorID;

	_tag_UserConfig()
	{
		szRegistrationTime.Empty();
		szOperatorID.Empty();
	};

	_tag_UserConfig& operator= (const _tag_UserConfig& ref)
	{
		szRegistrationTime = ref.szRegistrationTime;
		szOperatorID = ref.szOperatorID;

		return *this;
	};

}ST_UserConfig, *PST_UserConfig;

//-----------------------------------------------------------------------------
// 표준 바코드 설정용 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_UserConfigInfo
{
	// 표준 바코드 목록
	CArray <ST_UserConfig, ST_UserConfig&>	Operatorlist;

	_tag_UserConfigInfo()
	{

	};

	_tag_UserConfigInfo& operator= (const _tag_UserConfigInfo& ref)
	{
		Operatorlist.RemoveAll();
		Operatorlist.Copy(ref.Operatorlist);

		return *this;
	};

	// 갯수
	INT_PTR GetCount() const
	{
		return Operatorlist.GetCount();
	};

	// 모든 삭제
	virtual void RemoveAll()
	{
		Operatorlist.RemoveAll();
	};

	// 추가
	virtual void UserConfig_Add(__in ST_UserConfig stUserConfig)
	{
		Operatorlist.Add(stUserConfig);
	};

	// 삽입
	virtual void UserConfig_Insert(__in UINT nIdex, __in ST_UserConfig stUserConfig)
	{
		if (0 < GetCount())
		{
			Operatorlist.InsertAt(nIdex, stUserConfig);
		}
	};

	// 삭제
	virtual void UserConfig_Remove(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			Operatorlist.RemoveAt(nIdex);
		}
	};

	// 위로 이동
	virtual void UserConfig_Up(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			// 0번 인덱스는 위로 이동 불가
			if ((0 < nIdex) && (1 < GetCount()))
			{
				ST_UserConfig stUserConfig = Operatorlist.GetAt(nIdex);

				Operatorlist.RemoveAt(nIdex);
				Operatorlist.InsertAt(nIdex - 1, stUserConfig);
			}
		}
	};

	// 아래로 이동
	virtual void UserConfig_Down(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			// 마지막 인덱스는 아래로 이동 불가
			if ((nIdex < (UINT)(GetCount() - 1)) && (1 < (UINT)GetCount()))
			{
				ST_UserConfig stUserConfig = Operatorlist.GetAt(nIdex);

				Operatorlist.RemoveAt(nIdex);

				// 변경되는 위치가 최하단이면, Insert 대신 Add 사용
				if ((nIdex + 1) < (UINT)(GetCount()))
				{
					Operatorlist.InsertAt(nIdex + 1, stUserConfig);
				}
				else
				{
					Operatorlist.Add(stUserConfig);
				}
			}
		}
	};

}ST_UserConfigInfo, *PST_UserConfigInfo;


//-----------------------------------------------------------------------------
// 라벨 프린터 설정용 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_LabelPrinter
{
	int		iPosX[enLabelDataType_Max];
	int		iPosY[enLabelDataType_Max];
	int		iWidthSz[enLabelDataType_Max];
	int		iHeightSz[enLabelDataType_Max];

	CString szText[enLabelDataType_Max];
	CString szPrnFileName;

	void Reset()
	{
		for (int i = 0; i < enLabelDataType_Max; i++)
		{
			iPosX[i] = 0;
			iPosY[i] = 0;
			iWidthSz[i] = 0;
			iHeightSz[i] = 0;
			szText[i].Empty();
		}

		szPrnFileName.Empty();
	};

	_tag_LabelPrinter()
	{
		Reset();
	};

	_tag_LabelPrinter& operator= (const _tag_LabelPrinter& ref)
	{
		for (int i = 0; i < enLabelDataType_Max; i++)
		{
			iPosX[i] = ref.iPosX[i];
			iPosY[i] = ref.iPosY[i];
			iWidthSz[i] = ref.iWidthSz[i];
			iHeightSz[i] = ref.iHeightSz[i];
			szText[i] = ref.szText[i];
		}

		szPrnFileName = ref.szPrnFileName;

		return *this;
	};

}ST_LabelPrinter, *PST_LabelPrinter;


typedef struct _tag_LT_TestItem
{
	enLT_TestItem_ID	m_nTestItem_ID;		// 테스트 항목 고유 ID
	CString				m_szTestItemName;	// 테스트 항목 명칭
	CString				m_szTestItemDesc;	// 테스트 항목 설명
	LONG				m_lResult;			// 테스트 결과 코드

	_tag_LT_TestItem()
	{
		m_nTestItem_ID	= TIID_TestInitialize;
		m_lResult		= -1;
	};

	_tag_LT_TestItem& operator= (_tag_LT_TestItem& ref)
	{
		m_nTestItem_ID		= ref.m_nTestItem_ID;
		m_szTestItemName	= ref.m_szTestItemName;
		m_szTestItemDesc	= ref.m_szTestItemDesc;
		m_lResult			= ref.m_lResult;

		return *this;
	}
}ST_LT_TestItem, *PST_LT_TestItem;

typedef struct _tag_LT_TI_Angle: public ST_LT_TestItem
{
	// 검사 기준 데이터
	BOOL bTestMode;
	ST_Angle_Opt stAngleOpt;

	// 측정 데이터
	ST_Angle_Result stAngleResult;

	_tag_LT_TI_Angle()
	{
		m_szTestItemName = _T("Angle");
	};

	void Reset()
	{
		stAngleResult.Reset();
	};

	_tag_LT_TI_Angle& operator= (_tag_LT_TI_Angle& ref)
	{
		__super::operator=(ref);
		bTestMode = ref.bTestMode;
		stAngleOpt	= ref.stAngleOpt;
		stAngleResult = ref.stAngleResult;

		return *this;
	};
}ST_LT_TI_Angle, *PST_LT_TI_Angle;

typedef struct _tag_LT_TI_CenterPoint : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_CenterPoint_Opt stCenterPointOpt;

	// 측정 데이터
	ST_CenterPoint_Result stCenterPointResult;

	_tag_LT_TI_CenterPoint()
	{
		m_szTestItemName = _T("CenterPoint");
	};

	void Reset()
	{
		stCenterPointResult.Reset();
	};

	_tag_LT_TI_CenterPoint& operator= (_tag_LT_TI_CenterPoint& ref)
	{
		__super::operator=(ref);

		stCenterPointOpt	= ref.stCenterPointOpt;
		stCenterPointResult = ref.stCenterPointResult;

		return *this;
	};
}ST_LT_TI_CenterPoint, *PST_LT_TI_CenterPoint;

typedef struct _tag_LT_TI_Color : public ST_LT_TestItem
{
	BOOL bTestMode;
	// 검사 기준 데이터
	ST_Color_Opt stColorOpt;

	// 측정 데이터
	ST_Color_Result stColorResult;

	_tag_LT_TI_Color()
	{
		m_szTestItemName = _T("Color");
	};

	void Reset()
	{
		stColorResult.Reset();
	};

	_tag_LT_TI_Color& operator= (_tag_LT_TI_Color& ref)
	{
		__super::operator=(ref);

		bTestMode = ref.bTestMode;
		stColorOpt = ref.stColorOpt;
		stColorResult = ref.stColorResult;

		return *this;
	};
}ST_LT_TI_Color, *PST_LT_TI_Color;

typedef struct _tag_LT_TI_Current : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_Current_Opt	  stCurrentOpt;

	// 측정 데이터
	ST_Current_Result stCurrentResult;

	_tag_LT_TI_Current()
	{
		m_szTestItemName = _T("Current");
	};

	void Reset()
	{
		stCurrentResult.Reset();
	};

	_tag_LT_TI_Current& operator= (_tag_LT_TI_Current& ref)
	{
		__super::operator=(ref);

		stCurrentOpt		= ref.stCurrentOpt;
		stCurrentResult		= ref.stCurrentResult;

		return *this;
	};
}ST_LT_TI_Current, *PST_LT_TI_Current;

typedef struct _tag_LT_TI_Particle : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_Particle_Opt stParticleOpt;

	// 측정 데이터
	ST_Particle_Result stParticleResult;

	_tag_LT_TI_Particle()
	{
		m_szTestItemName = _T("Particle");
	};

	void Reset()
	{
		stParticleResult.Reset();
	};

	_tag_LT_TI_Particle& operator= (_tag_LT_TI_Particle& ref)
	{
		__super::operator=(ref);

		stParticleOpt = ref.stParticleOpt;
		stParticleResult = ref.stParticleResult;

		return *this;
	};
}ST_LT_TI_BlackSpot, *PST_LT_TI_Particle;

typedef struct _tag_TI_DefectPixel : public ST_LT_TestItem
{
	ST_DefectPixel_Opt		stDefectPixelOpt;		// 검사 기준 데이터
	ST_DefectPixel_Data		stDefectPixelData;		// 측정 데이터

	_tag_TI_DefectPixel()
	{
		m_szTestItemName = _T("DefectPixel");
	};

	void Reset()
	{
		stDefectPixelData.Reset();
	};

	_tag_TI_DefectPixel& operator= (_tag_TI_DefectPixel& ref)
	{
		__super::operator=(ref);
		stDefectPixelOpt	= ref.stDefectPixelOpt;
		stDefectPixelData	= ref.stDefectPixelData;

		return *this;
	};

}ST_LT_TI_DefectPixel, *PST_LT_TI_DefectPixel;

typedef struct _tag_LT_TI_Brightness : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_Brightness_Opt	stBrightnessOpt;
	// 측정 데이터
	ST_Brightness_Result stBrightnessResult;


	_tag_LT_TI_Brightness()
	{
		m_szTestItemName = _T("Brightness");
	};

	void Reset()
	{
		stBrightnessResult.Reset();
	};

	_tag_LT_TI_Brightness& operator= (_tag_LT_TI_Brightness& ref)
	{
		__super::operator=(ref);
		stBrightnessOpt = ref.stBrightnessOpt;
		stBrightnessResult = ref.stBrightnessResult;
		return *this;
	};

}ST_LT_TI_Brightness, *PST_LT_TI_Brightness;


typedef struct _tag_LT_TI_Rotate : public ST_LT_TestItem
{
	// 검사 기준 데이터
	BOOL bTestMode;

	ST_Rotate_Opt	stRotateOpt;
	// 측정 데이터
	ST_Rotate_Result stRotateResult;


	_tag_LT_TI_Rotate()
	{
		m_szTestItemName = _T("Rotate");
	};

	void Reset()
	{
		stRotateResult.Reset();
	};

	_tag_LT_TI_Rotate& operator= (_tag_LT_TI_Rotate& ref)
	{
		__super::operator=(ref);
		bTestMode = ref.bTestMode;
		stRotateOpt		= ref.stRotateOpt;
		stRotateResult  = ref.stRotateResult;
		return *this;
	};

}ST_LT_TI_Rotate, *PST_LT_TI_Rotate;

typedef struct _tag_LT_TI_SFR : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_SFR_Opt		stSFROpt;

	// 초기 데이터
	ST_SFR_Opt		stInitSFROpt;

	// 측정 데이터
	ST_SFR_Result	stSFRResult;

	_tag_LT_TI_SFR()
	{
		m_szTestItemName = _T("SFR");
	};

	void Reset()
	{
		stSFRResult.Reset();
	};

	_tag_LT_TI_SFR& operator= (_tag_LT_TI_SFR& ref)
	{
		__super::operator=(ref);

		stSFROpt	= ref.stSFROpt;
		stInitSFROpt = ref.stInitSFROpt;
		stSFRResult	= ref.stSFRResult;

		return *this;
	};

}ST_LT_TI_SFR, *PST_LT_TI_SFR;

typedef struct _tag_LT_TI_FFT : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_FFT_Opt		stFFTOpt;
	// 측정 데이터
	ST_FFT_Result	stFFTResult;

	_tag_LT_TI_FFT()
	{
		m_szTestItemName = _T("Sound");
	};

	void Reset()
	{
		stFFTResult.Reset();
	};

	_tag_LT_TI_FFT& operator= (_tag_LT_TI_FFT& ref)
	{
		__super::operator=(ref);

		stFFTOpt = ref.stFFTOpt;
		stFFTResult = ref.stFFTResult;

		return *this;
	};

}ST_LT_TI_FFT, *PST_LT_TI_FFT;
// 
// typedef struct _tag_LT_TI_EIAJ : public ST_LT_TestItem
// {
// 	// 검사 기준 데이터
// 	ST_EIAJ_Opt		stEIAJOpt;
// 
// 	// 초기 데이터
// 	ST_EIAJ_Opt		stInitEIAJOpt;
// 
// 	// 측정 데이터
// 	ST_EIAJ_Result	stEIAJResult;
// 
// 	_tag_LT_TI_EIAJ()
// 	{
// 		m_szTestItemName = _T("EIAJ");
// 	};
// 
// 	void Reset()
// 	{
// 		stEIAJData.Reset();
// 	};
// 
// 	_tag_LT_TI_EIAJ& operator= (_tag_LT_TI_EIAJ& ref)
// 	{
// 		__super::operator=(ref);
// 
// 		stEIAJOpt = ref.stEIAJOpt;
// 		stInitEIAJOpt = ref.stInitEIAJOpt;
// 		stEIAJResult = ref.stEIAJResult;
// 
// 		return *this;
// 	};
// 
// }ST_LT_TI_EIAJ, *PST_LT_TI_EIAJ;

typedef struct _tag_LT_TI_EIAJ : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_EIAJ_Op		stEIAJOp;
	// 측정 데이터
	ST_EIAJ_Data	stEIAJData;

	_tag_LT_TI_EIAJ()
	{
		m_szTestItemName = _T("EIAJ");
	};

	void Reset()
	{
		stEIAJData.Reset();
	};

	_tag_LT_TI_EIAJ& operator= (_tag_LT_TI_EIAJ& ref)
	{
		__super::operator=(ref);
		stEIAJOp = ref.stEIAJOp;
		stEIAJData = ref.stEIAJData;
		return *this;
	};

}ST_LT_TI_EIAJ, *PST_LT_TI_EIAJ;
typedef struct _tag_LT_TI_Reverse : public ST_LT_TestItem
{
	// 검사 기준 데이터
	BOOL bTestMode;
	ST_Reverse_Opt		stReverseOpt;

	// 초기 데이터
	ST_Reverse_Opt		stInitReverseOpt;

	// 측정 데이터
	ST_Reverse_Result	stReverseResult;

	_tag_LT_TI_Reverse()
	{
		m_szTestItemName = _T("Reverse");
	};

	void Reset()
	{
		stReverseResult.Reset();
	};

	_tag_LT_TI_Reverse& operator= (_tag_LT_TI_Reverse& ref)
	{
		__super::operator=(ref);
		bTestMode = ref.bTestMode;
		stReverseOpt = ref.stReverseOpt;
		stInitReverseOpt = ref.stInitReverseOpt;
		stReverseResult = ref.stReverseResult;

		return *this;
	};

}ST_LT_TI_Reverse, *PST_LT_TI_Reverse;

typedef struct _tag_LT_TI_ParticleMA : public ST_LT_TestItem
{
	// 측정 데이터
	ST_Particle_Result stParticleResult;

	_tag_LT_TI_ParticleMA()
	{
		m_szTestItemName = _T("ParticleManual");
	};

	void Reset()
	{
		stParticleResult.Reset();
	};

	_tag_LT_TI_ParticleMA& operator= (_tag_LT_TI_ParticleMA& ref)
	{
		__super::operator=(ref);

		stParticleResult = ref.stParticleResult;

		return *this;
	};
}ST_LT_TI_ParticleMA, *PST_LT_TI_ParticleMA;

typedef struct _tag_LT_TI_PatternNoise : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_PatternNoise_Opt		stPatternNoiseOpt;

	// 초기 데이터
	ST_PatternNoise_Opt		stInitPatternNoiseOpt;

	// 측정 데이터
	ST_PatternNoise_Result	stPatternNoiseResult;

	_tag_LT_TI_PatternNoise()
	{
		m_szTestItemName = _T("PatternNoise");
	};

	void Reset()
	{
		stPatternNoiseResult.Reset();
	};

	_tag_LT_TI_PatternNoise& operator= (_tag_LT_TI_PatternNoise& ref)
	{
		__super::operator=(ref);

		stPatternNoiseOpt = ref.stPatternNoiseOpt;
		stInitPatternNoiseOpt = ref.stInitPatternNoiseOpt;
		stPatternNoiseResult = ref.stPatternNoiseResult;

		return *this;
	};

}ST_LT_TI_PatternNoise, *PST_LT_TI_PatternNoise;


typedef struct _tag_LT_TI_IRFilter : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_IRFilter_Opt		stIRFilterOpt;

	// 측정 데이터
	ST_IRFilter_Result	stIRFilterResult;

	_tag_LT_TI_IRFilter()
	{
		m_szTestItemName = _T("IRFilter");
	};

	void Reset()
	{
		stIRFilterResult.Reset();
	};

	_tag_LT_TI_IRFilter& operator= (_tag_LT_TI_IRFilter& ref)
	{
		__super::operator=(ref);

		stIRFilterOpt = ref.stIRFilterOpt;
		stIRFilterResult = ref.stIRFilterResult;

		return *this;
	};

}ST_LT_TI_IRFilter, *PST_LT_TI_IRFilter;


typedef struct _tag_LT_TI_IRFilterMA : public ST_LT_TestItem
{
	// 측정 데이터
	ST_IRFilter_Result	stIRFilterResult;

	_tag_LT_TI_IRFilterMA()
	{
		m_szTestItemName = _T("IRFilterManual");
	};

	void Reset()
	{
		stIRFilterResult.Reset();
	};

	_tag_LT_TI_IRFilterMA& operator= (_tag_LT_TI_IRFilterMA& ref)
	{
		__super::operator=(ref);

		stIRFilterResult = ref.stIRFilterResult;

		return *this;
	};

}ST_LT_TI_IRFilterMA, *PST_LT_TI_IRFilterMA;


typedef struct _tag_LT_TI_LED : public ST_LT_TestItem
{
	// 리셋 카운트
	UINT nResetCnt;

	// 리셋 딜레이
	UINT nResetDelay;

	// 검사 기준 데이터
	ST_Current_Opt		stLEDCurrOpt;

	// 측정 데이터
	ST_Current_Result	stLEDCurrResult;

	_tag_LT_TI_LED()
	{
		m_szTestItemName = _T("LED");
	};

	void Reset()
	{
		nResetCnt = 0;
		nResetDelay = 0;
		stLEDCurrResult.Reset();
	};

	_tag_LT_TI_LED& operator= (_tag_LT_TI_LED& ref)
	{
		__super::operator=(ref);

		nResetCnt = ref.nResetCnt;
		nResetDelay = ref.nResetDelay;
		stLEDCurrOpt = ref.stLEDCurrOpt;
		stLEDCurrResult = ref.stLEDCurrResult;

		return *this;
	};

}ST_LT_TI_LED, *PST_LT_TI_LED;


typedef struct _tag_LT_TI_VideoSignal : public ST_LT_TestItem
{
	// 결과
	UINT nResult;

	_tag_LT_TI_VideoSignal()
	{
		m_szTestItemName = _T("VideoSignal");
	};

	void Reset()
	{
		nResult = 3;
	};

	_tag_LT_TI_VideoSignal& operator= (_tag_LT_TI_VideoSignal& ref)
	{
		__super::operator=(ref);

		nResult = ref.nResult;

		return *this;
	};

}ST_LT_TI_VideoSignal, *PST_LT_TI_VideoSignal;


typedef struct _tag_LT_TI_Reset : public ST_LT_TestItem
{
	// 결과
	UINT nResult;

	// 딜레이
	UINT nDelay;

	_tag_LT_TI_Reset()
	{
		m_szTestItemName = _T("Reset");
	};

	void Reset()
	{
		nResult = 3;
		nDelay = 0;
	};

	_tag_LT_TI_Reset& operator= (_tag_LT_TI_Reset& ref)
	{
		__super::operator=(ref);

		nResult = ref.nResult;
		nDelay = ref.nDelay;

		return *this;
	};

}ST_LT_TI_Reset, *PST_LT_TI_Reset;


typedef struct _tag_LT_TI_OperationMode : public ST_LT_TestItem
{
	// 최종 결과
	UINT nResult;
	UINT nEachResult[2];

	// ( 0:15, 1:30) 
	// 각 항목 결과
	UINT nResetResult[2]; // 프레임 
	UINT nFPSResult[2]; // 프레임 
	UINT nVideoResult[2]; // 영상출력
	ST_Current_Result stCurrResult[2]; // 전류

	// 전류 
	ST_Current_Opt stCurrOpt[2];

	// 리셋 딜레이
	UINT nResetDelay;

	_tag_LT_TI_OperationMode()
	{
		m_szTestItemName = _T("Operation Mode");
	};

	void Reset()
	{
		nResult = 3;
		nResetDelay = 0;

		for (int i = 0; i < 2; i++)
		{
			nResetResult[i] = 0;
			nEachResult[i] = 0;
			nFPSResult[i] = 0;
			nVideoResult[i] = 0;
			stCurrResult[i].Reset();
		}
	};

	_tag_LT_TI_OperationMode& operator= (_tag_LT_TI_OperationMode& ref)
	{
		__super::operator=(ref);

		nResult = ref.nResult;
		nResetDelay = ref.nResetDelay;

		for (int i = 0; i < 2; i++)
		{
			nResetResult[i] = ref.nResetResult[i];
			nEachResult[i] = ref.nEachResult[i];
			nFPSResult[i] = ref.nFPSResult[i];
			nVideoResult[i] = ref.nVideoResult[i];
			stCurrResult[i] = ref.stCurrResult[i];
		}

		return *this;
	};

}ST_LT_TI_OperationMode, *PST_LT_TI_OperationMode;


#endif // Def_TestItem_h__
