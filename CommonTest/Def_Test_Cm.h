﻿//*****************************************************************************
// Filename	: 	Def_Test_Cm.h
// Created	:	2016/06/30
// Modified	:	2016/08/08
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Test_Cm_h__
#define Def_Test_Cm_h__

#include "afxwin.h"
/*영상 WIDTH */
#define		CAM_IMAGE_WIDTH 1920
/*영상 HEIGHT */
#define		CAM_IMAGE_HEIGHT 1080
/*영상 WIDTH /2 */
#define		CAM_IMAGE_WIDTH_HALF 320
/*영상 HEIGHT /2 */
#define		CAM_IMAGE_HEIGHT_HALF 240

/*영상 총 버퍼 사이즈 720*480*4  */
#define		RGB_BUF_SIZE 3686400 
/*영상 버퍼 사이즈 720*480  */
#define		IMAGE_BUF_SIZE 921600 
/*영상 720*4  */
#define		IMAGE_WX4 1920 

// Use, Not Use string
static LPCTSTR	g_szUseNotUse[] =
{
	_T("Not Use"),
	_T("Use"),
	NULL
};

// 카메라의 영상상태
enum enCamState
{
	CamState_Mirror = 0,	// 좌우반전
	CamState_Original,		// 오리지널
	CamState_Flip,			// 상하반전
	CamStateRotate,			// 로테이트
	CamStateMaxNum,
};

static LPCTSTR	g_szCamState[] =
{
	_T("Original"),
	_T("Horizontal Mirror"),
	_T("Mirror & Flip"),
	_T("Vertical Flip"),
	NULL
};

// 카메라의 신호 방식 (디지털 그래버)
enum enCamSignalType
{
	//CamSignalType_TVI = 0,	
	CamSignalType_DN8_DVRS,			
	CamSignalTypeMaxNum,
};

static LPCTSTR	g_szCamSignalType[] =
{
	//_T("TVI"),
	_T("DN8 DVRS"),
	NULL
};


// 아날로그 주파수 방식
enum enVideoState
{
	VideoState_PAL = 0,
	VideoState_NTSC,
	VideoState_MaxNum,
};

static LPCTSTR	g_szVideoState[] =
{
	_T("PAL"),
	_T("NTSC"),
	NULL
};

// Cover 사용 유무
enum enCoverUse
{
	CoverUse_Empty = 0,
	CoverUse_Cover,
	CoverUse_MaxNum,
};

static LPCTSTR	g_szCoverState[] =
{
	_T("Empty"),
	_T("Cover"),
	NULL
};

static int	g_iImageSzWidth_Analog[VideoState_MaxNum] =
{
	720,
	720,
};

static int	g_iImageSzHeight_Analog[VideoState_MaxNum] =
{
	576,
	480,
};

// Device Fail Code
enum nResult
{
	result_Fail,
	result_Pass,
	result_MotorFail,
	result_CamDisconnect,
	result_HT_Fail,			
};

// 결과 코드 전체 검사
enum enResultCode_All
{
// 	RCA_NG,
// 	RCA_OK,
// 	RCA_USER_STOP,
// 	RCA_Power_Err,
// 	RCA_NoImage,
// 	RCA_NoBarcode,
// 	RCA_UnknownError,
// 	RCA_CenterPointDetect_Err,
// 	RCA_Model_Err,
// 	RCA_Motor_Err,
// 	RCA_IO_Err,

	RCA_NG,
	RCA_OK,
	RCA_RUN,
	RCA_Empty,
	RCA_Skip,
	RCA_Init,
	RCA_UserStop,
	RCA_MachineCheck,
	RCA_MachineExpcetion,
	RCA_TimeOut,
	RCA_NoImage,
	RCA_NoBarcode,
	RCA_TransferErr,
	RCA_PogoCountOver,
	RCA_EquipAlarm,
	RCA_SensorDetect,
	RCA_Master,
	RCA_OpenShort,

	RCA_Max,
};

static LPCTSTR g_szResultCode_All[] =
{
	_T("[Result] NG."),							// RCA_NG,
	_T("[Result] OK."),							// RCA_OK,
	_T("[Running] Ready."),						// RCA_RUN,
	_T("[Err] Empty."),						// RCA_Empty,
	_T("[Err] Skip."),							// RCA_Skip,
	_T("[Err] Init."),							// RCA_Init,
	_T("[Err] User Stop."),					// RCA_UserStop,
	_T("[Err] Machine Error."),				// RCA_MachineCheck,
	_T("[Err] Machine Exception."),			// RCA_MachineExpcetion,
	_T("[Err] Time Out."),						// RCA_TimeOut,
	_T("[Err] No Image."),						// RCA_NoImage,
	_T("[Err] No Barcode."),					// RCA_NoImage,
	_T("[Err] Devices Error."),				// RCA_TransferErr,
	_T("[Err] Pogo Count Over."),				// RCA_PogoCountOver,
	_T("[Err] Equipment Alarm."),				// RCA_EquipAlarm,
	_T("[Err] Sensor Not Recognized."),		// RCA_SensorDetect,
	_T("[Err] Not Confirm to Master."),		// RCA_SensorDetect,
	_T("[Err] Open Short Err"),		// RCA_SensorDetect,

	NULL
};

// 결과 코드 채널 개별 검사
enum enResultCode_Unit
{
	RCC_UnknownError = 0,
	RCC_OK,
	RCC_TestSkip,
	RCC_Invalid_Handle,
	RCC_Invalid_Point,
	RCC_Exception,
	RCC_Safty_Err,
	RCC_DeviceComm_Err,

	RCC_NoBarcode,	// 바코드 없음
	RCC_NoLotInfo,
	RCC_SiteA_Motor_T_Foward_Err,
	RCC_WaitAll_Timeout,
	RCC_WaitAll_Fail,
	RCC_TestAll_Start_Fail,

	RCC_LimitPogoCnt,
	RCC_Max,
};

// 결과 코드 Write Rom
enum enResultCode_TestItem
{
	RCTI_Error = 0,
	RCTI_OK = 1,
	RCTI_TestSkip,
	RCTI_Invalid_Handle,
	RCTI_Exception,
	
};

static LPCTSTR g_szResultCode_TestItem[] =
{
	_T("Error"),				// RCWR_UnknownError = 0,
	_T("OK"),					// RCWR_OK = 1,
	_T("Test Skip"),			// RCWR_TestSkip,
	_T("Invalid Handle"),		// RCWR_Invalid_Handle,
	_T("Exception Err"),				// RCWR_Exception,

	_T("Rom File Err"),			// RCWR_RomFile_Err,
	_T("Sync Fail"),				// RCWR_SyncFail,
	_T("Erase Fail"),			// RCWR_EraseFail,
	_T("Write Fail"),			// RCWR_WriteFail,
	_T("Read Fail"),			// RCWR_ReadFail,
	_T("Verify Fail"),			// RCWR_VerifyFail,
	_T("Comm Err"),				// RCWR_PCB_Comm_Err,
	NULL
};

// 기본 결과 코드
enum enResultCode
{
	RC_Error = 0,
	RC_OK = 1,
	RC_TestSkip,
	RC_Invalid_Handle,
	RC_Exception,

	RC_PowerOn_Err,
	RC_PowerOff_Err,
	RC_EEPROM_Erase_Err,
	RC_EEPROM_Write_Err,
	RC_EEPROM_Read_Err,
	RC_EEPROM_Verify_Err,
	RC_Comm_Err,
	RC_Comm_TimeOut_Err,

	RCC_EMO,
	RCC_ForcedStop,

	// Depth Call(H)
	RC_SA_Check_SafetySensor,
	RC_SA_Check_SocketEmptySensor,
	RC_SA_Check_Chart500EmptySensor,
	RC_SA_Check_Chart200EmptySensor,
	RC_SA_Check_ChartHallCodeEmptySensor,
	RC_SA_Check_CameraCurrent_Err,
	RC_SB_Check_SocketEmptySensor,
	RC_SB_Check_ChartTest_Err,
	RC_SC_Check_SocketEmptySensor,
	RC_SC_Check_ChartTest_Err,
	RC_SD_Check_SocketEmptySensor,
	RC_SD_Check_ChartTest_Err,

	// Depth Test(H)
	RC_SA_Check_Chart1000EmptySensor,
	RC_SA_Check_Chart300EmptySensor,

	RC_Motion_Err_BoardOpen,
	RC_Motion_Err_Alarm,
	RC_Motion_Err_MoveAxis,
	RC_Motion_Err_StageX,
	RC_Motion_Err_StageY,
	RC_Motion_Err_Distance,

	RC_Motor_Err_Alarm,

	RC_Motor_Err_Reset,
	RC_Motor_Err_EStop,
	RC_Motion_Err_MoveAxis_Particle,
	RC_Max,
};

static LPCTSTR g_szResultCode[] =
{
	_T("Error"),								// RC_UnknownError = 0,
	_T("OK"),									// RC_OK = 1,
	_T("Test Skip"),							// RC_TestSkip,
	_T("Invalid Handle"),						// RC_Invalid_Handle,
	_T("Exception Error"),						// RC_Exception,

	_T("Power On Error"),						// RC_PowerOn_Err,
	_T("Power Off Error"),						// RC_PowerOff_Err,
	_T("Erase Error"),							// RC_Erase_Err,
	_T("Write Error"),							// RC_Write_Err,
	_T("Read Error"),							// RC_Read_Err,
	_T("Verify Error"),							// RC_Verify_Err,
	_T("SafetySensor Check Error"),				// RC_ZA_Check_SafetySensor,
	_T("SocketEmpty Sensor Check Error"),		// RC_ZA_Check_SocketEmptySensor,

	_T("Chart 200mm Sensor Check Error"),		// RC_ZA_Check_Chart200EmptySensor,
	_T("Chart 500mm Sensor Check Error"),		// RC_ZA_Check_Chart500EmptySensor,
	_T("Chart HellCode Sensor Check Error"),	// RC_ZA_Check_ChartHallCodeEmptySensor,

	_T("Motion_Err_BoardOpen"),				// RC_Motion_Err_BoardOpen,
	_T("Motion_Err_Alarm"),					// RC_Motion_Err_Alarm,
	_T("Motion_Err_MoveAxis"),				// RC_Motion_Err_MoveAxis,
	_T("Motion_Err_StageX"),			// RC_Motion_Err_StageX,
	_T("Motion_Err_StageY"),			// RC_Motion_Err_StageY,
	_T("Motion_Err_Distance"),			// RC_Motion_Err_Distance,

	_T("[ERR] Motor Alarm"),				// RC_Motor_Err_Alarm,
	_T("[ERR] Motor Reset"),				// RC_Motor_Err_Alarm,
	_T("[ERR] Motor EStop"),				// RC_Motor_Err_Alarm,

	_T("[ERR] Particle Cylinder In"),				// RC_Motor_Err_Alarm,
	NULL
};

enum enFailureCause
{
	Fail_NoError			= 0x00000000,
	Fail_SiteA				= 0x00000001,
	Fail_SiteB				= 0x00000002,
	Fail_SiteC				= 0x00000004,
	Fail_SiteD				= 0x00000008,

	Fail_Barcode			= 0x00000010,
	Fail_EmptyCam			= 0x00000020,
	Fail_LightBrd_Comm		= 0x00000040,
	Fail_CAM_Disconnet		= 0x00000080,

	Fail_CommSignal			= 0x00000100,
	Fail_EmptySocket		= 0x00000200,
	Fail_EmptyChart500		= 0x00000400,
	Fail_EmptyChart200		= 0x00000800,
	Fail_EmptyChartTest		= 0x00001000,
	Fail_EmptyChartHallCode = 0x00002000,
	Fail_Current			= 0x00004000,
	Fail_ChartSetting		= 0x00008000,
	Fail_Safty				= 0x00010000,
	Fail_EmptyChart1000		= 0x00020000,
	Fail_EmptyChart300		= 0x00040000,
	Fail_Depthtest			= 0x00080000,
	Fail_CommTimeout		= 0x00100000,
	Fail_PDEFTest			= 0x00200000,
	Fail_AFTest				= 0x00400000,
	Fail_HHCTest			= 0x00800000,

};

typedef struct _tag_StaticInf
{
	LPCTSTR		szText;
	COLORREF	TextColor;
	COLORREF	BackColor;
	COLORREF	BorderColor;
}ST_StaticInf, *PST_StaticInf;

typedef enum enTestProcess
{
	TP_Idle,
	TP_Run,
	TP_Stop,
	TP_Completed,
	TP_Error,
	TP_ForcedStop, //EMO, 사용자 정지, Stop
	TP_TableRotation,
	TP_Hold,
	TP_DryRun,
};

static ST_StaticInf g_TestProcess[] =
{	// Text					Text Color			Back Color			Border Color
	{ _T("STAND BY"),		RGB(  0,   0,   0), RGB(255, 255, 255), RGB(  0,   0,   0)	},
	{ _T("PROCESSING"),		RGB(255, 255, 255), RGB(255, 192,   0), RGB(188, 140,   0)	},
	{ _T("STOP"),			RGB(255, 255, 255), RGB(  0, 176,  80), RGB( 80, 126,  50)	},
	{ _T("COMPLETION"),		RGB(255, 255, 255), RGB(  0, 176,  80), RGB( 80, 126,  50)	},
	{ _T("ERROR"),			RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("FORCED STOP"),	RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("ROTATE"),			RGB(255, 255, 255), RGB(255, 192, 0),	RGB(188, 140, 0)	},
	{ _T("HOLD"),			RGB(0, 0, 0),		RGB(255, 255, 255), RGB(255, 255, 255)	},
	{ _T("DRY RUN"),		RGB(0, 0, 0),		RGB(255, 255, 255), RGB(255, 255, 255)	},

	NULL
};

typedef enum enTestResult
{
	TR_Fail,
	TR_Pass,
	TR_Testing,
	TR_Empty,
	TR_Skip,
	TR_Init,			
	TR_UserStop,		
	TR_MachineCheck,
	TR_MachineException,
	TR_Timeout,			
	TR_NoImage,
	TR_NoBarcode,
	TR_TransferErr,
	TR_PogoCountOver,
	TR_EquipAlarm,
	TR_SensorDetect,
	TR_MasterCheck,
	TR_OpenShort,
};

static ST_StaticInf g_TestResult[] =
{	// Text					Text Color			Back Color			Border Color
	{ _T("FAIL"),			RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("PASS"),			RGB(255, 255, 255), RGB(  0, 176,  80), RGB( 80, 126,  50)	},
	{ _T("RUNNING"),		RGB(  0,   0,   0),	RGB(237, 125,  49), RGB(  0,   0,   0)	},
	{ _T("EMPTY"),			RGB(  0,   0,   0),	RGB(255, 255, 255), RGB(255, 255, 255)	},
	{ _T("SKIP"),			RGB(255, 255, 255), RGB(255, 255, 255),	RGB( 47,  82, 143)	},
	{ _T("STAND BY"),		RGB(255, 255, 255), RGB(125, 183, 91), RGB(  140,   255,   165)	},
//	{ _T("REWORK"),			RGB(255, 255, 255), RGB(112,  48, 160), RGB( 62,  20, 110)	},
	{ _T("USER STOP"),		RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("MACHINE ERROR"),	RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("MACHINE EXCEPTION"),	RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("TIMEOUT"),		RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)  },
	{ _T("NO IMAGE"),		RGB(255, 255, 255), RGB(192, 0, 0),		RGB(174, 90, 33) },
	{ _T("NO BARCODE"),		RGB(255, 255, 255), RGB(192, 0, 0),		RGB(174, 90, 33) },
	{ _T("DEVICES ERROR"),	RGB(255, 255, 255), RGB(192, 0, 0),		RGB(174, 90, 33) },
	{ _T("POGO COUNT OVER"), RGB(255, 255, 255), RGB(192, 0, 0), RGB(174, 90, 33) },
	{ _T("EQUIPMENT ALARM"), RGB(255, 255, 255), RGB(192, 0, 0), RGB(174, 90, 33) },
	{ _T("SENSOR NOT RECOGNIZED"), RGB(255, 255, 255), RGB(192, 0, 0), RGB(174, 90, 33) },
	{ _T("NOT CONFIRM TO MASTER"), RGB(255, 255, 255), RGB(192, 0, 0), RGB(174, 90, 33) },
	{ _T("OPEN SHORT"), RGB(255, 255, 255), RGB(192, 0, 0), RGB(174, 90, 33) },
	NULL
};

static LPCTSTR g_szAlarmMessage_All[] =
{
	_T("[ALARM] NG."),										
	_T("[ALARM] OK."),										
	_T("[ALARM] Running."),									
	_T("[ALARM] Empty."),									
	_T("[ALARM] Skip."),									
	_T("[ALARM] Stand by."),								
	_T("[ALARM] User Stop : User stopped."),								
	_T("[ALARM] Machine Error : Motor / IO Operation failed."),
	_T("[ALARM] Machine Exception : Motor / IO Position Exception Error."),
	_T("[ALARM] Time Out : Test Time Out."),				
	_T("[ALARM] No Image : No Video Output."),				
	_T("[ALARM] No Barcode : Barcode not entered."),		
	_T("[ALARM] Devices Error : Devices not connected."),
	_T("[ALARM] Pogo Count Over : The Pogo count is over."),
	_T("[ALARM] Equipment Alarm."),
	_T("[ALARM] Sensor not recognized : Cover sensor not recognized."),
	_T("[ALARM] Not Confirm to Master : Please check the Master."),

	NULL
};



typedef enum enStepStatus
{
	TSS_Fail,
	TSS_Pass,
	TSS_Run,
	TSS_Error,
	TSS_Idle,
};

static ST_StaticInf g_StepStatus[] =
{	// Text				Text Color			Back Color			Border Color
	{ _T("Fail"),		RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33) },
	{ _T("Pass"),		RGB(255, 255, 255), RGB(  0, 176,  80), RGB( 80, 126,  50) },
	{ _T("Run"),		RGB(255, 255, 255), RGB(255, 192,   0), RGB( 62,  20, 110) },
	{ _T("Error"),		RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33) },
	{ _T(""),			RGB(  0,   0,   0), RGB(255, 255, 255), RGB(255, 255, 255) },
	NULL
};




#endif // Def_Test_Cm_h__
