﻿//*****************************************************************************
// Filename	: 	Dlg_SelectLot.h
// Created	:	2016/10/28 - 15:58
// Modified	:	2016/10/28 - 15:58
//
// Author	:	
//	
// Purpose	:	
//*****************************************************************************
#ifndef Dlg_SelectLot_h__
#define Dlg_SelectLot_h__

#pragma once

#include "resource.h"
#include "VGStatic.h"

//-----------------------------------------------------------------------------
// CDlg_SelectLot 대화 상자입니다.
//-----------------------------------------------------------------------------
class CDlg_SelectLot : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_SelectLot)

public:
	CDlg_SelectLot(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_SelectLot();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_LOT };

protected:
	virtual void	DoDataExchange			(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnGetMinMaxInfo			(MINMAXINFO* lpMMI);
	virtual BOOL	OnInitDialog			();
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	afx_msg void	OnBnClickedButtonSave	();
	afx_msg void	OnBnClickedButtonCancel	();
	

	DECLARE_MESSAGE_MAP()

	CFont		m_font_Default;
	CFont		m_font_Btn;

	CVGStatic	m_st_Title;
	CVGStatic	m_st_LotName;
	CEdit		m_ed_LotName;
	CVGStatic	m_st_Operator;
	CEdit		m_ed_Operator;
	//CComboBox	m_cb_Operator;
	
	CButton		m_bn_Save;
	CButton		m_bn_Cancel;

	CString		m_szLotName;
	CString		m_szOperatorName;

	CString		m_szModel;
	CString		m_szPath;

	// 이전 LOT 정보 불러오기 설정
	BOOL		m_bLoadPrevLot;

	CStringArray* m_pszArrOperator;
	
	BOOL			CheckLOT();
	virtual void	CheckSave();

public:
	
	void		SetInfo(__in LPCTSTR szReportPath, __in LPCTSTR szModelName)
	{
		m_szPath	= szReportPath;
		m_szModel	= szModelName;

		if (m_szModel.IsEmpty())
		{
			m_szModel = _T("Default");
		}
	};

	void SetLotName(__in LPCTSTR szLotName);
	void SetOperator(__in LPCTSTR szOperator);

	void SetOperatorArray(__in CStringArray* pszArrOperator)
	{
		m_pszArrOperator = pszArrOperator;
	};

	CString		GetLotName()
	{ 
		return m_szLotName; 
	};

	CString		GetOperatorName()
	{ 
		return m_szOperatorName; 
	};

	BOOL		IsLoadPrevLot()
	{
		return m_bLoadPrevLot;
	};
	
	
};
#endif // Dlg_SelectLot_h__
