﻿#ifndef Def_T_Rotate_h__
#define Def_T_Rotatet_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

enum enRotateMode
{
	RT_Mode_Measurement,	// 측정
	RT_Mode_Adjustment,		// 조정
	RT_Mode_MaxNum,
};

static LPCTSTR	g_szRotateMode[] =
{
	_T("Adjustment Mode"),
	_T("Measuring Mode"),
	NULL
};

typedef enum enRotateMarkColor
{
	RT_Mark_B = 0,
	RT_Mark_W,
	RT_Mark_Max,
};

static LPCTSTR	g_szRotateMarkColor[] =
{
	_T("■"),
	_T("□"),
	NULL
};

typedef enum enROI_ROTATE
{
	ROI_RT_LT,
	ROI_RT_RT,
	ROI_RT_LB,
	ROI_RT_RB,
	ROI_RT_Max,
};

static LPCTSTR g_szRoiRotate[] =
{
	_T("LT"),
	_T("RT"),
	_T("LB"),
	_T("RB"),
	NULL
};

// ROI
typedef struct _tag_RegionRotate
{
	// ROI 영역 ( 카메라 [Send] )
	CRect	rtRoi;

	// 검사 모드
	UINT nMarkColor;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp	= rtRoi;

		rtRoi.left		= iPosX - (int)(rtTemp.Width() / 2);
		rtRoi.right		= rtRoi.left + rtTemp.Width();
		rtRoi.top		= iPosY - (int)(rtTemp.Height() / 2);
		rtRoi.bottom	= rtRoi.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left		= rtTemp.CenterPoint().x - (iWidth / 2);
		rtRoi.right		= rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtRoi.top		= rtTemp.CenterPoint().y - (iHeight / 2);
		rtRoi.bottom	= rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	void Reset()
	{
		rtRoi.SetRectEmpty();
		nMarkColor = RT_Mark_B;
	};

	_tag_RegionRotate()
	{
		Reset();
	};

	_tag_RegionRotate& operator= (_tag_RegionRotate& ref)
	{
		rtRoi		= ref.rtRoi;
		nMarkColor	= ref.nMarkColor;

		return *this;
	};

}ST_RegionRotate, *PST_RegionRotate;

typedef struct _tag_Rotate_Op
{
	ST_RegionRotate	stRegionOp[ROI_RT_Max];
	ST_RegionRotate	stTestRegionOp[ROI_RT_Max];
	
	// 검사 모드
	UINT nTestMode;

	// 기준(마스터) 각도
	double dbMasterDegree;

	double dbMinSpc;
	double dbMaxSpc;

	double dbOffset;

	int iSelectROI;

	_tag_Rotate_Op()
	{
		dbMinSpc = 0;
		dbMaxSpc = 0;
		dbOffset = 0;
		dbMasterDegree = 0.00;

		for (UINT nIdx = 0; nIdx < ROI_RT_Max; nIdx++)
		{
			stRegionOp[nIdx].Reset();
			stTestRegionOp[nIdx].Reset();
		}

		iSelectROI = -1;
	};

	_tag_Rotate_Op& operator= (_tag_Rotate_Op& ref)
	{
		dbMinSpc = ref.dbMinSpc;
		dbMaxSpc = ref.dbMaxSpc;
		dbOffset = ref.dbOffset;
		iSelectROI = ref.iSelectROI;
		dbMasterDegree = ref.dbMasterDegree;

		for (UINT nIdx = 0; nIdx < ROI_RT_Max; nIdx++)
		{
			stRegionOp[nIdx] = ref.stRegionOp[nIdx];
			stTestRegionOp[nIdx] = ref.stTestRegionOp[nIdx];
		}

		return *this;
	};

}ST_Rotate_Opt, *PST_Rotate_Opt;

typedef struct _tag_Rotate_Data
{
	// 최종 결과
	UINT nResult;

	double dbValue;

	CPoint ptCenter[ROI_RT_Max];

	void Reset()
	{
		nResult = 3;
		dbValue = 0.0;

		for (UINT nIdx = 0; nIdx < ROI_RT_Max; nIdx++)
		{
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;
		}
	};

	_tag_Rotate_Data()
	{
		Reset();
	};

	_tag_Rotate_Data& operator= (_tag_Rotate_Data& ref)
	{
		nResult = ref.nResult;
		dbValue = ref.dbValue;

		for (UINT nIdx = 0; nIdx < ROI_RT_Max; nIdx++)
		{
			ptCenter[nIdx].x = ref.ptCenter[nIdx].x;
			ptCenter[nIdx].y = ref.ptCenter[nIdx].y;
		}

		return *this;
	};

}ST_Rotate_Result, *PST_Rotate_Result;

#pragma pack(pop)

#endif // Def_Rotate_h__

