﻿//*****************************************************************************
// Filename	: 	Def_EVMS_Cm_Cm.h
// Created	:	2016/9/6 - 14:07
// Modified	:	2016/9/6 - 14:07
//
// Author	:	PiRing
//	
// Purpose	:	LG Innotek 광학 솔루션 설비 전산용 규칙 v2.5
//*****************************************************************************
#ifndef Def_EVMS_Cm_h__
#define Def_EVMS_Cm_h__

#include <afxwin.h>

/*
//-----------------------------------------------------------------------------
1. EVMS System 생성 Folder
//-----------------------------------------------------------------------------
-------------------------------------------------------------------------------
구분				1Level		2Level		3Level		4Level
-------------------------------------------------------------------------------
검사 Program		D ://		EVMS//		TP//		EXE : 실행 File
													ENV : 설비환경 File
													LOG_ECM : ECM수집 Data
													RAW : 내부 RAW데이터
													BAK : Back - Up

설비 구동 Pgm	D://		EVMS//		OP//		EXE : 실행 File
													ENV : 설비 환경 File
													LOG : Log Data
													BAK : Back-Up
-------------------------------------------------------------------------------

2. 검사 / 구동 Program 생성 Folder
	1). D://EVMS//TP(OP)//EXE 에서만 실행 File 동작 할 것 ( Zip File 형태로 제공 )
	2). D://EVMS//TP(OP)//ENV 환경 File은 모델 별/설비 종류별 Link 될 것
	( 화면 구조/기능은 LGIT 와 합의 할 것 )
*/

#define  EVMS_DIR			_T("EVMS")
#define  EVMS_DIR_TP		_T("TP")
#define  EVMS_DIR_OP		_T("OP")
#define  EVMS_DIR_EXE		_T("EXE")
#define  EVMS_DIR_ENV		_T("ENV")
#define  EVMS_DIR_LOG_ECM	_T("LOG_ECM")
#define  EVMS_DIR_RAW		_T("RAW")
#define  EVMS_DIR_BAK		_T("BAK")
#define  EVMS_DIR_LOG		_T("LOG")


#define  EVMS_PATH_EXE_OP	_T("D:\\EVMS\\OP\\EXE\\")
#define  EVMS_PATH_ENV_OP	_T("D:\\EVMS\\OP\\ENV\\")
#define  EVMS_PATH_LOG_OP	_T("D:\\EVMS\\TP\\LOG\\")
//#define  EVMS_PATH_LOG_OP	_T("D:\\EVMS\\OP\\LOG\\")
#define  EVMS_PATH_BAK_OP	_T("D:\\EVMS\\OP\\BAK\\")
#define  EVMS_PATH_EXE_TP	_T("D:\\EVMS\\TP\\EXE\\")
#define  EVMS_PATH_ENV_TP	_T("D:\\EVMS\\TP\\ENV\\")
#define  EVMS_PATH_LOG_TP	_T("D:\\EVMS\\TP\\LOG\\")
#define  EVMS_PATH_BAK_TP	_T("D:\\EVMS\\TP\\BAK\\")
#define  EVMS_PATH_RAW_TP	_T("D:\\EVMS\\TP\\RAW\\")

#define	 LOT_ID_LENGTH		12

// 검사ProgramZip File명
typedef struct _tag_EVMS_TP_ZipFile
{
	char	PgmType[2];	// PGM종류 (TP)
	char	UsageType;	// 용도
	char	Delimeter_1;
	char	Event[2];	// Event
	char	Delimeter_2;
	char	TestType;	// Test유형
	char	Delimeter_3;
	char	Model[5];	// 모델
	char	Delimeter_4;
	char	Year[2];	// 년
	char	Month[2];	// 월
	char	Day[2];		// 일
	char	SN[2];		// 일련번호
}ST_EVMS_TP_ZipFile;

// 구동프로그램Zip File명
typedef struct _tag_EVMS_OP_ZipFile
{
	char	PgmType[2];	// PGM종류 (OP)
	char	Delimeter_1;
	char	Event[2];	// Event
	char	Delimeter_2;
	char	Model[7];	// 설비모델
	char	Delimeter_3;
	char	Year[2];	// 년도
	char	Month[2];	// 월
	char	Day[2];		// 일
	char	SN[2];		// 일련번호
}ST_EVMS_OP_ZipFile;

typedef struct _tag_EVMS_SWVerstion_TP : public ST_EVMS_TP_ZipFile
{
	CString	szPgmName;
	CString	szTestName;
	//CString	szParaName;
}ST_EVMS_SWVerstion_TP;

typedef struct _tag_EVMS_SWVerstion_OP : public ST_EVMS_OP_ZipFile
{
	CString	szPgmName;
	CString	szTestName;
}ST_EVMS_SWVerstion_OP;

typedef enum enEVMS_PGM_Type
{
	EVMS_PGM_TP,
	EVMS_PGM_OP,
};

static LPCTSTR g_szEVMS_PGM_Type[] = 
{
	_T("TP"),
	_T("OP"),
	NULL
};

#endif // Def_EVMS_Cm_h__
