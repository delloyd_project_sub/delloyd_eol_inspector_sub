﻿#ifndef Def_T_Brightness_h__
#define Def_T_Brightness_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

typedef enum enROI_Brightness
{
	ROI_BR_Center,
	ROI_BR_Side_1,
	ROI_BR_Side_2,
	ROI_BR_Side_3,
	ROI_BR_Side_4,
	ROI_BR_Max,
};
	
static LPCTSTR g_szRoiBrightness[] =
{
	_T("Center"),
	_T("Side 1"),
	_T("Side 2"),
	_T("Side 3"),
	_T("Side 4"),
	NULL
};

// ROI
typedef struct _tag_RegionBrightness
{
	// ROI 영역 ( 카메라 [Send] )
	CRect	rtRoi;

	double dMinSpec;
	double dMaxSpec;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp	= rtRoi;

		rtRoi.left		= iPosX - (int)(rtTemp.Width() / 2);
		rtRoi.right		= rtRoi.left + rtTemp.Width();
		rtRoi.top		= iPosY - (int)(rtTemp.Height() / 2);
		rtRoi.bottom	= rtRoi.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left		= rtTemp.CenterPoint().x - (iWidth / 2);
		rtRoi.right		= rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtRoi.top		= rtTemp.CenterPoint().y - (iHeight / 2);
		rtRoi.bottom	= rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	void Reset()
	{
		rtRoi.SetRectEmpty();

		dMinSpec = 0;
		dMaxSpec = 0;
	};

	_tag_RegionBrightness()
	{
		Reset();
	};

	_tag_RegionBrightness& operator= (_tag_RegionBrightness& ref)
	{
		rtRoi = ref.rtRoi;
		dMaxSpec = ref.dMaxSpec;
		dMinSpec = ref.dMinSpec;

		return *this;
	};

}ST_RegionBrightness, *PST_RegionBrightness;

typedef struct _tag_Brightness_Op
{
	ST_RegionBrightness	stRegionOp[ROI_BR_Max];
	int iSelectROI;

	_tag_Brightness_Op()
	{
		for (UINT nIdx = 0; nIdx < ROI_BR_Max; nIdx++)
		{
			stRegionOp[nIdx].Reset();
		}

		iSelectROI = -1;
	};

	_tag_Brightness_Op& operator= (_tag_Brightness_Op& ref)
	{
		for (UINT nIdx = 0; nIdx < ROI_BR_Max; nIdx++)
		{
			stRegionOp[nIdx] = ref.stRegionOp[nIdx];
		}

		iSelectROI = ref.iSelectROI;

		return *this;
	};

}ST_Brightness_Opt, *PST_Brightness_Opt;

typedef struct _tag_Brightness_Data
{
	// 최종 결과
	UINT nResult;

	UINT nEachResult[ROI_BR_Max];
	double dbValue[ROI_BR_Max];

	int iSelectROI;

	void Reset()
	{
		nResult = 3;
		iSelectROI = -1;

		for (UINT nIdx = 0; nIdx < ROI_BR_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			dbValue[nIdx] = 0.0;
		}
	};

	_tag_Brightness_Data()
	{
		Reset();
	};

	_tag_Brightness_Data& operator= (_tag_Brightness_Data& ref)
	{
		nResult = ref.nResult;
		iSelectROI = ref.iSelectROI;

		for (UINT nIdx = 0; nIdx < ROI_BR_Max; nIdx++)
		{
			nEachResult[nIdx] = ref.nEachResult[nIdx];
			dbValue[nIdx] = ref.dbValue[nIdx];
		}

		return *this;
	};

}ST_Brightness_Result, *PST_Brightness_Result;

#pragma pack(pop)

#endif // Def_Brightness_h__

