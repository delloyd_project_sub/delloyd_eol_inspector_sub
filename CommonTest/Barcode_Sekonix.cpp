#include "Barcode_Sekonix.h"


CBarcode_Sekonix::CBarcode_Sekonix()
{
}


CBarcode_Sekonix::~CBarcode_Sekonix()
{
}

//=============================================================================
// Method		: CheckBarcodeType
// Access		: public  
// Returns		: enBarcodeType
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2017/5/4 - 14:51
// Desc.		:
//=============================================================================
enBarcodeType CBarcode_Sekonix::CheckBarcodeType(__in LPCTSTR szBarcode)
{
	CString			szTemp = szBarcode;
	enBarcodeType	nReturn = enBarcodeType::Barcode_UnknownType;

	szTemp.Remove(_T('\r'));
	szTemp.Remove(_T('\n'));
	szTemp.Remove(_T(' '));

	int iLen = szTemp.GetLength();

	switch (iLen)
	{
	case BARCODE_LOT_LEN:	// 공정이력번호
		nReturn = enBarcodeType::Barcode_Lot;
		break;

	case BARCODE_CAM_LEN:	// 제품 임시바코드
		nReturn = enBarcodeType::Barcode_Camera;
		break;

	case BARCODE_LOTEND_LEN:// COMPLETE
		if (0 == szTemp.Compare(BARCODE_LOTEND))
		{
			nReturn = enBarcodeType::Barcode_LotEnd;
		}
		break;

	case BARCODE_SOCKET_LEN:// 소켓 번호
		nReturn = enBarcodeType::Barcode_Socket;
		break;

	case BARCODE_PCB_LEN:
		nReturn = enBarcodeType::Barcode_PCB;
		break;

	default:
		nReturn = enBarcodeType::Barcode_UnknownType;
		break;
	}

	return nReturn;
}
