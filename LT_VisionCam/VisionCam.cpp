//*****************************************************************************
// Filename	: 	VisionCam.cpp
// Created	:	2016/5/10 - 15:25
// Modified	:	2016/5/10 - 15:25
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "VisionCam.h"
#include "PvBufferWriter.h"


#define DEFAULT_PAYLOAD_SIZE ( 1600 * 1200 * 2 )

CVisionCam::CVisionCam()
{
	m_pDevice		= NULL;
	m_pStream		= NULL;
	m_pPipeline		= NULL;
	m_pAcqStateMgr	= NULL;

	//m_lAcquisitionMode	= AcqMode_SingleFrame;
	m_lAcquisitionMode	= AcqMode_Continuous;
	m_bConnected		= FALSE;
	m_bLocked			= FALSE;
	m_bConnectionLost	= FALSE;

	m_nWidth			= 1600;
	m_nHeight			= 1200;
	m_nPixelFormat		= PvPixelType::PvPixelMono8;	//PvPixelType::PvPixelBGRa8;
	m_nPixelSize		= ((m_nPixelFormat & 0x00FF0000) >> 16) / 8;
	
	m_lDevGEVCnt		= 0;
	m_bAcquiringImages	= FALSE;
	
	m_lpbyMono8			= NULL;
	m_Mono8Size			= 0;

	m_lpbyRGB32			= NULL;
	m_RGB32Size			= 0;

	m_hOwnerWnd			= NULL;
	m_nWM_CommStatus	= NULL;
	m_nWM_RecvVideo		= NULL;
	m_byDeviceType		= 0;


	m_bLoopThread		= FALSE;
	m_hEvent_ExitLoop	= NULL;
	m_hThreadLoop		= NULL;

	m_pvBuf				= NULL;

	m_dExposureTime		= 40000.0;
}

CVisionCam::~CVisionCam()
{
	m_bLoopThread = FALSE;

	DeleteThread_Loop();

	TearDown(TRUE);

	if (NULL != m_lpbyMono8)
	{
		delete[] m_lpbyMono8;
	}

	if (NULL != m_lpbyRGB32)
	{
		//delete[] m_lpbyRGB32;
	}
}

//=============================================================================
// Method		: OnParameterUpdate
// Access		: virtual protected  
// Returns		: void
// Parameter	: PvGenParameter * aParameter
// Qualifier	:
// Last Update	: 2016/5/22 - 17:21
// Desc.		:
//=============================================================================
void CVisionCam::OnParameterUpdate(PvGenParameter *aParameter)
{
	ASSERT(NULL != aParameter);

	bool bBufferResize = false;
	PvString lName;
	aParameter->GetName(lName);

 	if (lName == "AcquisitionMode")
 	{
 		bool lAvailable = false, lWritable = false;
 		aParameter->IsAvailable(lAvailable);
 		if (lAvailable)
 		{
 			aParameter->IsWritable(lWritable);
 		}
 
 		PvGenEnum *lEnum = dynamic_cast<PvGenEnum *>(aParameter);
 		if (lEnum != NULL)
 		{
 			int64_t lEEValue = 0;
 			lEnum->GetValue(lEEValue);
 
			m_lAcquisitionMode = lEEValue;
 		}
 	}
	else if (lName == "Width")
	{
		int64_t value;
		m_pDevice->GetParameters()->GetIntegerValue("Width", value);
		if (m_nWidth != (int)value)
		{
			m_nWidth = (int)value;
			bBufferResize = true;
		}

	}
	else if (lName == "Height")
	{
		int64_t value;
		m_pDevice->GetParameters()->GetIntegerValue("Height", value);
		if (m_nHeight != (int)value)
		{
			m_nHeight = (int)value;
			bBufferResize = true;
		}
	}
	else if (lName == "PixelFormat")
	{
		int64_t i64PixelFormat;
		m_pDevice->GetParameters()->GetEnumValue("PixelFormat", i64PixelFormat);
		if (m_nPixelFormat != (int)i64PixelFormat)
		{
			m_nPixelFormat = (int)i64PixelFormat;
			m_nPixelSize = 1;
			m_nPixelSize = ((m_nPixelFormat & 0x00FF0000) >> 16) / 8;
			bBufferResize = true;
		}
	}

	if(bBufferResize)
	{
		if (m_pPipeline->IsStarted())
		{
			m_pPipeline->Stop();
		}

		m_pPipeline->SetBufferSize(m_nWidth * m_nHeight * m_nPixelSize);
		m_pPipeline->Start();
	}
}

//=============================================================================
// Method		: OnAcquisitionStateChanged
// Access		: virtual protected  
// Returns		: void
// Parameter	: PvDevice * aDevice
// Parameter	: PvStream * aStream
// Parameter	: uint32_t aSource
// Parameter	: PvAcquisitionState aState
// Qualifier	:
// Last Update	: 2016/5/22 - 17:21
// Desc.		:
//=============================================================================
void CVisionCam::OnAcquisitionStateChanged(PvDevice* aDevice, PvStream* aStream, uint32_t aSource, PvAcquisitionState aState)
{
	EnableInterface();	
}

//=============================================================================
// Method		: OnBufferQueued
// Access		: virtual protected  
// Returns		: void
// Parameter	: PvBuffer * aBuffer
// Qualifier	:
// Last Update	: 2016/5/22 - 17:22
// Desc.		:
//=============================================================================
void CVisionCam::OnBufferQueued(PvBuffer * aBuffer)
{

}

//=============================================================================
// Method		: OnBufferRetrieved
// Access		: virtual protected  
// Returns		: void
// Parameter	: PvBuffer * aBuffer
// Qualifier	:
// Last Update	: 2016/5/22 - 17:22
// Desc.		:
//=============================================================================
void CVisionCam::OnBufferRetrieved(PvBuffer * aBuffer)
{

}

//=============================================================================
// Method		: OnLinkDisconnected
// Access		: virtual protected  
// Returns		: void
// Parameter	: PvDevice * aDevice
// Qualifier	:
// Last Update	: 2016/5/22 - 17:22
// Desc.		:
//=============================================================================
void CVisionCam::OnLinkDisconnected(PvDevice * aDevice)
{
	m_bConnectionLost = TRUE;

	SendWM_CommStatus(enCommStatus::COMM_CONNECT_DROP);
}

//=============================================================================
// Method		: OnLinkReconnected
// Access		: virtual protected  
// Returns		: void
// Parameter	: PvDevice * aDevice
// Qualifier	:
// Last Update	: 2016/5/22 - 17:22
// Desc.		:
//=============================================================================
void CVisionCam::OnLinkReconnected(PvDevice * aDevice)
{
	SendWM_CommStatus(enCommStatus::COMM_CONNECTED);
}

//=============================================================================
// Method		: OnEvent
// Access		: virtual protected  
// Returns		: void
// Parameter	: PvDevice * aDevice
// Parameter	: uint16_t aEventID
// Parameter	: uint16_t aChannel
// Parameter	: uint64_t aBlockID
// Parameter	: uint64_t aTimestamp
// Parameter	: const void * aData
// Parameter	: uint32_t aDataLength
// Qualifier	:
// Last Update	: 2016/5/22 - 17:22
// Desc.		:
//=============================================================================
void CVisionCam::OnEvent(PvDevice * aDevice, uint16_t aEventID, uint16_t aChannel, uint64_t aBlockID, uint64_t aTimestamp, const void * aData, uint32_t aDataLength)
{
}

//=============================================================================
// Method		: Loop
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:26
// Desc.		:
//=============================================================================
void CVisionCam::Loop()
{
	bool lFirstTimeout = true;

	int64_t lImageCountVal = 0;
	double lFrameRateVal = 0.0;
	double lBandwidthVal = 0.0;
	// Retrieve next buffer     
// 	PvBuffer *lBuffer = NULL;
// 	PvResult  lOperationResult;
// 	PvResult lResult;

	// Get stream parameters
	//PvGenParameterArray *lStreamParams = m_pStream->GetParameters();
	// Map a few GenICam stream stats counters
	//PvGenFloat *lFrameRate = dynamic_cast<PvGenFloat *>(lStreamParams->Get("AcquisitionRate"));
	//PvGenFloat *lBandwidth = dynamic_cast<PvGenFloat *>(lStreamParams->Get("Bandwidth"));

	// Acquire images until the user instructs us to stop.
	while (m_bLoopThread)
	{
		// If connection flag is up, teardown device/stream
		if (m_bConnectionLost && (m_pDevice != NULL))
		{
			// Device lost: no need to stop acquisition
			TearDown(false);
		}

		// If the device is not connected, attempt reconnection
		if ((m_pDevice == NULL) || (FALSE == m_bConnected))
		{
			if (ConnectDevice())
			{
				// Device is connected, open the stream
				if (OpenStream())
				{
					// Device is connected, stream is opened: start acquisition
					if (!StartAcquisition())
					{
						TearDown(false);
					}
				}
				else
				{
					TearDown(false);
				}
			}
		}

		// If still no device, no need to continue the loop
		if (m_pDevice == NULL)
		{
			Sleep(100);
			continue;
		}

		if ((m_pStream != NULL) && m_pStream->IsOpen() && (m_pPipeline != NULL) && m_pPipeline->IsStarted())
		{
			// Retrieve next buffer
			PvBuffer *lBuffer = NULL;
			PvResult  lOperationResult;
			PvResult  lResult = m_pPipeline->RetrieveNextBuffer(&lBuffer, 1000, &lOperationResult);
			//lResult = m_pStream->RetrieveBuffer( &lBuffer, &lOperationResult, 1000 );
			//lResult = m_pPipeline->RetrieveNextBuffer(&lBuffer, 1000, &lOperationResult);
			if (lResult.IsOK())
			{
				if (lOperationResult.IsOK())
				{
					// We now have a valid buffer. This is where you would typically process the buffer.
					// -----------------------------------------------------------------------------------------
					// ... (5,038,848)
// 					int64_t lImageCountVal = 0;
// 					m_pStream->GetParameters()->GetIntegerValue("BlockCount", lImageCountVal);
// 					m_pStream->GetParameters()->GetFloatValue("AcquisitionRate", lFrameRateVal);
// 					m_pStream->GetParameters()->GetFloatValue("Bandwidth", lBandwidthVal);

					//lFrameRate->GetValue(lFrameRateVal);
					//lBandwidth->GetValue(lBandwidthVal);

					// If the buffer contains an image, display width and height.
					//TRACE(_T("BlockID: 0x%016X \n"), lBuffer->GetBlockID());

					if (lBuffer->GetPayloadType() == PvPayloadTypeImage)
					{
						// Get image specific buffer interface
						PvImage *lImage = lBuffer->GetImage();

						// Read width, height.
						m_nWidth = lImage->GetWidth();
						m_nHeight = lImage->GetHeight();
						//TRACE(_T("W: %d, H: %d \n"), m_nWidth, m_nHeight);
						
						if ((0 < m_nWidth) && (0 < m_nHeight))
						{
							// m_pvBuf = lBuffer;

							m_bAcquiringImages = TRUE;
														
							uint32_t nBufSize = lBuffer->GetAcquiredSize();
							uint8_t* pbyBuf = lBuffer->GetDataPointer();

							// Mono8 포맷을 RGB32 포맷으로 변환
							if (ConvertMonoToRGB32(pbyBuf, nBufSize))
							{
								// 오너윈도우로 영상 수신 메세지 통보
								SendWM_RecvVideo();
							}
						}
					}
					else
					{
						TRACE(_T(" (buffer does not contain image)\n"));
					}
					//TRACE(_T("Frame Rate: %f FPS,  %f Mb/S \n"), lFrameRateVal, (lBandwidthVal / 1000000.0));
				}
				else
				{
					// Non OK operational result
#ifdef UNICODE
					TRACE(_T("Non OK operational result: %s \n"), lOperationResult.GetCodeString().GetUnicode());
#else
					TRACE(_T("Non OK operational result: %s \n"), lOperationResult.GetCodeString().GetAscii());
#endif
				}

				// Release the buffer back to the pipeline
				m_pPipeline->ReleaseBuffer(lBuffer);
			}
			else
			{
				// Retrieve buffer failure
#ifdef UNICODE
				TRACE(_T("Retrieve buffer failure: %s \n"), lResult.GetCodeString().GetUnicode());
#else
				TRACE(_T("Retrieve buffer failure: %s \n"), lResult.GetCodeString().GetAscii());
#endif
			}

			//Sleep(70);
			Sleep(200); // CPU 부하 감소시키기 위해서 딜레이 늘림
		}
		else
		{
			// No stream/pipeline, must be in recovery. Wait a bit...
			Sleep(100);
		}
	}
}

//=============================================================================
// Method		: SetListAcquisitionMode
// Access		: protected  
// Returns		: void
// Parameter	: PvGenEnum * lMode
// Qualifier	:
// Last Update	: 2016/6/9 - 15:27
// Desc.		:
//=============================================================================
void CVisionCam::SetListAcquisitionMode(PvGenEnum *lMode)
{
	// Get acquisition mode GenICam parameter
	if (NULL == lMode)
		return;

	int64_t lEntriesCount = 0;
	lMode->GetEntriesCount(lEntriesCount);

	// Fill acquisition mode combo box
	//mModeCombo.ResetContent();
	for (uint32_t i = 0; i < lEntriesCount; i++)
	{
		const PvGenEnumEntry *lEntry = NULL;
		lMode->GetEntryByIndex(i, &lEntry);

		if (lEntry->IsAvailable())
		{
			PvString lEEName;
			lEntry->GetName(lEEName);

			int64_t lEEValue;
			lEntry->GetValue(lEEValue);

			TRACE(_T("%d: %s\n"), static_cast<DWORD_PTR>(lEEValue), lEEName.GetUnicode());			
		}
	}

	// Set mode combo box to value currently used by the device
	int64_t lValue = 0;
	lMode->GetValue(lValue);

	m_lAcquisitionMode = lValue;

}

//=============================================================================
// Method		: ChangeAcquisitionMode
// Access		: protected  
// Returns		: void
// Parameter	: uint64_t lValue
// Qualifier	:
// Last Update	: 2016/6/9 - 15:27
// Desc.		:
//=============================================================================
void CVisionCam::ChangeAcquisitionMode(uint64_t lValue)
{
	if (!m_pDevice->IsConnected())
	{
		return;
	}

	PvGenParameterArray *lDeviceParams = m_pDevice->GetParameters();

	//uint64_t lValue = mModeCombo.GetItemData(mModeCombo.GetCurSel());

	PvResult lResult = lDeviceParams->SetEnumValue("AcquisitionMode", lValue);
	
	if (lResult.IsOK())
	{
		m_lAcquisitionMode = lValue;
	}
	else
	{
		TRACE(_T("Unable to set AcquisitionMode value.\n"));
	}
}

//=============================================================================
// Method		: AcquireImages
// Access		: protected  
// Returns		: void
// Parameter	: PvDevice * aDevice
// Parameter	: PvStream * aStream
// Parameter	: PvPipeline * aPipeline
// Qualifier	:
// Last Update	: 2016/6/9 - 15:27
// Desc.		:
//=============================================================================
void CVisionCam::AcquireImages(PvDevice *aDevice, PvStream *aStream, PvPipeline *aPipeline)
{
	m_bAcquiringImages = FALSE;

	// Get device parameters need to control streaming
	PvGenParameterArray *lDeviceParams = aDevice->GetParameters();

	// Map the GenICam AcquisitionStart and AcquisitionStop commands
	PvGenCommand *lStart = dynamic_cast<PvGenCommand *>(lDeviceParams->Get("AcquisitionStart"));
	PvGenCommand *lStop = dynamic_cast<PvGenCommand *>(lDeviceParams->Get("AcquisitionStop"));

	// Note: the pipeline must be initialized before we start acquisition
	//TRACE (_T("Starting pipeline\n"));
	//aPipeline->Start();
	aPipeline->Reset();

	// Get stream parameters
	PvGenParameterArray *lStreamParams = aStream->GetParameters();

	// Map a few GenICam stream stats counters
	PvGenFloat *lFrameRate = dynamic_cast<PvGenFloat *>(lStreamParams->Get("AcquisitionRate"));
	PvGenFloat *lBandwidth = dynamic_cast<PvGenFloat *>(lStreamParams->Get("Bandwidth"));

	// Enable streaming and send the AcquisitionStart command
	TRACE (_T("Enabling streaming and sending AcquisitionStart command.\n"));
	aDevice->StreamEnable();
	lStart->Execute();

	double lFrameRateVal = 0.0;
	double lBandwidthVal = 0.0;
	PvBuffer *lBuffer = NULL;
	PvResult lOperationResult;
	PvResult lResult;

	DWORD dwStartTick = GetTickCount();
	DWORD dwCurreTick = 0;
	DWORD dwTimeCheck = 0;

	TRACE (_T("Acquire images until the user instructs us to stop.\n"));
	while ((m_bConnected) && (!m_bLocked))
	{
		dwCurreTick = GetTickCount();
		if (dwCurreTick < dwStartTick)
		{
			dwTimeCheck = 0xFFFFFFFF - dwStartTick + dwCurreTick;
		}
		else
		{
			dwTimeCheck = dwCurreTick - dwStartTick;
		}

		// Timeout
		if (10000 < dwTimeCheck)
		{
			TRACE(_T("AcquireImages Timeout!! \n"));
			break;
		}

		// Retrieve next buffer
		//lResult = aStream->RetrieveBuffer( &lBuffer, &lOperationResult, 1000 );
		lResult = aPipeline->RetrieveNextBuffer(&lBuffer, 1000, &lOperationResult);
		if (lResult.IsOK())
		{
			if (lOperationResult.IsOK())
			{
				PvPayloadType lType;

				//
				// We now have a valid buffer. This is where you would typically process the buffer.
				// -----------------------------------------------------------------------------------------
				// ... (5,038,848)
				int64_t lImageCountVal = 0;
				m_pStream->GetParameters()->GetIntegerValue("BlockCount", lImageCountVal);
				m_pStream->GetParameters()->GetFloatValue("AcquisitionRate", lFrameRateVal);
				m_pStream->GetParameters()->GetFloatValue("Bandwidth", lBandwidthVal);

				lFrameRate->GetValue(lFrameRateVal);
				lBandwidth->GetValue(lBandwidthVal);

				// If the buffer contains an image, display width and height.
				uint32_t lWidth = 0, lHeight = 0;
				lType = lBuffer->GetPayloadType();
				
				TRACE(_T("BlockID: 0x%016X \n"), lBuffer->GetBlockID());
				if (lType == PvPayloadTypeImage)
				{
					m_bAcquiringImages = TRUE;
					uint32_t nBufSize	= 0;
// 					nBufSize = lBuffer->GetPayloadSize();
// 					nBufSize = lBuffer->GetRequiredSize();
					nBufSize			= lBuffer->GetAcquiredSize();
					uint8_t* pbyBuf		= lBuffer->GetDataPointer();
					ConvertMonoToRGB32(pbyBuf, nBufSize);

					// Get image specific buffer interface.
					PvImage *lImage = lBuffer->GetImage();

					// Read width, height.
					lWidth = lImage->GetWidth();
					lHeight = lImage->GetHeight();
					TRACE(_T("W: %d, H: %d \n"), lWidth, lHeight);
				}
				else 
				{
					TRACE (_T(" (buffer does not contain image)\n"));
				}
				TRACE(_T("Frame Rate: %f FPS,  %f Mb/S \n"), lFrameRateVal, (lBandwidthVal / 1000000.0));

				break;
			}
			else
			{
				// Non OK operational result
#ifdef UNICODE
				TRACE(_T("Non OK operational result: %s \n"), lOperationResult.GetCodeString().GetUnicode());
#else
				TRACE(_T("Non OK operational result: %s \n"), lOperationResult.GetCodeString().GetAscii());
#endif
			}

			// Release the buffer back to the pipeline
			aPipeline->ReleaseBuffer(lBuffer);
		}
		else
		{
			// Retrieve buffer failure
#ifdef UNICODE
			TRACE(_T("Retrieve buffer failure: %s \n"), lResult.GetCodeString().GetUnicode());
#else
			TRACE(_T("Retrieve buffer failure: %s \n"), lResult.GetCodeString().GetAscii());
#endif
		}
	}

	// Tell the device to stop sending images.
	TRACE (_T("Sending AcquisitionStop command to the device\n"));
	lStop->Execute();

	// Disable streaming on the device
	TRACE (_T("Disable streaming on the controller.\n"));
	aDevice->StreamDisable();

	// Stop the pipeline
	//TRACE (_T("Stop pipeline\n"));
	//aPipeline->Stop();
}

//=============================================================================
// Method		: EnableInterface
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::EnableInterface()
{
	if ((m_pDevice != NULL) && m_pDevice->IsConnected())
		m_bConnected = TRUE;
	else
		m_bConnected = FALSE;

	m_bLocked = FALSE;
	if (m_pAcqStateMgr != NULL)
	{
		m_bLocked = m_pAcqStateMgr->GetState() == PvAcquisitionStateLocked;
	}
}

//=============================================================================
// Method		: SelectDevice
// Access		: protected  
// Returns		: const PvDeviceInfo *
// Parameter	: PvSystem * aPvSystem
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
const PvDeviceInfo *CVisionCam::SelectDevice(PvSystem *aPvSystem)
{
	const PvDeviceInfo *lDeviceInfo = NULL;
	PvResult lResult;

	if (NULL != aPvSystem)
	{
		// Get the selected device information.
		//lDeviceInfo = PvSelectDevice(*aPvSystem);
	}

	return lDeviceInfo;
}

//=============================================================================
// Method		: ConnectDevice
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
BOOL CVisionCam::ConnectDevice()
{
	//std::cout << "--> ConnectDevice " << mConnectionID.GetAscii() << std::endl;
	if (NULL != m_pDevice)
		DisconnectDevice();

	// Connect to the selected Device
	PvResult lResult = PvResult::Code::INVALID_PARAMETER;
	m_pDevice = PvDevice::CreateAndConnect(m_szConnectionID, &lResult);
	if (!lResult.IsOK())
	{
		return FALSE;
	}	

	// Register this class as an event sink for PvDevice call-backs
	m_pDevice->RegisterEventSink(this);

	// Clear connection lost flag as we are now connected to the device
	m_bConnected = TRUE;
	m_bConnectionLost = FALSE;

	SendWM_CommStatus(enCommStatus::COMM_CONNECTED);

	PvGenParameterArray *lDeviceParams = m_pDevice->GetParameters();
	lResult = lDeviceParams->SetFloatValue("ExposureTime", m_dExposureTime);
// 	if (!lResult.IsOK())
// 	{
// 		return FALSE;
// 	}

	return TRUE;
}

//=============================================================================
// Method		: DisconnectDevice
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::DisconnectDevice()
{
	//std::cout << "--> DisconnectDevice" << std::endl;

	if (m_pDevice != NULL)
	{
		if (m_pDevice->IsConnected())
		{
			// Unregister event sink (call-backs).
			m_pDevice->UnregisterEventSink(this);
		}

		PvDevice::Free(m_pDevice);
		m_pDevice = NULL;

		m_bConnected = FALSE;
		SendWM_CommStatus(enCommStatus::COMM_DISCONNECT);
	}
}

//=============================================================================
// Method		: OpenStream
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
BOOL CVisionCam::OpenStream()
{
	//std::cout << "--> OpenStream" << std::endl;

	// Creates and open the stream object based on the selected device.
	PvResult lResult = PvResult::Code::INVALID_PARAMETER;
	m_pStream = PvStream::CreateAndOpen(m_szConnectionID, &lResult);
	if (!lResult.IsOK())
	{
		//std::cout << "Unable to open the stream" << std::endl;
		return FALSE;
	}

	m_pPipeline = new PvPipeline(m_pStream);

	// Reading payload size from device
	int64_t lSize = m_pDevice->GetPayloadSize();

	// Create, init the PvPipeline object
	m_pPipeline->SetBufferSize(static_cast<uint32_t>(lSize));
	m_pPipeline->SetBufferCount(16);

	// The pipeline needs to be "armed", or started before  we instruct the device to send us images
	lResult = m_pPipeline->Start();
	if (!lResult.IsOK())
	{
		//std::cout << "Unable to start pipeline" << std::endl;
		return FALSE;
	}

	// Only for GigE Vision, if supported
	PvGenBoolean *lRequestMissingPackets = dynamic_cast<PvGenBoolean *>(m_pStream->GetParameters()->GetBoolean("RequestMissingPackets"));
	if ((lRequestMissingPackets != NULL) && lRequestMissingPackets->IsAvailable())
	{
		// Disabling request missing packets.
		lRequestMissingPackets->SetValue(false);
	}

	return TRUE;
}

//=============================================================================
// Method		: CloseStream
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::CloseStream()
{
	//std::cout << "--> CloseStream" << std::endl;

	if (m_pPipeline != NULL)
	{
		if (m_pPipeline->IsStarted())
		{
			if (!m_pPipeline->Stop().IsOK())
			{
				//std::cout << "Unable to stop the pipeline." << std::endl;
			}
		}

		delete m_pPipeline;
		m_pPipeline = NULL;
	}

	if (m_pStream != NULL)
	{
		if (m_pStream->IsOpen())
		{
			if (!m_pStream->Close().IsOK())
			{
				//std::cout << "Unable to stop the stream." << std::endl;
			}
		}

		PvStream::Free(m_pStream);
		m_pStream = NULL;
	}
}

//=============================================================================
// Method		: TearDown
// Access		: protected  
// Returns		: void
// Parameter	: bool aStopAcquisition
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::TearDown(bool aStopAcquisition)
{
	if (aStopAcquisition)
	{
		StopAcquisition();
	}

	CloseStream();
	DisconnectDevice();
}


//=============================================================================
// Method		: StartAcquisition
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
BOOL CVisionCam::StartAcquisition()
{
	//std::cout << "--> StartAcquisition" << std::endl;

	// Flush packet queue to make sure there is no left over from previous disconnect event
	PvStreamGEV* lStreamGEV = dynamic_cast<PvStreamGEV*>(m_pStream);
	if (lStreamGEV != NULL)
	{
		lStreamGEV->FlushPacketQueue();
	}

	// Set streaming destination (only GigE Vision devces)
	PvDeviceGEV* lDeviceGEV = dynamic_cast<PvDeviceGEV*>(m_pDevice);
	if (lDeviceGEV != NULL)
	{
		// If using a GigE Vision, it is same to assume the stream object is GigE Vision as well
		PvStreamGEV* lStreamGEV = static_cast<PvStreamGEV*>(m_pStream);

		// Have to set the Device IP destination to the Stream
		PvResult lResult = lDeviceGEV->SetStreamDestination(lStreamGEV->GetLocalIPAddress(), lStreamGEV->GetLocalPort());
		if (!lResult.IsOK())
		{
			//std::cout << "Setting stream destination failed" << std::endl;
			return FALSE;
		}
	}

	// Enables stream before sending the AcquisitionStart command.
	m_pDevice->StreamEnable();

	// The pipeline is already "armed", we just have to tell the device to start sending us images
	PvResult lResult = m_pDevice->GetParameters()->ExecuteCommand("AcquisitionStart");
	if (!lResult.IsOK())
	{
		//std::cout << "Unable to start acquisition" << std::endl;
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: StopAcquisition
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
BOOL CVisionCam::StopAcquisition()
{
	//std::cout << "--> StopAcquisition" << std::endl;

	if (NULL != m_pDevice)
	{
		// Tell the device to stop sending images.
		m_pDevice->GetParameters()->ExecuteCommand("AcquisitionStop");

		// Disable stream after sending the AcquisitionStop command.
		m_pDevice->StreamDisable();

		PvDeviceGEV* lDeviceGEV = dynamic_cast<PvDeviceGEV*>(m_pDevice);
		if (lDeviceGEV != NULL)
		{
			// Reset streaming destination (optional...)
			lDeviceGEV->ResetStreamDestination();
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckConnection
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
BOOL CVisionCam::CheckConnection()
{
	if (m_bConnectionLost && (m_pDevice != NULL))
	{
	 	// Device lost: no need to stop acquisition
	 	TearDown(FALSE);
	}
	 
	if (m_pDevice == NULL)
	{
	 	if (ConnectDevice())
	 	{
	 		// Device is connected, open the stream
	 		if (OpenStream())
	 		{
	 			// Device is connected, stream is opened: start acquisition
				if (!StartAcquisition())
				{
					TearDown(FALSE);
				}
	 		}
	 		else
	 		{
	 			TearDown(FALSE);
	 		}
	 	}
	}

	//If still no device, no need to continue the loop
	if (m_pDevice == NULL)
	{
	 	return FALSE;
	}
	return TRUE;
}

//=============================================================================
// Method		: Thread_Loop
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: LPVOID lParam
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
UINT WINAPI CVisionCam::Thread_Loop(LPVOID lParam)
{
	ASSERT(NULL != lParam);

	CVisionCam* pThis = (CVisionCam*)lParam;
	
	while (pThis->m_bLoopThread)
	{
		__try
		{
			pThis->Loop();
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
			TRACE(_T("*** Exception Error : CVisionCam::Thread_Loop()\n"));
		}

		if (pThis->m_bLoopThread)
		{
			pThis->TearDown(true);
			Sleep(100);
			pThis->ConnectDevice();
		}
	}
	
	TRACE(_T("쓰레드 종료 : CVisionCam::Thread_Loop Loop Exit\n"));
	return TRUE;
}

//=============================================================================
// Method		: CreateThread_Loop
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::CreateThread_Loop()
{
	m_bLoopThread = TRUE;

	m_hThreadLoop = HANDLE(_beginthreadex(NULL, 0, Thread_Loop, this, 0, NULL));

}

//=============================================================================
// Method		: DeleteThread_Loop
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::DeleteThread_Loop()
{
	m_bLoopThread = FALSE;

// 	if (FALSE == SetEvent(m_hEvent_ExitLoop))
// 	{
// 		TRACE(_T("Set m_hEvent_ExitLoop Event 실패!!\n"));
// 	}

	Sleep(200);

	DWORD dwExitCode = NULL;
	GetExitCodeThread(m_hThreadLoop, &dwExitCode);

	if (STILL_ACTIVE == dwExitCode)
	{
		TRACE(_T("쓰레드 강제 종료 : CVisionCam::Thread_Loop Loop Exit\n"));
		TerminateThread(m_hThreadLoop, dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TRACE(_T("쓰레드 강제 종료 실패: CVisionCam::Thread_Loop Loop Exit\n"));
		}
		WaitForSingleObject(m_hThreadLoop, WAIT_ABANDONED);
	}

	CloseHandle(m_hThreadLoop);
	m_hThreadLoop = NULL;
}

//=============================================================================
// Method		: ConvertMonoToRGB32
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const LPBYTE pbyInMono
// Parameter	: __in DWORD dwBufSize
// Qualifier	:
// Last Update	: 2016/6/9 - 15:20
// Desc.		:
//=============================================================================
BOOL CVisionCam::ConvertMonoToRGB32(__in const LPBYTE pbyInMono, __in DWORD dwBufSize)
{
	if (NULL == pbyInMono)
		return FALSE;

	if (0 == dwBufSize)
		return FALSE;

	DWORD dwRGBSize = dwBufSize * sizeof(RGBQUAD);

	if (dwRGBSize != m_RGB32Size)
	{
		m_RGB32Size = dwRGBSize;

		if (NULL != m_lpbyRGB32)
		{
			delete[] m_lpbyRGB32;
			m_lpbyRGB32 = NULL;
		}

		m_lpbyRGB32 = new BYTE[m_RGB32Size];
	}

	//IsBadReadPtr();IsBadWritePtr();

	UINT nRGBOffset = 0;
	for (UINT nOffset = 0; nOffset < dwBufSize; nOffset++)
	{
		m_lpbyRGB32[nRGBOffset++] = pbyInMono[nOffset];
		m_lpbyRGB32[nRGBOffset++] = pbyInMono[nOffset];
		m_lpbyRGB32[nRGBOffset++] = pbyInMono[nOffset];
		m_lpbyRGB32[nRGBOffset++] = 255;
	}

	return TRUE;
}

//=============================================================================
// Method		: SendWM_CommStatus
// Access		: protected  
// Returns		: void
// Parameter	: enCommStatus nStatus
// Qualifier	:
// Last Update	: 2016/5/23 - 9:57
// Desc.		:
//=============================================================================
void CVisionCam::SendWM_CommStatus(enCommStatus nStatus)
{
	m_CommStatus = nStatus;

	if ((NULL != m_hOwnerWnd) && (NULL != m_nWM_CommStatus))
		::SendNotifyMessage(m_hOwnerWnd, m_nWM_CommStatus, (WPARAM)m_byDeviceType, (LPARAM)m_CommStatus);
}

void CVisionCam::SendWM_RecvVideo()
{
	if ((NULL != m_hOwnerWnd) && (NULL != m_nWM_RecvVideo))
		::SendNotifyMessage(m_hOwnerWnd, m_nWM_RecvVideo, (WPARAM)m_byDeviceType, (LPARAM)0);
}

//=============================================================================
// Method		: SaveImage
// Access		: protected  
// Returns		: BOOL
// Parameter	: LPCTSTR szPath
// Parameter	: PvBuffer * aBuffer
// Parameter	: bool aUpdateStats
// Qualifier	:
// Last Update	: 2016/5/31 - 15:23
// Desc.		:
//=============================================================================
BOOL CVisionCam::SaveImage(LPCTSTR szPath, PvBuffer *aBuffer, bool aUpdateStats)
{
	return true;
}

//=============================================================================
// Method		: SetOwner
// Access		: public  
// Returns		: void
// Parameter	: HWND hOwnerWnd
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::SetOwner(HWND hOwnerWnd)
{
	m_hOwnerWnd = hOwnerWnd;
}

//=============================================================================
// Method		: SetWinMessage
// Access		: public  
// Returns		: void
// Parameter	: UINT nWM_CommStatus
// Parameter	: UINT nWM_RecvVideo
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::SetWinMessage(UINT nWM_CommStatus, UINT nWM_RecvVideo)
{
	m_nWM_CommStatus = nWM_CommStatus;
	m_nWM_RecvVideo	 = nWM_RecvVideo;
}

//=============================================================================
// Method		: SetDeviceType
// Access		: public  
// Returns		: void
// Parameter	: BYTE byType
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::SetDeviceType(BYTE byType)
{
	m_byDeviceType = byType;
}

//=============================================================================
// Method		: DeviceFinding
// Access		: public  
// Returns		: int
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
int CVisionCam::DeviceFinding()
{
	PvResult lResult;
	const PvDeviceInfo* lLastDeviceInfo = NULL;
	m_lDevGEVCnt = 0;

	// Find all devices on the network.
	PvSystem lSystem;
	lResult = lSystem.Find();
	if (!lResult.IsOK())
	{
		TRACE(_T("PvSystem::Find Error: %s\n"), lResult.GetCodeString().GetAscii());
		return -1;
	}

	// Go through all interfaces 
	uint32_t lInterfaceCount = lSystem.GetInterfaceCount();
	for (uint32_t iInterfaceIdx = 0; iInterfaceIdx < lInterfaceCount; iInterfaceIdx++)
	{
		TRACE(_T("Interface : %d\n"), iInterfaceIdx);

		// Get pointer to the interface.
		const PvInterface* lInterface = lSystem.GetInterface(iInterfaceIdx);

		// Is it a PvNetworkAdapter?
		const PvNetworkAdapter* lNIC = dynamic_cast<const PvNetworkAdapter*>(lInterface);
		if (lNIC != NULL)
		{
#ifdef UNICODE
			TRACE(_T("  MAC Address: %s\n"), lNIC->GetMACAddress().GetUnicode());
			TRACE(_T("  IP Address: %s\n"), lNIC->GetIPAddress().GetUnicode());
			TRACE(_T("  Subnet Mask: %s\n"), lNIC->GetSubnetMask().GetUnicode());
#else
			TRACE(_T("  MAC Address: %s\n"), lNIC->GetMACAddress().GetAscii());
			TRACE(_T("  IP Address: %s\n"), lNIC->GetIPAddress().GetAscii());
			TRACE(_T("  Subnet Mask: %s\n"), lNIC->GetSubnetMask().GetAscii());
#endif
		}

		// Go through all the devices attached to the interface
		uint32_t lDeviceCount = lInterface->GetDeviceCount();		 
		for (uint32_t iDevIdx = 0; iDevIdx < lDeviceCount; iDevIdx++)
		{
			const PvDeviceInfo *lDeviceInfo = lInterface->GetDeviceInfo(iDevIdx);

			TRACE(_T("  Device : %d\n"), iDevIdx);
#ifdef UNICODE
			TRACE(_T("    Display ID: %s\n"), lDeviceInfo->GetDisplayID().GetUnicode());
#else
			TRACE(_T("    Display ID: %s\n"), lDeviceInfo->GetDisplayID().GetAscii());
#endif

			const PvDeviceInfoGEV* lDeviceInfoGEV = dynamic_cast<const PvDeviceInfoGEV*>(lDeviceInfo);

			if (lDeviceInfoGEV != NULL) // Is it a GigE Vision device?
			{
#ifdef UNICODE
				TRACE(_T("    MAC Address: %s\n"), lDeviceInfoGEV->GetMACAddress().GetUnicode());
				TRACE(_T("    IP Address: %s\n"), lDeviceInfoGEV->GetIPAddress().GetUnicode());
				TRACE(_T("    Serial number: %s\n"), lDeviceInfoGEV->GetSerialNumber().GetUnicode());
#else
				TRACE(_T("    MAC Address: %s\n"), lDeviceInfoGEV->GetMACAddress().GetAscii());
				TRACE(_T("    IP Address: %s\n"), lDeviceInfoGEV->GetIPAddress().GetAscii());
				TRACE(_T("    Serial number: %s\n"), lDeviceInfoGEV->GetSerialNumber().GetAscii());
#endif
				++m_lDevGEVCnt;
				lLastDeviceInfo = lDeviceInfo;
			}
		}
	}

	return m_lDevGEVCnt;

	// Connect to the last device found
// 	if (lLastDeviceInfo != NULL)
// 	{
// 		const PvDeviceInfoGEV* pDevInfGEV = dynamic_cast<const PvDeviceInfoGEV*>(lLastDeviceInfo);
// 
// 		TRACE(_T("Connecting to %s\n"), lLastDeviceInfo->GetDisplayID().GetAscii());
// 
// 		// Creates and connects the device controller based on the selected device.
// 		PvDevice* lDevice = PvDevice::CreateAndConnect(lLastDeviceInfo, &lResult);
// 		if (!lResult.IsOK())
// 		{
// 			TRACE(_T("Unable to connect to %s\n"), lLastDeviceInfo->GetDisplayID().GetAscii());
// 		}
// 		else
// 		{
// 			TRACE(_T("Successfully connected to %s\n"), lLastDeviceInfo->GetDisplayID().GetAscii());
// 			TRACE(_T("Disconnecting the device %s\n"), lLastDeviceInfo->GetDisplayID().GetAscii());
// 			PvDevice::Free(lDevice);
// 		}
// 	}
// 	else
// 	{
// 		TRACE(_T("No device found\n"));
// 	}

//	return 0;
}

//=============================================================================
// Method		: Connect
// Access		: public  
// Returns		: BOOL
// Parameter	: LPCTSTR szIPAddress
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
BOOL CVisionCam::Connect(LPCTSTR szIPAddress)
{
	m_szConnectionID = szIPAddress;
	//PvString aInfo = _T("192.168.0.15");

	// Just in case we came here still connected...
	Disconnect();

	// Device connection, packet size negotiation and stream opening
	PvResult lResult = PvResult::Code::NOT_CONNECTED;

	// Connect device -----------------------------------------------
	m_pDevice = PvDevice::CreateAndConnect(m_szConnectionID, &lResult);
	if (!lResult.IsOK())
	{
		Disconnect();
		return FALSE;
	}

	m_pDevice->RegisterEventSink(this);

	// Open stream --------------------------------------------------
	m_pStream = PvStream::CreateAndOpen(m_szConnectionID, &lResult);
	if (!lResult.IsOK())
	{
		Disconnect();
		return FALSE;
	}

	m_pStream->RegisterEventSink(this);

	// GigE Vision devices only connection steps
	if (m_pDevice->GetType() == PvDeviceInfoTypeGEV)
	{
		PvDeviceGEV *lDeviceGEV = static_cast<PvDeviceGEV *>(m_pDevice);
		PvStreamGEV *lStreamGEV = static_cast<PvStreamGEV *>(m_pStream);

		PvString lLocalIpAddress = lStreamGEV->GetLocalIPAddress();
		uint16_t lLocalPort = lStreamGEV->GetLocalPort();

		// Perform automatic packet size negotiation
		lDeviceGEV->NegotiatePacketSize();

		// Now that the stream is opened, set the destination on the device
		lDeviceGEV->SetStreamDestination(lLocalIpAddress, lLocalPort);
	}

	PvGenParameterArray *lDeviceParams = m_pDevice->GetParameters();

	// Set device in RGB8 to match what our imaging library expects
	//lResult = lDeviceParams->SetEnumValue("PixelFormat", PvPixelBGRa8);
	lResult = lDeviceParams->SetEnumValue("PixelFormat", PvPixelMono8);
	if (lResult.IsOK())
	{
		int64_t i64PixelFormat;
		PvGenEnum* lPvGenEnum = m_pDevice->GetParameters()->GetEnum("PixelFormat");
		lResult = lPvGenEnum->GetValue(i64PixelFormat);

		if (m_nPixelFormat != i64PixelFormat)
		{
			m_nPixelFormat = (int)i64PixelFormat;
			//lPvGenEnum->RegisterEventSink(this);

			// Retrieve Pixel Size Byte per pixel (Bpp) information
			m_nPixelSize = ((m_nPixelFormat & 0x00FF0000) >> 16) / 8;
		}
	}

	// Create the PvPipeline object ---------------------------------
	m_pPipeline = new PvPipeline(m_pStream);

	// Register to all events of the parameters in the device's node map
	PvGenParameterArray *lGenDevice = m_pDevice->GetParameters();
	for (uint32_t i = 0; i < lGenDevice->GetCount(); i++)
	{
		//lGenDevice->GetString();
		lGenDevice->Get(i)->RegisterEventSink(this);
	}
	
	PvGenEnum *lMode = lGenDevice->GetEnum("AcquisitionMode");
	SetListAcquisitionMode(lMode);	

	// Create acquisition state manager
	m_pAcqStateMgr = new PvAcquisitionStateManager(m_pDevice, m_pStream);
	m_pAcqStateMgr->RegisterEventSink(this);

	//mAcquiringImages = false;

	// Force an update on all the parameters on acquisition mode
	OnParameterUpdate(lMode);

	// Ready image reception
	StartStreaming();

	m_bConnectionLost = FALSE;

	// Sync up UI
	EnableInterface();

	return TRUE;
}

//=============================================================================
// Method		: Disconnect
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
void CVisionCam::Disconnect()
{
	if (m_pDevice != NULL)
	{
		// Unregister all events of the parameters in the device's node map
		PvGenParameterArray *lGenDevice = m_pDevice->GetParameters();
		for (uint32_t i = 0; i < lGenDevice->GetCount(); i++)
		{
			lGenDevice->Get(i)->UnregisterEventSink(this);
		}
	}

	// If streaming, stop streaming
	StopStreaming();

	// Release acquisition state manager
	if (m_pAcqStateMgr != NULL)
	{		
// 		m_pAcqStateMgr->Stop();
// 		m_pAcqStateMgr->UnregisterEventSink(this);

		delete m_pAcqStateMgr;
		m_pAcqStateMgr = NULL;
	}

	// Reset device ID - can be called by the destructor when the window
	// no longer exists, be careful...

	if (m_pDevice != NULL)
	{
		if (m_pDevice->IsConnected())
		{
			// Unregister event sink (call-backs).
			m_pDevice->UnregisterEventSink(this);
		}

		PvDevice::Free(m_pDevice);
		m_pDevice = NULL;
	}

	if (m_pPipeline != NULL)
	{
		if (m_pPipeline->IsStarted())
		{
			if (!m_pPipeline->Stop().IsOK())
			{
				//std::cout << "Unable to stop the pipeline." << std::endl;
			}
		}

		delete m_pPipeline;
		m_pPipeline = NULL;
	}

	if (m_pStream != NULL)
	{
		if (m_pStream->IsOpen())
		{
			// Unregister event sink (call-backs).
			m_pStream->UnregisterEventSink(this);
			if (!m_pStream->Close().IsOK())
			{
				//std::cout << "Unable to stop the stream." << std::endl;
			}
		}

		PvStream::Free(m_pStream);
		m_pStream = NULL;
	}

	EnableInterface();
}

//=============================================================================
// Method		: ConnectAndRun
// Access		: public  
// Returns		: void
// Parameter	: LPCTSTR szIPAddress
// Qualifier	:
// Last Update	: 2016/6/9 - 15:28
// Desc.		:
//=============================================================================
void CVisionCam::ConnectAndRun(LPCTSTR szIPAddress)
{
	m_szConnectionID = szIPAddress;
	CreateThread_Loop();
}

//=============================================================================
// Method		: DisconnectAndExit
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
void CVisionCam::DisconnectAndExit()
{
	m_bLoopThread = FALSE;

	TearDown(TRUE);

	// 메모리 해제?
	DeleteThread_Loop();
}

//=============================================================================
// Method		: Start_AcquisitionMgr
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
void CVisionCam::Start_AcquisitionMgr()
{
	// Get payload size from device
	int64_t lPayloadSizeValue = DEFAULT_PAYLOAD_SIZE;
	if ((m_pDevice != NULL) && m_pDevice->IsConnected())
	{
		lPayloadSizeValue = m_pDevice->GetPayloadSize();
	}

	// If payload size is valid, force buffers re-alloc - better than 
	// adjusting as images are coming in
	if (lPayloadSizeValue > 0)
	{
		m_pPipeline->SetBufferSize(static_cast<uint32_t>(lPayloadSizeValue));
	}

	// Never hurts to start streaming on a fresh pipeline/stream...
	m_pPipeline->Reset();

	// Reset stream statistics
	m_pStream->GetParameters()->ExecuteCommand("Reset");

	m_pAcqStateMgr->Start();
}

//=============================================================================
// Method		: Stop_AcquisitionMgr
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
void CVisionCam::Stop_AcquisitionMgr()
{
	m_pAcqStateMgr->Stop();
}

//=============================================================================
// Method		: StartStreaming
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
void CVisionCam::StartStreaming()
{
	// Start pipeline
	m_pPipeline->Start();
}

//=============================================================================
// Method		: StopStreaming
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
void CVisionCam::StopStreaming()
{
	if (m_pPipeline != NULL)
	{
		// Stop stream thread
		if (m_pPipeline->IsStarted())
		{
			m_pPipeline->Stop();
		}
	}
}

//=============================================================================
// Method		: Start
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
void CVisionCam::Start()
{
	if (!m_pDevice->IsConnected())
	{
		return;
	}

	//Start_AcquisitionMgr();

	AcquireImages(m_pDevice, m_pStream, m_pPipeline);

	EnableInterface();
}

//=============================================================================
// Method		: Stop
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
void CVisionCam::Stop()
{
	if (!m_pDevice->IsConnected())
	{
		return;
	}

	//mAcquiringImages = false;
	//lStop->Execute();
	// Disable streaming on the device
	//cout << "Disable streaming on the controller." << endl;
	//m_pDevice->StreamDisable();
	// Stop the pipeline
	//cout << "Stop pipeline" << endl;
	//m_pPipeline->Stop();

	//Stop_AcquisitionMgr();

	EnableInterface();
}

//=============================================================================
// Method		: GetAcquiredImage
// Access		: public  
// Returns		: LPBYTE
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
LPBYTE CVisionCam::GetAcquiredImage()
{
	if (m_bAcquiringImages)
	{
		return m_lpbyRGB32;
	}
	else
	{
		return NULL;
	}
}

//=============================================================================
// Method		: GetAcquiredImage
// Access		: public  
// Returns		: BOOL
// Parameter	: __out LPBYTE & lpbyOutBuf
// Parameter	: __out DWORD & dwWidth
// Parameter	: __out DWORD & dwHeight
// Qualifier	:
// Last Update	: 2016/6/9 - 15:29
// Desc.		:
//=============================================================================
BOOL CVisionCam::GetAcquiredImage(__out LPBYTE& lpbyOutBuf, __out DWORD& dwWidth, __out DWORD& dwHeight)
{
	if (m_bAcquiringImages)
	{
		lpbyOutBuf = m_lpbyRGB32;
		dwWidth = m_nWidth;
		dwHeight = m_nHeight;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

